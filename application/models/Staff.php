<?php

class Application_Model_Staff extends Zend_Db_Table_Abstract {

    protected $_name = 'staff';

    /**
     * Một số config cục bộ
     * asm : các GROUP ID được xem dashboard vùng, ví dụ gồm ASM hay là gồm ASM và Sales Admin...3
     * -- viết tiếp nếu có config thêm
     * @var array
     */
    private $config = array(
        'asm' => array(
            ASM_ID,
            ASMSTANDBY_ID,
            SALES_ADMIN_ID,
        ),
        'ttl' => 60
    );

    function fetchPaginationScores($page, $limit, &$total, $params) {
        $db         = Zend_Registry::get('db');
        $sql_params = array(
            isset($params['pass']) ? $params['pass'] : null,
            isset($params['fail']) ? $params['fail'] : null,
            isset($params['result']) ? $params['result'] : null,
            null,
            isset($params['area_id']) ? $params['area_id'] : null,
            null,
            isset($params['name']) ? $params['name'] : null,
            isset($params['code']) ? $params['code'] : null,
            null,
            $limit,
            $page
        );

        $sql   = 'CALL sp_assessment(?,?,?,?,?,?,?,?,?,?,?,@total)';
        $stmt  = $db->query($sql, $sql_params);
        $list  = $stmt->fetchAll();
        $stmt->closeCursor();
        $total = $db->fetchOne('select @total');
        return $list;
    }

    public function fetchPaginationScoresNew($page, $limit, &$total, $params){
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            "p.code", 
            "fullname" => "CONCAT(p.firstname, ' ', p.lastname)", 
            "p.joined_at", 
            "area_name" => "a.name", 
            "d.total_attendance", 
            "d.total_kpi", 
            "d.total_ot", 
            "d.total_warning", 
            "d.total_reward", 
            "d.total_pass", 
            "d.total_fail",
            "d.total_other", 
            "d.total", 
            "d.result", 
            "d.thamnien",
            "upstar_result" => "u.result", 
            "upstar_date" => "u.date"
        );

        $select->from(array('p' => 'staff'), $arrCols);
        $select->joinLeft(array('r'=>'regional_market'), 'r.id = p.regional_market', array());
        $select->joinLeft(array('a'=>'area'), 'a.id = r.area_id', array());
        $select->joinLeft(array('d'=>'sp_assessment_temp_date'), "d.staff_id = p.id AND d.date = CURDATE()", array());

        $select->joinLeft(array('u'=>'staff_upstar_date'), "u.staff_id = p.id", array());

        $select->where("p.title IN (182,293,419,420)");

        if(!empty($params['code'])){
            $select->where("p.code = ?", $params['code']);
        }

        if(!empty($params['name'])){
            $select->where("CONCAT(p.firstname, ' ', p.lastname) LIKE ?", "%".$params['name']."%");
        }

        if(!empty($params['area_id'])){
            $select->where("a.id = ?", $params['area_id']);
        }

        if(!empty($params['area_list'])){
            $select->where("a.id IN (?)", $params['area_list']);
        }

        if(!empty($params['result'])){
            $select->where("u.result = ?", $params['result']);
        }

        if(!empty($params['from_date'])){
            $select->where("u.date >= ?", $params['from_date']);
        }

        if(!empty($params['to_date'])){
            $select->where("u.date <= ?", $params['to_date']);
        }

        if(empty($params['is_off'])){
            $select->where("p.off_date IS NULL");
        }
        else{
            $select->where("p.off_date IS NOT NULL");
        }

         if ($limit && empty($params['export']))
            $select->limitPage($page, $limit);

        if (isset($params['export']) && $params['export']){
            return $select;
        }

        $result = $db->fetchAll($select);

        if ($limit)
            $total  = $db->fetchOne("select FOUND_ROWS()");
        

        return $result;
    }

    function fetchPaginationScoresById($id) {

        $db      = Zend_Registry::get("db");
        $select  = $db->select();
        $select2 = $db->select();
        $select3 = $db->select();

        $arrCols = array(
            "p.*"
        );

        $select->from(array('p' => 'sp_assessment_temp_date'), $arrCols);
        $select->where('p.staff_id = ?', $id);
        $select->where('p.date = (select MAX(date) FROM sp_assessment_temp_date)', NULL);

        $result      = $db->fetchAll($select);
        // if($_GET['dev']){
        
        /*
        $arrColsView = array('total_attendance',
            'total_ot',
            'total_kpi',
            'total_warning',
            'total_reward', 'total_pass',
            'total_fail',
            'total',
            'result',
            'thamnien');

        $select2->from(['v' => 'v_first_day_pg_up_star'], $arrColsView);
        $select2->where('v.staff_id =? ', $id);
        $result2 = $db->fetchRow($select2);

        $select3->from(['v' => 'v_first_day_approved_up_star'], $arrColsView);
        $select3->where('v.staff_id =? ', $id);
        $result3 = $db->fetchRow($select3);

        // if($_GET['dev']){
        //     echo "<pre>";
        //     echo $select3->__toString();
        //     print_r($result);
        //     print_r($result2);
        //     print_r($result3);
        // }

        foreach ($result2 as $key => $value) {
            if (!empty($result[0][$key])) {
                if ($result[0][$key] > $value) {
                    $result[0][$key] = $result[0][$key] + ($value - $result3[$key]);
                }
            }
        }
        // }
        // print_r($result);
         * 
         */

        return $result;
    }

    function fetchPagination($page, $limit, &$total, $params) {
        $db = Zend_Registry::get('db');
        
        if ($limit) {
            $select = $db->select()
                    ->from(array('p' => $this->_name), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'), 'staff_code' => 'p.code', 'p.*'));
        } else {
            $select = $db->select()
                    ->from(array('p' => $this->_name), array(new Zend_Db_Expr('DISTINCT p.id'), 'staff_code' => 'p.code', 'p.*'));
        }
        $select->joinLeft(array("rm_default" => 'regional_market'), 'rm_default.parent = p.regional_market and rm_default.default_contract_district=1', array('district_name_insurance_default' => 'rm_default.name_insurance', 'district_code_default' => 'rm_default.code_insurance'));

        $select->joinLeft(array('sc' => 'staff_contract'), 'p.id = sc.staff_id AND sc.print_type = 1 and sc.is_next = 2 and sc.is_disable = 0 and sc.is_expired = 0 ', array('contract_from' => 'sc.from_date', 'contract_to' => 'sc.to_date', 'staff_contract_term' => 'sc.contract_term'));

        $select->joinLeft(array('e' => 'staff_education'), 'p.id = e.staff_id AND e.default_level = 1', array('d_level' => 'e.level', 'd_certificate' => 'field_of_study'));

        $select->joinLeft(array('bb' => 'level_staff'), 'bb.id = e.level_id', array('level_n' => "MAX((bb.Priority *100000000000) + (e.id * 1000000) + bb.id) % 1000000", 'level_name' => 'bb.name'));

        $select->joinLeft(array('pr'=>'province'), 'pr.id = p.id_place_province', array('id_place_province_name' => 'pr.name'));
        
        $select->joinLeft(array('pr2'=>'province'), 'pr2.id = p.id_citizen_province', array('id_place_province_name2' => 'pr2.name'));

        $select->joinLeft(array('regional_market'), 'regional_market.id = p.district', array('district_name_insurance' => 'IFNULL(regional_market.name_insurance,rm_default.name_insurance)', 'district_code' => 'IFNULL(regional_market.code_insurance,rm_default.code_insurance)'));

        $select->joinLeft(array('t' => 'team'), 'p.title = t.id', array('access_group' => 't.access_group', 'title_name' => 't.name'));

        $select->joinLeft(array('cg' => 'company_group'), 't.policy_group = cg.id', array('policy_group_name' => 'cg.name'));
        // lấy district office location
        $select->joinLeft(array('office' => 'office'), 'p.office_id = office.id', array());
        $select->joinLeft(array('rm_district' => 'regional_market'), 'rm_district.id = office.district', array('district_office' => 'rm_district.name'));
        //
        //$select->joinLeft(array('cg2'=>'company_group'),'cg.parent = cg2.id',array('company_group_name' => 'cg2.name'));
        $select->joinLeft(array('cg2' => 'company_group'), 't.company_group = cg2.id', array('company_group_name' => 'cg2.name'));


        $select->joinLeft(array('ss' => 'store_staff'), 'ss.staff_id = p.id', array());

        $select->joinLeft(array('sss' => 'store'), 'ss.store_id = sss.id AND sss.is_brand_shop = 1', array('brand_shop_address' => 'sss.name'));

        //TUONG
        $select->joinLeft(array('pit' => 'dependent_person_staff'), 'pit.staff_id = p.id', array('name_pit' => 'pit.name', 'dob_pit' => 'pit.dob', 'mst_pit' => 'pit.mst', 'relative' => 'pit.relative', 'from_month' => 'pit.from_month', 'to_month' => 'pit.to_month'));
        $select->joinLeft(array('bank' => 'staff_my_bank'), 'bank.staff_id = p.id', array('bank_number' => 'bank.bank_number', 'at_bank' => 'bank.at_bank', 'branch' => 'bank.branch_pgd', 'province_city' => 'bank.province_city'));
        $select->joinLeft(array('mst' => 'pti'), 'mst.staff_id = p.id', array('tax' => 'mst.tax'));
        //TUONG
        //TIEN
        /* if (isset($params['export']) && $params['export']) {
          $select->joinLeft(array('sta' => 'staff_address'), 'sta.staff_id = p.id and sta.address_type=1', array('bd_type' => 'sta.address_type', 'bd_province_code' => 'sta.province_code', 'bd_address' => 'sta.address', 'bd_ward' => 'sta.ward_id','bd_district' => 'sta.district'));
          $select->joinLeft(array('sta1' => 'staff_address'), 'sta1.staff_id = p.id and sta1.address_type=2', array('t_type' => 'sta1.address_type', 't_province_code' => 'sta1.province_code', 't_address' => 'sta1.address', 't_ward' => 'sta1.ward_id', 't_district' => 'sta1.district'));
          $select->joinLeft(array('sta2' => 'staff_address'), 'sta2.staff_id = p.id and sta2.address_type=3', array('p_type' => 'sta2.address_type', 'p_province_code' => 'sta2.province_code', 'p_address' => 'sta2.address', 'p_ward' => 'sta2.ward_id', 'p_district' => 'sta2.district'));
          $select->joinLeft(array('sta3' => 'staff_address'), 'sta3.staff_id = p.id and sta3.address_type=4', array('id_c_type' => 'sta3.address_type', 'id_c_province_code' => 'sta3.province_code', 'id_c_address' => 'sta3.address', 'id_c_ward' => 'sta3.ward_id', 'id_c_district' => 'sta3.district'));
          $select->joinLeft(array('to' => 'tag_object'), '`to`.type = 1 AND `to`.object_id = p.id', array('tax' => 'mst.tax'));
          $select->joinLeft(array('tg' => 'tag'), 'tg.id = `to`.tag_id', array('tag_name' => 'GROUP_CONCAT(DISTINCT(tg.name))'));
          }
         */
        //TIEN
        //Trang
        if (isset($params['export']) && $params['export']) {
            $select->joinLeft(array('sta' => 'staff_address'), 'sta.staff_id = p.id and sta.address_type=1', array('bd_type' => 'sta.address_type', 'bd_address' => 'sta.address', 'bd_ward' => 'sta.ward_id', 'bd_district' => 'sta.district'))
                    ->joinLeft(array('rmk' => 'regional_market'), "sta.	district = rmk.id", array("id_regional_test" => "rmk.id"))
                    ->joinLeft(array('rmkk' => 'regional_market'), "rmk.parent = rmkk.id", array("bd_province_name" => "rmkk.name"));

            $select->joinLeft(array('sta1' => 'staff_address'), 'sta1.staff_id = p.id and sta1.address_type=2', array('t_type' => 'sta1.address_type', 't_address' => 'sta1.address', 't_ward' => 'sta1.ward_id', 't_district' => 'sta1.district'))
                    ->joinLeft(array('rmk1' => 'regional_market'), "sta1.district = rmk1.id", array("id_regional_test1" => "rmk1.id"))
                    ->joinLeft(array('rmkk1' => 'regional_market'), "rmk1.parent = rmkk1.id", array("t_province_name" => "rmkk1.name"));

            $select->joinLeft(array('sta2' => 'staff_address'), 'sta2.staff_id = p.id and sta2.address_type=3', array('p_type' => 'sta2.address_type', 'p_address' => 'sta2.address', 'p_ward' => 'sta2.ward_id', 'p_district' => 'sta2.district'))
                    ->joinLeft(array('rmk2' => 'regional_market'), "sta2.district = rmk2.id", array("id_regional_test2" => "rmk2.id"))
                    ->joinLeft(array('rmkk2' => 'regional_market'), "rmk2.parent = rmkk2.id", array("p_province_name" => "rmkk2.name"));

            $select->joinLeft(array('sta3' => 'staff_address'), 'sta3.staff_id = p.id and sta3.address_type=4', array('id_c_type' => 'sta3.address_type', 'id_c_address' => 'sta3.address', 'id_c_ward' => 'sta3.ward_id', 'id_c_district' => 'sta3.district'))
                    ->joinLeft(array('rmk3' => 'regional_market'), "sta3.district = rmk3.id", array("id_regional_test3" => "rmk3.id"))
                    ->joinLeft(array('rmkk3' => 'regional_market'), "rmk3.parent = rmkk3.id", array("id_c_province_name" => "rmkk3.name"));

            $select->joinLeft(array('to' => 'tag_object'), '`to`.type = 1 AND `to`.object_id = p.id', array('tax' => 'mst.tax'));
            $select->joinLeft(array('tg' => 'tag'), 'tg.id = `to`.tag_id', array('tag_name' => 'GROUP_CONCAT(DISTINCT(tg.name))'));
        }
        //TRANG

        if (isset($params['name']) and $params['name']) {
            $select->where('( CONCAT(p.firstname, " ",p.lastname) LIKE ?', '%' . $params['name'] . '%');
            $select->orwhere('p.code LIKE ? ', '%' . $params['name'] . '%');
            $select->orwhere('p.email LIKE ? )', '%' . $params['name'] . '%');
        }

        if (isset($params['is_print']) and $params['is_print']) {
            $select->where('p.is_print = ?', 1);
        }

        if (isset($params['company_id']) and $params['company_id']) {
            $select->where('p.company_id = ?', $params['company_id']);
        }

        if (isset($params['team']) and $params['team']) {
            if (is_array($params['team']) && count($params['team']) > 0) {
                $select->where('p.team IN (?)', $params['team']);
            } else {
                $select->where('1=0', 1);
            }
        }


        if (isset($params['contract_term']) and $params['contract_term']) {
            if (is_array($params['contract_term']) && count($params['contract_term']) > 0) {
                $select->where('p.contract_term IN (?)', $params['contract_term']);
            } else {
                $select->where('1=0', 1);
            }
        }

        if (isset($params['department']) and $params['department']) {
            if (is_array($params['department']) && count($params['department']) > 0) {
                $select->where('p.department IN (?)', $params['department']);
            } else {
                $select->where('1=0', 1);
            }
        }

        if (isset($params['salary_sales']) and $params['salary_sales']) {
            $select->where('p.title IN (?)', array(PGPB_TITLE, PGPB_2_TITLE, SALES_TITLE, SALES_ACCESSORIES_TITLE, SALES_LEADER_TITLE, SALES_ACCESSORIES_LEADER_TITLE));
        }

        if (isset($params['is_officer']) and intval($params['is_officer']) > 0) {
            if ($params['is_officer'] == 1)
                $select->where('p.is_officer = ? or p.team = 11 or p.title in (274,179,181)', 1);
            elseif ($params['is_officer'] == 2)
                $select->where('p.is_officer ? or p.team = 11 or p.title in (274,179,181)', 0);
            //$select->orWhere('p.team = ?', WARRANTY_CENTER);
        }

        if (isset($params['email']) and $params['email']) {
            $params['email'] = preg_replace('/' . EMAIL_SUFFIX . '/', '', $params['email']);
            $select->where('p.email LIKE ?', $params['email'] . EMAIL_SUFFIX);
        }

        if (isset($params['date']) and $params['date']) {
            $date = explode(',', $params['date']);
            $select->where("DAY(STR_TO_DATE(p.dob, '%d/%m/%Y')) IN (?)", $date);
        }

        if (isset($params['month']) and $params['month']) {
            $date = explode(',', $params['month']);
            $select->where("MONTH(STR_TO_DATE(p.dob, '%d/%m/%Y')) IN (?)", $date);
        }

        if (isset($params['month_off']) and $params['month_off']) {
            $select
                    ->joinLeft(array('mo' => 'off_date_add'), 'p.id = mo.staff_id ', array('staff_id' => 'mo.staff_id'));
            $select->where('MONTH(mo.date) <> ?', $params['month_off'])->orWhere('MONTH(mo.date) is null', null);
        }

        if (isset($params['year']) and $params['year']) {
            $date = explode(',', $params['year']);
            $select->where("YEAR(STR_TO_DATE(p.dob, '%d/%m/%Y')) IN (?)", $date);
        }

        if (isset($params['ready_print']) and $params['ready_print']) {

            if ($params['ready_print'] == 1) {
                $select->where("p.address <> '' and p.birth_place <> ''  and p.ID_number <> '' and p.ID_place <> '' and p.ID_date <> '' and p.title <> '' and p.company_id <> 0");
            } elseif ($params['ready_print'] == 2) {
                $select->where("address = '' or birth_place = ''  or ID_number = '' or ID_place = '' or ID_date = '' or title = '' or company_id = 0");
            } elseif ($params['ready_print'] == 3) {
                $select->where("p.ID_number <> '' and p.ID_place <> '' and p.ID_date <> '' and p.title <> '' and p.company_id <> 0 and EXISTS (select * from staff_address sa2 where p.id = sa2.staff_id and sa2.address_type = 4 and sa2.address is not null)");
            } elseif ($params['ready_print'] == 4) {
                $select->where("p.ID_number = '' or p.ID_place = '' or p.ID_date = '' or p.title = '' or p.company_id = 0 or  and EXISTS (select * from staff_address sa2 where p.id = sa2.staff_id and sa2.address_type = 4 and sa2.address is not null)");
            }
        }

        if (isset($params['title']) and $params['title']) {

            if (is_array($params['title']) && count($params['title']) > 0) {
                $select->where('p.title IN (?)', $params['title']);
            } else {
                $select->where('1=0', 1);
            }
        }

        $select->joinLeft(array('st' => 'staff_temp'), '
                p.id = st.staff_id
            ', array('staff_temp_id' => 'st.id', 'staff_temp_is_approved' => 'st.is_approved'));

        if (isset($params['need_approve']) and $params['need_approve']) {
            $select->where('st.is_approved = ?', 0);
        }

        if (( isset($params['off']) and intval($params['off']) > 0 ) || ( isset($params['sname']) and $params['sname'] == 1 ))
            $select->where('p.off_date is '
                    . ( $params['off'] == 2 ? 'not' : '')
                    . ' null');

        // list
        if (isset($params['exp']) and $params['exp'])
            $select->where('p.contract_expired_at > NOW()');

        //joined at
        if (isset($params['joined_at']) and $params['joined_at']) {
            $arrFrom             = explode('/', $params['joined_at']);
            $params['joined_at'] = $arrFrom[2] . '-' . $arrFrom[1] . '-' . $arrFrom[0];
            $select->where('p.joined_at = ?', $params['joined_at']);
        }
        //joined_at_from at
        if (isset($params['joined_at_from']) and $params['joined_at_from']) {
            $arrFrom                 = explode('/', $params['joined_at_from']);
            $params['joined_at_from'] = $arrFrom[2] . '-' . $arrFrom[1] . '-' . $arrFrom[0];
            $select->where('p.joined_at >= ?', $params['joined_at_from']);
        }
        //joined_at_to at
        if (isset($params['joined_at_to']) and $params['joined_at_to']) {
            $arrFrom               = explode('/', $params['joined_at_to']);
            $params['joined_at_to'] = $arrFrom[2] . '-' . $arrFrom[1] . '-' . $arrFrom[0];
            $select->where('p.joined_at <= ?', $params['joined_at_to']);
        }
         //off_date_from at
        if (isset($params['off_date_from']) and $params['off_date_from']) {
            $arrFrom                 = explode('/', $params['off_date_from']);
            $params['off_date_from'] = $arrFrom[2] . '-' . $arrFrom[1] . '-' . $arrFrom[0];
            $select->where('p.off_date >= ?', $params['off_date_from']);
        }
        //off_date_to at
        if (isset($params['off_date_to']) and $params['off_date_to']) {
            $arrFrom               = explode('/', $params['off_date_to']);
            $params['off_date_to'] = $arrFrom[2] . '-' . $arrFrom[1] . '-' . $arrFrom[0];
            $select->where('p.off_date <= ?', $params['off_date_to']);
        }
        // id_number
        if (isset($params['indentity']) and $params['indentity']) {
            $select->where('p.ID_number = ?', $params['indentity']);
        }

        if (isset($params['off_type']) and $params['off_type']) {
            $select->where('p.off_type = ?', $params['off_type']);
        }

        //off_date
        if (isset($params['off_date']) and $params['off_date']) {
            $arrFrom            = explode('/', $params['off_date']);
            $params['off_date'] = $arrFrom[2] . '-' . $arrFrom[1] . '-' . $arrFrom[0];
            $select->where('p.off_date = ?', $params['off_date']);
        }

        if (isset($params['sales_team_id']) and $params['sales_team_id'])
            $select->where('p.sales_team_id = ?', $params['sales_team_id']);

        if (isset($params['is_leader']) and $params['is_leader'])
            $select->where('p.is_leader = ?', $params['is_leader']);

        if (isset($params['distribution_id']) and $params['distribution_id'])
            $select->where('p.distribution_id = ?', $params['distribution_id']);

        if (isset($params['note']) and $params['note'])
            $select->where('p.note LIKE ?', '%' . $params['note'] . '%');

        if (isset($params['code']) and $params['code'])
            $select->where('p.code LIKE ?', '%' . $params['code'] . '%');

        if (isset($params['has_photo']) and $params['has_photo'])
            $select->where('p.photo IS NOT NULL AND p.photo <> \'\'', 1);


        if (isset($params['ood']) and $params['ood']) {
            $select->where(' DATEDIFF(p.contract_expired_at,NOW()) <= ?', 30);
            /* $select->where(' DATEDIFF(NOW(),p.contract_expired_at) >= ?', 0); */
        }

        if (isset($params['no_print']) and $params['no_print']) {
            $select->where('p.print_time = 0 OR p.contract_signed_at is NULL and p.contract_expired_at is NULL', '');
        }

        if (isset($params['is_no_body']) and $params['is_no_body']) {
            $select->where('p.image_body IS NULL', '');
        }

        if (isset($params['tags']) and $params['tags']) {
            $select->join(array('ta_ob' => 'tag_object'), '
                    p.id = ta_ob.object_id
                    AND ta_ob.type = ' . TAG_STAFF . '
                ', array());
            $select->join(array('ta' => 'tag'), '
                    ta.id = ta_ob.tag_id
                ', array());

            $select->where('ta.name IN (?)', $params['tags']);
        }

        if (isset($params['area_id']) and $params['area_id'] && !(isset($params['regional_market']) and $params['regional_market'])) {
            $QRegionalMarket = new Application_Model_RegionalMarket();

            if (is_array($params['area_id']) && count($params['area_id']) > 0) {
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id IN (?)', $params['area_id']);
            } else {
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $params['area_id']);
            }

            $regional_markets = $QRegionalMarket->fetchAll($where);
            $tem              = array();

            foreach ($regional_markets as $regional_market)
                $tem[] = $regional_market->id;

            if (is_array($tem) && count($tem) > 0)
                $select->where('p.regional_market IN (?)', $tem);
            else
                $select->where('1=0', 1);
        } elseif (isset($params['regional_market']) and $params['regional_market']) {
            if (is_array($params['regional_market']) && count($params['regional_market']) > 0) {
                $select->where('p.regional_market IN (?)', $params['regional_market']);
            } else {
                $select->where('1=0', 1);
            }
        }

        // dành để lọc staff cho các asm, admin... xem; chỉ hiện thị staff thuộc khu vực được gán
        if (isset($params['list_province_ids']) && $params['list_province_ids']) {

            if (is_array($params['list_province_ids']) && count($params['list_province_ids'])) {

                $select->where('p.regional_market IN (?)', $params['list_province_ids']);
                //723 dept brandshop : sale admin quản lý dept branchshop
                $select->where('p.team in (75,119,294,397,133, 131, 611, 619,630) OR p.department IN(723,733,743)', '');
                $select->where("t.company_group IN (2,3) OR p.code IN ('1308HN98') ", '');
            } elseif (is_numeric($params['list_province_ids']) && intval($params['list_province_ids']) > 0) {
                $select->where('p.regional_market = ?', intval($params['list_province_ids']));
            } elseif ($params['list_province_ids'] == -1) {
                $select->where('1=0', 1);
            }
        }

        if (isset($params['area_trainer_right']) and $params['area_trainer_right']) {
            $QRegionalMarket = new Application_Model_RegionalMarket();

            if (is_array($params['area_trainer_right']) && count($params['area_trainer_right']) > 0) {
                $whereRegionalMarketRight = $QRegionalMarket->getAdapter()->quoteInto('area_id IN (?)', $params['area_trainer_right']);
            } else {
                $whereRegionalMarketRight = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $params['area_trainer_right']);
            }

            $regional_markets_rights = $QRegionalMarket->fetchAll($whereRegionalMarketRight);
            $tem                     = array();

            foreach ($regional_markets_rights as $regional_markets_right)
                $tem[] = $regional_markets_right->id;

            if (is_array($tem) && count($tem) > 0)
                $select->where('p.regional_market IN (?)', $tem);
            else
                $select->where('1=0', 1);
        }

        if (isset($params['s_assign']) && $params['s_assign']) {
            $QDeparment    = new Application_Model_Department();
            $where         = $QDeparment->getAdapter()->quoteInto('name LIKE ?', 'KINH DOANH');
            $department_id = $QDeparment->fetchAll($where)->current()->id;
            $select->where('p.department = ?', $department_id);
        }

        $select->where('p.joined_at IS NOT NULL', 1);

        $order_str = '';

        if (isset($params['sort']) and $params['sort']) {
            $collate = ' ';
            $desc    = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';

            if ($params['sort'] == 'name') {
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= ' CONCAT(p.firstname, " ",p.lastname) ' . $collate . $desc;
            } elseif (/* $params['sort'] == 'title' || */$params['sort'] == 'department' || $params['sort'] == 'team') {
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= $collate . $desc;
            } else {
                $order_str = 'p.`' . $params['sort'] . '` ' . $collate . $desc;
            }

            $select->order(new Zend_Db_Expr($order_str));
        }

        $select->group('p.id');
       
        if (!empty($_GET['dev'])) {
            echo "<pre>";
            print_r($select->__toString());
            exit;
        }

        if ($limit)
            $select->limitPage($page, $limit);
        if (isset($params['export']) && $params['export'])
            return $select->__toString();

        $result = $db->fetchAll($select);
        if ($limit)
            $total  = $db->fetchOne("select FOUND_ROWS()");
            
          
        return $result;
    }

    function fetchForSearch($provinces) {
        $db     = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('s' => $this->_name), array('s.id', 's.code', 's.firstname', 's.lastname', 's.email'))
                ->where('s.regional_market IN (?)', $provinces)
                ->where('s.title IN (?)', array(PGPB_TITLE, PGPB_2_TITLE, SALES_TITLE, LEADER_TITLE, SALES_TRAINEE_TITLE))
                ->order('s.lastname');
        return $db->query($select->__toString());
    }

    function fetchLeaderPagination($page, $limit, &$total, $params) {
        $db = Zend_Registry::get('db');

        if ($limit) {
            $select = $db->select()
                    ->from(array('p' => $this->_name), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'), 'p.firstname', 'p.lastname', 'p.email', 'p.phone_number', 'p.regional_market'));
        } else {
            $select = $db->select()
                    ->from(array('p' => $this->_name), array(new Zend_Db_Expr('DISTINCT p.id'), 'p.firstname', 'p.lastname', 'p.email', 'p.phone_number', 'p.regional_market'));
        }

        $select
                ->distinct()
                ->joinLeft(array('sl' => 'store_leader'), 'sl.staff_id=p.id AND sl.status=1', array('current_store' => 'COUNT(DISTINCT sl.store_id)'))
                ->joinLeft(array('sll' => 'store_leader'), 'sll.staff_id=p.id AND sll.status=0', array('pending_store' => 'COUNT(DISTINCT sll.store_id)'))
                ->group('p.id');

        $select->where('p.off_date is null and p.title = ?', LEADER_TITLE);

        if (isset($params['name']) and $params['name']) {
            $select->where('CONCAT(p.firstname, " ",p.lastname) LIKE ?', '%' . $params['name'] . '%');
        }

        if (isset($params['email']) and $params['email']) {
            $params['email'] = preg_replace('/' . EMAIL_SUFFIX . '/', '', $params['email']);
            $select->where('p.email LIKE ?', $params['email'] . EMAIL_SUFFIX);
        }

        if (isset($params['area_id']) and $params['area_id'] && !(isset($params['province']) and $params['province'])) {
            $QRegionalMarket = new Application_Model_RegionalMarket();
            $where           = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $params['area_id']);

            $regional_markets = $QRegionalMarket->fetchAll($where);
            $tem              = array();

            foreach ($regional_markets as $regional_market)
                $tem[] = $regional_market->id;

            if (is_array($tem) && count($tem) > 0)
                $select->where('p.regional_market IN (?)', $tem);
            else
                $select->where('1=0', 1);
        } elseif (isset($params['province']) and $params['province']) {
            $select->where('p.regional_market = ?', $params['province']);
        }

        if (isset($params['store']) && $params['store']) {
            $QStore = new Application_Model_Store();
            $where  = $QStore->getAdapter()->quoteInto('name LIKE ?', '%' . $params['store'] . '%');
            $result = $QStore->fetchAll($where);

            $store_ids = array();
            foreach ($result as $key => $value) {
                $store_ids[] = $value['id'];
            }

            $select->where('sl.store_id IN (?) OR sll.store_id IN (?)', $store_ids);
        }

        // Filter for ASM and Leader
        if (isset($params['asm']) and $params['asm']) {
            $QAsm         = new Application_Model_Asm();
            $list_regions = $QAsm->get_cache($params['asm']);
            $list_regions = isset($list_regions['province']) && is_array($list_regions['province']) ? $list_regions['province'] : array();

            if (count($list_regions))
                $select->where('p.regional_market IN (?)', $list_regions);
            else
                $select->where('1=0', 1);
        } elseif (isset($params['leader']) and $params['leader']) {
            $select->where('p.id = ?', $params['leader']);
        }

        $order_str = '';

        if (isset($params['sort']) and $params['sort']) {
            $collate = ' ';

            if ($params['sort'] == 'area_id') {
                $select
                        ->join(array('r' => 'regional_market'), 'r.id=p.regional_market', array())
                        ->join(array('a' => 'area'), 'a.id=r.area_id', array());
            } elseif ($params['sort'] == 'province') {
                $select
                        ->join(array('r' => 'regional_market'), 'r.id=p.regional_market', array());
            }

            $desc = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';

            if (in_array($params['sort'], array('area_id', 'province', 'name'))) {
                $collate = ' COLLATE utf8_unicode_ci ';
            }

            if ($params['sort'] == 'name') {
                $order_str .= ' CONCAT(p.firstname, " ",p.lastname) ' . $collate . $desc;
            } elseif ($params['sort'] == 'area_id') {
                $order_str .= ' a.name ' . $collate . $desc;
            } elseif ($params['sort'] == 'province') {
                $order_str .= ' r.name ' . $collate . $desc;
            } elseif (in_array($params['sort'], array('current_store', 'pending_store'))) {
                $order_str .= $params['sort'] . ' ' . $desc;
            } else {
                $order_str = 'p.`' . $params['sort'] . '` ' . $collate . $desc;
            }

            $select->order(new Zend_Db_Expr($order_str));
        }

        if (!empty($_GET['dev'])) {
            echo $select->__toString();
            exit;
        }

        if ($limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);

        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    function fetchSalesPgPagination($page, $limit, &$total, $params) {
        $db = Zend_Registry::get('db');
        /*
          $subselect_pg = $db
          ->select()
          ->from(array('sll' => 'store_staff_log'),
          array())
          ->join(array('s1' => 'store'), 's1.id = sll.store_id', array('tgdd' => 's1.name','staff_id' => 'sll.staff_id'))
          ->where('sll.released_at is null and s1.name like "%TGDĐ%"')
          ;
          // echo "<pre>";print_r($subselect_pg->__toString());die;
         */


        $select = $db->select()
                ->from(array('p' => $this->_name), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'), 'p.code', 'p.firstname', 'p.lastname', 'p.email', 'LENGTH(p.email) length_email', 'p.phone_number', 'p.regional_market', 'p.group_id', 'p.status', 'p.off_date'));

        $select
                ->distinct()
                ->joinLeft(array('cw' => 'casual_worker'), 'cw.staff_id=p.id', array('casual_status' => 'cw.status'))
                ->joinLeft(array('sl' => 'store_staff_log'), 'sl.staff_id=p.id ', array('current_store' => 'COUNT(DISTINCT sl.store_id)', 'sl.is_leader'))
                /* ->joinLeft(
                  array('rs' => $subselect_pg),
                  'rs.staff_id = p.id',
                  array('is_tgdd' => 'rs.tgdd',)
                  ) */
                ->group('p.id');
        //723 dept brandshop
        $select
                ->where('(p.title = ?', PGPB_TITLE)
                ->orWhere('p.title = ?', CHUYEN_VIEN_BAN_HANG_TITLE)
                ->orWhere('p.title = ?', PG_BRANDSHOP)
                ->orWhere('p.title = ?', SENIOR_PROMOTER_BRANDSHOP)
                ->orWhere('p.title = ?', PRODUCT_CONSULTANT_BRANDSHOP)
                ->orWhere('p.title = ?', PGS_SUPERVISOR)
                ->orWhere('p.title = ?', SALES_TITLE)
                ->orWhere('p.title = ?', STORE_LEADER)
                ->orWhere('p.title = ?', LEADER_TITLE)
                ->orWhere('p.title = ?', PG_LEADER_TITLE)
                ->orWhere('p.department = ?)', BRAND_SHOP_DEPT);
        // $select->where('sl.released_at IS NULL ', PGPB_TITLE);




        if (isset($params['email']) and $params['email']) {
            $params['email'] = preg_replace('/' . EMAIL_SUFFIX . '/', '', $params['email']);
            $select->where('p.email LIKE ?', $params['email'] . EMAIL_SUFFIX);
        }

        if (isset($params['province']) and $params['province']) {
            $select->where('p.regional_market = ?', $params['province']);
        } elseif (isset($params['area_id']) and $params['area_id']) {
            $QRegionalMarket = new Application_Model_RegionalMarket();
            $where           = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $params['area_id']);

            $regional_markets = $QRegionalMarket->fetchAll($where);
            $tem              = array();

            foreach ($regional_markets as $regional_market)
                $tem[] = $regional_market->id;

            if (is_array($tem) && count($tem) > 0)
                $select->where('p.regional_market IN (?)', $tem);
            else
                $select->where('1=0', 1);
        }

        if (isset($params['store']) && $params['store']) {
            $QStore = new Application_Model_Store();
            $where  = $QStore->getAdapter()->quoteInto('name LIKE ?', '%' . $params['store'] . '%');
            $result = $QStore->fetchAll($where);

            $store_ids = array();
            foreach ($result as $key => $value) {
                $store_ids[] = $value['id'];
            }

            $select->where('sl.store_id IN (?) OR sl.store_id IN (?)', $store_ids);
        }



        // 1 là on 2 là off
        if (isset($params['status']) and $params['status']) {

            if ($params['status'] == 2) {
                $select->where('p.off_date is not null');
            } else {
                $select->where('p.off_date is null');
            }
        }

        if (isset($params['code']) and $params['code']) {
            $select->where('p.code LIKE ?', '%' . $params['code'] . '%');
        }
        if (isset($params['name']) and $params['name']) {
            $select->where('CONCAT(p.firstname, " ",p.lastname) LIKE ?', '%' . $params['name'] . '%');
        }
        // Filter for ASM
        if (isset($params['asm']) and $params['asm']) {
            $QAsm         = new Application_Model_Asm();
            $list_regions = $QAsm->get_cache($params['asm']);
            $list_regions = isset($list_regions['province']) && is_array($list_regions['province']) ? $list_regions['province'] : array();

            if (count($list_regions))
                $select->where('p.regional_market IN (?)', $list_regions);
            else
                $select->where('1=0', 1);
        }

        if (isset($params['staff']) and $params['staff']) {
            $select->where('p.id = ?', intval($params['staff']));
        }

        $order_str = '';

        if (isset($params['sort']) and $params['sort']) {
            $collate = ' ';

            if ($params['sort'] == 'area_id') {
                $select
                        ->join(array('r' => 'regional_market'), 'r.id=p.regional_market', array())
                        ->join(array('a' => 'area'), 'a.id=r.area_id', array());
            } elseif ($params['sort'] == 'province') {
                $select
                        ->join(array('r' => 'regional_market'), 'r.id=p.regional_market', array());
            }

            $desc = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';

            if (in_array($params['sort'], array('area_id', 'province', 'name'))) {
                $collate = ' COLLATE utf8_unicode_ci ';
            }

            if ($params['sort'] == 'name') {
                $order_str .= ' CONCAT(p.firstname, " ",p.lastname) ' . $collate . $desc;
            } elseif ($params['sort'] == 'area_id') {
                $order_str .= ' a.name ' . $collate . $desc;
            } elseif ($params['sort'] == 'province') {
                $order_str .= ' r.name ' . $collate . $desc;
            } elseif (in_array($params['sort'], array('current_store', 'pending_store'))) {
                $order_str .= $params['sort'] . ' ' . $desc;
            } else {
                $order_str = 'p.`' . $params['sort'] . '` ' . $collate . $desc;
            }

            $select->order(new Zend_Db_Expr($order_str));
        }



        if ($limit)
            $select->limitPage($page, $limit);
        if (!empty($_GET['dev'])) {
            echo "<pre>";
            print_r($select->__toString());
            die;
        }
        $result = $db->fetchAll($select);


        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    function fetchSalesBrandshopPagination($page, $limit, &$total, $params) {
        $db = Zend_Registry::get('db');

        $select = $db->select()
                ->from(array('p' => $this->_name), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'), 'p.firstname', 'p.lastname', 'p.email', 'p.phone_number', 'p.regional_market', 'p.group_id', 'p.status'));

        $select
                ->distinct()
                ->joinLeft(array('cw' => 'casual_worker'), 'cw.staff_id=p.id', array('casual_status' => 'cw.status'))
                ->joinLeft(array('sl' => 'store_staff_log'), 'sl.staff_id=p.id', array('current_store' => 'COUNT(DISTINCT sl.store_id)', 'sl.is_leader'))
                ->group('p.id');

        $select
                ->where('sl.is_leader = 6 and p.title = ?', 424);


        if (isset($params['name']) and $params['name']) {
            $select->where('CONCAT(p.firstname, " ",p.lastname) LIKE ?', '%' . $params['name'] . '%');
        }

        if (isset($params['email']) and $params['email']) {
            $params['email'] = preg_replace('/' . EMAIL_SUFFIX . '/', '', $params['email']);
            $select->where('p.email LIKE ?', $params['email'] . EMAIL_SUFFIX);
        }

        if (isset($params['province']) and $params['province']) {
            $select->where('p.regional_market = ?', $params['province']);
        } elseif (isset($params['area_id']) and $params['area_id']) {
            $QRegionalMarket = new Application_Model_RegionalMarket();
            $where           = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $params['area_id']);

            $regional_markets = $QRegionalMarket->fetchAll($where);
            $tem              = array();

            foreach ($regional_markets as $regional_market)
                $tem[] = $regional_market->id;

            if (is_array($tem) && count($tem) > 0)
                $select->where('p.regional_market IN (?)', $tem);
            else
                $select->where('1=0', 1);
        }

        if (isset($params['store']) && $params['store']) {
            $QStore = new Application_Model_Store();
            $where  = $QStore->getAdapter()->quoteInto('name LIKE ?', '%' . $params['store'] . '%');
            $result = $QStore->fetchAll($where);

            $store_ids = array();
            foreach ($result as $key => $value) {
                $store_ids[] = $value['id'];
            }

            $select->where('sl.store_id IN (?) OR sl.store_id IN (?)', $store_ids);
        }

        // Filter for ASM
        if (isset($params['asm']) and $params['asm']) {
            $QAsm         = new Application_Model_Asm();
            $list_regions = $QAsm->get_cache($params['asm']);
            $list_regions = isset($list_regions['province']) && is_array($list_regions['province']) ? $list_regions['province'] : array();

            if (count($list_regions))
                $select->where('p.regional_market IN (?)', $list_regions);
            else
                $select->where('1=0', 1);
        }

        if (isset($params['staff']) and $params['staff']) {
            $select->where('p.id = ?', intval($params['staff']));
        }

        $order_str = '';

        if (isset($params['sort']) and $params['sort']) {
            $collate = ' ';

            if ($params['sort'] == 'area_id') {
                $select
                        ->join(array('r' => 'regional_market'), 'r.id=p.regional_market', array())
                        ->join(array('a' => 'area'), 'a.id=r.area_id', array());
            } elseif ($params['sort'] == 'province') {
                $select
                        ->join(array('r' => 'regional_market'), 'r.id=p.regional_market', array());
            }

            $desc = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';

            if (in_array($params['sort'], array('area_id', 'province', 'name'))) {
                $collate = ' COLLATE utf8_unicode_ci ';
            }

            if ($params['sort'] == 'name') {
                $order_str .= ' CONCAT(p.firstname, " ",p.lastname) ' . $collate . $desc;
            } elseif ($params['sort'] == 'area_id') {
                $order_str .= ' a.name ' . $collate . $desc;
            } elseif ($params['sort'] == 'province') {
                $order_str .= ' r.name ' . $collate . $desc;
            } elseif (in_array($params['sort'], array('current_store', 'pending_store'))) {
                $order_str .= $params['sort'] . ' ' . $desc;
            } else {
                $order_str = 'p.`' . $params['sort'] . '` ' . $collate . $desc;
            }

            $select->order(new Zend_Db_Expr($order_str));
        }
        // echo "<pre>";print_r($select->__toString());die;

        if ($limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);

        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    /**
     * Thống kê số nhân viên hiện tại, chia theo vùng kinh doanh
     * @author phamquocbuu
     * @version 2013-11-05 13:50
     */
    function analytics() {
        $db = Zend_Registry::get('db');

        $subselect = $db->select()->from('staff')
                //            ->where('contract_expired_at > NOW()')
                ->where('off_date IS NULL');

        $select = $db->select()->from($subselect, array(
                    'qty' => 'COUNT(*)', 'regional_market'
                ))
                ->group('regional_market');

        $stmt = $db->query($select);
        $data = $stmt->fetchAll();

        return $data;
    }

    function analytics_by_area() {
        $db = Zend_Registry::get('db');

        $subselect = $db->select()->from('staff')
                //            ->where('contract_expired_at > NOW()')
                ->where('off_date IS NULL');

        $select = $db
                ->select()
                ->from(array('s' => $subselect), array('qty' => 'COUNT(*)'))
                ->joinleft(array('r' => 'regional_market'), 'r.id = s.regional_market', array())
                ->joinleft(array('a' => 'area'), 'a.id = r.area_id', array('id' => 'a.id'))
                ->group('a.id');

        $stmt = $db->query($select);
        $data = $stmt->fetchAll();

        return $data;
    }

    /**
     * Chi tiết nhân viên hiện tại theo từng vùng kinh doanh
     * @author phamquocbuu
     * @version 2013-11-06 14:40
     */
    function analytics_by_region($regional_market) {
        $db = Zend_Registry::get('db');

        $where = $db->quoteInto('regional_market LIKE ?', $regional_market);

        $subselect = $db->select()->from('staff', array('id', 'department'))
                //            ->where('contract_expired_at > NOW()')
                ->where('off_date IS NULL OR off_date = 0 OR off_date =\'\'')
                ->where($where);
        $select    = $db->select()->from($subselect, array(
                    'qty' => 'COUNT(*)', 'title'
                ))
                ->group('title');

        $stmt = $db->query($select);
        $data = $stmt->fetchAll();
        return $data;
    }

    function get_cache() {
        $cache  = Zend_Registry::get('cache');
        $result = $cache->load($this->_name . '_cache');

        if ($result === false) {

            $data = $this->fetchAll();

            $result = array();
            if ($data) {
                foreach ($data as $item) {
                    $result[$item->id] = $item->firstname . ' ' . $item->lastname;
                }
            }
            $cache->save($result, $this->_name . '_cache', array(), null);
        }
        return $result;
    }

    function get_all_cache() {
        $cache  = Zend_Registry::get('cache');
        $result = $cache->load($this->_name . '_all_cache_staff');

        if ($result === false) {

            $data = $this->fetchAll();

            $result = array();
            if ($data) {
                foreach ($data as $item) {
                    $result[$item->id] = array(
                        'name'      => $item->firstname . ' ' . $item->lastname,
                        'email'     => $item->email,
                        'code'      => $item->code,
                        'ID_number' => $item->ID_number
                    );
                }
            }
            $cache->save($result, $this->_name . '_all_cache_staff', array(), null);
        }
        return $result;
    }

    function get_all_sale_cache() {
        $cache  = Zend_Registry::get('cache');
        $result = $cache->load($this->_name . '_all_sale_cache');

        if ($result === false) {
            $where = $this->getAdapter()->quoteInto('title IN (?)', My_Staff_Title::getSalesman());
            $data  = $this->fetchAll($where);

            $result = array();
            if ($data) {
                foreach ($data as $item) {
                    $result[$item->id] = array(
                        'name'            => $item->firstname . ' ' . $item->lastname,
                        'email'           => $item->email,
                        'code'            => $item->code,
                        'regional_market' => $item->regional_market,
                        'title'           => $item->title,
                        'phone_number'    => $item->phone_number,
                    );
                }
            }
            $cache->save($result, $this->_name . '_all_sale_cache', array(), 3600 * 24);
        }
        return $result;
    }

    function get_all_sale_cache_Tuan() {
        $cache  = Zend_Registry::get('cache');
        $result = $cache->load($this->_name . '_all_sale_cache_tuan');

        if ($result === false) {
            $where = $this->getAdapter()->quoteInto('title IN (?)', My_Staff_Title::getSalesmanTuan());
            $data  = $this->fetchAll($where);

            $result = array();
            if ($data) {
                foreach ($data as $item) {
                    $result[$item->id] = array(
                        'name'            => $item->firstname . ' ' . $item->lastname,
                        'email'           => $item->email,
                        'code'            => $item->code,
                        'regional_market' => $item->regional_market,
                        'title'           => $item->title,
                        'phone_number'    => $item->phone_number,
                    );
                }
            }
            $cache->save($result, $this->_name . '_all_sale_cache_tuan', array(), 3600 * 24);
        }
        return $result;
    }

    /**
     * Lấy các cột trong bảng này
     */
    function get_cols() {
        $cols = $this->info(Zend_Db_Table_Abstract::COLS);
        return $cols;
    }

    /**
     * Function Build Query
     * Dựng câu query để select các staff theo những điều kiện nhất định
     *      để hiển thị ở các ô trong dashboard
     *
     * @param array $cols_existed   - các cột yêu cầu có sẵn giá trị
     * @param array $cols_needed    - các cột chưa có giá trị,
     *                              yêu cầu nhập giá trị mới vào
     * @return string - câu SQL lấy danh sách các staff theo yêu cầu
     *                      trường hợp dữ liệu vào rỗng, trống hoặc không có thực
     *                      thì không chọn staff nào cả
     */
    function build_query($cols_existed = array(), $cols_needed = array(), $for = 'staff') {
        // bắt buộc 2 mảng này phải có phần tử
        // không điền thì check thế quái nào
        if (count($cols_needed) == 0 || count($cols_existed) == 0 || trim($cols_needed[0]) == '' || trim($cols_existed[0]) == '') {
            return "SELECT * FROM " . $this->_name . " WHERE 0";
        }

        // danh sách các cột của table
        $cols = $this->get_cols();
        // $other = array_diff($cols, $exclude);
        // giá trị mặc định
        // chỉ check các cột này
        $default = array(
            'code'                    => 'NULL',
            'contract_type'           => '0',
            'contract_signed_at'      => 'NULL',
            'contract_term'           => '0',
            'contract_expired_at'     => 'NULL',
            'department'              => '0',
            'team'                    => '0',
            'firstname'               => 'NULL',
            'lastname'                => 'NULL',
            'title'                   => '',
            'joined_at'               => 'NULL',
            'dob'                     => '',
            'certificate'             => '',
            'level'                   => '',
            'temporary_address'       => 'NULL',
            'address'                 => '',
            'regional_market'         => '0',
            'permanent_address'       => '',
            'birth_place'             => '',
            'native_place'            => '',
            'ID_number'               => '',
            'ID_place'                => '',
            'ID_date'                 => 'NULL',
            'nationality'             => '0',
            'religion'                => '0',
            'phone_number'            => '',
            'email'                   => array('', 'NULL'),
            'group_id'                => '0',
            'social_insurance_time'   => '',
            'social_insurance_number' => '',
            'photo'                   => '',
            'off_date'                => array('', 'NULL'),
        );

        /**
         * @var bool
         * Dùng để check xem từng cụm ( ) đã được thêm phép so sánh nào chưa
         * Nếu có, thêm toán tử OR
         * ngược lại thì thôi
         */
        $flag = false;

        // Mẫu where hợp lệ: ( a OR b OR c ) AND ( d AND e AND f ) AND ( off_date IS NULL )
        $where = " ( ";

        // mớ này duyệt các điều kiện cần điền vào
        // yêu cầu điền đủ tất cả các cột mới cho qua,
        //      thiếu 1 cột cũng liệt kê staff đó ra
        // vì vậy dùng toán tử OR để check
        foreach ($cols as $name) {
            // không có trong danh sách giá trị default thì bỏ qua
            //      (ví dụ created_at hay created_by thì check làm quái gì)
            if (!isset($default[$name]))
                continue;

            if (in_array($name, $cols_needed)) {

                if (is_array($default[$name])) {

                    foreach ($default[$name] as $k => $item) {
                        if($name == 'off_date')
                            $or = $k > 1 ? 'OR' : '';
                        else
                            $or = $k > 0 ? 'OR' : '';

                        if($name == 'off_date' and $item == '') continue;
                        $where .= ' ' . $or . ' `' . $name . '`' . $this->get_type($item);
                    }
                } else {

                    if (!$flag)
                        $where .= "`" . $name . "` ";
                    else
                        $where .= " OR `" . $name . "` ";

                    $flag = true;


                    $where .= $this->get_type($default[$name]);
                }
            }
        }

        // kiểm tra biến where, phòng trường hợp các mảng trên không hợp lý,
        //      dẫn tới where chả có gì
        // đã vào tới build query thì phải xét đc 1 số cột nào đó đã có,
        //      và 1 số chưa có giá trị (nhận giá trị mặc định)
        // còn không thì không select staff nào cả
        $where .= (trim($where) != '(' ? ' ) AND (' : ' 0 ) AND (');

        /**
         * @var bool
         * Dùng để check xem từng cụm ( ) đã được thêm phép so sánh nào chưa
         * Nếu có, thêm toán tử AND
         * ngược lại thì thôi
         */
        $flag = false;

        // mớ này duyệt các điều kiện phải có sẵn
        // yêu cầu có đủ tất cả các cột
        // vì vậy dùng toán tử AND để check
        foreach ($cols as $name) {
            // đã comment ở trên
            if (!isset($default[$name]))
                continue;

            if (in_array($name, $cols_existed)) {
                if (is_array($default[$name])) {

                    foreach ($default[$name] as $k => $item) {
                        if (!$flag)
                            $where .= " `" . $name . "` ";
                        else
                            $where .= " AND `" . $name . "` ";

                        $flag = true;
                        $where .= $this->get_type($item, 1);
                    }
                } else {

                    if (!$flag)
                        $where .= " `" . $name . "` ";
                    else
                        $where .= " AND `" . $name . "` ";

                    $flag = true;
                    $where .= $this->get_type($default[$name], 1);
                }
            }
        }

        // comment như đoạn trên
        $where .= (trim($where) != '(' ? ' ) ' : ' ( 0 ) ');

        // staff nghỉ rồi thì khỏi liệt kê ra
        // $where .= " AND ( off_date IS NULL ) AND ( group_id <> ".BOARD_ID.") ";
        $where .= " AND ( off_date IS NULL ) AND ( IFNULL(group_id,0) <> " . BOARD_ID . ") ";



        // xét xem có phải cho ASM/Sales Admin xem không
        // nếu phải thì lọc staff theo area
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        if ($for == 'asm' && in_array($userStorage->group_id, $this->config['asm'])) {
            // lấy theo khu vực mà thằng đó lead
            $QArea           = new Application_Model_Area();
            $QRegionalMarket = new Application_Model_RegionalMarket();

            $user_id = $userStorage->id;

            if ($userStorage->group_id == SALES_ADMIN_ID) {
                $staff = $this->find($user_id);
                $staff = $staff->current();

                if ($staff) {
                    $region = $staff['regional_market'];
                    $region = $QRegionalMarket->find($region);
                    $region = $region->current();
                }
            }

            $where_2          = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $region['area_id']);
            $regional_markets = $QRegionalMarket->fetchAll($where_2);


            $regional_markets_arr = array();

            foreach ($regional_markets as $regional_market) {
                $regional_markets_arr[] = $regional_market->id;
            }

            $result_asm = My_Asm::get_cache();

            // echo '<pre>';
            // var_dump($result_asm[$userStorage->id]['province']);
            // exit;
            if (count($result_asm[$userStorage->id]['province'])) {
                $regional_markets_arr = array_merge($regional_markets_arr, $result_asm[$userStorage->id]['province']);
            }

            $where .= (count($regional_markets_arr) ? (" AND regional_market IN (" . implode(',', $regional_markets_arr) . ") ") : '') . " AND created_at >= '" . date_sub(date_create(), new DateInterval('P' . $this->config['ttl'] . 'D'))->format('Y-m-d H:i:s') . "' ";
        }

        $order = " ORDER BY id DESC";

        return "SELECT * FROM " . $this->_name . " WHERE " . $where . $order;
    }

    /**
     * Có câu query rồi thì chạy nó thôi
     */
    function get_by_step($cols_existed = array(), $cols_needed = array(), $for = 'staff') {
        $db  = Zend_Registry::get('db');
        $sql = $this->build_query($cols_existed, $cols_needed, $for);
        return $db->query($sql);
    }

    /**
     * Dùng để build đoạn so sánh với giá trị mặc định
     * Ví dụ:   * mặc định : 0 và negative = 0 -> trả về ' = 0 '
     *          * mặc định : 0 và negative = 1 -> trả về ' <> 0 '
     */
    function get_type($value = '', $negative = 0) {
        switch ($value) {
            case 'NULL':
                return ' IS ' . ( $negative == 1 ? ' NOT ' : '' ) . ' NULL ';
                break;
            case '0':
                return ( $negative == 1 ? ' <> ' : '=' ) . ' 0 ';
                break;
            case '':
                return ( $negative == 1 ? ' <> ' : '=' ) . ' \'\' ';
                break;

            default:
                return '';
                break;
        }
    }

    public function fetchPaginationTransfer($page, $limit, &$total, $params) {
        if (isset($params['from']) AND $params['from']) {
            $arrFrom        = explode('/', $params['from']);
            $params['from'] = $arrFrom[2] . '-' . $arrFrom[1] . '-' . $arrFrom[0];
        }

        if (isset($params['to']) AND $params['to']) {
            $arrFrom      = explode('/', $params['to']);
            $params['to'] = $arrFrom[2] . '-' . $arrFrom[1] . '-' . $arrFrom[0];
        }

        $db      = Zend_Registry::get('db');
        $arrCols = array(
            'staff_code'     => 's.code',
            'fullname'       => new Zend_Db_Expr('CONCAT(s.firstname," ",s.lastname)'),
            'transfer_time'  => 'p.from_date',
            'transfer_id'    => 'p.id',
            'info_types'     => new Zend_Db_Expr('GROUP_CONCAT(sl.info_type)'),
            'current_values' => new Zend_Db_Expr('GROUP_CONCAT(sl.current_value)'),
            'old_values'     => new Zend_Db_Expr('GROUP_CONCAT(sl.old_value)'),
        );

        $select = $db->select()
                ->from(array('p' => 'staff_transfer'), $arrCols)
                ->join(array('sl' => 'staff_log_detail'), 'p.id = sl.transfer_id', array())
                ->join(array('s' => 'staff'), 's.id = p.staff_id', array())
                ->where('p.note IS NULL')
                ->group('p.id')

        ;

        if (isset($params['transfer_id']) AND $params['transfer_id']) {
            $select->where('p.id = ?', $params['transfer_id']);
        }

        if (isset($params['from']) AND $params['from']) {
            $select->where('p.from_date >= ?', $params['from']);
        }

        if (isset($params['to']) AND $params['to']) {
            $select->where('p.from_date <= ?', $params['to']);
        }

        $order_str = '';

        if (isset($params['sort']) and $params['sort']) {
            $collate = ' ';
            $desc    = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';

            if ($params['sort'] == 'name') {
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= ' CONCAT(s.firstname, " ",s.lastname) ' . $collate . $desc;
            } else {
                $order_str = 'p.`' . $params['sort'] . '` ' . $collate . $desc;
            }

            $select->order(new Zend_Db_Expr($order_str));
        } else {
            $select->order('p.created_at DESC');
        }

        if ($limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);
        return $result;
    }

    function genStaffCode($joined_at = null) {
        try {
            $joined_at = $joined_at ? $joined_at : date('Y-m-d');
            $firstDay  = date('Y-m-01', strtotime($joined_at));

            $db = Zend_Registry::get('db');

            $select = $db->select()
                    ->from(array('p' => 'staff'), array('count(id)'))
                    /* ->where('p.joined_at <= ?', $joined_at) */
                    ->where('p.joined_at >= ?', $firstDay)
            ;

            $total = $db->fetchOne($select);

            $filledUp = str_pad($total + 1, 4, '0', STR_PAD_LEFT);
            return date('ym', strtotime($joined_at . ' 00:00:00')) . $filledUp;
        } catch (Exception $e) {
            return null;
        }
    }

    function checkIsNotHeadOffice($department_id, $team_id, $title_id) {
        $arrNotHeadOffice = unserialize(CONFIG_NOT_HEAD_OFFICE);
        if (isset($arrNotHeadOffice[$department_id]) and ! is_array($arrNotHeadOffice[$department_id]) and $arrNotHeadOffice[$department_id] == $department_id)
            return true;

        if (isset($arrNotHeadOffice[$department_id][$team_id][$title_id]))
            return true;

        return false;
    }

    /**
     * @return array
     * Trả về danh sách được config cho phép xem report trong trainer
     */
    function staffTrainerAllowReport() {
        // 241: tienhung.nguyen
        // 3028: thienphan
        // 553: thin.nguyen
        // 644: thong.duong
        // 7278: ngocduyen.le
        // 3026: luyt.van
        return array(241, 553, 644, 5899, 3028, 7278, 3026, 3028, 2857);
    }

    function offDatePurposeFetchPagination($page, $limit, &$total, $params) {
        $db = Zend_Registry::get('db');

        if ($limit) {
            $select = $db->select()
                    ->from(array('p' => $this->_name), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'), 'p.*'));
        } else {
            $select = $db->select()
                    ->from(array('p' => $this->_name), array(new Zend_Db_Expr('DISTINCT p.id'), 'p.*'));
        }

        $select->joinLeft(array('e' => 'staff_education'), 'p.id = e.staff_id AND e.default_level = 1', array('d_level' => 'e.level', 'd_certificate' => 'field_of_study'));

        $select->joinLeft(array('province'), 'province.id = id_place_province', array('id_place_province_name' => 'province.name'));

        $select->where('(p.date_off_purpose is not null OR st.date_off_purpose is not null)');

        if (isset($params['off_date_from']) and $params['off_date_from']) {
            $arrFrom                 = explode('/', $params['off_date_from']);
            $params['off_date_from'] = $arrFrom[2] . '-' . $arrFrom[1] . '-' . $arrFrom[0];
            $select->where('(p.off_date >="' . $params['off_date_from'] . '" OR st.date_off_purpose >="' . $params['off_date_from'] . '")');
        }

        if (isset($params['off_date_to']) and $params['off_date_to']) {
            $arrFrom               = explode('/', $params['off_date_to']);
            $params['off_date_to'] = $arrFrom[2] . '-' . $arrFrom[1] . '-' . $arrFrom[0];
            $select->where('(p.off_date <="' . $params['off_date_to'] . '" OR st.date_off_purpose <="' . $params['off_date_to'] . '")');
        }

        if (isset($params['name']) and $params['name']) {
            $select->where('CONCAT(p.firstname, " ",p.lastname) LIKE ?', '%' . $params['name'] . '%');
            //            $select->orwhere('p.email LIKE ?', '%'.$params['name'].'%');
        }

        if (isset($params['company_id']) and $params['company_id']) {
            $select->where('p.company_id = ?', $params['company_id']);
        }

        if (isset($params['team']) and $params['team']) {
            if (is_array($params['team']) && count($params['team']) > 0) {
                $select->where('p.team IN (?)', $params['team']);
            } else {
                $select->where('1=0', 1);
            }
        }


        if (isset($params['contract_term']) and $params['contract_term']) {
            if (is_array($params['contract_term']) && count($params['contract_term']) > 0) {
                $select->where('p.contract_term IN (?)', $params['contract_term']);
            } else {
                $select->where('1=0', 1);
            }
        }

        if (isset($params['department']) and $params['department']) {
            if (is_array($params['department']) && count($params['department']) > 0) {
                $select->where('p.department IN (?)', $params['department']);
            } else {
                $select->where('1=0', 1);
            }
        }

        if (isset($params['salary_sales']) and $params['salary_sales']) {
            $select->where('p.title IN (?)', array(PGPB_TITLE, PGPB_2_TITLE, SALES_TITLE, SALES_ACCESSORIES_TITLE, SALES_LEADER_TITLE, SALES_ACCESSORIES_LEADER_TITLE));
        }

        if (isset($params['is_officer']) and intval($params['is_officer']) > 0) {
            if ($params['is_officer'] == 1)
                $select->where('p.is_officer = ? or p.team = 11 or p.title in (274,179,181)', 1);
            elseif ($params['is_officer'] == 2)
                $select->where('p.is_officer ? or p.team = 11 or p.title in (274,179,181)', 0);
            //$select->orWhere('p.team = ?', WARRANTY_CENTER);
        }

        if (isset($params['email']) and $params['email']) {
            $params['email'] = preg_replace('/' . EMAIL_SUFFIX . '/', '', $params['email']);
            $select->where('p.email LIKE ?', $params['email'] . EMAIL_SUFFIX);
        }

        if (isset($params['date']) and $params['date']) {
            $date = explode(',', $params['date']);
            $select->where("DAY(STR_TO_DATE(p.dob, '%d/%m/%Y')) IN (?)", $date);
        }

        if (isset($params['month']) and $params['month']) {
            $date = explode(',', $params['month']);
            $select->where("MONTH(STR_TO_DATE(p.dob, '%d/%m/%Y')) IN (?)", $date);
        }

        if (isset($params['month_off']) and $params['month_off']) {
            $select
                    ->joinLeft(array('mo' => 'off_date_add'), 'p.id = mo.staff_id ', array('staff_id' => 'mo.staff_id'));
            $select->where('MONTH(mo.date) <> ?', $params['month_off'])->orWhere('MONTH(mo.date) is null', null);
        }

        if (isset($params['year']) and $params['year']) {
            $date = explode(',', $params['year']);
            $select->where("YEAR(STR_TO_DATE(p.dob, '%d/%m/%Y')) IN (?)", $date);
        }

        if (isset($params['ready_print']) and $params['ready_print']) {

            if ($params['ready_print'] == 1) {
                $select->where("p.address <> '' and p.birth_place <> ''  and p.ID_number <> '' and p.ID_place <> '' and p.ID_date <> '' and p.title <> '' and p.company_id <> 0");
            } elseif ($params['ready_print'] == 2) {
                $select->where("address = '' or birth_place = ''  or ID_number = '' or ID_place = '' or ID_date = '' or title = '' or company_id = 0");
            } elseif ($params['ready_print'] == 3) {
                $select->where("p.ID_number <> '' and p.ID_place <> '' and p.ID_date <> '' and p.title <> '' and p.company_id <> 0 and EXISTS (select * from staff_address sa2 where p.id = sa2.staff_id and sa2.address_type = 4 and sa2.address is not null)");
            } elseif ($params['ready_print'] == 4) {
                $select->where("p.ID_number = '' or p.ID_place = '' or p.ID_date = '' or p.title = '' or p.company_id = 0 or  and EXISTS (select * from staff_address sa2 where p.id = sa2.staff_id and sa2.address_type = 4 and sa2.address is not null)");
            }
        }

        if (isset($params['title']) and $params['title']) {
            if (is_array($params['title']) && count($params['title']) > 0) {
                $select->where('p.title IN (?)', $params['title']);
            } else {
                $select->where('1=0', 1);
            }
        }

        $select->joinLeft(array('st' => 'staff_temp'), '
                p.id = st.staff_id
            ', array(
            'staff_temp_id'                      => 'st.id',
            'staff_temp_is_approved'             => 'st.is_approved',
            'staff_temp_date_off_purpose'        => 'st.date_off_purpose',
            'staff_temp_date_off_purpose_reason' => 'st.date_off_purpose_reason',
            'staff_temp_date_off_purpose_detail' => 'st.date_off_purpose_detail',
            'staff_temp_off_type'                => 'st.off_type',
            'staff_temp_created_at'              => 'st.created_at',
            'staff_temp_created_by'              => 'st.created_by',
            'order_offdate'                      =>  new Zend_Db_Expr("IF(p.off_date IS NULL,`st`.`date_off_purpose`,p.off_date)")
        ));

        if (isset($params['need_approve']) and $params['need_approve']) {
            $select->where('st.is_approved = ?', 0);
        }


        if (isset($params['is_approved']) and $params['is_approved'] != '') {
            if ($params['is_approved'] == 1)
                $select->where('st.is_approved IS NULL');
            elseif ($params['is_approved'] == 2)
                $select->where('st.is_approved = ?', 0);
            else
                $select->where('st.is_approved = ?', $params['is_approved']);
        }


        // list
        if (isset($params['exp']) and $params['exp'])
            $select->where('p.contract_expired_at > NOW()');

        //joined at
        if (isset($params['joined_at']) and $params['joined_at']) {
            $arrFrom             = explode('/', $params['joined_at']);
            $params['joined_at'] = $arrFrom[2] . '-' . $arrFrom[1] . '-' . $arrFrom[0];
            $select->where('p.joined_at = ?', $params['joined_at']);
        }
        // id_number
        if (isset($params['indentity']) and $params['indentity']) {
            $select->where('p.ID_number = ?', $params['indentity']);
        }

        if (isset($params['off_type']) and $params['off_type']) {
            $select->where('(p.off_type = "' . $params['off_type'] . '" OR st.off_type = "' . $params['off_type'] . '")');
        }

        //off_date
        if (isset($params['off_date']) and $params['off_date']) {
            $arrFrom            = explode('/', $params['off_date']);
            $params['off_date'] = $arrFrom[2] . '-' . $arrFrom[1] . '-' . $arrFrom[0];
            $select->where('p.off_date = ?', $params['off_date']);
        }

        if (isset($params['sales_team_id']) and $params['sales_team_id'])
            $select->where('p.sales_team_id = ?', $params['sales_team_id']);

        if (isset($params['is_leader']) and $params['is_leader'])
            $select->where('p.is_leader = ?', $params['is_leader']);

        if (isset($params['distribution_id']) and $params['distribution_id'])
            $select->where('p.distribution_id = ?', $params['distribution_id']);

        if (isset($params['note']) and $params['note'])
            $select->where('p.note LIKE ?', '%' . $params['note'] . '%');

        if (isset($params['code']) and $params['code'])
            $select->where('p.code LIKE ?', '%' . $params['code'] . '%');

        if (isset($params['has_photo']) and $params['has_photo'])
            $select->where('p.photo IS NOT NULL AND p.photo <> \'\'', 1);

        if (isset($params['ood']) and $params['ood']) {
            $select->where(' DATEDIFF(p.contract_expired_at,NOW()) <= ?', 30);
            /* $select->where(' DATEDIFF(NOW(),p.contract_expired_at) >= ?', 0); */
        }

        if (isset($params['no_print']) and $params['no_print']) {
            $select->where('p.print_time = 0 OR p.contract_signed_at is NULL and p.contract_expired_at is NULL', '');
        }

        if (isset($params['tags']) and $params['tags']) {
            $select->join(array('ta_ob' => 'tag_object'), '
                    p.id = ta_ob.object_id
                    AND ta_ob.type = ' . TAG_STAFF . '
                ', array());
            $select->join(array('ta' => 'tag'), '
                    ta.id = ta_ob.tag_id
                ', array());

            $select->where('ta.name IN (?)', $params['tags']);
        }

        if (isset($params['area_id']) and $params['area_id'] && !(isset($params['regional_market']) and $params['regional_market'])) {
            $QRegionalMarket = new Application_Model_RegionalMarket();

            if (is_array($params['area_id']) && count($params['area_id']) > 0) {
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id IN (?)', $params['area_id']);
            } else {
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $params['area_id']);
            }

            $regional_markets = $QRegionalMarket->fetchAll($where);
            $tem              = array();

            foreach ($regional_markets as $regional_market)
                $tem[] = $regional_market->id;

            if (is_array($tem) && count($tem) > 0)
                $select->where('p.regional_market IN (?)', $tem);
            else
                $select->where('1=0', 1);
        } elseif (isset($params['regional_market']) and $params['regional_market']) {
            if (is_array($params['regional_market']) && count($params['regional_market']) > 0) {
                $select->where('p.regional_market IN (?)', $params['regional_market']);
            } else {
                $select->where('1=0', 1);
            }
        }

        // dành để lọc staff cho các asm, admin... xem; chỉ hiện thị staff thuộc khu vực được gán
        if (isset($params['list_province_ids']) && $params['list_province_ids']) {
            if (is_array($params['list_province_ids']) && count($params['list_province_ids'])) {
                $select->where('p.regional_market IN (?)', $params['list_province_ids']);
            } elseif (is_numeric($params['list_province_ids']) && intval($params['list_province_ids']) > 0) {
                $select->where('p.regional_market = ?', intval($params['list_province_ids']));
            }
        }

        if (isset($params['area_trainer_right']) and $params['area_trainer_right']) {
            $QRegionalMarket = new Application_Model_RegionalMarket();

            if (is_array($params['area_trainer_right']) && count($params['area_trainer_right']) > 0) {
                $whereRegionalMarketRight = $QRegionalMarket->getAdapter()->quoteInto('area_id IN (?)', $params['area_trainer_right']);
            } else {
                $whereRegionalMarketRight = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $params['area_trainer_right']);
            }

            $regional_markets_rights = $QRegionalMarket->fetchAll($whereRegionalMarketRight);
            $tem                     = array();

            foreach ($regional_markets_rights as $regional_markets_right)
                $tem[] = $regional_markets_right->id;

            if (is_array($tem) && count($tem) > 0)
                $select->where('p.regional_market IN (?)', $tem);
            else
                $select->where('1=0', 1);
        }

        if (isset($params['s_assign']) && $params['s_assign']) {
            $QDeparment    = new Application_Model_Department();
            $where         = $QDeparment->getAdapter()->quoteInto('name LIKE ?', 'KINH DOANH');
            $department_id = $QDeparment->fetchAll($where)->current()->id;
            $select->where('p.department = ?', $department_id);
        }

        if (isset($params['date_off_purpose_update_by'])) {
            $select->where('(p.date_off_purpose_update_by = ' . $params['date_off_purpose_update_by'] . ' OR st.created_by = ' . $params['date_off_purpose_update_by'] . ' OR st.updated_by = ' . $params['date_off_purpose_update_by'] . ')');
        }

        $order_str = '';
        if (isset($params['sort']) and $params['sort']) {
            $collate = ' ';
            $desc    = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';

            if ($params['sort'] == 'name') {
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= ' CONCAT(p.firstname, " ",p.lastname) ' . $collate . $desc;
            } elseif ($params['sort'] == 'offdate') {
                $order_str .= ' order_offdate ' . $collate . $desc;
            } elseif (/* $params['sort'] == 'title' || */$params['sort'] == 'department' || $params['sort'] == 'team') {
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= $collate . $desc;
            } elseif ($params['sort'] == 'date_off_purpose') {
                $order_str .= 'st.id DESC, p.date_off_purpose_update_at DESC';
            }else {
                $order_str = 'p.`' . $params['sort'] . '` ' . $collate . $desc;
            }
            $select->order(new Zend_Db_Expr($order_str));
        }

        $select->group('p.id');
        
        if ($limit)
            $select->limitPage($page, $limit);

        if (isset($params['export']) && $params['export'])
            return $select->__toString();

        $result = $db->fetchAll($select);

        // echo $select->__toString();
        //echo $select->__toString();exit();
        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public function getStaffByStore($params) {
        $db       = Zend_Registry::get('db');
        $store_id = $params['store_id'];
        if (!$store_id) {
            throw new Exception('please select store');
        }

        $cols        = array(
            'staff_id'   => 'c.id',
            'staff_name' => 'CONCAT(c.firstname," ",c.lastname)',
            'title'      => 'c.title',
        );
        $select      = $db->select()
                ->from(array('a' => 'store'), $cols)
                ->join(array('b' => 'store_staff_log'), 'a.id = b.store_id', array())
                ->join(array('c' => 'staff'), 'c.id = b.staff_id', array())
                ->where('c.off_date IS NULL', 0)
                ->where('b.released_at IS NULL', 0)
                ->where('a.id = ?', $store_id);
        $store_staff = $db->fetchAll($select);

        $select_leader = $db->select()
                ->from(array('a' => 'store'), $cols)
                ->join(array('b' => 'store_leader'), 'a.id = b.store_id', array())
                ->join(array('c' => 'staff'), 'c.id = b.staff_id', array())
                ->where('c.off_date IS NULL', 0)
                ->where('a.id = ?', $store_id);
        $leader        = $db->fetchRow($select_leader);

        $result = array();

        if (count($store_staff)) {
            foreach ($store_staff as $item) {
                if (in_array($item['title'], My_Staff_Title::getPg())) {
                    $result['pg'][] = $item['staff_name'];
                } elseif (in_array($item['title'], My_Staff_Title::getSale())) {
                    $result['sale'][] = $item['staff_name'];
                } elseif (in_array($item['title'], My_Staff_Title::getStoreLeader())) {
                    $result['store_leader'][] = $item['staff_name'];
                }
            }
        }

        if ($leader) {
            $result['leader'][] = $leader['staff_name'];
        }

        return $result;
    }

    function getScoresCourse($staff_id) {
        $db     = Zend_Registry::get('db');
        $select = $db->select();
        $select = "SELECT staff_id, course_name, from_date, to_date, Result
                    FROM
                    (
                    	SELECT c.id AS staff_id, d.name course_name, d.from_date, d.to_date, CASE WHEN b.result = 1 THEN 'Ðậu' ELSE 'Rớt' END Result
                    	FROM `staff_training` a
                    	INNER JOIN `trainer_course_detail` b ON a.id = b.`new_staff_id` AND (b.result IS NOT NULL AND b.result > 0)
                    	INNER JOIN staff c ON a.cmnd = c.id_number 
                    	INNER JOIN trainer_course d ON b.course_id = d.id
                    	UNION ALL
                    	SELECT staff_id, b.name course_name, b.from_date, b.to_date, CASE WHEN a.result = 1 THEN 'Ðậu' ELSE 'Rớt' END Result
                    	FROM trainer_course_detail a
                    	INNER JOIN trainer_course b ON a.course_id = b.id
                    	WHERE staff_id IS NOT NULL AND staff_id > 0 AND (result IS NOT NULL AND result > 0)
                    ) course
                    
                    WHERE staff_id = " . $staff_id;

        $result = $db->fetchAll($select);

        return $result;
    }

    function getScoresWarning($staff_id) {
        $db     = Zend_Registry::get('db');
        $select = $db->select();

        $select = "SELECT *
                FROM `staff_reward_warning` 
                WHERE `type` = 2 AND staff_id = " . $staff_id;

        $result = $db->fetchAll($select);

        return $result;
    }

    function getAttendanceDetail($staff_id) {
        $db     = Zend_Registry::get('db');
        $select = $db->select();

        // $select = '
        //     SELECT NULL yearno, NULL monthno, WDate total_attendance, 0 total_ot
        //     FROM staff_miss_time
        //     WHERE staff_id = '.$staff_id.'
        //     UNION
        //     SELECT
        //         wo.`yearno`,
        //         wo.`monthno`,
        //         SUM(
        //             CASE
        //             WHEN ti.total_attendance > wo.`workingdate` THEN
        //                 wo.`workingdate`
        //             ELSE
        //                 ti.total_attendance
        //             END
        //         ) AS total_attendance,
        //         (
        //             SUM(
        //                 CASE
        //                 WHEN (wo.`yearno` < 2016 OR (wo.`yearno` = 2016 AND wo.`monthno` < 10)) then 0
        //                 WHEN  ti.total_attendance > wo.`workingdate` THEN
        //                     ti.total_attendance - wo.`workingdate`
        //                 ELSE
        //                     0
        //                 END
        //             ) * 3
        //         ) AS total_ot
        //     FROM
        //         (
        //             SELECT
        //                 staff_id,
        //                 YEAR (created_at) yearno,
        //                 MONTH (created_at) monthno,
        //                 COUNT(id) total_attendance
        //             FROM
        //                 (
        //                     SELECT
        //                         staff_id,
        //                         DATE(created_at) created_at,
        //                         MAX(id) id
        //                     FROM
        //                         `time`
        //                     WHERE
        //                         staff_id = '.$staff_id.' and off = 0 AND NOT (DAYOFWEEK(`created_at`) = 1 AND `created_at` < "2016-10-01 00:00")
        //                     GROUP BY
        //                         staff_id,
        //                         DATE(created_at)
        //                 ) tmp
        //             GROUP BY
        //                 staff_id,
        //                 YEAR (created_at),
        //                 MONTH (created_at)
        //         ) ti
        //     INNER JOIN workingday wo ON ti.yearno = wo.`yearno`
        //     AND ti.monthno = wo.`monthno`
        //     GROUP BY
        //         ti.staff_id,
        //         wo.`yearno`,
        //         wo.`monthno`
        // ';

        $select = '
            SELECT NULL yearno, NULL monthno, WDate total_attendance, 0 total_ot
            FROM staff_miss_time
            WHERE staff_id = ' . $staff_id . '
            UNION
            SELECT
                wo.`yearno`,
                wo.`monthno`,
                SUM(
                    CASE
                    WHEN ti.total_attendance > wo.`workingdate` THEN
                        wo.`workingdate`
                    ELSE
                        ti.total_attendance
                    END
                ) AS total_attendance,
                (
                    SUM(
                        CASE
                        WHEN  ti.total_attendance > wo.`workingdate` THEN
                            ti.total_attendance - wo.`workingdate`
                        ELSE
                            0
                        END
                    ) * 3
                ) AS total_ot
            FROM
                (
                    SELECT
                        staff_id,
                        YEAR (created_at) yearno,
                        MONTH (created_at) monthno,
                        COUNT(id) total_attendance
                    FROM
                        (
                            SELECT
                                staff_id,
                                DATE(created_at) created_at,
                                MAX(id) id
                            FROM
                                `time`
                            WHERE
                                staff_id = ' . $staff_id . ' and off = 0 AND `created_at` >= "2016-10-01 00:00"
                            GROUP BY
                                staff_id,
                                DATE(created_at)
                        ) tmp
                    GROUP BY
                        staff_id,
                        YEAR (created_at),
                        MONTH (created_at)
                ) ti
            INNER JOIN workingday wo ON ti.yearno = wo.`yearno`
            AND ti.monthno = wo.`monthno`
            GROUP BY
                ti.staff_id,
                wo.`yearno`,
                wo.`monthno`
        ';

        $result = $db->fetchAll($select);

        return $result;
    }

    function getKPIdetail($staff_id) {
        $db     = Zend_Registry::get('db');
        $select = $db->select();

        $select = "SELECT b. NAME good, c. NAME color, a.price, a.quantity, a.kpi_score
                    FROM (
                    		SELECT 	good_id, 	color_id, MAX(`value`) price, 	COUNT(imei_sn) quantity,
                    			SUM(
                    				CASE
                    				WHEN `value` BETWEEN 1 	AND 3999999 THEN 1
                    				WHEN `value` BETWEEN 4000000 AND 8000000 THEN 	2 ELSE 	3 END
                    			) kpi_score
                    		FROM  imei_kpi
                    		WHERE 	pg_id = " . $staff_id . " AND timing_date >= '2013-11-01 00:00'
                    		GROUP BY 	good_id, 	color_id
                    	) a
                    INNER JOIN warehouse.good b ON a.good_id = b.id
                    LEFT JOIN warehouse.good_color c ON a.color_id = c.id";

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getStaffBi($params) {

        $title = unserialize(LIST_STAFF_BI);

        $db     = Zend_Registry::get('db');
        //1
        $select = $db->select();
        $select->from(array('p' => $this->_name), array('num' => 'count(p.id)', 'p.title', 'staff_id' => 'p.id')
        );
        $select->joinLeft(array('t' => 'team'), 'p.title = t.id', array('name' => 't.name'));
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());
        $select->where('p.title IN (?)', $title);
        $select->where('off_date IS NULL', null);

        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        if (isset($params['province']) and $params['province']) {
            $select->where('r.id IN (?)', $params['province']);
        }

        $select->group('p.title');
        $select->order('field(p.title,' . LIST_STAFF_BI_ . ')');
        $data = $db->fetchAll($select);
        //
        //2
        $select2 = $db->select();
        $select2->from(array('p' => $this->_name), array('num' => 'count(p.id)')
        );
        $select2->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());
        $select2->where('off_date IS NULL', null);

        if (isset($params['area_list']) and $params['area_list']) {
            $select2->where('r.area_id IN (?)', $params['area_list']);
        }

        if (isset($params['province']) and $params['province']) {
            $select2->where('r.id IN (?)', $params['province']);
        }

        $data2 = $db->fetchRow($select2);
        //

        $result = array();
        foreach ($data as $key => $value) {
            $result[$key] = array(
                'title'    => $value['title'],
                'name'     => $value['name'],
                'num'      => $value['num'],
                'total'    => $data2['num'],
                'staff_id' => $value['staff_id']
            );
        }
        return $result;
    }

    public function getStaffGropTitle($params) {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'staff_id'   => 'p.id',
            'p.title',
            'p.team',
            'title_name' => 't.name',
            'team_name'  => 't2.name',
            'total'      => 'COUNT(p.id)'
        );

        $select->from(array('p' => 'staff'), $arrCols);
        $select->joinLeft(array('t' => 'team'), 't.id = p.title', array());
        $select->joinLeft(array('t2' => 'team'), 't2.id = p.team', array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());
        $select->where('p.off_date IS NULL', NULL);

        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        if (isset($params['province']) and $params['province']) {
            $select->where('r.id IN (?)', $params['province']);
        }

        $select->group('p.title');
        $select->order('p.team');

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getStaffBiOff($params) {
        $db    = Zend_Registry::get('db');
        $cache = Zend_Registry::get('cache');

        //1
        $select = $db->select();
        $select->from(array('p' => $this->_name), array('num' => 'count(p.id)', 'p.title', 'month_off_date' => 'MONTH(off_date)', 'year_off_date' => 'YEAR(off_date)')
                )
                ->where('p.title IN (183,182)', null)
                ->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());

        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        $select->group(array('p.title', 'MONTH(off_date)', 'YEAR(off_date)'));
        $data = $db->fetchAll($select);
        //
        //2
        $select2 = $db->select();
        $select2->from(array('p' => $this->_name), array('num' => 'count(p.id)')
                )
                ->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());

        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        $select2->where('off_date IS NULL', null);
        $data2 = $db->fetchRow($select2);
        //

        $result = array();
        foreach ($data as $key => $value) {
            $result[$value['title']][$value['month_off_date']][$value['year_off_date']] = $value['num'];
        }

        return $result;
    }

    public function getStaffBiJoined($params) {
        $db = Zend_Registry::get('db');

        //1
        $select = $db->select();
        $select->from(array('p' => $this->_name), array('num' => 'count(p.id)', 'p.title', 'month_joined_date' => 'MONTH(joined_at)', 'year_joined_date' => 'YEAR(joined_at)')
                )
                ->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());

        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        $select->where('p.title IN (183,182)', null);
        $select->group(array('p.title', 'MONTH(joined_at)', 'YEAR(joined_at)'));
        $data = $db->fetchAll($select);
        //
        //2
        $select2 = $db->select();
        $select2->from(array('p' => $this->_name), array('num' => 'count(p.id)')
                )
                ->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());

        if (isset($params['area_list']) and $params['area_list']) {
            $select2->where('r.area_id IN (?)', $params['area_list']);
        }

        $select2->where('joined_at IS NULL', null);
        $data2 = $db->fetchRow($select2);
        //

        $result = array();
        foreach ($data as $key => $value) {
            $result[$value['title']][$value['month_joined_date']][$value['year_joined_date']] = $value['num'];
        }

        return $result;
    }

    public function updatePhoto($params = array()) {
        if (isset($params['new_name']) && isset($params['staff_id'])) {
            $db = Zend_Registry::get('db');

            $sql_update_new_photo = "update staff 
                set photo = '" . $params['new_name'] . "'
                where id = " . $params['staff_id'] . "
                limit 1";
            $stmt                 = $db->prepare($sql_update_new_photo);
            $res                  = $stmt->execute();
            if ($res == true && !empty($params['old_name'])) {
                $sql_insert_old_photo = "insert into photo_back 
                (staff_id, photo) values 
                (" . $params['staff_id'] . " , '" . $params['old_name'] . "')";
                $db->query($sql_insert_old_photo);
            }

            $stmt->closeCursor();
            $db   = $stmt = null;
            return $res;
        }
    }

    public function updateIdPhoto($params = array()) {
        if (isset($params['new_name']) && isset($params['staff_id'])) {
            $db = Zend_Registry::get('db');

            $sql_update_new_photo = "update staff 
                set id_photo = '" . $params['new_name'] . "'
                where id = " . $params['staff_id'] . "
                limit 1";
            $stmt                 = $db->prepare($sql_update_new_photo);
            $res                  = $stmt->execute();
            if ($res == true && !empty($params['old_name'])) {
                $sql_insert_old_photo = "insert into photo_back 
                (staff_id, id_photo) values 
                (" . $params['staff_id'] . " , '" . $params['old_name'] . "')";
                $db->query($sql_insert_old_photo);
            }

            $stmt->closeCursor();
            $db   = $stmt = null;
            return $res;
        }
    }

    public function updateIdPhotoBack($params = array()) {
        if (isset($params['new_name']) && isset($params['staff_id'])) {
            $db = Zend_Registry::get('db');

            $sql_update_new_photo = "update staff 
                set id_photo_back = '" . $params['new_name'] . "'
                where id = " . $params['staff_id'] . "
                limit 1";
            $stmt                 = $db->prepare($sql_update_new_photo);
            $res                  = $stmt->execute();
            if ($res == true && !empty($params['old_name'])) {
                $sql_insert_old_photo = "insert into photo_back 
                (staff_id, id_photo_back) values 
                (" . $params['staff_id'] . " , '" . $params['old_name'] . "')";
                $db->query($sql_insert_old_photo);
            }

            $stmt->closeCursor();
            $db   = $stmt = null;
            return $res;
        }
    }

    function fetchPaginationPhoto($page, $limit, &$total, $params) {
        $db = Zend_Registry::get('db');

        if ($limit) {
            $select = $db->select()
                    ->from(array('p' => $this->_name), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'), 'p.*'));
        } else {
            $select = $db->select()
                    ->from(array('p' => $this->_name), array(new Zend_Db_Expr('DISTINCT p.id'), 'p.*'));
        }

        $select->joinLeft(array('e' => 'staff_education'), 'p.id = e.staff_id AND e.default_level = 1', array('d_level' => 'e.level', 'd_certificate' => 'field_of_study'));

        $select->joinLeft(array('province'), 'province.id = id_place_province', array('id_place_province_name' => 'province.name'));

        $select->joinLeft(array('rm' => 'regional_market'), 'p.regional_market=rm.id', array());

        $select->joinLeft(array('ar' => 'area'), 'ar.id=rm.area_id', array('area_name' => 'ar.name'));

        $select->joinLeft(array('spt' => 'sp_assessment_temp'), 'p.id=spt.staff_id', array('result_star' => 'spt.result', 'staff_title' => 'spt.title'));

        if (isset($params['empty_id_photo']) and $params['empty_id_photo']) {
            if ($params['empty_id_photo'] == 1) {
                $select->where('p.id_photo IS NULL OR p.id_photo = "" ');
            }

            if ($params['empty_id_photo'] == 2) {
                $select->where('p.id_photo IS NOT NULL');
            }
        }

        if (isset($params['empty_id_photo_back']) and $params['empty_id_photo_back']) {
            if ($params['empty_id_photo_back'] == 1) {
                $select->where('p.id_photo_back IS NULL OR p.id_photo = "" ');
            }

            if ($params['empty_id_photo_back'] == 2) {
                $select->where('p.id_photo_back IS NOT NULL');
            }
        }
        if (isset($params['empty_photo']) and $params['empty_photo']) {
            if ($params['empty_photo'] == 1) {
                $select->where('p.photo IS NULL OR p.photo = "" ');
            }

            if ($params['empty_photo'] == 2) {
                $select->where('p.photo IS NOT NULL');
            }
        }
        if (isset($params['is_print']) and $params['is_print']) {
            $select->where('p.is_print = ?', 1);
        }

        if (isset($params['is_not_print']) and $params['is_not_print']) {
            $select->where('p.is_print = ?', 0);
        }

        if (isset($params['status']) and $params['status'] != '') {
            $select->where('p.status = ?', $params['status']);
        }

        if (isset($params['is_official']) and $params['is_official']) {
            $select->where('p.contract_term in (1, 6, 7)');
        }

        if (isset($params['only_have_photo']) and $params['only_have_photo']) {
            $select->where('p.photo is not null');
        }

        if (isset($params['name']) and $params['name']) {
            $select->where('( CONCAT(p.firstname, " ",p.lastname) LIKE ?', '%' . $params['name'] . '%');
            $select->orwhere('p.code LIKE ? ', '%' . $params['name'] . '%');
            $select->orwhere('p.email LIKE ? )', '%' . $params['name'] . '%');
        }

        if (isset($params['company_id']) and $params['company_id']) {
            $select->where('p.company_id = ?', $params['company_id']);
        }

        if (isset($params['team']) and $params['team']) {
            if (is_array($params['team']) && count($params['team']) > 0) {
                $select->where('p.team IN (?)', $params['team']);
            } else {
                $select->where('1=0', 1);
            }
        }


        if (isset($params['contract_term']) and $params['contract_term']) {
            if (is_array($params['contract_term']) && count($params['contract_term']) > 0) {
                $select->where('p.contract_term IN (?)', $params['contract_term']);
            } else {
                $select->where('1=0', 1);
            }
        }

        if (isset($params['department']) and $params['department']) {
            if (is_array($params['department']) && count($params['department']) > 0) {
                $select->where('p.department IN (?)', $params['department']);
            } else {
                $select->where('1=0', 1);
            }
        }

        if (isset($params['salary_sales']) and $params['salary_sales']) {
            $select->where('p.title IN (?)', array(PGPB_TITLE, PGPB_2_TITLE, SALES_TITLE, SALES_ACCESSORIES_TITLE, SALES_LEADER_TITLE, SALES_ACCESSORIES_LEADER_TITLE));
        }

        if (isset($params['is_officer']) and intval($params['is_officer']) > 0) {
            if ($params['is_officer'] == 1)
                $select->where('p.is_officer = ? or p.team = 11 or p.title in (274,179,181)', 1);
            elseif ($params['is_officer'] == 2)
                $select->where('p.is_officer ? or p.team = 11 or p.title in (274,179,181)', 0);
            //$select->orWhere('p.team = ?', WARRANTY_CENTER);
        }

        if (isset($params['email']) and $params['email']) {
            $params['email'] = preg_replace('/' . EMAIL_SUFFIX . '/', '', $params['email']);
            $select->where('p.email LIKE ?', $params['email'] . EMAIL_SUFFIX);
        }

        if (isset($params['date']) and $params['date']) {
            $date = explode(',', $params['date']);
            $select->where("DAY(STR_TO_DATE(p.dob, '%d/%m/%Y')) IN (?)", $date);
        }

        if (isset($params['month']) and $params['month']) {
            $date = explode(',', $params['month']);
            $select->where("MONTH(STR_TO_DATE(p.dob, '%d/%m/%Y')) IN (?)", $date);
        }

        if (isset($params['month_off']) and $params['month_off']) {
            $select
                    ->joinLeft(array('mo' => 'off_date_add'), 'p.id = mo.staff_id ', array('staff_id' => 'mo.staff_id'));
            $select->where('MONTH(mo.date) <> ?', $params['month_off'])->orWhere('MONTH(mo.date) is null', null);
        }

        if (isset($params['year']) and $params['year']) {
            $date = explode(',', $params['year']);
            $select->where("YEAR(STR_TO_DATE(p.dob, '%d/%m/%Y')) IN (?)", $date);
        }

        if (isset($params['ready_print']) and $params['ready_print']) {

            if ($params['ready_print'] == 1) {
                $select->where("p.address <> '' and p.birth_place <> ''  and p.ID_number <> '' and p.ID_place <> '' and p.ID_date <> '' and p.title <> '' and p.company_id <> 0");
            } elseif ($params['ready_print'] == 2) {
                $select->where("address = '' or birth_place = ''  or ID_number = '' or ID_place = '' or ID_date = '' or title = '' or company_id = 0");
            } elseif ($params['ready_print'] == 3) {
                $select->where("p.ID_number <> '' and p.ID_place <> '' and p.ID_date <> '' and p.title <> '' and p.company_id <> 0 and EXISTS (select * from staff_address sa2 where p.id = sa2.staff_id and sa2.address_type = 4 and sa2.address is not null)");
            } elseif ($params['ready_print'] == 4) {
                $select->where("p.ID_number = '' or p.ID_place = '' or p.ID_date = '' or p.title = '' or p.company_id = 0 or  and EXISTS (select * from staff_address sa2 where p.id = sa2.staff_id and sa2.address_type = 4 and sa2.address is not null)");
            }
        }

        if (isset($params['title']) and $params['title']) {
            if (is_array($params['title']) && count($params['title']) > 0) {
                $select->where('p.title IN (?)', $params['title']);
            } else {
                $select->where('1=0', 1);
            }
        }

        $select->joinLeft(array('st' => 'staff_temp'), '
                p.id = st.staff_id
            ', array('staff_temp_id' => 'st.id', 'staff_temp_is_approved' => 'st.is_approved'));

        if (isset($params['need_approve']) and $params['need_approve']) {
            $select->where('st.is_approved = ?', 0);
        }

        if (( isset($params['off']) and intval($params['off']) > 0 ) || ( isset($params['sname']) and $params['sname'] == 1 ))
            $select->where('p.off_date is '
                    . ( $params['off'] == 2 ? 'not' : '')
                    . ' null');

        // list
        if (isset($params['exp']) and $params['exp'])
            $select->where('p.contract_expired_at > NOW()');

        //joined at
        if (isset($params['joined_at']) and $params['joined_at']) {
            $arrFrom             = explode('/', $params['joined_at']);
            $params['joined_at'] = $arrFrom[2] . '-' . $arrFrom[1] . '-' . $arrFrom[0];
            $select->where('p.joined_at = ?', $params['joined_at']);
        }
        // id_number
        if (isset($params['indentity']) and $params['indentity']) {
            $select->where('p.ID_number = ?', $params['indentity']);
        }

        if (isset($params['off_type']) and $params['off_type']) {
            $select->where('p.off_type = ?', $params['off_type']);
        }

        //off_date
        if (isset($params['off_date']) and $params['off_date']) {
            $arrFrom            = explode('/', $params['off_date']);
            $params['off_date'] = $arrFrom[2] . '-' . $arrFrom[1] . '-' . $arrFrom[0];
            $select->where('p.off_date = ?', $params['off_date']);
        }

        if (isset($params['sales_team_id']) and $params['sales_team_id'])
            $select->where('p.sales_team_id = ?', $params['sales_team_id']);

        if (isset($params['is_leader']) and $params['is_leader'])
            $select->where('p.is_leader = ?', $params['is_leader']);

        if (isset($params['distribution_id']) and $params['distribution_id'])
            $select->where('p.distribution_id = ?', $params['distribution_id']);

        if (isset($params['note']) and $params['note'])
            $select->where('p.note LIKE ?', '%' . $params['note'] . '%');

        if (isset($params['code']) and $params['code'])
            $select->where('p.code LIKE ?', '%' . $params['code'] . '%');

        if (isset($params['list_staff_code']) and $params['list_staff_code'])
            $select->where('p.code IN (?)', $params['list_staff_code']);

        if (isset($params['has_photo']) and $params['has_photo'])
            $select->where('p.photo IS NOT NULL AND p.photo <> \'\'', 1);

        if (isset($params['ood']) and $params['ood']) {
            $select->where(' DATEDIFF(p.contract_expired_at,NOW()) <= ?', 30);
            /* $select->where(' DATEDIFF(NOW(),p.contract_expired_at) >= ?', 0); */
        }

        if (isset($params['no_print']) and $params['no_print']) {
            $select->where('p.print_time = 0 OR p.contract_signed_at is NULL and p.contract_expired_at is NULL', '');
        }

        if (isset($params['tags']) and $params['tags']) {
            $select->join(array('ta_ob' => 'tag_object'), '
                    p.id = ta_ob.object_id
                    AND ta_ob.type = ' . TAG_STAFF . '
                ', array());
            $select->join(array('ta' => 'tag'), '
                    ta.id = ta_ob.tag_id
                ', array());

            $select->where('ta.name IN (?)', $params['tags']);
        }
        if (isset($params['area_id']) and ( $params['area_id'] || $params['area_id'] == 0) && !(isset($params['regional_market']) and $params['regional_market'])) {
            $QRegionalMarket = new Application_Model_RegionalMarket();

            if (is_array($params['area_id']) && count($params['area_id']) > 0) {
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id IN (?)', $params['area_id']);
            } else {
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $params['area_id']);
            }

            $regional_markets = $QRegionalMarket->fetchAll($where);
            $tem              = array();

            foreach ($regional_markets as $regional_market)
                $tem[] = $regional_market->id;

            if (is_array($tem) && count($tem) > 0)
                $select->where('p.regional_market IN (?)', $tem);
            else
                $select->where('1=0', 1);
        } elseif (isset($params['regional_market']) and $params['regional_market']) {
            if (is_array($params['regional_market']) && count($params['regional_market']) > 0) {
                $select->where('p.regional_market IN (?)', $params['regional_market']);
            } else {
                $select->where('1=0', 1);
            }
        }

        // dành để lọc staff cho các asm, admin... xem; chỉ hiện thị staff thuộc khu vực được gán
        if (isset($params['list_province_ids']) && $params['list_province_ids']) {
            if (is_array($params['list_province_ids']) && count($params['list_province_ids'])) {
                $select->where('p.regional_market IN (?)', $params['list_province_ids']);
            } elseif (is_numeric($params['list_province_ids']) && intval($params['list_province_ids']) > 0) {
                $select->where('p.regional_market = ?', intval($params['list_province_ids']));
            } else {
                // neu chua duoc gan khu vuc thi khong duoc xem
                $select->where('p.regional_market = -1');
            }
        }

        if (isset($params['area_trainer_right']) and $params['area_trainer_right']) {
            $QRegionalMarket = new Application_Model_RegionalMarket();

            if (is_array($params['area_trainer_right']) && count($params['area_trainer_right']) > 0) {
                $whereRegionalMarketRight = $QRegionalMarket->getAdapter()->quoteInto('area_id IN (?)', $params['area_trainer_right']);
            } else {
                $whereRegionalMarketRight = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $params['area_trainer_right']);
            }

            $regional_markets_rights = $QRegionalMarket->fetchAll($whereRegionalMarketRight);
            $tem                     = array();

            foreach ($regional_markets_rights as $regional_markets_right)
                $tem[] = $regional_markets_right->id;

            if (is_array($tem) && count($tem) > 0)
                $select->where('p.regional_market IN (?)', $tem);
            else
                $select->where('1=0', 1);
        }

        if (isset($params['s_assign']) && $params['s_assign']) {
            $QDeparment    = new Application_Model_Department();
            $where         = $QDeparment->getAdapter()->quoteInto('name LIKE ?', 'KINH DOANH');
            $department_id = $QDeparment->fetchAll($where)->current()->id;
            $select->where('p.department = ?', $department_id);
        }

        $order_str = '';

        if (isset($params['sort']) and $params['sort']) {
            $collate = ' ';
            $desc    = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';

            if ($params['sort'] == 'name') {
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= ' CONCAT(p.firstname, " ",p.lastname) ' . $collate . $desc;
            } elseif (/* $params['sort'] == 'title' || */$params['sort'] == 'department' || $params['sort'] == 'team') {
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= $collate . $desc;
            } else {
                $order_str = 'p.`' . $params['sort'] . '` ' . $collate . $desc;
            }

            $select->order(new Zend_Db_Expr($order_str));
        }
        $select->where('p.title <> 375');
        $select->group('p.id');
        
        if ($limit)
            $select->limitPage($page, $limit);

        if (isset($params['export']) && $params['export'])
            return $select->__toString();

        if (!empty($_GET['dev'])) {
            echo "<pre>";
            print_r($params);
            print_r($select->__toString());
            exit;
        }
        $result = $db->fetchAll($select);

        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public function UpdatePrintCardStatus($staff_code) {
        $db   = Zend_Registry::get('db');
        $stmt = $db->prepare('UPDATE staff SET is_print = 1 WHERE FIND_IN_SET(`code`, :staff_code)');
        $stmt->bindParam('staff_code', $staff_code, PDO::PARAM_INT);
        $res  = $stmt->execute();
        $stmt->closeCursor();
        return $res;
    }

    public function findStaffid($id) {
        return $this->fetchRow($this->select()->where('id = ?', $id));
    }

    public function getTotalSalePgs($params) {

        $list_sale_psg = unserialize(LIST_PGS_BI);

        $db     = Zend_Registry::get('db');
        $select = $db->select();

        $arrCols = array(
            'sum' => "count(p.id)"
        );

        $select->from(array('p' => 'staff'), $arrCols);
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());

        if (isset($params['area_list']) and $params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        } elseif (isset($params['area']) and $params['area']) {
            $select->where('r.area_id = ?', $params['area']);
        }

        if (isset($params['list_title_staff']) and $params['list_title_staff']) {
            $select->where('p.title IN (?)', $params['list_title_staff']);
        }

        if (isset($params['province']) and $params['province']) {
            $select->where('r.id = ?', $params['province']);
        }

        $select->where('p.off_date IS NULL', null);
        $select->where('p.status = 1', null);
        $select->where('p.title IN (?)', $list_sale_psg);
        if ($_GET['abc'] == 2) {
            echo $select->__toString();
            exit;
        }

        $resu = $db->fetchRow($select);
        return $resu;
    }

    public function fetchPaginationStaffBrandShop($page, $limit, &$total, $params) {
        $db = Zend_Registry::get('db');

        $cols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            'p.name' => "CONCAT(p.firstname, ' ',p.lastname)",
            'p.email',
            'p.id',
            'p.code',
            'p.firstname',
            'p.lastname',
            'p.department',
            'p.team',
            'p.title',
            'p.joined_at',
            'p.phone_number',
            'p.status'
        );

        $select = $db->select()
                ->from(array('p' => 'staff'), $cols)
                ->joinLeft(array('r' => 'regional_market'), 'p.regional_market = r.id', array());
        ;

        //$select->where('p.off_date IS NULL', NULL);

        if (!empty($params['name'])) {
            $select->where('CONCAT(p.firstname, " ", p.lastname) LIKE ?', '%' . $params['name'] . '%');
        }

        if (!empty($params['code'])) {
            $select->where('p.code LIKE ?', '%' . $params['code'] . '%');
        }

        if (!empty($params['email'])) {
            $select->where('p.email LIKE ?', '%' . $params['email'] . '%');
        }

        if (!empty($params['title'])) {
            $select->where('p.title IN (?)', $params['title']);
        }

        if (!empty($params['area_id'])) {
            $select->where('r.area_id IN (?)', $params['area_id']);
        }

        if (!empty($params['status']) AND $params['status']) {

            $from_date = $params['from_date'];
            $from_date = explode('/', trim($from_date));
            $from_date = $from_date[2] . '-' . $from_date[1] . '-' . $from_date[0];

            $to_date = $params['to_date'];
            $to_date = explode('/', trim($to_date));
            $to_date = $to_date[2] . '-' . $to_date[1] . '-' . $to_date[0];

            if ($params['status'] == 1) {
                $select->where('p.off_date >= ?', $from_date);
                $select->where('p.off_date <= ?', $to_date);
            } elseif ($params['status'] == 2) {
                $select->where('p.joined_at >= ?', $from_date);
                $select->where('p.joined_at <= ?', $to_date);
            } elseif ($params['status'] == 3) {
                $select->where('p.off_date IS NULL', NULL);
                //$select->where('p.status = 1', NULL);
            }
        }

        if ($_GET['dev']) {
            echo $select->__toString();
        }
        if ($limit) {
            $select->limitPage($page, $limit);
        }

        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    public function fetchPaginationStaffBrandShopExport($page, $limit, &$total, $params) {
        $db = Zend_Registry::get('db');

        $cols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            'full_name'  => "CONCAT(p.firstname, ' ',p.lastname)",
            'p.email',
            'p.id',
            'p.code',
            'p.firstname',
            'p.lastname',
            'p.department',
            'p.team',
            'p.title',
            'p.joined_at',
            'p.phone_number',
            'p.status',
            'store_name' => 's.name',
            'from_date'  => "DATE_FORMAT(FROM_UNIXTIME(l.joined_at), '%d/%m/%Y')",
            'to_date'    => "DATE_FORMAT(FROM_UNIXTIME(l.released_at), '%d/%m/%Y')"
        );

        $select = $db->select()
                ->from(array('p' => 'staff'), $cols)
                ->joinLeft(array('r' => 'regional_market'), 'p.regional_market = r.id', array())
                ->joinLeft(array('l' => 'store_staff_log'), 'l.staff_id = p.id', array())
                ->joinLeft(array('s' => 'store'), 's.id = l.store_id', array());
        ;

        $select->where('s.is_brand_shop = ?', 1);

        //$select->where('l.is_leader = ?', 0);
        //$select->where('p.off_date IS NULL', NULL);

        if (!empty($params['name'])) {
            $select->where('CONCAT(p.firstname, " ", p.lastname) LIKE ?', '%' . $params['name'] . '%');
        }

        if (!empty($params['code'])) {
            $select->where('p.code LIKE ?', '%' . $params['code'] . '%');
        }

        if (!empty($params['email'])) {
            $select->where('p.email LIKE ?', '%' . $params['email'] . '%');
        }

        if (!empty($params['title'])) {
            //$select->where('p.title IN (?)', $params['title']);
        }

        if (!empty($params['area_id'])) {
            $select->where('r.area_id IN (?)', $params['area_id']);
        }

        if (!empty($params['status']) AND $params['status']) {

            $from_date = $params['from_date'];
            $from_date = explode('/', trim($from_date));
            $from_date = $from_date[2] . '-' . $from_date[1] . '-' . $from_date[0];

            $to_date = $params['to_date'];
            $to_date = explode('/', trim($to_date));
            $to_date = $to_date[2] . '-' . $to_date[1] . '-' . $to_date[0];

            if ($params['status'] == 1) {
                $select->where('p.off_date >= ?', $from_date);
                $select->where('p.off_date <= ?', $to_date);
            } elseif ($params['status'] == 2) {
                $select->where('p.joined_at >= ?', $from_date);
                $select->where('p.joined_at <= ?', $to_date);
            }
        }

        if (!empty($params['from_date'])) {
            $from_date = $params['from_date'];
            $from_date = explode('/', trim($from_date));
            $from_date = $from_date[2] . '-' . $from_date[1] . '-' . $from_date[0];

            $select->where("(DATE_FORMAT(FROM_UNIXTIME(l.released_at), '%Y-%m-%d') >= ?
OR l.released_at IS NULL)", $from_date);
        }

        if (!empty($params['to_date'])) {
            $to_date = $params['to_date'];
            $to_date = explode('/', trim($to_date));
            $to_date = $to_date[2] . '-' . $to_date[1] . '-' . $to_date[0];

            $select->where("DATE_FORMAT(FROM_UNIXTIME(l.joined_at), '%Y-%m-%d') <= ?", $to_date);
        }

        if ($limit) {
            //$select->limitPage($page, $limit);
        }

        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    public function fetchPaginationQuit($page, $limit, &$total, $params) {
        $db = Zend_Registry::get('db');
        define(QUIT_STATUS, 0);

        $select = "";

        if ($limit) {
            $select = $db
                    ->select()
                    ->from(array('p' => $this->_name), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'), 'p.*'));
        } else {
            $select = $db
                    ->select()
                    ->from(array('p' => $this->_name), array(new Zend_Db_Expr('DISTINCT p.id'), 'p.*'));
        }

        $select->joinLeft(array('rm' => 'regional_market'), 'p.regional_market=rm.id', array());

        $select->joinLeft(array('ar' => 'area'), 'ar.id=rm.area_id', array('area_name' => 'ar.name'));

        $select->join(array('v' => new Zend_Db_Expr('(SELECT * FROM  `v_staff_contract_begin` WHERE  `staff_joined` IS NOT NULL GROUP BY staff_id)')), 'v.staff_id = p.id', array());


        $select->where('p.status = ?', QUIT_STATUS);

        if (isset($params['name']) and $params['name']) {
            $select->where('( CONCAT(p.firstname, " ",p.lastname) LIKE ?', '%' . $params['name'] . '%');
            $select->orwhere('p.code LIKE ? ', '%' . $params['name'] . '%');
            $select->orwhere('p.email LIKE ? )', '%' . $params['name'] . '%');
        }
        if (isset($params['code']) and $params['code']) {
            $select->where('p.code LIKE ?', '%' . $params['code'] . '%');
        }
        if (isset($params['offdate_file']) and $params['offdate_file']) {
            $select->where('p.offdate_file ' . $params['offdate_file']);
        }
        if (isset($params['off_type']) and $params['off_type']) {
            $select->where('p.off_type = ' . $params['off_type']);
        }
        if (isset($params['area_id']) and $params['area_id'] && !(isset($params['regional_market']) and $params['regional_market'])) {
            $QRegionalMarket = new Application_Model_RegionalMarket();

            if (is_array($params['area_id']) && count($params['area_id']) > 0) {
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id IN (?)', $params['area_id']);
            } else {
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $params['area_id']);
            }

            $regional_markets = $QRegionalMarket->fetchAll($where);
            $tem              = array();

            foreach ($regional_markets as $regional_market)
                $tem[] = $regional_market->id;

            if (is_array($tem) && count($tem) > 0)
                $select->where('p.regional_market IN (?)', $tem);
            else
                $select->where('1=0', 1);
        } elseif (isset($params['regional_market']) and $params['regional_market']) {
            if (is_array($params['regional_market']) && count($params['regional_market']) > 0) {
                $select->where('p.regional_market IN (?)', $params['regional_market']);
            } else {
                $select->where('1=0', 1);
            }
        }
        if (isset($params['team']) and $params['team']) {
            if (is_array($params['team']) && count($params['team']) > 0) {
                $select->where('p.team IN (?)', $params['team']);
            } else {
                $select->where('1=0', 1);
            }
        }
        if (isset($params['department']) and $params['department']) {
            if (is_array($params['department']) && count($params['department']) > 0) {
                $select->where('p.department IN (?)', $params['department']);
            } else {
                $select->where('1=0', 1);
            }
        }
        if (isset($params['title']) and $params['title']) {

            if (is_array($params['title']) && count($params['title']) > 0) {
                $select->where('p.title IN (?)', $params['title']);
            } else {
                $select->where('1=0', 1);
            }
        }
        // dành để lọc staff cho các asm, admin... xem; chỉ hiện thị staff thuộc khu vực được gán
        if (isset($params['list_province_ids']) && $params['list_province_ids']) {
            if (is_array($params['list_province_ids']) && count($params['list_province_ids'])) {
                $select->where('p.regional_market IN (?)', $params['list_province_ids']);
            } elseif (is_numeric($params['list_province_ids']) && intval($params['list_province_ids']) > 0) {
                $select->where('p.regional_market = ?', intval($params['list_province_ids']));
            } else {
                // neu chua duoc gan khu vuc thi khong duoc xem
                $select->where('p.regional_market = -1');
            }
        }
        if (isset($params['export']) && $params['export']) {
            return $select->__toString();
        }
        if (isset($params['off_date_from']) and $params['off_date_from']) {
            $arrFrom                 = explode('/', $params['off_date_from']);
            $params['off_date_from'] = $arrFrom[2] . '-' . $arrFrom[1] . '-' . $arrFrom[0];
            $select->where('p.off_date_created_at >= ?', $params['off_date_from']);
        }
        if (isset($params['off_date_to']) and $params['off_date_to']) {
            $arrFrom               = explode('/', $params['off_date_to']);
            $params['off_date_to'] = $arrFrom[2] . '-' . $arrFrom[1] . '-' . ($arrFrom[0] + 1);
            $select->where('p.off_date_created_at < ?', $params['off_date_to']);
        }

        $select->where('p.title <> 375');

        // fetch
        $select->order('p.code', 'COLLATE utf8_unicode_ci ASC');
        $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);
        $total  = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public function updateOffFile($params = array()) {

        if (isset($params['new_name']) && isset($params['staff_id'])) {
            $db = Zend_Registry::get('db');

            $sql_update_new_photo = "update staff
            set offdate_file = '" . $params['new_name'] . "'
            where id = " . $params['staff_id'] . "
            limit 1";
            $stmt                 = $db->prepare($sql_update_new_photo);
            $res                  = $stmt->execute();
            if ($res == true && !empty($params['old_name'])) {
                $sql_insert_old_photo = "insert into photo_back
              (staff_id, photo) values
              (" . $params['staff_id'] . " , '" . $params['old_name'] . "')";
                $db->query($sql_insert_old_photo);
            }

            $stmt->closeCursor();
            $db   = $stmt = null;
            return $res;
        }
    }

//TUONG
    public function getDataListContract($id) {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'contract_term'      => "t.name",
            'date_signed'        => "v.from_date",
            'contract_id'        => "v.contract_id",
            'date_expired'       => "v.to_date",
            'print_name'         => "v.Contract type",
            'print_type'         => "v.print_type",
            'date_send'          => "p.send_letter",
            'date_received'      => "p.return_letter",
            'date_upload_file'   => "c.created_at",
            'url'                => "c.url",
            'file_name'          => "c.file_name",
            'return_letter'      => "p.return_letter",
            'img_contract_frond' => "p.img_contract_frond",
            'img_contract_back'  => "p.img_contract_back",
            'is_approved_img'    => "p.is_approved",
            'upload_letter'      => "p.upload_letter"
        );

        $select->from(array('v' => 'v_staff_contract_insurance'), $arrCols);
        $select->joinLeft(array('p' => 'staff_contract'), 'v.contract_id = p.id', array());
        $select->joinLeft(array('t' => 'contract_types'), 't.id = p.contract_term', array());
        $select->joinLeft(array('c' => 'contract_file_log'), 'c.contract_id = v.contract_id', array());

        $select->where('v.staff_id = ?', $id);
        //$select->where('p.return_letter <> p.send_letter');
        $select->order('v.from_date DESC');
        $select->group('v.contract_id');
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getInsurance_bk($id) {

        $db     = Zend_Registry::get('db');
        $cols   = array(
            'a.*',
            'title_name'      => 'CONCAT(b.name_vn," - ",h.name," - ",k.name)',
            'team_name'       => 'c.name',
            'department_name' => 'd.name',
            'regional_name'   => 'e.name',
            'area_name'       => 'f.name',
            'gender_name'     => 'IF(i.gender = 1,"NAM","NỮ")',
            'status_name'     => '(CASE WHEN a.status = 0 THEN "OFF" WHEN a.status = 1 THEN "ON" WHEN a.status = 2 THEN "TEMP OFF" ELSE "CHILDBEARING" END)',
            'id_number_ins'   => 'IFNULL(i.ID_number,a.ID_number)',
            'dob_ins'         => 'IFNULL(i.dob,a.dob)',
            'firstname_ins'   => 'IFNULL(i.firstname,a.firstname)',
            'lastname_ins'    => 'IFNULL(i.lastname,a.lastname)',
        );
        $select = $db->select()
                ->from(array('a' => 'staff'), $cols)
                ->joinLeft(array('i' => 'insurance_info'), 'i.staff_id = a.id', array())
                ->join(array('b' => 'team'), 'a.title = b.id', array())
                ->join(array('c' => 'team'), 'b.parent_id = c.id', array())
                ->join(array('d' => 'team'), 'c.parent_id = d.id', array())
                ->join(array('e' => 'regional_market'), 'e.id = a.regional_market', array())
                ->join(array('f' => 'area'), 'f.id = e.area_id', array())
                ->joinLeft(array('h' => 'province'), 'e.province_id = h.id', array())
                ->joinLeft(array('k' => 'area_nationality'), 'k.id = h.area_national_id', array())
                ->where('a.id = ?', $id)
                ->group('a.id')
        ;
        $result = $db->fetchRow($select);
        return $result;
    }

    public function getInsurance($id) {
        $db = Zend_Registry::get('db');
        $cols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.id'),
            'staff_id' => 'a.id',
            'a.dob',
            'a.gender',
            'a.ID_number',
            'staff_code' => 'a.code',
            'staff_name' => 'CONCAT(a.firstname," ",a.lastname)',
            'unit_code_name' => 'e.unit_code',
            'company_name' => 'd.name',
            'regional_market' => 'b.name',
            'department_name' => 'h.name',
            'team_name' => 'g.name',
            'title_name' => 'CONCAT(f.name_vn," - ",l.name," - ",m.name)',
            'a.insurance_number',
            'a.have_book',
            'a.close_book',
            'a.return_ins_card_time',
            'a.return_ins_book_time',
            'a.medical_card_number',
            'a.take_card_from_ins',
            'a.take_card_from_staff',
            'a.delivery_card_ins',
            'note' => 'a.note_ins',
            'increase' => 'CASE WHEN DAY(i.time_alter) > 15 THEN DATE_ADD(i.time_alter, INTERVAL 1 MONTH) ELSE i.time_alter END',
            'decrease' => 'CASE WHEN DAY(j.time_alter) > 15 THEN DATE_ADD(j.time_alter, INTERVAL 1 MONTH) ELSE j.time_alter END',
            'status' => 'a.status',
            'a.code_BHYT_ins',
            'a.have_BHYT_time',
            'a.qdnv',
            'a.hospital_id',
            'hospital_name' => 'ho.name',
            'hospital_code' => 'ho.code',
            'province_code' => 'ho.province_code',
            'province_name' => 'pr.name',
            'log_created_at' => 'MAX(ili.created_at)'
        );
        $select = $db->select()
                ->from(array('a' => 'staff'), $cols)
                ->join(array('b' => 'regional_market'), 'b.id = a.regional_market', array())
                ->join(array('ist' => 'insurance_staff_TM_now'), 'ist.staff_id = a.id', array())
                ->join(array('i' => 'insurance_staff'), 'i.staff_id = a.id  AND i.`type` = 1 AND i.`option` in (1, 2) AND i.del = 0 AND i.locked = 1 AND i.time_alter = ist.time_alter', array())
                ->joinLeft(array('j' => 'insurance_staff'), 'a.id = j.staff_id AND j.`type` = 2 AND j.`option` = 3 AND j.del = 0 AND j.locked = 1', array())
                ->joinLeft(array('c' => 'insurance_info'), 'c.staff_id = a.id', array())
                ->joinLeft(array('d' => 'company'), 'd.id = a.company_id', array())
                ->joinLeft(array('e' => 'unit_code'), '(e.company_id = i.company_id AND ((a.`id_place_province` in (64, 65)  AND e.`is_foreigner` = 1) OR (a.`id_place_province` not in (64, 65) AND e.`is_foreigner` = 0)))', array())
                ->joinLeft(array('f' => 'team'), 'f.id = a.title', array())
                ->joinLeft(array('g' => 'team'), 'g.id = a.team', array())
                ->joinLeft(array('h' => 'team'), 'h.id = a.department', array())
                ->joinLeft(array('l' => 'province'), 'b.province_id = l.id', array())
                ->joinLeft(array('m' => 'area_nationality'), 'm.id = l.area_national_id', array())
                ->joinLeft(array('ho' => 'hospital'), 'ho.id = a.hospital_id', array())
                ->joinLeft(array('pr' => 'province'), 'pr.id = ho.province_id', array())
                ->joinLeft(array('ili' => 'insurance_log_info'), 'a.id = ili.staff_id', array())
                ->where('a.id = ?', $id)
                ->group('a.id');
        $result = $db->fetchRow($select);
        return $result;
    }

    public function getDataBank($code) {
        $db     = Zend_Registry::get('db');
        $select = $db->select();
        $cols   = array(
            'a.id',
            'a.fullname',
            'a.bank_number',
            'a.at_bank',
            'a.branch_pgd',
            'a.province_city',
            'a.note'
        );
        $select->from(array('a' => 'staff_my_bank'), $cols);
        $select->where('a.staff_code = ?', $code);
        $result = $db->fetchRow($select);
        return $result;
    }

    public function getDataPti($code) {
        $db     = Zend_Registry::get('db');
        $select = $db->select();
        $cols   = array(
            'a.id',
            'a.code',
            'a.fullname',
            'a.tax'
        );
        $select->from(array('a' => 'pti'), $cols);
        $select->where('a.code = ?', $code);
        $result = $db->fetchRow($select);
        return $result;
    }

    public function getId_($code) {
        $db     = Zend_Registry::get('db');
        $select = $db->select();
        $cols   = array(
            'a.id'
        );
        $select->from(array('a' => 'staff'), $cols);
        $select->where('a.code = ?', $code);
        $result = $db->fetchRow($select);
        return $result;
    }

    //TUONG

    public function isPgSumupSales($staff_id) {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $title       = $userStorage->title;
        $db          = Zend_Registry::get('db');
        $select      = $db->select()
                ->from(['s1' => 'store_staff_log'], ['s1.store_id'])
                ->joinLeft(['s2' => 'store'], 's1.store_id = s2.id AND s2.is_sumup_daily = 1', [])
                ->where('s1.staff_id = ?', $staff_id)
                ->where('s1.is_leader = ?', 0)
                ->where('s2.id IS NOT NULL')
                ->where("DATE_FORMAT(FROM_UNIXTIME(s1.joined_at), '%Y-%m-%d') < ?", date('Y-m-d'));
        $stores      = $db->fetchAll($select);

        if (in_array($title, [PGPB_TITLE, SALES_TITLE])) {
            return $stores ? true : false;
        } else {
            return false;
        }
    }

    public function getStaffByTeam($team) {
        $db     = Zend_Registry::get("db");
        $select = $db->select()
                ->from(['st' => 'staff'], ['st.id'])
                ->where('st.team = ?', $team);

        $result = $db->fetchAll($select);

        foreach ($result as $staff) {
            $listStaffId [] = $staff['id'];
        }

        return $listStaffId;
    }

    public static function checkDistanceBetweenTwoMonth($month, $year, $amountMonth) {
        $currentMonth = date('m');
        $currentYear  = date('Y');

        $differentMonth = (($currentYear - $year) * 12 + $currentMonth) - $month;

        if ($differentMonth <= $amountMonth) {
            return true;
        }
        return false;
    }

    public function getStaffInfo($id) {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "s.id",
            "s.height",
            "s.weight",
            "s.image_body",
            "fullname"      => "CONCAT(s.firstname, ' ', s.lastname)",
            "code"          => "s.code",
            "team_name"     => "team.name",
            "title_name"    => "title.name",
            "area_name"     => "a.name",
            "province_name" => "r.name",
            "s.photo",
            "s.department",
        );

        $select->from(array('s' => 'staff'), $arrCols);
        $select->joinLeft(array('team' => 'team'), 'team.id = s.team', array());
        $select->joinLeft(array('title' => 'team'), 'title.id = s.title', array());
        $select->joinLeft(array('r' => 'regional_market'), 's.regional_market = r.id', array());
        $select->joinLeft(array('a' => 'area'), 'a.id = r.area_id', array());

        $select->where('s.id = ?', $id);
        $select->where('s.off_date IS NULL', NULL);
        $result = $db->fetchRow($select);
        return $result;
    }

    public function getIDCardInfor($staff_id) {

        $db  = Zend_Registry::get('db');
        $sql = " SELECT 
                p.name AS place_issued, -- noi cap
                w.name AS ward, 
                d.name AS district,
                rm.name AS province,
                sa.address AS street,
                 st.id_place_province AS  id_place_province,
                w.id  AS id_card_ward_id,
                d.id AS id_card_district,
                rm.id AS id_card_province

                 FROM `staff` AS `st`
                 LEFT JOIN province AS `p` ON st.id_place_province = p.id OR st.id_citizen_province = p.id
                 LEFT JOIN staff_address AS `sa` ON st.id = sa.staff_id AND sa.address_type = 4 
                 LEFT JOIN ward AS `w` ON sa.ward_id = w.id 
                 LEFT JOIN regional_market AS `d` ON sa.district = d.id  
                 LEFT JOIN regional_market AS `rm` ON d.parent = rm.id  

                 WHERE st.id = $staff_id ";

        $stmt   = $db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();


        return $result[0];
    }

    public function getArea($staffId) {
        $db     = Zend_Registry::get("db");
        $select = $db->select()
                ->from(['st' => 'staff'], [
                    're.area_id'
                ])
                ->joinLeft(['re' => 'regional_market'], 'st.regional_market = re.id', [])
                ->where('st.id = ?', $staffId);

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $listArea [] = $element['area_id'];
        }

        return $listArea;
    }

    public function isHeadOffice($staffId) {
        $db     = Zend_Registry::get("db");
        $select = $db->select()
                ->from(['st' => 'staff'], [
                    'r.area_id'
                ])
                ->joinLeft(['r' => 'regional_market'], 'st.regional_market = r.id', [])
                ->where('st.id = ?', $staffId)
                ->where('r.area_id = ?', 65); // headoffice

        $result = $db->fetchALl($select);

        return $result ? true : false;
    }

    public function getListEmail($email) {
        $db     = Zend_Registry::get('db');
        $select = $db->select();
        $cols   = array(
            'a.email'
        );
        $select->from(array('a' => 'staff'), $cols);
        $select->where('a.email = ?', $email);
        $result = $db->fetchRow($select);
        return $result;
    }

    public function getAllEmail() {
        $db     = Zend_Registry::get('db');
        $select = 'select s.email from staff s';
        $result = $db->fetchAll($select);
        return $result;
    }

    function fetchSalesPgNewPagination($page, $limit, &$total, $params) {
        $db = Zend_Registry::get('db');

        $select = $db->select()
                ->from(array('p' => $this->_name), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'), 'p.firstname', 'p.lastname', 'p.email', 'p.phone_number', 'p.regional_market', 'p.group_id', 'p.status'));

        $select
                ->distinct()
                ->joinLeft(array('cw' => 'casual_worker'), 'cw.staff_id=p.id', array('casual_status' => 'cw.status'))
                ->joinLeft(array('sl' => 'store_staff_log'), 'sl.staff_id=p.id', array('current_store' => 'COUNT(DISTINCT sl.store_id)', 'sl.is_leader'))
                ->group('p.id');

        $select
                ->where(' p.title in (		182,
			293,
			419,
			420,
			545,
			577,
			183,
			403,
			417)');


        if (isset($params['name']) and $params['name']) {
            $select->where('CONCAT(p.firstname, " ",p.lastname) LIKE ?', '%' . $params['name'] . '%');
        }

        if (isset($params['email']) and $params['email']) {
            $params['email'] = preg_replace('/' . EMAIL_SUFFIX . '/', '', $params['email']);
            $select->where('p.email LIKE ?', $params['email'] . EMAIL_SUFFIX);
        }

        if (isset($params['province']) and $params['province']) {
            $select->where('p.regional_market = ?', $params['province']);
        } elseif (isset($params['area_id']) and $params['area_id']) {
            $QRegionalMarket = new Application_Model_RegionalMarket();
            $where           = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $params['area_id']);

            $regional_markets = $QRegionalMarket->fetchAll($where);
            $tem              = array();

            foreach ($regional_markets as $regional_market)
                $tem[] = $regional_market->id;

            if (is_array($tem) && count($tem) > 0)
                $select->where('p.regional_market IN (?)', $tem);
            else
                $select->where('1=0', 1);
        }

        if (isset($params['store']) && $params['store']) {
            $QStore = new Application_Model_Store();
            $where  = $QStore->getAdapter()->quoteInto('name LIKE ?', '%' . $params['store'] . '%');
            $result = $QStore->fetchAll($where);

            $store_ids = array();
            foreach ($result as $key => $value) {
                $store_ids[] = $value['id'];
            }

            $select->where('sl.store_id IN (?) OR sl.store_id IN (?)', $store_ids);
        }

        // Filter for ASM
        if (isset($params['asm']) and $params['asm']) {
            $QAsm         = new Application_Model_Asm();
            $list_regions = $QAsm->get_cache($params['asm']);
            $list_regions = isset($list_regions['province']) && is_array($list_regions['province']) ? $list_regions['province'] : array();

            if (count($list_regions))
                $select->where('p.regional_market IN (?)', $list_regions);
            else
                $select->where('1=0', 1);
        }

        if (isset($params['staff']) and $params['staff']) {
            $select->where('p.id = ?', intval($params['staff']));
        }

        $order_str = '';

        if (isset($params['sort']) and $params['sort']) {
            $collate = ' ';

            if ($params['sort'] == 'area_id') {
                $select
                        ->join(array('r' => 'regional_market'), 'r.id=p.regional_market', array())
                        ->join(array('a' => 'area'), 'a.id=r.area_id', array());
            } elseif ($params['sort'] == 'province') {
                $select
                        ->join(array('r' => 'regional_market'), 'r.id=p.regional_market', array());
            }

            $desc = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';

            if (in_array($params['sort'], array('area_id', 'province', 'name'))) {
                $collate = ' COLLATE utf8_unicode_ci ';
            }

            if ($params['sort'] == 'name') {
                $order_str .= ' CONCAT(p.firstname, " ",p.lastname) ' . $collate . $desc;
            } elseif ($params['sort'] == 'area_id') {
                $order_str .= ' a.name ' . $collate . $desc;
            } elseif ($params['sort'] == 'province') {
                $order_str .= ' r.name ' . $collate . $desc;
            } elseif (in_array($params['sort'], array('current_store', 'pending_store'))) {
                $order_str .= $params['sort'] . ' ' . $desc;
            } else {
                $order_str = 'p.`' . $params['sort'] . '` ' . $collate . $desc;
            }

            $select->order(new Zend_Db_Expr($order_str));
        }
        // echo "<pre>";print_r($select->__toString());die;

        if ($limit)
            $select->limitPage($page, $limit);
        if (!empty($_GET['dev'])) {
            echo "<pre>";
            print_r($select->__toString());
            exit;
        }
        $result = $db->fetchAll($select);

        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");
    }

    public function getChannelId($staff_id) {
        $db = Zend_Registry::get('db');

        $select = $db->select()
                ->from(array('s' => 'store_staff_log'), array("
                            CASE
                                 WHEN s1.`name` LIKE '%ĐMX%' OR s1.`name` LIKE '%TAG%' THEN 1 
                                WHEN  ( s1.`name` LIKE '%TGDĐ%' AND (s1.`name` NOT LIKE '%ĐMX%' OR s1.`name` NOT LIKE '%TAG%')) THEN 2
                                 WHEN s1.is_fpt = 1 THEN 3
                                WHEN s1.is_viettel = 1 THEN 1
                                WHEN s1.is_vta = 1 THEN 4
                                WHEN s1.is_dmcl = 1 THEN 6
                                WHEN s1.`name` LIKE '%Điện máy HC%' THEN 6
                                WHEN s1.is_media = 1 THEN 6
                                WHEN s1.is_pico = 1 THEN 6
                                WHEN s1.is_nk = 1 THEN 6
                                WHEN s1.`name` LIKE '%Thiên Hòa%' THEN 6
                                WHEN s1.`name` LIKE '%OPPO Brandshop%' THEN 12
                                WHEN s1.`name` LIKE '%BKC_Công nghệ số%' THEN 12
                                WHEN s1.`name` LIKE '%BKC_Quốc Nghĩa%' THEN 12
                                WHEN s1.`name` LIKE '%Mobifoneplus%' THEN 12
                                WHEN (d.is_ka = 0 AND (dl.loyalty_plan_id IS NULL OR dl.loyalty_plan_id IN (4) ) ) THEN 10
                                WHEN (d.is_ka = 0 AND (dl.loyalty_plan_id IS NULL OR dl.loyalty_plan_id IN (1) ) ) THEN 9
                                WHEN (d.is_ka = 0 AND (dl.loyalty_plan_id IS NULL OR dl.loyalty_plan_id IN (2) ) ) THEN 8
                                WHEN (d.is_ka = 0 AND (dl.loyalty_plan_id IS NULL OR dl.loyalty_plan_id IN (3) ) ) THEN 7
                        END AS type"
        ));
        $select->join(array('s1' => 'store'), 's1.id=s.store_id', array());
        $select->join(array('s2' => 'staff'), 's2.id=s.staff_id AND s.is_leader=0', array());
        $select->join(array('rm' => 'regional_market'), 'rm.id=s2.regional_market', array());
        $select->join(array('a' => 'area'), 'a.id=rm.area_id', array());
        $select->joinLeft(array('d' => 'warehouse.distributor'), 'd.id=s1.d_id', array());
        $select->joinLeft(array('dl' => 'dealer_loyalty'), 'd.id=dl.dealer_id', array());
        $select->where('s.released_at is null');
        $select->where('s.staff_id = ?', $staff_id);
        $select->having('type is not null');
        $result = $db->fetchRow($select);
        if ($_GET['dev']) {
            echo $select->__toString();
        }
        return $result['type'];
    }

    public function getTotalPgsByChannelId($channelId = null, $from = null, $to = null) {
        $list_sale_psg = unserialize(LIST_PGS_BI);
        $db            = Zend_Registry::get('db');
        $select        = $db->select();
        $select->from(array(), array());
        $nestedSelect  = $db->select()
                ->from(array('p' => 'staff'), array("
                            CASE
                                 WHEN s1.`name` LIKE '%ĐMX%' OR s1.`name` LIKE '%TAG%' THEN 1 
                                WHEN  ( s1.`name` LIKE '%TGDĐ%' AND (s1.`name` NOT LIKE '%ĐMX%' OR s1.`name` NOT LIKE '%TAG%')) THEN 2
                                 WHEN s1.is_fpt = 1 THEN 3
                                WHEN s1.is_viettel = 1 THEN 1
                                WHEN s1.is_vta = 1 THEN 4
                                WHEN s1.is_dmcl = 1 THEN 6
                                WHEN s1.`name` LIKE '%Điện máy HC%' THEN 6
                                WHEN s1.is_media = 1 THEN 6
                                WHEN s1.is_pico = 1 THEN 6
                                WHEN s1.is_nk = 1 THEN 6
                                WHEN s1.`name` LIKE '%Thiên Hòa%' THEN 6
                                WHEN s1.`name` LIKE '%OPPO Brandshop%' THEN 12
                                WHEN s1.`name` LIKE '%BKC_Công nghệ số%' THEN 12
                                WHEN s1.`name` LIKE '%BKC_Quốc Nghĩa%' THEN 12
                                WHEN s1.`name` LIKE '%Mobifoneplus%' THEN 12
                                WHEN (d.is_ka = 0 AND (dl.loyalty_plan_id IS NULL OR dl.loyalty_plan_id IN (4) ) ) THEN 10
                                WHEN (d.is_ka = 0 AND (dl.loyalty_plan_id IS NULL OR dl.loyalty_plan_id IN (1) ) ) THEN 9
                                WHEN (d.is_ka = 0 AND (dl.loyalty_plan_id IS NULL OR dl.loyalty_plan_id IN (2) ) ) THEN 8
                                WHEN (d.is_ka = 0 AND (dl.loyalty_plan_id IS NULL OR dl.loyalty_plan_id IN (3) ) ) THEN 7
                                ELSE 12
                        END AS channel_id"
        ));
        $nestedSelect->joinLeft(array('rm' => 'regional_market'), 'rm.id=p.regional_market', array());
        $nestedSelect->joinLeft(array('a' => 'area'), 'a.id=rm.area_id', array());

        if (!empty($from) AND ! empty($to)) {
            $nestedSelect->joinLeft(array('store_sl' => 'store_staff_log'), 'p.id=`store_sl`.staff_id AND FROM_UNIXTIME(store_sl.joined_at) <= "' . $from . '"
	AND (
		FROM_UNIXTIME(store_sl.released_at) >= "' . $to . '"
		OR store_sl.released_at IS NULL
        OR FROM_UNIXTIME(store_sl.released_at) < "' . $to . '"
	) 
    ', array());
        } else {
            $nestedSelect->joinLeft(array('store_sl' => 'store_staff_log'), 'p.id=`store_sl`.staff_id', array());
        }


        $nestedSelect->joinLeft(array('s1' => 'store'), 's1.id=store_sl.store_id', array());
        $nestedSelect->joinLeft(array('d' => 'warehouse.distributor'), 'd.id=s1.d_id', array());
        $nestedSelect->joinLeft(array('dl' => 'dealer_loyalty'), 'd.id=dl.dealer_id AND dl.from_date = (SELECT MAX("2019-01-01") FROM dealer_loyalty)', array());
        $nestedSelect->where('p.title IN (?)', $list_sale_psg);
        $nestedSelect->where('store_sl.id is not null');
        // $nestedSelect->where('store_sl.id ='.new Zend_Db_Expr("(SELECT MAX(id) from store_staff_log where store_staff_log.staff_id=p.id)"));
        // $nestedSelect->where('p.status =1 ');
        // $nestedSelect->where('p.off_date is null ');
        $nestedSelect->group('p.id');
        if (!empty($channelId)) {
            $nestedSelect->having('channel_id = ?', $channelId);
        }
        $select->joinLeft(array('subquery' => new Zend_Db_Expr('(' . $nestedSelect . ')')), '', array('total_pgs' => new Zend_Db_Expr('COUNT(subquery.channel_id)')));
        // $select->order('subquery.channel_id');
        if (!empty($channelId)) {
            $select->group('subquery.channel_id');
        }
        // if($_GET['dev'] == 4){
        // echo $select->__toString();
        // exit;
        // }
        $result = $db->fetchRow($select);
        return $result;
    }

   	
	public function getAllName($strId) {
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $cols = array(
            'full_name' => 'CONCAT(a.firstname," ",a.lastname)',
			'staff_code' => 'a.code',
			'staff_id' => 'a.id',
        );
        $select->from(array('a'=>'staff'), $cols);
        $select->where('a.id in (?)', $strId);
        $result = $db->fetchAll($select);
        //var_dump($result);die;
        return $result;
    }

    function getCacheLeaderByArea(){
        $db         = Zend_Registry::get('db');
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name . '_cache_area_leader');
        if (!$result) {
            $select = $db->select()
                ->from(array('p' => $this->_name), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'), 'fullname' => 'CONCAT(p.firstname, " ",p.lastname)'));
            $select
                ->distinct()
                ->joinLeft(array('sl' => 'store_leader'), 'sl.staff_id=p.id AND sl.status=1', array())
                ->joinLeft(array('sll' => 'store_leader'), 'sll.staff_id=p.id AND sll.status=0', array())
                ->join(array('r' => 'regional_market'), 'r.id=p.regional_market', array())
                ->join(array('a' => 'area'), 'a.id=r.area_id', array('area_title' => 'a.name'))
                ->group('p.id');
            $select->where('p.off_date is null and p.title = ?', LEADER_TITLE);
            $data = $db->fetchAll($select);
            $result = array();
            foreach ($data as $value) {
                $result[$value['area_title']] = $value['fullname'];
            }
            $cache->save($result, $this->_name . '_cache_area_leader', array(), null);
        }
        return $result;
    }

    public function getStaffByArray($params) {
        $db = Zend_Registry::get('db');
        if(empty($params['code']) AND empty($params['staff_id'])){
            return 0;
        }
        $select = $db->select();
        $cols = array(
            'full_name' => 'CONCAT(s.firstname," ",s.lastname)',
            'staff_code' => 's.code',
            'department' => 's.department',
            'staff_id'   => 's.id',
        );
        $select->from(array('s'=> $this->_name), $cols);
        $select->joinLeft(array('t' => 'team'), 's.title = t.id', array('position' => 't.name'));
        if(!empty($params['code'])){
            $select->where('s.code in (?)', $params['code']);
        }
        if(!empty($params['staff_id'])){
            $select->where('s.id in (?)', $params['staff_id']);
        }
        $select->where('s.off_date IS NULL');
        $select->where('s.joined_at IS NOT NULL');
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getInfoStar($staff_id){
        $db = Zend_Registry::get('db');
        if(empty($staff_id) AND empty($staff_id)){
            return NULL;
        }
        $select = $db->select();
        $cols = array(
            'p.id', 
            'p.code', 
            'fullname' => "CONCAT(p.firstname, ' ', p.lastname)", 
            "p.joined_at", 
            "p.off_date",
            "temp_date" => "s.date", 
            "s.total_attendance", 
            "s.total_ot", 
            "s.total_kpi", 
            "s.total_warning",
            "s.total_reward", 
            "s.total_pass", 
            "s.total_fail", 
            "s.total", 
            "s.result", 
            "s.area", 
            "s.title", 
            "s.thamnien",
            "s.total_other"
        );
        $select->from(array('p'=> $this->_name), $cols);
        $select->joinLeft(array('s' => 'sp_assessment_temp_date'), 's.staff_id = p.id', array());

        $select->where('p.id = ?', $staff_id);
        $select->order('s.date DESC');

        $result = $db->fetchRow($select);
        return $result;
    }

}

// end class model
