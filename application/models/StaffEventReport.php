<?php
class Application_Model_StaffEventReport extends Zend_Db_Table_Abstract
{
    protected $_name = 'staff_event_report';

    function fetchPagination($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get('db');

        $select = $db->select();

        $select->from(array('p' => $this->_name),array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.*'),
            'event_name' => 'b.name',
            'dealer_name' => 'c.title',
            'staff_name' => 'CONCAT(d.firstname," ", d.lastname)',
            'area_name' => 'f.name'
        ))
        ->joinLeft(array('b'=>'trainer_event_type'),'p.event_type = b.id',array())
        ->joinLeft(array('c'=>WAREHOUSE_DB.'.distributor'),'c.id = p.dealer_id',array())
        ->join(array('d'=>'staff'),'d.id = p.staff_id',array())
        ->join(array('e'=>'regional_market'),'d.regional_market = e.id',array())
        ->join(array('f'=>'area'),'f.id = e.area_id',array())
        ;

        if(isset($params['staff_id']) and $params['staff_id'])
        {
            $select
                ->where('p.staff_id IN (?)', $params['staff_id']);
        }

        if(isset($params['date']) and $params['date'])
        {
            $select
                ->where('p.date = ?', $params['date']);
        }

        if(isset($params['from_date']) and $params['from_date'])
        {
            $select
                ->where('p.date >= ?', $params['from_date']);
        }
        if(isset($params['to_date']) and $params['to_date'])
        {
            $select
                ->where('p.date <= ?', $params['to_date']);
        }

        if(isset($params['dealer']) and $params['dealer'])
        {
            $select
                ->where('p.dealer_id = ?', $params['dealer']);
        }

        if(isset($params['store']) and $params['store'])
        {
            $select
                ->where('p.store_id = ?', $params['store']);
        }

        if (isset($params['sort']) and $params['sort']) {

            $order_str = ' ';

            $desc = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';

            if ($params['sort'] == 'dealer_id' || $params['sort'] == 'store_id' ) {

                $order_str = 'p.`'.$params['sort'] . '` ' .$desc;
            }

            if ($params['sort'] == 'visitors' || $params['sort'] == 'visitors' ) {

                $order_str = 'p.`'.$params['sort'] . '` ' .$desc;
            }

            if ($params['sort'] == 'date') {

                $order_str = 'p.`'.$params['sort'] . '` ' . $desc;
            }

            if ($params['sort'] == 'visitors') {

                $order_str = 'p.`'.$params['sort'] . '` ' . $desc;
            }

            $select->order(new Zend_Db_Expr($order_str));
        }

        $select->where('p.del = ? OR p.del IS NULL', 0);

        $select->order('p.date DESC');

        if ($limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        foreach($result as $key => $value){
            $date = new DateTime($value['date']);
            $date->sub(new DateInterval('P7D'));
            $from =  $date->format('Y-m-d');

            $date2 = new DateTime($value['date']);
            //$date2->add(new DateInterval('P2D'));
            $to =  $date2->format('Y-m-d');

            $cols = array('sell_out'=>'COUNT(DISTINCT(a.imei_sn))');
            $select2 = $db->select()
                ->from(array('a'=>'imei_kpi'),$cols)
                ->where('a.store_id = ?',$value['store_id'])
                ->where('DATE(a.timing_date)  = ?',$from)    
                //->where('DATE(a.timing_date) < ?',$value['date'])
                ->group('a.store_id');
            $sell_out_before = $db->fetchOne($select2);

            $select3 = $db->select()
                ->from(array('a'=>'imei_kpi'),$cols)
                ->where('a.store_id = ?',$value['store_id'])
                ->where('DATE(a.timing_date)  = ?',$value['date'])    
                //->where('DATE(a.timing_date) <= ?',$to)
                ->group('a.store_id');
            $sell_out_after = $db->fetchOne($select3);
            $result[$key]['sell_out_before'] = $sell_out_before;
            $result[$key]['sell_out_after']  = $sell_out_after;
            
        }

        return $result;
    }

}