<?php

class Application_Model_TimingCheck extends Zend_Db_Table_Abstract
{
    public function checkImeiDealer($imei, $store)
    {
        $db = Zend_Registry::get('db');

        $stmt = $db->prepare('CALL `SP_imei_map_CheckImei`(:imei, :store)');
        $stmt->bindParam('imei', $imei, PDO::PARAM_STR);
        $stmt->bindParam('store', $store, PDO::PARAM_INT);

        $stmt->execute();
        $res = $stmt->fetch();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $res;
    }
    public function checkImeiDuAn($imei){
        $db = Zend_Registry::get('db');

        $sql = 'SELECT * FROM imei_du_an WHERE imei_sn = :imei';

        $stmt = $db->prepare($sql);
        $stmt->bindParam('imei', $imei, PDO::PARAM_STR);

        $stmt->execute();
        $res = $stmt->fetch();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $res;
    }

    public function checkImeiTranAnh($imei,$store){
        $db = Zend_Registry::get('db');

        $sql = 'SELECT imei_sn,
                CASE WHEN d.title  LIKE "%viettel%" THEN 1 ELSE 2 END as dealer,
                CASE  WHEN s.`name` LIKE "%trần anh%" THEN 1 ELSE 2 END as store, 
                d.title,s.`name`
                from warehouse.imei i 
                JOIN warehouse.distributor d 
                    ON i.distributor_id = d.id
                JOIN store s 
                    ON s.id = :store
                WHERE imei_sn = :imei';

        $stmt = $db->prepare($sql);
        $stmt->bindParam('imei', $imei, PDO::PARAM_STR);
        $stmt->bindParam('store', $store, PDO::PARAM_INT);
        $stmt->execute();
        $res = $stmt->fetch();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $res;
    }
    public function insertImeiDuAnLog($staff_id,$imei,$store)
    {     

            $timing = date("Y-m-d H:i:s");

            $db = Zend_Registry::get('db');
            $sql = "INSERT INTO imei_du_an_log (staff_id,imei,store_id,timing) VALUES (:staff_id,:imei,:store_id,:timing)";
            $stmt = $db->prepare($sql);
            $stmt->bindParam("staff_id",  $staff_id, PDO::PARAM_STR);
            $stmt->bindParam("imei",  $imei, PDO::PARAM_STR);
            $stmt->bindParam("store_id",  $store, PDO::PARAM_STR);
            $stmt->bindParam("timing",  $timing);
            $stmt->execute();
            $stmt->closeCursor();
            $db = $stmt = null; 
           
    }

    public function checkImeiBrandshop($imei,$store){
        $db = Zend_Registry::get('db');

        $sql = '
                SELECT i.imei_sn,IF(d.title LIKE "%OPPO Brandshop%",1,2) check_sell_in,d.is_price_retail_demo check_brs,s.is_brand_shop,brand.type
                FROM warehouse.imei i
                JOIN warehouse.distributor d
                    ON i.distributor_id = d.id
                LEFT JOIN store s
                    ON s.id = :store
                LEFT JOIN imei_daily_brandshop brand
                    ON i.imei_sn = brand.imei_sn
                WHERE i.imei_sn = :imei';

        $stmt = $db->prepare($sql);
        $stmt->bindParam('imei', $imei, PDO::PARAM_STR);
        $stmt->bindParam('store', $store, PDO::PARAM_INT);
        $stmt->execute();
        $res = $stmt->fetch();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $res;
    }
    
     public function checkImeiVinpro($imei){
        $db = Zend_Registry::get('db');

        $sql = 'SELECT imei_sn, d.title
                from warehouse.imei i 
                JOIN warehouse.distributor d 
                    ON i.distributor_id = d.id
               
                WHERE imei_sn = :imei AND (d.id IN (10360, 22670, 23389) OR d.`parent` IN (10360, 22670, 23389) ) ';

        $stmt = $db->prepare($sql);
        $stmt->bindParam('imei', $imei, PDO::PARAM_STR);
        $stmt->execute();
        $res = $stmt->fetch();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $res;
    }

    public function checkDifferentAreaImei($imei, $store_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['i' => WAREHOUSE_DB . '.imei'], [
                'i.imei_sn'
            ])
            ->joinLeft(['d' => WAREHOUSE_DB . '.distributor'], 'i.distributor_id = d.id', [])
            ->joinLeft(['di' => DATABASE_CENTER . '.regional_market'], 'd.district = di.id', [])
            ->joinLeft(['pi' => DATABASE_CENTER . '.regional_market'], 'di.parent = pi.id', [])

            ->joinLeft(['s' => DATABASE_CENTER . '.store'], 's.id = ' . $store_id, [])
            ->joinLeft(['ds' => DATABASE_CENTER . '.regional_market'], 's.district = ds.id', [])
            ->joinLeft(['ps' => DATABASE_CENTER . '.regional_market'], 'ds.parent = ps.id', [])

            ->joinLeft(['l' => DATABASE_CENTER. '.locked_imei'], 'i.imei_sn = l.imei_sn', [])
            ->joinLeft(['f' => DATABASE_CENTER. '.imei_demo_fpt'], 'f.imei_sn = i.imei_sn', [])
            ->joinLeft(['ie' => DATABASE_CENTER. '.imei_exception'], 'ie.imei_sn = i.imei_sn', [])

            ->where('i.imei_sn = ?', $imei)
            ->where('pi.area_id <> ps.area_id' )
            ->where('l.imei_sn IS NULL')
            ->where('f.imei_sn IS NULL')
            ->where('ie.imei_sn IS NULL');

        $result = $db->fetchOne($select);

        return $result ? true : false;
    }


}
