<?php

class Application_Model_ReserveFee extends Zend_Db_Table_Abstract
{
    protected $_name = 'reserve_fee';
    function getReserveFeeInDepartment(&$day){
        $db = Zend_Registry::get('db');
        $QDepartment=new Application_Model_Team();
        $department= $QDepartment->get_department();

        $arrCols = array(
            new Zend_Db_Expr('p.*,CAST(p.created_at AS DATE) as date,SUM(p.amount) AS total_amount'));
        $select = $db->select()->from(array('p' => $this->_name), $arrCols);
//        get form today to 6 days after
        if(!empty($day)){
            $i=$day;
            $today=date("Y-m-d",strtotime("+".$i." days"));
//            echo $today;
        }else{
            $today=date("Y-m-d");
        }

        $theFollowingDay=date("Y-m-d",strtotime("+6 days"));

        $today_temp=$today;
        for($i=1;$i<=6;$i++){
            $arrayDate[$today_temp]=getdate($today_temp)["weekday"];
            $today_temp=date("Y-m-d",strtotime("+1 days"));
        }

//
//        $select->where("p.created_at >= ?",$today);
//        $select->where("p.created_at <= ?",$theFollowingDay);
        $select->group('p.department_id');
        $select->group('CAST(p.created_at AS DATE)');
        $select->order("p.created_at ASC");
        $data = $db->fetchAll($select);
        $result=array();

        foreach($arrayDate as $date => $dayofweek){
            foreach($data as $key => $value){
                    $result[$value["date"]][$department[$value["department_id"]]]=$value["total_amount"];
            }
        }
//        debug($result);

        return $result;
    }
    function getTotalFee(&$day){
        $db = Zend_Registry::get('db');

        $arrCols = array(
            new Zend_Db_Expr('p.*,CAST(p.created_at AS DATE) as date,SUM(p.amount) AS total_amount'));
        $select = $db->select()->from(array('p' => $this->_name), $arrCols);
//        get form today to 6 days after
        if(!empty($day)){
            $i=$day;
            $today=date("Y-m-d",strtotime("+".$i." days"));
        }else{
            $today=date("Y-m-d");
        }
        $theFollowingDay=date("Y-m-d",strtotime("+6 days"));

        $today_temp=$today;
        for($i=1;$i<=6;$i++){
            $arrayDate[$today_temp]=getdate($today_temp)["weekday"];
            $today_temp=date("Y-m-d",strtotime("+1 days"));
        }

//
        $select->where("p.created_at >= ?",$today);
        $select->where("p.created_at <= ?",$theFollowingDay);
        $select->group('CAST(p.created_at AS DATE)');
        $select->order("p.created_at ASC");
        $data = $db->fetchAll($select);
        $result=array();

        foreach($arrayDate as $date => $dayofweek){
            foreach($data as $key => $value){
                    $result[$value["date"]]=$value["total_amount"];
            }
        }
        return $result;

    }

}

