<?php

class Application_Model_StoreStaffLog extends Zend_Db_Table_Abstract {

    protected $_name = 'store_staff_log';

    function fetchPagination($page, $limit, &$total, $params) {
        $db = Zend_Registry::get('db');

        $select = $db->select()
                ->from(array('p' => $this->_name), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        $select
                ->joinLeft(array('s' => 'staff'), 'p.staff_id = s.id', array('s.email'))
                ->join(array('st' => 'store'), 'st.id=p.store_id', array('store_id' => 'st.id', 'store_name' => 'st.name', 'st.district', 'is_tgdd' => 'st.is_tgdd'));

        if (isset($params['email']) and $params['email'])
            $select->where('s.email LIKE ?', str_replace(EMAIL_SUFFIX, '', $params['email']) . EMAIL_SUFFIX);

        if (isset($params['staff_id']) and $params['staff_id'])
            $select->where('s.id = ?', $params['staff_id']);

        if (isset($params['from']) and $params['from'] && isset($params['to']) and $params['to']) {
            $select->where('FROM_UNIXTIME(p.joined_at, "%Y-%m-%d") <= ?', $params['to']);
            $select->where('p.released_at IS NULL OR FROM_UNIXTIME(p.released_at, "%Y-%m-%d") > ?', $params['from']);
        }

        $order_str = ' released_at IS NULL DESC, released_at DESC, joined_at DESC';

        $select->order(new Zend_Db_Expr($order_str));

        if ($limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);

        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    function fetchSalesBrandshopPagination($page, $limit, &$total, $params) {
        $db = Zend_Registry::get('db');

        $select = $db->select()
                ->from(array('p' => $this->_name), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        $select
                ->joinLeft(array('s' => 'staff'), 'p.staff_id = s.id', array('s.email'))
                ->join(array('st' => 'store'), 'st.id=p.store_id', array('store_id' => 'st.id', 'store_name' => 'st.name', 'st.district'));

        if (isset($params['email']) and $params['email'])
            $select->where('s.email LIKE ?', str_replace(EMAIL_SUFFIX, '', $params['email']) . EMAIL_SUFFIX);

        if (isset($params['staff_id']) and $params['staff_id'])
            $select->where('s.id = ?', $params['staff_id']);

        if (isset($params['from']) and $params['from'] && isset($params['to']) and $params['to']) {
            $select->where('FROM_UNIXTIME(p.joined_at, "%Y-%m-%d") <= ?', $params['to']);
            $select->where('p.released_at IS NULL OR FROM_UNIXTIME(p.released_at, "%Y-%m-%d") > ?', $params['from']);
        }

        $select->where('p.is_leader = 6');

        // $select->where('is_leader = ?' 6);
        $order_str = ' released_at IS NULL DESC, released_at DESC, joined_at DESC';

        $select->order(new Zend_Db_Expr($order_str));
        // echo "<pre>";print_r($select->__toString());die;
        if ($limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);

        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    /**
     * Kiểm tra xem chấm công vào lúc timing_time, tại cửa hàng store_id có...
     *     ...thuộc quản lý của staff_id hay không
     * @param  int  $staff_id    - id của người muốn check hàng
     * @param  int  $store_id    - id cửa hàng
     * @param  datetime  $timing_time - thời gian của chấm công (timing for)
     * @param  boolean $is_leader - true: là sales;
     *                            false: là PG
     *                            NULL: không quan tâm, chỉ cần biết nó thuộc cửa hàng
     * @return boolean              - true: thuộc thằng đó;
     *                                false: không thuộc
     */
    public function belong_to($staff_id, $store_id, $timing_time, $is_leader = NULL) {
        $where   = array();
        $where[] = $this->getAdapter()->quoteInto('store_id = ?', $store_id);
        $where[] = $this->getAdapter()->quoteInto('staff_id = ?', $staff_id);

        if (!is_null($is_leader))
            if ($is_leader)
                $where[] = $this->getAdapter()->quoteInto('is_leader = ?', 1);
            else
                $where[] = $this->getAdapter()->quoteInto('is_leader = ?', 0);

        $where[] = $this->getAdapter()
                ->quoteInto('? >= FROM_UNIXTIME(joined_at, \'%Y-%m-%d\') AND ( ? < FROM_UNIXTIME(released_at, \'%Y-%m-%d\') OR released_at IS NULL OR released_at = 0 )', date('Y-m-d', strtotime($timing_time)));

        return $this->fetchRow($where) ? true : false;
    }

    /**
     * Lấy danh sách các store mà staff đó thuộc tại thời điểm $time
     * @param  int $staff_id  -  staff id
     * @param  datetime $time - thời gian để check
     * @return array          - mảng các store id
     */
    public function get_stores($staff_id, $time) {
        $where   = array();
        $where[] = $this->getAdapter()->quoteInto('staff_id = ?', $staff_id);
        $where[] = $this->getAdapter()->quoteInto('FROM_UNIXTIME(joined_at, \'%Y-%m-%d\') <= ?', $time);
        $where[] = $this->getAdapter()->quoteInto('released_at IS NULL OR released_at = 0 OR FROM_UNIXTIME(released_at, \'%Y-%m-%d\') > ?', $time);

        $result = $this->fetchAll($where);

        $stores = array();

        foreach ($result as $value)
            $stores[] = $value['store_id'];

        return count($stores) > 0 ? $stores : false;
    }

    /**
     * Lấy danh sách các store mà staff đó quản lý từ $from đến $to
     * @param  int $staff_id  -  staff id
     * @param  datetime $time - thời gian để check
     * @return array          - mảng các store id
     */
    public function get_stores_cache($staff_id, $from, $to) {
        $cache  = Zend_Registry::get('cache');
        $result = $cache->load($this->_name . '_cache');

        if ($result === false) {
            $db     = Zend_Registry::get('db');
            $select = $db->select()
                    ->distinct()
                    ->from(array('p' => $this->_name), array('p.store_id', 'p.staff_id'))
                    ->where('FROM_UNIXTIME(joined_at, \'%Y-%m-%d\') <= ?', $to)
                    ->where('released_at IS NULL OR released_at = 0 OR FROM_UNIXTIME(released_at, \'%Y-%m-%d\') > ?', $from);

            // echo "<pre>";print_r($select->__toString());die;
            $data = $db->fetchAll($select);

            $result = array();

            foreach ($data as $value)
                $result[$value['staff_id']][] = $value['store_id'];

            $cache->save($result, $this->_name . '_cache', array(), null);
        }
        // echo "<pre>";print_r($result[ $staff_id ]);die;
        return isset($result[$staff_id]) && count($result[$staff_id]) ? $result[$staff_id] : false;
    }

    public function get_stores_brandshop($staff_id, $from, $to) {
        $cache  = Zend_Registry::get('cache');
        $result = $cache->load($this->_name . '_cache_brs');

        if ($result === false) {
            $db     = Zend_Registry::get('db');
            $select = $db->select()
                    ->distinct()
                    ->from(array('p' => $this->_name), array('p.store_id', 'p.staff_id'))
                    ->where('FROM_UNIXTIME(joined_at, \'%Y-%m-%d\') <= ?', $to)
                    ->where('is_leader = ?', 6)
                    ->where('released_at IS NULL OR released_at = 0 OR FROM_UNIXTIME(released_at, \'%Y-%m-%d\') > ?', $from);

            // echo "<pre>";print_r($result);die;
            $data = $db->fetchAll($select);

            $result = array();

            foreach ($data as $value)
                $result[$staff_id][] = $value['store_id'];

            $cache->save($result, $this->_name . '_cache_brs', array(), null);
        }
        // echo "<pre>";print_r($result[ $staff_id ]);die;
        return isset($result[$staff_id]) && count($result[$staff_id]) ? $result[$staff_id] : false;
    }

    /**
     * Khi PG/Sales chấm công, hàm này giúp lấy...
     *     ...ID sales quản lý cửa hàng đó, vào ngày chấm công
     * @param  int $store_id - ID cửa hàng
     * @param  datetime $time     - Ngày chấm công
     * @return int           - ID sales quản lý cửa hàng
     */
    public function get_sales_man($store_id, $time) {
        $where   = array();
        $where[] = $this->getAdapter()->quoteInto('is_leader = ?', 1);
        $where[] = $this->getAdapter()->quoteInto('store_id = ?', $store_id);

        $where[] = $this->getAdapter()->quoteInto(
                'FROM_UNIXTIME(joined_at, \'%Y-%m-%d\') <= ?', date('Y-m-d', strtotime($time))
        );

        $where[] = $this->getAdapter()->quoteInto(
                'released_at IS NULL OR released_at = 0 OR FROM_UNIXTIME(released_at, \'%Y-%m-%d\') > ?', date('Y-m-d', strtotime($time))
        );

        $result = $this->fetchRow($where);

        return $result && intval($result['staff_id']) > 0 ? $result['staff_id'] : false;
    }

    public function getSaleManList($from, $to) {
        $db  = Zend_Registry::get('db');
        $sql = "SELECT DISTINCT store_staff_log.staff_id, store_staff_log.store_id, staff.firstname, staff.lastname
            FROM store_staff_log
            INNER JOIN staff ON staff.id=store_staff_log.staff_id
            WHERE DATE(FROM_UNIXTIME(store_staff_log.joined_at)) <= ?
            AND (store_staff_log.released_at IS NULL OR DATE(FROM_UNIXTIME(store_staff_log.released_at)) > ?)
            AND store_staff_log.is_leader=1";

        $result = $db->query($sql, array($to, $from));

        $list = array();

        foreach ($result as $key => $value) {
            if (!isset($list[$value['store_id']]))
                $list[$value['store_id']] = '';
            $list[$value['store_id']] .= $value['firstname'] . ' ' . $value['lastname'] . ', ';
        }

        return $list;
    }

    /**
     *   Lấy danh sách sale + pg đứng shop
     */
    public function get_sales_list($store_id, $time) {
        $where   = array();
        $where[] = $this->getAdapter()->quoteInto('store_id = ?', $store_id);

        $where[] = $this->getAdapter()->quoteInto(
                'FROM_UNIXTIME(joined_at, \'%Y-%m-%d\') <= ?', date('Y-m-d', strtotime($time))
        );

        $where[] = $this->getAdapter()->quoteInto(
                'released_at IS NULL OR released_at = 0 OR FROM_UNIXTIME(released_at, \'%Y-%m-%d\') > ?', date('Y-m-d', strtotime($time))
        );

        $result = $this->fetchAll($where);

        return $result && intval($result['staff_id']) > 0 ? $result : false;
    }

    /**
     *   Lấy tất cả danh sách sales đứng shop
     */
    public function get_list_sale($store_id, $time) {
        $db     = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('p' => $this->_name), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id, s.firstname, s.lastname, p.is_leader')));

        $select->joinLeft(array('s' => 'staff'), 'p.staff_id = s.id', array('firstname'    => 's.firstname',
            'lastname'     => 's.lastname',
            'email'        => 's.email',
            'phone_number' => 's.phone_number',
            'title'        => 's.title',
            'staff_id'     => 's.id'));

        $select->joinLeft(array('t' => 'team'), 't.id = s.title', array('title_name' => 't.name'));

        $select->where('FROM_UNIXTIME(p.joined_at, \'%Y-%m-%d\') <= ?', date('Y-m-d', strtotime($time)));
        $select->where('p.released_at IS NULL OR p.released_at = 0 OR FROM_UNIXTIME(released_at, \'%Y-%m-%d\') > ?', date('Y-m-d', strtotime($time)));

        if (!empty($store_id)) {
            if (is_array($store_id) && count($store_id) > 0) {
                $select->where('p.store_id IN (?)', $store_id);
            } else {
                $select->where('p.store_id = ?', $store_id);
            }
        }
        $select->where('s.title IN (?)', array(PGPB_TITLE, SALES_TITLE, CHUYEN_VIEN_BAN_HANG_TITLE, PB_SALES_TITLE, STORE_LEADER_TITLE, PG_BRANDSHOP));

        if (!empty($_GET['dev'])) {
            echo "<pre>";
            print_r($select->__toString());
            exit;
        }
        $result = $db->fetchAll($select);
        return $result;
    }

    /**
     *   Lấy tất cả danh sách sales leader đứng shop
     */
    public function get_list_sale_leader($store_id, $time) {
        $db     = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('p' => 'store_leader_log'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id, s.firstname, s.lastname')));

        $select->joinLeft(array('s' => 'staff'), 'p.staff_id = s.id', array('firstname'    => 's.firstname',
            'lastname'     => 's.lastname',
            'email'        => 's.email',
            'phone_number' => 's.phone_number',
            'title'        => 's.title',
            'staff_id'     => 's.id'));

        $select->joinLeft(array('t' => 'team'), 't.id = s.title', array('title_name' => 't.name'));

        $select->where('FROM_UNIXTIME(p.joined_at, \'%Y-%m-%d\') <= ?', date('Y-m-d', strtotime($time)));
        $select->where('p.released_at IS NULL OR p.released_at = 0 OR FROM_UNIXTIME(released_at, \'%Y-%m-%d\') > ?', date('Y-m-d', strtotime($time)));

        if (is_array($store_id) && count($store_id) > 0) {
            $select->where('p.store_id IN (?)', $store_id);
        } else {
            $select->where('p.store_id = ?', $store_id);
        }

        $result = $db->fetchAll($select);



        return $result;
    }

    public function findPrimaykey($id) {
        return $this->fetchRow($this->select()->where('id = ?', $id));
    }

    public function findStaffId($id) {
        return $this->fetchRow($this->select()->where('staff_id = ?', $id));
    }

    public function checkSales($staff_id) {
        $db = Zend_Registry::get('db');

        $sql  = '
             SELECT count(*) n
             FROM store_staff_log 
             WHERE staff_id = :staff_id AND released_at IS NULL
             GROUP BY staff_id
        ';
        $stmt = $db->prepare($sql);
        $stmt->bindParam("staff_id", $staff_id, PDO::PARAM_STR);
        $stmt->execute();
        $data = $stmt->fetch();
        $stmt->closeCursor();
        $db   = $stmt = null;
        return $data;
    }

    public function get_staff_by_sales_leader($store_id) {
        $db = Zend_Registry::get('db');

        $sql  = '
             SELECT staff_id FROM store_staff_log WHERE store_id = :store_id AND released_at IS NULL AND is_leader = 1
        ';
        $stmt = $db->prepare($sql);
        $stmt->bindParam("store_id", $store_id, PDO::PARAM_STR);
        $stmt->execute();
        $data = $stmt->fetch();
        $stmt->closeCursor();
        $db   = $stmt = null;
        return $data['staff_id'];
    }

    public function getStore($staff_id) {
        $db = Zend_Registry::get('db');

        $sql  = '
             SELECT s1.`name`
            FROM store_staff s
            JOIN store s1
                ON s.store_id = s1.id
            WHERE s.staff_id = :staff_id  AND s1.is_brand_shop = 1

        ';
        $stmt = $db->prepare($sql);
        $stmt->bindParam("staff_id", $staff_id, PDO::PARAM_STR);
        $stmt->execute();
        $data = $stmt->fetch();
        $stmt->closeCursor();
        $db   = $stmt = null;
        return $data['name'];
    }

    public function pgTgdd($store_id) {
        $db = Zend_Registry::get('db');

        $sql  = '
             SELECT s.id,s.staff_id,s.is_kpi,s1.`code`,FROM_UNIXTIME(s.joined_at,"%d/%m/%Y") from_date,CONCAT(s1.firstname," ",s1.lastname) `name`
            FROM store_staff_log s
            JOIN staff s1
                ON s.staff_id = s1.id
            WHERE s.store_id= :store_id AND s.released_at is null and s.is_leader = 0

        ';
        $stmt = $db->prepare($sql);
        $stmt->bindParam("store_id", $store_id, PDO::PARAM_STR);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $db   = $stmt = null;
        return $data;
    }

    public function checkPgTgdd($id) {
        $db = Zend_Registry::get('db');

        $sql  = '
             SELECT s.is_tgdd
             FROM store_staff_log a
             join store s
                on a.store_id = s.id
             WHERE staff_id = :staff_id AND released_at IS NULL AND s.is_tgdd = 1

        ';
        $stmt = $db->prepare($sql);
        $stmt->bindParam("staff_id", $id, PDO::PARAM_STR);
        $stmt->execute();
        $data = $stmt->fetch();
        $stmt->closeCursor();
        $db   = $stmt = null;
        return $data;
    }

    public function infoStaff($id) {
        $db = Zend_Registry::get('db');

        $sql  = '
             SELECT CONCAT(s.firstname," ",s.lastname) `name` ,s.`code`,s.email
             FROM store_staff_log a
             join staff s
                on a.id = :id
             and s.id = a.staff_id

        ';
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id, PDO::PARAM_STR);
        $stmt->execute();
        $data = $stmt->fetch();
        $stmt->closeCursor();
        $db   = $stmt = null;
        return $data;
    }

    public function checkSetKPI($id) {
        $db = Zend_Registry::get('db');

        $sql  = '
            SELECT CONCAT(s2.firstname," ",s2.lastname) staff,s1.`name` store,s2.id staff_id
             FROM store_staff_log s 
            join store s1
                on s1.id = s.store_id
            join staff s2
                on s2.id = s.staff_id
             where s.released_at is null and s.store_id = :store_id and s.is_kpi = 1 and s.is_leader = 0
             

        ';
        $stmt = $db->prepare($sql);
        $stmt->bindParam("store_id", $id, PDO::PARAM_STR);
        $stmt->execute();
        $data = $stmt->fetch();
        $stmt->closeCursor();
        $db   = $stmt = null;
        return $data;
    }

    public function getStoreByStaffAndTitle($staff_id, $old_title, $new_title, $old_province, $new_province) {
        $data               = [];
        $db                 = Zend_Registry::get('db');
        if (($old_title <> $new_title || $old_province <> $new_province)
                && (!in_array($old_province, array(4212, 4213, 3417)) || !in_array($new_province, array(4212, 4213, 3417))) 
                && (!in_array($old_title, array(182, 293, 419, 420)) || !in_array($new_title, array(182, 293, 419, 420)) || $old_province <> $new_province) ) {
            $sql  = 'SELECT * FROM store_staff_log s 
             where s.released_at is null and s.staff_id=:staff_id';
            $stmt = $db->prepare($sql);
            $stmt->bindParam("staff_id", $staff_id, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll();
            $stmt->closeCursor();
            $db   = $stmt = null;
        }
        return $data;
    }
    
    public function checkReleasedStoreByStaffId($staff_id){
        $db                 = Zend_Registry::get('db');
        $sql  = 'SELECT * FROM store_staff_log s 
             where s.released_at is null and s.staff_id=:staff_id';
        $stmt = $db->prepare($sql);
        $stmt->bindParam("staff_id", $staff_id, PDO::PARAM_INT);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $db   = $stmt = null;
        if(!empty($data)){
            return 1;
        }
        return 0;
    }

    public function getStoreByStaff($staff_id) {
        $db = Zend_Registry::get('db');

        $sql = " SELECT s2.id AS store_id, s2.name 
                 FROM `store_staff_log` AS s1
                 INNER JOIN store AS s2 ON s1.store_id = s2.id AND s2.is_sumup_daily = 1
                 WHERE s1.staff_id = $staff_id 
                 AND s1.released_at IS NULL 
                 AND s1.is_leader IN (0, 1) ";

        $stmt   = $db->prepare($sql);
        $stmt->execute();
        $stores = $stmt->fetchAll();
        $stmt->closeCursor();

        return $stores;
    }
	
	    /**
     * Kiểm tra xem chấm công vào lúc timing_time, tại cửa hàng store_id có...
     *     ...sale cua realme hay khong
     * neu co thi khong cho sale cua oppo cham va nguoc lai
     * phat sinh khi cho sale oppo cham may cua realme
     * @param  int  $store_id    - id cửa hàng
     * @param  datetime  $timing_time - thời gian của chấm công (timing for)
     *                            false: là PG
     *                            NULL: không quan tâm, chỉ cần biết nó thuộc cửa hàng
     * @return boolean              - true: co sale realme;
     *                                false: khong co sale realme
     */
    public function checkRealmeSale( $store_id, $timing_time) {
        $db          = Zend_Registry::get('db');
        $select = $db->select()->from(array('st' => 'hr.store'), array(''))->where('st.id = ?', $store_id);
        $select->joinLeft(array('d'=> 'warehouse.distributor'), 'st.d_id = d.id',array());
        $select->joinLeft(array('dmo'=> 'realme_warehouse.distributor_mapping_oppo'), 'd.id = dmo.id_oppo',array());
        $select->joinLeft(array('rmd'=> 'realme_warehouse.distributor'), 'dmo.id_rm = rmd.id',array());
        $select->joinLeft(array('rms'=> 'realme_hr.store'), 'rms.d_id = rmd.id',array());
        $select->joinLeft(array('rmssl'=> 'realme_hr.store_staff_log'), 'rmssl.store_id = rms.id AND rmssl.is_leader = 1',array('rmssl.staff_id'));

        $select->where('FROM_UNIXTIME(rmssl.joined_at, \'%Y-%m-%d\') <= ?', date('Y-m-d', strtotime($timing_time)));
        $select->where('rmssl.released_at IS NULL OR rmssl.released_at = 0 OR FROM_UNIXTIME(rmssl.released_at, \'%Y-%m-%d\') > ?', date('Y-m-d', strtotime($timing_time)));


        $result       = $db->fetchOne($select);
        

        return count($result['staff_id']) ? true : false;
    }

    public function getListStore($staff_id, $is_leader, $date)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['l' => 'store_staff_log'], [
                'l.store_id',
                'store_name' => 's.name',
                'r.area_id'
            ])
            ->join(['s' => 'store'], 'l.store_id = s.id  AND (s.is_brand_shop = 0 OR s.is_brand_shop IS NULL)', [])
            ->join(['di' => WAREHOUSE_DB. '.distributor'], 's.d_id = di.id', [])
            ->join(['d' => 'regional_market'], 'd.id = s.district', [])
            ->join(['r' => 'regional_market'], 'd.parent = r.id', [])
            ->where('l.staff_id = ?', $staff_id)
            ->where('di.is_ka = ?', 0)
            ->where('l.is_leader = ?', $is_leader)
            ->where("FROM_UNIXTIME(l.joined_at, '%Y-%m-%d') <= ?", $date)
            ->where("l.released_at IS NULL OR  FROM_UNIXTIME(l.released_at, '%Y-%m-%d') > ?", $date);

        $result = $db->fetchAll($select);

        foreach ($result as $item) {
            $list [$item['store_id']] = $item;
        }

        return $list;
    }

    public function getStoreStaff($staff_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['l' => DATABASE_CENTER . '.store_staff_log'], [
                'store_name' => 's.name',
                'l.store_id',
                'v.channel_id'
            ])
            ->joinLeft(['s' => DATABASE_CENTER . '.store'], 'l.store_id = s.id', [])
            ->joinLeft(['v' => DATABASE_CENTER . '.v_channel_dealer'], 's.d_id = v.d_id', [])
            ->where('l.released_at IS NULL')
            ->where('l.is_leader = ?', 0)
            ->where('l.staff_id = ?', $staff_id);

        $result = $db->fetchAll($select);

        return $result;
    }

}
