<?php
class Application_Model_InsuranceStaffChange extends Zend_Db_Table_Abstract
{
    protected $_name = 'insurance_staff_change';

    /**
     * @param $before
     * @param $after
     * @param $staff_id
     * sử dụng ở staff/save và staff/approve-basic
     */
    public function save($before,$after,$staff_id){
        $data_before = array();
        $data_after = array();

        if(trim($before['dob']) != trim($after['dob']) ){
            $data_before['dob'] = trim($before['dob']);
            $data_after['dob']  = trim($after['dob']);
        }

        if(trim($before['ID_number']) != trim($after['ID_number']) ){
            $data_before['ID_number'] = trim($before['ID_number']);
            $data_after['ID_number']  = trim($after['ID_number']);
        }

        if(trim($before['firstname']).'-'.trim($before['lastname']) != trim($after['firstname']).'-'.trim($after['lastname']) ){
            $data_before['name'] = trim($before['firstname']).' '.trim($before['lastname']);
            $data_after['name']  = trim($after['firstname']).' '.trim($after['lastname']);
        }

        if($before['gender'] != $after['gender']){
            $data_before['gender'] = $before['gender'];
            $data_after['gender']  = $after['gender'];
        }

        //Nếu đã đủ điều kiện đóng bảo hiểm
        if( in_array($after['contract_term'],array(1,6,7))   ){
            $data = array(
                'staff_id'   => $staff_id,
                'before'     => serialize($data_before),
                'after'      => serialize($data_after),
                'created_at' => date('Y-m-d H:i:s'),
            );

            if(serialize($data_before) != serialize($data_after)){
                $this->insert($data);
            }

        }

    }

    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');
        $cols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.id'),
            'a.*',
            'code' => 'b.code',
            'staff_name' => 'CONCAT(b.firstname," ",b.lastname)'
        );
        $select = $db->select()
            ->from(array('a'=> $this->_name),$cols)
            ->join(array('b'=> 'staff'),'a.staff_id = b.id',array())
            ->where('a.before != a.after')
            ->order('a.created_at DESC')
        ;

        if(isset($limit) AND $limit){
            $select->limitPage($page,$limit);
        }

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public function approve($id,$created_at = NULL){
        $row = $this->find($id)->current();
        if($row){
            $before = unserialize($row['before']);
            $after = unserialize($row['after']);
            $created_at = ($created_at) ? $created_at : date('Y-m-d H:i:s');
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $created_by = $userStorage->id;

            $QInsuranceInfo = new Application_Model_InsuranceInfo();
            $where = $QInsuranceInfo->getAdapter()->quoteInto('staff_id = ?',$row->staff_id);
            $QInsuranceInfo->update($after,$where);

            $QInsuranceLogInfo = new Application_Model_InsuranceLogInfo();
            $QInsuranceLogInfo->save($before,$after,$row['staff_id'],$created_by,$created_at);
        }

    }

}