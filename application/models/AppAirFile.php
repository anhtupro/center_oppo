<?php
class Application_Model_AppAirFile extends Zend_Db_Table_Abstract
{
	protected $_name = 'app_air_file';
	    protected $_schema = DATABASE_TRADE;
	function getImg($id){
         $db = Zend_Registry::get('db');
          $select = $db->select();
           $select->from(array('a' => DATABASE_TRADE.'.app_air_file'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.id'), 'a.url'));
            $select->where('air_id = ?',$id);
            $select->where('type = ?',1);
            $img = $db->fetchAll($select);
            return $img;
    }
    function getImgResult($id){
         $db = Zend_Registry::get('db');
          $select = $db->select();
           $select->from(array('a' => DATABASE_TRADE.'.app_air_file'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.id'), 'a.url'));
            $select->where('air_id = ?',$id); 
            $select->where('type = ?',3);
            $result = $db->fetchAll($select);
            return $result;
    }

    function getFileDecord($id){
         $db = Zend_Registry::get('db');
          $select = $db->select();
           $select->from(array('a' => DATABASE_TRADE.'.app_air_file'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.id'), 'a.url'));
            $select->where('air_id = ?',$id); 
            $select->where('type = ?',2);
            $result = $db->fetchAll($select);
            return $result;
    }
}