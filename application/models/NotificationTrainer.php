<?php
class Application_Model_NotificationTrainer extends Zend_Db_Table_Abstract
{
	protected $_name = 'notification_trainer';
	
        public function getTrainerNotification($params) {
            $db     = Zend_Registry::get("db");
            $select = $db->select();

            $arrCols = array(
                "trainer_id" => "p.id", 
                "p.title", 
                "p.regional_market", 
                "p.email"
            );

            $select->from(array('p' => 'staff'), $arrCols);
            $select->where('p.title IN (?)', [317, 175]);
            $select->where('p.regional_market = ?', $params['regional_market']);
            $select->where('p.off_date IS NULL', NULL);

            $result = $db->fetchAll($select);

            return $result;
        }
        
        public function getNotificationTrainer($params){
            $db = Zend_Registry::get("db");
            $select = $db->select();

            $arrCols = array(
                "p.id", 
                "p.staff_id_read", 
                "p.staff_id", 
                "p.status", 
                "p.created_at", 
                "p.note" 
            );

            $select->from(array('p' => 'notification_trainer'), $arrCols);
            $select->where('p.staff_id_read = ?', $params['staff_id_read']);
            $select->where('p.status <>  ?', 1);

            $result = $db->fetchAll($select);

            return $result;
        }
}