<?php
class Application_Model_ImeiKpi extends Zend_Db_Table_Abstract
{
    protected $_name       = 'imei_kpi';
    protected $_active     = null;
    protected $_status     = null;
    protected $_active_all = true;

    public function __construct($type = 1) {
        if ($type == 2 || !$this->_active_all) {
            $this->_active = ' i.activation_date IS NOT NULL AND i.activation_date <> 0 ';
            $this->_status = ' i.`status` IS NOT NULL AND i.`status` > 0 ';

        } else {
            $this->_active = ' 1 ';
            $this->_status = ' 1 ';
        }

        parent::__construct();
    }

    /**
     * Láº¥y tá»•ng doanh sá»‘ cá»§a má»™t Ä‘á»‘i tÆ°á»£ng trong khoáº£ng thá»i gian
     * @param  My_Kpi_Type $type Loáº¡i dá»¯ liá»‡u (sell out, active...)
     * @param  array $params cÃ¡c tham sá»‘
     * @return int Doanh sá»‘
     */
    public function getTotal($type, array $params)
    {
        $total = 0;

        $db = Zend_Registry::get('db');
        $select = $db->select();

        switch ($type) {
            case My_Kpi_Type::SellOut:
                $type_mysql_string = 'COUNT(DISTINCT i.imei_sn)';
                break;
            case My_Kpi_Type::Value:
                $type_mysql_string = 'SUM(i.value) * 0.8';
                break;
            case My_Kpi_Type::Activated:
                $type_mysql_string = 'COUNT(DISTINCT i.imei_sn)';
                $params['activated'] = true;
                break;
            case My_Kpi_Type::Activation:
                $type_mysql_string = 'COUNT(DISTINCT i.imei_sn)';

                if (isset($params['from']) && $params['from']) {
                    $params['activation_from'] = $params['from'];
                    unset($params['from']);
                }

                if (isset($params['to']) && $params['to']){
                    $params['activation_to'] = $params['to'];
                    unset($params['to']);
                }
                break;

            default:
                throw new Exception("Invalid KPI type", 2);
                break;
        }

        $select->from(array('i' => $this->_name), array('total' => $type_mysql_string));

        if (isset($params['from']) && $params['from'])
            $select->where('DATE(i.timing_date) >= ?', $params['from']);

        if (isset($params['to']) && $params['to'])
            $select->where('DATE(i.timing_date) <= ?', $params['to']);

        if (isset($params['activated']) && $params['activated'])
            $select->where('i.activation_date IS NOT NULL AND i.activation_date <> 0 AND i.activation_date <> \'\'', 1);

        if (isset($params['activation_from']) && $params['activation_from'])
            $select->where('DATE(i.activation_date) >= ?', $params['activation_from']);

        if (isset($params['activation_to']) && $params['activation_to'])
            $select->where('DATE(i.activation_date) <= ?', $params['activation_to']);

        if (isset($params['pg']) && $params['pg'])
            $select->where('i.pg_id = ?', intval($params['pg']));

        if (isset($params['sale']) && $params['sale'])
            $select->where('i.sale_id = ?', intval($params['sale']));

        if (isset($params['leader']) && $params['leader'])
            $select->where('i.leader_id = ?', intval($params['leader']));

        if (isset($params['area']) && $params['area'])
            $select->where('i.area_id = ?', intval($params['area']));

        if (isset($params['district']) && $params['district'])
            $select->where('i.district_id = ?', intval($params['district']));

        if (isset($params['province']) && $params['province'])
            $select->where('i.province_id = ?', intval($params['province']));

        if (isset($params['store']) && $params['store'])
            $select->where('i.store_id = ?', intval($params['store']));

        if (isset($params['good']) && $params['good'])
            $select->where('i.good_id = ?', intval($params['good']));

        if (isset($params['color']) && $params['color'])
            $select->where('i.color_id = ?', intval($params['color']));

        if (isset($params['dealer']) && $params['dealer'])
            $select->where('i.distributor_id = ?', intval($params['dealer']));

        $total = $db->fetchOne($select);

        return $total;
    }

    /**
     * Láº¥y doanh sá»‘ chi tiáº¿t theo loáº¡i Ä‘á»‘i tÆ°á»£ng, tráº£ vá» máº£ng, má»—i pháº§n tá»­ lÃ  doanh sá»‘ cá»§a má»™t model
     * @param  int $object_id   ID cá»§a Ä‘á»‘i tÆ°á»£ng
     * @param  My_Kpi_Object $object_type Loáº¡i Ä‘á»‘i tÆ°á»£ng (sale, pg, store...)
     * @param  date $from        Tá»« ngÃ y
     * @param  date $to          Äáº¿n ngÃ y
     * @param  My_Kpi_Type $object_type Loáº¡i Ä‘á»‘i tÆ°á»£ng (sale, pg, store...)
     * @return array              Máº£ng array(good_id => sell_out)
     */
    public function getTotalByModel($type, $params)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('i' => $this->_name), array('i.good_id', 'total' => 'COUNT(DISTINCT i.imei_sn)'));

        switch ($type) {
            case My_Kpi_Type::SellOut:

                break;
            case My_Kpi_Type::Activated:
                $select->where('i.activation_date IS NOT NULL AND i.activation_date <> 0', 1);
                break;
            case My_Kpi_Type::Not_Activated:
                $select->where('i.activation_date IS NULL OR i.activation_date = 0', 1);
                break;
            default:

                break;
        }

        if (isset($params['from']) && $params['from'])
            $select->where('DATE(i.timing_date) >= ?', $params['from']);

        if (isset($params['to']) && $params['to'])
            $select->where('DATE(i.timing_date) <= ?', $params['to']);

        if (isset($params['pg']) && $params['pg'])
            $select->where('i.pg_id = ?', intval($params['pg']));

        if (isset($params['sale']) && $params['sale'])
            $select->where('i.sale_id = ?', intval($params['sale']));

        if (isset($params['leader']) && $params['leader'])
            $select->where('i.leader_id = ?', intval($params['leader']));

        if (isset($params['area']) && $params['area'])
            $select->where('i.area_id = ?', intval($params['area']));

        if (isset($params['district']) && $params['district'])
            $select->where('i.district_id = ?', intval($params['district']));

        if (isset($params['province']) && $params['province'])
            $select->where('i.province_id = ?', intval($params['province']));

        if (isset($params['store']) && $params['store'])
            $select->where('i.store_id = ?', intval($params['store']));

        if (isset($params['dealer']) && $params['dealer'])
            $select->where('i.distributor_id = ?', intval($params['dealer']));

        $select->group('i.good_id');

        $sell_out = array();
        $result = $db->fetchAll($select);

        if ($result)
            foreach ($result as $key => $item)
                $sell_out[ $item['good_id'] ] = isset($item['total']) ? $item['total'] : 0;

        return $sell_out;
    }

    /**
     * Láº¥y dá»¯ liá»‡u hiá»ƒn thá»‹ dáº¡ng báº£ng, cho tá»«ng ngÆ°á»i cá»¥ thá»ƒ xem, hoáº·c HR kiá»ƒm tra KPI
     * @param  [type] $type   [description]
     * @param  [type] $params [description]
     * @return [type]         [description]
     */
    public function fetchGrid($params)
    {
        if (!isset($params['from']) || !$params['from'] || !strtotime($params['from'])){
            return false;
        }

        if (!isset($params['to']) || !$params['to'] || !strtotime($params['to'])){
            return false;
        }

        $db = Zend_Registry::get('db');
        $select = $db->select();
        $before_closing = My_Kpi::isBeforeClosingDate($params['to']);

        if (!$before_closing) {
            $get = array(
                'good_id'       => 'g.good_id', 
                'color_id'      => 'g.color_id',
                'quantity'      => 'COUNT(DISTINCT i.imei_sn)',
                'total_value'   => 'SUM(i.value)',
                'activated'     => new Zend_Db_Expr('SUM(CASE WHEN '.$this->_active.' THEN 1 ELSE 0 END)'),
                'non_activated' => new Zend_Db_Expr('SUM(CASE WHEN '.$this->_active.' THEN 0 ELSE 1 END)'),
            );
        } else {
            $get = array(
                'good_id'       => 'g.good_id', 
                'color_id'      => 'g.color_id',
                'quantity'      => 'COUNT(DISTINCT i.imei_sn)',
                'total_value'   => 'SUM(i.value)',
                'activated'     => new Zend_Db_Expr('SUM(CASE WHEN ' .$this->_status. ' THEN 1 ELSE 0 END)'),
                'non_activated' => new Zend_Db_Expr('SUM(CASE WHEN '.$this->_status.' THEN 0 ELSE 1 END)'),
            );
        }

        $group = array();

        if (isset($params['pg'])) {

            if (isset($params['kpi']) && $params['kpi'])
                $get['staff_id'] = 'i.pg_id';

            if (!$before_closing) {
                $get['kpi'] = new Zend_Db_Expr('SUM(CASE WHEN '.$this->_active.'
                   AND i.pg_id <> i.sale_id AND i.pg_id <> i.leader_id
                   THEN i.kpi_pg
                   ELSE 0 END)');
            } else {
                $get['kpi'] = new Zend_Db_Expr('SUM(CASE WHEN  '.$this->_status.'
                   AND i.pg_id <> i.sale_id AND i.pg_id <> i.leader_id
                   THEN i.kpi_pg
                   ELSE 0 END)');
            }

            if (isset($params['kpi']) && 2 == $params['kpi']) {
                $get['kpi_support'] = new Zend_Db_Expr(sprintf(
                    "fn_calc_kpi_support_for_pg(i.pg_id, '%s', '%s', %s)",
                    $params['from'],
                    $params['to'],
                    $get['kpi']
                ));
            }
        } elseif (isset($params['sale'])) {
            if (isset($params['kpi']) && $params['kpi']){
                $get['staff_id'] = 'i.sale_id';
            }

            if (!$before_closing) {
                $get['kpi'] = new Zend_Db_Expr('SUM(CASE WHEN '.$this->_active.'
                   THEN i.kpi_sale
                   ELSE 0 END)');
            } else {
                $get['kpi'] = new Zend_Db_Expr('SUM(CASE WHEN '.$this->_status.'
                   THEN i.kpi_sale
                   ELSE 0 END)');
            }
        } elseif (isset($params['leader'])) {
            if (isset($params['kpi']) && $params['kpi'])
                $get['staff_id'] = 'i.leader_id';

            if (!$before_closing) {
                $get['kpi'] = new Zend_Db_Expr('SUM(CASE WHEN '.$this->_active.'
                        THEN (i.`value` * 0.8 * 0.002)
                    ELSE 0 END)');
            } else {
                $get['kpi'] = new Zend_Db_Expr('SUM(CASE WHEN '.$this->_status.'
                        THEN (i.`value` * 0.8 * 0.002)
                    ELSE 0 END)');
            }
        } elseif(isset($params['pb_sale'])){

            if (isset($params['kpi']) && $params['kpi']){
                $get['staff_id'] = 'i.pb_sale_id';
            }

            if (!$before_closing) {
                $get['kpi'] = new Zend_Db_Expr('SUM(CASE WHEN '.$this->_active.'
                   THEN i.kpi_pb_sale
                   ELSE 0 END)');
            } else {
                $get['kpi'] = new Zend_Db_Expr('SUM(CASE WHEN '.$this->_status.'
                   THEN i.kpi_pb_sale
                   ELSE 0 END)');
            }
        }
        
        $select->from(array('i' => $this->_name), $get);
        $select->where('DATE(i.timing_date) >= ?', $params['from']);
        $select->where('DATE(i.timing_date) <= ?', $params['to']);
        
        if (isset($params['pg']) && $params['pg']) {

            $select->where('i.pg_id <> i.sale_id AND i.pg_id <> i.leader_id', 1);
            if (!isset($params['kpi']) || !$params['kpi']) {
                $select->where('i.pg_id = ?', intval($params['pg']));

            } else {
                if (is_array($params['pg']) && count($params['pg']))
                    $select->where('i.pg_id IN (?)', $params['pg']);
                else
                    $select->where('1=0', 1);

                $group = array('i.pg_id');
            }

            $select->where('g.type = ?', My_Kpi_Object::Pg);

        } elseif (isset($params['sale']) && $params['sale']) {
            if (!isset($params['kpi']) || !$params['kpi']) {
                $select->where('i.sale_id = ?', intval($params['sale']));

            } else {
                if (is_array($params['sale']) && count($params['sale']))
                    $select->where('i.sale_id IN (?)', $params['sale']);
                else
                    $select->where('1=0', 1);

                $group = array('i.sale_id');
            }

            $select->where('g.type = ?', My_Kpi_Object::Sale);

        } elseif (isset($params['leader']) && $params['leader']) {
            if (!isset($params['kpi']) || !$params['kpi']) {
                $select->where('i.leader_id = ?', intval($params['leader']));

            } else {
                if (is_array($params['leader']) && count($params['leader']))
                    $select->where('i.leader_id IN (?)', $params['leader']);
                else
                    $select->where('1=0', 1);

                $group = array('i.leader_id');
            }
        } elseif (isset($params['pb_sale']) && $params['pb_sale']) {
            if (!isset($params['kpi']) || !$params['kpi']) {
                $select->where('i.pb_sale_id = ?', intval($params['pb_sale']));
            } else {
                if (is_array($params['pb_sale']) && count($params['pb_sale']))
                    $select->where('i.pb_sale_id IN (?)', $params['pb_sale']);
                else
                    $select->where('1=0', 1);

                $group = array('i.pb_sale_id');
            }

            $select->where('g.type = ?', My_Kpi_Object::Pb_sale);

        }

        if (isset($params['area']) && $params['area']) {
            if (is_array($params['area']) && count($params['area']))
                    $select->where('i.area_id IN (?)', $params['area']);
            elseif (is_numeric($params['area']))
                    $select->where('i.area_id = ?', intval($params['area']));
            else
                $select->where('1=0', 1);
        }

        if (isset($params['district']) && $params['district']) {
            if (is_array($params['district']) && count($params['district']))
                    $select->where('i.district_id IN (?)', $params['district']);
            elseif (is_numeric($params['district']))
                    $select->where('i.district_id = ?', intval($params['district']));
            else
                $select->where('1=0', 1);
        }

        if (isset($params['province']) && $params['province']) {
            if (is_array($params['province']) && count($params['province']))
                    $select->where('i.province_id IN (?)', $params['province']);
            elseif (is_numeric($params['province']))
                    $select->where('i.province_id = ?', intval($params['province']));
            else
                $select->where('1=0', 1);
        }

        if (isset($params['store']) && $params['store']) {
            if (is_array($params['store']) && count($params['store']))
                    $select->where('i.store_id IN (?)', $params['store']);
            elseif (is_numeric($params['store']))
                    $select->where('i.store_id = ?', intval($params['store']));
            else
                $select->where('1=0', 1);
        }

        if (isset($params['dealer']) && $params['dealer']) {
            if (is_array($params['dealer']) && count($params['dealer']))
                    $select->where('i.distributor_id IN (?)', $params['dealer']);
            elseif (is_numeric($params['dealer']))
                    $select->where('i.distributor_id = ?', intval($params['dealer']));
            else
                $select->where('1=0', 1);
        }

        if (isset($params['distributor_id']) && $params['distributor_id']) {
            $select->join(array('s' => 'store'), 's.id=i.store_id', array());

            if (is_array($params['distributor_id']) && count($params['distributor_id']))
                    $select->where('s.d_id IN (?)', $params['distributor_id']);
            elseif (is_numeric($params['distributor_id']))
                    $select->where('s.d_id = ?', intval($params['distributor_id']));
            else
                $select->where('1=0', 1);
        }
      
        // Leader thÃ¬ láº¥y KPI trÃªn % giÃ¡ bÃ¡n
        if (isset($params['leader']) && intval($params['leader'])) {
            $join_get = array(
                'from_date' => new Zend_Db_Expr(sprintf("CASE WHEN IFNULL(g.from_date,g2.from_date) < '%s' THEN '%s' ELSE IFNULL(g.from_date,g2.from_date) END", $params['from'], $params['from'])),
                'to_date' => new Zend_Db_Expr(sprintf("CASE WHEN IFNULL(g.to_date,g2.to_date) IS NULL OR IFNULL(g.to_date,g2.to_date) > '%s' THEN '%s' ELSE IFNULL(g.to_date,g2.to_date) END", $params['to'], $params['to'])),
                'kpi_unit' => new Zend_Db_Expr('IFNULL(g.price,g2.price) * 0.002 * 0.8'),
            );

            /*
            $select->join(
                array('g' => WAREHOUSE_DB.'.good_price_log'),
                'g.good_id=i.good_id
                AND
                    CASE WHEN g.color_id <> 0 AND g.color_id IS NOT NULL THEN i.color_id=g.color_id
                    ELSE i.color_id NOT IN (
                            SELECT '.WAREHOUSE_DB.'.`good_price_log`.color_id
                            FROM '.WAREHOUSE_DB.'.`good_price_log`
                            WHERE '.WAREHOUSE_DB.'.`good_price_log`.color_id <> 0
                            AND '.WAREHOUSE_DB.'.`good_price_log`.good_id = i.good_id
                            AND '.WAREHOUSE_DB.'.`good_price_log`.from_date <= DATE(i.timing_date)
                            AND ('.WAREHOUSE_DB.'.`good_price_log`.to_date IS NULL OR '.WAREHOUSE_DB.'.`good_price_log`.to_date = 0 OR '.WAREHOUSE_DB.'.`good_price_log`.to_date >= DATE(i.timing_date))
                        )
                    END
                AND g.from_date<= DATE(i.timing_date)
                AND ( g.to_date IS NULL OR g.to_date = 0 OR g.to_date >= DATE(i.timing_date) )',
                $join_get
            );
            */
           
            $select->joinLeft(
                array('g' => WAREHOUSE_DB.'.good_price_log'),
                '   g.good_id=i.good_id
                    AND IFNULL(g.color_id,0)  <> 0
                    AND IFNULL(g.color_id,0) = i.color_id
                    AND g.from_date <= DATE(i.timing_date)
                    AND ( g.to_date IS NULL OR g.to_date = 0 OR g.to_date >= DATE(i.timing_date) )
                ',
                $join_get
            );

            $select->joinLeft(
                array('g2' => WAREHOUSE_DB.'.good_price_log'),
                '   g2.good_id=i.good_id
                    AND IFNULL(g2.color_id,0)  = 0
                    AND g2.from_date <= DATE(i.timing_date)
                    AND ( g2.to_date IS NULL OR g2.to_date = 0 OR g2.to_date >= DATE(i.timing_date) )
                    ',
                array()
            );

        } else {
            $join_get = array(
                'from_date' => new Zend_Db_Expr(sprintf("CASE WHEN g.from_date < '%s' THEN '%s' ELSE g.from_date END", $params['from'], $params['from'])),
                'to_date'   => new Zend_Db_Expr(sprintf("CASE WHEN g.to_date IS NULL OR g.to_date > '%s' THEN '%s' ELSE g.to_date END", $params['to'], $params['to'])),
                'kpi_unit'  => 'g.kpi',
            );

            $select->join(
                array('g' => 'good_kpi_log'),
                'g.good_id=i.good_id
                AND
                    CASE WHEN g.color_id <> 0 AND g.color_id IS NOT NULL THEN i.color_id=g.color_id
                    ELSE i.color_id NOT IN (
                            SELECT good_kpi_log.color_id
                            FROM `good_kpi_log`
                            WHERE good_kpi_log.color_id <> 0
                            AND good_kpi_log.good_id = i.good_id
                            AND good_kpi_log.from_date <= DATE(i.timing_date)
                            AND (good_kpi_log.to_date IS NULL OR good_kpi_log.to_date = 0 OR good_kpi_log.to_date >= DATE(i.timing_date))
                        )
                    END
                AND g.from_date<= DATE(i.timing_date)
                AND ( g.to_date IS NULL OR g.to_date = 0 OR g.to_date >= DATE(i.timing_date) )
                AND ( IFNULL(g.type,0) IN (2) OR i.`kpi_pg` = g.`kpi` )
                AND ( IFNULL(g.type,0) IN (2) OR i.`kpi_area` = g.`area_id`)
                ',
                $join_get
            );

            if(isset($params['kpi_area_id'])){
                $select->where('kpi_area = ?',$params['kpi_area_id']);
            }
        }

        // $union = clone $select; // tháº±ng select thuá»™c loáº¡i tham chiáº¿u, clone ra Ä‘á»ƒ group nÃ³ tiáº¿p, union thÃ¬ khÃ´ng group
        $default_group = array('g.good_id', 'g.color_id', 'g.from_date');

        if (isset($params['kpi']) && 2 == $params['kpi']){
            $default_group = array();
        }

        $select->group( array_merge( $group,  $default_group) );

        if (isset($params['kpi']) && $params['kpi']) {
            PC::debug($select->__toString());
            return $db->fetchAll($select);
        } else {
            return $db->fetchAll($select);
        }
    }

    /**
     * @param  [type] $type   [description]
     * @param  [type] $params [description]
     * @return [type]         [description]
     */
    public function fetchByMonth($params)
    {
        if (!isset($params['from']) || !$params['from'] || !strtotime($params['from']))
            return false;

        if (!isset($params['to']) || !$params['to'] || !strtotime($params['to']))
            return false;

        $db = Zend_Registry::get('db');
        $select = $db->select();
        $before_closing = My_Kpi::isBeforeClosingDate($params['to']);

        if (!$before_closing) {
            $get = array(
                'good_id',
                'quantity'      => 'COUNT(DISTINCT i.imei_sn)',
                'total_value'   => 'SUM(i.value)',
                'activated'     => new Zend_Db_Expr('SUM(CASE WHEN ' . $this->_active . ' THEN 1 ELSE 0 END)'),
                'non_activated' => new Zend_Db_Expr('SUM(CASE WHEN ' . $this->_active . ' THEN 0 ELSE 1 END)'),
            );
        } else {
            $get = array(
                'good_id',
                'quantity'      => 'COUNT(DISTINCT i.imei_sn)',
                'total_value'   => 'SUM(i.value)',
                'activated'     => new Zend_Db_Expr('SUM(CASE WHEN ' . $this->_status . ' THEN 1 ELSE 0 END)'),
                'non_activated' => new Zend_Db_Expr('SUM(CASE WHEN ' . $this->_status . ' THEN 0 ELSE 1 END)'),
            );
        }

        $get = array_merge($get, array(
            'year' => new Zend_Db_Expr('YEAR(timing_date)'),
            'month' => new Zend_Db_Expr('MONTH(timing_date)'),
        ));

        $group = array();
        //
        if (isset($params['pg'])) {
            if (isset($params['kpi']) && $params['kpi'])
                $get['staff_id'] = 'i.pg_id';

            if (!$before_closing) {
                $get['kpi'] = new Zend_Db_Expr('SUM(CASE WHEN '.$this->_active.'
                   AND i.pg_id <> i.sale_id AND i.pg_id <> i.leader_id
                   THEN i.kpi_pg
                   ELSE 0 END)');
            } else {
                $get['kpi'] = new Zend_Db_Expr('SUM(CASE WHEN  '.$this->_status.'
                   AND i.pg_id <> i.sale_id AND i.pg_id <> i.leader_id
                   THEN i.kpi_pg
                   ELSE 0 END)');
            }

            if (isset($params['kpi']) && 2 == $params['kpi']) {
                $get['kpi_support'] = new Zend_Db_Expr(sprintf(
                    "fn_calc_kpi_support_for_pg(i.pg_id, '%s', '%s', %s)",
                    $params['from'],
                    $params['to'],
                    $get['kpi']
                ));
            }

        } elseif (isset($params['sale'])) {
            if (isset($params['kpi']) && $params['kpi'])
                $get['staff_id'] = 'i.sale_id';

            if (!$before_closing) {
                $get['kpi'] = new Zend_Db_Expr('SUM(CASE WHEN '.$this->_active.'
                   THEN i.kpi_sale
                   ELSE 0 END)');
            } else {
                $get['kpi'] = new Zend_Db_Expr('SUM(CASE WHEN '.$this->_status.'
                   THEN i.kpi_sale
                   ELSE 0 END)');
            }

        } elseif (isset($params['leader'])) {
            if (isset($params['kpi']) && $params['kpi'])
                $get['staff_id'] = 'i.leader_id';

            if (!$before_closing) {
                $get['kpi'] = new Zend_Db_Expr('SUM(CASE WHEN '.$this->_active.'
                        THEN (i.`value` * 0.8 * 0.002)
                    ELSE 0 END)');
            } else {
                $get['kpi'] = new Zend_Db_Expr('SUM(CASE WHEN '.$this->_status.'
                        THEN (i.`value` * 0.8 * 0.002)
                    ELSE 0 END)');
            }
        }
        //
        $select->from(array('i' => $this->_name), $get);
        $select->where('DATE(i.timing_date) >= ?', $params['from']);
        $select->where('DATE(i.timing_date) <= ?', $params['to']);
        //

        if (isset($params['pg']) && $params['pg']) {
            $select->where('i.pg_id <> i.sale_id AND i.pg_id <> i.leader_id', 1);

            if (!isset($params['kpi']) || !$params['kpi']) {
                $select->where('i.pg_id = ?', intval($params['pg']));

            } else {
                if (is_array($params['pg']) && count($params['pg']))
                    $select->where('i.pg_id IN (?)', $params['pg']);
                else
                    $select->where('1=0', 1);

                $group = array('i.pg_id');
            }

        } elseif (isset($params['sale']) && $params['sale']) {
            if (!isset($params['kpi']) || !$params['kpi']) {
                $select->where('i.sale_id = ?', intval($params['sale']));

            } else {
                if (is_array($params['sale']) && count($params['sale']))
                    $select->where('i.sale_id IN (?)', $params['sale']);
                else
                    $select->where('1=0', 1);

                $group = array('i.sale_id');
            }

        } elseif (isset($params['leader']) && $params['leader']) {
            if (!isset($params['kpi']) || !$params['kpi']) {
                $select->where('i.leader_id = ?', intval($params['leader']));

            } else {
                if (is_array($params['leader']) && count($params['leader']))
                    $select->where('i.leader_id IN (?)', $params['leader']);
                else
                    $select->where('1=0', 1);

                $group = array('i.leader_id');
            }
        }
        //

        // $union = clone $select; // tháº±ng select thuá»™c loáº¡i tham chiáº¿u, clone ra Ä‘á»ƒ group nÃ³ tiáº¿p, union thÃ¬ khÃ´ng group
        $default_group = array('YEAR(timing_date)', 'MONTH(timing_date)', 'good_id');

        $select->group( array_merge( $group,  $default_group) );

        return $select->__toString();
    }

    /**
     * @param  [type] $type   [description]
     * @param  [type] $params [description]
     * @return [type]         [description]
     */
    private function _fetchByMonthAllStaff($params)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $before_closing = My_Kpi::isBeforeClosingDate($params['to']);

        if (!$before_closing) {
            $get = array(
                'quantity'  => 'COUNT(DISTINCT i.imei_sn)',
                'activated' => new Zend_Db_Expr('SUM(CASE WHEN ' . $this->_active . ' THEN 1 ELSE 0 END)'),
            );
        } else {
            $get = array(
                'quantity'  => 'COUNT(DISTINCT i.imei_sn)',
                'activated' => new Zend_Db_Expr('SUM(CASE WHEN ' . $this->_status . ' THEN 1 ELSE 0 END)'),
            );
        }

        $get = array_merge($get, array(
            'year' => new Zend_Db_Expr('YEAR(timing_date)'),
            'month' => new Zend_Db_Expr('MONTH(timing_date)'),
        ));

        $group = array();



        if (isset($params['type']) && in_array($params['type'], My_Staff_Title::getPg())) {
            $get['staff_id'] = 'i.pg_id';
            $group = array('i.pg_id');

            $select->where('i.pg_id <> i.sale_id AND i.pg_id <> i.leader_id', 1);
            if (isset($params['staff_id'])) {
                if (is_array($params['staff_id']) && count($params['staff_id']))
                    $select->where('i.pg_id IN (?)', $params['staff_id']);
                elseif (is_numeric($params['staff_id']) && intval($params['staff_id']) > 0)
                    $select->where('i.pg_id = ?', intval($params['staff_id']));
                else
                    $select->where('1=0', 1);
            }
        }

        elseif (isset($params['type']) && $params['type'] == SALES_TITLE) {
            $get['staff_id'] = 'i.sale_id';
            $group = array('i.sale_id');

            if (isset($params['staff_id'])) {
                if (is_array($params['staff_id']) && count($params['staff_id']))
                    $select->where('i.sale_id IN (?)', $params['staff_id']);
                elseif (is_numeric($params['staff_id']) && intval($params['staff_id']) > 0)
                    $select->where('i.sale_id = ?', intval($params['staff_id']));
                else
                    $select->where('1=0', 1);
            }
        }

        elseif (isset($params['type']) && $params['type'] == SALES_TRAINEE_TITLE) {
            $get['staff_id'] = 'i.sale_id';
            $group = array('i.sale_id');

            if (isset($params['staff_id'])) {
                if (is_array($params['staff_id']) && count($params['staff_id']))
                    $select->where('i.sale_id IN (?)', $params['staff_id']);
                elseif (is_numeric($params['staff_id']) && intval($params['staff_id']) > 0)
                    $select->where('i.sale_id = ?', intval($params['staff_id']));
                else
                    $select->where('1=0', 1);
            }
        }

        elseif (isset($params['type']) && $params['type'] == LEADER_TITLE) {
            $get['staff_id'] = 'i.leader_id';
            $group = array('i.leader_id');

            if (isset($params['staff_id'])) {
                if (is_array($params['staff_id']) && count($params['staff_id']))
                    $select->where('i.leader_id IN (?)', $params['staff_id']);
                elseif (is_numeric($params['staff_id']) && intval($params['staff_id']) > 0)
                    $select->where('i.leader_id = ?', intval($params['staff_id']));
                else
                    $select->where('1=0', 1);
            }
        }elseif ( isset($params['type']) && in_array($params['type'], My_Staff_Title::getPbSale()) ) {
            
            $get['staff_id'] = 'i.pb_sale_id';
            $group           = array('i.pb_sale_id');
            $select->where('i.pb_sale_id', 1);
            if (isset($params['staff_id'])) {
                if (is_array($params['staff_id']) && count($params['staff_id'])){
                    $select->where('i.pb_sale_id IN (?)', $params['staff_id']);
                }elseif (is_numeric($params['staff_id']) && intval($params['staff_id']) > 0){
                    $select->where('i.pb_sale_id = ?', intval($params['staff_id']));
                }else{
                    $select->where('1=0', 1);
                }
            }
        }else{
            return null;
        }

        if(isset($params['group_year_month_good']) and $params['group_year_month_good'] ==1){
            $group[] = 'good_id';
            $get['good_id'] = 'i.good_id';
        }

        $select
            ->from(array('i' => $this->_name), $get)
            ->join(array('s' => 'staff'), sprintf('s.id=%s AND s.title=%d', $get['staff_id'], $params['type']), array())
            ;

        $QRegion = new Application_Model_RegionalMarket();

        if ( isset($params['asm']) && $params['asm'] ) {
            $QAsm = new Application_Model_Asm();
            $list_regions = $QAsm->get_cache($params['asm']);
            $list_regions = isset($list_regions['province']) && is_array($list_regions['province']) ? $list_regions['province'] : array();

            if (count($list_regions) > 0)
                $select->where( 's.regional_market IN (?)', $list_regions);
            else
                $select->where('1=0', 1);
        }

        if (isset($params['region_id']) && $params['region_id']) {
            if (is_array($params['region_id']) && count($params['region_id']))
                $select->where('s.regional_market IN (?)', $params['region_id']);

            elseif (is_numeric($params['region_id']) && $params['region_id'])
                $select->where('s.regional_market = ?', intval($params['region_id']));

        } elseif (isset($params['area_id']) && $params['area_id']) {
            $province_arr = array();

            if (is_array($params['area_id']) && count($params['area_id'])) {
                foreach ($params['area_id'] as $_key => $_area_id)
                    $province_arr = array_merge($province_arr, $QRegion->nget_province_id_by_area_cache($_area_id));

            } elseif (is_numeric($params['area_id']) && $params['area_id']) {
                $province_arr = $QRegion->nget_province_id_by_area_cache($params['area_id']);
            }

            if (count($province_arr))
                $select->where('s.regional_market IN (?)', $province_arr);
            else
                $select->where('1=0', 1);
        }

        if (isset($params['distributor_id']) && $params['distributor_id']) {
            $select->join(array('st' => 'store'), 'st.id=i.store_id', array());

            if (is_array($params['distributor_id']) && count($params['distributor_id']))
                $select->where('st.d_id IN (?)', $params['distributor_id']);

            elseif (is_numeric($params['distributor_id']) && $params['distributor_id'])
                $select->where('st.d_id = ?', intval($params['distributor_id']));
        }

        if (isset($params['store_id']) && $params['store_id']) {
            if (is_array($params['store_id']) && count($params['store_id']))
                $select->where('i.store_id IN (?)', $params['store_id']);

            elseif (is_numeric($params['store_id']) && $params['store_id'])
                $select->where('i.store_id = ?', intval($params['store_id']));
        }

        if (isset($params['name']) && $params['name']) {
            $select->where('CONCAT(TRIM(BOTH FROM s.firstname), \' \', TRIM(BOTH FROM s.lastname)) LIKE ?', '%'.$params['name'].'%');
        }

        $select->where('DATE(i.timing_date) >= ?', $params['from']);
        $select->where('DATE(i.timing_date) <= ?', $params['to']);



        $default_group = array('YEAR(timing_date)', 'MONTH(timing_date)');

        $select->group( array_merge( $group,  $default_group) );

        return  $select->__toString();
    }

    public function fetchByMonthAllStaff($page, $limit, &$total, $params)
    {
        if (!isset($params['title']) || !is_array($params['title']) || !count($params['title']))
            $list_title = My_Staff_Title::getSalesman();
        elseif (is_array($params['title']))
            $list_title = $params['title'];
        else
            $list_title = My_Staff_Title::getSalesman();


        $result_sql = array();

        if (!isset($params['from']) || !date_create_from_format('d/m/Y', $params['from']))
            throw new Exception("Invalid date format", 1);

        $params['from'] = date_create_from_format('d/m/Y', $params['from'])->format('Y-m-d');

        if (!isset($params['to']) || !date_create_from_format('d/m/Y', $params['to']))
            throw new Exception("Invalid date format", 1);

        $params['to'] = date_create_from_format('d/m/Y', $params['to'])->format('Y-m-d');

        foreach ($list_title as $key => $title) {
            $params['type'] = $title;
            $result_sql[ $title ] = $this->_fetchByMonthAllStaff($params);
        }

        $db = Zend_Registry::get('db');
        $sub_select = $db->select()->union($result_sql);
        $sub_select->order('quantity DESC');

        $x_select = $db->select()->from(array('X' => $sub_select), array('X.*'))->order('X.staff_id');

         if(!empty($_GET['dev'])){
    echo $x_select->__toString();
    exit;
}

        if(isset($params['group_year_month_good']) AND $params['group_year_month_good'] == 1){
            $x_select->order('quantity DESC');
            return $db->fetchAll($x_select);
        }

        $select = $db->select()
            ->from(array('A' => $x_select), array(
                'A.*',
                'dummy_1' => new Zend_Db_Expr('@num := IF (@staff_id <> A.staff_id, @num + 1, @num)'),
                'dummy_2' => new Zend_Db_Expr('@staff_id := IF (@staff_id <> A.staff_id, A.staff_id, @staff_id)'),
            ))
            ->group(array('year', 'month', 'dummy_1'));



        if (isset($params['get_total_sales']) && $params['get_total_sales']) {
            $sql = "SET @staff_id = 0;SET @num = 0;";
            $db->query($sql);
            $y_select = $db->select()->from(array('Y' => $select), array('total' => 'SUM(Y.quantity)'));
            $total_sales = $db->fetchRow($y_select);
            $get_total_sales = isset($total_sales['total']) && is_numeric($total_sales['total']) ? $total_sales['total'] : 0;
            return $get_total_sales;
        }

        $sql = "SET @staff_id = 0;SET @num = 0;";
        $db->query($sql);
        $y_select = $db->select()->from(array('Y' => $select), array('row' => 'COUNT(distinct Y.staff_id)'));
        $total_sales = $db->fetchRow($y_select);
        $total = isset($total_sales['row']) && is_numeric($total_sales['row']) ? $total_sales['row'] : 0;

        $select 
            ->having('dummy_1 >= ?', ($page-1)*$limit)
            ->having('dummy_1 < ?', $page*$limit)
            ->order('dummy_1');

        $sql = "SET @staff_id = 0;SET @num = 0;";
        $db->query($sql);



        
        return $db->fetchAll($select);
    }

    public function fetchArea($params)
    {
        if (!isset($params['from']) || !$params['from']){
            return false;
        }

        if (!isset($params['to']) || !$params['to']){
            return false;
        }

        $from           = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
        $to             = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");
        $before_closing = My_Kpi::isBeforeClosingDate($to);
        $db             = Zend_Registry::get('db');
        $select = $db->select();

        $countActivateConditionBefore = $countActivateConditionAfter = '(i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0) or ifnull(i.status,0) = 1';

        if (!$before_closing) {
            $get = array(
                'total_quantity'  => new Zend_Db_Expr("COUNT(DISTINCT i.imei_sn)"),
                'total_activated' => new Zend_Db_Expr("SUM(CASE WHEN (i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0) or ifnull(i.status,0) = 1
                    THEN 1 ELSE 0 END)"),
            );
        } else {
            $get = array(
                'total_quantity'  => new Zend_Db_Expr("COUNT(DISTINCT i.imei_sn)"),
                'total_activated' => new Zend_Db_Expr("SUM(CASE WHEN i.`status` IS NOT NULL AND i.`status` > 0
                    THEN 1 ELSE 0 END)"),
            );
        }

        if (isset($params['kpi']) && $params['kpi']) {
            $join_get = array(
                'good_id'     => new Zend_Db_Expr('IFNULL(g.good_id,g2.good_id)'),
                'color_id'    => new Zend_Db_Expr('IFNULL(IFNULL(g.color_id,g2.color_id), 0)'),
                'from_date'   => new Zend_Db_Expr(sprintf("CASE WHEN IFNULL(g.from_date,g2.from_date) < '%s' THEN '%s' ELSE IFNULL(g.from_date,g2.from_date) END", $from, $from)),
                'to_date'     => new Zend_Db_Expr(sprintf("CASE WHEN IFNULL(g.to_date,g2.to_date) IS NULL OR IFNULL(g.to_date,g2.to_date) > '%s' THEN '%s' ELSE IFNULL(g.to_date,g2.to_date) END", $to, $to)),
                'total_value' => new Zend_Db_Expr("SUM(IFNULL(g.price,g2.price)) * 0.8"),
            );

            if (!$before_closing) {
                $join_get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0 or ifnull(i.status,0) = 1 THEN IFNULL(g.price,g2.price) ELSE 0 END) * 0.8");
            } else {
                $join_get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0 or ifnull(i.status,0) = 1 THEN IFNULL(g.price,g2.price) ELSE 0 END) * 0.8");
            }

        }elseif(isset($params['group_year_month_good']) and $params['group_year_month_good']){

            $get['year']        = 'YEAR(i.timing_date)';
            $get['month']       = 'MONTH(i.timing_date)';
            $get['good_id']     = 'i.good_id';
            $get['total_value'] = new Zend_Db_Expr("SUM(i.`value`) * 0.8");

            if (!$before_closing) {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN $countActivateConditionAfter THEN i.`value` ELSE 0 END) * 0.8");
            } else {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN $countActivateConditionBefore THEN i.`value` ELSE 0 END) * 0.8");
            }

        }else {

            $get['year']        = 'YEAR(i.timing_date)';
            $get['month']       = 'MONTH(i.timing_date)';
            $get['total_value'] = new Zend_Db_Expr("SUM(i.`value`) * 0.8");
            if (!$before_closing) {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN $countActivateConditionAfter THEN i.`value` ELSE 0 END) * 0.8");
            } else {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN $countActivateConditionBefore THEN i.`value` ELSE 0 END) * 0.8");
            }
        }

        $select->from(array('i' => $this->_name), $get);

        if (isset($params['kpi']) && $params['kpi']) {
            /*
            $select->join(
                array('g' => WAREHOUSE_DB.'.good_price_log'),
                'g.good_id=i.good_id
                AND
                    CASE WHEN g.color_id <> 0 AND g.color_id IS NOT NULL THEN i.color_id=g.color_id
                    ELSE i.color_id NOT IN (
                            SELECT '.WAREHOUSE_DB.'.`good_price_log`.color_id
                            FROM '.WAREHOUSE_DB.'.`good_price_log`
                            WHERE '.WAREHOUSE_DB.'.`good_price_log`.color_id <> 0
                            AND '.WAREHOUSE_DB.'.`good_price_log`.good_id = i.good_id
                            AND '.WAREHOUSE_DB.'.`good_price_log`.from_date <= DATE(i.timing_date)
                            AND ('.WAREHOUSE_DB.'.`good_price_log`.to_date IS NULL OR '.WAREHOUSE_DB.'.`good_price_log`.to_date = 0 OR '.WAREHOUSE_DB.'.`good_price_log`.to_date >= DATE(i.timing_date))
                        )
                    END
                AND g.from_date<= DATE(i.timing_date)
                AND ( g.to_date IS NULL OR g.to_date = 0 OR g.to_date >= DATE(i.timing_date) )',
                $join_get
            );
            */
           
            $select->joinLeft(
                array('g' => WAREHOUSE_DB.'.good_price_log'),
                '   g.good_id=i.good_id
                    AND IFNULL(g.color_id,0)  <> 0
                    AND IFNULL(g.color_id,0) = i.color_id
                    AND g.from_date <= DATE(i.timing_date)
                    AND ( g.to_date IS NULL OR g.to_date = 0 OR g.to_date >= DATE(i.timing_date) )',
                $join_get
            );

            $select->joinLeft(
                array('g2' => WAREHOUSE_DB.'.good_price_log'),
                '   g2.good_id=i.good_id
                    AND IFNULL(g2.color_id,0)  = 0
                    AND g2.from_date <= DATE(i.timing_date)
                    AND ( g2.to_date IS NULL OR g2.to_date = 0 OR g2.to_date >= DATE(i.timing_date) )',
                array()
            );

        }

        $select->join(array('a' => 'area'), 'a.id=i.area_id',array('area_id' => 'a.id', 'area_name' => 'a.name', 'region_share' => 'a.region_share'));
        
        if (isset($params['dealer_id']) && $params['dealer_id']){
            $select->joinLeft(array('s' => 'store'), 's.id = i.store_id',array());
            $select->joinLeft(array('d' => WAREHOUSE_DB.'.distributor'), 'd.id = s.d_id',array());    
            $select->where("d.id = ? OR d.parent = ?", $params['dealer_id']);
        }

        if (isset($params['from']) && $params['from']){
            $select->where("i.timing_date >= '".$from." 00:00:00'", null);
        }

        if (isset($params['to']) && $params['to']){
            $select->where("i.timing_date <= '".$to." 23:59:59'", null);
        }

        if( isset($params['area']) ){
            if(is_array($params['area']) and count($params['area']) > 0){
                $select->where('a.id IN (?)',$params['area']);
            }elseif($params['area']){
                $select->where('a.id = ?',$params['area']);
            }
        }

        if (!isset($params['get_total_sales']) || !$params['get_total_sales']) {

            if (isset($params['kpi']) && $params['kpi']){
                $select->group( array('a.id', 'IFNULL(g.good_id,g2.good_id)', 'IFNULL(g.color_id,g2.color_id)', 'IFNULL(g.from_date,g2.from_date)') );
            }elseif(isset($params['group_year_month_good']) and $params['group_year_month_good']){
                $select->group( array('a.id','YEAR(i.timing_date)','MONTH(i.timing_date)', 'i.good_id') );
            }else{
                $select->group( array('a.id') );
            }

            if(isset($params['group_year_month_good']) and $params['group_year_month_good']){
                $select->order(array('year DESC','month DESC','a.id','total_quantity DESC'));
            }

            // if(!empty($_GET['dev'])){
            //     echo $select->__toString();
            //     exit;
            // }
            // PC::db($select->__toString());
            return $db->fetchAll($select);
        } else {
            return $db->fetchRow($select);
        }
    }

    public function fetchAreaProvince($params)
    {
        if (!isset($params['from']) || !$params['from']){
            return false;
        }

        if (!isset($params['to']) || !$params['to']){
            return false;
        }

        $from           = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
        $to             = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");
        $before_closing = My_Kpi::isBeforeClosingDate($to);
        $db             = Zend_Registry::get('db');
        $select = $db->select();

        $countActivateConditionBefore = $countActivateConditionAfter = '(i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0) or ifnull(i.status,0) = 1';

        if (!$before_closing) {
            $get = array(
                'total_quantity'  => new Zend_Db_Expr("COUNT(DISTINCT i.imei_sn)"),
                'total_activated' => new Zend_Db_Expr("SUM(CASE WHEN (i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0) or ifnull(i.status,0) = 1
                    THEN 1 ELSE 0 END)"),
            );
        } else {
            $get = array(
                'total_quantity'  => new Zend_Db_Expr("COUNT(DISTINCT i.imei_sn)"),
                'total_activated' => new Zend_Db_Expr("SUM(CASE WHEN i.`status` IS NOT NULL AND i.`status` > 0
                    THEN 1 ELSE 0 END)"),
            );
        }

        if (isset($params['kpi']) && $params['kpi']) {
            $join_get = array(
                'good_id'     => new Zend_Db_Expr('IFNULL(g.good_id,g2.good_id)'),
                'color_id'    => new Zend_Db_Expr('IFNULL(IFNULL(g.color_id,g2.color_id), 0)'),
                'from_date'   => new Zend_Db_Expr(sprintf("CASE WHEN IFNULL(g.from_date,g2.from_date) < '%s' THEN '%s' ELSE IFNULL(g.from_date,g2.from_date) END", $from, $from)),
                'to_date'     => new Zend_Db_Expr(sprintf("CASE WHEN IFNULL(g.to_date,g2.to_date) IS NULL OR IFNULL(g.to_date,g2.to_date) > '%s' THEN '%s' ELSE IFNULL(g.to_date,g2.to_date) END", $to, $to)),
                'total_value' => new Zend_Db_Expr("SUM(IFNULL(g.price,g2.price)) * 0.8"),
            );

            if (!$before_closing) {
                $join_get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0 THEN IFNULL(g.price,g2.price) ELSE 0 END) * 0.8");
            } else {
                $join_get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`status`> 0 THEN IFNULL(g.price,g2.price) ELSE 0 END) * 0.8");
            }

        }elseif(isset($params['group_year_month_good']) and $params['group_year_month_good']){

            $get['year']        = 'YEAR(i.timing_date)';
            $get['month']       = 'MONTH(i.timing_date)';
            $get['good_id']     = 'i.good_id';
            $get['total_value'] = new Zend_Db_Expr("SUM(i.`value`) * 0.8");

            if (!$before_closing) {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN $countActivateConditionAfter THEN i.`value` ELSE 0 END) * 0.8");
            } else {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN $countActivateConditionBefore THEN i.`value` ELSE 0 END) * 0.8");
            }

        }else {

            $get['year']        = 'YEAR(i.timing_date)';
            $get['month']       = 'MONTH(i.timing_date)';
            $get['total_value'] = new Zend_Db_Expr("SUM(i.`value`) * 0.8");
            if (!$before_closing) {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN $countActivateConditionAfter THEN i.`value` ELSE 0 END) * 0.8");
            } else {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN $countActivateConditionBefore THEN i.`value` ELSE 0 END) * 0.8");
            }
        }

        $get['area_id'] = 'IF(a2.id = 51, a2.id, a.id)';
        $get['area_name'] = 'IF(a2.id = 51, a2.name, a.name)';
        $get['region_share'] = 'IF(a2.id = 51, a2.region_share, ap.region_share)';

        $select->from(array('i' => $this->_name), $get);

        if (isset($params['kpi']) && $params['kpi']) {
            /*
            $select->join(
                array('g' => WAREHOUSE_DB.'.good_price_log'),
                'g.good_id=i.good_id
                AND
                    CASE WHEN g.color_id <> 0 AND g.color_id IS NOT NULL THEN i.color_id=g.color_id
                    ELSE i.color_id NOT IN (
                            SELECT '.WAREHOUSE_DB.'.`good_price_log`.color_id
                            FROM '.WAREHOUSE_DB.'.`good_price_log`
                            WHERE '.WAREHOUSE_DB.'.`good_price_log`.color_id <> 0
                            AND '.WAREHOUSE_DB.'.`good_price_log`.good_id = i.good_id
                            AND '.WAREHOUSE_DB.'.`good_price_log`.from_date <= DATE(i.timing_date)
                            AND ('.WAREHOUSE_DB.'.`good_price_log`.to_date IS NULL OR '.WAREHOUSE_DB.'.`good_price_log`.to_date = 0 OR '.WAREHOUSE_DB.'.`good_price_log`.to_date >= DATE(i.timing_date))
                        )
                    END
                AND g.from_date<= DATE(i.timing_date)
                AND ( g.to_date IS NULL OR g.to_date = 0 OR g.to_date >= DATE(i.timing_date) )',
                $join_get
            );
            */
           
            $select->joinLeft(
                array('g' => WAREHOUSE_DB.'.good_price_log'),
                '   g.good_id=i.good_id
                    AND IFNULL(g.color_id,0)  <> 0
                    AND IFNULL(g.color_id,0) = i.color_id
                    AND g.from_date <= DATE(i.timing_date)
                    AND ( g.to_date IS NULL OR g.to_date = 0 OR g.to_date >= DATE(i.timing_date) )',
                $join_get
            );

            $select->joinLeft(
                array('g2' => WAREHOUSE_DB.'.good_price_log'),
                '   g2.good_id=i.good_id
                    AND IFNULL(g2.color_id,0)  = 0
                    AND g2.from_date <= DATE(i.timing_date)
                    AND ( g2.to_date IS NULL OR g2.to_date = 0 OR g2.to_date >= DATE(i.timing_date) )',
                array()
            );

        }

        $select->join(array('ap' => 'area_province'), 'ap.district_id = i.district_id', array());
        $select->join(array('a' => 'area'), 'a.id = ap.area_id',array());

        $select->join(array('a2' => 'area'), 'a2.id = i.area_id',array());    

        if (isset($params['from']) && $params['from']){
            $select->where("i.timing_date >= '".$from." 00:00:00'", null);
        }

        if (isset($params['to']) && $params['to']){
            $select->where("i.timing_date <= '".$to." 23:59:59'", null);
        }

        if( isset($params['area']) ){
            if(is_array($params['area']) and count($params['area']) > 0){
                $select->where('a.id IN (?)',$params['area']);
            }elseif($params['area']){
                $select->where('a.id = ?',$params['area']);
            }
        }

        if (!isset($params['get_total_sales']) || !$params['get_total_sales']) {

            if (isset($params['kpi']) && $params['kpi']){
                $select->group( array('area_id', 'IFNULL(g.good_id,g2.good_id)', 'IFNULL(g.color_id,g2.color_id)', 'IFNULL(g.from_date,g2.from_date)') );
            }elseif(isset($params['group_year_month_good']) and $params['group_year_month_good']){
                $select->group( array('area_id','YEAR(i.timing_date)','MONTH(i.timing_date)', 'i.good_id') );
            }else{
                $select->group( array('area_id') );
            }

            if(isset($params['group_year_month_good']) and $params['group_year_month_good']){
                $select->order(array('year DESC','month DESC','a.id','total_quantity DESC'));
            }

            // PC::db($select->__toString());
            return $db->fetchAll($select);
        } else {
            return $db->fetchRow($select);
        }
    }

    public function getListLeader($listname, $from_month_last, $to_month_last, $from_month_now, $to_month_now )
    {
        $total = 0;

        $db = Zend_Registry::get('db');
        $select = $db->select();

        $select = "SELECT 
            i.good_id AS `good_id`,
            n.`name` AS `product`,
            s.firstname AS `firstname`,
            s.lastname AS `lastname`,
            h.`name` AS `color`,
            i.leader_id AS `leader_id`,
            SUM(IF(DATE(i.timing_date) between '".$from_month_last."' and '".$to_month_last."', i.value, 0)) AS `value_total_last`,
            SUM(IF(DATE(i.timing_date) between '".$from_month_last."' and '".$to_month_last."', 1, 0)) AS `quantity_last`,
            MAX(IF(DATE(i.timing_date) between '".$from_month_last."' and '".$to_month_last."', i.value, 0)) AS 'value_last',
            MAX(IF(DATE(i.timing_date) between '".$from_month_now."' and '".$to_month_now."', i.value, 0)) AS 'value_now',
            SUM(IF(DATE(i.timing_date) between '".$from_month_now."' and '".$to_month_now."', i.value, 0)) AS `value_total_now`,
            SUM(IF(DATE(i.timing_date) between '".$from_month_now."' and '".$to_month_now."', 1, 0)) AS `quantity_now`
        FROM imei_kpi i 
        LEFT JOIN warehouse.good as n ON i.good_id = n.id
        LEFT JOIN warehouse.good_color AS h ON i.color_id = h.id
        LEFT JOIN staff as s ON i.leader_id = s.id    
        WHERE i.leader_id IN (". $listname .")
        AND DATE(i.timing_date) between '".$from_month_last."' and '".$to_month_now."'
        AND i.value > 0
        GROUP BY  i.good_id, i.color_id, i.leader_id
        ORDER BY i.leader_id, i.good_id";

        $total = $db->fetchAll($select);
        return $total;
    }

    public function fetchLeader($params)
    {
        if (!isset($params['from']) || !$params['from']){
            return false;
        }

        if (!isset($params['to']) || !$params['to']){
            return false;
        }

        $from           = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
        $to             = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");
        $before_closing = My_Kpi::isBeforeClosingDate($to);
        $db             = Zend_Registry::get('db');
        $select         = $db->select();

        $countActivateConditionBefore = $countActivateConditionAfter = '(i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0) or ifnull(i.status,0) = 1';

        if (!$before_closing) {
            $get = array(
                'total_quantity'  => new Zend_Db_Expr("COUNT(DISTINCT i.imei_sn)"),
                'total_activated' => new Zend_Db_Expr("SUM(CASE WHEN $countActivateConditionAfter THEN 1 ELSE 0 END)"),
            );
        } else {
            $get = array(
                'total_quantity'  => new Zend_Db_Expr("COUNT(DISTINCT i.imei_sn)"),
                'total_activated' => new Zend_Db_Expr("SUM(CASE WHEN $countActivateConditionBefore THEN 1 ELSE 0 END)"),
            );
        }

        if (isset($params['kpi']) && $params['kpi']) {

            $join_get = array(
                'good_id'     => new Zend_Db_Expr('IFNULL(g.good_id,g2.good_id)'),
                'color_id'    => new Zend_Db_Expr('IFNULL(IFNULL(g.color_id,g2.color_id), 0)'),
                'from_date'   => new Zend_Db_Expr(sprintf("CASE WHEN IFNULL(g.from_date,g2.from_date) < '%s' THEN '%s' ELSE IFNULL(g.from_date,g2.from_date) END", $from, $from)),
                'to_date'     => new Zend_Db_Expr(sprintf("CASE WHEN IFNULL(g.to_date,g2.to_date) IS NULL OR IFNULL(g.to_date,g2.to_date) > '%s' THEN '%s' ELSE IFNULL(g.to_date,g2.to_date) END", $to, $to)),
                'total_value' => new Zend_Db_Expr("SUM(IFNULL(g.price,g2.price)) * 0.8"),
                'total_value_bonus' => new Zend_Db_Expr("SUM(CASE WHEN i.sale_id = i.leader_id THEN i.`value` ELSE 0 END) * 0.8"),
            );

            if (!$before_closing) {
                $join_get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN ((i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0) or ifnull(i.status,0) = 1) THEN IFNULL(g.price,g2.price) ELSE 0 END) * 0.8");
            } else {
                $join_get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`status`> 0 THEN IFNULL(g.price,g2.price) ELSE 0 END) * 0.8");
            }

        } else {
            $get['total_value'] = new Zend_Db_Expr("SUM(i.`value`) * 0.8");
            $get['total_value_bonus'] = new Zend_Db_Expr("SUM(CASE WHEN i.sale_id = i.leader_id THEN i.`value` ELSE 0 END) * 0.8");
            if (!$before_closing) {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN $countActivateConditionAfter THEN i.`value` ELSE 0 END) * 0.8");
            } else {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN $countActivateConditionBefore THEN i.`value` ELSE 0 END) * 0.8");
            }
        }

        $get['timing_from_date'] = new Zend_Db_Expr("MIN(DATE(timing_date))");
        $get['timing_to_date'] = new Zend_Db_Expr("MAX(DATE(timing_date))");

        $select->from(array('i' => $this->_name), $get);

        if (isset($params['kpi']) && $params['kpi']) {

            $select->joinLeft(
                array('g' => WAREHOUSE_DB.'.good_price_log'),
                '   g.good_id=i.good_id
                    AND IFNULL(g.color_id,0)  <> 0
                    AND IFNULL(g.color_id,0) = i.color_id
                    AND g.from_date <= DATE(i.timing_date)
                    AND ( g.to_date IS NULL OR g.to_date = 0 OR g.to_date >= DATE(i.timing_date) )',
                $join_get
            );

            $select->joinLeft(
                array('g2' => WAREHOUSE_DB.'.good_price_log'),
                '   g2.good_id=i.good_id
                    AND IFNULL(g2.color_id,0)  = 0
                    AND g2.from_date <= DATE(i.timing_date)
                    AND ( g2.to_date IS NULL OR g2.to_date = 0 OR g2.to_date >= DATE(i.timing_date) )',
                array()
            );
        }

        $select->join(array('s' => 'sales_region_staff_log'), 
                    's.staff_id=i.leader_id 
                    AND (s.from_date <= DATE(i.timing_date))
                    AND (s.to_date IS NULL OR DATE(i.timing_date) <= s.to_date)',
                    array('s.staff_id','sales_region_staff_log_id'=>'s.id')); 
        $select->join(array('a'=>'sales_region'),'a.id = s.sales_region_id',array());

        if (isset($params['from']) && $params['from']){
            $select->where('DATE(i.timing_date) >= ?', $from);
            //$select->where('a.from_date <= ?', $from);
        }

        if (isset($params['to']) && $params['to']){
            $select->where('DATE(i.timing_date) <= ?', $to);
            //$select->where('a.from_date <= ?', $from);
        }

        if (!isset($params['get_total_sales']) || !$params['get_total_sales']) {
            if (isset($params['kpi']) && $params['kpi']){
                $select->group( array('s.id', 'IFNULL(g.good_id,g2.good_id)', 'IFNULL(g.color_id,g2.color_id)', 'IFNULL(g.from_date,g2.from_date)') );
            }else{
                $select->group('s.id');
            }
        }

        $cols = array(
            'staff_from_date'         => 'a.from_date',
            'staff_to_date'           => 'a.to_date',
            'a.type',
            'b.*',
            'region_id'                 => 'c.id',
            'region_name'               => 'c.name',
            'region_shared'             => 'c.shared',
            'area_id'                   => 'd.id',
            'area_name'                 => 'd.name',
            'area_shared'               => 'd.region_share',
            'email'                     => 'e.email',
            'code'                      => 'e.code',
            'staff_id'                  => 'e.id',
            'staff_title_id'            => 'e.title',
            'staff_title'               => 'f.name',
            'staff_name'                => new Zend_Db_Expr("CONCAT(e.firstname, ' ', e.lastname)"),
            'sales_region_staff_log_id' => 'a.id',
        );

        $main_select = $db->select()
            ->from(array('a'     => 'sales_region_staff_log'),$cols)
            ->joinLeft(array('b' => $select),'a.id = b.sales_region_staff_log_id',array())
            ->join(array('c'     => 'sales_region'),'c.id = a.sales_region_id',array())
            ->join(array('d'     => 'area'),'d.id = c.parent',array())
            ->join(array('e'     => 'staff'),'e.id = a.staff_id',array())
            ->join(array('f'     => 'team'),'f.id = e.title',array())
        ;

        if (isset($params['from']) && $params['from']){
            $main_select->where('a.to_date IS NULL OR a.to_date >= ?', $from);
        }

        if (isset($params['to']) && $params['to']){
            $main_select->where("c.from_date <= ? ",$to);
        }

        if (isset($params['area']) && $params['area']){
            $main_select->where("d.id = ? ",$params['area']);
        }

        if (isset($params['title']) && $params['title']){
            $main_select->where("e.title = ? ",$params['title']);
        }

        $main_select->order(array('d.name', 'a.type','a.staff_id'));
// if(!empty($_GET['dev'])){
//     echo $main_select->__toString();die;
// }
             PC::debug($main_select->__toString());
        
        
        if (!isset($params['get_total_sales']) || !$params['get_total_sales']) {
            return $db->fetchAll($main_select);
        } else {
            return $db->fetchRow($main_select);
        }
    }

    

    public function fetchStore($page, $limit, &$total, $params)
    {
        if (!isset($params['from']) || !$params['from']){
            return false;
        }

        if (!isset($params['to']) || !$params['to']){
            return false;
        }


        $from = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
        $to = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");

        $before_closing = My_Kpi::isBeforeClosingDate($to);

        $db = Zend_Registry::get('db');
        $select = $db->select();

        $main_get = array();

        if (!$before_closing) {
            $get = array(
                'total_quantity'  => new Zend_Db_Expr("COUNT(DISTINCT i.imei_sn)"),
                'total_activated' => new Zend_Db_Expr("SUM(CASE WHEN (i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0) or ifnull(i.status,0) = 1 THEN 1 ELSE 0 END)"),
            );
        } else {
            $get = array(
                'total_quantity'  => new Zend_Db_Expr("COUNT(DISTINCT i.imei_sn)"),
                'total_activated' => new Zend_Db_Expr("SUM(CASE WHEN i.`status`>0 THEN 1 ELSE 0 END)"),
            );
        }

        $get['store_id'] = 'i.store_id';
        
        if (isset($params['kpi']) && $params['kpi']) {
            $join_get = array(
                'good_id'     => new Zend_Db_Expr('IFNULL(g.good_id,g2.good_id)'),
                'color_id'    => new Zend_Db_Expr('IFNULL(IFNULL(g.color_id,g2.color_id), 0)'),
                'from_date'   => new Zend_Db_Expr(sprintf("CASE WHEN IFNULL(g.from_date,g2.from_date) < '%s' THEN '%s' ELSE IFNULL(g.from_date,g2.from_date) END", $from, $from)),
                'to_date'     => new Zend_Db_Expr(sprintf("CASE WHEN IFNULL(g.to_date,g2.to_date) IS NULL OR IFNULL(g.to_date,g2.to_date) > '%s' THEN '%s' ELSE IFNULL(g.to_date,g2.to_date) END", $to, $to)),
                'total_value' => new Zend_Db_Expr("SUM(IFNULL(g.price,g2.price)) * 0.8"),
            );

            if (!$before_closing) {
                $join_get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0 THEN IFNULL(g.price,g2.price) ELSE 0 END) * 0.8");
            } else {
                $join_get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`status`> 0 THEN IFNULL(g.price,g2.price) ELSE 0 END) * 0.8");
            }

        } elseif(isset($params['group_year_month_good']) and $params['group_year_month_good']){

            $get['year']         = 'YEAR(i.timing_date)';
            $get['month']        = 'MONTH(i.timing_date)';
            $get['good_id']      = 'i.good_id';
            $main_get['year']    = 'year';
            $main_get['month']   = 'month';
            $main_get['good_id'] = 'B.good_id';
            $get['total_value']  = new Zend_Db_Expr("SUM(i.`value`) * 0.8");

            if (!$before_closing) {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0 THEN i.`value` ELSE 0 END) * 0.8");
            } else {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`status`> 0 THEN i.`value` ELSE 0 END) * 0.8");
            }

        }else {
            $get['total_value'] = new Zend_Db_Expr("SUM(i.`value`) * 0.8");

            if (!$before_closing) {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0 THEN i.`value` ELSE 0 END) * 0.8");
            } else {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`status`> 0 THEN i.`value` ELSE 0 END) * 0.8");
            }
        }

        $select->from(array('i' => $this->_name), $get);
       
        if (isset($params['kpi']) && $params['kpi']) {
            $select->joinLeft(
                array('g' => WAREHOUSE_DB.'.good_price_log'),
                '   g.good_id=i.good_id
                    AND IFNULL(g.color_id,0)  <> 0
                    AND IFNULL(g.color_id,0) = i.color_id
                    AND g.from_date <= DATE(i.timing_date)
                    AND ( g.to_date IS NULL OR g.to_date = 0 OR g.to_date >= DATE(i.timing_date) )',
                $join_get
            );

            $select->joinLeft(
                array('g2' => WAREHOUSE_DB.'.good_price_log'),
                '   g2.good_id=i.good_id
                    AND IFNULL(g2.color_id,0)  = 0
                    AND g2.from_date <= DATE(i.timing_date)
                    AND ( g2.to_date IS NULL OR g2.to_date = 0 OR g2.to_date >= DATE(i.timing_date) )',
                array()
            );
        }
        
        $select->join(array('s' => 'store'), 's.id=i.store_id', array());
        $select->joinLeft(array('d'=>WAREHOUSE_DB.'.distributor'),'d.id = s.d_id',array())
        ->joinLeft(array('dl'=>'dealer_loyalty'),"dl.dealer_id = d.id AND '$from' BETWEEN dl.from_date AND dl.to_date AND '$to' BETWEEN dl.from_date AND dl.to_date",array())
        ->joinLeft(array('l'=>'loyalty_plan'),'l.id = dl.loyalty_plan_id',array());


        if (isset($params['sale_id']) && $params['sale_id']){

            if(isset($params['title']) and $params['title'] == PB_SALES_TITLE){
                $select->where('i.pb_sale_id = ?', intval($params['sale_id']));
            }else{
                $select->where('i.sale_id = ?', intval($params['sale_id']));
            }
            
        }

        if (isset($params['leader_id']) && $params['leader_id']){
            $select->where('(i.leader_id = ?', intval($params['leader_id']));
            $select->orwhere('i.sale_id = ?)', intval($params['leader_id']));
        }

        if (isset($params['distributor_id']) && $params['distributor_id']){
            $select->where('s.d_id = ?', intval($params['distributor_id']));
        }


        /*    
        if (isset($params['area_list']) && $params['area_list']) {
            if (is_array($params['area_list']) && count($params['area_list']))
                    $select->where('i.area_id IN (?)', $params['area_list']);
            elseif (is_numeric($params['area_list']))
                    $select->where('i.area_id = ?', intval($params['area_list']));
            else
                $select->where('1=0', 1);
        }
        
        if (isset($params['district']) && $params['district']) {
            if (is_array($params['district']) && count($params['district']))
                    $select->where('i.district_id IN (?)', $params['district']);
            elseif (is_numeric($params['district']))
                    $select->where('i.district_id = ?', intval($params['district']));
            else
                $select->where('1=0', 1);
        }elseif (isset($params['province']) && $params['province']) {
            if (is_array($params['province']) && count($params['province']))
                    $select->where('i.province_id IN (?)', $params['province']);
            elseif (is_numeric($params['province']))
                    $select->where('i.province_id = ?', intval($params['province']));
            else
                $select->where('1=0', 1);
        }elseif (isset($params['area']) && $params['area']) {
            if (is_array($params['area']) && count($params['area']))
                    $select->where('i.area_id IN (?)', $params['area']);
            elseif (is_numeric($params['area']))
                    $select->where('i.area_id = ?', intval($params['area']));
            else
                $select->where('1=0', 1);
        }
        */
       
        if (isset($params['store']) && $params['store']) {
            if (is_array($params['store']) && count($params['store']))
                    $select->where('i.store_id IN (?)', $params['store']);
            elseif (is_numeric($params['store']))
                    $select->where('i.store_id = ?', intval($params['store']));
            else
                $select->where('1=0', 1);
        }
    
        if (isset($params['store_list']) && $params['store_list']) {
            if (is_array($params['store_list']) && count($params['store_list']))
                    $select->where('i.store_id IN (?)', $params['store_list']);
            elseif (is_numeric($params['store_list']))
                    $select->where('i.store_id = ?', intval($params['store_list']));
            else
                $select->where('1=0', 1);
        }


        if (isset($params['from']) && $params['from']){
            $select->where("i.timing_date >= ?", $from.' 00:00:00');
        }

        if (isset($params['to']) && $params['to']){
            $select->where("i.timing_date <= ?", $to.' 23:59:59');
        }

        if (isset($params['name']) && $params['name']) {
            $select->joinRight(array('ss' => 'store'), 'ss.id=i.store_id', array());
            $select->where('ss.name LIKE ?', '%'.$params['name'].'%');
        }

        if(isset($params['store_level']) and $params['store_level']){
            if($params['store_level'] == 4){
                $select->where('l.id IS NULL OR l.id NOT IN (?)',array(1,2,3));
                $select->where('d.is_ka IS NULL OR d.is_ka = 0');
            }else{
                $select->where('l.id = ?',$params['store_level']);
            }
        }

        if(isset($params['channel']) and $params['channel'] >= 0){
            $select->where('d.is_ka = ?',$params['channel']);
        }
        
        if(isset($params['goods']) && $params['goods']){
            $select->where('i.good_id IN (?)',$params['goods']);
        }
       
        if (isset($params['kpi']) && $params['kpi']){
            $select->group( array('i.store_id', 'IFNULL(g.good_id,g2.good_id)', 'IFNULL(g.color_id,g2.color_id)', 'IFNULL(g.from_date,g2.from_date)') );
        }elseif(isset($params['group_year_month_good']) and $params['group_year_month_good']){
            $select->group(array('YEAR(timing_date)', 'MONTH(timing_date)','i.store_id','i.good_id'));
        }else{
            $select->group('i.store_id');
        }

        if (isset($params['kpi']) && $params['kpi']){
            $main_get = array('B.good_id', 'B.color_id', 'B.from_date', 'B.to_date');
        }

        $main_get = array_merge($main_get, array('B.total_quantity', 'B.total_activated', 'B.total_value_activated', 'B.total_value'));

        $date_level = (isset($to) and $to) ? ("'".$to."'") : 'CURDATE()';

        $cols = array('store_id' => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS s.id'),
                    'store_name'       => 's.name', 
                    'store_district'   => 'v_store.district',
                    's.company_address',
                    's.d_id',
                    'store_short_name' => 'IFNULL(s.short_name,s.name)',
                    'store_chanel'     => '(CASE WHEN IFNULL(d.is_ka,0) = 0 THEN "IND" ELSE "KA" END)',
                    'store_level'      => 'l.name'
                    );

        if (isset($params['get_total_sales']) && $params['get_total_sales']){
            $cols = array('store_id' => 's.id');
        }

        $main_select = $db->select()
            ->from(array('s' => 'store'),$cols)
            ->join(array('v_store'=>'v_store_area'),'v_store.store_id = s.id',array())
            ->joinLeft(array('d'=>WAREHOUSE_DB.'.distributor'),'d.id = s.d_id',array('name_distributor' => 'd.name', 'd.is_ka'))
            ->joinLeft(array('dl'=>'dealer_loyalty'),'dl.dealer_id = d.id AND '.$date_level.' BETWEEN dl.from_date AND dl.to_date',array())
            ->joinLeft(array('l'=>'loyalty_plan'),'l.id = dl.loyalty_plan_id',array())
            ->joinLeft(array('B' => $select), 'B.store_id=s.id', $main_get)
            // ->where('B.total_quantity > 0 ')
            ;

        if(empty($params['include_0_sell_out'])) $main_select->where('B.total_quantity > 0 ');

        if (isset($params['store']) && $params['store']) {
            if (is_array($params['store']) && count($params['store']))
                $main_select->where('s.id IN (?)', $params['store']);
            elseif (is_numeric($params['store']))
                $main_select->where('s.id = ?', intval($params['store']));
            else
                $main_select->where('1=0', 1);
        }

        if (isset($params['store_list']) && $params['store_list']) {
            if (is_array($params['store_list']) && count($params['store_list']))
                $main_select->where('s.id IN (?)', $params['store_list']);
            elseif (is_numeric($params['store_list']))
                $main_select->where('s.id = ?', intval($params['store_list']));
            else
                $main_select->where('1=0', 1);
        }
    
        if (isset($params['area_list']) && $params['area_list']) {
            if (is_array($params['area_list']) && count($params['area_list']))
                $main_select
                    ->join(array('dt' => 'v_regional_market'), 'dt.id=v_store.district AND dt.parent = v_store.regional_market', array())
                    ->join(array('pr' => 'v_regional_market'), 'pr.id=dt.parent', array())
                    ->where('pr.area_id IN (?)', $params['area_list']);
            elseif (is_numeric($params['area_list']))
                $main_select
                    ->join(array('dt' => 'v_regional_market'), 'dt.id=v_store.district AND dt.parent = v_store.regional_market', array())
                    ->join(array('pr' => 'v_regional_market'), 'pr.id=dt.parent', array())
                    ->where('pr.area_id = ?', intval($params['area_list']));
            else
                $main_select->where('1=0', 1);
        }

        if (isset($params['district']) && $params['district']) {
            if (is_array($params['district']) && count($params['district']))
                $main_select->where('s.district IN (?)', $params['district']);
            elseif (is_numeric($params['district']))
                $main_select->where('s.district = ?', intval($params['district']));
            else
                $main_select->where('1=0', 1);

        } elseif (isset($params['province']) && $params['province']) {
            if (is_array($params['province']) && count($params['province']))
                $main_select
                    ->join(array('dt2' => 'v_regional_market'), 'dt2.id=s.district', array())
                    ->where('dt2.parent IN (?)', $params['province']);
            elseif (is_numeric($params['province']))
                $main_select
                    ->join(array('dt2' => 'v_regional_market'), 'dt2.id=s.district', array())
                    ->where('dt2.parent = ?', intval($params['province']));
            else
                $main_select->where('1=0', 1);

        } elseif (isset($params['area']) && $params['area']) {
            if (is_array($params['area']) && count($params['area']))
                $main_select
                    ->join(array('dt2' => 'v_regional_market'), 'dt2.id=v_store.district AND dt2.parent = v_store.regional_market', array())
                    ->join(array('pr2' => 'v_regional_market'), 'pr2.id=dt2.parent', array())
                    ->where('pr2.area_id IN (?)', $params['area']);
            elseif (is_numeric($params['area']))
                $main_select
                    ->join(array('dt2' => 'v_regional_market'), 'dt2.id=v_store.district AND dt2.parent = v_store.regional_market', array())
                    ->join(array('pr2' => 'v_regional_market'), 'pr2.id=dt2.parent', array())
                    ->where('pr2.area_id = ?', intval($params['area']));
            else
                $main_select->where('1=0', 1);
        }


        if (isset($params['name']) && $params['name']){
            $main_select->where('(s.name LIKE ?', '%'.$params['name'].'%');
            $main_select->orwhere('s.short_name LIKE ?)', '%'.$params['name'].'%');
        }
           

        if (isset($params['distributor_id']) && $params['distributor_id'])
            $main_select->where('s.d_id = ?', intval($params['distributor_id']));

        if(isset($params['sales_from']) and $params['sales_from']){
            $main_select->where('total_quantity >= ?',$params['sales_from']);
        }

        if(isset($params['sales_to']) and $params['sales_to']){
            $main_select->where('total_quantity <= ?',$params['sales_to']);
        }

        if(isset($params['store_level']) and $params['store_level']){
            if($params['store_level'] == 4){
                $main_select->where('l.id IS NULL OR l.id NOT IN (?)',array(1,2,3));
                $main_select->where('d.is_ka IS NULL OR d.is_ka = 0');
            }else{
                $main_select->where('l.id = ?',$params['store_level']);
            }
        }

        if(isset($params['channel']) and $params['channel'] >= 0){
            $main_select->where('d.is_ka = ?',$params['channel']);
        }

        if(isset($params['group_year_month_good']) and $params['group_year_month_good']){
            $main_select->order(array('year DESC','month DESC','s.id','total_quantity DESC'));
        }else{
            $main_select->order('total_quantity DESC');
        }

        if (isset($params['export']) && $params['export']){
            return $main_select->__toString();

        }

        if (isset($params['get_total_sales']) && $params['get_total_sales']){
            $cols_total = array(
                'total_quantity'        =>'SUM(a.total_quantity)', 
                'total_activated'       =>'SUM(a.total_activated)', 
                'total_value_activated' =>'SUM(a.total_value_activated)', 
                'total_value'           =>'SUM(a.total_value)'
            );
            $select_total = $db->select()
                ->from(array('a'=> $main_select),$cols_total);
            return $db->fetchRow($select_total);

        }
// if(!empty($_GET['dev'])){
//     echo '<pre>';
//     echo $main_select->__toString();
//     die;
// }
// 
        
        if ($limit){
            $main_select->limitPage($page, $limit);
        }
        
        PC::db($main_select->__toString());
        $result = $db->fetchAll($main_select);

        if ((!isset($params['export']) || !$params['export']) && $limit)
            $total = $db->fetchOne("SELECT FOUND_ROWS()");

        return $result;
    }

    public function fetchDistributor($page, $limit, &$total, $params)
    {
        if (!isset($params['from']) || !$params['from'])
            return false;

        if (!isset($params['to']) || !$params['to'])
            return false;

        $from = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
        $to = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");

        $before_closing = My_Kpi::isBeforeClosingDate($to);

        $db = Zend_Registry::get('db');
        $select = $db->select();
        $main_get = array();

        if (!$before_closing) {
            $get = array(
                'total_quantity'  => new Zend_Db_Expr("COUNT(DISTINCT i.imei_sn)"),
                'total_activated' => new Zend_Db_Expr("SUM(CASE WHEN (i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0) or ifnull(i.status,0) = 1 THEN 1 ELSE 0 END)"),
            );
        } else {
            $get = array(
                'total_quantity'  => new Zend_Db_Expr("COUNT(DISTINCT i.imei_sn)"),
                'total_activated' => new Zend_Db_Expr("SUM(CASE WHEN i.`status`>0 THEN 1 ELSE 0 END)"),
            );
        }

        if (isset($params['kpi']) && $params['kpi']) {
            $join_get = array(
                'good_id'     => new Zend_Db_Expr('IFNULL(g.good_id,g2.good_id)'),
                'color_id'    => new Zend_Db_Expr('IFNULL(IFNULL(g.color_id,g2.color_id), 0)'),
                'from_date'   => new Zend_Db_Expr(sprintf("CASE WHEN IFNULL(g.from_date,g2.from_date) < '%s' THEN '%s' ELSE IFNULL(g.from_date,g2.from_date) END", $from, $from)),
                'to_date'     => new Zend_Db_Expr(sprintf("CASE WHEN IFNULL(g.to_date,g2.to_date) IS NULL OR IFNULL(g.to_date,g2.to_date) > '%s' THEN '%s' ELSE IFNULL(g.to_date,g2.to_date) END", $to, $to)),
                'total_value' => new Zend_Db_Expr("SUM(IFNULL(g.price,g2.price)) * 0.8"),
            );

            if (!$before_closing) {
                $join_get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0 or ifnull(i.status,0) = 1 THEN IFNULL(g.price,g2.price) ELSE 0 END) * 0.8");
            } else {
                $join_get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0 or ifnull(i.status,0) = 1 THEN IFNULL(g.price,g2.price) ELSE 0 END) * 0.8");
            }

        } else {
            $get['total_value']    = new Zend_Db_Expr("SUM(i.`value`) * 0.8");
            $get['district_id']    = 'i.district_id';
            $get['province_id']    = 'i.province_id';
            $get['area_id']        = 'i.area_id';
            $get['sell_out_store'] = new Zend_Db_Expr("COUNT(DISTINCT i.store_id)");

            if (!$before_closing) {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0 or ifnull(i.status,0) = 1 THEN i.`value` ELSE 0 END) * 0.8");
            } else {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0 or ifnull(i.status,0) = 1 THEN i.`value` ELSE 0 END) * 0.8");
            }
        }

        if(isset($params['group_year_month_good']) and $params['group_year_month_good']){
            $get['year']    = 'YEAR(i.timing_date)';
            $get['month']   = 'MONTH(i.timing_date)';
            $get['good_id'] = 'i.good_id';
            $main_get[]     = 'year';
            $main_get[]     = 'month';
            $main_get[]     = 'good_id';
        }

        $get['store_chanel'] = '(CASE WHEN IFNULL(d.is_ka,0) = 0 THEN "IND" ELSE "KA" END)';
        $get['store_level'] = 'e.name';

        $date_level = (isset($to) and $to) ? ("'".$to."'") : 'CURDATE()';

        $select->from(array('i' => $this->_name), $get)
            ->joinRight(array('s' => 'store'), 's.id=i.store_id', array('store_id' => 's.id'))
            ->joinRight(array('d' => WAREHOUSE_DB.'.distributor'), 'd.id=s.d_id', array('dealer_id' => 'd.id'))
            ->joinLeft(array('c'=> 'dealer_loyalty'),'c.dealer_id = d.id AND '.$date_level.' BETWEEN c.from_date AND c.to_date',array())
            ->joinLeft(array('e'=> 'loyalty_plan'),'e.id = c.loyalty_plan_id',array());


        if (isset($params['kpi']) && $params['kpi']) {

            $select->joinLeft(
                array('g' => WAREHOUSE_DB.'.good_price_log'),
                '   g.good_id=i.good_id
                    AND IFNULL(g.color_id,0)  <> 0
                    AND IFNULL(g.color_id,0) = i.color_id
                    AND g.from_date <= DATE(i.timing_date)
                    AND ( g.to_date IS NULL OR g.to_date = 0 OR g.to_date >= DATE(i.timing_date) )',
                $join_get
            );

            $select->joinLeft(
                array('g2' => WAREHOUSE_DB.'.good_price_log'),
                '   g2.good_id=i.good_id
                    AND IFNULL(g2.color_id,0)  = 0
                    AND g2.from_date <= DATE(i.timing_date)
                    AND ( g2.to_date IS NULL OR g2.to_date = 0 OR g2.to_date >= DATE(i.timing_date) )',
                array()
            );
        }

        if (isset($params['sale_id']) && $params['sale_id'])
            $select->where('i.sale_id = ?', intval($params['sale_id']));

        if (isset($params['leader_id']) && $params['leader_id'])
            $select->where('i.leader_id = ?', intval($params['leader_id']));

        /*
        if (isset($params['area_list']) && $params['area_list']) {
            if (is_array($params['area_list']) && count($params['area_list']))
                    $select->where('i.area_id IN (?)', $params['area_list']);
            elseif (is_numeric($params['area_list']))
                    $select->where('i.area_id = ?', intval($params['area_list']));
            else
                $select->where('1=0', 1);
        }

        if (isset($params['district']) && $params['district']) {
            if (is_array($params['district']) && count($params['district']))
                    $select->where('i.district_id IN (?)', $params['district']);
            elseif (is_numeric($params['district']))
                    $select->where('i.district_id = ?', intval($params['district']));
            else
                $select->where('1=0', 1);
        }elseif (isset($params['province']) && $params['province']) {
            if (is_array($params['province']) && count($params['province']))
                    $select->where('i.province_id IN (?)', $params['province']);
            elseif (is_numeric($params['province']))
                    $select->where('i.province_id = ?', intval($params['province']));
            else
                $select->where('1=0', 1);
        }elseif (isset($params['area']) && $params['area']) {
            if (is_array($params['area']) && count($params['area']))
                    $select->where('i.area_id IN (?)', $params['area']);
            elseif (is_numeric($params['area']))
                    $select->where('i.area_id = ?', intval($params['area']));
            else
                $select->where('1=0', 1);
        }else{

        }        
        */
       
        if (isset($params['distributor']) && $params['distributor']) {
            if (is_array($params['distributor']) && count($params['distributor']))
                $select->where('d.id IN (?)', $params['distributor']);
            elseif (is_numeric($params['distributor']))
                $select->where('d.id = ?', intval($params['distributor']));
            else
                $select->where('1=0', 1);
        }

        if (isset($params['store_list']) && $params['store_list']) {
            if (is_array($params['store_list']) && count($params['store_list']))
                    $select->where('i.store_id IN (?)', $params['store_list']);
            elseif (is_numeric($params['store_list']))
                    $select->where('i.store_id = ?', intval($params['store_list']));
            else
                $select->where('1=0', 1);
        }

        if (isset($params['from']) && $params['from'])
            $select->where('DATE(i.timing_date) >= ?', $from);

        if (isset($params['to']) && $params['to'])
            $select->where('DATE(i.timing_date) <= ?', $to);

        if (isset($params['name']) && $params['name']) {
            $select->where('d.title LIKE ?', '%'.$params['name'].'%');
        }

        
        if (isset($params['kpi']) && $params['kpi']){
            $select->group( array('d.id', 'IFNULL(g.good_id,g2.good_id)', 'IFNULL(g.color_id,g2.color_id)', 'IFNULL(g.from_date,g2.from_date)') );
        }elseif(isset($params['group_year_month_good']) and $params['group_year_month_good']){
            $select->group(array('d.id','YEAR(i.timing_date)','MONTH(i.timing_date)','i.good_id'));
        }else{
            $select->group('d.id');
        }
        

        if (isset($params['export']) && $params['export']){
            return $select->__toString();
        }

        if (isset($params['kpi']) && $params['kpi']){
            $main_get = array('B.good_id', 'B.color_id', 'B.from_date', 'B.to_date');
        }


        $main_get = array_merge($main_get, array(
            'total_quantity'        => 'B.total_quantity',
            'total_activated'       => 'B.total_activated',
            'total_value_activated' => 'B.total_value_activated',
            'total_value'           => 'B.total_value',
            'sell_out_store'        => 'B.sell_out_store',
            'store_level'           => new Zend_Db_Expr('(CASE WHEN e.name IS NOT NULL THEN e.name ELSE "NORMAL" END)')
        ));

        $date_level = (isset($to) and $to) ? ("'".$to."'") : 'CURDATE()';
        $cols = array(
                'store_id'        => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS d.id'),
                'store_name'      => 'd.title',
                'store_district'  => 'd.district',
                'company_address' => 'd.add',
                'd_id'            => 'd.parent',
                'partner_id'      => 'd.partner_id'
            );

        if (isset($params['get_total_sales']) && $params['get_total_sales']){
            $cols = array('store_id'        => 'd.id');
        }

        $main_select = $db->select()
            ->distinct()
            ->from(
                array('d' => WAREHOUSE_DB.'.distributor'),$cols)
            ->joinLeft(array('B' => $select), 'B.dealer_id=d.id', $main_get)
            ->joinLeft(array('c'=> 'dealer_loyalty'),'c.dealer_id = d.id AND '.$date_level.' BETWEEN c.from_date AND c.to_date',array())
            ->joinLeft(array('e'=> 'loyalty_plan'),'e.id = c.loyalty_plan_id',array())
            ->join(array('f'=>'v_regional_market'),'f.id = d.district',array())
            ->join(array('g'=>'v_regional_market'),'g.id = f.parent',array())
        ;

        if (isset($params['distributor']) && $params['distributor']) {
            if (is_array($params['distributor']) && count($params['distributor']))
                $main_select->where('d.id IN (?)', $params['distributor']);
            elseif (is_numeric($params['distributor']))
                $main_select->where('d.id = ?', intval($params['distributor']));
            else
                $main_select->where('1=0', 1);
        }

        if (isset($params['store_list']) && $params['store_list']) {
            if (is_array($params['store_list']) && count($params['store_list']))
                $main_select->where('B.store_id IN (?)', $params['store_list']);
            elseif (is_numeric($params['store_list']))
                $main_select->where('B.store_id = ?', intval($params['store_list']));
            else
                $main_select->where('1=0', 1);
        }

        if (isset($params['area_list']) && $params['area_list']) {
            if (is_array($params['area_list']) && count($params['area_list']))
                $main_select->where('g.area_id IN (?)', $params['area_list']);
            elseif (is_numeric($params['area_list']))
                $main_select->where('g.area_id = ?', intval($params['area_list']));
            else
                $main_select->where('1=0', 1);
        }

        if (isset($params['district']) && $params['district']) {
            if (is_array($params['district']) && count($params['district']))
                $main_select->where('f.id IN (?)', $params['district']);
            elseif (is_numeric($params['district']))
                $main_select->where('f.id = ?', intval($params['district']));
            else
                $main_select->where('1=0', 1);

        } elseif (isset($params['province']) && $params['province']) {
            if (is_array($params['province']) && count($params['province']))
                $main_select->where('g.id IN (?)', $params['province']);
            elseif (is_numeric($params['province']))
                $main_select->where('g.id = ?', intval($params['province']));
            else
                $main_select->where('1=0', 1);

        } elseif (isset($params['area']) && $params['area']) {
            if (is_array($params['area']) && count($params['area']))
                $main_select->where('g.area_id IN (?)', $params['area']);
            elseif (is_numeric($params['area']))
                $main_select->where('g.area_id = ?', intval($params['area']));
            else
                $main_select->where('1=0', 1);
        }

        if (isset($params['name']) && $params['name'])
            $main_select->where('d.title LIKE ?', '%'.$params['name'].'%');

        if (isset($params['store_level']) && $params['store_level']){
            $main_select->where('e.id = ?', intval($params['store_level']));
        }

        if(isset($params['group_year_month_good']) and $params['group_year_month_good']){
            $main_select->group(array('d.id','year','month','good_id'));
        }else{
            $main_select->group('d.id');
        }

        if(isset($params['group_year_month_good']) and $params['group_year_month_good']){
            $main_select->order(array('year','month','d.id','total_quantity DESC'));
        }else{
            $main_select->order('total_quantity DESC');
        }
// if(!empty($_GET['dev'])){
//     echo '<pre>';
//     echo($main_select->__toString());
//     exit;
// }
        if (isset($params['get_total_sales']) && $params['get_total_sales']){
            $cols_total = array(
                'total_quantity'        =>'SUM(a.total_quantity)', 
                'total_activated'       =>'SUM(a.total_activated)', 
                'total_value_activated' =>'SUM(a.total_value_activated)', 
                'total_value'           =>'SUM(a.total_value)'
            );
            $select_total = $db->select()
                ->from(array('a'=> $main_select),$cols_total);
            return $db->fetchRow($select_total);
        }

        if ($limit){
            $main_select->limitPage($page, $limit);
        }


        
        $result = $db->fetchAll($main_select);

        if ((!isset($params['export']) || !$params['export']) && $limit)
            $total = $db->fetchOne("SELECT FOUND_ROWS()");

        return $result;
    }
    
    public function fetchDistributorBi($page, $limit, &$total, $params)
    {
        if (!isset($params['from']) || !$params['from'])
            return false;

        if (!isset($params['to']) || !$params['to'])
            return false;

        $from = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
        $to = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");

        $before_closing = My_Kpi::isBeforeClosingDate($to);

        $db = Zend_Registry::get('db');
        $select = $db->select();
        $main_get = array();

        if (!$before_closing) {
            $get = array(
                'total_quantity'  => new Zend_Db_Expr("COUNT(DISTINCT i.imei_sn)"),
                'total_activated' => new Zend_Db_Expr("SUM(CASE WHEN i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0 THEN 1 ELSE 0 END)"),
            );
        } else {
            $get = array(
                'total_quantity'  => new Zend_Db_Expr("COUNT(DISTINCT i.imei_sn)"),
                'total_activated' => new Zend_Db_Expr("SUM(CASE WHEN i.`status`>0 THEN 1 ELSE 0 END)"),
            );
        }

        if (isset($params['kpi']) && $params['kpi']) {
            $join_get = array(
                'g.good_id',
                'color_id'    => new Zend_Db_Expr('IFNULL(g.color_id, 0)'),
                'from_date'   => new Zend_Db_Expr(sprintf("CASE WHEN g.from_date < '%s' THEN '%s' ELSE g.from_date END", $from, $from)),
                'to_date'     => new Zend_Db_Expr(sprintf("CASE WHEN g.to_date IS NULL OR g.to_date > '%s' THEN '%s' ELSE g.to_date END", $to, $to)),
                'total_value' => new Zend_Db_Expr("SUM(g.price) * 0.8"),
            );

            if (!$before_closing) {
                $join_get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0 THEN g.price ELSE 0 END) * 0.8");
            } else {
                $join_get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`status`> 0 THEN g.price ELSE 0 END) * 0.8");
            }

        } else {
            $get['total_value']    = new Zend_Db_Expr("SUM(i.`value`) * 0.8");
            $get['district_id']    = 'i.district_id';
            $get['province_id']    = 'i.province_id';
            $get['area_id']        = 'i.area_id';
            $get['sell_out_store'] = new Zend_Db_Expr("COUNT(DISTINCT i.store_id)");

            if (!$before_closing) {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0 THEN i.`value` ELSE 0 END) * 0.8");
            } else {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`status`> 0 THEN i.`value` ELSE 0 END) * 0.8");
            }
        }

        if(isset($params['group_year_month_good']) and $params['group_year_month_good']){
            $get['year']    = 'YEAR(i.timing_date)';
            $get['month']   = 'MONTH(i.timing_date)';
            $get['good_id'] = 'i.good_id';
            $main_get[]     = 'year';
            $main_get[]     = 'month';
            $main_get[]     = 'good_id';
        }


        $select->from(array('i' => $this->_name), $get)
            ->joinRight(array('s' => 'store'), 's.id=i.store_id', array('store_id' => 's.id'))
            ->joinLeft(array('d' => WAREHOUSE_DB.'.distributor'), 'd.id=s.d_id', array('dealer_id' => 'd.id'));


        if (isset($params['kpi']) && $params['kpi']) {
            $select->join(
                array('g' => WAREHOUSE_DB.'.good_price_log'),
                'g.good_id=i.good_id
                AND
                    CASE WHEN g.color_id <> 0 AND g.color_id IS NOT NULL THEN i.color_id=g.color_id
                    ELSE i.color_id NOT IN (
                            SELECT '.WAREHOUSE_DB.'.`good_price_log`.color_id
                            FROM '.WAREHOUSE_DB.'.`good_price_log`
                            WHERE '.WAREHOUSE_DB.'.`good_price_log`.color_id <> 0
                            AND '.WAREHOUSE_DB.'.`good_price_log`.good_id = i.good_id
                            AND '.WAREHOUSE_DB.'.`good_price_log`.from_date <= DATE(i.timing_date)
                            AND ('.WAREHOUSE_DB.'.`good_price_log`.to_date IS NULL OR '.WAREHOUSE_DB.'.`good_price_log`.to_date = 0 OR '.WAREHOUSE_DB.'.`good_price_log`.to_date >= DATE(i.timing_date))
                        )
                    END
                AND g.from_date<= DATE(i.timing_date)
                AND ( g.to_date IS NULL OR g.to_date = 0 OR g.to_date >= DATE(i.timing_date) )',
                $join_get
            );
        }

        if (isset($params['sale_id']) && $params['sale_id'])
            $select->where('i.sale_id = ?', intval($params['sale_id']));

        if (isset($params['leader_id']) && $params['leader_id'])
            $select->where('i.leader_id = ?', intval($params['leader_id']));
       
        if (isset($params['distributor']) && $params['distributor']) {
            if (is_array($params['distributor']) && count($params['distributor']))
                $select->where('d.id IN (?)', $params['distributor']);
            elseif (is_numeric($params['distributor']))
                $select->where('d.id = ?', intval($params['distributor']));
            else
                $select->where('1=0', 1);
        }

        if (isset($params['store_list']) && $params['store_list']) {
            if (is_array($params['store_list']) && count($params['store_list']))
                    $select->where('i.store_id IN (?)', $params['store_list']);
            elseif (is_numeric($params['store_list']))
                    $select->where('i.store_id = ?', intval($params['store_list']));
            else
                $select->where('1=0', 1);
        }

        if (isset($params['from']) && $params['from'])
            $select->where("i.timing_date >= '".$from." 00:00:00'", null);

        if (isset($params['to']) && $params['to'])
            $select->where("i.timing_date <= '".$to." 23:59:59'", null);

        if (isset($params['name']) && $params['name']) {
            $select->where('d.title LIKE ?', '%'.$params['name'].'%');
        }

        /*
        if (isset($params['get_total_sales']) && $params['get_total_sales']){
            return $db->fetchRow($select);
        }
        */
        
        if (isset($params['kpi']) && $params['kpi']){
            $select->group( array('d.id', 'g.good_id', 'g.color_id', 'g.from_date') );
        }elseif(isset($params['group_year_month_good']) and $params['group_year_month_good']){
            $select->group(array('d.id','YEAR(i.timing_date)','MONTH(i.timing_date)','i.good_id'));
        }else{
            $select->group('d.id');
        }
        

        if (isset($params['export']) && $params['export'])
            return $select->__toString();

        if (isset($params['kpi']) && $params['kpi'])
            $main_get = array('B.good_id', 'B.color_id', 'B.from_date', 'B.to_date');


        $main_get = array_merge($main_get, array(
            'total_quantity'        => 'B.total_quantity',
            'total_activated'       => 'B.total_activated',
            'total_value_activated' => 'B.total_value_activated',
            'total_value'           => 'B.total_value',
            'sell_out_store'        => 'B.sell_out_store',
            'store_level'           => new Zend_Db_Expr('(CASE WHEN e.name IS NOT NULL THEN e.name ELSE "NORMAL" END)')
        ));

        $date_level = (isset($to) and $to) ? ("'".$to."'") : 'CURDATE()';
        $cols = array(
                'store_id'        => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS d.id'),
                'store_name'      => 'd.title',
                'store_district'  => 'd.district',
                'company_address' => 'd.add',
                'd_id'            => 'd.parent'
            );

        if (isset($params['get_total_sales']) && $params['get_total_sales']){
            $cols = array('store_id'        => 'd.id');
        }

        $main_select = $db->select()
            ->distinct()
            ->from(
                array('d' => WAREHOUSE_DB.'.distributor'),$cols)
            ->joinLeft(array('B' => $select), 'B.dealer_id=d.id', $main_get)
            ->joinLeft(array('c'=> 'dealer_loyalty'),'c.dealer_id = d.id AND '.$date_level.' BETWEEN c.from_date AND c.to_date',array())
            ->joinLeft(array('e'=> 'loyalty_plan'),'e.id = c.loyalty_plan_id',array())
            ->join(array('f'=>'v_regional_market'),'f.id = d.district',array())
            ->join(array('g'=>'v_regional_market'),'g.id = f.parent',array())
        ;

        if (isset($params['distributor']) && $params['distributor']) {
            if (is_array($params['distributor']) && count($params['distributor']))
                $main_select->where('d.id IN (?)', $params['distributor']);
            elseif (is_numeric($params['distributor']))
                $main_select->where('d.id = ?', intval($params['distributor']));
            else
                $main_select->where('1=0', 1);
        }

        if (isset($params['store_list']) && $params['store_list']) {
            if (is_array($params['store_list']) && count($params['store_list']))
                $main_select->where('B.store_id IN (?)', $params['store_list']);
            elseif (is_numeric($params['store_list']))
                $main_select->where('B.store_id = ?', intval($params['store_list']));
            else
                $main_select->where('1=0', 1);
        }

        if (isset($params['area_list']) && $params['area_list']) {
            if (is_array($params['area_list']) && count($params['area_list']))
                $main_select->where('g.area_id IN (?)', $params['area_list']);
            elseif (is_numeric($params['area_list']))
                $main_select->where('g.area_id = ?', intval($params['area_list']));
            else
                $main_select->where('1=0', 1);
        }

        if (isset($params['district']) && $params['district']) {
            if (is_array($params['district']) && count($params['district']))
                $main_select->where('f.id IN (?)', $params['district']);
            elseif (is_numeric($params['district']))
                $main_select->where('f.id = ?', intval($params['district']));
            else
                $main_select->where('1=0', 1);

        } elseif (isset($params['province']) && $params['province']) {
            if (is_array($params['province']) && count($params['province']))
                $main_select->where('g.id IN (?)', $params['province']);
            elseif (is_numeric($params['province']))
                $main_select->where('g.id = ?', intval($params['province']));
            else
                $main_select->where('1=0', 1);

        } elseif (isset($params['area']) && $params['area']) {
            if (is_array($params['area']) && count($params['area']))
                $main_select->where('g.area_id IN (?)', $params['area']);
            elseif (is_numeric($params['area']))
                $main_select->where('g.area_id = ?', intval($params['area']));
            else
                $main_select->where('1=0', 1);
        }

        if (isset($params['name']) && $params['name'])
            $main_select->where('d.title LIKE ?', '%'.$params['name'].'%');

        if (isset($params['store_level']) && $params['store_level']){
            $main_select->where('e.id = ?', intval($params['store_level']));
        }

        if(isset($params['group_year_month_good']) and $params['group_year_month_good']){
            $main_select->group(array('d.id','year','month','good_id'));
        }else{
            $main_select->group('d.id');
        }

        if(isset($params['group_year_month_good']) and $params['group_year_month_good']){
            $main_select->order(array('year','month','d.id','total_quantity DESC'));
        }else{
            $main_select->order('total_quantity DESC');
        }

        if (isset($params['get_total_sales']) && $params['get_total_sales']){
            $cols_total = array(
                'total_quantity'        =>'SUM(a.total_quantity)', 
                'total_activated'       =>'SUM(a.total_activated)', 
                'total_value_activated' =>'SUM(a.total_value_activated)', 
                'total_value'           =>'SUM(a.total_value)'
            );
            $select_total = $db->select()
                ->from(array('a'=> $main_select),$cols_total);
            return $db->fetchRow($select_total);
        }

        if ($limit){
            $main_select->limitPage($page, $limit);
        }

        $result = $db->fetchAll($main_select);

        if ((!isset($params['export']) || !$params['export']) && $limit)
            $total = $db->fetchOne("SELECT FOUND_ROWS()");

        return $result;
    }

    public function fetchProduct($page, $limit, &$total, $params)
    {
        if (!isset($params['from']) || !$params['from'] || !date_create_from_format("d/m/Y", $params['from']))
            return false;

        if (!isset($params['to']) || !$params['to'] || !date_create_from_format("d/m/Y", $params['to']))
            return false;

        $from = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
        $to = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");

        $before_closing = My_Kpi::isBeforeClosingDate($to);

        $db = Zend_Registry::get('db');
        $select = $db->select();

        if (!$before_closing) {
            $get = array(
                'total_quantity'  => new Zend_Db_Expr("COUNT(DISTINCT i.imei_sn)"),
                'total_activated' => new Zend_Db_Expr("SUM(CASE WHEN i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0 THEN 1 ELSE 0 END)"),
            );
        } else {
            $get = array(
                'total_quantity'  => new Zend_Db_Expr("COUNT(DISTINCT i.imei_sn)"),
                'total_activated' => new Zend_Db_Expr("SUM(CASE WHEN i.`status`>0 THEN 1 ELSE 0 END)"),
            );
        }

        $get['good_id'] = 'i.good_id';

        if (isset($params['kpi']) && $params['kpi']) {
            $join_get = array(
                'good_id'     => new Zend_Db_Expr('IFNULL(g.good_id,g2.good_id)'),
                'color_id'    => new Zend_Db_Expr('IFNULL(IFNULL(g.color_id,g2.color_id), 0)'),
                'from_date'   => new Zend_Db_Expr(sprintf("CASE WHEN IFNULL(g.from_date,g2.from_date) < '%s' THEN '%s' ELSE IFNULL(g.from_date,g2.from_date) END", $from, $from)),
                'to_date'     => new Zend_Db_Expr(sprintf("CASE WHEN IFNULL(g.to_date,g2.to_date) IS NULL OR IFNULL(g.to_date,g2.to_date) > '%s' THEN '%s' ELSE IFNULL(g.to_date,g2.to_date) END", $to, $to)),
                'total_value' => new Zend_Db_Expr("SUM(IFNULL(g.price,g2.price)) * 0.8"),
            );

            if (!$before_closing) {
                $join_get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0 THEN IFNULL(g.price,g2.price) ELSE 0 END) * 0.8");
            } else {
                $join_get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`status`> 0 THEN IFNULL(g.price,g2.price) ELSE 0 END) * 0.8");
            }
        } else {
            $get['total_value'] = new Zend_Db_Expr("SUM(i.`value`) * 0.8");

            if (!$before_closing) {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0 THEN i.`value` ELSE 0 END) * 0.8");
            } else {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`status`> 0 THEN i.`value` ELSE 0 END) * 0.8");
            }
        }

        if(isset($params['group_year_month_good']) and $params['group_year_month_good']){
            $get['year'] = 'YEAR(i.timing_date)';
            $get['month'] = 'MONTH(i.timing_date)';
        }

        $select->from(array('i' => $this->_name), $get);

        if (isset($params['kpi']) && $params['kpi']) {
            /*
            $select->join(
                array('g' => WAREHOUSE_DB.'.good_price_log'),
                'g.good_id=i.good_id
                AND
                    CASE WHEN g.color_id <> 0 AND g.color_id IS NOT NULL THEN i.color_id=g.color_id
                    ELSE i.color_id NOT IN (
                            SELECT '.WAREHOUSE_DB.'.`good_price_log`.color_id
                            FROM '.WAREHOUSE_DB.'.`good_price_log`
                            WHERE '.WAREHOUSE_DB.'.`good_price_log`.color_id <> 0
                            AND '.WAREHOUSE_DB.'.`good_price_log`.good_id = i.good_id
                            AND '.WAREHOUSE_DB.'.`good_price_log`.from_date <= DATE(i.timing_date)
                            AND ('.WAREHOUSE_DB.'.`good_price_log`.to_date IS NULL OR '.WAREHOUSE_DB.'.`good_price_log`.to_date = 0 OR '.WAREHOUSE_DB.'.`good_price_log`.to_date >= DATE(i.timing_date))
                        )
                    END
                AND g.from_date<= DATE(i.timing_date)
                AND ( g.to_date IS NULL OR g.to_date = 0 OR g.to_date >= DATE(i.timing_date) )',
                $join_get
            );
            */
            $select->joinLeft(
                array('g' => WAREHOUSE_DB.'.good_price_log'),
                '   g.good_id=i.good_id
                    AND IFNULL(g.color_id,0)  <> 0
                    AND IFNULL(g.color_id,0) = i.color_id
                    AND g.from_date <= DATE(i.timing_date)
                    AND ( g.to_date IS NULL OR g.to_date = 0 OR g.to_date >= DATE(i.timing_date) )',
                $join_get
            );

            $select->joinLeft(
                array('g2' => WAREHOUSE_DB.'.good_price_log'),
                '   g2.good_id=i.good_id
                    AND IFNULL(g2.color_id,0)  = 0
                    AND g2.from_date <= DATE(i.timing_date)
                    AND ( g2.to_date IS NULL OR g2.to_date = 0 OR g2.to_date >= DATE(i.timing_date) )',
                array()
            );
        }
        
        $select->join(array('s' => 'store'), 's.id=i.store_id', array());

        if (isset($params['sale_id']) && $params['sale_id'])
            $select->where('i.sale_id = ?', intval($params['sale_id']));

        if (isset($params['leader_id']) && $params['leader_id'])
            $select->where('i.leader_id = ?', intval($params['leader_id']));

        if (isset($params['distributor_id']) && $params['distributor_id'])
            $select->where('s.d_id = ?', intval($params['distributor_id']));

        if (isset($params['area']) && $params['area']) {
            if (is_array($params['area']) && count($params['area']))
                    $select->where('i.area_id IN (?)', $params['area']);
            elseif (is_numeric($params['area']))
                    $select->where('i.area_id = ?', intval($params['area']));
            else
                $select->where('1=0', 1);
        }

        if (isset($params['area_list']) && $params['area_list']) {
            if (is_array($params['area_list']) && count($params['area_list']))
                    $select->where('i.area_id IN (?)', $params['area_list']);
            elseif (is_numeric($params['area_list']))
                    $select->where('i.area_id = ?', intval($params['area_list']));
            else
                $select->where('1=0', 1);
        }

        if (isset($params['district']) && $params['district']) {
            if (is_array($params['district']) && count($params['district']))
                    $select->where('i.district_id IN (?)', $params['district']);
            elseif (is_numeric($params['district']))
                    $select->where('i.district_id = ?', intval($params['district']));
            else
                $select->where('1=0', 1);
        }

        if (isset($params['province']) && $params['province']) {
            if (is_array($params['province']) && count($params['province']))
                    $select->where('i.province_id IN (?)', $params['province']);
            elseif (is_numeric($params['province']))
                    $select->where('i.province_id = ?', intval($params['province']));
            else
                $select->where('1=0', 1);
        }

        if (isset($params['store']) && $params['store']) {
            if (is_array($params['store']) && count($params['store']))
                    $select->where('i.store_id IN (?)', $params['store']);
            elseif (is_numeric($params['store']))
                    $select->where('i.store_id = ?', intval($params['store']));
            else
                $select->where('1=0', 1);
        }

        if (isset($params['store_list']) && $params['store_list']) {
            if (is_array($params['store_list']) && count($params['store_list']))
                    $select->where('i.store_id IN (?)', $params['store_list']);
            elseif (is_numeric($params['store_list']))
                    $select->where('i.store_id = ?', intval($params['store_list']));
            else
                $select->where('1=0', 1);
        }

        if(isset($params['good_id']) and $params['good_id']){
            if (is_array($params['good_id']) && count($params['good_id']))
                $select->where('i.good_id IN (?)', $params['good_id']);
            elseif (is_numeric($params['good_id']))
                $select->where('i.good_id = ?', intval($params['good_id']));
            else
                $select->where('1=0', 1);
        }

        if (isset($params['from']) && $params['from'])
            $select->where('DATE(i.timing_date) >= ?', $from);

        if (isset($params['to']) && $params['to'])
            $select->where('DATE(i.timing_date) <= ?', $to);

        if (isset($params['name']) && $params['name']) {
            $select->joinRight(array('gg' => 'warehouse.good'), 'gg.id=i.good_id', array());
            $select->where('gg.name LIKE ?', '%'.$params['name'].'%');
        }

        if (isset($params['get_total_sales']) && $params['get_total_sales'])
            return $db->fetchRow($select);

        if (isset($params['kpi']) && $params['kpi'])
            $select->group( array('i.store_id', 'IFNULL(g.good_id,g2.good_id)', 'IFNULL(g.color_id,g2.color_id)', 'IFNULL(g.from_date,g2.from_date)') );
        elseif(isset($params['group_year_month_good']) and $params['group_year_month_good']){
            $select->group( array('YEAR(i.timing_date)', 'MONTH(i.timing_date)', 'i.good_id') );
        }else{
            $select->group('i.good_id');
        }


        if (isset($params['kpi']) && $params['kpi'])
            $main_get = array('B.good_id', 'B.color_id', 'B.from_date', 'B.to_date');
        else
            $main_get = array();

        if(isset($params['group_year_month_good']) and $params['group_year_month_good']) {
            $main_get[] = 'year';
            $main_get[] = 'month';
        }

        $main_get = array_merge($main_get, array('B.total_quantity', 'B.total_activated', 'B.total_value_activated', 'B.total_value'));

        $main_select = $db->select()
            ->from(array('w' => WAREHOUSE_DB.'.good'), array('good_id' => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS w.id'), 'product_name' => 'w.name', 'product_desc' => 'w.desc'))
            ->joinLeft(array('B' => $select), 'B.good_id=w.id', $main_get);

        $main_select->where('w.cat_id = ?', 11);

        $main_select->where('w.del IS NULL OR w.del = ?',0);

        if (isset($params['good_id']) && $params['good_id']) {
            if (is_array($params['good_id']) && count($params['good_id']))
                $main_select->where('w.id IN (?)', $params['good_id']);
            elseif (is_numeric($params['good_id']))
                $main_select->where('w.id = ?', intval($params['good_id']));
            else
                $main_select->where('1=0', 1);
        }

        if (isset($params['name']) && $params['name'])
            $main_select->where('w.name LIKE ?', '%'.$params['name'].'%');

        $main_select->order('total_quantity DESC');
// if(!empty($_GET['dev'])){
//     echo $main_select->__toString();
//     exit;
// }
        if (isset($params['export']) && $params['export'])
            return $main_select->__toString();

        if ($limit){
            $main_select->limitPage($page, $limit);
        }

        $result = $db->fetchAll($main_select);

        if ((!isset($params['export']) || !$params['export']) && $limit)
            $total = $db->fetchOne("SELECT FOUND_ROWS()");

        return $result;

    }

    public function getKpiType($staff_id, $from, $to)
    {
        $pg = $sale = $leader = $pb_sale = 0 ;

        $where = array();
        $where[] = $this->getAdapter()->quoteInto('pg_id = ?', $staff_id);
        $where[] = $this->getAdapter()->quoteInto('pg_id <> sale_id', 1);
        $where[] = $this->getAdapter()->quoteInto('DATE(timing_date) >= ?', $from);
        $where[] = $this->getAdapter()->quoteInto('DATE(timing_date) <= ?', $to);
        $result = $this->fetchRow($where);

        if ($result) $pg = 1;

        $where = array();
        $where[] = $this->getAdapter()->quoteInto('sale_id = ?', $staff_id);
        $where[] = $this->getAdapter()->quoteInto('DATE(timing_date) >= ?', $from);
        $where[] = $this->getAdapter()->quoteInto('DATE(timing_date) <= ?', $to);
        $result = $this->fetchRow($where);

        if ($result) $sale = 1;

        $where   = array();
        $where[] = $this->getAdapter()->quoteInto('leader_id = ?', $staff_id);
        $where[] = $this->getAdapter()->quoteInto('DATE(timing_date) >= ?', $from);
        $where[] = $this->getAdapter()->quoteInto('DATE(timing_date) <= ?', $to);
        $result  = $this->fetchRow($where);

        if ($result) {
            $pg = 0;
            $leader = 1;
        }

        $where = array();
        $where[] = $this->getAdapter()->quoteInto('pb_sale_id = ?', $staff_id);
        $where[] = $this->getAdapter()->quoteInto('DATE(timing_date) >= ?', $from);
        $where[] = $this->getAdapter()->quoteInto('DATE(timing_date) <= ?', $to);
        $result  = $this->fetchRow($where);

        if ($result) {
            $pb_sale = 1;
        }

        return array(
            'pg'      => $pg,
            'sale'    => $sale,
            'leader'  => $leader,
            'pb_sale' => $pb_sale
        );
    }

    /**
     * @author buu.pham
     * Danh sÃ¡ch IMEI chÆ°a active, cáº§n gá»i Ä‘iá»‡n kiá»ƒm tra thÃ´ng tin khÃ¡ch hÃ ng
     * @return object
     */
    public function fetchDetailPagination($page, $limit, &$total, $params)
    {
        $sql = "SELECT SQL_CALC_FOUND_ROWS
            DISTINCT
            st.`code` AS pg_code,
            st.firstname AS pg_name,
            st.lastname AS pg_name2,
            st.email AS pg_email,
            ".
            (isset($params['export']) && $params['export'] ? "
            st2.firstname AS sale_name,
            st2.lastname AS sale_name2,
            st3.firstname AS leader_name,
            st3.lastname AS leader_name2,
             " : " ")
            ."
            dt.id AS dealer_id,
            dt.title AS dealer_name,
            CONCAT(\"'\",im.sales_sn) AS sales_sn,
            m.invoice_time AS invoice_time,
            i.imei_sn AS imei,
            i.good_id,
            i.color_id,
            i.timing_date AS sell_out_date,
            CASE WHEN ia.activated_at=0 OR ISNULL(ia.activated_at) THEN '' ELSE ia.activated_at END AS activation_date,
            s.id AS store_id,
            s.`name` AS store_name,
            i.district_id,
            cc.id AS cc_id, cc.status, cc.info_checking, cc.network, cc.resurvey, i.value,
            c.`name` AS customer_name,
            CONCAT(\"'\",c.phone_number) AS phone_number,
            c.address AS address
        FROM
            imei_kpi i
        LEFT JOIN store s ON s.id = i.store_id
        LEFT JOIN staff st ON st.id = i.pg_id
        ".
        (isset($params['export']) && $params['export'] ? "
        LEFT JOIN staff st2 ON st2.id = i.sale_id
        LEFT JOIN staff st3 ON st3.id = i.leader_id
         " : " ")
        ."
        LEFT JOIN timing_sale ts ON i.imei_sn = ts.imei
        LEFT JOIN customer c ON ts.customer_id = c.id
        LEFT JOIN warehouse.distributor dt ON dt.id = i.distributor_id
        LEFT JOIN warehouse.imei im ON im.imei_sn=i.imei_sn
        LEFT JOIN warehouse.market m ON m.sn=im.sales_sn
        LEFT JOIN warehouse.imei_activation ia ON ia.imei_sn=i.imei_sn
        LEFT JOIN customer_check cc ON i.imei_sn=cc.imei_sn
        WHERE 1 "
        . (
            isset($params['from']) && date_create_from_format("d/m/Y", $params['from'])
            ? " AND i.timing_date >= '" . date_create_from_format("d/m/Y", $params['from'])->format('Y-m-d') . "' "
            : " "
        )
        . (
            isset($params['to']) && date_create_from_format("d/m/Y", $params['to'])
            ? " AND i.timing_date <= '" . date_create_from_format("d/m/Y", $params['to'])->format('Y-m-d') . " 23:59:59' "
            : " "
        )
        . (
            isset($params['inactive']) && $params['inactive']
            ? " AND (i.status = 0 OR i.status IS NULL) "
            : " AND (i.status > 1 AND i.status IS NOT NULL) "
        )
        . (
            isset($params['area_id']) && $params['area_id']
            ? " AND i.area_id = " . intval($params['area_id']) . " "
            : " "
        )
        . (
            isset($params['imei']) && $params['imei']
            ? " AND i.imei_sn = " . $params['imei'] . " "
            : " "
        )
        . (
            isset($params['imei_status']) && $params['imei_status']
            ? (1 == intval($params['imei_status']) ? " AND (ia.activated_at IS NOT NULL AND ia.activated_at <> 0) " : " AND (ia.activated_at IS NULL OR ia.activated_at = 0) " )
            : " "
        )
        . (
            isset($params['status']) && $params['status']
            ? " AND cc.status = " . intval($params['status']) . " "
            : " "
        )
        . (
            isset($params['resurvey']) && $params['resurvey']
            ? " AND cc.resurvey > 0 "
            : " "
        )

        . ($limit ? sprintf(" LIMIT %d ", $limit) : " ")
        . ($limit && ($limit*($page-1) > 0) ? sprintf(" OFFSET %d ", $limit*($page-1)) : " ");

        if (isset($params['export']) && $params['export']) return $sql;

        $db = Zend_Registry::get('db');
        $result = $db->query($sql);

        if (!isset($params['detail']) || !$params['detail'])
            $total = $db->fetchOne('SELECT FOUND_ROWS()');

        return $result;
    }

    public function fetchRegionShareAsm($params){

        if (!isset($params['from']) || !$params['from']){
            return false;
        }

        if (!isset($params['to']) || !$params['to']){
            return false;
        }

        $from           = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
        $to             = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");
        $before_closing = My_Kpi::isBeforeClosingDate($to);
        $db             = Zend_Registry::get('db');
        $select         = $db->select();

        $countActivateConditionBefore = $countActivateConditionAfter = ' (i.`activation_date` IS NOT NULL AND i.`activation_date` <>0) OR ((i.`activation_date` IS NULL OR i.`activation_date` =0) AND i.`status` > 0)';

        if (!$before_closing) {
            $get = array(
                'total_quantity'  => new Zend_Db_Expr("COUNT(DISTINCT i.imei_sn)"),
                'total_activated' => new Zend_Db_Expr("SUM(CASE WHEN i.`activation_date` IS NOT NULL AND i.`activation_date` <>0
                    THEN 1 ELSE 0 END)"),
            );
        } else {
            $get = array(
                'total_quantity'  => new Zend_Db_Expr("COUNT(DISTINCT i.imei_sn)"),
                'total_activated' => new Zend_Db_Expr("SUM(CASE WHEN i.`status` IS NOT NULL AND i.`status` > 0
                    THEN 1 ELSE 0 END)"),
            );
        }

        if (isset($params['kpi']) && $params['kpi']) {
        
            $join_get = array(
                'good_id'     => new Zend_Db_Expr('IFNULL(g.good_id,g2.good_id)'),
                'color_id'    => new Zend_Db_Expr('IFNULL(IFNULL(g.color_id,g2.color_id), 0)'),
                'from_date'   => new Zend_Db_Expr(sprintf("CASE WHEN IFNULL(g.from_date,g2.from_date) < '%s' THEN '%s' ELSE IFNULL(g.from_date,g2.from_date) END", $from, $from)),
                'to_date'     => new Zend_Db_Expr(sprintf("CASE WHEN IFNULL(g.to_date,g2.to_date) IS NULL OR IFNULL(g.to_date,g2.to_date) > '%s' THEN '%s' ELSE IFNULL(g.to_date,g2.to_date) END", $to, $to)),
                'total_value' => new Zend_Db_Expr("SUM(IFNULL(g.price,g2.price)) * 0.8"),
            );

            if (!$before_closing) {
                $join_get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0 THEN IFNULL(g.price,g2.price) ELSE 0 END) * 0.8");
            } else {
                $join_get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`status`> 0 THEN IFNULL(g.price,g2.price) ELSE 0 END) * 0.8");
            }

        } else {
            $get['total_value'] = new Zend_Db_Expr("SUM(i.`value`) * 0.8");
            if (!$before_closing) {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN $countActivateConditionAfter THEN i.`value` ELSE 0 END) * 0.8");
            } else {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN $countActivateConditionBefore THEN i.`value` ELSE 0 END) * 0.8");
            }
        }
        
        $get['sales_region_id'] = 'a.id';
        $get['sales_region_staff_log_id'] = 'b.id';
        $select->from(array('i' => $this->_name), $get);
        $select->join(array('a'=>'sales_region'), 'a.parent = i.area_id',array())
                ->join(array('b'=>'sales_region_staff_log'),'a.id = b.sales_region_id AND b.type = 1',array())
        ;
        $select->where('b.from_date <= DATE(i.timing_date)',1);
        $select->where('b.to_date IS NULL OR b.to_date >= DATE(i.timing_date)',1);
        $select->where('i.leader_id = 0 OR i.leader_id IS NULL');

        if (isset($params['kpi']) && $params['kpi']) {
            $select->joinLeft(
                array('g' => WAREHOUSE_DB.'.good_price_log'),
                '   g.good_id=i.good_id
                    AND IFNULL(g.color_id,0)  <> 0
                    AND IFNULL(g.color_id,0) = i.color_id
                    AND g.from_date <= DATE(i.timing_date)
                    AND ( g.to_date IS NULL OR g.to_date = 0 OR g.to_date >= DATE(i.timing_date) )',
                $join_get
            );

            $select->joinLeft(
                array('g2' => WAREHOUSE_DB.'.good_price_log'),
                '   g2.good_id=i.good_id
                    AND IFNULL(g2.color_id,0)  = 0
                    AND g2.from_date <= DATE(i.timing_date)
                    AND ( g2.to_date IS NULL OR g2.to_date = 0 OR g2.to_date >= DATE(i.timing_date) )',
                array()
            );
        }

        if (isset($params['from']) && $params['from']){
            $select->where('DATE(i.timing_date) >= ?', $from);
        }

        if (isset($params['to']) && $params['to']){
            $select->where('DATE(i.timing_date) <= ?', $to);
        }

        if (isset($params['kpi']) && $params['kpi']){
            $select->group( array('b.id', 'IFNULL(g.good_id,g2.good_id)', 'IFNULL(g.color_id,g2.color_id)', 'IFNULL(g.from_date,g2.from_date)') );
        }else{
            $select->group('b.id');
        }
// if(!empty($_GET['dev'])){
//     echo '<pre>';
//     print_r($select);
//     echo $select->__toString();
//     exit;
// }    
        return $db->fetchAll($select);
    }

    public function fetchStoreSellOut($params){
        $from = $params['from_date'];
        $to = $params['to_date'];
        $db = Zend_Registry::get('db');
        $cols = array(
                'distributor_id' => 'c.id',
                'store_sellout_number' => new Zend_Db_Expr('COUNT(DISTINCT a.store_id)')
            );
        $select = $db->select()
            ->from(array('a'=>'imei_kpi'),$cols)
            ->join(array('b'=>'store'),'a.store_id = b.id',array())
            ->joinRight(array('c'=>WAREHOUSE_DB.'.distributor'),'c.id = b.d_id',array())
            ->where('DATE(a.timing_date) >= ?',$from)
            ->where('DATE(a.timing_date) <= ?',$to)
            ->group('c.id');

        $list   =  $db->fetchAll($select);    
        $result = array();

        if(count($list)){
            foreach($list as $item){
                $result[$item['distributor_id']] = $item;
            }
        }
        //PC::debug($select->__toString());
        return $result;
    }

    public function fetchStoreSellOutKa($params){
        $from = $params['from_date'];
        $to = $params['to_date'];
        $db = Zend_Registry::get('db');
        $cols = array(
                'distributor_id' => 'IF(c.parent = 0, c.id, c.parent)',
                'store_sellout_number' => new Zend_Db_Expr('COUNT(DISTINCT a.store_id)')
            );
        $select = $db->select()
            ->from(array('a'=>'imei_kpi'),$cols)
            ->join(array('b'=>'store'),'a.store_id = b.id',array())
            ->joinRight(array('c'=>WAREHOUSE_DB.'.distributor'),'c.id = b.d_id',array())
            ->where('DATE(a.timing_date) >= ?',$from)
            ->where('DATE(a.timing_date) <= ?',$to)
            ->group('IF(c.parent = 0, c.id, c.parent)');

        $list   =  $db->fetchAll($select);    
        $result = array();

        if(count($list)){
            foreach($list as $item){
                $result[$item['distributor_id']] = $item;
            }
        }
        //PC::debug($select->__toString());
        return $result;
    }

    /*
    * controller action: /timing/kpi-review
    * ToÃ n create 2.7.2016
    */
     public function getListSale($listname, $from_month_last, $to_month_last, $from_month_now, $to_month_now )
    {

        $db = Zend_Registry::get('db');
        $select = $db->select();
        
       $select = "SELECT  MAX(s.quantity_last) quantity_last , MAX(s.kpi_sale_last) kpi_sale_last, MAX(s.kpi_sale_total_last) kpi_sale_total_last,
                MAX(s.quantity_now) quantity_now, MAX(s.kpi_sale_now) kpi_sale_now, MAX(s.kpi_sale_total_now) kpi_sale_total_now,
                s.sale_id, s.`value` , s.kpi_sale ,s.good_id , s.color_id,
                n.`name` as product,
                m.firstname , m.lastname,
                h.`name` color
                FROM 
                (
                        SELECT  b.quantity_last, b.kpi_sale_last, b.quantity_last * b.kpi_sale_last as kpi_sale_total_last,
                                        c.quantity_now  , c.kpi_sale_now, c.quantity_now * c.kpi_sale_now as kpi_sale_total_now ,
                                        a.sale_id, a.`value` , a.kpi_sale , a.good_id, a.color_id
                        FROM (
                                    SELECT COUNT(a.imei_sn) as quantity, a.*
                                    FROM imei_kpi a
                                    WHERE a.sale_id in (". $listname .") 
                                    AND     DATE(a.timing_date) between '". $from_month_last ."' and '". $to_month_now ."'
                                    GROUP BY  a.good_id, a.color_id, a.sale_id, a.kpi_sale
                                    ORDER BY a.timing_date
                        ) a
                        LEFT JOIN(
                                    SELECT COUNT(a.imei_sn) as quantity_last, a.* , a.kpi_sale as kpi_sale_last
                                    FROM imei_kpi a
                                    WHERE a.sale_id in (". $listname .") 
                                    AND     DATE(a.timing_date) between '". $from_month_last ."' and '". $to_month_last ."'
                                    GROUP BY  a.good_id, a.color_id, a.sale_id, a.kpi_sale
                                    ORDER BY a.timing_date
                        ) b
                        ON a.good_id = b.good_id AND a.color_id = b.color_id AND a.sale_id = b.sale_id AND a.kpi_sale = b.kpi_sale
                
                        LEFT JOIN(
                                    SELECT COUNT(a.imei_sn) as quantity_now, a.* , a.kpi_sale as kpi_sale_now
                                    FROM imei_kpi a
                                    WHERE a.sale_id in (". $listname .") 
                                    AND     DATE(a.timing_date) between '". $from_month_now ."' and '". $to_month_now ."'
                                    GROUP BY  a.good_id, a.color_id, a.sale_id, a.kpi_sale
                                    ORDER BY a.timing_date
                        ) c
                        ON a.good_id = c.good_id AND a.color_id = c.color_id AND a.sale_id = c.sale_id AND a.kpi_sale = c.kpi_sale
                        ORDER BY a.sale_id
                
                ) AS s
                LEFT JOIN warehouse.good as n ON s.good_id = n.id
                LEFT JOIN warehouse.good_color AS h ON s.color_id = h.id
                LEFT JOIN staff as m ON s.sale_id = m.id
                GROUP BY  s.good_id, s.color_id, s.sale_id
                ORDER BY s.sale_id, s.good_id";

        $total = $db->fetchAll($select);

        return $total;
    }
    
    
    public function getListPBSale($listname, $from_month_last, $to_month_last, $from_month_now, $to_month_now )
    {

        $db = Zend_Registry::get('db');
        $select = $db->select();
        
       $select = "SELECT  MAX(s.quantity_last) quantity_last , MAX(s.kpi_sale_last) kpi_sale_last, MAX(s.kpi_sale_total_last) kpi_sale_total_last,
                MAX(s.quantity_now) quantity_now, MAX(s.kpi_sale_now) kpi_sale_now, MAX(s.kpi_sale_total_now) kpi_sale_total_now,
                s.pb_sale_id, s.`value` , s.kpi_pb_sale ,s.good_id , s.color_id,
                n.`name` as product,
                m.firstname , m.lastname,
                h.`name` color
                FROM 
                (
                        SELECT  b.quantity_last, b.kpi_sale_last, b.quantity_last * b.kpi_sale_last as kpi_sale_total_last,
                                        c.quantity_now  , c.kpi_sale_now, c.quantity_now * c.kpi_sale_now as kpi_sale_total_now ,
                                        a.pb_sale_id, a.`value` , a.kpi_pb_sale , a.good_id, a.color_id
                        FROM (
                                    SELECT COUNT(a.imei_sn) as quantity, a.*
                                    FROM imei_kpi a
                                    WHERE a.pb_sale_id in (". $listname .") 
                                    AND     DATE(a.timing_date) between '". $from_month_last ."' and '". $to_month_now ."'
                                    GROUP BY  a.good_id, a.color_id, a.pb_sale_id, a.kpi_pb_sale
                                    ORDER BY a.timing_date
                        ) a
                        LEFT JOIN(
                                    SELECT COUNT(a.imei_sn) as quantity_last, a.* , a.kpi_pb_sale as kpi_sale_last
                                    FROM imei_kpi a
                                    WHERE a.pb_sale_id in (". $listname .") 
                                    AND     DATE(a.timing_date) between '". $from_month_last ."' and '". $to_month_last ."'
                                    GROUP BY  a.good_id, a.color_id, a.pb_sale_id, a.kpi_pb_sale
                                    ORDER BY a.timing_date
                        ) b
                        ON a.good_id = b.good_id AND a.color_id = b.color_id AND a.pb_sale_id = b.pb_sale_id AND a.kpi_pb_sale = b.kpi_pb_sale
                
                        LEFT JOIN(
                                    SELECT COUNT(a.imei_sn) as quantity_now, a.* , a.kpi_pb_sale as kpi_sale_now
                                    FROM imei_kpi a
                                    WHERE a.pb_sale_id in (". $listname .") 
                                    AND     DATE(a.timing_date) between '". $from_month_now ."' and '". $to_month_now ."'
                                    GROUP BY  a.good_id, a.color_id, a.pb_sale_id, a.kpi_pb_sale
                                    ORDER BY a.timing_date
                        ) c
                        ON a.good_id = c.good_id AND a.color_id = c.color_id AND a.pb_sale_id = c.pb_sale_id AND a.kpi_pb_sale = c.kpi_pb_sale
                        ORDER BY a.pb_sale_id
                
                ) AS s
                LEFT JOIN warehouse.good as n ON s.good_id = n.id
                LEFT JOIN warehouse.good_color AS h ON s.color_id = h.id
                LEFT JOIN staff as m ON s.pb_sale_id = m.id
                GROUP BY  s.good_id, s.color_id, s.pb_sale_id
                ORDER BY s.pb_sale_id, s.good_id";

        $total = $db->fetchAll($select);

        return $total;
    }
    
    /*
    * controller action: /timing/kpi-review
    * ToÃ n create 2.7.2016
    */
    public function getListPG($listname, $from_month_last, $to_month_last, $from_month_now, $to_month_now )
    {
        $total = 0;

        $db = Zend_Registry::get('db');
        $select = $db->select();
        
         $select = "SELECT  MAX(s.quantity_last) quantity_last , MAX(s.kpi_pg_last) kpi_pg_last, MAX(s.kpi_pg_total_last) kpi_pg_total_last,
                MAX(s.quantity_now) quantity_now, MAX(s.kpi_pg_now) kpi_pg_now, MAX(s.kpi_pg_total_now) kpi_pg_total_now,
                s.pg_id, s.`value` , s.kpi_pg ,s.good_id , s.color_id,
                n.`name` as product,
                m.firstname , m.lastname,
                h.`name` color
                FROM 
                (
                        SELECT  b.quantity_last, b.kpi_pg_last, b.quantity_last * b.kpi_pg_last as kpi_pg_total_last,
                                        c.quantity_now  , c.kpi_pg_now, c.quantity_now * c.kpi_pg_now as kpi_pg_total_now ,
                                        a.pg_id, a.`value` , a.kpi_pg , a.good_id, a.color_id
                        FROM (
                                    SELECT COUNT(a.imei_sn) as quantity, a.*
                                    FROM imei_kpi a
                                    WHERE a.pg_id in (". $listname .") 
                                    AND     DATE(a.timing_date) between '".$from_month_last."' and '".$to_month_now."'
                                    GROUP BY  a.good_id, a.color_id, a.pg_id, a.kpi_pg
                                    ORDER BY a.timing_date
                        ) a
                        LEFT JOIN(
                                    SELECT COUNT(a.imei_sn) as quantity_last, a.* , a.kpi_pg as kpi_pg_last
                                    FROM imei_kpi a
                                    WHERE a.pg_id in (". $listname .") 
                                    AND     DATE(a.timing_date) between '".$from_month_last."' and '".$to_month_last."'
                                    GROUP BY  a.good_id, a.color_id, a.pg_id, a.kpi_pg
                                    ORDER BY a.timing_date
                        ) b
                        ON a.good_id = b.good_id and a.color_id = b.color_id and a.pg_id = b.pg_id AND  a.kpi_pg = b.kpi_pg
                
                        LEFT JOIN(
                                    SELECT COUNT(a.imei_sn) as quantity_now, a.* , a.kpi_pg as kpi_pg_now
                                    FROM imei_kpi a
                                    WHERE a.pg_id in (". $listname .") 
                                    AND     DATE(a.timing_date) between '".$from_month_now."' and '".$to_month_now."'
                                    GROUP BY  a.good_id, a.color_id, a.pg_id, a.kpi_pg
                                    ORDER BY a.timing_date
                        ) c
                        ON a.good_id = c.good_id and a.color_id = c.color_id and a.pg_id = c.pg_id AND  a.kpi_pg = c.kpi_pg
                        ORDER BY a.pg_id
                
                ) AS s
                LEFT JOIN warehouse.good as n ON s.good_id = n.id
                LEFT JOIN warehouse.good_color AS h ON s.color_id = h.id
                LEFT JOIN staff as m ON s.pg_id = m.id
                GROUP BY  s.good_id, s.color_id, s.pg_id
                ORDER BY s.pg_id, s.good_id";
       

        $total = $db->fetchAll($select);

        return $total;
    }
    
    
    /*
    * controller action: /timing/kpi-review
    * ToÃ n create 2.7.2016
    */    
    public function getListNameSalePG($listname)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select();
        
         $select = "SELECT st.id as `id`, 
                            st.firstname as `firstname`, 
                            st.lastname as `lastname`, 
                            st.email as `email`, 
                            st.joined_at as `joined_at`,
                            st.code as `code`,
                            ar.name as `area`
                    FROM staff st
                    INNER JOIN regional_market rm ON rm.id = st.regional_market
                    INNER JOIN area ar ON ar.id = rm.area_id
                    WHERE st.id IN (".$listname.")";
       
        $total = $db->fetchAll($select);

        return $total;
    }
    
    /**
    ** Function Láº¥y sellout 3 thÃ¡ng gáº§n nháº¥t Ä‘Ã³ cháº¿
    */
    public function get3month($params){
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('count(p.imei_sn) AS num, SUM(p.`value`) AS sum, MONTH(timing_date) AS month,YEAR(timing_date) AS year')));
        $select->where('timing_date  >= DATE_SUB(NOW(), INTERVAL 3 MONTH)',null);
        
        if(isset($params['area']) and $params['area'])
            $select->where('area_id  = ?', $params['area']);
        
        if(isset($params['distributor_id']) and $params['distributor_id'])
            $select->where('distributor_id  = ?', $params['distributor_id']);
            
        if(isset($params['store_id']) and $params['store_id'])
            $select->where('store_id  = ?', $params['store_id']);
        
        $select->group(array('YEAR(timing_date)','MONTH(timing_date)'));
        $select->order(array('YEAR(timing_date) ASC','MONTH(timing_date) ASC'));
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function get3month_good($params){
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('count(p.imei_sn) AS num, SUM(p.`value`) AS sum, MONTH(timing_date) AS month,YEAR(timing_date) AS year,good_id')));
        $select->where('timing_date  >= DATE_SUB(NOW(), INTERVAL 3 MONTH)',null);
        
        if(isset($params['area']) and $params['area'])
            $select->where('area_id  = ?', $params['area']);
        
        if(isset($params['distributor_id']) and $params['distributor_id'])
            $select->where('distributor_id  = ?', $params['distributor_id']);
            
        if(isset($params['store_id']) and $params['store_id'])
            $select->where('store_id  = ?', $params['store_id']);
        
        $select->group(array('YEAR(timing_date)','MONTH(timing_date)','good_id'));
        $select->order(array('YEAR(timing_date) ASC','MONTH(timing_date) ASC'));
        $result = $db->fetchAll($select);
        
        $list = array();
        foreach($result as $row=>$value){
            $list[$value['good_id']][$value['month']][$value['year']] =  $value['num'];
        }
        
        //list good_id 3month
        $select_good = $db->select();
        $select_good->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('good_id')));
        $select_good->where('timing_date  >= DATE_SUB(NOW(), INTERVAL 3 MONTH)',null);
        
        if(isset($params['area']) and $params['area'])
            $select_good->where('area_id  = ?', $params['area']);
        
        if(isset($params['distributor_id']) and $params['distributor_id'])
            $select_good->where('distributor_id  = ?', $params['distributor_id']);
            
        if(isset($params['store_id']) and $params['store_id'])
            $select_good->where('store_id  = ?', $params['store_id']);
        
        $select_good->group('good_id');
        $result_good = $db->fetchAll($select_good);
        //end 
        
        $data = array(
            'data' => $list,
            'good' => $result_good
        );
        
        return $data;
    }
    
    public function get_total_good_by_store($params){
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('count(p.imei_sn) AS num, SUM(p.`value`) AS sum')));
        
        if(isset($params['area']) and $params['area'])
            $select->where('area_id  = ?', $params['area']);
        
        if(isset($params['distributor_id']) and $params['distributor_id'])
            $select->where('distributor_id  = ?', $params['distributor_id']);
            
        if(isset($params['store_id']) and $params['store_id'])
            $select->where('store_id  = ?', $params['store_id']);
        
        $select->group('store_id');
        $result = $db->fetchRow($select);
        
        return $result;
    }

    public function fetchArea2($params)
    {
        if (!isset($params['from']) || !$params['from'])
            return false;

        if (!isset($params['to']) || !$params['to'])
            return false;

        $from = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
        $to = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");

        $before_closing = My_Kpi::isBeforeClosingDate($to);

        $db = Zend_Registry::get('db');
        $select = $db->select();

        $countActivateConditionBefore = $countActivateConditionAfter = ' (i.`activation_date` IS NOT NULL AND i.`activation_date` <>0)
                                    OR ((i.`activation_date` IS NULL OR i.`activation_date` =0) AND i.`status` > 0)';

        if (!$before_closing) {
            $get = array(
                'total_quantity'  => new Zend_Db_Expr("COUNT(DISTINCT i.imei_sn)"),
                'total_activated' => new Zend_Db_Expr("SUM(CASE WHEN i.`activation_date` IS NOT NULL AND i.`activation_date` <>0
                    THEN 1 ELSE 0 END)"),
            );
        } else {
            $get = array(
                'total_quantity'  => new Zend_Db_Expr("COUNT(DISTINCT i.imei_sn)"),
                'total_activated' => new Zend_Db_Expr("SUM(CASE WHEN i.`status` IS NOT NULL AND i.`status` > 0
                    THEN 1 ELSE 0 END)"),
            );
        }

        if (isset($params['kpi']) && $params['kpi']) {
            $join_get = array(
                'g.good_id',
                'color_id'    => new Zend_Db_Expr('IFNULL(g.color_id, 0)'),
                'from_date'   => new Zend_Db_Expr(sprintf("CASE WHEN g.from_date < '%s' THEN '%s' ELSE g.from_date END", $from, $from)),
                'to_date'     => new Zend_Db_Expr(sprintf("CASE WHEN g.to_date IS NULL OR g.to_date > '%s' THEN '%s' ELSE g.to_date END", $to, $to)),
                'total_value' => new Zend_Db_Expr("SUM(g.price) * 0.8"),
            );

            if (!$before_closing) {
                $join_get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN $countActivateConditionAfter THEN g.price ELSE 0 END) * 0.8");
            } else {
                $join_get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN $countActivateConditionBefore THEN g.price ELSE 0 END) * 0.8");
            }

        }elseif(isset($params['group_year_month_good']) and $params['group_year_month_good']){

            $get['year']        = 'YEAR(i.timing_date)';
            $get['month']       = 'MONTH(i.timing_date)';
            $get['good_id']     = 'i.good_id';
            $get['total_value'] = new Zend_Db_Expr("SUM(i.`value`) * 0.8");

            if (!$before_closing) {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN $countActivateConditionAfter THEN i.`value` ELSE 0 END) * 0.8");
            } else {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN $countActivateConditionBefore THEN i.`value` ELSE 0 END) * 0.8");
            }

        }else {

            $get['year']        = 'YEAR(i.timing_date)';
            $get['month']       = 'MONTH(i.timing_date)';
            $get['total_value'] = new Zend_Db_Expr("SUM(i.`value`) * 0.8");
            if (!$before_closing) {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN $countActivateConditionAfter THEN i.`value` ELSE 0 END) * 0.8");
            } else {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN $countActivateConditionBefore THEN i.`value` ELSE 0 END) * 0.8");
            }
        }

        $select->from(array('i' => $this->_name), $get);

        if (isset($params['kpi']) && $params['kpi']) {
            $select->join(
                array('g' => WAREHOUSE_DB.'.good_price_log'),
                'g.good_id=i.good_id
                AND
                    CASE WHEN g.color_id <> 0 AND g.color_id IS NOT NULL THEN i.color_id=g.color_id
                    ELSE i.color_id NOT IN (
                            SELECT '.WAREHOUSE_DB.'.`good_price_log`.color_id
                            FROM '.WAREHOUSE_DB.'.`good_price_log`
                            WHERE '.WAREHOUSE_DB.'.`good_price_log`.color_id <> 0
                            AND '.WAREHOUSE_DB.'.`good_price_log`.good_id = i.good_id
                            AND '.WAREHOUSE_DB.'.`good_price_log`.from_date <= DATE(i.timing_date)
                            AND ('.WAREHOUSE_DB.'.`good_price_log`.to_date IS NULL OR '.WAREHOUSE_DB.'.`good_price_log`.to_date = 0 OR '.WAREHOUSE_DB.'.`good_price_log`.to_date >= DATE(i.timing_date))
                        )
                    END
                AND g.from_date<= DATE(i.timing_date)
                AND ( g.to_date IS NULL OR g.to_date = 0 OR g.to_date >= DATE(i.timing_date) )',
                $join_get
            );
        }

        $select->join(array('a' => 'area'), 'a.id=i.area_id',
                array('area_id' => 'a.id', 'area_name' => 'a.name', 'region_share' => 'a.region_share'));

        if (isset($params['from']) && $params['from'])
            $select->where('DATE(i.timing_date) >= ?', $from);

        if (isset($params['to']) && $params['to'])
            $select->where('DATE(i.timing_date) <= ?', $to);

        if( isset($params['area_id']) ){
            if(is_array($params['area_id']) and count($params['area_id']) > 0){
                $select->where('a.id IN (?)',$params['area_id']);
            }elseif($params['area_id']){
                $select->where('a.id = ?',$params['area_id']);
            }
        }

        if (!isset($params['get_total_sales']) || !$params['get_total_sales']) {

            if (isset($params['kpi']) && $params['kpi']){
                $select->group( array('a.id', 'g.good_id', 'g.color_id', 'g.from_date') );
            }elseif(isset($params['group_year_month_good']) and $params['group_year_month_good']){
                $select->group( array('a.id','YEAR(i.timing_date)','MONTH(i.timing_date)', 'i.good_id') );
            }else{
                //$select->group( array('a.id','YEAR(i.timing_date)','MONTH(i.timing_date)') );
                $select->group( array('a.id') );
            }

            if(isset($params['group_year_month_good']) and $params['group_year_month_good']){
                $select->order(array('year DESC','month DESC','a.id','total_quantity DESC'));
            }

            //PC::db($select->__toString());
            return $db->fetchAll($select);
        } else {
            return $db->fetchRow($select);
        }
    }

    public function fetchProvince($params){

        if (!isset($params['from']) || !$params['from']){
            return false;
        }

        if (!isset($params['to']) || !$params['to']){
            return false;
        }

        $from = DateTime::createFromFormat('d/m/Y', $params['from']);
        $from = $from->format('Y-m-d 00:00:00');

        $to = DateTime::createFromFormat('d/m/Y', $params['to']);
        $to = $to->format('Y-m-d 23:59:59');

        $db = Zend_Registry::get('db');
        $select = $db->select();

        $arrCols = array(
            'p.province_id',
            'province_name'      => 'r.name',
            'area_name'          => 'a.name',
            'area_id'            => 'p.area_id',
            'total_quantity'     => 'count(p.imei_sn)',
            'total_activated'    => 'COUNT(activation_date)',
            'total_value'        => 'SUM(p.`value`)*8/10'
        );

        $select->from(array('p'=> 'imei_kpi'), $arrCols);
        $select->joinLeft(array('a'=>'area'), 'a.id = p.area_id',array());
        $select->joinLeft(array('r'=>'regional_market'), 'r.id = p.province_id',array());

        $select->where('p.timing_date >= ?', $from);
        $select->where('p.timing_date <= ?', $to);

        if (isset($params['area']) and $params['area']) {
            $select->where('p.area_id = ?', $params['area']);
        }

        if (!isset($params['get_total_sales']) || !$params['get_total_sales']) {
            $select->group('p.province_id');
            $select->order('total_quantity DESC');
            $result = $db->fetchAll($select);
        }
        else{
            $result = $db->fetchRow($select);
        }

        return $result;

    }

    public function fetchKa($page, $limit, &$total, $params)
    {
        if (!isset($params['from']) || !$params['from']){
            return false;
        }

        if (!isset($params['to']) || !$params['to']){
            return false;
        }

        $from = DateTime::createFromFormat('d/m/Y', $params['from']);
        $from = $from->format('Y-m-d 00:00:00');

        $to = DateTime::createFromFormat('d/m/Y', $params['to']);
        $to = $to->format('Y-m-d 23:59:59');

        $db = Zend_Registry::get('db');
        $select = $db->select();

        $get = array(
            'id'                    => 'IF(d.parent = 0, d.id, d.parent)',
            'd.title',
            'total_quantity'        => 'COUNT(DISTINCT p.imei_sn)',
            'total_activated'       => 'SUM(CASE WHEN (p.`activation_date` IS NOT NULL AND p.`activation_date` <> 0) or ifnull(p.status,0) = 1 THEN 1 ELSE 0 END)',
            'total_value'           => 'SUM(IFNULL(g.price,g2.price)) * 0.8',
            'total_value_activated' => 'SUM(CASE WHEN (p.`activation_date` IS NOT NULL AND p.`activation_date` <> 0) or ifnull(p.status,0) = 1 THEN IFNULL(g.price,g2.price) ELSE 0 END) * 0.8',
            'sell_out_store'        => 'COUNT(DISTINCT p.store_id)'
        );

        $select->from(array('p' => $this->_name), $get);
        $select->joinLeft(array('s'=>'store'), 's.id = p.store_id',array());
        $select->joinLeft(array('d'=> WAREHOUSE_DB.'.distributor'), 'd.id = s.d_id', array());
        $select->joinLeft(array('g'=> WAREHOUSE_DB.'.good_price_log'), 'g.good_id=p.good_id
                                                                            AND IFNULL(g.color_id,0)  <> 0
                                                                            AND IFNULL(g.color_id,0) = p.color_id
                                                                            AND g.from_date <= DATE(p.timing_date)
                                                                            AND ( g.to_date IS NULL OR g.to_date = 0 OR g.to_date >= DATE(p.timing_date) )', array());
        $select->joinLeft(array('g2'=> WAREHOUSE_DB.'.good_price_log'), 'g2.good_id=p.good_id
                                                                            AND IFNULL(g2.color_id,0)  = 0
                                                                            AND g2.from_date <= DATE(p.timing_date)
                                                                            AND ( g2.to_date IS NULL OR g2.to_date = 0 OR g2.to_date >= DATE(p.timing_date) )', array());

        if (isset($params['area_list']) && $params['area_list']) {
            if (is_array($params['area_list']) && count($params['area_list']))
                    $select->where('p.area_id IN (?)', $params['area_list']);
            elseif (is_numeric($params['area_list']))
                    $select->where('p.area_id = ?', intval($params['area_list']));
            else
                $select->where('1=0', 1);
        }

        if (isset($params['area']) && $params['area']) {
            if (is_array($params['area']) && count($params['area']))
                    $select->where('p.area_id IN (?)', $params['area']);
            elseif (is_numeric($params['area']))
                    $select->where('p.area_id = ?', intval($params['area']));
            else
                $select->where('1=0', 1);
        }

        if (isset($params['district']) && $params['district']) {
            if (is_array($params['district']) && count($params['district']))
                    $select->where('p.district_id IN (?)', $params['district']);
            elseif (is_numeric($params['district']))
                    $select->where('p.district_id = ?', intval($params['district']));
            else
                $select->where('1=0', 1);
        }

        if (isset($params['province']) && $params['province']) {
            if (is_array($params['province']) && count($params['province']))
                    $select->where('p.province_id IN (?)', $params['province']);
            elseif (is_numeric($params['province']))
                    $select->where('p.province_id = ?', intval($params['province']));
            else
                $select->where('1=0', 1);
        }

        $select->where('p.timing_date >= ?', $from);
        $select->where('p.timing_date <= ?', $to);
        $select->where('d.is_ka = ?', 1);
        //$select->where('d.parent = ?', 0);
        $select->group('IF(d.parent = 0, d.id, d.parent)');
        $select->order('total_quantity DESC');

            // echo "<pre>";print_r($select->__toString());die;
         
        
        return $db->fetchAll($select);
    }

    public function fetchKaModel($params)
    {
        if (!isset($params['from']) || !$params['from']){
            return false;
        }

        if (!isset($params['to']) || !$params['to']){
            return false;
        }

        if (!isset($params['distributor_id']) || !$params['distributor_id']){
            return false;
        }

        $from = DateTime::createFromFormat('d/m/Y', $params['from']);
        $from = $from->format('Y-m-d 00:00:00');

        $to = DateTime::createFromFormat('d/m/Y', $params['to']);
        $to = $to->format('Y-m-d 23:59:59');

        $db = Zend_Registry::get('db');
        $select = $db->select();

        $get = array(
            'd.id',
            'distributor'    => 'd.title',
            'good_name'      => 'g.name',
            'good_desc'      => 'g.desc',
            'total_quantity' => 'COUNT(DISTINCT p.imei_sn)',
        );

        $select->from(array('p' => $this->_name), $get);
        $select->joinLeft(array('s'=>'store'), 's.id = p.store_id',array());
        $select->joinLeft(array('d'=> WAREHOUSE_DB.'.distributor'), 'd.id = s.d_id', array());
        $select->joinLeft(array('g'=> WAREHOUSE_DB.'.good'), 'g.id = p.good_id', array());

        $select->where('p.timing_date >= ?', $from);
        $select->where('p.timing_date <= ?', $to);
        $select->where('d.id = ? OR d.parent = ?', $params['distributor_id']);
        $select->where('d.is_ka = ?', 1);

        $select->group('p.good_id');
        
        return $db->fetchAll($select);
    }

    public function fetchDistributorKa($page, $limit, &$total, $params)
    {
        if (!isset($params['from']) || !$params['from'])
            return false;

        if (!isset($params['to']) || !$params['to'])
            return false;

        $from = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
        $to = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");

        $before_closing = My_Kpi::isBeforeClosingDate($to);

        $db = Zend_Registry::get('db');
        $select = $db->select();
        $main_get = array();

        if (!$before_closing) {
            $get = array(
                'total_quantity'  => new Zend_Db_Expr("COUNT(DISTINCT i.imei_sn)"),
                'total_activated' => new Zend_Db_Expr("SUM(CASE WHEN (i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0) or ifnull(i.status,0) = 1 THEN 1 ELSE 0 END)"),
            );
        } else {
            $get = array(
                'total_quantity'  => new Zend_Db_Expr("COUNT(DISTINCT i.imei_sn)"),
                'total_activated' => new Zend_Db_Expr("SUM(CASE WHEN i.`status`>0 THEN 1 ELSE 0 END)"),
            );
        }

        if (isset($params['kpi']) && $params['kpi']) {
            $join_get = array(
                'good_id'     => new Zend_Db_Expr('IFNULL(g.good_id,g2.good_id)'),
                'color_id'    => new Zend_Db_Expr('IFNULL(IFNULL(g.color_id,g2.color_id), 0)'),
                'from_date'   => new Zend_Db_Expr(sprintf("CASE WHEN IFNULL(g.from_date,g2.from_date) < '%s' THEN '%s' ELSE IFNULL(g.from_date,g2.from_date) END", $from, $from)),
                'to_date'     => new Zend_Db_Expr(sprintf("CASE WHEN IFNULL(g.to_date,g2.to_date) IS NULL OR IFNULL(g.to_date,g2.to_date) > '%s' THEN '%s' ELSE IFNULL(g.to_date,g2.to_date) END", $to, $to)),
                'total_value' => new Zend_Db_Expr("SUM(IFNULL(g.price,g2.price)) * 0.8"),
            );

            if (!$before_closing) {
                $join_get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0 THEN IFNULL(g.price,g2.price) ELSE 0 END) * 0.8");
            } else {
                $join_get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`status`> 0 THEN IFNULL(g.price,g2.price) ELSE 0 END) * 0.8");
            }

        } else {
            $get['total_value']    = new Zend_Db_Expr("SUM(i.`value`) * 0.8");
            $get['district_id']    = 'i.district_id';
            $get['province_id']    = 'i.province_id';
            $get['area_id']        = 'i.area_id';
            $get['sell_out_store'] = new Zend_Db_Expr("COUNT(DISTINCT i.store_id)");

            if (!$before_closing) {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`activation_date` IS NOT NULL AND i.`activation_date` <> 0 or ifnull(i.status,0) = 1 THEN i.`value` ELSE 0 END) * 0.8");
            } else {
                $get['total_value_activated'] = new Zend_Db_Expr("SUM(CASE WHEN i.`status`> 0 THEN i.`value` ELSE 0 END) * 0.8");
            }
        }

        if(isset($params['group_year_month_good']) and $params['group_year_month_good']){
            $get['year']    = 'YEAR(i.timing_date)';
            $get['month']   = 'MONTH(i.timing_date)';
            $get['good_id'] = 'i.good_id';
            $main_get[]     = 'year';
            $main_get[]     = 'month';
            $main_get[]     = 'good_id';
        }


        $select->from(array('i' => $this->_name), $get)
            ->joinRight(array('s' => 'store'), 's.id=i.store_id', array('store_id' => 's.id'))
            ->joinRight(array('d' => WAREHOUSE_DB.'.distributor'), 'd.id=s.d_id', array('dealer_id' => 'IF(d.parent = 0, d.id, d.parent)'));


        if (isset($params['kpi']) && $params['kpi']) {

            $select->joinLeft(
                array('g' => WAREHOUSE_DB.'.good_price_log'),
                '   g.good_id=i.good_id
                    AND IFNULL(g.color_id,0)  <> 0
                    AND IFNULL(g.color_id,0) = i.color_id
                    AND g.from_date <= DATE(i.timing_date)
                    AND ( g.to_date IS NULL OR g.to_date = 0 OR g.to_date >= DATE(i.timing_date) )',
                $join_get
            );

            $select->joinLeft(
                array('g2' => WAREHOUSE_DB.'.good_price_log'),
                '   g2.good_id=i.good_id
                    AND IFNULL(g2.color_id,0)  = 0
                    AND g2.from_date <= DATE(i.timing_date)
                    AND ( g2.to_date IS NULL OR g2.to_date = 0 OR g2.to_date >= DATE(i.timing_date) )',
                array()
            );
        }

        if (isset($params['sale_id']) && $params['sale_id'])
            $select->where('i.sale_id = ?', intval($params['sale_id']));

        if (isset($params['leader_id']) && $params['leader_id'])
            $select->where('i.leader_id = ?', intval($params['leader_id']));


        if (isset($params['area_list']) && $params['area_list']) {
            if (is_array($params['area_list']) && count($params['area_list']))
                    $select->where('i.area_id IN (?)', $params['area_list']);
            elseif (is_numeric($params['area_list']))
                    $select->where('i.area_id = ?', intval($params['area_list']));
            else
                $select->where('1=0', 1);
        }

        if (isset($params['district']) && $params['district']) {
            if (is_array($params['district']) && count($params['district']))
                    $select->where('i.district_id IN (?)', $params['district']);
            elseif (is_numeric($params['district']))
                    $select->where('i.district_id = ?', intval($params['district']));
            else
                $select->where('1=0', 1);
        }elseif (isset($params['province']) && $params['province']) {
            if (is_array($params['province']) && count($params['province']))
                    $select->where('i.province_id IN (?)', $params['province']);
            elseif (is_numeric($params['province']))
                    $select->where('i.province_id = ?', intval($params['province']));
            else
                $select->where('1=0', 1);
        }elseif (isset($params['area']) && $params['area']) {
            if (is_array($params['area']) && count($params['area']))
                    $select->where('i.area_id IN (?)', $params['area']);
            elseif (is_numeric($params['area']))
                    $select->where('i.area_id = ?', intval($params['area']));
            else
                $select->where('1=0', 1);
        }else{

        }        

       
        if (isset($params['distributor']) && $params['distributor']) {
            if (is_array($params['distributor']) && count($params['distributor']))
                $select->where('d.id IN (?) OR d.parent IN (?)', $params['distributor']);
            elseif (is_numeric($params['distributor']))
                $select->where('d.id = ? OR d.parent = ?', intval($params['distributor']));
            else
                $select->where('1=0', 1);
        }

        if (isset($params['store_list']) && $params['store_list']) {
            if (is_array($params['store_list']) && count($params['store_list']))
                    $select->where('i.store_id IN (?)', $params['store_list']);
            elseif (is_numeric($params['store_list']))
                    $select->where('i.store_id = ?', intval($params['store_list']));
            else
                $select->where('1=0', 1);
        }

        if (isset($params['from']) && $params['from'])
            $select->where('DATE(i.timing_date) >= ?', $from);

        if (isset($params['to']) && $params['to'])
            $select->where('DATE(i.timing_date) <= ?', $to);

        if (isset($params['name']) && $params['name']) {
            $select->where('d.title LIKE ?', '%'.$params['name'].'%');
        }

        if (isset($params['export']) && $params['export']){
            $select->where('d.is_ka = ?', 1);
            //$select->where('d.parent = ?', 0);
        }

        
        if (isset($params['kpi']) && $params['kpi']){
            $select->group( array('IF(d.parent = 0, d.id, d.parent)', 'IFNULL(g.good_id,g2.good_id)', 'IFNULL(g.color_id,g2.color_id)', 'IFNULL(g.from_date,g2.from_date)') );
        }elseif(isset($params['group_year_month_good']) and $params['group_year_month_good']){
            $select->group(array('IF(d.parent = 0, d.id, d.parent)','YEAR(i.timing_date)','MONTH(i.timing_date)','i.good_id'));
        }else{
            $select->group('IF(d.parent = 0, d.id, d.parent)');
        }
        

        if (isset($params['export']) && $params['export']){
            return $select->__toString();
        }

        if (isset($params['kpi']) && $params['kpi']){
            $main_get = array('B.good_id', 'B.color_id', 'B.from_date', 'B.to_date');
        }


        $main_get = array_merge($main_get, array(
            'total_quantity'        => 'B.total_quantity',
            'total_activated'       => 'B.total_activated',
            'total_value_activated' => 'B.total_value_activated',
            'total_value'           => 'B.total_value',
            'sell_out_store'        => 'B.sell_out_store',
            'store_level'           => new Zend_Db_Expr('(CASE WHEN e.name IS NOT NULL THEN e.name ELSE "NORMAL" END)')
        ));

        $date_level = (isset($to) and $to) ? ("'".$to."'") : 'CURDATE()';
        $cols = array(
                'store_id'        => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS d.id'),
                'store_name'      => 'd.title',
                'store_district'  => 'd.district',
                'company_address' => 'd.add',
                'd_id'            => 'd.parent'
            );

        if (isset($params['get_total_sales']) && $params['get_total_sales']){
            $cols = array('store_id'        => 'd.id');
        }

        $main_select = $db->select()
            ->distinct()
            ->from(
                array('d' => WAREHOUSE_DB.'.distributor'),$cols)
            ->join(array('B' => $select), 'B.dealer_id=d.id', $main_get)
            ->joinLeft(array('c'=> 'dealer_loyalty'),'c.dealer_id = d.id AND '.$date_level.' BETWEEN c.from_date AND c.to_date',array())
            ->joinLeft(array('e'=> 'loyalty_plan'),'e.id = c.loyalty_plan_id',array())
            ->join(array('f'=>'v_regional_market'),'f.id = d.district',array())
            ->join(array('g'=>'v_regional_market'),'g.id = f.parent',array())
        ;

        if (isset($params['name']) && $params['name'])
            $main_select->where('d.title LIKE ?', '%'.$params['name'].'%');

        if (isset($params['store_level']) && $params['store_level']){
            $main_select->where('e.id = ?', intval($params['store_level']));
        }

        if(isset($params['group_year_month_good']) and $params['group_year_month_good']){
            $main_select->group(array('IF(d.parent = 0, d.id, d.parent)','year','month','good_id'));
        }else{
            $main_select->group('IF(d.parent = 0, d.id, d.parent)');
        }

        if(isset($params['group_year_month_good']) and $params['group_year_month_good']){
            $main_select->order(array('year','month','d.id','total_quantity DESC'));
        }else{
            $main_select->order('total_quantity DESC');
        }
// if(!empty($_GET['dev'])){
//     echo '<pre>';
//     echo($main_select->__toString());
//     exit;
// }
        if (isset($params['get_total_sales']) && $params['get_total_sales']){
            $cols_total = array(
                'total_quantity'        =>'SUM(a.total_quantity)', 
                'total_activated'       =>'SUM(a.total_activated)', 
                'total_value_activated' =>'SUM(a.total_value_activated)', 
                'total_value'           =>'SUM(a.total_value)'
            );
            $select_total = $db->select()
                ->from(array('a'=> $main_select),$cols_total);
            return $db->fetchRow($select_total);
        }

        if ($limit){
            $main_select->limitPage($page, $limit);
        }

        $result = $db->fetchAll($main_select);

        if ((!isset($params['export']) || !$params['export']) && $limit)
            $total = $db->fetchOne("SELECT FOUND_ROWS()");

        return $result;
    }

    public function fetchStoreActive($params){
        
        $db = Zend_Registry::get('db');
        
        $sql = "SELECT d.id distributor_id,COUNT(s.id) store_number
                FROM warehouse.distributor d
                left JOIN store s 
                    ON d.id = s.d_id
                WHERE s.del <> 1 OR s.del IS NULL
                GROUP BY distributor_id ";

        $stmt = $db->prepare($sql);                      
        $stmt->execute();
        $list = $stmt->fetchAll();
        $stmt->closeCursor();
        $db = $stmt = null;

        $result = array();

        if(count($list)){
            foreach($list as $item){
                $result[$item['distributor_id']] = $item;
            }
        }

        return $result;
    }

    public function fetchStoreActiveKa($params){
        
        $db = Zend_Registry::get('db');
        
        $sql = "SELECT IF(d.parent = 0, d.id, d.parent) distributor_id,COUNT(s.id) store_number
                FROM warehouse.distributor d
                left JOIN store s 
                    ON d.id = s.d_id
                WHERE s.del <> 1 OR s.del IS NULL
                GROUP BY IF(d.parent = 0, d.id, d.parent) ";

        $stmt = $db->prepare($sql);                      
        $stmt->execute();
        $list = $stmt->fetchAll();
        $stmt->closeCursor();
        $db = $stmt = null;

        $result = array();

        if(count($list)){
            foreach($list as $item){
                $result[$item['distributor_id']] = $item;
            }
        }

        return $result;
    }


}
