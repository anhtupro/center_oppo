<?php

class Application_Model_StoreCapacity extends Zend_Db_Table_Abstract
{
    protected $_name = 'store_capacity';
    protected $_schema = DATABASE_TRADE;


    public function getLatestStage()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['s' => DATABASE_TRADE . '.store_capacity_stage'], [
                's.id'
            ])
            ->order('s.id DESC')
            ->limit(1);
        
        $stage_id = $db->fetchOne($select);

        return $stage_id;
    }
    
    public function getCurrentCapacity($store_id)
    {
        $latest_stage_id = $this->getLatestStage();

        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['r' => DATABASE_TRADE . '.store_capacity_result'], [
                'c.name'
            ])
            ->joinLeft(['c' => DATABASE_TRADE . '.store_capacity'], 'r.capacity_id = c.id', [])
            ->where('r.stage_id = ?', $latest_stage_id)
            ->where('r.store_id = ?', $store_id);

        $result = $db->fetchRow($select);

        return $result;
                             
    }
}