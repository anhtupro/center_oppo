<?php
class Application_Model_TrackingPosition extends Zend_Db_Table_Abstract
{
	protected $_name = 'tracking_position';

	function fetchPagination($page, $limit, &$total, $params){

		$db         = Zend_Registry::get('db');
		$cols 			= array(
			'id'							=>	'tp.id',
			'staff_id'				=>	'a.id',
			'firstname'				=>	'a.firstname',
			'lastname'				=>	'a.lastname',
			'location'				=>	'tp.location_arr',
			'created_at'			=>	'tp.created_at',
			'gender_name' 		=> 	'IF(a.gender = 1,"NAM","NỮ")',
			'title_name'      => 	'b.name',
			'team_name'       => 	'c.name',
			'department_name' => 	'd.name',
			'area_name'       => 	'f.name',
		);
		$select 		= $db ->select()
											->from(array('tp'=>'tracking_position'),$cols)
											->joinLeft(array('a' => 'staff'),'tp.staff_id = a.id',array())
											->join(array('b'=>'team'),'a.title = b.id',array())
											->join(array('c'=>'team'),'b.parent_id = c.id',array())
											->join(array('d'=>'team'),'c.parent_id = d.id',array())
											->join(array('e'=>'regional_market'),'e.id = a.regional_market',array())
											->join(array('f'=>'area'),'f.id = e.area_id',array())
											->joinLeft(array('h'=>'province'),'e.province_id = h.id',array())
											->joinLeft(array('k'=>'area_nationality'),'k.id = h.area_national_id',array())
											->order('tp.created_at DESC')
		;

		if( (isset($params['staff_id']) and $params['staff_id']) ){
			$select->where('a.id = ?',$params['staff_id']);
		} else	if( (isset($params['id']) and $params['id']) ){
			$select->where('tp.id = ?',$params['id']);
		} else {
			$select->group(array('a.id'));
		}

		$select->limitPage($page, $limit);

    $result = $db->fetchAll($select);
    $total 	= $db->fetchOne("select FOUND_ROWS()");
    return $result;

	}

}
