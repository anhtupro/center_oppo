<?php

class Application_Model_StoreCapacityResult extends Zend_Db_Table_Abstract
{
    protected $_name = 'store_capacity_result';
    protected $_schema = DATABASE_TRADE;

    public function getStatistic($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['t' => DATABASE_TRADE . '.store_capacity_result'], [
                't.capacity_id',
                't.store_id',
                'r.area_id',
                'count_store' => "COUNT(s.id)",
                'd.is_ka'
            ])
            ->joinLeft(['s' => 'store'], 't.store_id = s.id', [])
            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'di.parent = r.id', [])
            ->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = s.d_id', array())
            ->joinLeft(array('l' => 'dealer_loyalty'), 'l.dealer_id = (IF((d.parent = 0 OR d.parent IS NULL), d.id, d.parent)) AND l.is_last = 1', array())
            ->where('t.stage_id = ?', $params['stage_id'])
             ->where('s.del IS NULL OR s.del = 0')
//            ->where('d.is_ka = 1 OR (d.is_ka = 0 AND l.loyalty_plan_id IS NOT NULL)')
            ->where('r.area_id IN (?)', $params['area_list'])
        ->group(['r.area_id', 't.capacity_id', 'd.is_ka']);


        $result = $db->fetchAll($select);


        foreach ($result as $element) {
            $list [$element['area_id']] [$element['capacity_id']] [$element['is_ka']] = $element['count_store'];
        }

        return $list;
    }

    public function getStatisticNotReview($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['s' => 'store'], [
                'r.area_id',
                'count_store' => "COUNT(s.id)",
                'd.is_ka'
            ])
            ->joinLeft(['t' => DATABASE_TRADE . '.store_capacity_result'], 't.store_id = s.id AND t.stage_id = ' . $params['stage_id'], [])
            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'di.parent = r.id', [])
            ->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = s.d_id', array())
            ->joinLeft(array('l' => 'dealer_loyalty'), 'l.dealer_id = (IF((d.parent = 0 OR d.parent IS NULL), d.id, d.parent)) AND l.is_last = 1', array())
            ->where('s.del IS NULL OR s.del = 0')
//            ->where('d.is_ka = 1 OR (d.is_ka = 0 AND l.loyalty_plan_id IS NOT NULL)')
            ->where('r.area_id IN (?)', $params['area_list'])
            ->where('t.id IS NULL')
            ->group(['r.area_id', 'd.is_ka']);

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['area_id']] [$element['is_ka']] = $element['count_store'];
        }

        return $list;
    }

    public function getDataExport($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['f' => DATABASE_TRADE.'.store_capacity_result'], [
                         'capacity_name' => 'c.name',
                         'store_name' => 's.name',
                         'district_name' => 'di.name',
                         'province_name' => 'r.name',
                         'area_name' => 'a.name',
                         'store_id' => 's.id',
                         'channel'           => "CASE WHEN (loyalty.loyalty_plan_id IS NOT NULL AND d.is_ka = 0) THEN plan.name
                                    WHEN (d.is_ka = 1) THEN 'KA'
                                    WHEN (loyalty.loyalty_plan_id IS NULL AND d.is_ka = 0) THEN 'SHOP THƯỜNG'
                                    END",
                     ])
        ->joinLeft(['s' => 'store'], 'f.store_id = s.id', [])
            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'di.parent = r.id', [])
            ->joinLeft(['a' => 'area'], 'r.area_id = a.id', [])
            ->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = s.d_id', array())
            ->joinLeft(array('loyalty' => 'dealer_loyalty'), 'loyalty.dealer_id = (IF((d.parent = 0 OR d.parent IS NULL), d.id, d.parent)) AND loyalty.is_last = 1', array())
                    ->joinLeft(array('plan' => 'loyalty_plan'), 'plan.id = loyalty.loyalty_plan_id', array())

        ->joinLeft(['c' => DATABASE_TRADE.'.store_capacity'], 'f.capacity_id = c.id', [])
        ->where('f.stage_id = ?', $params['stage_id'])
        ->where('a.id IN (?)', $params['area_list']);

        $result = $db->fetchAll($select);

        return $result;


    }
}