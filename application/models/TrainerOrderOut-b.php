<?php
class Application_Model_TrainerOrderOut extends Zend_Db_Table_Abstract
{
    protected $_name = 'trainer_order_out';

    function fetchPagination($page, $limit, &$total, $params){

        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));


        if (isset($params['sn']) and $params['sn'])
            $select->where('p.sn = ?', $params['sn']);

        if (isset($params['asset_id']) and $params['asset_id'])
        {
            $select->joinleft(array('tood'=>'trainer_order_out_detail'),'tood.order_out_id = p.id', array('*'));
            $select->where('tood.asset_id = ?', $params['asset_id']);
            $select->where('tood.asset_id = ?', $params['asset_id']);
        }


        if (isset($params['confirmed_at_from']) and $params['confirmed_at_from'])
            $select->where('p.confirmed_at >= ?', $params['confirmed_at_from']);

        if (isset($params['confirmed_at_to']) and $params['confirmed_at_to'])
            $select->where('p.confirmed_at <= ?', $params['confirmed_at_to']);

        if (isset($params['created_at_from']) and $params['created_at_from'])
            $select->where('p.created_at >= ?', $params['created_at_from']);

        if (isset($params['created_at_to']) and $params['created_at_to'])
            $select->where('p.created_at <= ?', $params['created_at_to']);

        if (isset($params['status']) and $params['status'])
            $select->where('p.status = ?', $params['status']);

        if (isset($params['output_to_staff_id']) and $params['output_to_staff_id'])
        {
            $select->joinleft(array('tood'=>'trainer_order_out_detail'),'tood.order_out_id = p.id', array('*'));
            $select->where('tood.output_to_staff_id = ?', $params['output_to_staff_id']);
        }

        $select->where('p.del = ? OR p.del IS NULL',0);


        if(!isset($params['export']) and $params['export'] != 1)
        {
            $select->group(array('p.sn'));

            if ($limit)
                $select->limitPage($page, $limit);

            $result = $db->fetchAll($select);
        }
        else
        {
            $result = $select->__toString();
        }

        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    function save($params)
    {
        $order_id                   = isset($params['order_id']) ? $params['order_id'] : null;
        $ip                         = isset($params['ip']) ? $params['ip'] : null;
        $userStorage                = isset($params['userStorage']) ? $params['userStorage'] : null;
        $date                       = isset($params['date']) ? $params['date'] : null;
        $list                       = isset($params['list']) ? $params['list'] : null;
        $QLog                       = new Application_Model_Log();
        $QTrainerOrderOutDetail     = new Application_Model_TrainerOrderOutDetail();
        $QTrainerOrderImei          = new Application_Model_TrainerOrderImei();
        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        $order_id = intval($order_id);

        if(!$date){
            return array('code'=>-1,'message'=>'Vui lòng nhập ngày');
        }
        try {

            if($order_id > 0){
                $order = $this->find($order_id)->current();
                if(!$order){
                    return array('code'=>-1,'message'=>'order not exist');
                }

                // Kiểm tra imei trước khi update
                $checkData = $this->checkData($list);
                if($checkData['code'] == -1 ){
                    return $checkData['message'];
                }

                // lấy dữ liệu cũ
                $selectOldDetail = $db->select()
                    ->from(array('p'=>'trainer_order_out_detail'),array('p.*'))
                    ->where('p.del IS NULL OR p.del = 0')
                    ->where('p.order_out_id = ?',$order_id)
                ;
                $listOldDetail = $db->fetchAll($selectOldDetail);
                $arrListOldDetail = array();
                foreach($listOldDetail as $item){
                    $arrListOldDetail[$item['id']] = $item;
                }

                foreach($list as $item){
                    if(isset($item['order_detail_id'])){
                        $arrOrderDetailId[] = $item['order_detail_id'];
                    }

                }
                $arrKeyOrderDetail      = array_keys($arrListOldDetail);
                $arrRemoveOrderDetail   = array_diff($arrKeyOrderDetail,$arrOrderDetailId);
                if(count($arrRemoveOrderDetail) > 0){
                    $whereRemoveDetail      = $QTrainerOrderOutDetail->getAdapter()->quoteInto('id IN (?)',$arrRemoveOrderDetail);
                    $QTrainerOrderOutDetail->delete($whereRemoveDetail);
                }

                foreach($list as $item){
                    if(isset($item['order_detail_id']) AND $item['order_detail_id']) {
                        $dataUpdate = array(
                            'output_to_staff_id' => $item['trainer'],
                            'asset_id'           => $item['asset_id'],
                            'type'               => $item['type'],
                            'quantity'           => $item['quantity'],
                            'note'               => ($item['type'] == CATEGORY_ASSET_TRAINER_STATIONERY ) ? $item['note_stationery'] : ''
                        );

                        //update detail
                        $where = $QTrainerOrderOutDetail->getAdapter()->quoteInto('id = ?', $item['order_detail_id']);
                        $QTrainerOrderOutDetail->update($dataUpdate, $where);

                        //xử lý imei
                        if($item['type'] == CATEGORY_ASSET_TRAINER_PHONE){

                            //nếu type không thay đổi
                            if($item['type'] == $arrListOldDetail[$item['order_detail_id']]['type']){
                                //danh sách imei cũ
                                $selectImei = $db->select()
                                    ->from(array('p'=>'trainer_order_imei'),array('p.id','p.imei'))
                                    ->where('order_detail_id = ?',$item['order_detail_id'])
                                    ->where('p.type = ?',2)
                                    ->where('del IS NULL OR del = 0');
                                $imeis = $db->fetchPairs($selectImei);

                                //xóa imei
                                $arrKeyImei = array_keys($imeis);
                                $removeImei =  array_diff($arrKeyImei,$item['imei_id']);

                                if(count($removeImei) > 0){
                                    $whereRemoveImei = $QTrainerOrderImei->getAdapter()->quoteInto('id IN (?)',$removeImei);
                                    $QTrainerOrderImei->delete($whereRemoveImei);
                                }

                                foreach($item['imei'] as $k => $imei){
                                    if(isset($item['imei_id'][$k]) AND $item['imei_id'][$k]){
                                        $whereUpdateImei  = $QTrainerOrderImei->getAdapter()->quoteInto('id = ?',$item['imei_id'][$k]);
                                        $QTrainerOrderImei->update(array('imei'=>$imei,'note'=>$item['note'][$k]),$whereUpdateImei);
                                    }else{
                                        $dataIn = array(
                                            'imei'            => $imei,
                                            'note'            => $item['note'][$k],
                                            'asset_id'        => $item['asset_id'],
                                            'order_detail_id' => $item['order_detail_id'],
                                            'order_out'        => $order_id,
                                            'type'            => 2
                                        );
                                        $QTrainerOrderImei->insert($dataIn);
                                    }
                                }
                            }
                        }else{
                            // neu la Van phong pham thi xoa het imei
                            if($item['type'] != $arrListOldDetail[$item['order_detail_id']]['type']){
                                $whereImei = array();
                                $whereImei[] = $QTrainerOrderImei->getAdapter()->quoteInto('order_detail_id = ?', $item['order_detail_id']);
                                $whereImei[] = $QTrainerOrderImei->getAdapter()->quoteInto('type = ?',2);
                                $QTrainerOrderImei->delete($whereImei);
                            }
                        }
                    }else{
                        //insert new row
                        $data_detail = array(
                            'output_to_staff_id' => $item['trainer'],
                            'order_out_id'       => $order_id,
                            'asset_id'           => $item['asset_id'],
                            'quantity'           => $item['quantity'],
                            'type'               => $item['type'],
                            'note'               => $item['note_stationery']
                        );
                        $order_detail_id = $QTrainerOrderOutDetail->insert($data_detail);

                        //Nếu phone thì nhập thêm imei
                        if($item['type'] == 1){
                            foreach($item['imei'] as $k => $imei):
                                $dataIn = array(
                                    'imei'            => $imei,
                                    'note'            => $item['note'][$k],
                                    'asset_id'        => $item['asset_id'],
                                    'order_detail_id' => $order_detail_id,
                                    'order_out'       => $order_id,
                                    'type'            => 2,
                                    'trainer_id'      => $item['trainer']
                                );
                                $QTrainerOrderOutDetail->insert($dataIn);
                            endforeach;
                        }
                    }//End insert data
                }//End foreach
            }else{

                /* Kiểm tra imei trước khi update */
                $checkData = $this->checkData($list);
                if($checkData['code'] == -1 ){
                   return $checkData['message'];
                }

                $sn = date ('YmdHis') . substr ( microtime (), 2, 4 );
                $dataOrder = array(
                    'sn'         => $sn,
                    'date'       => $date,
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => $userStorage->id,
                    'status'     => ORDER_STATUS_PENDING_TRAINING
                );
                $order_id = $this->insert($dataOrder);

                //detail
                foreach($list as $key => $value):
                    $note = '';
                    $data_detail = array(
                        'order_out_id'          => $order_id,
                        'asset_id'              => intval($value['asset_id']),
                        'quantity'              => intval($value['quantity']),
                        'type'                  => intval($value['type']),
                        'output_to_staff_id'    => $value['trainer'],
                        'note'                  => $note
                    );
                    $order_detail_id = $QTrainerOrderOutDetail->insert($data_detail);

                    //Nếu phone thì nhập thêm imei
                    if($value['type'] == 1){
                        foreach($value['imei'] as $k => $imei):
                            $dataIn = array(
                                'imei'            => $imei,
                                'note'            => $value['note'][$k],
                                'asset_id'        => $value['asset_id'],
                                'order_detail_id' => $order_detail_id,
                                'order_out'       => $order_id,
                                'type'            => 2,
                                'trainer_id'      => $value['trainer']
                            );
                            $QTrainerOrderImei->insert($dataIn);
                        endforeach;
                    }
                endforeach;

            }
            $db->commit();
            return array('code'=>1,'message'=>'éc');
        } catch (Exception $e) {
            $db->rollBack();
            return array('code'=> -1,'message'=>$e->getMessage());
        }
    }

    public function getStaffTrainer()
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('p' => 'staff'),array('p.id','p.firstname','p.lastname','p.team'));
        $select->where('p.team = ?', TRAINING_TEAM);
        $select->where('p.off_date IS NULL');
        $rows = $db->fetchAll($select);
        $result = array();

        if(count($rows)>0)
        {
            foreach($rows as $key => $value)
            {
                $result[$value['id']] = $value['firstname'].' '.$value['lastname'];
            }
        }

        return $result;

    }

    public function checkQuantityAsset($asset_id)
    {
        $result =  null;

        $db     = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => 'trainer_inventory_asset'),array('*'));

        $select->where('p.asset_id = ?', $asset_id);

        $row = $db->fetchRow($select);

        if($row)
        {
            $result = $row['total_quantity'];
        }

        return $result;
    }

    function checkImei($imei,$asset){
        $db = Zend_Registry::get('db');

        //Neu san pham moi ve chua co tren he thong thì tạm thời bỏ qua
        $selectCheckNew = $db->select()
            ->from(array('p'=>'trainer_type_asset'),array('p.*'))
            ->where('p.status = ?',1)
            ->where('p.id = ?',$asset);

        $resultCheckNew = $db->fetchRow($selectCheckNew);
        if($resultCheckNew){
            if(strlen(trim($imei)) != 15){
                return false;
            }
            return true;
        }

        return true;
        // Tạm thời check đến đây thôi vì nhiều imei không có trên hệ thống

        $select = $db->select()
            ->from(array('a'=>WAREHOUSE_DB.'.imei'),array('a.*'))
            ->join(array('c'=>'trainer_type_asset'),'a.good_id = c.good_id AND a.good_color = c.color_id',array())
            ->join(array('b'=>WAREHOUSE_DB.'.market'),'b.sn = a.sales_sn AND b.type = 4',array())
            ->where('c.id = ?',$asset)
            ->where('a.imei_sn = ?',$imei)
        ;
        $row = $db->fetchRow($select);
        if($row){
            return true;
        }else{
            return false;
        }
    }

    function checkData($list){
        $QTrainerTypeAsset = new Application_Model_TrainerTypeAsset();
        $type_asset = $QTrainerTypeAsset->getAsset(array());
        $arrImei = array();
        if(count($list) == 0){
            return array('code'=>-1,'message'=>array('code'=>'-1','message'=>'không được để trống'));
        }
        foreach($list as $item){
            if(!$item['asset_id']){
                return array('code'=>-1,'message'=>array('code'=>'-1','message'=>'Vui lòng nhập sản phẩm'));
            }

            if(!$item['quantity']){
                return array('code'=>-1,'message'=>array('code'=>'-1','message'=>'Số lượng không được để trông'));
            }

            if(!$this->checkStock($item['asset_id'],$item['quantity'])){
                return array('code'=>-1,'message'=>array('code'=>'-1','message'=>'Số lượng '.$type_asset[$item['asset_id']].' tồn kho không đủ'));
            }

            if($item['type'] == CATEGORY_ASSET_TRAINER_PHONE){
                $arrImei += $item['imei'];
                if($item['quantity'] != count($item['imei'])){
                    return array('code'=>-1,'message'=>array('code'=>'-1','message'=>'số lượng và số imei nhập vào không khớp'));
                }

                foreach($item['imei'] as $imei){
                    $result = $this->checkImei($imei,$item['asset_id']);
                    if(!$result){
                        return array('code'=>-1,'message'=>array('code'=>'-1','message'=>'imei '.$imei.' không hợp lệ'));
                    }
                }
            }
        }

        //kiểm tra imei bị trung
        $arr_unique = array_unique($arrImei);
        $diff = array_diff_assoc($arrImei,$arr_unique);
        if(count($diff)){
            $imei = array_shift($diff);
            return array('code'=>-1,'message'=>array('code'=>'-1','message'=>'imei '.$imei.' đã được nhập hơn 1 lần'));
        }
    }

    function checkStock($asset_id,$num){
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('a'=>'v_trainer_inventory'),array('a.total'))
            ->where('a.asset_id = ?',$asset_id);
        $stock = $db->fetchOne($select);
        if(intval($stock) < $num){
            return false;
        }
        return true;
    }

    function  getOrderConfirmByTrainer($staff_id,$trainer_confirm = 0){
        $db = Zend_Registry::get('db');
        $cols = array(
            'd.sn',
            'id' => 'a.id',
            'staff_name' => new Zend_Db_Expr('CONCAT(b.firstname," ",b.lastname)'),
            'asset_name' => 'c.name',
            'type_asset' => 'c.type',
            'quantity'  => 'a.quantity',
            'note' => 'a.note',
            'date'             => 'd.date',
            'trainer_confirm' => 'a.trainer_confirm',
        );
        $select = $db->select()
            ->from(array('a'=>'trainer_order_out_detail'),$cols)
            ->join(array('b'=>'staff'),'a.output_to_staff_id = b.id',array())
            ->join(array('c'=>'trainer_type_asset'),'a.asset_id = c.id',array())
            ->join(array('d'=>'trainer_order_out'),'a.order_out_id = d.id',array())
            ->order(array('d.sn','d.date ASC'));
        $select->where('b.id = ?',$staff_id);
        $select->where('a.trainer_confirm = ?',$trainer_confirm);
        $result = $db->fetchAll($select);
        foreach($result as $k => $value){
            if($value['type_asset'] == 1){
                $select2 = $db->select()
                    ->from(array('p'=>'trainer_order_imei'),array('imei','note'))
                    ->where('type = ?',2)
                    ->where('order_detail_id = ?',$value['id']);
                $rs = $db->fetchAll($select2);
                $result[$k]['phone'] = $rs;
            }
        }
        return $result;
    }
}