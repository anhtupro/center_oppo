<?php
class Application_Model_TradeDelete extends Zend_Db_Table_Abstract
{
    protected $_name = 'category_trade';

    public function getCategory(){

        $db = Zend_Registry::get('db');

        $col = array(
            'p.*'
        );

        $select = $db->select()
            ->from(array('p'=> DATABASE_TRADE.'.category'), $col);
        
        $select->where('p.id IN (?)', array(43,421,113,44,363,261,153,154,152,1,2,4,6,7,9));

        $result = $db->fetchAll($select); 

        return $result;

    }

    public function getDataArea($params){
        $db = Zend_Registry::get('db');
        
        $sub_select = 
        "SELECT p.imei_sn, p.good_id, p.object_id, p.object_active, p.del, 1 AS number
        FROM `".DATABASE_TRADE."`.imei p
        WHERE p.del <> 1 OR p.del IS NULL
        UNION 
        SELECT null, p.good_id, p.object_id, p.created_at, null, p.number
        FROM `".DATABASE_TRADE."`.store_good p
        WHERE p.`status` IN (1,2)";

        $col = array(
            'a.id', 
            'i.good_id', 
            'total' => 'SUM(i.number)',
        );

        $select = $db->select()
            ->from(array('p'=> 'store_staff_log'), $col)
            ->joinLeft(array('s'=> 'store'), 's.id = p.store_id', array())
            ->joinLeft(array('i'=> new Zend_Db_Expr('(' . $sub_select . ')')), 'i.object_id = s.id', array())
            ->joinLeft(array('c'=> DATABASE_TRADE.'.category'), 'c.id = i.good_id', array())
            ->joinLeft(array('staff'=> 'staff'), 'staff.id = p.staff_id', array())
            ->joinLeft(array('r'=> 'regional_market'), 'r.id = s.regional_market', array())
            ->joinLeft(array('channel'=> 'store_area_chanel'), 'channel.store_id = s.id', array())
            ->joinLeft(array('a'=> 'area'), 'a.id = IFNULL(channel.area_id,r.area_id)', array())
            ->where('p.`released_at` IS NULL OR p.`released_at` >= UNIX_TIMESTAMP()', NULL);

        $select->where('p.is_leader = 1', NULL);

        //Xóa hết dữ liệu scan trừ HCM1
        $select->where("(i.object_active >= '2012-08-08 00:00') OR (i.object_active >= '2012-08-08 00:00' AND a.id = 24) OR (i.object_id IS NULL)", NULL);

        if(!empty($params['staff_id'])){
            $select->where('p.staff_id = ?', $params['staff_id']);
        }
        elseif(!empty($params['area_list'])){
            $select->where('IFNULL(channel.area_id,r.area_id) IN (?)', $params['area_list']);
        }


        $select->group(array('IFNULL(channel.area_id,r.area_id)', 'i.good_id'));
        
        $result = $db->fetchAll($select);

        return $result;
    }

    public function getDataAreaChannel($params){
        $db = Zend_Registry::get('db');
        
        $sub_select = 
        "SELECT p.imei_sn, p.good_id, p.object_id, p.object_active, p.del, 1 AS number
        FROM `".DATABASE_TRADE."`.imei p
        WHERE p.del <> 1 OR p.del IS NULL
        UNION 
        SELECT null, p.good_id, p.object_id, p.created_at, null, p.number
        FROM `".DATABASE_TRADE."`.store_good p
        WHERE p.`status` IN (1,2)";

        $col = array(
            'a.id', 
            'i.good_id', 
            'total' => 'SUM(i.number)',
            'd.is_ka'
        );

        $select = $db->select()
            ->from(array('p'=> 'store_staff_log'), $col)
            ->joinLeft(array('s'=> 'store'), 's.id = p.store_id', array())
            ->joinLeft(array('d'=> WAREHOUSE_DB.'.distributor'), 'd.id = s.d_id', array())
            ->joinLeft(array('i'=> new Zend_Db_Expr('(' . $sub_select . ')')), 'i.object_id = s.id', array())
            ->joinLeft(array('c'=> DATABASE_TRADE.'.category'), 'c.id = i.good_id', array())
            ->joinLeft(array('staff'=> 'staff'), 'staff.id = p.staff_id', array())
            ->joinLeft(array('r'=> 'regional_market'), 'r.id = s.regional_market', array())
            ->joinLeft(array('channel'=> 'store_area_chanel'), 'channel.store_id = s.id', array())
            ->joinLeft(array('a'=> 'area'), 'a.id = IFNULL(channel.area_id,r.area_id)', array())
            ->where('p.`released_at` IS NULL OR p.`released_at` >= UNIX_TIMESTAMP()', NULL);

        $select->where('p.is_leader = 1', NULL);

        //Xóa hết dữ liệu scan trừ HCM1
        $select->where("(i.object_active >= '2012-08-08 00:00') OR (i.object_active >= '2012-08-08 00:00' AND a.id = 24) OR (i.object_id IS NULL)", NULL);

        if(!empty($params['staff_id'])){
            $select->where('p.staff_id = ?', $params['staff_id']);
        }
        elseif(!empty($params['area_list'])){
            $select->where('IFNULL(channel.area_id,r.area_id) IN (?)', $params['area_list']);
        }


        $select->group(array('IFNULL(channel.area_id,r.area_id)', 'i.good_id', 'd.is_ka'));
        
        $result = $db->fetchAll($select);

        return $result;
    }

    public function getDataSale($params){
        $db = Zend_Registry::get('db');
        
        $sub_select = 
        "SELECT p.imei_sn, p.good_id, p.object_id, p.object_active, p.del, 1 AS number
        FROM `".DATABASE_TRADE."`.imei p
        WHERE p.del <> 1 OR p.del IS NULL
        UNION 
        SELECT null, p.good_id, p.object_id, p.created_at, null, p.number
        FROM `".DATABASE_TRADE."`.store_good p
        WHERE p.`status` IN (1,2)";

        $col = array(
            'staff_id' => 'staff.id' , 
            "fullname" => "CONCAT(staff.firstname, ' ',staff.lastname)", 
            'staff.email', 
            "area_id" => "a.id" , 
            "area_name" => "a.name" , 
            "i.good_id", 
            "total" => "SUM(i.number)" 
        );

        $select = $db->select()
            ->from(array('p'=> 'store_staff_log'), $col)
            ->joinLeft(array('s'=> 'store'), 's.id = p.store_id', array())
            ->joinLeft(array('i'=> new Zend_Db_Expr('(' . $sub_select . ')')), 'i.object_id = s.id', array())
            ->joinLeft(array('c'=> DATABASE_TRADE.'.category'), 'c.id = i.good_id', array())
            ->joinLeft(array('staff'=> 'staff'), 'staff.id = p.staff_id', array())
            ->joinLeft(array('r'=> 'regional_market'), 'r.id = s.regional_market', array())
            ->joinLeft(array('channel'=> 'store_area_chanel'), 'channel.store_id = s.id', array())
            ->joinLeft(array('a'=> 'area'), 'a.id = IFNULL(channel.area_id,r.area_id)', array())
            ->where('p.`released_at` IS NULL OR p.`released_at` >= UNIX_TIMESTAMP()', NULL);

        $select->where('p.is_leader = 1', NULL);

        //Xóa hết dữ liệu scan trừ HCM1
        $select->where("(i.object_active >= '2012-08-08 00:00') OR (i.object_active >= '2012-08-08 00:00' AND a.id = 24) OR (i.object_id IS NULL)", NULL);

        if(!empty($params['staff_id'])){
            $select->where('p.staff_id IN (?)', $params['staff_id']);
        }
        elseif(!empty($params['area_list'])){
            $select->where('IFNULL(channel.area_id,r.area_id) IN (?)', $params['area_list']);
        }


        $select->group(array('staff.id', 'i.good_id'));

        $select->order('a.id');
        
        $result = $db->fetchAll($select);

        return $result;
    }

    public function getListSale(){

    }

    public function getListStoreScan(){
        $db = Zend_Registry::get('db');
        
        $sub_select = 
        "SELECT p.imei_sn, p.good_id, p.object_id, p.object_active, p.del, 1 AS number
        FROM `".DATABASE_TRADE."`.imei p
        WHERE object_active >= '2017-08-08 00:00'
        UNION 
        SELECT null, p.good_id, p.object_id, p.created_at, null, p.number
        FROM `".DATABASE_TRADE."`.store_good p
        WHERE p.`status` IN (1,2)";

        $col = array(
            'p.*'
        );

        $select = $db->select()
            ->from(array('p'=> new Zend_Db_Expr('(' . $sub_select . ')')), $col);
            

        $select->group(array('p.object_id'));

        $result = $db->fetchAll($select);

        $data = array();

        foreach($result as $key=>$value){
            $data[] = $value['object_id'];
        }

        return $data;
    }
    
}