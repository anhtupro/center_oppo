<?php

class Application_Model_ChangePictureStage extends Zend_Db_Table_Abstract
{
    protected $_name = 'change_picture_stage';
    protected $_schema = DATABASE_TRADE;

    public function fetchPagination($page, $limit, &$total, $params){

        $db = Zend_Registry::get("db");
        // sub query
        $select = $db->select();
        //DISTINCT
        $arrCols = array(
            'id' => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            'p.*'
        );

        $select->from(array('p' => DATABASE_TRADE.'.change_picture_stage'), $arrCols);

        $select->limitPage($page, $limit);

        $select->order('p.id DESC');

        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    public function getHistoryChangePicture($checkshop_id_lock, $params = null)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['d' => DATABASE_TRADE.'.app_checkshop_detail_child'], [
                'd.category_id',
                'category_name' => 'c.name',
                'quantity' => 'd.quantity',
                'c.is_picture',
                'p.app_checkshop_detail_child_id' ,
                'product_name' => 'o.name',
                'p.logo_type',
                'p.note',
                'p.stage_id',
                'stage_name' => 's.name',
                'change_picture_detail_id' => 'p.id',
                'p.has_changed_picture'
            ])
            ->join(['c' => DATABASE_TRADE.'.category'], 'd.category_id = c.id AND c.is_picture = 1', [])
            ->joinLeft(['p' => DATABASE_TRADE.'.change_picture_detail'], 'd.id = p.app_checkshop_detail_child_id AND p.product_id <> 0', [])
            ->joinLeft(['o' => DATABASE_TRADE.'.phone_product'], 'p.product_id = o.id', [])
            ->joinLeft(['s' => DATABASE_TRADE.'.change_picture_stage'], 'p.stage_id = s.id', [])
            ->where('d.checkshop_id = ?', $checkshop_id_lock)
            ->where('s.is_running = ?', 0)
            ->where('d.quantity > ?', 0);
//             ->where('p.product_id <> 0');


        if ($params['change_picture_stage_id']) {
            $select->where('s.id = ?', $params['change_picture_stage_id']);
        }
        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['app_checkshop_detail_child_id']] [] = $element;
        }

        return $list;
    }
    public function getHistoryChangePictureChild($checkshop_id_lock, $params = null)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['d' => DATABASE_TRADE.'.app_checkshop_detail_child'], [
                'd.category_id',
                'category_name' => 'c.name',
                'quantity' => 'd.quantity',
                'c.is_picture',
                'p.app_checkshop_detail_child_id' ,
                'product_name' => 'o.name',
                'p.logo_type',
                'p.note',
                'p.stage_id',
                'p.child_painting_id',
                'stage_name' => 's.name',
                'change_picture_detail_id' => 'p.id',
                'p.has_changed_picture'
            ])
            ->join(['c' => DATABASE_TRADE.'.category'], 'd.category_id = c.id AND c.has_painting = 1', [])
            ->joinLeft(['i' => DATABASE_TRADE.'.app_checkshop_detail_child_painting'], 'i.app_checkshop_detail_child_id = d.id', [])
            ->joinLeft(['p' => DATABASE_TRADE.'.change_picture_detail'], 'i.id = p.child_painting_id AND p.product_id <> 0', [])
            ->joinLeft(['o' => DATABASE_TRADE.'.phone_product'], 'p.product_id = o.id', [])
            ->joinLeft(['s' => DATABASE_TRADE.'.change_picture_stage'], 'p.stage_id = s.id', [])
            ->where('d.checkshop_id = ?', $checkshop_id_lock)
            ->where('s.is_running = ?', 0)
            ->where('d.quantity > ?', 0);
//            ->where('p.product_id <> 0');


        if ($params['change_picture_stage_id']) {
            $select->where('s.id = ?', $params['change_picture_stage_id']);
        }

        $result = $db->fetchAll($select);


        foreach ($result as $element) {
            $list [$element['child_painting_id']] [] = $element;
        }

        return $list;
    }

    public function getCurrentChangePicture($checkshop_id_lock)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['d' => DATABASE_TRADE.'.app_checkshop_detail_child'], [
                'd.category_id',
                'category_name' => 'c.name',
                'quantity' => 'd.quantity',
                'c.is_picture',
                'p.app_checkshop_detail_child_id' ,
                'p.product_id',
                'p.logo_type',
                'p.note',
                'p.stage_id'
            ])
            ->join(['c' => DATABASE_TRADE.'.category'], 'd.category_id = c.id AND c.is_picture = 1', [])
            ->joinLeft(['p' => DATABASE_TRADE.'.change_picture_detail'], 'd.id = p.app_checkshop_detail_child_id', [])
            ->joinLeft(['o' => DATABASE_TRADE.'.phone_product'], 'p.product_id = o.id', [])
            ->joinLeft(['s' => DATABASE_TRADE.'.change_picture_stage'], 'p.stage_id = s.id', [])
            ->where('d.checkshop_id = ?', $checkshop_id_lock)
            ->where('s.is_running = ?', 1)
            ->where('d.quantity > ?', 0);

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['app_checkshop_detail_child_id']]  = $element;
        }

        return $list;
    }
    public function getCurrentChangePictureChild($checkshop_id_lock)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['d' => DATABASE_TRADE.'.app_checkshop_detail_child'], [
                'd.category_id',
                'category_name' => 'c.name',
                'quantity' => 'd.quantity',
                'c.is_picture',
                'p.app_checkshop_detail_child_id' ,
                'p.product_id',
                'p.logo_type',
                'p.note',
                'p.stage_id',
                'p.child_painting_id'
            ])
            ->join(['c' => DATABASE_TRADE.'.category'], 'd.category_id = c.id AND c.has_painting = 1', [])
            ->joinLeft(['i' => DATABASE_TRADE.'.app_checkshop_detail_child_painting'], 'i.app_checkshop_detail_child_id = d.id', [])
            ->joinLeft(['p' => DATABASE_TRADE.'.change_picture_detail'], 'i.id = p.child_painting_id', [])
            ->joinLeft(['o' => DATABASE_TRADE.'.phone_product'], 'p.product_id = o.id', [])
            ->joinLeft(['s' => DATABASE_TRADE.'.change_picture_stage'], 'p.stage_id = s.id', [])
            ->where('d.checkshop_id = ?', $checkshop_id_lock)
            ->where('s.is_running = ?', 1)
            ->where('d.quantity > ?', 0);

        $result = $db->fetchAll($select);
     

        foreach ($result as $element) {
            $list [$element['child_painting_id']]  = $element;
        }

        return $list;
    }

    public function getValidStage()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['s' => DATABASE_TRADE.'.change_picture_stage'], [
                       's.*'
                     ])
        ->where('s.is_running = ?', 1);
        
        $result = $db->fetchRow($select);

        return $result;
        
    }

    public function getStageForSale()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['s' => DATABASE_TRADE.'.change_picture_stage'], [
                's.*'
            ])
            ->where('s.from_date_check <= CURDATE()')
            ->where('s.to_date_check >= CURDATE()');

        $result = $db->fetchRow($select);

        return $result;
    }

    public function checkStoreValid($store_id, $stage_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['a' => DATABASE_TRADE.'.change_picture_assign_store'], [
                         'a.store_id'
                     ])
                    ->where('a.store_id = ?', $store_id)
                    ->where('a.stage_id = ?', $stage_id);

        $result = $db->fetchOne($select);
   

        return $result ? true : false;

    }


    public function getDataPicture($params)
    {


        $db = Zend_Registry::get("db");

        $arrCols = array(
            'store_name'        => 's.name' ,
            'store_id'          => 's.id' ,
            'shipping_address'  => 's.shipping_address',

            'area_name' => "area.name",
            'province' => 'r.name',

            'child_id' => 'de.id',
            'painting_id' => 'p.id',
            'category_name' => 'c.name',
            'de.quantity',
            'type' => 't.name',
            'type_child' => 't2.name',
            'material' => 'm.name',
            'height' => 'de.height',
            'width' => 'de.width',
            'height_painting' => 'p.height',
            'width_painting' => 'p.width',
            'material_painting' => 'm2.name',
            'district' => 'district.name',
            'sale_name' => "CONCAT(sale.firstname, ' ', sale.lastname)",
            'c.is_picture'
        );

        $select = $db->select();
        $select->from(array('s'=> 'store'), $arrCols);
        $select->joinLeft(array('district' => 'regional_market'), 's.district = district.id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'district.parent = r.id', array());
        $select->joinLeft(array('area' => 'area'), 'area.id = r.area_id', array());
//        $select->joinLeft(array('l' => 'v_store_staff_leader_log'), 'l.store_id = s.id AND l.released_at IS NULL AND l.is_leader = 1', array());

        $select->joinLeft(['l' => 'store_staff_log'], 'l.store_id = s.id AND l.released_at IS NULL AND l.is_leader = 1', []);
        $select->joinLeft(['q' => 'store_leader_log'], 'q.store_id = s.id AND q.released_at IS NULL', []);

        $select->joinLeft(array('sale' => 'staff'), 'IF(l.staff_id IS NULL, q.staff_id, l.staff_id) = sale.id', array());
        $select->joinLeft(array('a' => DATABASE_TRADE.'.app_checkshop'), 'a.store_id = s.id AND a.is_lock = 1',array());
        $select->joinLeft(array('de' => DATABASE_TRADE.'.app_checkshop_detail_child'), 'a.id = de.checkshop_id AND de.quantity > 0',array());
        $select->joinLeft(array('p' => DATABASE_TRADE.'.app_checkshop_detail_child_painting'), 'de.id = p.app_checkshop_detail_child_id',array());
        $select->joinLeft(array('t' => DATABASE_TRADE.'.category_inventory_type'), 'de.category_type = t.id',array());
        $select->joinLeft(array('t2' => DATABASE_TRADE.'.category_inventory_type'), 'de.category_type_child = t2.id',array());
        $select->joinLeft(array('m' => DATABASE_TRADE.'.category_material'), 'de.material_id = m.id',array());
        $select->joinLeft(array('m2' => DATABASE_TRADE.'.category_material'), 'p.material_id = m2.id',array());
        $select->joinLeft(array('c' => DATABASE_TRADE.'.category'), 'de.category_id = c.id',array());

        $select->join(array('w' => DATABASE_TRADE.'.change_picture_assign_store'), 'w.store_id = s.id AND w.stage_id = ' . $params['change_picture_stage'], array());

        $select->joinLeft(array('k' => DATABASE_TRADE.'.change_picture_detail'), "IF(k.app_checkshop_detail_child_id IS NULL, k.child_painting_id = p.id, k.app_checkshop_detail_child_id = de.id) AND k.stage_id = " . $params['change_picture_stage'] . ' AND k.product_id <> 0' ,array('k.has_changed_picture', 'k.logo_type', 'change_picture_note' => 'k.note'));
        $select->joinLeft(array('v' => DATABASE_TRADE.'.phone_product'), 'k.product_id = v.id',array('key_visual' => 'v.id'));

        $select->where('c.is_picture = 1 OR c.has_painting = 1');
//        $select->where('de.quantity > ?', 0);
        $select->where('s.del IS NULL OR s.del = 0');

//        if ($params['export_change_picture']) {
//            $select->joinLeft(array('k' => DATABASE_TRADE.'.change_picture_detail'), "IF(k.app_checkshop_detail_child_id IS NULL, k.child_painting_id = p.id, k.app_checkshop_detail_child_id = de.id) AND k.stage_id = " . $params['change_picture_stage'] . ' AND k.product_id <> 0' ,array('k.has_changed_picture', 'k.logo_type', 'change_picture_note' => 'k.note'));
//            $select->joinLeft(array('v' => DATABASE_TRADE.'.phone_product'), 'k.product_id = v.id',array('key_visual' => 'v.name'));
//            $select->join(array('w' => DATABASE_TRADE.'.change_picture_assign_store'), 'w.store_id = s.id AND w.stage_id = ' . $params['change_picture_stage'], array('key_visual' => 'v.name'));
//            $select->where('c.is_picture = 1 OR c.has_painting = 1');
//        }

        if(!empty($params['staff_id'])){
            $select->where('l.staff_id = ?', $params['staff_id']);
        }

        if(!empty($params['area_id'])){
            $select->where('r.area_id IN (?)', $params['area_id']);
        }

        if(!empty($params['list_area'])){
            $select->where('r.area_id IN (?)', $params['list_area']);
        }

        if ((isset($params['is_ka']) && $params['is_ka'] === "0") || !empty($params['is_ka'])){


            if(isset($params['is_ka']) && $params['is_ka'] === "0"){
                $select->where('loyalty.loyalty_plan_id IS NOT NULL', NULL);
                $select->where('d.is_ka = ?', 0);
            }

            if(isset($params['is_ka']) && $params['is_ka'] === "1"){
                $select->where('d.is_ka = ?', 1);
            }

            if(isset($params['is_ka']) && $params['is_ka'] === "2"){
                $select->where('loyalty.loyalty_plan_id IS NULL', NULL);
                $select->where('d.is_ka = ?', 0);
            }
        }

        if(!empty($params['is_ka_details'])){

            if(isset($params['is_ka']) && $params['is_ka'] === "0"){
                $select->where('loyalty.loyalty_plan_id = ?', $params['is_ka_details']);
            }

            if(isset($params['is_ka']) && $params['is_ka'] === "1"){
                $select->where('IF(d.parent = 0, d.id, d.parent) = ?', $params['is_ka_details']);
            }

        }
        if ($_GET['dev']) {
            echo $select->__toString();
            die;
        }
        $result = $db->fetchAll($select);

        return $result;
    }

    public function downloadTemplateMassUpload($data)
    {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();
        $heads = array(
            'Store ID',
            'Tên shop',
            'Khu vực',
            'Tỉnh',
            'Huyện',
            'Sale quản lý',
            'Hạng mục',
            'ID Hạng mục',
            'ID chi tiết tranh',
            'Số lượng',
            'Chiều ngang tranh (mm)',
            'Chiều cao tranh (mm)',
            'Chất liệu tranh',
            'Key visual(Đăng ký)',
            'Logo(Đăng ký)',
            'Ghi chú(Đăng ký)'
        );
        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $title)
        {
            $sheet->setCellValue($alpha.$index, $title);
            $alpha++;
        }

        // style header
        $sheet->getStyle('N1:P1')->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'c4f5b0')
                )
            )
        );

        // end style header

        $index    = 2;

        $i = 1;

        foreach($data as $item){

            $alpha    = 'A';
            $sheet->setCellValue($alpha++.$index, $item['store_id']);
            $sheet->setCellValue($alpha++.$index, $item['store_name']);
            $sheet->setCellValue($alpha++.$index, $item['area_name']);
            $sheet->setCellValue($alpha++.$index, $item['province']);
            $sheet->setCellValue($alpha++.$index, $item['district']);
            $sheet->setCellValue($alpha++.$index, $item['sale_name']);
            $sheet->setCellValue($alpha++.$index, $item['category_name']);
            $sheet->setCellValue($alpha++.$index, $item['child_id']);
            $sheet->setCellValue($alpha++.$index, $item['painting_id']);
            $sheet->setCellValue($alpha++.$index, $item['quantity']);

            if ($item['is_picture'] == 1) {
                $sheet->setCellValue($alpha++.$index, $item['width']);
                $sheet->setCellValue($alpha++.$index, $item['height']);
            } else {
                $sheet->setCellValue($alpha++.$index, $item['width_painting']);
                $sheet->setCellValue($alpha++.$index, $item['height_painting']);
            }

            $sheet->setCellValue($alpha++.$index, $item['material_painting'] ? $item['material_painting'] : $item['material']);


            $sheet->setCellValue($alpha++.$index, $item['key_visual']);
            $sheet->setCellValue($alpha++.$index, $item['logo_type'] == 1 ? 1 : ($item['logo_type'] == 2 ? 0 : '') );
            $sheet->setCellValue($alpha++.$index, $item['change_picture_note']);


            $index++;

        }

        $filename = 'Template Thay tranh' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }

    public function getDetailQuantity($stage_id, $store_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['d' => DATABASE_TRADE.'.change_picture_detail'], [
                'category_name' => 'c.name',
                'd.has_changed_picture'
            ])
            ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'd.category_id = c.id', [])
            ->where('d.store_id = ?', $store_id)
            ->where('d.stage_id = ?', $stage_id);

        $result = $db->fetchAll($select);

        return $result;
    }

}