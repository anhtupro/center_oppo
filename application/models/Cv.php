<?php
class Application_Model_Cv extends Zend_Db_Table_Abstract
{
    protected $_name = 'cv';
   
    function fetchPagination($page, $limit, &$total, $params){
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $db = Zend_Registry::get('db');
        $select = $db->select();

        $col = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
            'p.name', 
            'p.dob', 
            'p.address', 
            'p.phone', 
            'p.email', 
            'p.status', 
            'p.created_at', 
            'p.source',
            'p.gender',
            'job_area'  => "CONCAT(r.name, ' - ',a.name)",
            'job_title' => "job.title",
            'cv_area'   => "CONCAT(r2.name,' - ', province.name)",
            'p.note',
            'p.salary',
            'p.regional_market',
            'province_name' => "province.name",
            'department_name' => "t.name",
            'cat_name' => "c2.name",
            'job_group_name' => "c3.name",
            'is_potential' => "p.is_potential",
        );


        $select->from(array('p' => 'cv'), $col);
        $select->joinLeft(array('job' => 'job'), 'job.id = p.job_id', array() );
        $select->joinLeft(array('t' => 'team'), 't.id = job.department', array() );
        $select->joinLeft(array('c2' => 'job_category'), 'c2.id = job.category', array() );
        $select->joinLeft(array('c3' => 'job_category'), 'p.job_category = c3.id', array() );
        $select->joinLeft(array('j' => 'job_regional'), 'j.job_id = p.job_id', array() );
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = j.regional_market_id', array() );
        $select->joinLeft(array('a' => 'area'), 'a.id = r.area_id', array() );
        $select->joinLeft(array('c' => 'cv_region'), 'c.cv_id = p.id', array() );
        $select->joinLeft(array('r2' => 'regional_market'), 'r2.id = c.district_id', array() );
        $select->joinLeft(array('r3' => 'regional_market'), 'r3.id = r2.parent', array() );
        $select->joinLeft(array('province' => 'province'), 'province.id = r3.province_id', array() );
        $select->joinLeft(array('cv_language' => 'cv_language'), 'cv_language.cv_id = p.id', array());
        
        
        if( isset($params['cv_area']) and $params['cv_area']){
            /*$select->where('r3.area_id IN (?) OR r3.area_id IS NULL', $params['cv_area']);*/
            $select->where('r.area_id IN (?) OR r.area_id IS NULL', $params['cv_area']);
        }
        else{
            if($userStorage->group_id != HR_ID){
                $select->where('job.department = ?', $userStorage->department);
            }
            
        }
        

        if (isset($params['name']) and $params['name'])
            $select->where('p.name LIKE ?', '%'.$params['name'].'%');        

        if (isset($params['dob_from']) and $params['dob_from'])
            $select->where('YEAR(p.dob) >= ?', $params['dob_from']);

        if (isset($params['dob_to']) and $params['dob_to'])
            $select->where('YEAR(p.dob) <= ?', $params['dob_to']);

        if (isset($params['status']) and $params['status'] and ($params['status'] != '')){
            if($params['status'] == 999){
                $select->where('p.status IS NULL OR p.status = 0', null);
            }
            else{
                $select->where('p.status = ?', $params['status']);
            }
        }

        if (isset($params['province']) && $params['province']) {
            $select->where('r3.province_id = ?', $params['province']);
        }


        if (isset($params['created_at_from']) && $params['created_at_from']) {
            $select->where('p.created_at >= ?', $params['created_at_from']);
        }

        if (isset($params['job_category']) && $params['job_category']) {
            $select->where('p.job_category = ? OR job.category = ?', $params['job_category']);
        }

        if (isset($params['schools']) and $params['schools'] and ($params['schools'] != ''))
            $select->where('p.education LIKE ?', '%'.$params['schools'].'%');

        if (isset($params['qualification']) and $params['qualification'])
            $select->where('p.education_type = ?', $params['qualification']);

        if (isset($params['address']) and $params['address'] and ($params['address'] != ''))
            $select->where('p.address = ?', $params['address']);

        if (isset($params['job']) and $params['job'] and ($params['job'] != '')){
            if($params['job'] == 'null')
                $select->where('p.job_id is null');
            else
                $select->where('p.job_id = ?', $params['job']);
        }

        if (isset($params['phone']) and $params['phone'] and ($params['phone'] != ''))
            $select->where('p.phone = ?', $params['phone']);

        if (isset($params['email']) and $params['email'] and ($params['email'] != ''))
            $select->where('p.email LIKE ?', '%'.$params['email'].'%');

        if (isset($params['source']) and $params['source'] and ($params['source'] != ''))
            $select->where('p.source = ?', $params['source']);

        /*
        if (isset($params['district']) && $params['district']) {
            $select->where('r2.id = ?', $params['district']);
        }
        */

        if (isset($params['created_at_to']) && $params['created_at_to']) {
            $select->where('p.created_at <= ?', $params['created_at_to']);
        }

        if (isset($params['brand']) and $params['brand'] and ($params['brand'] != ''))
            $select->where('p.education_branch LIKE ?', '%'.$params['brand'].'%');

        if (isset($params['language_other']) && $params['language_other']) {
            foreach($params['language_other'] as $k => $v){
                if(!$v){
                    $select->orwhere('cv_language.language = ? ',$k);
                }else{
                    $select->orwhere('cv_language.language ='.$k.' AND cv_language.language ='.$v);
                }
            }
        }
        if (isset($params['is_potential']) && $params['is_potential']) {
            $select->where('p.is_potential = ? ',$params['is_potential']);
        }

        $select->group('p.id');
        $select->order('p.id DESC');

        if($limit and empty($params['export'])) {
            $select->limitPage($page, $limit);
        }

        if($_GET['dev']==1){
            echo $select;exit;
        }

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

   function fetchPagination_b($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        if (isset($params['name']) and $params['name'])
            $select->where('p.name LIKE ?', '%'.$params['name'].'%');
        
        if (isset($params['location']) and $params['location'] and ($params['location'] != ''))
            $select->where('p.regional_market = ?', $params['location']);
            
        if (isset($params['schools']) and $params['schools'] and ($params['schools'] != ''))
            $select->where('p.education LIKE ?', '%'.$params['schools'].'%');
            
        if (isset($params['brand']) and $params['brand'] and ($params['brand'] != ''))
            $select->where('p.education_branch LIKE ?', '%'.$params['brand'].'%');
            
        if (isset($params['dob_from']) and $params['dob_from'])
            $select->where('YEAR(p.dob) >= ?', $params['dob_from']);
            
        if (isset($params['qualification']) and $params['qualification'])
            $select->where('p.education_type = ?', $params['qualification']);
            
        if (isset($params['dob_to']) and $params['dob_to'])
            $select->where('YEAR(p.dob) <= ?', $params['dob_to']);
            
        if (isset($params['status']) and $params['status'] and ($params['status'] != '')){
            if($params['status'] == 999){
                $select->where('p.status IS NULL OR p.status = 0', null);
            }
            else{
                $select->where('p.status = ?', $params['status']);
            }
        }
            
        if (isset($params['address']) and $params['address'] and ($params['address'] != ''))
            $select->where('p.address = ?', $params['address']);
            
        if (isset($params['job']) and $params['job'] and ($params['job'] != ''))
            $select->where('p.job_id = ?', $params['job']);

        if (isset($params['source']) and $params['source'] and ($params['source'] != ''))
            $select->where('p.source = ?', $params['source']);
        
        if (isset($params['phone']) and $params['phone'] and ($params['phone'] != ''))
            $select->where('p.phone = ?', $params['phone']);
        
        if (isset($params['email']) and $params['email'] and ($params['email'] != ''))
            $select->where('p.email LIKE ?', '%'.$params['email'].'%');

        if (isset($params['content']) and $params['content'])
           $select->where('p.content LIKE ?', '%'.$params['content'].'%');

        $select->joinLeft(array('j' => 'job'), 'p.job_id = j.id', array('job_name' => 'j.title'));
        
        //Lọc theo ASM
        $select->joinLeft(array('b' => 'job_regional'), 'j.id = b.job_id', array('job_regional_id' => 'b.id'));


        $select->joinLeft(array('r' => 'cv_region'), 'r.cv_id = p.id', array('regional_id_cv' => 'r.region_id', 'district_id_cv' => 'r.district_id'));

        $nestedSelect = "SELECT * FROM province_district GROUP BY province_code";
        $select->joinLeft(array('province' => new Zend_Db_Expr('(' . $nestedSelect . ')')),'r.region_id = province.province_code',array('province.province_name'));


        $select->joinLeft(array('m' => 'regional_market'), 'm.id = province.district_id', array());
        $select->joinLeft(array('a' => 'area_province'), 'r.district_id = a.district_id', array());
        $select->joinLeft(array('district' => 'province_district'), 'r.district_id = district.district_id',array('district.district_center'));

        $select->joinLeft(array('c' => 'regional_market'), 'b.regional_market_id = c.id', array('regional_market_id' => 'c.id'));
        $select->joinLeft(array('d' => 'area'), 'c.area_id = d.id', array('area_id' => 'd.id'));
        
        if (isset($params['list_province_ids']) && $params['list_province_ids']) {
            if (is_array($params['list_province_ids']) && count($params['list_province_ids'])) {
                $select->where('b.id IS NULL OR c.id IN (?)', $params['list_province_ids']);
            } elseif (is_numeric($params['list_province_ids']) && intval($params['list_province_ids']) > 0) {
                $select->where('b.id IS NULL OR c.id = ?', intval($params['list_province_ids']));
            }
        }

        if (isset($params['list_province_ids']) && $params['list_province_ids']) {
            if (is_array($params['list_province_ids']) && count($params['list_province_ids'])) {
                $select->where('m.parent IS NULL OR m.parent IN (?)', $params['list_province_ids']);
            } 
        }

        if (isset($params['province']) && $params['province']) {
            $select->where('r.region_id = ?', $params['province']);
        }

        if (isset($params['district']) && $params['district']) {
            $select->where('r.district_id = ?', $params['district']);
        }

        if (isset($params['cv_area']) && $params['cv_area']) {
            //$select->where('a.area_id IN (?) OR a.area_id IS NULL', $params['cv_area']);
            $select->where('c.area_id IN (?) OR c.area_id IS NULL', $params['cv_area']);
        }

        if (isset($params['job_title']) && $params['job_title']) {
            $select->where('j.job_title = ?', $params['job_title']);
        }

        if (isset($params['created_at_from']) && $params['created_at_from']) {
            $select->where('p.created_at >= ?', $params['created_at_from']);
        }

        if (isset($params['created_at_to']) && $params['created_at_to']) {
            $select->where('p.created_at <= ?', $params['created_at_to']);
        }

        if (isset($params['job_category']) && $params['job_category']) {
            $select->where('p.job_category = ?', $params['job_category']);
        }
        //
        
        $select->group('p.id');
        $select->order('p.id DESC');

        if($limit and empty($params['export'])) {
            $select->limitPage($page, $limit);
        }

        if($_GET['dev']==1){
            echo $select;exit;
        }

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
    
    function fetchPagination_suggest($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));
            $select->where('p.job_id = ?', 0);

        if (isset($params['name']) and $params['name'])
            $select->where('p.name LIKE ?', '%'.$params['name'].'%');
        
        if (isset($params['location']) and $params['location'] and ($params['location'] != ''))
            $select->where('p.regional_market = ?', $params['location']);
            
        if (isset($params['schools']) and $params['schools'] and ($params['schools'] != ''))
            $select->where('p.education LIKE ?', '%'.$params['schools'].'%');
            
        if (isset($params['brand']) and $params['brand'] and ($params['brand'] != ''))
            $select->where('p.education_branch LIKE ?', '%'.$params['brand'].'%');
            
        if (isset($params['dob_from']) and $params['dob_from'])
            $select->where('YEAR(p.dob) >= ?', $params['dob_from']);
            
        if (isset($params['qualification']) and $params['qualification'])
            $select->where('p.education_type = ?', $params['qualification']);
            
        if (isset($params['dob_to']) and $params['dob_to'])
            $select->where('YEAR(p.dob) <= ?', $params['dob_to']);
            
        if (isset($params['status']) and $params['status'] and ($params['status'] != ''))
            $select->where('p.status = ?', $params['status']);
            
        if (isset($params['address']) and $params['address'] and ($params['address'] != ''))
            $select->where('p.address = ?', $params['address']);
            
        if (isset($params['job']) and $params['job'] and ($params['job'] != ''))
            $select->where('p.job_id = ?', $params['job']);
        
        if (isset($params['phone']) and $params['phone'] and ($params['phone'] != ''))
            $select->where('p.phone = ?', $params['phone']);
        
        if (isset($params['email']) and $params['email'] and ($params['email'] != ''))
            $select->where('p.email LIKE ?', '%'.$params['email'].'%');

        if (isset($params['content']) and $params['content'])
           $select->where('p.content LIKE ?', '%'.$params['content'].'%');

        $select->joinLeft(array('j' => 'job'), 'p.job_id = j.id', array('job_name' => 'j.title'));
        
        //Lọc theo ASM
        $select->joinLeft(array('b' => 'job_regional'), 'j.id = b.job_id', array('job_regional_id' => 'b.id'));
        $select->joinLeft(array('c' => 'regional_market'), 'p.regional_market = c.id', array('regional_market_id' => 'c.id'));
        $select->joinLeft(array('d' => 'area'), 'c.area_id = d.id', array('area_id' => 'd.id'));
        
        if (isset($params['list_province_ids']) && $params['list_province_ids']) {
            if (is_array($params['list_province_ids']) && count($params['list_province_ids'])) {
                $select->where('p.regional_market IS NULL OR c.id IN (?)', $params['list_province_ids']);
            } elseif (is_numeric($params['list_province_ids']) && intval($params['list_province_ids']) > 0) {
                $select->where('c.id = ?', intval($params['list_province_ids']));
            }
        }
        //
        
        $select->group('p.id');
        $select->order('p.created_at DESC');

        if ($limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }
    
    function replyAction($email,$name){
        $hr_signature = '
                    <h3 style="color: #00b050;margin-bottom: 0;">Recruitment Team</h3>
                    <p style="margin-bottom: 0;
                        margin-top: 0;">__________________________________</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">T (84-8) 39202555 - ext: 111</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">F (84-8) 39202555 - ext: 120</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">E <a href="mailto:recruitment@oppo-aed.vn">recruitment@oppo-aed.vn</a></p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">OPPO Science & Technology Co.,Ltd</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">12th Floor, Lim II Tower, No. 62A CMT8 St., Ward 6, Dist. 3, HCMC, VN</p>
                    <p style="margin-bottom: 0;
                        margin-top: 0;">__________________________________</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 0;"><a href="http://vieclam.oppomobile.vn/">www.vieclam.oppomobile.vn</a></p>';
        
   //     $this->_helper->viewRenderer->setNoRender();
     //   $this->_helper->layout->disableLayout();
        $en_name = My_String::khongdau( $name );
       
        try {
            error_reporting(~E_ALL);
            ini_set("display_error", 0);
            set_time_limit(0);
            $mailfromname = "OPPO Tuyển dụng"; // Config this
            //123321abc
            $subject = 'CONFIRMATION FOR RECEIPT OF APPLICATION – OPPO VIET NAM';
            $content = '<div><p class="">
                Dear '. $en_name.',
                <br><br>OPPO Vietnam appreciates your interest in OPPO and we acknowledge receipt of your application. ';
            $content .= '<br><br>It takes us maximal two weeks to review your application letter. If your skills, qualifications and experience matches with our requirements, we will arrange an interview with you as soon as possible.';
            $content .= '<br><br>Unfortunately, if the position that you applied for is unavailable, OPPO will save your resume in our candidate system for 12 months to refer for other positions.';
            $content .= '<br><br>Thank you for your application to OPPO Việt Nam.';
            $content .= '<br><br>Sincerely,';
            $content .= '<br><br><strong>Note: This is an automotive reply-email. Kindly don’t reply this email. </strong>';
            $content .= '<br><br>---------------------------------------------------------------';
            $content .= '<br><br>Chào '. $name .',
                     <br><br>OPPO Việt Nam rất cảm ơn bạn đã quan tâm đến thông tin tuyển dụng tại OPPO, chúng tôi xác nhận bạn đã ứng tuyển thành công.';
            $content .= '<br><br>Chúng tôi sẽ cần tối đa hai tuần để xem xét hồ sơ của bạn. Nếu kỹ năng, năng lực và kinh nghiệm của bạn phù hợp với yêu cầu tuyển dụng, chúng tôi sẽ sắp xếp một buổi phỏng vấn với bạn trong thời gian sớm nhất có thể. ';
            $content .= '<br><br>Trong trường hợp chúng tôi đã hoàn thành việc tuyển dụng cho vị trí này, OPPO sẽ lưu trữ hồ sơ của bạn vào hệ thống dữ liệu ứng viên trong 12 tháng cho những vị trí khác. ';
            $content .= '<br><br>Cảm ơn bạn đã ứng tuyển vào OPPO Việt Nam.';
            $content .= '<br><br>Trân trọng, ';
            $content .= '<br>Bộ phận Tuyển dụng OPPO';
            $content .= '<br><br><strong>Lưu ý: Đây là email trả lời tự động. Vui lòng không phản hồi lại email này.</strong>'. $hr_signature;
        
            $mailto = array();
           // $mailto[] = 'truongtoan2611.1510@gmail.com';
              $mailto[] = $email;
                        
            $res = $this->sendmail($mailto, $subject, $content);

        }catch (Exception $e) {
        
        }
        return $res;
    }
    
    function  informToPersonChargAction($name, $email , $id_cv , $id_job=0){
        if(!isset($id_job) || empty($id_job)){
            return 1;
        }
        $QJob = new Application_Model_Job();
        $QJobEmailPersonCharge = new Application_Model_JobEmailPersonCharge();
        
        //email person in charge
        $where_person = [];
        $where_person[] = $QJobEmailPersonCharge->getAdapter()->quoteInto("job_id = ?",$id_job);
        $persion_charg_tmp = $QJobEmailPersonCharge->fetchAll($where_person);
       
        if(empty($persion_charg_tmp->toArray())){
            return 1;
        }
        
        $where_job = [];
        $where_job = $QJob->getAdapter()->quoteInto('id = ?', $id_job);
        $job = $QJob->fetchRow($where_job);
       
        $hr_signature = '
                    <h3 style="color: #00b050;margin-bottom: 0;">Recruitment Team</h3>
                    <p style="margin-bottom: 0;
                        margin-top: 0;">__________________________________</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">T (84-8) 39202555 - ext: 111</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">F (84-8) 39202555 - ext: 120</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">E <a href="mailto:recruitment@oppo-aed.vn">recruitment@oppo-aed.vn</a></p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">OPPO Science & Technology Co.,Ltd</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">12th Floor, Lim II Tower, No. 62A CMT8 St., Ward 6, Dist. 3, HCMC, VN</p>
                    <p style="margin-bottom: 0;
                        margin-top: 0;">__________________________________</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 0;"><a href="http://vieclam.oppomobile.vn/">www.vieclam.oppomobile.vn</a></p>';
        try {
            error_reporting(~E_ALL);
            ini_set("display_error", 0);
            set_time_limit(0);
            
            $mailfromname = "OPPO Thông báo hồ sơ mới"; // Config this
            $subject = $name.' đã nộp hồ sơ cho vị trí '.$job->title;
            $content = 'Xin chào.';
            $content .= '<br><br>Vừa có hồ sơ ứng tuyển vào công việc của bạn';
            $content .= '<br><br><b>Vị trí ứng tuyển:</b> '.$job->title;
            $content .= '<br><b>Ứng viên:</b> '.$name;
            $content .= '<br><b>Email:</b> '.$email;
            $content .= '<br><b>Link xem hồ sơ:</b> <a>'.HOST.'job/view-cv?id='.$id_cv.'</a>';
            $content .= '<br><br><strong>Vui lòng truy cập theo link phía trên để xem hồ sơ ứng tuyển.</strong>'. $hr_signature;
            $mailto = array();
            foreach($persion_charg_tmp as $value){
                $mailto[] = $value->email;
            }
            
            $res = $this->sendmail($mailto, $subject, $content);
        }catch (Exception $e) {
        }
        return $res;
    }
    
     private function sendmail($to, $subject, $maildata, $image = ''){
        try {
            //todo send mail
            $app_config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');

            $config = array(
                'auth' => $app_config->mail->smtp->auth,
                'username' => 'recruitment@oppo-aed.vn',
                'password' => '0pp0Te@m2019',
                'port' => $app_config->mail->smtp->port,
                'ssl' => $app_config->mail->smtp->ssl
            );


            $transport = new Zend_Mail_Transport_Smtp($app_config->mail->smtp->host, $config);
            
            $mail = new Zend_Mail($app_config->mail->smtp->charset);
            
            $mail->setBodyHtml($maildata);
            if($image != ''){
                $mail->addAttachment($image);
            }
            
            $mail->setFrom('recruitment@oppo-aed.vn', 'OPPO Recruitment');

            $mail->addTo($to);
            $mail->setSubject($subject);

            $r = $mail->send($transport);

            if (!$r)
                return -1;
            else return 1;
        } catch (Exception $exc) {
            return -1;
        }
    }

}