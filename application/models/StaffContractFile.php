<?php

class Application_Model_StaffContractFile extends Zend_Db_Table_Abstract
{
    protected $_name = 'staff_contract_file';

    public function getByStaffContract($staff_contract_id){
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('s' => $this->_name))
            ->where('s.staff_contract_id =?', $staff_contract_id);
        $result = $db->fetchAll($select);
        return $result;

    }

    public function save($data){

        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
            $this->insert($data);
            $arr = array('code' => 1, 'message' => 'Done');
            $db->commit();
            return $arr;
        } catch (Exception $e) {
            $db->rollBack();
            $arr = array('code' => -1, 'message' => $e->getMessage());
            return $arr;
        }

    }
    public function remove($id){

        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
            $where = $this->getAdapter()->quoteInto('id = ?',$id);
            $this->delete($where);
            $arr = array('code' => 1, 'message' => 'Done');
            $db->commit();
            return $arr;
        } catch (Exception $e) {
            $db->rollBack();
            $arr = array('code' => -1, 'message' => $e->getMessage());
            return $arr;
        }

    }
    public function geUrlById($id){

        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('s' => $this->_name),'s.url')
            ->where('s.id =?', $id);
        $result = $db->fetchRow($select);
        return $result;

    }

}



