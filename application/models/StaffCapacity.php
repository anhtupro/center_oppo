<?php
class Application_Model_StaffCapacity extends Zend_Db_Table_Abstract
{
    protected $_name = 'staff_capacity';
    
    public function getInfoCapacity($capacity_id) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.staff_id",
            "p.title_id", 
            "title_name" => "t.name", 
            "team_name" => "t2.name", 
            "department_name" => "t3.name",
            "plan_title" => "c.title",
            "plan_id" => "p.plan_id",
            "note" => "p.note"
        );

        $select->from(array('p' => 'staff_capacity'), $arrCols);
        $select->joinLeft(array('t' => 'team'), 't.id = p.title_id', array());
        $select->joinLeft(array('t2' => 'team'), 't2.id = t.parent_id', array());
        $select->joinLeft(array('t3' => 'team'), 't3.id = t2.parent_id', array());
        $select->joinLeft(array('c' => 'capacity_plan'), 'c.id = p.plan_id', array());

        $select->where('p.id = ?', $capacity_id);
        $result = $db->fetchRow($select);

        return $result;
    }
    
    public function getDictionaryStaff($capacity_id) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.staff_id", 
            "title_name" => "dictionary.title",
            "dictionary.type", 
            "type_name" => "(CASE WHEN dictionary.type = 1 THEN 'Tư duy' WHEN dictionary.type = 2 THEN 'Kỹ năng' END)",
            "d.level", 
            "d.share", 
            "level_name" => "l.title",
            "d.appraisal_staff_level", 
            "d.appraisal_head_level", 
            "level_head_name" => "l2.title",
            "dictionary_id" => "dictionary.id",
            "field_title" => "f.title"
        );

        $select->from(array('p' => 'staff_capacity'), $arrCols);
        $select->joinLeft(array('f' => 'staff_capacity_field'), 'f.capacity_id = p.id AND f.is_deleted = 0', array());
        $select->joinLeft(array('d' => 'staff_capacity_field_dictionary'), 'd.field_id = f.id AND d.is_deleted = 0', array());
        $select->joinLeft(array('dictionary' => 'capacity_dictionary'), 'dictionary.id = d.dictionary_id', array());
        $select->joinLeft(array('l' => 'capacity_dictionary_level'), 'l.level = d.level AND l.dictionary_id = dictionary.id', array());
        $select->joinLeft(array('l2' => 'capacity_dictionary_level'), 'l2.level = d.appraisal_head_level AND l2.dictionary_id = dictionary.id', array());
		
	$select->where('d.id IS NOT NULL', NULL);
        $select->where('dictionary.id IS NOT NULL', NULL);
        $select->where('p.id = ?', $capacity_id);
	$select->order(['dictionary.type', 'dictionary.title']);
        
        $result = $db->fetchAll($select);
        
        return $result;
    }
    
    public function getDictionaryStaffGroup($capacity_id) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.staff_id", 
            "title_name" => "dictionary.title",
            "dictionary.type", 
            "type_name" => "(CASE WHEN dictionary.type = 1 THEN 'Tư duy' WHEN dictionary.type = 2 THEN 'Kỹ năng' END)",
            "level" => "MAX(d.level)", 
            "d.share", 
            "level_name" => "l.title",
            "d.appraisal_staff_level", 
            "d.appraisal_head_level", 
            "level_head_name" => "l2.title",
            "dictionary_id" => "dictionary.id",
            "field_title" => "f.title"
        );

        $select->from(array('p' => 'staff_capacity'), $arrCols);
        $select->joinLeft(array('f' => 'staff_capacity_field'), 'f.capacity_id = p.id AND f.is_deleted = 0', array());
        $select->joinLeft(array('d' => 'staff_capacity_field_dictionary'), 'd.field_id = f.id AND d.is_deleted = 0', array());
        $select->joinLeft(array('dictionary' => 'capacity_dictionary'), 'dictionary.id = d.dictionary_id', array());
        $select->joinLeft(array('l' => 'capacity_dictionary_level'), 'l.level = d.level AND l.dictionary_id = dictionary.id', array());
        $select->joinLeft(array('l2' => 'capacity_dictionary_level'), 'l2.level = d.appraisal_head_level AND l2.dictionary_id = dictionary.id', array());
		
	$select->where('d.id IS NOT NULL', NULL);
        $select->where('dictionary.id IS NOT NULL', NULL);
        $select->where('dictionary.is_deleted = 0 OR dictionary.is_deleted IS NULL', NULL);
        $select->where('p.id = ?', $capacity_id);
        $select->group(['dictionary.id']);
	$select->order(['dictionary.type', 'dictionary.title']);
        
        $result = $db->fetchAll($select);
        
        
        return $result;
    }
    
    public function getScienceStaff($capacity_id) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "title_name" => "p.desc", 
            "p.level", 
            "p.share", 
            "p.appraisal_staff_level", 
            "p.appraisal_head_level",
        );

        $select->from(array('p' => 'staff_capacity_science'), $arrCols);
        $select->joinLeft(array('f' => 'staff_capacity_field'), 'f.id = p.capacity_field_id', array());

        $select->where('f.capacity_id = ?', $capacity_id);
        $select->where('p.is_deleted = 0', NULL);
        $select->where('f.is_deleted = 0', NULL);
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getListLevel($capacity_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "dictionary.id", 
            "p.title_id",
            "title_name" => "dictionary.title", 
            "type" => '(CASE WHEN (dictionary.type = 1) THEN "Tư duy" 
                                       WHEN (dictionary.type = 2) THEN "Kỹ năng" ELSE "" END)', 
            "d.share", 
            "d.level", 
            "level_name" => "l.title",
            "d.appraisal_staff_level",
            "appraisal_staff_level_name" => "l2.title",
            "d.appraisal_head_level",
            "appraisal_head_level_name" => "l3.title",
            'dictionary_id' => 'dictionary.id',
            'field_id' => 'f.id',
            'staff_capacity_field_dictionary_id' => 'd.id'
        );

        $select->from(array('p' => 'staff_capacity'), $arrCols);
        $select->joinLeft(array('f' => 'staff_capacity_field'), 'f.capacity_id = p.id AND f.is_deleted = 0 ', array());
        $select->joinLeft(array('d' => 'staff_capacity_field_dictionary'), 'd.field_id = f.id AND d.is_deleted = 0', array());
        $select->joinLeft(array('dictionary' => 'capacity_dictionary'), 'dictionary.id = d.dictionary_id', array());
        $select->joinLeft(array('l' => 'capacity_dictionary_level'), 'l.dictionary_id = dictionary.id AND l.level = d.level', array());
        $select->joinLeft(array('l2' => 'capacity_dictionary_level'), 'l2.dictionary_id = dictionary.id AND l2.level = d.appraisal_staff_level', array());
        $select->joinLeft(array('l3' => 'capacity_dictionary_level'), 'l3.dictionary_id = dictionary.id AND l3.level = d.appraisal_head_level', array());
        
        $select->where('f.is_deleted = 0 ', NULL);
        $select->where('d.is_deleted = 0 AND dictionary.id IS NOT NULL', NULL);
        $select->where('p.id = ?', $capacity_id);
        $select->order(['dictionary.type', 'dictionary.title']);
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getListLevelGroup($capacity_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "dictionary.id", 
            "p.title_id",
            "title_name" => "dictionary.title", 
            "type" => '(CASE WHEN (dictionary.type = 1) THEN "Tư duy" 
                                       WHEN (dictionary.type = 2) THEN "Kỹ năng" ELSE "" END)', 
            "type_id" => "dictionary.type",
            "d.share", 
            "d.level", 
            "level_name" => "l.title",
            "d.appraisal_staff_level",
            "appraisal_staff_level_name" => "l2.title",
            "d.appraisal_head_level",
            "appraisal_head_level_name" => "l3.title",
            'dictionary_id' => 'dictionary.id',
            'field_id' => 'f.id',
            'staff_capacity_field_dictionary_id' => 'd.id'
        );

        $select->from(array('p' => 'staff_capacity'), $arrCols);
        $select->joinLeft(array('f' => 'staff_capacity_field'), 'f.capacity_id = p.id AND f.is_deleted = 0 ', array());
        $select->joinLeft(array('d' => 'staff_capacity_field_dictionary'), 'd.field_id = f.id AND d.is_deleted = 0', array());
        $select->joinLeft(array('dictionary' => 'capacity_dictionary'), 'dictionary.id = d.dictionary_id', array());
        $select->joinLeft(array('l' => 'capacity_dictionary_level'), 'l.dictionary_id = dictionary.id AND l.level = d.level', array());
        $select->joinLeft(array('l2' => 'capacity_dictionary_level'), 'l2.dictionary_id = dictionary.id AND l2.level = d.appraisal_staff_level', array());
        $select->joinLeft(array('l3' => 'capacity_dictionary_level'), 'l3.dictionary_id = dictionary.id AND l3.level = d.appraisal_head_level', array());
        
        $select->where('f.is_deleted = 0 ', NULL);
        $select->where('(d.is_deleted = 0 OR d.is_deleted IS NULL) AND dictionary.id IS NOT NULL', NULL);
        $select->where('p.id = ?', $capacity_id);
        $select->where('dictionary.is_deleted = 0 OR dictionary.is_deleted IS NULL', NULL);
        //$select->group('dictionary.id');
        $select->order(['dictionary.type', 'dictionary.title']);
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getListLevelGroupBy($capacity_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "dictionary.id", 
            "p.title_id",
            "title_name" => "dictionary.title", 
            "type" => '(CASE WHEN (dictionary.type = 1) THEN "Tư duy" 
                                       WHEN (dictionary.type = 2) THEN "Kỹ năng" ELSE "" END)', 
            "type_id" => "dictionary.type",
            "d.share", 
            "d.level", 
            "level_name" => "l.title",
            "d.appraisal_staff_level",
            "appraisal_staff_level_name" => "l2.title",
            "d.appraisal_head_level",
            "appraisal_head_level_name" => "l3.title",
            'dictionary_id' => 'dictionary.id',
            'field_id' => 'f.id',
            'staff_capacity_field_dictionary_id' => 'd.id'
        );

        $select->from(array('p' => 'staff_capacity'), $arrCols);
        $select->joinLeft(array('f' => 'staff_capacity_field'), 'f.capacity_id = p.id AND f.is_deleted = 0 ', array());
        $select->joinLeft(array('d' => 'staff_capacity_field_dictionary'), 'd.field_id = f.id AND d.is_deleted = 0', array());
        $select->joinLeft(array('dictionary' => 'capacity_dictionary'), 'dictionary.id = d.dictionary_id', array());
        $select->joinLeft(array('l' => 'capacity_dictionary_level'), 'l.dictionary_id = dictionary.id AND l.level = d.level', array());
        $select->joinLeft(array('l2' => 'capacity_dictionary_level'), 'l2.dictionary_id = dictionary.id AND l2.level = d.appraisal_staff_level', array());
        $select->joinLeft(array('l3' => 'capacity_dictionary_level'), 'l3.dictionary_id = dictionary.id AND l3.level = d.appraisal_head_level', array());
        
        $select->where('f.is_deleted = 0 ', NULL);
        $select->where('(d.is_deleted = 0 OR d.is_deleted IS NULL) AND dictionary.id IS NOT NULL', NULL);
        $select->where('p.id = ?', $capacity_id);
        $select->where('dictionary.is_deleted = 0 OR dictionary.is_deleted IS NULL', NULL);
        $select->group('dictionary.id');
        $select->order(['dictionary.type', 'dictionary.title']);
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getListScience($capacity_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "type" => "('Kiến thức')",
            "title_name" => "p.desc", 
            "p.level",
            "p.appraisal_staff_level",
            "p.appraisal_head_level",
            "appraisal_staff_level_name" => "(CONCAT('Level',' ',p.appraisal_staff_level))",
            "appraisal_head_level_name" => "(CONCAT('Level',' ',p.appraisal_head_level))",
            "f.capacity_id",
            'field_id' => 'capacity_field_id',
            'capacity_science_id' => 'p.id'
        );

        $select->from(array('p' => 'staff_capacity_science'), $arrCols);
        $select->joinLeft(array('f' => 'staff_capacity_field'), 'f.id = p.capacity_field_id', array());

        $select->where('f.capacity_id = ? ', $capacity_id);
        $select->where('p.is_deleted = 0', NULL);
        $result = $db->fetchAll($select);

        return $result;
    }

    
    public function getDataExport($params)
    {
        $db = Zend_Registry::get("db");
        $select_dictionary = $db->select();
        $select_science = $db->select();

        $sub_select = "  
            SELECT d.id, MAX(d.level) as level, d.field_id, MAX(d.share) as share,  d.dictionary_id, i.title, MAX(d.appraisal_head_level) as appraisal_head_level
            from staff_capacity c 
            JOIN staff_capacity_field f ON c.id = f.capacity_id AND f.is_deleted = 0
            JOIN staff_capacity_field_dictionary d ON f.id = d.field_id AND d.is_deleted = 0
            JOIN capacity_dictionary i ON d.dictionary_id = i.id 
            WHERE c.is_deleted = 0 AND c.is_head_appraisal = 1 AND (i.is_deleted = 0 OR i.is_deleted IS NULL)
            GROUP BY c.id, dictionary_id
        ";

        // select dictionary
        $arrColsDictionary = array(
            'staff_id' => 'p.to_staff',
            'code' => 's.code',
            'staff_name' => "CONCAT(s.firstname, ' ', s.lastname)",
            'title_id' => 't.id',
            'capacity_id' => 'c.id',
            'field_id' => 'f.id',
            'title_name' => 't.name',
            'team_name' => 't2.name',
            'department_name' => 't3.name',
            'dictionary' => 'd.title',
            'd.level',
            'd.share',
            'd.appraisal_head_level',
            'c.plan_id'
        );

        $select_dictionary->from(array('p' => 'appraisal_office_to_do_root'), $arrColsDictionary);
        $select_dictionary->joinLeft(array('s' => 'staff'), 's.id = p.to_staff', array());
        $select_dictionary->joinLeft(array('t' => 'team'), 't.id = s.title', array());
        $select_dictionary->joinLeft(array('t2' => 'team'), 't2.id = t.parent_id', array());
        $select_dictionary->joinLeft(array('t3' => 'team'), 't3.id = t2.parent_id', array());
        $select_dictionary->join(array('c' => 'staff_capacity'), 'c.staff_id = s.id AND c.is_deleted = 0 AND c.is_head_appraisal = 1', array());
        $select_dictionary->join(array('f' => 'staff_capacity_field'), 'f.capacity_id = c.id AND f.is_deleted = 0', array());
        $select_dictionary->join(array('d' => new Zend_Db_Expr('(' . $sub_select . ')')), 'f.id = d.field_id', array());
        $select_dictionary->where('s.off_date IS NULL', NULL);
        $select_dictionary->where('p.is_del = 0');
        $select_dictionary->where('c.plan_id = ?', $params['plan_id']);


        // select science
        $arrColsScience = array(
            'staff_id' => 'p.to_staff',
            'code' => 's.code',
            'staff_name' => "CONCAT(s.firstname, ' ', s.lastname)",
            'title_id' => 't.id',
            'capacity_id' => 'c.id',
            'field_id' => 'f.id',
            'title_name' => 't.name',
            'team_name' => 't2.name',
            'department_name' => 't3.name',
            'dictionary' => 'd.desc',
            'd.level',
            'd.share',
            'd.appraisal_head_level',
            'c.plan_id'
        );


        $select_science->from(array('p' => 'appraisal_office_to_do_root'), $arrColsScience);
        $select_science->joinLeft(array('s' => 'staff'), 's.id = p.to_staff', array());
        $select_science->joinLeft(array('t' => 'team'), 't.id = s.title', array());
        $select_science->joinLeft(array('t2' => 'team'), 't2.id = t.parent_id', array());
        $select_science->joinLeft(array('t3' => 'team'), 't3.id = t2.parent_id', array());
        $select_science->join(array('c' => 'staff_capacity'), 'c.staff_id = s.id AND c.is_deleted = 0 AND c.is_head_appraisal = 1', array());
        $select_science->join(array('f' => 'staff_capacity_field'), 'f.capacity_id = c.id AND f.is_deleted = 0', array());
        $select_science->join(array('d' => 'staff_capacity_science'), 'f.id = d.capacity_field_id AND d.is_deleted = 0', array());
        $select_science->where('s.off_date IS NULL', NULL);
        $select_science->where('p.is_del = 0');
        $select_science->where('c.plan_id = ?', $params['plan_id']);


        if(!empty($params['title'])){
            $select_dictionary->where("t.id IN (?)", $params['title']);
            $select_science->where("t.id IN (?)", $params['title']);
        }

        if(!empty($params['team'])){
            $select_dictionary->where("t2.id IN (?)", $params['team']);
            $select_science->where("t2.id IN (?)", $params['team']);
        }

        if(!empty($params['department'])){
            $select_dictionary->where("t3.id IN (?)", $params['department']);
            $select_science->where("t3.id IN (?)", $params['department']);
        }

        if($params['level'] == 2){
            $select_dictionary->where("p.from_staff = ?", $params['staff_id']);
            $select_science->where("p.from_staff = ?", $params['staff_id']);
        }

        $select_union = $db->select()->union(array($select_dictionary, $select_science), Zend_Db_Select::SQL_UNION_ALL)->order('staff_id');

        $result = $db->fetchAll($select_union);

        return $result;
    }

    public function export($params)
    {
        require_once 'PHPExcel.php';
        
        $QCapacity = new Application_Model_Capacity();
        
        $page = NULL;
        $limit = NULL; 
        $total = 0;
        
        $data = $this->getDataExport($params);
        $get_data_point = $QCapacity->fetchPaginationResult($page, $limit, $total, $params);
        
        
        $data_point = [];
        foreach($get_data_point as $key=>$value){
            $data_point[$value['staff_id']] = $value['point'];
        }
        

        $PHPExcel = new PHPExcel();
        $heads = array(
            'STAFF ID',
            'CODE',
            'HỌ TÊN',
            'PHÒNG BAN',
            'BỘ PHẬN',
            'CHỨC DANH',
            'TƯ DUY',
            'LEVEL',
            'TRỌNG SỐ',
            'TBP ĐÁNH GIÁ',
            'ĐIỂM'
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($data as $item){

            if ($item['dictionary'] && $item['level'] && $item['share']) {
                $alpha    = 'A';
                //            $sheet->setCellValue($alpha++.$index, $i++);
                $sheet->setCellValue($alpha++.$index, $item['staff_id']);
                $sheet->setCellValue($alpha++.$index, $item['code']);
                $sheet->setCellValue($alpha++.$index, $item['staff_name']);
                $sheet->setCellValue($alpha++.$index, $item['department_name']);
                $sheet->setCellValue($alpha++.$index, $item['team_name']);
                $sheet->setCellValue($alpha++.$index, $item['title_name']);
                $sheet->setCellValue($alpha++.$index, $item['dictionary']);
                $sheet->setCellValue($alpha++.$index, $item['level']);
                $sheet->setCellValue($alpha++.$index, $item['share']);
                $sheet->setCellValue($alpha++.$index, $item['appraisal_head_level']);
                $sheet->setCellValue($alpha++.$index, ($data_point[$item['staff_id']] ? number_format($data_point[$item['staff_id']], 2) : NULL) );

                $index++;
            }

        }

        $filename = 'Khung năng lực nhân viên';
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }
    
    
    
    
}