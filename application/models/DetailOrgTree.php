<?php

class Application_Model_DetailOrgTree extends Zend_Db_Table_Abstract {

    protected $_name = "detail_org_tree";
    
    public function getListDataWithLabel($level_chart,$id_label,$flag_org_tree){
        $db = Zend_Registry::get('db');
        $select = $db->select()
        ->from(array('dot' => $this->_name))
        ->joinInner(array('ot' => "org_tree"),"ot.id = dot.id_org_tree")
        ->where('dot.id_label = ?',$id_label)
        ->where('ot.level_chart = ?',$level_chart)
        ->where('ot.flag_org_tree=?',$flag_org_tree);

        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getIdTitleByLevelChartAndFlag($flag,$level_chart){
        $db = Zend_Registry::get('db');
        $select = $db->select()
        ->from(array('ot' => "org_tree"),array())
        ->joinInner(array('dot' => $this->_name),"ot.id = dot.id_org_tree",array("dot.id_label"))
        ->where('ot.flag_org_tree = ?',$flag)
        ->where('ot.level_chart = ?',$level_chart);
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function getListDataWithIdOrg($id_node) {
        $db = Zend_Registry::get('db');
        $select = $db->select()
        ->from(array('dot' => $this->_name),array("dot.id_label","dot.group"))
        ->joinInner(array('ot' => "org_tree"),"ot.id = dot.id_org_tree",array("ot.parent_id"))
        ->joinLeft(array('st' => "staff"),"dot.staff_id = st.id",array("st.id", "st.code","CONCAT(st.firstname, ' ', st.lastname) as name",))
        ->where('dot.id_org_tree = ?',$id_node);
        $result = $db->fetchAll($select);
        return $result;
    }
    public function getListIdStaff($id_org_tree){
        $db = Zend_Registry::get('db');
        $select = $db->select()
        ->from(array('dot' => $this->_name),array("dot.staff_id","dot.id"))
        ->where('dot.id_org_tree = ?',$id_org_tree);
        $result = $db->fetchAll($select);
        return $result;
    }
    public function deleteStaffByOrgTree($org_tree_id , $staff_id){
        $db = Zend_Registry::get('db');
        $where = array (
          "id_org_tree = ? " =>   $org_tree_id,
          "staff_id = ? " => $staff_id
        );
        return $db->delete($this->_name,$where);
    }
    public function addDetailOrgTree($id_org_tree, $id_label , $id_staff){
        $db = Zend_Registry::get('db');
        $data = array(
            "id_org_tree" => $id_org_tree,
            "id_label" => $id_label,
            "staff_id"=>$id_staff,
            "group" => "",
        );
        return $db->insert($this->_name,$data);
    }
    public function updateGroup($org_tree_id,$group){
        $db = Zend_Registry::get('db');
        $where = array (
          "id_org_tree = ? " =>   $org_tree_id,
        );
        $data = array(
            "group" => $group,
        );
        return $db->update($this->_name,$data, $where);
    }
}