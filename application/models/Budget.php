<?php

class Application_Model_Budget extends Zend_Db_Table_Abstract
{
    protected $_name = 'budget';
    protected $_schema = DATABASE_TRADE;

    public function isExisted($areaId, $year)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                    ->from(['b' => DATABASE_TRADE . '.budget'], ['b.id'])
                    ->where('b.year = ?', $year)
                    ->where('b.area_id = ?', $areaId);

        $result = $db->fetchRow($select);

        return $result ? true : false;
    }

    public function getListBudget($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                    ->from(['b' => DATABASE_TRADE . '.budget'], [
                        'b.year',
                        'b.total_value',
                        'area' => 'a.name',
                        'd.*'
                    ])
                    ->joinLeft(['d' => DATABASE_TRADE . '.budget_detail'], 'b.id = d.budget_id', [])
                    ->joinLeft(['a' => 'area'], 'b.area_id = a.id', [])
                    ->where('a.bigarea_id IS NOT NULL')
                    ->where('b.year = ?', $params['year']);

        if ($params['area_id']) {
            $select->where('b.area_id = ?', $params['area_id']);
        }

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $listBudget [$element['budget_id']] ['year'] = $element['year'];
            $listBudget [$element['budget_id']] ['total_value'] = $element['total_value'];
            $listBudget [$element['budget_id']] ['area'] = $element['area'];
            $listBudget [$element['budget_id']] ['id'] = $element['budget_id'];
            $listBudget [$element['budget_id']] [$element['type']] = $element['value'];
        }

        return $listBudget;
    }

    public function getTotalBudget($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['b' => DATABASE_TRADE.'.budget'], [
                        'total_budget' => 'SUM(b.total_value)'
                     ])
                    ->where('b.year = ?', $params['year']);

        if ($params['area_list']) {
            $select->where('b.area_id IN (?)', $params['area_list']);
        }

        if ($params['area_id']) {
            $select->where('b.area_id = ?', $params['area_id']);
        }

        $result = $db->fetchOne($select);

        return $result;
    }
}    