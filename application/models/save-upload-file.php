<?php
$this->_helper->layout->disableLayout();
$this->_helper->viewRenderer->setNoRender(true);
echo '<link rel="stylesheet" href="' . HOST . 'css/bootstrap.min.css">';


$userStorage                = Zend_Auth::getInstance()->getStorage()->read();
// $staff_id          = $this->getRequest()->getParam('staff_id');
$staff_id          = 5899;
$QStaffUploadFile  = new Application_Model_StaffUploadFile();
$db = Zend_Registry::get('db');
$db->beginTransaction();

try {

    //UPLOAD FILE
    foreach ($_FILES['pr_quotation']['size'] as $k => $val) {

        if ($_FILES['pr_quotation']['size'][$k] != 0) {

            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'staff-upload' .
                    DIRECTORY_SEPARATOR . $staff_id;


            $upload = new Zend_File_Transfer();
            $upload->setOptions(array('ignoreNoFile' => true));

            $upload->addValidator('Extension', false, 'ppt,pptx,pdf,doc,docx,jpg,jpeg,png,gif,xls,xlsx,msg');
            $upload->addValidator('Size', false, array('max' => '4MB'));
            $upload->addValidator('ExcludeExtension', false, 'php,sh');

            $name_file_k = "pr_quotation_" . $k . "_";
            $files       = $upload->getFileInfo($name_file_k);
            $hasPhoto    = false;
            $data_file   = array();
            if (isset($files[$name_file_k]) and $files[$name_file_k]) {
                $fileInfo = (isset($files[$name_file_k]) and $files[$name_file_k]) ? $files[$name_file_k] : null;

                if (!is_dir($uploaded_dir))
                    @mkdir($uploaded_dir, 0777, true);
                $upload->setDestination($uploaded_dir);

                //Rename
                $old_name = $fileInfo['name'];

                $tExplode  = explode('.', $old_name);
                $extension = end($tExplode); // đuôi file
                //$new_name = 'UPLOAD_' .date('Ymd').substr ( microtime (), 2, 4 ).$k.'_'. $old_name;
                $new_name  = 'UPLOAD-' . date('Ymd') . substr(microtime(), 2, 4) . md5(uniqid('', true)) . '.' . $extension;
               
                $upload->addFilter('Rename', array('target' => $uploaded_dir . DIRECTORY_SEPARATOR . $new_name));
                $r         = $upload->receive(array($name_file_k));

                if ($r) {
                    $data_file['file_name'] = $new_name;
                }
                else {
                    $messages = $upload->getMessages();
                    foreach ($messages as $msg) {
                        throw new Exception(" ERROR: " . $msg . " !!");
                    }
                }
                $data = array(
                    'name' => $new_name,
                    'staff_id' => $staff_id,
                    'created_by' => $userStorage->id,
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $QStaffUploadFile->insert($data);
            }
        }
    }
    //END UPLOAD HÌNH ẢNH

    $db->commit();
    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
    echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
    echo '<script>window.parent.document.getElementById("save").style.display = \'none\';</script>';
    echo '<div class="alert alert-success">Done</div>';

    /// load lại trang
    $back_url = "upload-file?staff_id=" . $staff_id;
    echo '<script>setTimeout(function(){parent.location.href="' . $back_url . '"}, 1000)</script>';
} catch (Exception $e) {
    $db->rollBack();
    echo '<script>window.parent.document.getElementById("iframe").style.display = \'block\';</script>';
    echo '<script>window.parent.document.getElementById("iframe").height = \'60px\';</script>';
    echo '<div class="alert alert-error">Lỗi : ' . $e->getMessage() . '</div>';
}
exit;
