<?php
class Application_Model_Imei extends Zend_Db_Table_Abstract
{
    protected $_name = 'imei';

    public function getImeiInfo($imei){

    	$db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => WAREHOUSE_DB.'.imei'),array('p.*'))
            ->where('p.imei_sn = ?', $imei);
        $result = $db->fetchRow($select);
        
        return $result;

    }
}
