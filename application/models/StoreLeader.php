<?php
class Application_Model_StoreLeader extends Zend_Db_Table_Abstract
{
    protected $_name = 'store_leader';

    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('p' => $this->_name),
                    array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        if (isset($params['staff_id']) && $params['staff_id']) {
            $select->where('p.staff_id = ?', $params['staff_id']);
        }

        if ($limit) {
            $select->limitPage($page, $limit);
        }

        $result = $db->fetchAll($select);

        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    function fetchAllLeader($staff_id){
        $db = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('p' => $this->_name),
                    array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*', 'pid' => 'p.id', ));

        $select
            ->distinct()
            ->join(array('s' => 'store'), 's.id=p.store_id', array('store_id' => 's.id', 'store_name' => 's.name','company_address'))
            ->join(array('r' => 'regional_market'), 'r.id=s.regional_market', array('region_id' => 'r.id', 'region_name' => 'r.name'))
            ->join(array('a' => 'area'), 'a.id=r.area_id', array('area_id' => 'a.id', 'area_name' => 'a.name'))
            ->join(array('lg' => 'store_leader_log'), 'lg.parent=p.id', array('log_id' => 'lg.id', 'joined_at' => 'lg.joined_at'));

        if (isset($staff_id) && $staff_id) {
            $select->where('p.staff_id = ?', $staff_id);
        }
         if(!empty($_GET['dev'])){
     echo $select->__toString();
     exit;
 }
        $result = $db->fetchAll($select);
        return $result;
    }

    function countStore($staff_id, $status)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('p' => $this->_name), array('total' => 'COUNT(DISTINCT p.store_id)'));

        $select->where('p.staff_id = ?', $staff_id);
        $select->where('p.status = ?', $status);
        
        $total = $db->fetchOne($select);
        return $total;
    }

    /**
     * @param $leaderId
     * @return array
     */
    public function getSubordinate($leaderId)
    {
        $data = $this->fetchAll(
            $this->select()
                ->distinct()
                ->from(['l' => $this], [])
                ->join(['s' => 'store_staff_log'], "l.store_id = s.store_id and s.is_leader = 1 and s.released_at is null and l.staff_id = $leaderId", ['s.staff_id'])
        )->toArray();

        $result = [];
        foreach ($data as $datum) {
            $result[] = [
                'staff_id' => $datum['staff_id'],
                'title' => 'Sale'
            ];
        }
        return $result;
    }

    /**
     * @param $asm_staff_id
     * @return array
     */
    public function getSuperior($asm_staff_id)
    {
        $data = $this->fetchAll(
            $this->select()
                ->from(['rsm' => $this], ['rsm.staff_id'])
                ->join(['s' => 'staff'], "rsm.staff_id = s.id and s.title = 308", [])
                ->join($this, "rsm.area_id = asm.area_id and rsm.staff_id != asm.staff_id and asm.staff_id = $asm_staff_id", [])
        )->toArray();
        $result = [];
        foreach ($data as $datum) {
            $result[] = [
                'staff_id' => $datum['staff_id'],
                'title' => 'RSM'
            ];
        }
        return $result;
    }
}