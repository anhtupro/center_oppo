<?php
class Application_Model_HrCourseStaff extends Zend_Db_Table_Abstract
{
    protected $_name = 'hr_course_staff';

    public function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');
        $select = $db->select();

        $cols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            'full_name' => 'CONCAT(s.firstname," ",s.lastname)',
            'staff_code' => 's.code',
            'department' => 's.department',
            'staff_id'   => 's.id',
            'course_num' => 'COUNT(p.course_id)'
        );
        $select->from(array('p'=> $this->_name), $cols);
        $select->joinLeft(array('s' => 'staff'), 's.id = p.staff_id', array());
        $select->joinLeft(array('t' => 'team'), 's.title = t.id', array('position' => 't.name'));
        $select->joinLeft(array('c' => 'hr_course'), 'c.id = p.course_id', array());
        $select->where('s.off_date IS NULL');
        $select->where('s.joined_at IS NOT NULL');
        $select->where('p.del = ?', 0);
        $select->where('c.del = ?', 0);
        if (isset($params['department_id']) and $params['department_id']) {
                $select->where('s.department IN (?)', $params['department_id']);
        }

        if (isset($params['staff_name']) and $params['staff_name']) {
            $select->where('CONCAT(s.firstname, " ",s.lastname) LIKE ?', '%' . $params['staff_name'] . '%');
        }

        if (isset($params['staff_code']) and $params['staff_code']){
            $select->where('s.code IN (?)', $params['staff_code']);
        }
        $select->group('p.staff_id');
        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public function getCurrentCourse(){
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $cols = array(
            'staff_id'   => 'p.staff_id',
            'num'        => 'COUNT(p.course_id)'
        );
        $select->from(array('p'=> $this->_name), $cols);
        $select->joinLeft(array('c' => 'hr_course'), 'c.id = p.course_id', array());
        $select->where('p.del = ?', 0);
        $select->where('c.del = ?', 0);
        $select->where('YEAR(c.from_date) >= YEAR(CURRENT_DATE())');
        $select->where('YEAR(c.to_date)   >= YEAR(CURRENT_DATE())');
        $select->group('p.staff_id');
        $data = $db->fetchAll($select);
        $result = array();
        foreach ($data as $value){
            $result[$value['staff_id']] = $value['num'];
        }
        return $result;
    }
}