<?php
class Application_Model_CampaignContractor extends Zend_Db_Table_Abstract
{
    protected $_name = 'campaign_contractor';

    public function getData($params){
    	$db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'id', 
            'campaign_id', 
            'area_id',
            'category_id',
            'quantity',
            'price',
            'status'
        );

        $select->from(array('p'=> 'campaign_contractor'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('c' => DATABASE_TRADE.'.app_checkshop'), 'c.store_id = s.id',array());
        $select->where('p.released_at IS NULL ', NULL);

        if(!empty($params['staff_id'])){
        	$select->where('p.staff_id = ?', $params['staff_id']);
        }

        $select->group('s.id');

		$select->limitPage($page, $limit);

		$result = $db->fetchAll($select);

		$total = $db->fetchOne("select FOUND_ROWS()");

		return $result;
    }

}