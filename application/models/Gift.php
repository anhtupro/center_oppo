<?php
class Application_Model_Gift extends Zend_Db_Table_Abstract
{
    protected $_name = 'gift';

    public function fetchPagination($page, $limit, &$total, $params){
        $db         = Zend_Registry::get("db");
        $select     = $db->select();
        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            "p.*", 'fullname' => "CONCAT(s.firstname, ' ', s.lastname)"
        );
        $select->from(array('p' => $this->_name), $arrCols);
        $select->joinLeft(array('s' => 'staff'), 's.id = p.created_by', array());
        $select->where('p.del = ?', 0);
        $select->order('p.created_at DESC');
        if(empty($params['export'])){
            $select->limitPage($page, $limit);
        }
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }
}