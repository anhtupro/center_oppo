<?php

class Application_Model_AssignAreaRecheckShop extends Zend_Db_Table_Abstract
{
    protected $_name = 'assign_area_recheck_shop';
    protected $_schema = DATABASE_TRADE;

    public function getAll($roundId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['a' => DATABASE_TRADE.'.assign_area_recheck_shop'], [
                         'a.staff_id',
//                         'a.area_id',
                         'string_area_id' => "GROUP_CONCAT(DISTINCT a.area_id)",
                         'string_area_name' => "GROUP_CONCAT(DISTINCT e.name)",
                         'staff_name' => "CONCAT(st.firstname, ' ', st.lastname)",
                         'email' => 'st.email',
                         'title' => 't.name'
                     ])
                    ->joinLeft(['st' => 'staff'], 'a.staff_id = st.id', [])
                    ->joinLeft(['e' => 'area'], 'a.area_id = e.id', [])
                    ->joinLeft(['t' => 'team'], 'st.title = t.id', [])
                     ->where('a.recheck_shop_round_id = ?', $roundId)
                     ->group('a.staff_id')
                    ->order('st.title');

        $result = $db->fetchAll($select);

        foreach ($result as $key => $item) {
            $result[$key] ['list_area_id'] = explode(',', $item['string_area_id']);
            $result[$key] ['list_area_name'] = explode(',', $item['string_area_name']);

            $listAreaId =  $result[$key] ['list_area_id'];
            $listAreaName =  $result[$key] ['list_area_name'];

            foreach ( $listAreaId as $index => $value) {
                $result[$key] ['list_area'] [] = [
                    'area_id' => $value,
                    'area_name' => $listAreaName[$index]
                ];
            }
        }

        return $result;
    }

    public function getListAreaId($staffId, $roundId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['a' => DATABASE_TRADE.'.assign_area_recheck_shop'], [
                         'DISTINCT(a.area_id)'
                     ])
                    ->where('a.staff_id = ?', $staffId)
                    ->where('a.recheck_shop_round_id = ?', $roundId);

        $result = $db->fetchAll($select);

        foreach ($result as $area) {
            $listAreaId [] = $area['area_id'];
        }

        return $listAreaId;
    }
}