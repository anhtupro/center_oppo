<?php

class Application_Model_NewImeiKpiRealme extends Zend_Db_Table_Abstract{

	protected $name = 'imei_kpi';
    protected $_schema = HR_DB_RM;

    public function dbconnect(){
		return Zend_Registry::get('db');
	}

	public function Get_All($params){

		$db = Zend_Registry::get('db');

		$from_date = $params['from'];
		$to_date   = $params['to'];

		$sql =
				'SELECT 	
					g.`desc` name,
					os.name store,
					COUNT(ik.imei_sn) sellout,
					CASE s.id
						WHEN ik.pg_id THEN "PGPB" 
						WHEN ik.sale_id THEN "Sale"
						WHEN ik.store_leader_id THEN "Store Leader"
						WHEN ik.sales_brs_id THEN "Sale Leader Brandshop"
					END AS staff_title,
					CASE 
						WHEN s.id = tm.staff_id and (s.title=293 or s.title=182) THEN ik.kpi_pg 
						WHEN (s.id = tm.staff_id or s.id=ossl.staff_id) and s.title=183 THEN ik.kpi_sale
						WHEN ik.store_leader_id THEN ik.kpi_store_leader
						WHEN ik.sales_brs_id THEN ik.kpi_sales_brs
					END AS kpi,
					MIN(ik.timing_date) from_date,
					MAX(ik.timing_date) to_date
				FROM realme_hr.imei_kpi ik
				inner join realme_hr.timing_sale_realme tsm on ik.imei_sn=tsm.imei 
				inner join realme_hr.timing_realme tm on tm.id=tsm.timing_id 
				 JOIN hr.staff s ON tm.created_by=s.id
				JOIN realme_warehouse.good g ON ik.good_id = g.id
				
				join realme_hr.store_mapping rmsm on ik.store_id= rmsm.id_rm
				
				JOIN hr.store os on os.id = rmsm.id_oppo
				left JOIN hr.store_staff_log ossl on ossl.store_id = os.id and ossl.is_leader=1 
				AND :to_date >= FROM_UNIXTIME(ossl.joined_at,"%Y-%m-%d") 
				AND (ossl.released_at IS NULL OR :from_date < FROM_UNIXTIME(ossl.released_at,"%Y-%m-%d"))

				WHERE ik.timing_date BETWEEN :from_date AND :to_date AND s.id = :staff_id
				GROUP BY g.id,kpi,staff_title,ik.store_id
				ORDER BY g.name,from_date,sellout';

		$stmt = $db->prepare($sql);
		$stmt->bindParam('staff_id', intval($params['staff_id']), PDO::PARAM_INT);
		$stmt->bindParam('from_date', $from_date, PDO::PARAM_STR);
		$stmt->bindParam('to_date', $to_date, PDO::PARAM_STR);


		$stmt->execute();
		$data = $stmt->fetchAll();
		$db   = $stmt = NULL;
		// echo "<pre>";print_r($data);die;
		return $data;

	}

	public function GetAll($params = array()){
		$params = array_merge(
			array(
				'staff_id' => null,
				'from' => null,
				'to' => null
			),
			$params
		);
		
		$db  = $this->dbconnect();
 		$stmt = $db->prepare('CALL SP_imei_kpi_GetAll(:staff_id, :from, :to, @total_pg, @total_sale, @total_pb_sale)');
		$stmt->bindParam('staff_id', $params['staff_id'], PDO::PARAM_INT);
		$stmt->bindParam('from', $params['from'], PDO::PARAM_STR);
		$stmt->bindParam('to', $params['to'], PDO::PARAM_STR);
		$stmt->execute();
		$res['data'] = $stmt->fetchAll();
		$stmt->closeCursor();

		$pg = $db->query('SELECT @total_pg AS total')->fetch();
		$sale = $db->query('SELECT @total_sale AS total')->fetch();
		$pb_sale = $db->query('SELECT @total_pb_sale AS total')->fetch();
		$res['total_pg_level'] = $pg['total'];
		$res['total_sale_level'] = $sale['total'];
		$res['total_pb_sale_level'] = $pb_sale['total'];
		$stmt = $db->prepare('DROP TEMPORARY TABLE IF EXISTS _temp');
		$stmt->execute();
		$stmt->closeCursor();

		$db = $stmt = null;
		return $res;
	}

	public function checkTimeOnShop($dataCheck = array()){
		
		$dataCheck = array_merge(
			array(
				'store_id'	=> null,
				'date'		=> null,
				'is_leader'	=> null,
			),
			$dataCheck
		);
		$db  = $this->dbconnect();
		// var_dump($dataCheck);die;
		$stmt = $db->prepare('CALL sp_check_time_on_shop(:p_date, :p_store, :p_is_leader)');
		$stmt->bindParam('p_date', $dataCheck['date'], PDO::PARAM_INT);
		$stmt->bindParam('p_store', $dataCheck['store_id'], PDO::PARAM_INT);
		$stmt->bindParam('p_is_leader', $dataCheck['is_leader'], PDO::PARAM_INT);
		$stmt->execute();
		$res = $stmt->fetch();

		$stmt->closeCursor();

		$db = $stmt = null;
		return $res;

	}

}