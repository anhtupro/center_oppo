<?php
class Application_Model_TrainerTypeAsset extends Zend_Db_Table_Abstract
{
    protected $_name = 'trainer_type_asset';

    function fetchPagination($page, $limit, &$total, $params){

        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        if (isset($params['name']) and $params['name'])
        {
            $select->where('p.name LIKE ?', '%'.$params['name'].'%');
        }

        if (isset($params['type']) and $params['type'])
        {
            $select->where('p.type = ?',$params['type']);
        }

        $select->where('del = ? OR del IS NULL',0);

        $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if ($result === false) {

            $data = $this->fetchAll(null, 'name');

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->name;
                }
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }

    function getAsset($params){
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('p'=>$this->_name),array('p.id','p.name'));
        if(isset($params['type']) AND $params['type']){
            $select->where('p.type = ?',$params['type']);
        }

        $result = $db->fetchPairs($select);
        return $result;

    }
}