<?php

class Application_Model_CategoryInventoryTypeAssign extends Zend_Db_Table_Abstract
{
    protected $_name = 'category_inventory_type_assign';
    protected $_schema = DATABASE_TRADE;

    public function getType($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['a' => DATABASE_TRADE.'.category_inventory_type_assign'], [
                         'type_id' => 't.id',
                         'type_name' => 't.name'
                     ])
                    ->joinLeft(['t' => DATABASE_TRADE.'.category_inventory_type'], 'a.type = t.id', [])
                    ->where('a.category_id = ?', $params['category_id']);

        $result = $db->fetchAll($select);

        return $result;
    }
}    