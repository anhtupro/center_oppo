<?php

class Application_Model_RequestPRPaymentOffice extends Zend_Db_Table_Abstract
{
    protected $_name = 'request_pr_payment_office';
    function getPayment(){
		$QTeam = new Application_Model_Team();
		$department = $QTeam->get_list_department();

		$listPaymentDetail=$this->fetchAll();
		$arr_temp=array();

		foreach ($listPaymentDetail as $key => $value) {
			# code...
			if(!empty($arr_temp[$value["request_id"]])){
				foreach ($arr_temp as $k => $v) {
					# code...
					$arr_temp[$value["request_id"]]["department"]=$v["department_id"]." ".$department[$value["department_id"]];
					$arr_temp[$value["request_id"]]["cost"]=$v["cost"]." ".$value["cost"];
					$arr_temp[$value["request_id"]]["amount"]=$v["amount"]." ".$value["amount"];
				}
			}else{
				$arr_temp[$value["request_id"]]=array("department"=>$department[$value["department_id"]],"cost"=>$value["cost"],"amount"=>$value["amount"]);
			}
		}
// 				echo "<pre>";
// print_r($arr_temp);
// echo "</pre>";
		return $arr_temp;

    }
}
