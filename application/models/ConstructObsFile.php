<?php

class Application_Model_ConstructObsFile extends Zend_Db_Table_Abstract
{
    protected $_name = 'construct_obs_file';
    protected $_schema = DATABASE_TRADE;

    public function get($construct_obs_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['o' => DATABASE_TRADE .'.construct_obs'], [
                'f.*'
            ])
            ->join(['f' => DATABASE_TRADE . '.construct_obs_file'], 'o.id = f.construct_obs_id', [])
            ->where('o.id = ?', $construct_obs_id);
        
        $result = $db->fetchAll($select);

        $list = [];
        foreach ($result as $item) {
            $list [$item['type']] [] = $item;
        }

        return $list;
    }
}