<?php
class Application_Model_PurchasingRequestSupplier extends Zend_Db_Table_Abstract
{
    protected $_name = 'purchasing_request_supplier';

    protected $_schema = DATABASE_SALARY;

    function getListSupplier($params){

        $db = Zend_Registry::get('db');

        $col = array(
                "p.id",
                "p.*"
                );

        $select = $db->select()->from(array('p' => DATABASE_SALARY.'.purchasing_request_supplier'), $col);
        $select->joinleft(array('pr'=>'purchasing_request'), 'pr.id = p.pr_id', array('ids' => 'pr.id'));
        $select->joinleft(array('s'=>'supplier'), 'p.supplier_id = s.id', array('supplier_name' => 's.title'));

        if(!empty($params['sn'])){
            $select->where('pr.sn = ?', $params['sn']);
        }
        
        $result = $db->fetchAll($select);
        return $result;
    }
    

    public function getListSupplierReview($sn)
    {
        $db = Zend_Registry::get('db');
        $data = array();

        if(!empty($sn)){

            $sql = "SELECT pr.sn, pr.`name`, prs.id as prs_id, prs.* ,s.title as supplier_name 
                    FROM ".DATABASE_SALARY.".`purchasing_request_supplier` prs
                    LEFT JOIN purchasing_request pr ON prs.pr_id = pr.id
                    LEFT JOIN supplier s ON prs.supplier_id = s.id
                    WHERE prs.select_supplier = 1 AND pr.sn = '".$sn."' "
                    . "GROUP BY prs.supplier_id";

            $stmt = $db->prepare($sql);
            $stmt->execute();
            $data = $stmt->fetchAll();
            $stmt->closeCursor();
            $stmt = $db = null;
        }

        return $data;
    }
    
    
    public function getListHistoryReview($supplier_id, $sn)
    {
        $db = Zend_Registry::get('db');
        $data = array();

        if(!empty($supplier_id) and !empty($sn)){

            $sql = "SELECT prs.id as prs_id, s.title as supplier_name, pr.`name` as name_pr, prs.* , CONCAT(st.firstname,' ',st.lastname) AS review_by_name
                    FROM ".DATABASE_SALARY.".`purchasing_request_supplier` prs
                    LEFT JOIN purchasing_request pr ON prs.pr_id = pr.id
                    LEFT JOIN supplier s ON prs.supplier_id = s.id
                    LEFT JOIN staff st ON st.id = prs.review_by
                    WHERE pr.del = 0 AND pr.`status` = 3 AND prs.review_by is not NULL
                    AND prs.select_supplier = 1 AND s.id = ".$supplier_id." AND pr.sn <> '".$sn."'
                    ORDER BY prs.review_at DESC
                    LIMIT 10";

            $stmt = $db->prepare($sql);
            $stmt->execute();
            $data = $stmt->fetchAll();
            $stmt->closeCursor();
            $stmt = $db = null;
        }

        return $data;
    }
    
}



