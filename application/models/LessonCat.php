<?php
class Application_Model_LessonCat extends Zend_Db_Table_Abstract
{
    protected $_name = 'lesson_cat';

    function fetchPagination($page, $limit, &$total, $params) {
        $db = Zend_Registry::get('db');

         $select = $db->select()
        ->from(array('p' => $this->_name),
            array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),'title'=>'p.title','parent_id'=>'p.parent_id','stt'=>'p.stt','created_at'=>'p.created_at','title_cat'=>'o.title'))
        ->joinLeft(array('o' => 'lesson_cat'), 'p.parent_id = o.id', array())
        ->order('o.id DESC');
        
        if ($limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }


    function get_cache(){
        $cache  = Zend_Registry::get('cache');
        $result = $cache->load($this->_name.'_cache');

        if ($result === false) {

            $data = $this->fetchAll();

            $result = array();
            if ($data){
                foreach ($data as $key => $value) {
                    $result[ $value['id'] ] = $value['title'];
                }
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }
    
    public function getCat(){
        $db     = Zend_Registry::get("db");
        $select = $db->select();
         $select_main = $db->select();   
        $arrCols = array(
            'id'               => 'l.id',
            'title'         =>'l.title',
            'parent_id'         =>'l.parent_id'
        );
        $select->from(array('l'=> 'lesson_cat'), $arrCols);
        $select ->where('l.parent_id = ?',0);
        $result  = $db->fetchAll($select);
        return $result;
    }
    
    public function getCatArr($id){
        $db     = Zend_Registry::get("db");
        $select = $db->select();
         $select_main = $db->select();   
        $arrCols = array(
            'id'               => 'l.id',
            'title'         =>'l.title',
            'parent_id'         =>'l.parent_id',
            'new'               =>'l.new'
        );
        $select->from(array('l'=> 'lesson_cat'), $arrCols);
        $select ->where('l.parent_id = ?',$id);
        $select ->where('l.del = ?',0);
        $select ->order('l.id DESC');        
        $result  = $db->fetchAll($select);
        return $result;
    }
    
    function fetchPagination_2($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('p'=> $this->_name),array(
                new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
                'category' => 'p.title', 
                'p.created_at', 
                'p.updated_at',
                'p.id',
                'p.del'
            ))
            ->where('p.parent_id = ?', 1);
        if(isset($params['sort']) && $params['sort']) {
            $desc = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';            
            $order_str = 'p.`'.$params['sort']. '` ' . $desc;
            $select->order(new Zend_Db_Expr($order_str));
        }else {
            $select->order('p.title DESC');     
        }            
        if(isset($params['title']) and $params['title'])
            $select->where('p.title LIKE ?','%'.$params['title'].'%');
        if ($limit)
            $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }    
}

