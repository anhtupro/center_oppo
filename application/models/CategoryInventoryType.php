<?php

class Application_Model_CategoryInventoryType extends Zend_Db_Table_Abstract
{
    protected $_name = 'category_inventory_type';
    protected $_schema = DATABASE_TRADE;

    public function getType($params = null)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['a' => DATABASE_TRADE.'.category_inventory_type_assign'], [
                        'type_id' => 't.id',
                         'type_name' => 't.name',
                         'category_id' => 'a.category_id'
                     ])
        ->joinLeft(['t' => DATABASE_TRADE.'.category_inventory_type'], 'a.type = t.id', []);

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $listType [$element['category_id']] [] = $element;
        }
        return $listType;
    }
}    