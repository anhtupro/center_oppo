<?php
class Application_Model_HeadOfDepartment extends Zend_Db_Table_Abstract
{
    protected $_name = 'head_of_department';
    protected $_primary = 'hod_department_id';

    public function getHeadIdByDepartment($department)
    {
        $hod = $this->fetchRow($this->getAdapter()->quoteInto('hod_department_id = ?', $department));
        if($hod) {
            return intval($hod['hod_staff_id']);
        }
        return null;
    }
}