<?php
class Application_Model_TrainerInventoryAsset extends Zend_Db_Table_Abstract
{
    protected $_name = 'trainer_inventory_asset';

    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');
        $cols = array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.asset_id'),
                'p.*',
                'a.type'
        );
        $select = $db->select()
            ->from(array('p' => 'v_trainer_inventory'),$cols)
            ->join(array('a'=>'trainer_type_asset'),'p.asset_id = a.id',array())
        ;

        if (isset($params['asset_id']) and $params['asset_id'])
            $select->where('p.asset_id = ?', $params['asset_id']);


        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    function getInventoryImei($params){
        $db = Zend_Registry::get('db');
        $cols  = array(
            'asset_name' => 'a.name',
            'asset_id' => 'a.id',
            'b.imei',
            'b.trainer_id',
            'imei_note'=>'b.note',
            'staff_name' => 'CONCAT(c.firstname," ", c.lastname) ',
        );
        $select = $db->select()
            ->from(array('a'=>'trainer_type_asset'),$cols)
            ->join(array('b'=>'v_trainer_inventory_imei'),'a.id = b.asset_id',array())
            ->joinLeft(array('c'=>'staff'),'c.id = b.trainer_id',array())
            ->order('b.trainer_id')
        ;

        if(isset($params['asset_id']) AND $params['asset_id']){
            $select->where('a.id = ?',$params['asset_id']);
        }

        if(isset($params['imei']) AND $params['imei']){
            $select->where('b.imei = ?',$params['imei']);
        }

        if(isset($params['available_stock'])){
            if($params['available_stock'] == 0){
                $select->where('b.trainer_id = ?',0);
            }elseif($params['available_stock'] == 1){
                $select->where('b.trainer_id > ?',0);
            }else{
                $select->where('b.trainer_id >= ?',0);
            }
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    function fetchInventoryByUser($staff_id,$confirm =1){
        $db = Zend_Registry::get('db');
        $stmt = $db->query('CALL p_trainer_inventory_by_user(?,?)',array($staff_id,$confirm));
        $result = $stmt->fetchAll();
        return $result;
    }

    public function  fetchPaginationByUser($page, $limit, &$total, $params){
        $db  = Zend_Registry::get('db');
        $stmt = $db->query('CALL p_trainer_list_inventory_by_staff');
        $result = $stmt->fetchAll();
        return $result;
    }

    public function fetchNotConfirm($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');
        $cols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.id'),
            'staff_name' => 'CONCAT(c.firstname," ",c.lastname)',
            'd.sn',
            'asset_name' => 'b.name',
            'a.quantity',
            'd.date'
        );
        $select = $db->select()
            ->from(array('a'=>'trainer_order_out_detail'),$cols)
            ->join(array('b'=>'trainer_type_asset'),'a.asset_id = b.id',array())
            ->join(array('c'=>'staff'),'c.id = a.output_to_staff_id',array())
            ->join(array('d'=>'trainer_order_out'),'d.id = a.order_out_id',array())
            ->where('a.trainer_confirm = ?',0);

        if($limit){
            $select->limitPage($page,$limit);
        }

        if(isset($params['sort']) AND $params['sort']){
            $order_by_str= (isset($params['desc']) == 1) ? 'DESC' : 'ASC';
            $order_str = $params['sort'].' '.$order_by_str;
            $select->order($order_str);
        }
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public function getStatusByImei(){
        $sql = '
            SELECT a.imei, a.note, c.`code` as staff_code,
            CASE WHEN c.id IS NOT NULL THEN CONCAT(c.firstname," ",c.lastname) ELSE "KHO" END as staff_name,
            d.`name` as asset_name
            FROM trainer_order_imei a
            INNER JOIN (
                SELECT MAX(id) id, imei FROM trainer_order_imei
                GROUP BY imei
            ) b ON a.id = b.id
            LEFT JOIN staff c ON c.id = a.trainer_id
            INNER JOIN trainer_type_asset d ON d.id = a.asset_id
            ORDER BY d.id
        ';
        $db = Zend_Registry::get('db');
        $stmt = $db->query($sql);
        $result = $stmt->fetchAll();
        return $result;
    }
}
