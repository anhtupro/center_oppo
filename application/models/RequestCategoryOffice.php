<?php
class Application_Model_RequestCategoryOffice extends Zend_Db_Table_Abstract
{
    protected $_name = 'request_category_office';
    public function getCategory()
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id',
            'name'        => 'p.name'
        );
        $select->from(array('p'=> 'request_category_office'), $arrCols);
        
        if($userStorage->department == FINANCE_DEPARTMENT_ID){
            $select->where('p.id IN (?)', [1,2,3,7,8,9]);
        }
        else{
            $select->where('p.id IN (?)', [1,2,3,7]);
        }
        
        $result = $db->fetchAll($select);
        return $result;
    }
}