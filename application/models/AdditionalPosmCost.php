<?php

class Application_Model_AdditionalPosmCost extends Zend_Db_Table_Abstract
{
    protected $_name = 'additional_posm_cost';
    protected $_schema = DATABASE_TRADE;

    public function getCost($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['c' => DATABASE_TRADE.'.category_additional'], [
                         'p.category_id_additional',
                         'p.area_id',
                         'p.cost'
                     ])
        ->joinLeft(['p' => DATABASE_TRADE.'.additional_posm_cost'], 'c.id = p.category_id_additional', [])
        ->where('p.is_deleted = 0')
        ->where('c.is_deleted = 0');

        if ($params['season']) {
            $select->where('p.season = ?', $params['season']);
        }

        if ($params['year']) {
            $select->where('p.year = ?', $params['year']);
        }

        if ($params['from_date']) {
            $select->where('p.date >= ?', $params['from_date']);
        }

        if ($params['to_date']) {
            $select->where('p.date <= ?', $params['to_date']);
        }

        $result = $db->fetchAll($select);
        
        foreach ($result as $element) {
            $additionalPosmCost [$element['area_id']] [$element['category_id_additional']] = $element;
        }

        return $additionalPosmCost;
    }

    public function getTotal($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['c' => DATABASE_TRADE.'.category_additional'], [
                'SUM(p.cost)'
            ])
            ->joinLeft(['p' => DATABASE_TRADE.'.additional_posm_cost'], 'c.id = p.category_id_additional', [])
            ->where('p.is_deleted = 0')
            ->where('c.is_deleted = 0');

        if ($params['season']) {
            $select->where('p.season = ?', $params['season']);
        }

        if ($params['year']) {
            $select->where('p.year = ?', $params['year']);
        }

        if ($params['from_date']) {
            $select->where('p.date >= ?', $params['from_date']);
        }

        if ($params['to_date']) {
            $select->where('p.date <= ?', $params['to_date']);
        }

        $result = $db->fetchOne($select);

        return $result;
    }
}