<?php

class Application_Model_ChangePictureDetail extends Zend_Db_Table_Abstract
{
    protected $_name = 'change_picture_detail';
    protected $_schema = DATABASE_TRADE;


    public function getAllStoreEachArea($params, $not_finish = null)
    {
        $db = Zend_Registry::get("db");
        $sub_select = $db->select()
            ->from(['a' => DATABASE_TRADE.'.change_picture_detail'], [
                'store_id' => new Zend_Db_Expr('DISTINCT a.store_id'),
                'area_id' => 'r.area_id',
                'area_name' => 'e.name',
            ])
            ->joinLeft(['s' => 'store'], 'a.store_id = s.id', [])
            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'di.parent = r.id', [])
            ->joinLeft(['e' => 'area'], 'r.area_id = e.id', [])

            ->where('a.stage_id = ?', $params['stage_id'])
            ->where('a.product_id > 0')
            ->where('r.area_id IN (?)', $params['area_list']);

            if ($not_finish) {
                $sub_select->where('a.has_changed_picture = ?', 0);
            }

        $select = $db->select()
            ->from(['g' => $sub_select], [
                'g.area_id',
                'g.area_name',
                'count_store' => 'count(g.store_id)',
                'list_store' => "GROUP_CONCAT(g.store_id)"
            ])
            ->group('g.area_id');
//echo $select;;die;
        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['area_id']] = $element;
        }

        return $list;
    }



    public function getAllStore($params, $not_finish = null)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE.'.change_picture_detail'], [
                'count_store' => new Zend_Db_Expr('COUNT(DISTINCT a.store_id)'),
            ])
            ->joinLeft(['s' => 'store'], 'a.store_id = s.id', [])
            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'di.parent = r.id', [])
            ->joinLeft(['e' => 'area'], 'r.area_id = e.id', [])

            ->where('a.stage_id = ?', $params['stage_id'])
            ->where('a.product_id > 0')
            ->where('r.area_id IN (?)', $params['area_list']);

        if ($not_finish) {
            $select->where('a.has_changed_picture = ?', 0);
        }

        $result = $db->fetchOne($select);

        return $result;
    }


    public function getAllStoreSale($params, $not_finish = null)
    {
        $db = Zend_Registry::get("db");
        $sub_select = $db->select()
            ->from(['a' => DATABASE_TRADE.'.change_picture_detail'], [
                'store_id' => new Zend_Db_Expr('DISTINCT a.store_id'),
                'area_id' => 'r.area_id',
                'area_name' => 'e.name',
                'sale_id' => 'f.id',
                'sale_name' => "CONCAT(f.firstname, ' ', f.lastname)"
            ])
            ->joinLeft(['s' => 'store'], 'a.store_id = s.id', [])
            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'di.parent = r.id', [])
            ->joinLeft(['e' => 'area'], 'r.area_id = e.id', [])
            ->joinLeft(['l' => 'store_staff_log'], 'l.store_id = s.id AND l.released_at IS NULL AND l.is_leader = 1', [])
            ->joinLeft(['q' => 'store_leader_log'], 'q.store_id = s.id AND q.released_at IS NULL', [])
//            ->joinLeft(['l' => 'v_store_staff_leader_log'], 'l.store_id = s.id AND l.released_at IS NULL AND l.is_leader = 1', [])
            ->joinLeft(['f' => 'staff'], 'IF(l.staff_id IS NULL, q.staff_id, l.staff_id) = f.id', [])

            ->where('a.stage_id = ?', $params['stage_id'])
            ->where('a.product_id > 0')
            ->where('e.id = ?', $params['area_id']);


        if ($not_finish) {
            $sub_select->where('a.has_changed_picture = ?', 0);
        }


        $select = $db->select()
            ->from(['g' => $sub_select], [
                'g.area_id',
                'g.area_name',
                'count_store' => 'count(g.store_id)',
                'sale_id' => 'g.sale_id',
                'sale_name' => 'g.sale_name'
            ])
            ->group('g.sale_id');

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['sale_id']] = $element;
        }

        return $list;
    }

    public function getStatisticCategory($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE.'.change_picture_detail'], [
                'area_id' => 'e.id',
                'area_name' => 'e.name',
                'category_id' => 'c.id',
                'category_name' => 'c.name',
                'total_quantity' => "COUNT(a.category_id)",
                 'total_real_quantity' => "SUM( IF(a.has_changed_picture = 1, 1, 0) )"
            ])
            ->joinLeft(['s' => 'store'], 'a.store_id = s.id', [])
            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'di.parent = r.id', [])
            ->joinLeft(['e' => 'area'], 'r.area_id = e.id', [])
            ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'a.category_id = c.id', [])

            ->where('a.stage_id = ?', $params['stage_id'])
            ->where('a.product_id > 0')
            ->where('e.id IN (?)', $params['area_list'])
            ->group(['e.id','a.category_id']);

        if ($params['area_id']) {
            $select->where('e.id = ?', $params['area_id']);
        }

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['area_id']] [$element['category_id']] = $element;
        }

        return $list;
    }
    

    public function getStatisticCategoryAll($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE.'.change_picture_detail'], [
                'category_id' => 'c.id',
                'category_name' => 'c.name',
                'total_quantity' => "count(a.category_id)",
                'total_real_quantity' => "SUM( IF(a.has_changed_picture = 1, 1, 0) )"
            ])
            ->joinLeft(['s' => 'store'], 'a.store_id = s.id', [])
            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'di.parent = r.id', [])
            ->joinLeft(['e' => 'area'], 'r.area_id = e.id', [])
            ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'a.category_id = c.id', [])

            ->where('a.stage_id = ?', $params['stage_id'])
            ->where('a.product_id > 0')
            ->where('e.id IN (?)', $params['area_list'])
            ->group(['a.category_id']);

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['category_id']] = $element;
        }

        return $list;
    }

    public function getStatisticCategorySale($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE.'.change_picture_detail'], [
                'area_id' => 'e.id',
                'area_name' => 'e.name',
                'category_id' => 'c.id',
                'category_name' => 'c.name',
                'total_quantity' => "COUNT(a.category_id)",
                'total_real_quantity' => "SUM(IF(a.has_changed_picture = 1, 1, 0))",
                'percent_finish' => "SUM(IF(a.has_changed_picture = 1, 1, 0)) * 100 / COUNT(a.category_id)",
                'sale_id' => 'f.id',
                'sale_name' => "CONCAT(f.firstname, ' ', f.lastname)"
            ])
            ->joinLeft(['s' => 'store'], 'a.store_id = s.id', [])
            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'di.parent = r.id', [])
            ->joinLeft(['e' => 'area'], 'r.area_id = e.id', [])
            ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'a.category_id = c.id', [])
            ->joinLeft(['l' => 'store_staff_log'], 'l.store_id = s.id AND l.released_at IS NULL AND l.is_leader = 1', [])
            ->joinLeft(['q' => 'store_leader_log'], 'q.store_id = s.id AND q.released_at IS NULL', [])
//            ->joinLeft(['l' => 'v_store_staff_leader_log'], 'l.store_id = s.id AND l.released_at IS NULL AND l.is_leader = 1', [])
            ->joinLeft(['f' => 'staff'], 'IF(l.staff_id IS NULL, q.staff_id, l.staff_id) = f.id', [])

            ->where('a.stage_id = ?', $params['stage_id'])
            ->where('a.product_id > 0')
            ->where('e.id = ?', $params['area_id'])
            ->group(['f.id','a.category_id']);

        $result = $db->fetchAll($select);


        foreach ($result as $element) {
            $list [$element['sale_id']] ['sale_name']  = $element['sale_name'];
            $list [$element['sale_id']] ['detail'] [$element['category_id']]  = $element;
        }

        return $list;
    }

    public function getDataDetail($params)
    {


        $db = Zend_Registry::get("db");

        $arrCols = array(
            'store_id' => 's.id',
            'store_name' => 's.name',
            'area_name' => 'e.name',
            'province' => 'r.name',
            'district' => 'i.name',
            'category_id' => 'c.id',
            'category_name' => 'c.name',
            'height' => 'de.height',
            'width' => 'de.width',
            'painting_id' => 'p.id',
            'height_painting' => 'p.height',
            'width_painting' => 'p.width',
            'type' => 't.name',
            'type_child' => 't2.name',
            'material' => 'm.name',
            'material_painting' => 'm2.name',
            'key_visual' => 'v.name',
            'd.logo_type',
            'd.has_changed_picture',
            'sale_name' => "CONCAT(sale.firstname, ' ', sale.lastname)",
            'de.quantity',
            'd.note',
            'c.is_picture',
            'child_id' => 'd.app_checkshop_detail_child_id'
        ,

            'channel'           => "CASE WHEN (dl.loyalty_plan_id IS NOT NULL AND dis.is_ka = 0) THEN plan.name
                                    WHEN (dis.is_ka = 1) THEN 'KA'
                                    WHEN (dl.loyalty_plan_id IS NULL AND dis.is_ka = 0) THEN 'SHOP THƯỜNG'
                                    END",
        );

        $select = $db->select();
        $select->from(array('d'=> DATABASE_TRADE . '.change_picture_detail'), $arrCols)
        ->join(['s' => 'store'], 'd.store_id = s.id', [])
            ->joinLeft(['dis' => WAREHOUSE_DB . '.distributor'], 's.d_id = dis.id', [])
            ->joinLeft(['dl' =>  'dealer_loyalty'], 'dl.dealer_id= dis.id AND dl.is_last = 1', [])
            ->joinLeft(array('plan' => 'loyalty_plan'), 'plan.id = dl.loyalty_plan_id', array())

            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'di.parent = r.id', [])
        ->joinLeft(['e' => 'area'], 'r.area_id = e.id', [])
        ->joinLeft(['i' => 'regional_market'], 's.district = i.id', [])
            ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'c.id = d.category_id', [])
            ->joinLeft(['v' => DATABASE_TRADE.'.phone_product'], 'd.product_id = v.id', [])

            ->joinLeft(['de' => DATABASE_TRADE.'.app_checkshop_detail_child'], 'd.app_checkshop_detail_child_id = de.id', [])
            ->joinLeft(['p' => DATABASE_TRADE.'.app_checkshop_detail_child_painting'], 'd.child_painting_id = p.id', [])
            ->joinLeft(['t' => DATABASE_TRADE.'.category_inventory_type'], 'de.category_type = t.id', [])
            ->joinLeft(['t2' => DATABASE_TRADE.'.category_inventory_type'], 'de.category_type_child = t2.id', [])
            ->joinLeft(['m' => DATABASE_TRADE.'.category_material'], 'de.material_id = m.id', [])
            ->joinLeft(['m2' => DATABASE_TRADE.'.category_material'], 'p.material_id = m2.id', [])

            ->joinLeft(['l' => 'store_staff_log'], 'l.store_id = s.id AND l.released_at IS NULL AND l.is_leader = 1', [])
            ->joinLeft(['q' => 'store_leader_log'], 'q.store_id = s.id AND q.released_at IS NULL', [])
            ->joinLeft(['sale' => 'staff'], 'IF(l.staff_id IS NULL, q.staff_id, l.staff_id) = sale.id', [])

            ->where('d.stage_id = ?', $params['stage_id'])
//            ->where('de.quantity > ?', 0)
            ->where('d.product_id > 0');


        if(!empty($params['area_id'])){
            $select->where('r.area_id = ?', $params['area_id']);
        }

        if(!empty($params['area_list'])){
            $select->where('r.area_id IN (?)', $params['area_list']);
        }
//echo $select;die;
        $result = $db->fetchAll($select);

        return $result;
    }


    public function ExportDetail($params)
    {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $data = $this->getDataDetail($params);

        $heads = array(
            'Kênh',
            'Store ID',
            'Shop đăng ký ',
            'Khu vực',
            'Tỉnh',
            'Huyện',
            'Sale quản lý',
            'Hạng mục',
            'ID Hạng mục',
            'ID chi tiết tranh',
            'Số lượng',
            'Chiều ngang tranh (mm)',
            'Chiều cao tranh (mm)',
            'Chất liệu tranh',
            'Key visual',
            'Logo',
            'Ghi chú',
            'Trạng thái'
        );
        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $title)
        {
            $sheet->setCellValue($alpha.$index, $title);
            $alpha++;
        }


        $index    = 2;

        $i = 1;

        foreach($data as $item){

            $alpha    = 'A';
            $sheet->setCellValue($alpha++.$index, $item['channel']);
            $sheet->setCellValue($alpha++.$index, $item['store_id']);
            $sheet->setCellValue($alpha++.$index, $item['store_name']);
            $sheet->setCellValue($alpha++.$index, $item['area_name']);
            $sheet->setCellValue($alpha++.$index, $item['province']);
            $sheet->setCellValue($alpha++.$index, $item['district']);
            $sheet->setCellValue($alpha++.$index, $item['sale_name']);
            $sheet->setCellValue($alpha++.$index, $item['category_name']);
            $sheet->setCellValue($alpha++.$index, $item['child_id']);
            $sheet->setCellValue($alpha++.$index, $item['painting_id']);
            $sheet->setCellValue($alpha++.$index, $item['quantity']);

            if ($item['is_picture'] == 1) {
                $sheet->setCellValue($alpha++.$index, $item['width']);
                $sheet->setCellValue($alpha++.$index, $item['height']);
            } else {
                $sheet->setCellValue($alpha++.$index, $item['width_painting']);
                $sheet->setCellValue($alpha++.$index, $item['height_painting']);
            }

            $sheet->setCellValue($alpha++.$index, $item['material_painting'] ? $item['material_painting'] : $item['material']);


            $sheet->setCellValue($alpha++.$index, $item['key_visual']);
            $sheet->setCellValue($alpha++.$index, $item['logo_type'] == 1 ? 'Có' : 'Không' );
            $sheet->setCellValue($alpha++.$index, $item['change_picture_note']);
            $sheet->setCellValue($alpha++.$index, $item['has_changed_picture'] == 1 ? 'Đã thay' : 'Chưa thay');


            $index++;

        }

        $filename = 'Tiến độ Thay tranh' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }


    public function getListCategory($stage_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['d' => DATABASE_TRADE.'.change_picture_detail'], [
                         'id' => new Zend_Db_Expr('DISTINCT d.category_id'),
                         'name' => 'c.name'
                     ])
                    ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'd.category_id = c.id', [])
        ->where('d.stage_id = ?', $stage_id);

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getDataStoreFinish($params)
    {
        $db = Zend_Registry::get("db");

        $select_store_not_finish = $db->select()
            ->from(['a' => DATABASE_TRADE.'.change_picture_detail'], [
                'a.store_id'
            ])
            ->joinLeft(['s' => 'store'], 'a.store_id = s.id', [])
            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'di.parent = r.id', [])
            ->joinLeft(['e' => 'area'], 'r.area_id = e.id', [])
            ->where('a.stage_id = ?', $params['stage_id'])
            ->where('a.product_id > 0')
            ->where('e.id IN (?)', $params['area_list'])
            ->where('a.has_changed_picture = ?', 0)
            ->group(['a.store_id']);


        $select = $db->select()
            ->from(['a' => DATABASE_TRADE.'.change_picture_detail'], [
                'a.store_id',
                'store_name' => 's.name',
                'area' => 'e.name'
            ])
            ->joinLeft(['s' => 'store'], 'a.store_id = s.id', [])
            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'di.parent = r.id', [])
            ->joinLeft(['e' => 'area'], 'r.area_id = e.id', [])
            ->joinLeft(['n' => $select_store_not_finish], 'a.store_id = n.store_id', [])

            ->where('a.stage_id = ?', $params['stage_id'])
            ->where('a.product_id > 0')
            ->where('e.id IN (?)', $params['area_list'])
            ->where('n.store_id IS NULL')
            ->group(['a.store_id']);

        if ($params['area_id']) {
            $select->where('e.id = ?', $params['area_id']);
            $select_store_not_finish->where('e.id = ?', $params['area_id']);
        }

        $result = $db->fetchAll($select);

        return $result;
    }
    public function exportStoreFinish($params)
    {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $data = $this->getDataStoreFinish($params);

        $heads = array(
           'Store ID',
            'Store name',
            'Area'
        );
        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $title)
        {
            $sheet->setCellValue($alpha.$index, $title);
            $alpha++;
        }


        $index    = 2;

        $i = 1;

        foreach($data as $item){

            $alpha    = 'A';
            $sheet->setCellValue($alpha++.$index, $item['store_id']);
            $sheet->setCellValue($alpha++.$index, $item['store_name']);
            $sheet->setCellValue($alpha++.$index, $item['area']);
            $index++;

        }

        $filename = 'Store đã hoàn thành' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }


}