<?php
class Application_Model_AsmContract extends Zend_Db_Table_Abstract
{
	protected $_name = 'asm_contract';

	public function save($data,$id = NULL){
		if($id){
			$where = $this->getAdapter()->quoteInto('staff_id = ?',$id);
			$this->update($data,$where);
		}else{
			$this->insert($data);
		}
	}	

	public function refresh($asm_contract_id){

        $contract = $this->find($asm_contract_id)->current();
        if(!$contract){
            throw new Exception('Contract is not exist');
        }

        $QStaff = new Application_Model_Staff();
        $staff = $QStaff->find($contract->staff_id)->current();
        $data = array(
            'new_contract'    => $staff['contract_term'],
            'regional_market' => $staff['regional_market'],
            'from_date'       => ($staff['contract_signed_at']) ? $staff['contract_signed_at'] : NULL,
            'to_date'         => ($staff['contract_expired_at']) ? $staff['contract_expired_at'] : NULL ,
            'title'           => $staff['title'],

        );

        $where = $this->getAdapter()->quoteInto('id = ?',$asm_contract_id);
        $this->update($data,$where);

        /*
        $sql = '
            UPDATE asm_contract a
            INNER JOIN (
                SELECT a.staff_id, MAX(a.id) id, b.level 
                FROM asm_contract a
                LEFT JOIN contract_term b ON a.new_contract = b.id
                WHERE a.print_type = 1 AND locked = 0
                GROUP BY a.staff_id
                HAVING count(a.staff_id) = 1 AND b.`level` = 1
            ) as b ON a.staff_id = b.staff_id AND a.id = b.id
            INNER JOIN staff c ON b.staff_id = c.id
                AND ( 
                    a.regional_market != c.regional_market
                    OR a.title        != c.title
                    OR a.from_date    != c.contract_signed_at
                    OR a.to_date      != c.contract_expired_at
                ) 
            SET a.regional_market = c.regional_market
            ,a.title = c.title
            ,a.from_date = c.contract_signed_at
            ,a.to_date = c.contract_expired_at
            WHERE a.new_contract IN (2,3,4)
        ';
		$db = Zend_Registry::get('db');
		$stmt = $db->query($sql);
		$stmt->execute();
        */
	}

    public function fetchStaffContract($page,$limit, &$total, $params){
        $db = Zend_Registry::get('db');
        $expired_from       = (isset($params['expired_from']) OR $params['expired_from'] != '') ? My_Date::normal_to_mysql($params['expired_from']): NULL;
        $expired_to         = (isset($params['expired_to']) OR $params['expired_to'] != '') ? My_Date::normal_to_mysql($params['expired_to']): NULL;
        $signed_from        = (isset($params['signed_from']) AND $params['signed_from']) ? My_Date::normal_to_mysql($params['signed_from']) : NULL;
        $signed_to          = (isset($params['signed_to']) AND $params['signed_to']) ? My_Date::normal_to_mysql($params['signed_to']) : NULL;
        $return_letter_from = (isset($params['return_letter_from']) AND $params['return_letter_from'] ) ? My_Date::normal_to_mysql($params['return_letter_from']) : NULL;
        $return_letter_to   = ($params['return_letter_to']) ? My_Date::normal_to_mysql($params['return_letter_to']) : NULL;
        $send_letter_from   = ($params['send_letter_from']) ? My_Date::normal_to_mysql($params['send_letter_from']) : NULL;
        $send_letter_to     = ($params['send_letter_to']) ? My_Date::normal_to_mysql($params['send_letter_to']) : NULL;
        $strAreaId          = ( count($params['area_id']) > 0 ) ? implode(',', $params['area_id']) : '';
        $strDepartment      = ( count($params['department']) > 0 ) ? implode(',', $params['department']) : '';
        $strTeam            = ( count($params['team']) > 0 ) ? implode(',', $params['team']) : '';
        $strTitle           = ( count($params['title']) > 0 ) ? implode(',', $params['title']) : '';
        $strContractTerm    = ( count($params['contract_term']) > 0) ? implode(',', $params['contract_term']) : '';
        $strCompanyId       = ( count($params['company_id']) > 0 ) ? implode(',', $params['company_id']) : '';
        $userStorage        = Zend_Auth::getInstance()->getStorage()->read();
        $new_staff          = (isset($params['new_staff']) AND $params['new_staff'] == 1 ) ? 1 : 0;
        $transfer           = (isset($params['transfer']) AND $params['transfer'] == 1 ) ? 1 : 0;
        $re_view            = (isset($params['re_view']) AND $params['re_view'] == 1 ) ? 1 : 0;
        $re_active          = (isset($params['re_active']) AND $params['re_active'] == 1 ) ? 1 : 0;
        $sql_params = array(
            $userStorage->id,
            $params['name'],
            $params['code'],
            $params['office'],
            $params['letter_status'],
            $params['print_type'],
            $params['status'],
            $signed_from,
            $signed_to,
            $expired_from,
            $expired_to,
            $strAreaId,
            $strDepartment,
            $strTeam,
            $strTitle,
            $params['different'],
            $params['off'],
            $strContractTerm,
            $params['return_letter'],
            $strCompanyId,
            $page,
            $limit,
            $params['print_status'],
            $return_letter_from,
            $return_letter_to,
            $re_active,
            $send_letter_from,
            $send_letter_to,
            $new_staff,
            $transfer,
            $re_view,

        );

        $sql = 'CALL p_contract(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,@total,?,?,?,?,?,?,?,?,?)';
        $stmt = $db->query($sql,$sql_params);
        $list = $stmt->fetchAll();
        $stmt->closeCursor();
        $total = $db->fetchOne('select @total');
        return $list;

    }

    public function confirmContract($store_params){
       $db = Zend_Registry::get('db');
       try {
           $stmt = $db->query('CALL p_ASM_Contract_confirm(?,?,?,?,?)',$store_params);
           $arrCode = $stmt->fetchAll()[0];
           foreach($arrCode as $value):
               $code = intval($value);
           endforeach;
       }catch (Exception $e){
           echo $e->getMessage();
           exit;
       }

       return $code;
   }

    public function  getSalary($asm_contract_id){
        $db = Zend_Registry::get('db');
        $asm_contract_id = intval($asm_contract_id);
        if(!$asm_contract_id){
            return false;
        }

        $sql = "
            SELECT
                a.salary as 'base_salary',
                CASE
                        WHEN a.total_salary IS NOT NULL AND a.total_salary > 0 THEN a.total_salary
                        WHEN b.bonus_salary IS NOT NULL THEN b.bonus_salary
                        WHEN c.bonus_salary IS NOT NULL THEN c.bonus_salary
                        ELSE 0 END as 'bonus_salary',
                CASE WHEN a.allowance_1 IS NOT NULL AND a.allowance_1 > 0 THEN a.allowance_1
                        WHEN b.allowance_1 IS NOT NULL AND b.allowance_1 > 0 THEN b.allowance_1
                        WHEN c.allowance_1 IS NOT NULL AND c.allowance_1 > 0 THEN c.allowance_1
                        ELSE 0 END as 'allowance_1',
                CASE WHEN a.allowance_2 IS NOT NULL AND a.allowance_2 > 0 THEN a.allowance_2
                        WHEN b.allowance_2 IS NOT NULL AND b.allowance_2 > 0 THEN b.allowance_2
                        WHEN c.allowance_2 IS NOT NULL AND c.allowance_2 > 0 THEN c.allowance_2
                        ELSE 0 END as 'allowance_2',
                CASE WHEN a.allowance_3 IS NOT NULL AND a.allowance_3 > 0 THEN a.allowance_3
                        WHEN b.allowance_3 IS NOT NULL AND b.allowance_3 > 0 THEN b.allowance_3
                        WHEN c.allowance_3 IS NOT NULL AND c.allowance_3 > 0 THEN c.allowance_3
                        ELSE 0 END as 'allowance_3',
                CASE WHEN a.kpi IS NOT NULL AND a.kpi > 0 THEN a.kpi
                        WHEN b.kpi IS NOT NULL AND b.kpi > 0 THEN b.kpi
                        WHEN c.kpi IS NOT NULL AND c.kpi > 0 THEN c.kpi
                        ELSE 0 END as 'kpi',
            FROM asm_contract a
            LEFT JOIN salary_sales b ON a.regional_market = b.province_id AND a.id IN (183,190,162,164)
            LEFT JOIN salary_pg c ON a.regional_market = c.province_id AND a.id IN (182,190)
            WHERE a.id = $asm_contract_id";

            $stmt = $db->query($sql);
            $result = $stmt->fetchRow();
            return $result;

    }

    public function save_contract($params,$id){
        
        $staff_id              = (isset($params['staff_id']) AND $params['staff_id']) ? intval($params['staff_id']) : NULL;
        $new_contract          = (isset($params['new_contract']) AND $params['new_contract']) ? intval($params['new_contract']) : NULL;
        $from_date             = (isset($params['from_date']) AND trim($params['from_date'])) ? My_Date::normal_to_mysql($params['from_date']) : NULL;
        $to_date               = (isset($params['to_date']) AND trim($params['to_date'])) ? My_Date::normal_to_mysql($params['to_date']) : NULL;
        $asm_proposal          = (isset($params['asm_proposal']) AND $params['asm_proposal']) ? intval($params['asm_proposal']) : 0;
        $note                  = (isset($params['note']) AND trim($params['note'])) ? trim($params['note']) : NULL;
        $hospital_id           = (isset($params['hospital_id']) AND $params['hospital_id']) ? intval($params['hospital_id']) : NULL;
        $status                = (isset($params['status']) AND $params['status']) ? intval($params['status']) : 0;
        $title                 = (isset($params['title']) AND $params['title']) ? intval($params['title']) : NULL;
        $regional_market       = (isset($params['regional_market']) AND $params['regional_market']) ? intval($params['regional_market']) : NULL;
        $send_letter           = (isset($params['send_letter']) AND trim($params['send_letter'])) ? My_Date::normal_to_mysql($params['send_letter']) : NULL;
        $return_letter         = (isset($params['return_letter']) AND trim($params['return_letter'])) ? My_Date::normal_to_mysql($params['return_letter']) : NULL;
        $print_type            = (isset($params['print_type']) AND $params['print_type']) ? intval($params['print_type']) : NULL;
        $print_status          = (isset($params['print_status']) AND $params['print_status']) ? intval($params['print_status']) : 0;
        $salary                = (isset($params['salary']) AND $params['salary']) ? intval($params['salary']) : NULL;
        $total_salary          = (isset($params['total_salary']) AND $params['total_salary']) ? intval($params['total_salary']) : NULL;
        $kpi_text              = (isset($params['kpi_text']) AND $params['kpi_text']) ? trim($params['kpi_text']) : NULL;
        $kpi_text_probation    = (isset($params['kpi_text_probation']) AND $params['kpi_text_probation']) ? trim($params['kpi_text_probation']) : NULL;
        $allowance_1           = (isset($params['allowance_1']) AND $params['allowance_1']) ? intval($params['allowance_1']) : NULL;
        $allowance_2           = (isset($params['allowance_2']) AND $params['allowance_2']) ? intval($params['allowance_2']) : NULL;
        $allowance_3           = (isset($params['allowance_3']) AND $params['allowance_3']) ? intval($params['allowance_3']) : NULL;
        $locked                = (isset($params['locked']) AND $params['locked']) ? intval($params['locked']) : 0;
        $work_cost             = (isset($params['work_cost']) AND $params['work_cost']) ? intval($params['work_cost']) : NULL;
        $probation_salary      = (isset($params['probation_salary']) AND $params['probation_salary']) ? intval($params['probation_salary']) : NULL;
        $base_probation_salary = (isset($params['base_probation_salary']) AND $params['base_probation_salary']) ? intval($params['base_probation_salary']) : NULL;
        $parent_id             = (isset($params['parent_id']) AND $params['parent_id']) ? intval($params['parent_id']) : NULL;
        $company_id            = (isset($params['company_id']) AND $params['company_id']) ? intval($params['company_id']) : NULL;


        if(!$staff_id){
            return array('code' => -1,'message'=> 'Vui lòng chọn nhân viên');
        }

        if(!$title){
            return array('code' => -1,'message'=> 'Vui lòng chọn chức vụ của nhân viên');
        }

        if(!$new_contract){
            return array('code' => -1,'message'=> 'Vui lòng chọn HĐ');
        }

        if($print_type == 2 and !$parent_id){
            return array('code' => -1, 'message'=> 'Vui lòng chọn Phụ lục của HĐ chính thức');
        }

        if(!$salary){
            return array('code' => -1, 'message'=> 'Vui lòng nhập mức lương cơ bản của nhân viên');
        }

        $QStaff = new Application_Model_Staff();
        $staff = $QStaff->find($staff_id)->current();
        if(!$hospital_id){
            $hospital_id = $staff->hospital_id;
        }


        $data = array(
            'staff_id'              => $staff_id,
            'new_contract'          => $new_contract,
            'from_date'             => $from_date,
            'to_date'               => $to_date,
            'asm_proposal'          => $asm_proposal,
            'note'                  => $note,
            'hospital_id'           => $hospital_id,
            'status'                => $status,
            'title'                 => $title,
            'regional_market'       => $regional_market,
            'send_letter'           => $send_letter,
            'return_letter'         => $return_letter,
            'print_type'            => $print_type,
            'print_status'          => $print_status,
            'salary'                => $salary,
            'total_salary'          => $total_salary,
            'kpi_text'              => $kpi_text,
            'kpi_text_probation'    => $kpi_text_probation,
            'allowance_1'           => $allowance_1,
            'allowance_2'           => $allowance_2,
            'allowance_3'           => $allowance_3,
            'locked'                => $locked,
            'work_cost'             => $work_cost,
            'probation_salary'      => $probation_salary,
            'base_probation_salary' => $base_probation_salary,
            'parent_id'             => $parent_id,
            'company_id'            => $company_id
        );

        try{
            if($id){
                $where = $this->getAdapter()->quoteInto('id = ?',$id);
                $this->update($data,$where);
            }else{
                $this->insert($data);
            }
            return array('code'=>1,'message'=>'Done');
        }catch (Exception $e){
            return array('code'=>-1,'message'=>$e->getMessage());
        }
    }

    public function getMainContractByStaffId($staff_id){
        $db = Zend_Registry::get('db');
        $cols = array(
            'id',
            'parent_name' => new Zend_Db_Expr("CONCAT('HD ', b.name,' từ ',DATE_FORMAT(a.from_date,'%d/%m/%Y'),' - ',DATE_FORMAT(a.to_date,'%d/%m/%Y'))")
        );

        $select = $db->select()
            ->from(array('a'=>$this->_name),$cols)
            ->join(array('b'=>'contract_term'),'b.id = a.new_contract',array())
            ->where('a.locked IS NULL OR locked = ?',0)
            ->where('a.print_type = ?',1)
            ->where('a.staff_id = ?',$staff_id);
        $result = $db->fetchAll($select);

        $data = array();
        $code = 0;
        if(count($result) > 0){
            $code = 1;
            foreach($result as $item){
                $data[$item['id']] = $item;
            }
        }

        return array('code'=>$code,'data'=>$data);

    }

    public static function update_parent($staff_id = null){
        $sql = "UPDATE asm_contract a
                INNER JOIN asm_contract b ON a.staff_id = b.staff_id 
                INNER JOIN staff c ON c.id = a.staff_id
                    AND a.locked = 0 
                    AND b.locked = 0
                    AND b.print_type = 1
                    AND a.print_type = 2 
                    AND (a.parent_id IS NULL OR a.parent_id = 0)
                    AND a.from_date BETWEEN b.from_date AND b.to_date
                SET a.parent_id = b.id,
                a.auto_note = CONCAT(IFNULL(a.auto_note,a.''),';',CURDATE(),' update parent')";
        $db = Zend_Registry::get('db');
        $stmt = $db->query($sql);
        $stmt->execute();
    }

    public function getCurrentContract($staff_id){
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare('SELECT * FROM asm_contract WHERE NOW() BETWEEN from_date AND to_date AND staff_id = :staff_id AND print_type = 1 ORDER BY id DESC LIMIT 1');
        $stmt->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
        $stmt->execute();
        $res = $stmt->fetch();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $res;
    }

}