<?php
class Application_Model_KpiStoreChecking extends Zend_Db_Table_Abstract
{
      public function findPrimaykey($id)
      {
            return $this->fetchRow($this->select()->where('id = ?',$id));
      }
      public function searchStore($table,$code,$email,$store_name,$group_id,$area_id,$regional_market,$store_on,$is_admin)
      {

            // if(!empty($_GET['dev'])){
            //       echo($store_on);die;
            // }
            $userStorage = Zend_Auth::getInstance()->getStorage()->read();
            $user_id     = $userStorage->username;
            if ($user_id != 'chau.phan') {
                // echo "<pre>";
                // print_r("Hệ thống đang bảo trì");
                // die;
            }

            $db   = Zend_Registry::get('db');
            $sql  = "CALL SP_Fetch_Store_By_Email_Code_Name(:table,:code,:email,:storename,:group_id,:area_id,:regional_market,:store_on,:is_admin)";
            $stmt               = $db->prepare($sql);
            $stmt->bindParam("table", $table,PDO::PARAM_STR);
            $stmt->bindParam("code", My_Util::escape_string($code), PDO::PARAM_STR);
            $stmt->bindParam("email", My_Util::escape_string($email), PDO::PARAM_STR);
            $stmt->bindParam("storename", My_Util::escape_string($store_name), PDO::PARAM_STR);
            $stmt->bindParam("group_id", My_Util::escape_string($group_id), PDO::PARAM_STR);
            $stmt->bindParam("area_id", My_Util::escape_string($area_id), PDO::PARAM_STR);
            $stmt->bindParam("regional_market", My_Util::escape_string($regional_market), PDO::PARAM_STR);
            $stmt->bindParam("store_on", $store_on, PDO::PARAM_INT);
            $stmt->bindParam("is_admin", $is_admin, PDO::PARAM_INT);
            $stmt->execute();

            $list               = $stmt->fetchAll();
            $stmt->closeCursor();
            return $list;
      }

      public function getStaffByEmail($code,$email){
            $db = Zend_Registry::get('db');

            $sql = '
                  SELECT s.id,s.code,s.email,CONCAT(s.firstname, " ", s.lastname) as fullname,s.status,s.regional_market, r.area_id
                  FROM staff s
                  JOIN regional_market r ON s.regional_market = r.id
                  where s.code = :code OR s.email = IF(POSITION("@oppo-aed.vn" IN :email) > 0, :email, CONCAT(:email, "@oppo-aed.vn"))
            ';
            $stmt = $db->prepare($sql);
            $stmt->bindParam("code",  My_Util::escape_string($code), PDO::PARAM_STR);
            $stmt->bindParam("email",  My_Util::escape_string($email), PDO::PARAM_STR);
            $stmt->execute();
            

            $data = $stmt->fetch();
            $stmt->closeCursor();
            $db = $stmt = null;
            return $data;
      }

      public function getAllTiming($params = array())
      {
            $db = Zend_Registry::get('db');
            $params = array_merge(
                  array(
                        'staff_id' => null,
                        'from'     => null,
                        'to'       => null
                  ),
                  $params
            );
          
            $from_date = $params['from'].' 00:00:00';
            $to_date   = $params['to'].' 23:59:59';
      
            $sql = 'SELECT g.`desc` AS product,ts.imei, st.`name` as store_name , t.approved_by, t.`from` ,t.approved_at
                                    FROM timing as t
                                    JOIN store as st
                                    ON t.store = st.id
                                    JOIN timing_sale as ts
                                    ON t.id = ts.timing_id
                                    JOIN warehouse.good as g
                                    ON ts.product_id = g.id 
                                    WHERE t.`from` >= :from_date AND t.`from` <= :to_date 
                                    AND t.staff_id = :staffid
                                    ORDER BY g.`desc`,st.`name`,t.`from` ';

            $stmt = $db->prepare($sql);                       
            $stmt->bindParam("staffid",  $params['staff_id'], PDO::PARAM_INT);
            $stmt->bindParam("from_date",  My_Util::escape_string($from_date), PDO::PARAM_STR);
            $stmt->bindParam("to_date",  My_Util::escape_string($to_date), PDO::PARAM_STR);
            $stmt->execute();
            $list = $stmt->fetchAll();
            $stmt->closeCursor();
            $db = $stmt = null;
            return $list;
      }
      public function storeTracking($params = array())
      {
            $db = Zend_Registry::get('db');
            $params = array_merge(
                  array(
                        'staff_id'    => null,
                        'created_at'  => null,
                        'table_name'  => null,
                        'id_tracking' => null,
                        'params'      => null
                  ),
                  $params
            );
            $sql = "INSERT INTO store_staff_tracking (staff_id,created_at,table_name,id_tracking,params) VALUES (:staff_id,:created_at,:table_name,:id_tracking,:params)";
            $stmt = $db->prepare($sql);
            $stmt->bindParam("staff_id",  $params['staff_id'], PDO::PARAM_INT);
            $stmt->bindParam("created_at",  My_Util::escape_string($params['created_at']), PDO::PARAM_STR);
            $stmt->bindParam("table_name",  My_Util::escape_string($params['table_name']), PDO::PARAM_STR);
            $stmt->bindParam("id_tracking",  My_Util::escape_string($params['id_tracking']), PDO::PARAM_STR);
            $stmt->bindParam("params",  My_Util::escape_string($params['params']), PDO::PARAM_STR);
            $stmt->execute();
            $stmt->closeCursor();
            $db = $stmt = null;
           
      }
      public function massUpdateStaff($params = array()){
            $db = Zend_Registry::get('db');
            $params = array_merge(
                  array(
                        'id'   => null,
                        'from' => null,
                        'to'   => null
                  ),
                  $params
            );
            
            if(empty($params['to']))
                  $params['to'] = null;
            // echo "<pre>";print_r($params);die;

            $sql = 'UPDATE store_staff_log SET joined_at=:joined_at,released_at=:released_at WHERE id IN ('.$params['id'].')';
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":joined_at",  My_Util::escape_string($params['from']), PDO::PARAM_STR);
            $stmt->bindParam(":released_at",  My_Util::escape_string($params['to']), PDO::PARAM_STR);
            
            $stmt->execute();
            $stmt->closeCursor();
            $db = $stmt = null;
            return $params['id'];
            
      }
      public function massUpdateLeader($params = array()){
            $db = Zend_Registry::get('db');
            $params = array_merge(
                  array(
                        'id'   => null,
                        'from' => null,
                        'to'   => null
                  ),
                  $params
            );
            
            if(empty($params['to']))
                  $params['to'] = null;

            $sql = 'UPDATE store_leader_log SET joined_at=:joined_at,released_at=:released_at WHERE id IN ('.$params['id'].')';
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":joined_at",  My_Util::escape_string($params['from']), PDO::PARAM_STR);
            $stmt->bindParam(":released_at",  My_Util::escape_string($params['to']), PDO::PARAM_STR);
            $stmt->execute();
            
            $stmt->closeCursor();
            $db = $stmt = null;
            return $params['id'];
      }
      public function getAllTimingRm($params = array())
      {
            $db = Zend_Registry::get('db');
            $params = array_merge(
                  array(
                        'staff_id' => null,
                        'from'     => null,
                        'to'       => null
                  ),
                  $params
            );
          
            $from_date = $params['from'].' 00:00:00';
            $to_date   = $params['to'].' 23:59:59';
      
            $sql = 'SELECT g.`desc` AS product,tss.imei, st.`name` as store_name , t.approved_by, t.`from` ,t.approved_at
                                    FROM 
                                    realme_hr.timing_realme ts 
                                    INNER JOIN realme_hr.timing_sale_realme tss ON tss.timing_id = ts.id
                                    JOIN realme_hr.timing_sale as ts1  ON ts1.imei = tss.imei
                                    JOIN realme_hr.timing as t ON t.id = ts1.timing_id
                                    JOIN realme_hr.store as st ON t.store = st.id
                                    JOIN realme_warehouse.good as g
                                    ON tss.product_id = g.id 
                                    WHERE t.`from` >= :from_date AND t.`from` <= :to_date 
                                    AND ts.staff_id = :staffid
                                    ORDER BY g.`desc`,st.`name`,t.`from` ';

            $stmt = $db->prepare($sql);                       
            $stmt->bindParam("staffid",  $params['staff_id'], PDO::PARAM_INT);
            $stmt->bindParam("from_date",  My_Util::escape_string($from_date), PDO::PARAM_STR);
            $stmt->bindParam("to_date",  My_Util::escape_string($to_date), PDO::PARAM_STR);
            $stmt->execute();
            $list = $stmt->fetchAll();
            $stmt->closeCursor();
            $db = $stmt = null;
            return $list;
      }
      

}