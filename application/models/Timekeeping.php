<?php
class Application_Model_Timekeeping extends Zend_Db_Table_Abstract
{
	protected $_name = 'timekeeping';

    public function getQueryTimeKeeping()
    {
        $rs = array('error'=>0);
        try {
            $QWS = new Application_Model_WS();
            $result = $QWS->getQueryTimeKeeping();
            return $result;

        } catch (Exception $e){
            $rs['error'] = 1;
            $rs['message'] = $e->getMessage();
        }
        return $rs;
    }
    
    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        if ($limit) {
            $select = $db->select()
                ->from(array('p' => $this->_name),
                    array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'), 'p.*'));
        } else {
            $select = $db->select()
                ->from(array('p' => $this->_name),
                    array(new Zend_Db_Expr('DISTINCT p.id'), 'p.*'));
        }

        $select->joinLeft(array('b'=>'timekeeping_staff'),'p.EnrollNumber = b.UserEnrollNumber',array('code'=>'b.UserFullCode'));

        $select->joinLeft(array('c'=>'staff'),'b.UserFullCode = c.code',array('firstname'=>'c.firstname','lastname'=>'c.lastname','email'=>'c.email'));
        
        $select->joinLeft(array('d'=>'timekeeping_device'),'d.id = p.IdMachine',array('name_machine'=>'d.Nametimekeeping_device'));
        
        $select->order('p.Date DESC');
        
        if ($limit)
            $select->limitPage($page, $limit);
            
        $result = $db->fetchAll($select);
        
        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");
            
        return $result;
    }
}                                                      
