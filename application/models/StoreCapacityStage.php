<?php

class Application_Model_StoreCapacityStage extends Zend_Db_Table_Abstract
{
    protected $_name = 'store_capacity_stage';
    protected $_schema = DATABASE_TRADE;

    public function checkValidStage($stage_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['c' => DATABASE_TRADE.'.store_capacity_stage'], [
                         'c.id'
                     ])
        ->where('c.id = ?', $stage_id)
        ->where('CURDATE() >= c.from_date')
        ->where('CURDATE() <= c.to_date');

        $result = $db->fetchOne($select);


        return $result ? true : false;
    }

    public function fetchPagination($page, $limit, &$total, $params){

        $db = Zend_Registry::get("db");
        // sub query
        $select = $db->select();
        //DISTINCT
        $arrCols = array(
            'id' => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            'p.*'
        );

        $select->from(array('p' => DATABASE_TRADE.'.store_capacity_stage'), $arrCols);

        $select->limitPage($page, $limit);

        $select->order('p.id DESC');

        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

}