<?php

class Application_Model_TransferImage extends Zend_Db_Table_Abstract
{
    protected $_name = 'transfer_image';
    protected $_schema = DATABASE_TRADE;

    public function getImage($transferId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['i' => DATABASE_TRADE.'.transfer_image'], [
                         'i.*'
                     ])
                    ->where('i.transfer_id = ?', $transferId);

        $result = $db->fetchAll($select);

        return $result;
    }
}