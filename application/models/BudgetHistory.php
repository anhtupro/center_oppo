<?php

class Application_Model_BudgetHistory extends Zend_Db_Table_Abstract
{
    protected $_name = 'budget_history';
    
    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('p'=>'budget_history'),array(
                new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
                'p.*',
                'budget_name'       => 'b.name',
                'category_group'    => 'rtg.title',
                'category_type'     => 'rt.title',
            ))
            ->joinLeft(array('b'=>'budget_hr'),'b.id = p.budget_id',array())      
            ->joinLeft(array('rt'=>'request_type'),'rt.id = b.category',array())
            ->joinLeft(array('rtg'=>'request_type_group'),'rtg.id = b.category_group',array())
            ->order('p.id DESC');
            
        ;
        if ($limit) {
            $select->limitPage($page, $limit);
        }
        $result = $db->fetchAll($select);
        
        if ($limit) {
            $total = $db->fetchOne("select FOUND_ROWS()");
        }
        
        return $result;
    }
}

