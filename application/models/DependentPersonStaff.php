<?php
class Application_Model_DependentPersonStaff extends Zend_Db_Table_Abstract
{
    protected $_name = 'dependent_person_staff';


    public function getList($code){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
        	'id'	=> "p.id",
            'mst'     => "p.mst",
            'name'       => "p.name",
            'dob'       => "p.dob",
            'relative_id'      => "p.relative_id",
            'relative'			=> "p.relative",
            'from_month'        => "p.from_month",
            'to_month'        => "p.to_month"
        );

        $select->from(array('p' => 'dependent_person_staff'), $arrCols);
        $select->where('p.code = ?', $code);
        $result = $db->fetchAll($select);
        return $result;
    }
    public function getListExport(){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'id'    => "p.id",
            'mst'     => "p.mst",
            'name'       => "p.name",
            'dob'       => "p.dob",
            'relative_id'      => "p.relative_id",
            'relative'          => "p.relative",
            'from_month'        => "p.from_month",
            'to_month'        => "p.to_month"
        );

        $select->from(array('p' => 'dependent_person_staff'), $arrCols);
        $select->joinLeft(array('s'=>'staff'),'p.staff_id=s.id',array('staff_name' => "Concat(s.firstname,' ',s.lastname)",'staff_code' =>'s.code'));
        $select->joinLeft(array('team'=>'team'),'s.team=team.id',array('team_name'=>'team.name'));
        $select->joinLeft(array('department'=>'team'),'s.department=department.id',array('department_name'=>'department.name'));
        $select->joinLeft(array('title'=>'team'),'s.title=title.id',array('title_name'=>'title.name'));

        $result = $db->fetchAll($select);
        return $result;
    }
}