<?php

class Application_Model_CampaignArea extends Zend_Db_Table_Abstract
{
    protected $_name = 'campaign_area';
    protected $_schema = DATABASE_TRADE;

    function getCampaignAreaInsert($params = null)
    {

        $db = Zend_Registry::get('db');

        $arrCols = array(
            'area_id' => 'p.id',
            'campaign_id' => "c.id",
            'category_id' => "cat.category_id",
            'price' => "cat.price"
        );

        $select = $db->select()
            ->from(array('p' => 'area'), $arrCols);
        $select->joinCross(array('c' => DATABASE_TRADE . '.campaign_demo'), NULL, array());
        $select->joinLeft(array('cat' => DATABASE_TRADE . '.campaign_category_demo'), 'cat.campaign_id = c.id', array());
        $select->where('p.status = ?', 1);

        if ($params['campaign_id']) {
            $select->where('c.id = ?', $params['campaign_id']);
        }

        $result = $db->fetchAll($select);

        return $result;
    }

    function getCampaignArea($params)
    {
        $db = Zend_Registry::get('db');

        $arrCols = array(
            'area_id' => 'a.id',
            'area_name' => "a.name",
            'campaign_id' => "p.campaign_id",
            'category_id' => "p.category_id",
            'price' => "p.price",
            'quantity' => "p.quantity",
            'quantity_limit' => "p.quantity_limit",
            'total_price' => "p.total_price",
            'status' => "p.status",
        );

        $select = $db->select()
            ->from(array('p' => DATABASE_TRADE . '.campaign_area'), $arrCols);
        $select->joinLeft(array('a' => 'area'), 'a.id = p.area_id', array());

        $select->where('p.campaign_id = ?', $params['campaign_id']);

        if ($params['area_id']) {
            $select->where('p.area_id IN (?)', $params['area_id']);
        }
        if (!empty($params['confirm'])) {
//            $select->where('p.quantity > 0', NULL);
        }

        if ($_GET['dev'] == 2) {
            echo $select;
            exit;
        }

        $result = $db->fetchAll($select);

        return $result;
    }

    function getContractorCategory($params)
    {
        $db = Zend_Registry::get('db');

        $arrCols = array(
            'p.contractor_id',
            'p.area_id',
            'p.category_id',
            'p.price',
            'code' => 'GROUP_CONCAT(c.code)',
            'status' => 'GROUP_CONCAT(p.status)',
            'price' => 'GROUP_CONCAT(p.price)'
        );

        $select = $db->select()
            ->from(array('p' => DATABASE_TRADE . '.contractor_category'), $arrCols);
        $select->joinLeft(array('c' => DATABASE_TRADE . '.contructors'), 'c.id = p.contractor_id', array());

        $select->where('p.campaign_id = ?', $params['campaign_id']);

        $select->group(['p.area_id', 'p.category_id']);

        $result = $db->fetchAll($select);

        return $result;
    }

    function get_status_area($params = null)
    {
        $db = Zend_Registry::get('db');

        $arrCols = array(
            'p.campaign_id',
            'p.area_id',
            'p.category_id',
            'p.quantity_limit',
            'p.quantity',
            'p.price',
            'p.status',
            's.name'
        );

        $select = $db->select()
            ->from(array('p' => DATABASE_TRADE . '.campaign_area'), $arrCols);

        $select->joinLeft(array('s' => DATABASE_TRADE . '.app_status'), 's.status = p.status', array());

        $select->where('p.quantity > 0', NULL);

        $select->where('s.type = ?', 3);

        $select->group('p.area_id');

        if ($params['campaign_id']) {
            $select->where('p.campaign_id = ?', $params['campaign_id']);
        }

        if ($params['area_id']) {
            $select->where('p.area_id IN (?)', $params['area_id']);
        }


        $result = $db->fetchAll($select);

        $data = [];

        foreach ($result as $key => $value) {
            $data[$value['area_id']] = $value;
        }

        return $data;

    }

    function get_status_max($params = null)
    {
        $db = Zend_Registry::get('db');

        $arrCols = array(
            'p.campaign_id',
            'p.area_id',
            'p.category_id',
            'p.quantity_limit',
            'p.quantity',
            'p.price',
            'status' => 'MAX(p.status)',
            's.name'
        );

        $select = $db->select()
            ->from(array('p' => DATABASE_TRADE . '.campaign_area'), $arrCols);

        $select->joinLeft(array('s' => DATABASE_TRADE . '.app_status'), 's.status = p.status', array());

        if (!empty($params['area_id'])) {
            $select->where('p.area_id IN (?)', $params['area_id']);
        }


        $select->where('s.type = ?', 3);

        if ($params['campaign_id']) {
            $select->where('p.campaign_id = ?', $params['campaign_id']);
        }

        $result = $db->fetchRow($select);

        return $result['status'];
    }

    function get_status_min($params = null)
    {
        $db = Zend_Registry::get('db');

        $arrCols = array(
            'p.campaign_id',
            'p.area_id',
            'p.category_id',
            'p.quantity_limit',
            'p.quantity',
            'p.price',
            'status' => 'MIN(p.status)',
            's.name'
        );

        $select = $db->select()
            ->from(array('p' => DATABASE_TRADE . '.campaign_area'), $arrCols);

        $select->joinLeft(array('s' => DATABASE_TRADE . '.app_status'), 's.status = p.status', array());

        $select->where('s.type = ?', 3);

        if ($params['area_id']) {
            $select->where('p.area_id IN (?)', $params['area_id']);
        }

        if ($params['campaign_id']) {
            $select->where('p.campaign_id = ?', $params['campaign_id']);
        }

        if ($_GET['dev'] == 3) {
            echo $select;
            exit;
        }

        $result = $db->fetchRow($select);

        return $result['status'];
    }

    function getAreaByCampaign($campaign_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "a.id",
            "a.name"
        );

        $select->from(array('p' => DATABASE_TRADE . '.contractor_category'), $arrCols);
        $select->joinLeft(array('a' => 'area'), 'a.id = p.area_id', array());
        $select->where('p.campaign_id = ?', $campaign_id);

        $select->group('p.area_id');
        $select->order('a.name');

        $result = $db->fetchAll($select);

        $data = [];
        foreach ($result as $key => $value) {
            $data[$value['id']] = $value['name'];
        }

        return $data;
    }

    public function getPosmCost($params)
    {
        $date = "2019-04-01"; // không tính chi phí quí 1 năm 2019

        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['ca' => DATABASE_TRADE . '.category_additional'], [
                'category_id_additional' => 'ca.id',
                'additional_category_name' => 'ca.name',
                'category_id' =>  'c.id',
                'category_name' => 'c.name',
                'e.area_id',
                'cost' => 'SUM(e.quantity * t.price)',
                'campaign.id',
                'campaign.from',
            ])
            ->joinLeft(['c' => DATABASE_TRADE . '.category'], 'ca.id = c.parent_id_additional', [])
            ->joinLeft(['e' => DATABASE_TRADE . '.campaign_area'], 'c.id = e.category_id', [])
            ->joinLeft(['t' => DATABASE_TRADE . '.contractor_category'], 'e.area_id = t.area_id AND e.campaign_id = t.campaign_id AND e.category_id = t.category_id', [])
            ->joinLeft(['campaign' => DATABASE_TRADE . '.campaign_demo'], 't.campaign_id = campaign.id', [])
            ->where('campaign.created_at >= ? ', $date)
            ->group(['ca.id', 'e.area_id'])
            ->order(['e.area_id']);

        if ($params['month_in_season']) {
            $select->where('MONTH(campaign.from) IN (?)', $params['month_in_season']);
        }

        if ($params['month']) {
            $select->where('MONTH(campaign.from) = ?', $params['month']);
        }




        if ($params['from_date']) {
            $select->where("DATE_FORMAT(campaign.from,'%Y-%m-%d') >= ?", $params['from_date']);
        }

        if ($params['to_date']) {
            $select->where("DATE_FORMAT(campaign.from,'%Y-%m-%d') <= ?", $params['to_date']);
        }

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $posmCost [$element['area_id']] [$element['category_id_additional']]  = $element;
        }

        return $posmCost;

    }


    public function getDataExport($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['c' => DATABASE_TRADE.'.campaign_demo'], [
                'campaign_name' => 'c.name',
                'area_name' => 'a.name',
                'category_name' => 'ca.name',
                'd.quantity'
            ])
            ->joinLeft(['d' => DATABASE_TRADE.'.campaign_area'], 'c.id = d.campaign_id', [])
            ->joinLeft(['ca' => DATABASE_TRADE.'.category'], 'd.category_id = ca.id', [])
            ->joinLeft(['a' => 'area'], 'd.area_id = a.id', [])
            ->where('d.quantity > 0');

        if ($params['category_id']) {
            $select->where('d.category_id = ?', $params['category_id']);
        }

        if ($params['campaign_id']) {
            $select->where('c.campaign_id = ?', $params['campaign_id']);
        }

        if ($params['area_id']) {
            $select->where('d.area_id = ?', $params['area_id']);
        } else {
            $select->where('d.area_id IN (?)', $params['area_list']);
        }

        $result = $db->fetchALl($select);

        return $result;

    }

    public function exportAll($params)
    {

        require_once 'PHPExcel.php';

        $data = $this->getDataExport($params);

        $PHPExcel = new PHPExcel();
        $heads = array(
            'Stt',
            'Tên đợt đặt hàng',
            'Khu vực',
            'Hạng mục',
            'Số lượng'
        );
        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $title)
        {
            $sheet->setCellValue($alpha.$index, $title);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($data as $item){

            $alpha    = 'A';
            $sheet->setCellValue($alpha++.$index, $i++);
            $sheet->setCellValue($alpha++.$index, $item['campaign_name']);
            $sheet->setCellValue($alpha++.$index, $item['area_name']);
            $sheet->setCellValue($alpha++.$index, $item['category_name']);
            $sheet->setCellValue($alpha++.$index, $item['quantity']);

            $index++;

        }

        $filename = 'Tổng hợp số lượng đặt hàng POSM' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }

}















