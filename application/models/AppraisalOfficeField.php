<?php
class Application_Model_AppraisalOfficeField extends Zend_Db_Table_Abstract
{
    protected $_name = 'appraisal_office_field';
    protected $_primary = 'aof_id';
    
    public function getFieldPrd($planId, $userId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = ['aot_row' => 'COUNT(DISTINCT t.aot_id)', 't.aot_id' ,'p.aom_head_department_id', 'p.aom_staff_id', 'p.aom_head_approved', 'p.aom_staff_approved', 'f.aof_id', 
            'f.aof_name', 'f.aof_is_deleted', 't.aot_target', 't.aot_task', 't.aot_skill', 't.aot_ratio', 't.aot_self_appraisal', 't.aot_note',  't.aot_head_department_appraisal', 't.aot_head_department_note', 'f.aof_name'];

        $select->from(array('p' => 'appraisal_office_member'), $arrCols);
        $select->joinLeft(array('f' => 'appraisal_office_field'), 'f.aom_id = p.aom_id', array());
        $select->joinLeft(array('t' => 'appraisal_office_task'), 't.aof_id = f.aof_id', array());

        $select->where('p.aop_id = ?', $planId);
        $select->where('p.aom_is_deleted = 0', NULL);
        $select->where('f.aof_is_deleted = 0', NULL);
        $select->where('t.aot_is_deleted = 0', NULL);
        $select->group('f.aof_id');
        
        $result = $db->fetchAll($select);
        
        return $result;
    }
    
    public function getTaskPrd($planId, $userId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = [ 't.aot_id' ,'p.aom_head_department_id', 'p.aom_staff_id', 'p.aom_head_approved', 'p.aom_staff_approved', 'f.aof_id', 
            'f.aof_name', 'f.aof_is_deleted', 't.aot_target', 't.aot_task', 't.aot_skill', 't.aot_ratio', 't.aot_self_appraisal', 't.aot_note',  't.aot_head_department_appraisal', 't.aot_head_department_note'];

        $select->from(array('p' => 'appraisal_office_member'), $arrCols);
        $select->joinLeft(array('f' => 'appraisal_office_field'), 'f.aom_id = p.aom_id', array());
        $select->joinLeft(array('t' => 'appraisal_office_task'), 't.aof_id = f.aof_id', array());

        $select->where('p.aop_id = ?', $planId);
        $select->where('t.aot_is_deleted = 0');
        
        $result = $db->fetchAll($select);

        return $result;
    }

    public function getFieldNull($planId, $userId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.aom_id", 
            "p.aom_staff_id", 
            "f.aof_id",
        );

        $select->from(array('p' => 'appraisal_office_member'), $arrCols);
        $select->joinLeft(array('f' => 'appraisal_office_field'), 'f.aom_id = p.aom_id', array());
        $select->joinLeft(array('t' => 'appraisal_office_task'), 't.aof_id = f.aof_id', array());

        $select->where('p.aop_id = ?', $planId);
        $select->where('p.aom_staff_id = ?', $userId);
        $select->where('p.aom_is_deleted = 0 AND f.aof_is_deleted = 0 AND t.aof_id IS NULL', NULL);
        $result = $db->fetchAll($select);

        return $result;
    }
    
    
}