<?php

class Application_Model_CheckShopEntireImageRatio extends Zend_Db_Table_Abstract
{
    protected $_name = 'check_shop_entire_image_ratio';
    protected $_schema = DATABASE_TRADE;

    public function get($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['i' => DATABASE_TRADE.'.check_shop_entire_image_ratio'], [
                         'i.*',
                         'category_name' => 'c.name'
                     ])
                      ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'i.category_id = c.id', [])
                     ->where('i.check_shop_id = ?', $params['check_shop_id']);

        $result = $db->fetchAll($select);

        return $result;
    }
}    