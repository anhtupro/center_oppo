<?php
class Application_Model_AppraisalOfficeToDo extends Zend_Db_Table_Abstract
{
    protected $_name = 'appraisal_office_to_do';
    
    public function initPlan($planId)
    {
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $sql = "call sp_create_cache_appraisal_office(:plan_id);" ;
        $stmt = $db->prepare($sql);
        $stmt->bindParam('plan_id', $planId, PDO::PARAM_INT);
        $stmt->execute();
        $stmt->closeCursor();
    }
    
    public function initPlanPrd($planId)
    {
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $sql = "call sp_create_cache_appraisal_office_prd(:plan_id);" ;
        $stmt = $db->prepare($sql);
        $stmt->bindParam('plan_id', $planId, PDO::PARAM_INT);
        $stmt->execute();
        $stmt->closeCursor();
    }
    
    public function getListStaff($planId, $userId)
    {
        $select = $this->select()
            ->from(['s' => 'staff'], ['id', 'department', 'team', 'title', 'lastname', 'firstname'])
            ->join([$this->_name], 'to_staff = s.id')
            ->where('from_staff = ?', $userId)
            ->where('fk_plan = ?', $planId)
            ->where('s.off_date IS NULL')
            ->setIntegrityCheck(false);
        
        return $this->fetchAll($select)->toArray();
    }
    
    public function getListStaffPrd($planId, $userId)
    {
        $select = $this->select()
            ->from(['s' => 'staff'], ['id', 'department', 'team', 'title', 'lastname', 'firstname'])
            ->join(['appraisal_office_member'], 'aom_staff_id = s.id')
            ->where('aom_head_department_id = ?', $userId)
            ->where('aop_id = ?', $planId)
            ->where('s.off_date IS NULL')
            ->setIntegrityCheck(false);
        
        return $this->fetchAll($select)->toArray();
    }
    
    public function getListStaffView($planId, $headId, $userId, $star)
    {
        $sql = "SELECT *
                FROM (
                        SELECT p.aom_id, p.aop_id, p.aom_head_department_id, p.aom_staff_id, p.aom_head_approved, p.aom_staff_approved, p.aom_is_deleted,
                        t.aot_ratio, t.aot_self_appraisal, t.aot_head_department_appraisal,
                        SUM(star.ratio*t.aot_ratio)/100 total_point,
                        CASE WHEN SUM(star.ratio*t.aot_ratio)/100 >= 110 THEN 5
                        WHEN SUM(star.ratio*t.aot_ratio)/100 >= 100 THEN 4
                        WHEN SUM(star.ratio*t.aot_ratio)/100 >= 90 THEN 3
                        WHEN SUM(star.ratio*t.aot_ratio)/100 >= 70 THEN 2
                        WHEN SUM(star.ratio*t.aot_ratio)/100 >= 0 THEN 1
                        END total_star,
                        s.department,
                        s.lastname,
                        s.firstname,
                        s.id,
                        s.team,
                        s.title
                        FROM appraisal_office_member p
                        LEFT JOIN staff s ON s.id = p.aom_staff_id
                        LEFT JOIN appraisal_office_field f ON f.aom_id = p.aom_id
                        LEFT JOIN appraisal_office_task t ON t.aof_id = f.aof_id
                        LEFT JOIN appraisal_office_star star ON star.star = t.aot_head_department_appraisal
                        WHERE p.aop_id = :plan_id AND p.aom_is_deleted = 0 AND s.off_date IS NULL
                        GROUP BY p.aom_staff_id
                ) p WHERE 1";
        
        if(!empty($star)){
            $sql .= ' AND total_star = '.$star;
        }
        
        if(!empty($headId)){
            $sql .= ' AND p.aom_head_department_id = '.$headId;
        }
        
        if(!empty($userId)){
            $sql .= ' AND p.aom_staff_id = '.$userId;
        }
        
        $db   = Zend_Registry::get('db');
        $stmt = $db->prepare($sql);
        $stmt->bindParam('plan_id', $planId, PDO::PARAM_INT);
        
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        
        return $data;
    }
    
    public function checkIsHead($staff_id)
    {
        /*
        $sql = "SELECT p.aom_head_department_id, p.aom_staff_id
                FROM appraisal_office_member_root p
                WHERE p.aom_head_department_id = :staff_id
                ";
        
        $db   = Zend_Registry::get('db');
        $stmt = $db->prepare($sql);
        $stmt->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
        
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        
        if(!empty($data)){
            return 1;
        }
        else{
            return 0;
        }
         */
        
        $sql = "SELECT p.from_staff aom_head_department_id, p.to_staff aom_staff_id
                FROM appraisal_office_to_do_root p
                WHERE p.from_staff = :staff_id AND p.is_del = 0
                ";
        
        $db   = Zend_Registry::get('db');
        $stmt = $db->prepare($sql);
        $stmt->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
        
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        
        if(!empty($data)){
            return 1;
        }
        else{
            return 0;
        }
        
        
    }
    
    public function getHeadOfStaff($staff_id)
    {
        $sql = "SELECT p.fk_plan, p.from_staff, p.to_staff
                FROM appraisal_office_to_do p
                WHERE p.fk_plan = (SELECT MAX(aop_id) aop_id FROM appraisal_office_plan WHERE aop_type = 1 )
                AND p.to_staff = :staff_id
                ";
        
        $db   = Zend_Registry::get('db');
        $stmt = $db->prepare($sql);
        $stmt->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
        
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        
        if(!empty($data)){
            return $data[0]['from_staff'];
        }
        else{
            return false;
        }
    }
    
    public function checkIsStaff($staff_id)
    {
        /*
        $sql = "SELECT p.aom_head_department_id, p.aom_staff_id
                FROM appraisal_office_member_root p
                WHERE p.aom_staff_id = :staff_id
               ";
        
        $db   = Zend_Registry::get('db');
        $stmt = $db->prepare($sql);
        $stmt->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
        
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        
        if(!empty($data)){
            return 1;
        }
        else{
            return 0;
        }
         */
        
        $sql = "SELECT p.from_staff aom_head_department_id, p.to_staff aom_staff_id
                FROM appraisal_office_to_do_root p
                WHERE p.to_staff = :staff_id 
               ";
        
        $db   = Zend_Registry::get('db');
        $stmt = $db->prepare($sql);
        $stmt->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
        
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        
        if(!empty($data)){
            return 1;
        }
        else{
            return 0;
        }
        
    }

    public function getStaff($planId, $userId)
    {
        $select = $this->select()
            ->from(['s' => 'staff'], ['id', 'department', 'team', 'title', 'lastname', 'firstname'])
            ->join([$this->_name], 'aotd_staff_id = s.id')
            ->where('aotd_staff_id = ?', $userId)
            ->where('fk_plan = ?', $planId)
            ->setIntegrityCheck(false);
        return $this->fetchAll($select)->toArray();
    }
    
    public function getStaffMember($planId, $userId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = ['p.id', 'p.department', 'p.team', 'p.title', 'p.lastname', 'p.firstname'];

        $select->from(array('p' => 'staff'), $arrCols);
        $select->joinLeft(array('m' => 'appraisal_office_member'), 'm.aom_staff_id = p.id', array());

        $select->where('m.aom_staff_id = ?', $userId);
        $select->where('m.aop_id = ?', $planId);
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getStaffById($userId)
    {
        $db          = Zend_Registry::get('db');
        $select = $db->select()
            ->from(['s' => 'staff'], ['id', 'department', 'team', 'title', 'lastname', 'firstname'])
            ->joinLeft(array('r' => 'appraisal_office_to_do_root'), 'r.to_staff = s.id', array())
            ->where('r.to_staff = ?', $userId);
        return $db->fetchAll($select);
    }
    
    public function getToDoSurvey($planId, $staffId)
    {
       
        $db          = Zend_Registry::get('db');
        $select      = $db->select();

        $arrCols = array(
            'aotd_id',
            'fk_plan',
            'from_staff',
            'from_title',
            'to_staff',
            'type',
            'survey_id',
            'to_title' => 't.name'
        );

        $select->from(array('p' => 'appraisal_office_to_do'), $arrCols);
        $select->joinLeft(array('t' => 'team'), 't.id = p.to_title', array());

        $select->where('p.fk_plan = ?', $planId);
        $select->where('p.from_staff = ?', $staffId);

        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getTitleById($userId, $planId)
    {
        $data = $this->fetchAll(
            $this->select()
            ->distinct()
            ->from($this->_name, ['from_title'])
            ->where('from_staff = ?', $userId)
            ->where('fk_plan = ?', $planId)
        )->toArray();
        $titleId = null;
        foreach ($data as $datum) {
            $titleId = $datum['from_title'];
            break;
        }
        return $titleId;
    }
    
    public function getStaffOffice()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            'fullname' => "CONCAT(p.firstname, ' ',p.lastname)",
            'title_id' => 'p.title'
        );

        $select->from(array('p' => 'staff'), $arrCols);
        $select->joinLeft(array('t' => 'team'), 't.id = p.title', array());
        $select->joinLeft(array('c' => 'company_group'), 'c.id = t.company_group', array());
        
        
        //Lấy khối văn phòng và Trainer leader, TMK Leader
        $select->where('(c.id = 1 AND p.off_date IS NULL) OR (p.title IN (639, 626))', NULL);
        $select->order('p.firstname ASC');
        $result = $db->fetchAll($select);
        
        return $result;
    }
    
    public function getStaffOfficeNotPrd()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            'fullname' => "CONCAT(p.firstname, ' ',p.lastname)",
            'title_id' => 'p.title',
            'title_name' => 't.name',
            'team_name' => 't2.name',
            'p.joined_at'
        );

        $select->from(array('p' => 'staff'), $arrCols);
        $select->joinLeft(array('t' => 'team'), 't.id = p.title', array());
        $select->joinLeft(array('t2' => 'team'), 't2.id = p.team', array());
        $select->joinLeft(array('c' => 'company_group'), 'c.id = t.company_group', array());
        $select->joinLeft(array('r' => 'appraisal_office_to_do_root'), '(r.to_staff = p.id OR r.from_staff = p.id)', array());

        $select->where('(c.id = 1 OR t.id IN (?)) AND p.off_date IS NULL', APPRAISAL_OFFICE_TITLE);
        $select->where('r.to_staff IS NULL AND r.from_staff IS NULL', NULL);
        $select->order('p.firstname ASC');
        $result = $db->fetchAll($select);
        
        return $result;
    }
    
    public function getStaffApproveByStaff($staff_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            's.id',
            'fullname' => "CONCAT(s.firstname, ' ',s.lastname)",
            's.title',
            'from_fullname' => "CONCAT(s2.firstname, ' ',s2.lastname)",
            'from_id' => "s2.id",
            'from_title' => "s2.title",
            'p.is_prd'
        );

        $select->from(array('p' => 'appraisal_office_to_do_root'), $arrCols);
        $select->joinLeft(array('s' => 'staff'), 's.id = p.to_staff', array());
        $select->joinLeft(array('s2' => 'staff'), 's2.id = p.from_staff', array());

        $select->where('p.to_staff = ?', $staff_id);
        $select->where('s.off_date IS NULL AND p.is_del = 0', NULL);
        $result = $db->fetchRow($select);

        return $result;
    }
}