<?php
class Application_Model_DateCategory extends Zend_Db_Table_Abstract
{
    protected $_name = "date_category";

    public function getAllSelect()
    {
        $db = Zend_Registry::get('db');
        $sql = "SELECT *
                FROM date_category
                WHERE is_del <> 1";
                
        $data = $db->fetchAll($sql);

        $db = null;
        return  $data;
    }

    public function getAll()
    {
        $db = Zend_Registry::get('db');
        $sql = "SELECT *
                FROM date_category";
                
        $data = $db->fetchAll($sql);

        $db = null;
        return  $data;
    }
    
}   