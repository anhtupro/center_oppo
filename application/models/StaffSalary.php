<?php

class Application_Model_StaffSalary extends Zend_Db_Table_Abstract {

    protected $_name = 'staff_salary';

    public function getRecentWorkCost($staff_id) {
        $db = Zend_Registry::get('db');
        $select = "SELECT * FROM staff_salary s
        JOIN ( SELECT MAX(id) AS id  FROM staff_salary s WHERE s.`work_cost_salary` > 0 AND s.staff_id = :staff_id GROUP BY s.staff_id) 
        AS m ON s.id=m.id ";
        $stmt = $db->prepare($select);
        $stmt->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
//        $stmt->bindParam('title', $params['title'], PDO::PARAM_INT);

        $stmt->execute();
    
        $data = $stmt->fetch();
        $stmt->closeCursor();
        $stmt = $db = null;
        return $data;
    }
     public function getMaxInsuranceSalary($staff_id) {
        $db = Zend_Registry::get('db');
        $select = "SELECT * FROM v_max_seniority s
         WHERE  s.staff_id = :staff_id";
        $stmt = $db->prepare($select);
        $stmt->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
//        $stmt->bindParam('title', $params['title'], PDO::PARAM_INT);

        $stmt->execute();
    
        $data = $stmt->fetch();
        $stmt->closeCursor();
        $stmt = $db = null;
        return $data;
    }

}
