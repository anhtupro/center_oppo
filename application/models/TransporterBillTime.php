<?php
class Application_Model_TransporterBillTime extends Zend_Db_Table_Abstract
{
    protected $_name = 'transporter_bill_time';

    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            'p.reception_id',
            'p.bill_number',
            'p.bill_time',
            'p.bill_type',
            'p.desc',
            'p.is_send_partner',
        );

        $select->from(array('p' => 'transporter_bill_time'), $arrCols);

        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
}