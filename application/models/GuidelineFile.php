<?php

class Application_Model_GuidelineFile extends Zend_Db_Table_Abstract
{
    protected $_name = 'guideline_file';
    protected $_schema = DATABASE_TRADE;

    public function getAll()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['f' => DATABASE_TRADE.'.guideline_file'], [
                         'f.*'
                     ])
                     ->where('f.is_deleted IS NULL');
        
        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['guideline_id']] [] = $element;
        }

        return $list;
    }
}