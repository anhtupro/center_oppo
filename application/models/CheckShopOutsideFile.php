<?php

class Application_Model_CheckShopOutsideFile extends Zend_Db_Table_Abstract
{
    protected $_name = 'check_shop_outside_file';
    protected $_schema = DATABASE_TRADE;

    public function get($recheckShopId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['f' => DATABASE_TRADE . '.check_shop_outside_file'], [
                'url' => 'f.url',
                'type' => 'f.type'
            ])
            ->where('f.check_shop_outside_id = ?', $recheckShopId)
            ->where('f.type IN (?)', [0, 1, 2])
            ->where('f.is_deleted = ?', 0);

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getImgDifferent($checkShopId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.url',
            'p.type'
        );
        $select->from(array('p' => DATABASE_TRADE . '.check_shop_outside_file'), $arrCols);
        $select->where('p.check_shop_outside_id = ?', $checkShopId);
        $select->where('p.type NOT IN (?)', array(0, 1, 2));
        $select->order('p.type ASC');
        $result = $db->fetchAll($select);
        return $result;
    }
}