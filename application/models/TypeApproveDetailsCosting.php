<?php
class Application_Model_TypeApproveDetailsCosting extends Zend_Db_Table_Abstract
{
	//protected $_schema = DATABASE_COST;
    protected $_name = 'type_approve_details_costing';

    public function getData()
    {
    	$db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id',
            'staff_id'=> 'p.staff_id',
            'staff_name'	=> "CONCAT('Mr.','',s.lastname)",
            'p.type_approve_id'
        );
         
        $select->from(array('p'=> 'type_approve_details_costing'), $arrCols);
        $select->joinLeft(array('s' => DATABASE_CENTER.'.staff'), 's.id = p.staff_id',array());
        $select->joinLeft(array('t' => 'type_signature_costing'), 't.id = p.type',array());
        
        $result = $db->fetchAll($select);
        return $result;
    }
    public function getDetails($params)
    {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id',
            'staff_id'=> 'p.staff_id',
            'staff_name'    => "CONCAT('Mr.','',s.lastname)",
            'p.type_approve_id',
            'p.type',
            'p.key_signature',
            'p.img_signature',
            'p.swh_signature'
        );
         
        $select->from(array('p'=> 'type_approve_details_costing'), $arrCols);
        $select->joinLeft(array('s' => DATABASE_CENTER.'.staff'), 's.id = p.staff_id',array());
        $select->joinLeft(array('t' => 'type_signature_costing'), 't.id = p.type',array());
        if(!empty($params['id'])){
            $select->where('p.type_approve_id = ?',$params['id']);
        }
        $result = $db->fetchAll($select);
        return $result;
    }
  }