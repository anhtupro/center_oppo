<?php
class Application_Model_TeamBuildingServicePQ extends Zend_Db_Table_Abstract
{
    protected $_name = 'team_building_service_pq';

    public function check_staff_service($staff_id){
        $db = Zend_Registry::get('db');
        $sql = 'SELECT staff_id,team,status,is_mentor,room,bus_number
				FROM team_building_service_pq 
				WHERE staff_id = :staff_id';
        $stmt = $db->prepare($sql);
        $stmt->bindParam("staff_id",  $staff_id, PDO::PARAM_STR);
        $stmt->execute();

        $data = $stmt->fetch();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $data;
    }

    public function get_data_team($team){
        $db = Zend_Registry::get('db');
        $sql = 'SELECT t.full_name, t.staff_id,
				s.`code`,t.team color,department.`name` department_name,team.`name` team_name,title.`name` title_name,
				s.gender,s.photo,t.`status`
				FROM team_building_service_pq t
				JOIN staff s
				ON t.staff_id = s.id
				LEFT JOIN team team ON s.team = team.id
				LEFT JOIN team title ON s.title = title.id
				LEFT JOIN team department ON s.department = department.id AND department.parent_id = 0 
				WHERE t.team = :team AND (is_mentor = 0 or is_mentor is null)
				ORDER BY t.`status` DESC,staff_id ASC
				';
        $stmt = $db->prepare($sql);
        $stmt->bindParam("team",  $team, PDO::PARAM_STR);
        $stmt->execute();

        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $data;
    }
}