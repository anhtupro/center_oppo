<?php

class Application_Model_RequestPermission extends Zend_Db_Table_Abstract
{
    protected $_name = 'request_permission';
    function getPermissionByStaffID($staff_id){

        $db = Zend_Registry::get("db");
        $select = $db->select();
        $select->from(array('p' => 'request_permission'),array(""))
            ->joinLeft(array("request_permission_type"),"p.permission_type=request_permission_type.id",array())
                ->joinLeft(array("rpd"=>"request_permission_detail"),"request_permission_type.permission_detail=rpd.id",array("rpd.payment_type","rpd.company_id","rpd.has_purchasing_request"));

        $select->where("p.staff_id = ? ",$staff_id);

        $result = $db->fetchAll($select);
        $total = $db->fetchAll("select FOUND_ROWS()");
        return $result;
    }
}