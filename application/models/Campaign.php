<?php
class Application_Model_Campaign extends Zend_Db_Table_Abstract
{
    protected $_name = 'campaign_demo';
	protected $_schema = DATABASE_TRADE;
	
    public function getCampaign($params){

        $curent_date = date('Y-m-d');

		$db = Zend_Registry::get('db');
        $select  = $db->select();
        $select_main  = $db->select();

        $arrCols = array(
        	'id' 			=> "p.id",
            'name' 			=> "p.name",
            'created_at' 	=> 'p.created_at',
            'from' 			=> 'p.from',
            'to' 			=> 'p.to',
            'status_id'     => 'MAX(d.status)',
            'status_name'   => NULL
        );

        $select->from(array('p'=> DATABASE_TRADE.'.campaign_demo'), $arrCols);

        $select->joinLeft(array('a' => DATABASE_TRADE.'.app_order'), 'a.campaign_id = p.id', array());        
        
        $select->joinLeft(array('d' => DATABASE_TRADE.'.app_order_details'), 'd.order_id = a.id', array());
        $select->joinLeft(array('s'=> 'store'), 's.id = d.store_id',array());
        $select->joinLeft(array('c'=> 'store_area_chanel'), 'c.store_id = s.store_id',array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.district', array());
        $select->joinLeft(array('r2' => 'regional_market'), 'r2.id = s.regional_market', array());
        $select->joinLeft(array('status' => DATABASE_TRADE.'.app_status'), 'status.`status` = d.`status` AND status.type = '.TU_BAN_BUCGOC.'', array());

        $select->where('DATE(p.to) >= ?', $curent_date);

        if(isset($params['area_id']) and $params['area_id']){
            $select->where('IFNULL(c.area_id, r2.area_id) IN (?)', $params['area_id']);
        }

        $select->group('p.id');

        $select->order('p.created_at DESC');
       
        $result = $db->fetchAll($select);
        return $result;	

	}

    public function getCategory($params){

        if(empty($params['campaign_id'])){
            return false;
        }

        $curent_date = date('Y-m-d');

        $db = Zend_Registry::get('db');
        $select  = $db->select();
        $select_main  = $db->select();

        $arrCols = array(
            'id'            => "cat.id",
            'name'          => "cat.name",
            'category_id'   => 'cat.id',
            'campaign_name' => 'p.name',
            'campaign_id'   => 'p.id',
            'photo'         => 'cat.photo'
        );

        $select->from(array('p'=> DATABASE_TRADE.'.campaign_demo'), $arrCols);

        $select->joinLeft(array('cc' => DATABASE_TRADE.'.campaign_category_demo'), 'cc.campaign_id = p.id', array());

        $select->joinLeft(array('cat' => DATABASE_TRADE.'.category'), 'cat.id = cc.category_id', array());

        $select->joinLeft(array('c' => DATABASE_TRADE.'.contractor_category'), 'c.category_id = cat.id', array('c.price'));

        if(!empty($params['contractor_id'])){
            $select->where('c.contractor_id = ?', $params['contractor_id']);
        }

        if(!empty($params['campaign_id'])){
            $select->where('p.id = ?', $params['campaign_id']);
        }

        $result = $db->fetchAll($select);
        return $result;     
    }

    public function getCategoryCampaign($params){

        if(empty($params['campaign_id'])){
            return false;
        }

        $curent_date = date('Y-m-d');

        $db = Zend_Registry::get('db');
        $select  = $db->select();
        $select_main  = $db->select();

        $arrCols = array(
            'id'            => "cat.id",
            'name'          => "cat.name",
            'category_id'   => 'cat.id',
            'campaign_name' => 'p.name',
            'campaign_id'   => 'p.id',
            'photo'         => 'cat.photo',
            'cat.code'
        );

        $select->from(array('p'=> DATABASE_TRADE.'.campaign_demo'), $arrCols);

        $select->joinLeft(array('cc' => DATABASE_TRADE.'.campaign_category_demo'), 'cc.campaign_id = p.id', array());

        $select->joinLeft(array('cat' => DATABASE_TRADE.'.category'), 'cat.id = cc.category_id', array());
        
        $select->joinLeft(array('cac' => DATABASE_TRADE.'.campaign_area_contractor'), 'cac.category_id = cat.id', array());
        
        $select->where('cc.del = 0 OR cc.del IS NULL');
        
        
        if(!empty($params['contractor_id'])){
            $select->where('cac.contractor_id = ?', $params['contractor_id']);
        }
        
        
        if(!empty($params['campaign_id'])){
            $select->where('p.id = ?', $params['campaign_id']);
        }
        
        $select->group('cat.id');

        $result = $db->fetchAll($select);
        return $result;     
    }

    public function getListStore($params){

        $db = Zend_Registry::get('db');
        $select  = $db->select();

        $arrCols = array(
            'name_store'    => "p.name",
            'id'            => "p.id",
        );

        $select->from(array('p'=> 'store'), $arrCols);
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if ($userStorage->title == SALES_LEADER_TITLE) {
            $select->joinLeft(array('s' => 'store_leader_log'), 's.store_id = p.id', array());
        }
        else{
            $select->joinLeft(array('s' => 'store_staff_log'), 's.store_id = p.id', array());
        }

        $select->joinLeft(array('r' => 'regional_market'), 'r.id = p.district', array());

        $select->joinLeft(array('r2' => 'regional_market'), 'r2.id = p.regional_market', array());

        $select->where('s.released_at IS NULL AND s.staff_id = ?', $params['staff_id']);
        $select->where('p.del is NULL or p.del= 0', NULL);
        
        $select->order('p.created_at DESC');

        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getListStoreArea($params){

        $db = Zend_Registry::get('db');
        $select  = $db->select();

        $arrCols = array(
            'name_store'    => "p.name",
            'id'            => "p.id",
        );

        $select->from(array('p'=> 'store'), $arrCols);

        $select->joinLeft(array('s' => 'store_staff_log'), 's.store_id = p.id', array());

        $select->joinLeft(array('r' => 'regional_market'), 'r.id = p.district', array());

        $select->joinLeft(array('r2' => 'regional_market'), 'r2.id = p.regional_market', array());

        $select->where('s.released_at IS NULL', NULL);
        $select->where('p.del is NULL or p.del= 0', NULL);
        
        if(!empty($params['area_id'])){
            $select->where('r2.area_id IN (?)', $params['area_id']);
        }
        
        $select->order('p.created_at DESC');

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getCategoryQuantity($params){

        if(empty($params['campaign_id'])){
            return false;
        }

        $curent_date = date('Y-m-d');

        $db = Zend_Registry::get('db');
        $select  = $db->select();
        $select_main  = $db->select();

        $arrCols = array(
            'category_id'   => "p.category_id",
            'quantity'      => "SUM(p.quantity)",
            'name'          => 'c.name'
        );

        $select->from(array('p'=> DATABASE_TRADE.'.app_order_details'), $arrCols);

        $select->joinLeft(array('o' => DATABASE_TRADE.'.app_order'), 'o.id = p.order_id', array());

        $select->joinLeft(array('c' => DATABASE_TRADE.'.category'), 'c.id = p.category_id', array());

        if(!empty($params['campaign_id'])){
            $select->where('o.campaign_id = ?', $params['campaign_id']);
        }

        $select->group('p.category_id');

        $result = $db->fetchAll($select);
        return $result;     
    }

    public function getListContractor($type){

        $db = Zend_Registry::get('db');
        $select  = $db->select();
        $select_main  = $db->select();

        $arrCols = array(
            'id'     => "p.id",
            'name'   => "p.name",
            'type'   => 'p.type'
        );

        $select->from(array('p'=> DATABASE_TRADE.'.contructors'), $arrCols);

        $select->where('p.type = ?', $type);

        $result = $db->fetchAll($select);
        return $result;  
        
    }

    public function getCampaignStatus($params){

        if(empty($params['staff_id'])){
            return false;
        }

        $curent_date = date('Y-m-d');

        $db = Zend_Registry::get('db');
        $select  = $db->select();
        $select_main  = $db->select();

        $arrCols = array(
            "campaign_id"   => "p.campaign_id",
            'status'        => "MAX(d.status)",
            'status_name'   => 's.name',
        );

        $select->from(array('p'=> DATABASE_TRADE.'.app_order'), $arrCols);

        $select->joinLeft(array('d' => DATABASE_TRADE.'.app_order_details'), 'd.order_id = p.id', array());

        $select->joinLeft(array('s' => DATABASE_TRADE.'.app_status'), 's.id = d.status', array());

        $select->where('p.created_by = ?', $params['staff_id']);

        $select->group('p.campaign_id');

        $result = $db->fetchAll($select);
        return $result;     
    }

    public function getListCampaign($params){

        $curent_date = date('Y-m-d');

        $db = Zend_Registry::get('db');
        $select  = $db->select();
        $select_main  = $db->select();

        $arrCols = array(
            'id'     => "p.id",
            'name'   => "p.name",
            'type'   => 'p.type',
            'from'   => 'p.from',
            'to'     => 'p.to'
        );

        $select->from(array('p'=> DATABASE_TRADE.'.campaign_demo'), $arrCols);

        $select->where('p.type = ?', $params['type']);

        $select->order('p.id DESC');

        //$select->where('DATE(p.to) >= ?', $curent_date);

        $result = $db->fetchAll($select);
        //echo $select; exit;
        return $result;  
        
    }

    public function fetchPagination($page, $limit, &$total, $params){

        $db = Zend_Registry::get('db');

        $select = $db->select();
            
        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 
            "p.name", 
            "p.from", 
            "p.to",
            "p.status"
        );

        $select->from(array('p'=> DATABASE_TRADE.'.campaign_demo'), $arrCols);
        
        $select->where('p.status = 1', NULL);

        $select->where('p.type = 3', NULL);

        $select->order('created_at DESC');

        if ($limit)
            $select->limitPage($page, $limit);

        $result  = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;

    }

    public function getContractor($params){

        $db = Zend_Registry::get('db');

        $select = $db->select();
            
        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 
            "p.name", 
            "p.short_name", 
            "p.address",
            "p.code",
            "p.status",
            "p.user",
        );

        $select->from(array('p'=> DATABASE_TRADE.'.contructors'), $arrCols);

        $result  = $db->fetchAll($select);

        return $result;

    }

    public function getListCampaignStatus($params){ 

        $db     = Zend_Registry::get("db");
        $select = $db->select();
            
        $arrCols = array(
            'campaign_id' => 'p.id', 
            'status'        =>  'MAX(d.status)', 
            'campaign_name' => 'p.name', 
            'area_id'       => 'r.area_id'
        );
        
        $select->from(array('p'=> DATABASE_TRADE.'.campaign_demo'), $arrCols);
        $select->joinLeft(array('o' => DATABASE_TRADE.'.app_order'), 'o.campaign_id = p.id', array());
        $select->joinLeft(array('d' => DATABASE_TRADE.'.app_order_details'), 'd.order_id = o.id', array());
        $select->joinLeft(array('s' => 'store'), 's.id = d.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());

        $select->where('p.type = ?', TU_BAN_BUCGOC);

        if(!empty($params['staff_id'])){
            $select->where('o.created_by = ?', $params['staff_id']);
        }
        
        
        if(!empty($params['area_id'])){
            $select->where('r.area_id IN (?)', $params['area_id']);
        }
        
        $select->group('p.id');
        
        $result  = $db->fetchAll($select);

        if($_GET['dev'] == 1){
            echo $select;exit;
        }
        
        return $result;
    }

    public function getStatusCampaign(){

        $db     = Zend_Registry::get("db");
        $select = $db->select();
            
        $arrCols = array(
            'id'        => "p.id",
            'status'    => "p.status",
            'name'      => "p.name",
            'type'      => "p.type",
        );
        
        $select->from(array('p'=> DATABASE_TRADE.'.app_status'), $arrCols);
        $select->where('p.type = ?', 2);
        
        $result  = $db->fetchAll($select);

        $data = [];
        foreach($result as $key=>$value){
            $data[$value['status']] = $value['name'];
        }
        
        return $data;
    }
    public function getListStoreDecord($params){

        $db = Zend_Registry::get('db');
        $select  = $db->select();

        $arrCols = array(
            'name_store'    => "p.name",
            'id'            => "p.id",
        );

        $select->from(array('p'=> 'store'), $arrCols);
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if ($userStorage->title == SALES_LEADER_TITLE) {
            $select->joinLeft(array('s' => 'store_leader_log'), 's.store_id = p.id', array());
        }
        else{
            $select->joinLeft(array('s' => 'store_staff_log'), 's.store_id = p.id', array());
        }
        $select->joinLeft(array('r2' => 'regional_market'), 'r2.id = p.regional_market', array());

        $select->where('s.released_at IS NULL');
        $select->where('r2.area_id IN (?)',$params['area_list']);
        $select->where('p.del is NULL or p.del= 0', NULL);
        
        $select->order('p.created_at DESC');

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getListPosmFee($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['c' => DATABASE_TRADE.'.campaign_demo'], [
                         new Zend_Db_Expr('SQL_CALC_FOUND_ROWS c.id'),
                         'campaign_name' => 'c.name',
                         'c.from',
                         'c.to',
                         'c.created_at',
                         'c.month',
                         'total_price' => 'SUM(a.quantity * r.price)'

                     ])
                    ->joinLeft(['a' => DATABASE_TRADE.'.campaign_area'], 'c.id = a.campaign_id', [])
                    ->joinLeft(['r' => DATABASE_TRADE.'.contractor_category'], 'a.campaign_id = r.campaign_id AND a.area_id = r.area_id AND a.category_id = r.category_id', [])
                    ->where('a.quantity <> 0');

        if ($params['list_area']) {
            $select->where('a.area_id IN (?)', $params['list_area']);
        }

        if ($params['month']) {
            $select->where('MONTH(c.created_at) = ?', $params['month']);
        }

        if ($params['year']) {
            $select->where('YEAR(c.created_at) = ?', $params['year']);
        }

        if ($params['from_date']) {
            $select->where("DATE_FORMAT(c.from,'%Y-%m-%d') >= ?", $params['from_date']);
        }

        if ($params['to_date']) {
            $select->where("DATE_FORMAT(c.from,'%Y-%m-%d') <= ?", $params['to_date']);
        }

        if ($params['cost_type']) {
            $select->where("c.cost_type = ?", $params['cost_type']);
        }

        $select->group('c.id');
        $select->having('SUM(a.quantity * r.price) > 0');
        $select->order('c.id DESC');

        if ($limit) {
            $select->limitPage($page, $limit);
        }
//        echo $select->__toString();
//        die;
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;


    }



}