<?php

class Application_Model_StaffPermissionFinal extends Zend_Db_Table_Abstract
{
    protected $_name = 'staff_permission_final';

    public function getDepartment()
    {
        $db = Zend_Registry::get('db');

        $sql = "SELECT t.id, t.`name`
                    FROM `team` t
                    WHERE t.parent_id = 0 and t.del <>1";

        $data = $db->fetchAll($sql);
        $db = null;
        return $data;
    }

    public function checkPermission($code)
    {
        if(empty($code))
        {
            return false;
        }

        $db = Zend_Registry::get('db');
        $sql = "SELECT *
                    FROM `staff_permission` sp
                    WHERE sp.staff_code = :code
                        AND (sp.is_leader > 0 OR sp.is_manager > 0)";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("code", $code, PDO::PARAM_STR);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $stmt = $db = null;
        if(!empty($data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function fetchPagination($params = array())
    {
        $db = Zend_Registry::get('db');

        $stmt = $db->prepare("CALL `get_staff_permission_final`(:group, :type_permission, :value_permission, :office, :department, :permission, :limit, :offset)");

        $params['permission'] = empty($params['permission'])?null:$params['permission'];
        $params['department'] = empty($params['department'])?null:$params['department'];
        $params['limit'] = empty($params['limit'])?null:$params['limit'];
        $params['office'] = empty($params['office'])?null:$params['office'];
        $params['group']=null;
        $stmt->bindParam("group", $params['group'], PDO::PARAM_INT);
        $stmt->bindParam("type_permission",$params['group'], PDO::PARAM_INT);
        $stmt->bindParam("value_permission",$params['group'], PDO::PARAM_INT);
        $stmt->bindParam("office",$params['office'], PDO::PARAM_INT);
        $stmt->bindParam("permission",$params['permission'], PDO::PARAM_INT);
        $stmt->bindParam("department",$params['department'], PDO::PARAM_INT);
        $stmt->bindParam("limit",$params['limit'], PDO::PARAM_INT);
        $stmt->bindParam("offset",$params['offset'], PDO::PARAM_INT);


        $stmt->execute();
        $data['data'] = $stmt->fetchAll();
        $stmt->closeCursor();
        $data['total'] = $db->query("SELECT FOUND_ROWS()")->fetchColumn();
        return $data;
    }

    public function getStaffPermissionLogin($staff_code)
    {
        $db = Zend_Registry::get('db');

        $stmt = $db->prepare("CALL `get_staff_permission_login`(:code)");

        $stmt->bindParam("code",$staff_code, PDO::PARAM_STR);

        $stmt->execute();

        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $data;
    }

    public function updateStaffPermission($params = array())
    {
        $db = Zend_Registry::get('db');

        $stmt = $db->prepare("UPDATE `staff_permission`
                                    SET is_leader = :is_leader, is_manager = :is_manager, is_global = :is_global
                                    WHERE id = :id ");

        $stmt->bindParam("is_leader", $params['is_leader'], PDO::PARAM_INT);
        $stmt->bindParam("is_manager", $params['is_manager'], PDO::PARAM_INT);
        $stmt->bindParam("is_global", $params['is_global'], PDO::PARAM_INT);
        $stmt->bindParam("id", $params['id'], PDO::PARAM_INT);

        $stmt->execute();
        $stmt->closeCursor();
        $db = $stmt = null;
    }

//        public function inserValues($values)
//        {
//            $db = Zend_Registry::get('db');
//            $sql = "INSERT INTO
//                        `staff_permission` (`staff_code`, `office_id`,`is_all_office`, `department_id`, `team_id`, `title_id`, `is_manager`, `is_leader`, `is_global`,`is_approve`)
//                    VALUES " . $values . "
//                    ON duplicate key UPDATE `is_manager` = VALUES(`is_manager`) , `is_leader` =  VALUES(`is_leader`),
//                                `department_id` = VALUES(`department_id`),`team_id` = `team_id`,
//                                `title_id` = `title_id`,
//                                `office_id` = VALUES(`office_id`), `is_all_office` = VALUES(`is_all_office`),
//                                `is_global` = VALUES(`is_global`), `is_approve` = VALUES(`is_approve`)
//                    ";
//            $db->query($sql);
//            $db = null;
//        }

    public function save($data, $id){
        $db = Zend_Registry::get('db');

        $db->beginTransaction();
        try {
            if ($id) {

                $where = $this->getAdapter()->quoteInto('id = ?', $id);

//                $this->update($data, $where);
//                debug( $this->update($data, $where)); exit;

            } else {
                    $this->insert($data);
            }

            $arr = array('code' => 1, 'message' => 'Done');
            $db->commit();
            return $arr;

        } catch (Exception $e) {
            $db->rollBack();
            $arr = array('code' => -1, 'message' => $e->getMessage());
        }

    }

    public function getOffice()
    {
        $db = Zend_Registry::get('db');

        $sql = "SELECT o.*
                    FROM `office` o
                    WHERE o.del = 0";

        $data = $db->fetchAll($sql);

        $db = null;

        return $data;
    }

    public function getStaff($input = '')
    {
        $db = Zend_Registry::get('db');

        $sql = "SELECT st.id as `id`, st.code as `code`, CONCAT(st.firstname, ' ', st.lastname, ' | ', st.code) as `staff_info`
                    FROM staff st
                    WHERE st.code = :input_code
                        OR st.email = :input_email 
                        OR CONCAT(st.firstname, ' ', st.lastname) LIKE :input_name";

        $input_code = $input;
        $input_email = $input . "@oppomobile.vn";
        $input_name = "%" . $input . "%";

        $stmt = $db->prepare($sql);

        $stmt->bindParam("input_code", $input_code, PDO::PARAM_STR);
        $stmt->bindParam("input_email", $input_email, PDO::PARAM_STR);
        $stmt->bindParam("input_name", $input_name, PDO::PARAM_STR);

        $stmt->execute();

        $data = $stmt->fetchAll();

        $stmt->closeCursor();
        $stmt = $db = null;

        return $data;
    }

    public function getListCode($code = '')
    {

    }


    public function getPermissionPurchasingRequest($staff_id)
    {
        $db = Zend_Registry::get('db');

        $data = array();

        if(!empty($staff_id)){

            $sql = "SELECT st.id AS  'staff_id', st.`code` , sp.`is_manager` , sp.`is_leader` , sp.`department_id` 
                        FROM staff st
                        INNER JOIN staff_permission sp ON st.code = sp.`staff_code` 
                        WHERE st.id = ".$staff_id."
                        AND sp.department_id is not NULL
                        AND (sp.is_leader = 1 OR sp.is_manager = 1)
                        GROUP BY  sp.`staff_code` ,  sp.`department_id` ";

            $stmt = $db->prepare($sql);
            $stmt->execute();
            $data = $stmt->fetchAll();
            $stmt->closeCursor();
            $stmt = $db = null;
        }

        return $data;
    }
    public function getOfficeId($staff_code)
    {
        $db     = Zend_Registry::get("db");
        $select = $db->select();
        $select->from(array('o'=> 'staff_permission'), array('office_id' => 'o.office_id'))->where('staff_code =?', $staff_code);

        $result = $db->fetchAll($select);
        foreach ($result as $value) {
            $officeId[] = $value['office_id'];
        }

        return $officeId;
    }

    public function getBySn($sn){

        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('sp'=>'staff_permission_final'),array("*"))
            ->where('sp.del = ?',0)
            ->where('sp.sn = ?',$sn)
        ;
        $result = $db->fetchAll($select);
        return $result;
    }

    public function updateValueApp($sn, $id, $valueApp){
        $db = Zend_Registry::get('db');
        $sql = "UPDATE `staff_permission_final` SET `value_app` = '" . My_Util::escape_string($valueApp) . "' WHERE `sn` = " . My_Util::escape_string($sn) . " AND `type_app` = '" . $id . "'";
        $db->query($sql);
    }
	
    public function getListIdStaffCong($params)
    {
        $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($params['month']), $params['year']);        
        $from_date = date($params['year'] . '-' . $params['month'] . '-1');
        $to_date = date($params['year'] . '-' . $params['month'] . '-' . $number_day_of_month);
		
        $db = Zend_Registry::get('db');
        $data = array();
		if(empty($params['areaApproved'])) {
			$sql = "
					SELECT SQL_CALC_FOUND_ROWS st.id as `id` 
					FROM `staff` AS `st`
					INNER JOIN `team` AS `te` ON st.team = te.id
					INNER JOIN `regional_market` AS `rm` ON st.regional_market = rm.id
					INNER JOIN `regional_market` AS `r` ON rm.id = r.parent
					LEFT JOIN `company_group_map` AS `cgm` ON st.title = cgm.title
					LEFT JOIN `staff_pending_time` AS `spt` ON st.id = spt.staff_id AND cgm.company_group <> 6 AND spt.month = " . $params['month'] . " AND spt.year = " . $params['year'] . "
					INNER JOIN `area` AS `ar` ON ar.id = rm.area_id
					LEFT JOIN (
						SELECT tcd.staff_id as `staff_id`
						FROM `trainer_course_detail` tcd
						JOIN `trainer_course_timing` AS `tct` ON tct.course_detail_id = tcd.id
						WHERE tcd.staff_id IS NOT NULL AND (tct.date BETWEEN '" . $from_date . "' AND '" . $to_date . "')
						GROUP BY tcd.staff_id
					) AS `str` ON str.staff_id = st.id 
					WHERE (( (st.joined_at <= '" . $from_date . "' OR (st.joined_at between '" . $from_date . "' AND '" . $to_date . "') ) AND (st.off_date IS NULL OR st.off_date > '" . $from_date . "') ) OR str.staff_id IS NOT NULL) 
						AND (st.title <> 375) AND (st.off_date IS NULL OR st.off_date > '" . $from_date . "') AND te.id IN (" . $params['teamApproved'] . ")
					GROUP BY `st`.`id` 
					ORDER BY `spt`.`status` desc, `st`.`id` asc
			";
		} else {
			$sql = "
					SELECT SQL_CALC_FOUND_ROWS st.id as `id` 
					FROM `staff` AS `st`
					INNER JOIN `team` AS `te` ON st.team = te.id
					INNER JOIN `regional_market` AS `rm` ON st.regional_market = rm.id
					INNER JOIN `regional_market` AS `r` ON rm.id = r.parent
					LEFT JOIN `company_group_map` AS `cgm` ON st.title = cgm.title
					LEFT JOIN `staff_pending_time` AS `spt` ON st.id = spt.staff_id AND cgm.company_group <> 6 AND spt.month = " . $params['month'] . " AND spt.year = " . $params['year'] . "
					INNER JOIN `area` AS `ar` ON ar.id = rm.area_id
					LEFT JOIN (
						SELECT tcd.staff_id as `staff_id`
						FROM `trainer_course_detail` tcd
						JOIN `trainer_course_timing` AS `tct` ON tct.course_detail_id = tcd.id
						WHERE tcd.staff_id IS NOT NULL AND (tct.date BETWEEN '" . $from_date . "' AND '" . $to_date . "')
						GROUP BY tcd.staff_id
					) AS `str` ON str.staff_id = st.id 
					WHERE (( (st.joined_at <= '" . $from_date . "' OR (st.joined_at between '" . $from_date . "' AND '" . $to_date . "') ) AND (st.off_date IS NULL OR st.off_date > '" . $from_date . "') ) OR str.staff_id IS NOT NULL) 
						AND (st.title <> 375) AND (st.off_date IS NULL OR st.off_date > '" . $from_date . "') AND te.id IN (" . $params['teamApproved'] . ") AND ar.id IN (" . $params['areaApproved'] . ")
					GROUP BY `st`.`id` 
					ORDER BY `spt`.`status` desc, `st`.`id` asc
			";
		}
		//echo '<pre>'; print_r($sql); die;
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll();
		$stmt->closeCursor();
		$stmt = $db = null;
		
        return $data;
    }
	
	public function checkPermissionStaffId($id)
    {
        if(empty($id))
        {
            return false;
        }

        $db = Zend_Registry::get('db');
        $sql = "SELECT *
                    FROM `staff_permission_final` sp
                    WHERE sp.value = :id
                        AND (sp.is_leader > 0 OR sp.is_manager > 0)";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id, PDO::PARAM_STR);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $stmt = $db = null;
        if(!empty($data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function getNameArea($id){

        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('a'=>'area'),array('area_name' => 'GROUP_CONCAT(a.`name`)', 'area_id' => 'GROUP_CONCAT(a.`id`)'))
            ->where('a.id IN (' . $id . ')')
        ;
        $result = $db->fetchAll($select);
        return $result;
    }
	
	public function getDepartmentApr()
    {
        $db = Zend_Registry::get('db');
        $sql = "SELECT spf.`value`, t.id, t.`name`
				FROM staff_permission_final spf
				INNER JOIN team t ON t.id = spf.`value`
				GROUP BY t.id
			";
        $data = $db->fetchAll($sql);
        $db = null;
        return $data;
    }
	
	public function getTypePermission()
    {
        $db = Zend_Registry::get('db');
        $sql = "SELECT spf.type, spf.group_permission, spf.is_manager
				FROM staff_permission_final spf
				GROUP BY spf.type, spf.group_permission, spf.is_manager
			";
        $data = $db->fetchAll($sql);
        $db = null;
        return $data;
    }
	
	public function getTeam()
    {
        $db = Zend_Registry::get('db');
        $sql = "SELECT t1.id, t1.`name`
				FROM team t
				INNER JOIN team t1 ON t1.parent_id = t.id
				INNER JOIN staff_permission_final spf ON t1.id = spf.value_app
				WHERE t.parent_id = 0 AND t.del = 0 AND t1.del = 0
			";
        $data = $db->fetchAll($sql);
        $db = null;
        return $data;
    }
	
	public function getTitle()
    {
        $db = Zend_Registry::get('db');
        $sql = "SELECT t2.id, t2.`name`
				FROM team t
				INNER JOIN team t1 ON t1.parent_id = t.id
				INNER JOIN team t2 ON t2.parent_id = t1.id
				INNER JOIN staff_permission_final spf ON t2.id = spf.value_app
				WHERE t.parent_id = 0 AND t.del = 0 AND t1.del = 0 AND t2.del = 0
			";
        $data = $db->fetchAll($sql);
        $db = null;
        return $data;
    }
	
	public function getTitleDeparmentApproved()
    {
        $db = Zend_Registry::get('db');
        $sql = "SELECT t2.id, t2.`name`
				FROM team t
				INNER JOIN team t1 ON t1.parent_id = t.id
				INNER JOIN team t2 ON t2.parent_id = t1.id
				INNER JOIN staff_permission_final spf ON t2.id = spf.value
				WHERE t.parent_id = 0 AND t.del = 0 AND t1.del = 0 AND t2.del = 0
				GROUP BY t2.id
			";
        $data = $db->fetchAll($sql);
        $db = null;
        return $data;
    }
	
	public function getInforStaffPermissionFinal()
    {
        $db = Zend_Registry::get('db');
        $sql = "SELECT spf.`value` id_staff, CONCAT(s.firstname,' ',s.lastname) full_name
				FROM staff_permission_final spf
				INNER JOIN staff s ON s.id = spf.`value`
				GROUP BY s.id
			";
        $data = $db->fetchAll($sql);
        $db = null;
        return $data;
    }
	
	public function getInforTitlePermissionFinal()
    {
        $db = Zend_Registry::get('db');
        $sql = "SELECT spf.`value` id_staff, t.`name` title_name
				FROM staff_permission_final spf
				INNER JOIN team t ON t.id = spf.`value`
				GROUP BY t.id
			";
        $data = $db->fetchAll($sql);
        $db = null;
        return $data;
    }
	
	public function getDepartmentApproved($id)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('t'=>'team'),array('id_team' => 't.id', 'name_team' => 't.name'))
            ->where('t.id = ' . $id . '')
            ->where('t.del = 0')
        ;
        $result = $db->fetchAll($select);
        $db = null;
        return $result;
    }
	
	public function getTeamApproved($id)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('t'=>'team'),array())
			->join(array('t2'=>'team'),'t.id = t2.parent_id',array('id_team' => 't2.id', 'name_team' => 't2.name'))
            ->where('t.id = ' . $id . '')
            ->where('t2.del = 0')
        ;
        $result = $db->fetchAll($select);
        $db = null;
        return $result;
    }
	
	public function getTitleApproved($id)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('t'=>'team'),array())
			->join(array('t2'=>'team'),'t.id = t2.parent_id',array())
			->join(array('t3'=>'team'),'t2.id = t3.parent_id',array('id_team' => 't3.id', 'name_team' => 't3.name'))
            ->where('t.id = ' . $id . '')
            ->where('t3.del = 0')
        ;
        $result = $db->fetchAll($select);
        $db = null;
        return $result;
    }

    public function getStaffApproved($id)
    {
        $db = Zend_Registry::get('db');
        $sql = "SELECT s.id, CONCAT(s.firstname,' ',s.lastname) full_name
				FROM staff s
				WHERE s.off_date is null
                AND s.id in (" . $id . ")
			";
        $data = $db->fetchAll($sql);
        $db = null;
        return $data;
    }

    public function getStaffPermissionFinalApprove($staff_id_or_title, $type, $area_id_staff = 0) {
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare('CALL get_staff_approve_permission_final (:staff_id_or_title, :type, :area_id_staff)');
        $stmt->bindParam('staff_id_or_title', $staff_id_or_title, PDO::PARAM_INT);
        $stmt->bindParam('type', $type, PDO::PARAM_INT);
        $stmt->bindParam('area_id_staff', $area_id_staff, PDO::PARAM_INT);

        $stmt->execute();
        $staffs = $stmt->fetchAll();
        $stmt->closeCursor();

        return $staffs;
    }

    public function getDataPermissionFinalByGroup($group_permission_id) {
        $db = Zend_Registry::get('db');
        $sql = "SELECT spf.*,
                    GROUP_CONCAT(IF(spf.type_app = 1, spf.value_app, null)) AS t_a_id,
                    GROUP_CONCAT(IF(spf.type_app = 4, spf.value_app, null)) AS t_department_id,
                    GROUP_CONCAT(IF(spf.type_app = 5, spf.value_app, null)) AS t_team_id,
                    GROUP_CONCAT(IF(spf.type_app = 6, spf.value_app, null)) AS t_title_id,
                    GROUP_CONCAT(IF(spf.type_app = 7, spf.value_app, null)) AS t_office_id,
                    GROUP_CONCAT(IF(spf.type_app = 8, spf.value_app, null)) AS t_staff_id,
                    GROUP_CONCAT(IF(spf.type_app = 9, spf.value_app, null)) AS t_filter_area_id,
                    COUNT(spf.id) as count_id
                FROM staff_permission_final as spf
                WHERE group_permission = " . $group_permission_id . " and del = 0
                group by spf.sn
        ";
       $data = $db->fetchAll($sql);
       $db = null;
       return $data;
    }

    public function getAreaIdByRegionalMarket($regional_market) {
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->distinct()
            ->from(array('s'=>'staff'),array())
			->join(array('rm'=>'regional_market'),'s.regional_market = rm.id',array(''))
			->join(array('a'=>'area'),'rm.area_id = a.id',array('a.id', 'a.name'))
            ->where('s.regional_market = ' . $regional_market)
        ;
        $result = $db->fetchRow($select);
        $db = null;
        return $result;
    }

    public function getLeaderByStaff($params) {
        
        define('TYPE_AREA_ID', 1);
        define('TYPE_PROVINCE_ID', 2);
        define('TYPE_DISTRICT_ID', 3);
        define('TYPE_DEPARTMENT_ID', 4);
        define('TYPE_TEAM_ID', 5);
        define('TYPE_TITLE_ID', 6);
        define('TYPE_OFFICE_ID', 7);
        define('TYPE_STAFF_ID', 8);
        
        if(empty($params['staff_id'])){
            return;
        }
        
        if(empty($params['group_permission'])){
            return;
        }
        
        $QStaff = new Application_Model_Staff();
        $where = $QStaff->getAdapter()->quoteInto('id = ?', $params['staff_id']);
        $staff = $QStaff->fetchRow($where);
        
        //Lấy theo title => approve team
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.group_permission", 
            "p.type", 
            "p.value", 
            "p.level_manage", 
            "d.type_app", 
            "d.value_app",
            "fullname" => "CONCAT(s.firstname, ' ',s.lastname)", 
            "staff_id" => "s.id",
            "title_name" => "t.name"
        );

        $select->from(array('p' => 'staff_permissions'), $arrCols);
        $select->joinLeft(array('d' => 'staff_permission_detail'), 'd.id_staff_permissions = p.id', array());
        $select->joinLeft(array('s' => 'staff'), 's.title = p.value', array());
        $select->joinLeft(array('t' => 'team'), 't.id = p.value', array());

        $select->where('p.group_permission = '.$params['group_permission']);//Tool Name
        $select->where('d.type_app = ?', 6);//Approve of Title
        $select->where('d.value_app = ?', $staff['title']);//Accept theo Title
        $select->where('p.type = ?', 2);//Permission For Title
        
        $permission_staff = $db->fetchRow($select);
        
        return $permission_staff ? $permission_staff : NULL;
    }
}