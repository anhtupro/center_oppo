<?php
class Application_Model_TrainingStaffType extends Zend_Db_Table_Abstract
{
	protected $_name = 'training_staff_type';
  public function getType($params){

        $db     = Zend_Registry::get("db");
        $select = $db->select();
            
        $arrCols = array(
            'type'       => 's.type',
            'ID_number'   =>'s.ID_number'
        );
        $select->from(array('s'=> 'training_staff_type'), $arrCols);
        $select ->where('s.ID_number = ?',$params['ID_number']);
        $result  = $db->fetchAll($select);
        return $result;
  }

  public function getStaffBrand()
  {
      $db     = Zend_Registry::get("db");
        $select = $db->select();
            
        $arrCols = array(
            'ID_number'       => 's.ID_number'
        );
        $select->from(array('s'=> 'training_staff_type'), $arrCols);
        $select->where('s.type = ?',1);
        $result  = $db->fetchAll($select);
        return $result;
  }
   public function getStaffStore()
  {
      $db     = Zend_Registry::get("db");
        $select = $db->select();
            
        $arrCols = array(
            'ID_number'       => 's.ID_number'
        );
        $select->from(array('s'=> 'training_staff_type'), $arrCols);
        $select->where('s.type = ?',2);
        $result  = $db->fetchAll($select);
        return $result;
  }

}