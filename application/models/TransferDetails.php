<?php
class Application_Model_TransferDetails extends Zend_Db_Table_Abstract
{
	protected $_name = 'transfer_details';
    protected $_schema = DATABASE_TRADE;
    
    public function getTransferDetails($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.transfer_id", 
            "p.category_id", 
            "p.quantity", 
            "reason_title" => "r.title", 
            "category_name" => "c.name", 
            "p.note",
            "p.transfer_type",
            "contructors_name"  => "con.name",
            "p.transfer_date",
            "c.has_type",
            "c.has_width",
            'c.has_material',
            'c.has_painting',
            'a.height',
            'a.width',
            'material_name' => 'm.name',
            'app_checkshop_detail_child_id' => 'a.id',
            'category_type_name' => 'w.name',
            'category_type_child_name' => 'y.name'


        );

        $select->from(array('p' => DATABASE_TRADE.'.transfer_details'), $arrCols);
        $select->joinLeft(array('c' => DATABASE_TRADE.'.category'), 'c.id = p.category_id', array());
        $select->joinLeft(array('r' => DATABASE_TRADE.'.transfer_reason'), 'r.id = p.transfer_reason_id', array());
        $select->joinLeft(array('con' => DATABASE_TRADE.'.contructors'), 'con.id = p.contructors_id', array());
        $select->joinLeft(array('a' => DATABASE_TRADE.'.app_checkshop_detail_child'), 'p.app_checkshop_detail_child_id = a.id', array());
        $select->joinLeft(array('m' => DATABASE_TRADE.'.category_material'), 'a.material_id = m.id', array());
        $select->joinLeft(['w' => DATABASE_TRADE.'.category_inventory_type'], 'a.category_type = w.id', []);
        $select->joinLeft(['y' => DATABASE_TRADE.'.category_inventory_type'], 'a.category_type_child = y.id', []);

        $select->where('p.transfer_id = ?', $params['id']);

        if ($params['version'] && $params['version'] == 2) {
            $select->joinLeft(['o' => DATABASE_TRADE . '.app_checkshop_detail_child'], 'p.app_checkshop_detail_child_id = o.id', ['o.height', 'o.width']);
            $select->joinLeft(['t' => DATABASE_TRADE . '.category_inventory_type'], 'o.category_type = t.id', [
                'category_type_name' => 't.name',
                'category_type_id' => 't.id'
            ]);
        }
        
        $result = $db->fetchAll($select);
        
        return $result;
    }
    
     public function getContract($params){
         $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "store"    => "s.name",
            "category"      => "c.name",
            "p.id", 
            "p.transfer_id", 
            "p.category_id", 
            "final_price"      => "p.price_final",
            "p.quantity",
            "p.contract_id"
        );

        $select->from(array('p' => DATABASE_TRADE.'.transfer_details'), $arrCols);
         $select->joinLeft(array('a' => DATABASE_TRADE.'.transfer'), 'a.id = p.transfer_id', array());
        $select->joinLeft(array('c' => DATABASE_TRADE.'.category'), 'c.id = p.category_id', array());
         $select->joinLeft(array('s' => 'store'), 's.id = a.store_from', array());
        
        $select->where('p.contructors_id = ?', $params['contractor_id']);
        $select->where('a.id IS NOT NULL');
        $result = $db->fetchAll($select);
        return $result;
    }
    
    
    
}