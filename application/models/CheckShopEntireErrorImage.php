<?php

class Application_Model_CheckShopEntireErrorImage extends Zend_Db_Table_Abstract
{
    protected $_name = 'check_shop_entire_error_image';
    protected $_schema = DATABASE_TRADE;

    public function getError($check_shop_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['e' => DATABASE_TRADE.'.check_shop_entire_error_image'], [
                'e.check_shop_id',
                'e.note',
                'error_name' => 'r.name',
                'e.is_repair',
                'e.id',
                'repair_by_title' => "IF(st.title = 640, 'Trade Local', IF(st.title = 183, 'Sales', 'Chưa rõ chức danh'))",
                'repair_by' => "CONCAT(st.firstname, ' ', st.lastname)"
            ])
            ->joinLeft(['r' => DATABASE_TRADE.'.error_image'], 'e.error_image_id = r.id', [])
            ->joinLeft(['st' => 'staff'], 'st.id = e.repair_by', [])
            ->where('e.check_shop_id = ?', $check_shop_id);

        $result = $db->fetchAll($select);

        return $result;
    }

    public function checkErrorShop($check_shop_entire_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['e' => DATABASE_TRADE.'.check_shop_entire_error_image'], [
                         'e.is_repair'
                     ])
        ->where('e.check_shop_id = ?', $check_shop_entire_id);
        
        $result = $db->fetchCol($select);

        if (in_array(0, $result)) {
            $error = 1;
        } else {
            $error = 0;
        }


        return $error;

    }
}    