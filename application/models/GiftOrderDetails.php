<?php
class Application_Model_GiftOrderDetails extends Zend_Db_Table_Abstract
{
    protected $_name = 'gift_order_details';

    function fetchAllData($id){
        $db = Zend_Registry::get('db');

        $cols = array(
            'p.num', 'g.name', 'g.desc', 'g.file', 'g.id'
        );

        $select = $db->select()->from(array('p' => $this->_name), $cols);
        $select->joinLeft(array('g'=>'gift'),'g.id = p.gift_id',array());
        $select->where('order_id = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }
}