<?php
class Application_Model_StoreStaff extends Zend_Db_Table_Abstract
{
	protected $_name = 'store_staff';

    function getStore($staff_id)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('p' => $this->_name),
                    array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*', 'pid' => 'p.id', ));


        if (isset($staff_id) && $staff_id) {
            $select->where('p.staff_id = ?', $staff_id);
        }

        $result = $db->fetchRow($select);

        return $result['store_id'];
    }
	
	function getListStore($staff_id){
    	$db = Zend_Registry::get('db');
    	$select = $db->select()
    	->from(array('p' => $this->_name),
    			array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*', 'store.latitude','store.longitude' ))
    	->joinLeft('store', 'p.store_id = store.id');
    	
    	if (isset($staff_id) && $staff_id) {
    		$select->where('p.staff_id = ?', $staff_id);
    	}
    	
    	$result = $db->fetchAll($select);
    	return $result;
    }

    
}
