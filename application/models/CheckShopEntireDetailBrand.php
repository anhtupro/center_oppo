<?php

class Application_Model_CheckShopEntireDetailBrand extends Zend_Db_Table_Abstract
{
    protected $_name = 'check_shop_entire_detail_brand';
    protected $_schema = DATABASE_TRADE;

    public function getCheckShopDetail($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE.'.check_shop_entire'], [
                'category_name' => 'c.name',
                'd.quantity',
                'brand_name' => 'b.name'
            ])
            ->joinLeft(['d' => DATABASE_TRADE.'.check_shop_entire_detail_brand'], 'a.id = d.check_shop_id', [])
            ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'd.category_id = c.id', [])
            ->joinLeft(['b' => DATABASE_TRADE.'.brand'], 'd.brand_id = b.id', [])
        ->order('b.id');

        if ($params['check_shop_id']) {
            $select->where('a.id = ?', $params['check_shop_id']);
        }

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getDetail($check_shop_id)
    {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'category_id' => 'd.category_id',
            'd.quantity',
            'category_name' => 'c.name',
            'brand_name' => 'b.name',
            'brand_id' => 'b.id'

        );

        $select->from(array('d'=> DATABASE_TRADE.'.check_shop_entire_detail_brand'), $arrCols);
        $select->joinLeft(array('c' => DATABASE_TRADE.'.category'), 'd.category_id = c.id', array());
        $select->joinLeft(array('b' => DATABASE_TRADE . '.brand'), 'd.brand_id = b.id', array());
        $select->where('d.check_shop_id = ?', $check_shop_id);
        $select->where('d.quantity > 0');
        $select->where('d.category_id IS NOT NULL');
        $select->order('b.id');
        $result = $db->fetchAll($select);

        return $result;
    }
}    