<?php

class Application_Model_AppNoti extends Zend_Db_Table_Abstract
{
    protected $_name = 'app_notification';

    public function getNotiTranfer($params)
    {

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.store_from",
            "p.store_to",
            "p.status",
            "status_name" => "status.name",
            "r.area_id",
            "t.title",
            "store_name" => "s.name"
        );

        $select->from(array('p' => DATABASE_TRADE . '.transfer'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_from', array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());
        $select->joinLeft(array('status' => DATABASE_TRADE . '.app_status'), 'status.status = p.status AND status.type = 6', array());
        $select->joinLeft(array('t' => DATABASE_TRADE . '.app_status_title'), 't.status_id = status.id', array());

        if (!empty($params['area_list'])) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        if (!empty($params['title'])) {
            $select->where('t.title = ?', $params['title']);
        }

        $select->order('p.id DESC');

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getNotiRepair($params)
    {

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.store_id",
            "p.status",
            "status_name" => "status.name",
            "r.area_id",
            "t.title",
            "store_name" => "s.name"
        );

        $select->from(array('p' => DATABASE_TRADE . '.repair'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());
        $select->joinLeft(array('status' => DATABASE_TRADE . '.app_status'), 'status.id = p.status', array());
        $select->joinLeft(array('t' => DATABASE_TRADE . '.app_status_title'), 't.status_id = status.id', array());

        if (!empty($params['area_list'])) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        if (!empty($params['title'])) {
            $select->where('t.title = ?', $params['title']);
        }

        $select->order('p.id DESC');

        $result = $db->fetchAll($select);

        if ($_GET['dev'] == 3) {
            echo $select;
            exit;
        }

        return $result;
    }

    public function getNotiDestruction($params)
    {

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.store_id",
            "p.status",
            "status_name" => "status.name",
            "r.area_id",
            "t.title",
            "store_name" => "s.name"
        );

        $select->from(array('p' => DATABASE_TRADE . '.destruction'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());
        $select->joinLeft(array('status' => DATABASE_TRADE . '.app_status'), 'status.id = p.status', array());
        $select->joinLeft(array('t' => DATABASE_TRADE . '.app_status_title'), 't.status_id = status.id', array());

        if (!empty($params['area_list'])) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        if (!empty($params['title'])) {
            $select->where('t.title = ?', $params['title']);
        }

        $select->order('p.id DESC');

        $result = $db->fetchAll($select);

        if ($_GET['dev'] == 2) {
            echo $select;
            exit;
        }

        return $result;
    }

    public function getNotiAir($params)
    {

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "a.id",
            "a.sn",
            "a.created_by",
            "a.store_id",
            "a.created_at",
            "a.status",
            "status_name" => "c.name",
            "created_by" => "CONCAT(b.firstname, ' ', b.lastname)",
            "namestore" => "d.name",
            "t.title"
        );

        $select->from(array('a' => DATABASE_TRADE . '.app_air'), $arrCols);
        $select->joinLeft(array('c' => DATABASE_TRADE . '.app_status'), 'c.status = a.status', array());
        $select->joinLeft(array('b' => 'staff'), 'a.created_by = b.id', array());
        $select->joinLeft(array('d' => 'store'), 'd.id = a.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = d.regional_market', array());
        $select->joinLeft(array('t' => DATABASE_TRADE . '.app_status_title'), 't.status_id = c.id', array());
        $select->where('c.type = ?', 7);

        if (!empty($params['area_list'])) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        if (!empty($params['title'])) {
            $select->where('t.title = ?', $params['title']);
        }

        $select->order('a.id DESC');

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getNotiBantu($params)
    {

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.campaign_id",
            "p.created_at",
            "p.created_by",
            "d.store_id",
            "d.category_id",
            "d.quantity",
            "r.area_id",
            "status" => "MAX(d.`status`)"
        );

        $select->from(array('p' => DATABASE_TRADE . '.app_order'), $arrCols);
        $select->joinLeft(array('d' => DATABASE_TRADE . '.app_order_details'), 'd.order_id = p.id', array());
        $select->joinLeft(array('s' => 'store'), 's.id = d.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());

        $select->group(['p.campaign_id', 'r.area_id']);


        $select_main = $db->select();

        $arrCols_main = [
            "p.id",
            "p.campaign_id",
            "p.created_at",
            "p.created_by",
            "p.area_id",
            "p.status",
            "status_name" => "s.name",
            "t.title"
        ];

        $select_main->from(array('p' => new Zend_Db_Expr('(' . $select . ')')), $arrCols_main);
        $select_main->joinLeft(array('s' => DATABASE_TRADE . '.app_status'), 's.status = p.status AND s.type = 2', array());
        $select_main->joinLeft(array('t' => DATABASE_TRADE . '.app_status_title'), 't.status_id = s.status AND t.type = 2', array());

        if (!empty($params['area_list'])) {
            $select_main->where('p.area_id IN (?)', $params['area_list']);
        }

        if (!empty($params['title'])) {
            $select_main->where('t.title = ?', $params['title']);
        }

        $result = $db->fetchAll($select_main);

        if ($_GET['dev'] == 5) {
            echo $select_main;
            exit;
        }

        return $result;
    }

    public function getNotiPosm($params)
    {

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.campaign_id",
            "p.area_id",
            "p.category_id",
            "status" => "MIN(p.status)",
            "status_name" => "s.name",
            "p.quantity",
            "campaign_name" => "c.name"
        );

        $select->from(array('p' => DATABASE_TRADE . '.campaign_area'), $arrCols);
        $select->joinLeft(array('c' => DATABASE_TRADE . '.campaign_demo'), 'c.id = p.campaign_id', array());
        $select->joinLeft(array('s' => DATABASE_TRADE . '.app_status'), 's.status = p.status AND s.type = 3', array());

        $select->where('p.quantity > 0', NULL);

        $select->group(['p.area_id', 'p.campaign_id']);


        $select_main = $db->select();

        $arrCols_main = [
            "p.campaign_id",
            "p.area_id",
            "p.category_id",
            "p.status",
            "p.status_name",
            "p.quantity",
            "p.campaign_name",
            "s.title"
        ];

        $select_main->from(array('p' => new Zend_Db_Expr('(' . $select . ')')), $arrCols_main);
        $select_main->joinLeft(array('s' => DATABASE_TRADE . '.app_status_title'), 's.status_id = p.status AND s.type = 3', array());

        if (!empty($params['area_list'])) {
            $select_main->where('p.area_id IN (?)', $params['area_list']);
        }

        if (!empty($params['title'])) {
            $select_main->where('s.title = ?', $params['title']);
        }

        $result = $db->fetchAll($select_main);

        return $result;
    }

    public function getNotiCheckshop($params)
    {

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.staff_id",
            "store_name" => "s.name",
            "checkshop_last" => "MAX(c.created_at)",
            "checkshop_id" => 'MAX(c.id)',
            "p.store_id"
        );

        $select->from(array('p' => 'store_staff_log'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('c' => DATABASE_TRADE . '.app_checkshop'), 'c.store_id = s.id', array());

        $select->where('(p.released_at IS NULL)', NULL);
        $select->where('p.staff_id = ?', $params['staff_id']);

        $select->group('s.id');


        $select_main = $db->select();

        $arrCols_main = [
            "p.id",
            "p.staff_id",
            "p.store_name",
            "p.checkshop_last",
            "p.checkshop_id",
            "p.store_id"
        ];

        $select_main->from(array('p' => new Zend_Db_Expr('(' . $select . ')')), $arrCols_main);

        $select_main->where('p.checkshop_last < ( CURDATE() - INTERVAL 15 DAY ) OR p.checkshop_last IS NULL', NULL);

        $result = $db->fetchAll($select_main);

        return $result;

    }

    function send_notification($user_token, $msg_content)
    {

        $url = 'https://fcm.googleapis.com/fcm/send';
		$my_key = 'AAAA8TRDzYk:APA91bF4AK4SiTJMGr8jG672KG5ykN_9cNB0b8RJL3HXGUD3RxYJwFlFsgPvC7fR44c-NeWbZ8tf88SJcC_cX5bqjsEz9ofSWyaiTHGSBlb0Qxs2bpNLJesGFkXeUKZfsN42mK-fSkfJ';
        // $my_key = 'AAAAZh3rgvU:APA91bH9shmV81oukozDBtLEBS0A2UUiQ-iKPmgCDyjQghq7Uh4zYPm8pEOlBqAryGVzk7vqGykiNUSiMr14WOKSdDkBSTq0a3-FiX9-x_toVRg_sKmMZPJK7b3bJ33sAWy64g2AXBoa';
        // $my_key = 'AAAAZh3rgvU:APA91bEzKoYJRb0o0uu8Wet_osA0rnrZUzan9102sVr_VqI1RKIEdnOgVo1vdWb1ahXkyxXrzmqv73EkP9q-nfwuc7nJ3OobXZsscy3slC6ou4RTqUycSR_XGwaHHeeVlSr7X3GgJOKc';
        $fields = array(
            "to" => $user_token,
            "priority" => "high",
            "notification"  =>      array(
                   "title"             =>  "Bạn có 1 thông báo từ OPPO Trade Marketing",
                   "body"              =>  $msg_content,
                   "sound"             =>  "default",
                   "click_action"      =>  "FCM_PLUGIN_ACTIVITY",
                   "icon"              =>  "fcm_push_icon",
             ),
            "data" => array(
                "title" => "Bạn có 1 thông báo từ OPPO Trade Marketing",
                "message" => $msg_content,
                "count" => 8,
            ),
        );
        $fields = json_encode($fields);
        $headers = array(
            'Authorization: key=' . $my_key,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        $result = curl_exec($ch);
        curl_close($ch);

        return $result;

    }


    public function getPendingRepair($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
//            "p.store_id", 
//            "p.status", 
//            "p.create_at", 
//            "p.staff_id", 
//            "r.area_id", 
//            "a.name", 
//            "t.title" 
        );

        $select->from(array('p' => $params['table']), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.' . $params['store_id'], array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());
        $select->joinLeft(array('a' => DATABASE_TRADE . '.app_status'), 'a.status = p.status AND a.type = ' . $params['type'], array());
        $select->joinLeft(array('t' => DATABASE_TRADE . '.app_status_title'), 't.status_id = p.status AND t.type = '. $params['type'], array());

        $select->where('p.remove IS NULL OR p.remove = 0');
        $select->where('p.reject IS NULL OR p.reject = 0');

        if (!empty($params['area_list'])) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        if (!empty($params['title'])) {
            $select->where('t.title = ?', $params['title']);
        }

        if (!empty($params['type'])) {
            $select->where('a.type = ?', $params['type']);
            $select->where('t.type = ?', $params['type']);
        }

        $select->group('p.id');

        $select->order('p.id DESC');
        
        $result = $db->fetchAll($select);

        return $result;
    }

    public function getSuccessRepair($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "success" => "IFNULL(SUM(IF(p.status = a.status, 1, 0)),0)",
            "pending" => "IFNULL(SUM(IF(p.status <> a.status, 1, 0)),0)",
            "max_status" => "a.status"
        );

        $nestedSelect = "SELECT MAX(status) `status` FROM `" . DATABASE_TRADE . "`.`app_status` WHERE type = " . $params['type'];

        $select->from(array('p' => $params['table']), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.' . $params['store_id'], array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());
        $select->joinCross(array('a' => new Zend_Db_Expr('(' . $nestedSelect . ')')), NULL, NULL);

        $select->where('p.remove IS NULL OR p.remove = 0');
        $select->where('p.reject IS NULL OR p.reject = 0');

        if (!empty($params['area_list'])) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        if (!empty($params['staff_id']) AND !empty($params['created_by'])) {
            $select->where('p.' . $params['created_by'] . ' = ?', $params['staff_id']);
        }

        $select->order('p.id DESC');

        $result = $db->fetchRow($select);

        return $result;
    }

    public function getCheckshopInfo($params)
    {
        if (empty($params['staff_id']) AND empty($params['area_id']) AND empty($params['list_area'])) {
            return false;
        }


        $sub_select = "
        SELECT s.name, p.store_id, p.updated_at, p.last_updated_at, COUNT(DISTINCT p.last_updated_at) eligible
        FROM
        (
                select
                                p.store_id,
                                p.updated_at,
                                @last_updated_at,
                                @last_updated_at := IF(@last_store_id <> p.store_id, DATE_FORMAT(p.updated_at, '%Y-%m-%d'), IF( DATE_FORMAT(p.updated_at, '%Y-%m-%d') > DATE_ADD(@last_updated_at, INTERVAL +2 DAY) , DATE_FORMAT(p.updated_at, '%Y-%m-%d'), @last_updated_at )) AS last_updated_at,
                                @last_store_id := p.store_id
                from
                                `trade_marketing`.app_checkshop p,
                                ( select @last_store_id := 0, @last_updated_at := 0 ) i
                WHERE MONTH(p.updated_at) = MONTH(CURRENT_DATE()) AND YEAR(p.updated_at) = YEAR(CURRENT_DATE())
                order by p.store_id,p.updated_at ASC
        ) p
        LEFT JOIN store s ON s.id = p.store_id
        GROUP BY p.store_id
        ";


        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'total_store' => 'COUNT(DISTINCT p.store_id)',
            'total_store_check' => 'SUM( IF( e.eligible > 0, 1, 0 ) )',
            'total_checkshop' => "SUM(e.eligible)",
            'dc_success' => "SUM( IF( (l.loyalty_plan_id IN (1,2,3) AND e.eligible > 3), 1, 0 ) )",
            'dc_pending' => 'SUM( IF( (l.loyalty_plan_id IN (1,2,3) AND (e.eligible <= 3 OR e.eligible IS NULL)), 1, 0 ) )',
        );

        if (!empty($params['staff_id'])) {

            $QStaff = new Application_Model_Staff();
            $where = $QStaff->getAdapter()->quoteInto('id = ?', $params['staff_id']);
            $staff = $QStaff->fetchRow($where);

            if ($staff['title'] == SALES_LEADER_TITLE) {
                $select->from(array('p' => 'store_leader_log'), $arrCols);
            } else {
                $select->from(array('p' => 'store_staff_log'), $arrCols);
            }

        } else {
            $select->from(array('p' => 'v_store_staff_leader_log'), $arrCols);
        }

        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 's.regional_market = r.id', array());
        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = s.d_id', array());
        $select->joinLeft(array('l' => 'dealer_loyalty'), 'l.dealer_id = (IF((d.parent = 0 OR d.parent IS NULL), d.id, d.parent)) AND l.is_last = 1', array());
        $select->joinLeft(array('e' => new Zend_Db_Expr('(' . $sub_select . ')')), 'e.store_id = s.id', array());


        $select->where('p.released_at IS NULL AND p.is_leader IN (1,8) ', NULL);
        $select->where('s.del is NULL or s.del= 0', NULL);

        if (!empty($params['staff_id'])) {
            $select->where('p.staff_id = ?', $params['staff_id']);
        }

        if (!empty($params['area_id'])) {
            $select->where('r.area_id IN (?)', $params['area_id']);
        }

        if (!empty($params['area_list'])) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getDestructionCost($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                    ->from(['d' => DATABASE_TRADE . '.destruction'], [
                        'total_cost' => 'SUM(q.total_price) + SUM(q.total_price) * 0.1'
                    ])
                    ->join(['s' => 'store'], 'd.store_id = s.id', [])
                    ->joinLeft(['r' => 'regional_market'], 's.regional_market = r.id', [])
                    ->joinLeft(['q' => DATABASE_TRADE . '.destruction_price'], 'd.id = q.destruction_id AND q.status = (
                          SELECT MAX(status) FROM trade_marketing.destruction_price where destruction_id = d.id AND status <> 3 AND status <> 1 AND status <> 5
                      )', [])
                    ->where('d.remove =  0 OR d.remove is null')
                    ->where('q.id IS NOT NULL');


        if ($params['status_finish']) {
            $select->where('d.status = ?', $params['status_finish']);
        }

        if ($params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        if ($params['area_id']) {
            $select->where('r.area_id = ?', $params['area_id']);
        }

        if(!empty($params['staff_id'])){
            $select->where('d.staff_id = ?', $params['staff_id']);
        }

        if ($params['month']) {
            $select->where('MONTH(d.create_at) = ?', $params['month']);
        }

        if ($params['year']) {
            $select->where('YEAR(d.create_at) = ?', $params['year']);
        }

        if ($params['from_date']) {
            $select->where("DATE_FORMAT(d.create_at,'%Y-%m-%d') >= ?", $params['from_date']);
        }

        if ($params['to_date']) {
            $select->where("DATE_FORMAT(d.create_at,'%Y-%m-%d') <= ?", $params['to_date']);
        }

        $result = $db->fetchOne($select);

        return $result;
    }

    public function getTransferCost($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                    ->from(['d' => DATABASE_TRADE . '.transfer'], [
                        'total_cost' => 'SUM(q.total_price) + SUM(q.total_price) * 0.1'
                    ])
                    ->join(['s' => 'store'], 'd.store_from = s.id', [])
                    ->joinLeft(['r' => 'regional_market'], 's.regional_market = r.id', [])
                    ->joinLeft(['q' => DATABASE_TRADE . '.transfer_price'], 'd.id = q.transfer_id AND q.status = (
                        SELECT MAX(status) FROM trade_marketing.transfer_price where transfer_id = d.id AND status <> 3 AND status <> 1 AND status <> 5
                     )', [])
                    ->where('d.remove =  0 OR d.remove is null')
                    ->where('q.id IS NOT NULL');


        if ($params['status_finish']) {
            $select->where('d.status = ?', $params['status_finish']);
        }

        if ($params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        if ($params['area_id']) {
            $select->where('r.area_id = ?', $params['area_id']);
        }

        if(!empty($params['staff_id'])){
            $select->where('d.staff_id = ?', $params['staff_id']);
        }

        if ($params['month']) {
            $select->where('MONTH(d.created_at) = ?', $params['month']);
        }

        if ($params['year']) {
            $select->where('YEAR(d.created_at) = ?', $params['year']);
        }

        if ($params['from_date']) {
            $select->where("DATE_FORMAT(d.created_at,'%Y-%m-%d') >= ?", $params['from_date']);
        }

        if ($params['to_date']) {
            $select->where("DATE_FORMAT(d.created_at,'%Y-%m-%d') <= ?", $params['to_date']);
        }

        $result = $db->fetchOne($select);

        return $result;
    }

    public function getRepairCost($params)
    {
        $minDate = '2019-04-01';

        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['r' => DATABASE_TRADE . '.repair'], [
                         'cost_ia_not_brandshop' => "IF (d.is_ka = 0 AND d.is_company_brandshop = 0, SUM(total_price) + SUM(total_price) * 0.1, 0)",
                         'cost_ia_brandshop' => "IF (d.is_ka = 0 AND d.is_company_brandshop = 1, SUM(total_price) + SUM(total_price) * 0.1, 0)",
                         'cost_ka_not_brandshop' => "IF (d.is_ka = 1 AND d.is_company_brandshop = 0, SUM(total_price) + SUM(total_price) * 0.1, 0)",
                         'cost_ka_brandshop' => "IF (d.is_ka = 1 AND d.is_company_brandshop = 1, SUM(total_price) + SUM(total_price) * 0.1, 0)"
                     ])
                    ->join(['s' => 'store'], 'r.store_id = s.id', [])
                    ->joinLeft(['re' => 'regional_market'], 's.regional_market = re.id', [])
                    ->joinLeft(['d' => WAREHOUSE_DB.'.distributor'], 's.d_id = d.id', [])
                    ->joinLeft(['detail' => DATABASE_TRADE.'.repair_details'], 'r.id = detail.repair_id', [])
                    ->joinLeft(['q' => DATABASE_TRADE.'.repair_quotation'], 'detail.id = q.repair_details_id AND q.status = (
                                select MAX(status) from trade_marketing.repair_quotation where repair_details_id = detail.id AND status <> 3 AND status <> 1 AND status <> 5
                                )', [])
                    ->where('r.remove = 0 OR r.remove IS NULL')
                    ->where('q.id IS NOT NULL')
                    ->group(['d.is_ka', 'd.is_company_brandshop']);


        if ($params['status_finish']) {
            $select->where('r.status = ?', $params['status_finish']);
        }

        if ($params['area_list']) {
            $select->where('re.area_id IN (?)', $params['area_list']);
        }

        if ($params['area_id']) {
            $select->where('re.area_id = ?', $params['area_id']);
        }

        if(!empty($params['staff_id'])){
            $select->where('r.staff_id = ?', $params['staff_id']);
        }

        if ($params['month']) {
            $select->where('MONTH(r.create_at) = ?', $params['month']);
        }

        if ($params['year']) {
            if ($params['year'] == 2019) {
                $months = [4,5,6,7,8,9,10,11,12];
                $select->where('MONTH(r.create_at) IN (?)', $months);
            }
            $select->where('YEAR(r.create_at) = ?', $params['year']);
        }

        if ($params['from_date']) {
            if ($params['from_date'] >= '2019-01-01' && $params['from_date'] <= '2019-03-31') {
                $select->where("DATE_FORMAT(r.create_at,'%Y-%m-%d') >= ?", $minDate);
                $select->where("DATE_FORMAT(r.create_at,'%Y-%m-%d') <= ?", $params['to_date']);
            } else {
                $select->where("DATE_FORMAT(r.create_at,'%Y-%m-%d') >= ?", $params['from_date']);
            }
        }

        if ($params['to_date']) {
            if ($params['from_date'] >= '2019-01-01' && $params['from_date'] <= '2019-03-31') {
                $select->where("DATE_FORMAT(r.create_at,'%Y-%m-%d') >= ?", $minDate);
                $select->where("DATE_FORMAT(r.create_at,'%Y-%m-%d') <= ?", $params['to_date']);
            } else {
                $select->where("DATE_FORMAT(r.create_at,'%Y-%m-%d') <= ?", $params['to_date']);
            }
        }

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $repairCost ['ka']  += $element ['cost_ka_not_brandshop'] + $element ['cost_ka_brandshop'];
            $repairCost ['brandshop']  += $element ['cost_ka_brandshop'] + $element ['cost_ia_brandshop'];
            $repairCost ['ia']  += $element ['cost_ia_not_brandshop'] + $element ['cost_ia_brandshop'];
        }

        return $repairCost;


    }

    public function getAirCost($params)
    {
        $minDate = '2019-04-01';

        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['a' => DATABASE_TRADE.'.app_air'], [
                         'cost' => 'SUM(q.total_price) + SUM(q.total_price) * 0.1'
                     ])
                    ->join(['s' => 'store'], 'a.store_id = s.id', [])
                    ->joinLeft(['r' => 'regional_market'], 's.regional_market = r.id', [])
                    ->joinLeft(['d' => DATABASE_TRADE.'.air_details'], 'a.id = d.air_id', [])
                    ->joinLeft(['q' => DATABASE_TRADE.'.air_quotation'], 'd.id = q.air_details_id AND q.status = (
                                select MAX(status) from '.DATABASE_TRADE.'.air_quotation where air_details_id = d.id AND status <> 3 AND status <> 1 AND status <> 5
                                )', [])
                    ->where('a.remove IS NULL OR a.remove = 0');

        if ($params['status_finish']) {
            $select->where('a.status = ?', $params['status_finish']);
        }

        if ($params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        if ($params['area_id']) {
            $select->where('r.area_id = ?', $params['area_id']);
        }

        if(!empty($params['staff_id'])){
            $select->where('a.created_by = ?', $params['staff_id']);
        }

        if ($params['month']) {
            $select->where('MONTH(a.created_at) = ?', $params['month']);
        }

        if ($params['year']) {
            if ($params['year'] == 2019) {
                $months = [4,5,6,7,8,9,10,11,12];
                $select->where('MONTH(a.created_at) IN (?)', $months);
            }

            $select->where('YEAR(a.created_at) = ?', $params['year']);
        }

        if ($params['from_date']) {
            if ($params['from_date'] >= '2019-01-01' && $params['from_date'] <= '2019-03-31') {
                $select->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') >= ?", $minDate);
                $select->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') <= ?", $params['to_date']);
            } else {
                $select->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') >= ?", $params['from_date']);
            }
        }

        if ($params['to_date']) {
            if ($params['from_date'] >= '2019-01-01' && $params['from_date'] <= '2019-03-31') {
                $select->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') >= ?", $minDate);
                $select->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') <= ?", $params['to_date']);
            } else {
                $select->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') <= ?", $params['to_date']);
            }
        }

        $result = $db->fetchOne($select);

        return $result;
    }

    public function getAirCostYear($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE.'.app_air'], [
                'cost' => 'IFNULL(d.final_price, d.price)'
            ])
            ->join(['s' => 'store'], 'a.store_id = s.id', [])
            ->joinLeft(['r' => 'regional_market'], 's.regional_market = r.id', [])
            ->joinLeft(['d' => DATABASE_TRADE.'.air_details'], 'a.id = d.air_id', [])
            ->where('a.remove IS NULL OR a.remove = 0');

        if ($params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        if ($params['area_id']) {
            $select->where('r.area_id = ?', $params['area_id']);
        }

        if(!empty($params['staff_id'])){
            $select->where('a.created_by = ?', $params['staff_id']);
        }

        if ($params['month']) {
            $select->where('MONTH(a.created_at) = ?', $params['month']);
        }

        if ($params['year']) {
            $months = [4,5,6,7,8,9,10,11,12];
            $select->where('MONTH(a.created_at) IN (?)', $months);
            $select->where('YEAR(a.created_at) = ?', $params['year']);
        }

//        echo $select->__toString();
//        die;
        $result = $db->fetchAll($select);


        foreach ($result as $element) {
            $airCost += $element['cost'];
        }

        return $airCost;
    }


    public function getPosmCost($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['d' => DATABASE_TRADE.'.campaign_demo'], ['SUM(ca.quantity * cc.price)'])
                     ->joinLeft(['ca' => DATABASE_TRADE.'.campaign_area'], 'd.id = ca.campaign_id', [])
                     ->joinLeft(['cc' => DATABASE_TRADE.'.contractor_category'], 'ca.campaign_id = cc.campaign_id AND ca.category_id = cc.category_id AND ca.area_id = cc.area_id', [])
                     ->where('ca.quantity <> 0')
                     ->where('cc.price <> 0');

        if ($params['area_list']) {
            $select->where('ca.area_id IN (?)', $params['area_list']);
        }

        if ($params['area_id']) {
            $select->where('ca.area_id = ?', $params['area_id']);
        }

        if ($params['month']) {
            $select->where('MONTH(d.created_at) = ?', $params['month']);
        }

        if ($params['year']) {
            $select->where('YEAR(d.created_at) = ?', $params['year']);
        }

        if ($params['from_date']) {
            $select->where("DATE_FORMAT(d.from,'%Y-%m-%d') >= ?", $params['from_date']);
        }

        if ($params['to_date']) {
            $select->where("DATE_FORMAT(d.from,'%Y-%m-%d') <= ?", $params['to_date']);
        }

        if ($params['cost_type']) {
            $select->where('d.cost_type = ?', $params['cost_type']);
        }

        $result = $db->fetchOne($select);

        return $result;
            
    }

    public function getPosmCostYear($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['d' => DATABASE_TRADE.'.campaign_demo'], ['SUM(ca.quantity * cc.price)'])
            ->joinLeft(['ca' => DATABASE_TRADE.'.campaign_area'], 'd.id = ca.campaign_id', [])
            ->joinLeft(['cc' => DATABASE_TRADE.'.contractor_category'], 'ca.campaign_id = cc.campaign_id AND ca.category_id = cc.category_id AND ca.area_id = cc.area_id', [])
            ->where('ca.quantity <> 0')
            ->where('cc.price <> 0');

        if ($params['area_list']) {
            $select->where('ca.area_id IN (?)', $params['area_list']);
        }

        if ($params['area_id']) {
            $select->where('ca.area_id = ?', $params['area_id']);
        }

        if ($params['year']) {
            $select->where('YEAR(d.created_at) = ?', $params['year']);
        }

        if ($params['cost_type']) {
            $select->where('d.cost_type = ?', $params['cost_type']);
        }

        if ($params['cost_type']) {
            $select->where('d.cost_type = ?', $params['cost_type']);
        }

        $result = $db->fetchOne($select);

        return $result;

    }

    public function getAdditionalPosmCost($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['c' => DATABASE_TRADE.'.category_additional'], [
                'SUM(p.cost)'
            ])
            ->joinLeft(['p' => DATABASE_TRADE.'.additional_posm_cost'], 'c.id = p.category_id_additional', [])
            ->where('p.is_deleted = 0');


        if ($params['area_list']) {
            $select->where('p.area_id IN (?)', $params['area_list']);
        }

        if ($params['area_id']) {
            $select->where('p.area_id = ?', $params['area_id']);
        }

        if ($params['season']) {
            $select->where('p.season = ?', $params['season']);
        }

        if ($params['year']) {
            $select->where('p.year = ?', $params['year']);
        }

        if ($params['from_date']) {
            if ($params['from_date'] >= '2019-01-01' && $params['from_date'] <= '2019-03-31') {
                $select->where("p.date BETWEEN '2019-01-01' AND '2019-03-31'");
            } else {
                $select->where('p.date >= ?', $params['from_date']);
            }
        }

        if ($params['to_date']) {
            if ($params['to_date'] >= '2019-01-01' && $params['to_date'] <= '2019-03-31') {
                $select->where("p.date BETWEEN '2019-01-01' AND '2019-03-31'");
            } else {
                $select->where('p.date <= ?', $params['to_date']);
            }
        }

        $result = $db->fetchOne($select);

        return $result;
    }



    public function getOrderCost($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['p' => DATABASE_TRADE.'.additional_part'], [
                        'cost' => 'SUM(c.cost)'
                     ])
        ->joinLeft(['c' => DATABASE_TRADE.'.additional_part_cost'], 'p.id = c.additional_part_id', [])
        ->where('p.type = ?', 2);

        if ($params['month']) {
            $select->where('c.month = ?', $params['month']);
        }

        if ($params['year']) {
            $select->where('c.year = ?', $params['year']);
        }

        if ($params['from_date']) {
            $select->where("c.date >= ?", $params['from_date']);
        }

        if ($params['to_date']) {
            $select->where("c.date <= ?", $params['to_date']);
        }

        if ($params['area_list']) {
            $select->where('c.area_id IN (?)', $params['area_list']);
        }

        if ($params['area_id']) {
            $select->where('c.area_id = ?', $params['area_id']);
        }

        $result = $db->fetchOne($select);

        return $result;
    }

    public function getAdditionalCost($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE.'.additional_part_cost'], [
                "SUM(a.cost)"
            ])
            ->joinLeft(['p' => DATABASE_TRADE.'.additional_part'], 'a.additional_part_id = p.id', []);

        if ($params['additional_part_type']) {
            $select->where('p.type = ?', $params['additional_part_type']);
        }

        if ($params['season']) {
            $select->where('a.season = ?', $params['season']);
        }

        if ($params['year']) {
            $select->where('a.year = ?', $params['year']);
        }

        if ($params['month']) {
            $select->where('a.month = ?', $params['month']);
        }

//        if ($params['from_date']) {
//            $select->where('a.date >= ?', $params['from_date']);
//        }
//
//        if ($params['to_date']) {
//            $select->where('a.date <= ?', $params['to_date']);
//        }


        if ($params['from_date']) {
            if ($params['from_date'] <= '2019-03-31' && $params['to_date'] <= '2019-03-31') {
                $select->where("a.date BETWEEN '2019-01-01' AND '2019-03-31'");
            } else {
                $select->where('a.date >= ?', $params['from_date']);
            }
        }

        if ($params['to_date']) {
            if ($params['from_date'] <= '2019-03-31' && $params['to_date'] <= '2019-03-31') {
                $select->where("a.date BETWEEN '2019-01-01' AND '2019-03-31'");
            } else {
                $select->where('a.date <= ?', $params['to_date']);
            }
        }

        if ($params['area_list']) {
            $select->where('a.area_id IN (?)', $params['area_list']);
        }

        if ($params['area_id']) {
            $select->where('a.area_id = ?', $params['area_id']);
        }

        $result = $db->fetchOne($select);

        return $result;
    }

    public function getAdditionalCostSeason($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE.'.additional_part_cost'], [
                "SUM(a.cost)"
            ])
            ->joinLeft(['p' => DATABASE_TRADE.'.additional_part'], 'a.additional_part_id = p.id', []);

        if ($params['additional_part_type']) {
            $select->where('p.type = ?', $params['additional_part_type']);
        }

        if ($params['season']) {
            $select->where('a.season = ?', $params['season']);
        }

        if ($params['year']) {
            $select->where('a.year = ?', $params['year']);
        }

        if ($params['month']) {
            $select->where('a.month = ?', $params['month']);
        }

        if ($params['area_list']) {
            $select->where('a.area_id IN (?)', $params['area_list']);
        }

        if ($params['area_id']) {
            $select->where('a.area_id = ?', $params['area_id']);
        }

        $result = $db->fetchOne($select);

        return $result;
    }

    public function getOrderCostYear($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE.'.additional_part_cost'], [
                "SUM(a.cost)"
            ])
            ->joinLeft(['p' => DATABASE_TRADE.'.additional_part'], 'a.additional_part_id = p.id', [])
           ->where('p.type = ?', 2 );

        if ($params['year']) {
            $select->where('a.year = ?', $params['year']);
        }

        if ($params['area_list']) {
            $select->where('a.area_id IN (?)', $params['area_list']);
        }

        if ($params['area_id']) {
            $select->where('a.area_id = ?', $params['area_id']);
        }

        $result = $db->fetchOne($select);

        return $result;
    }

    public function getAdditionalPosmCostSeason($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['c' => DATABASE_TRADE.'.category_additional'], [
                'SUM(p.cost)'
            ])
            ->joinLeft(['p' => DATABASE_TRADE.'.additional_posm_cost'], 'c.id = p.category_id_additional', [])
            ->where('p.is_deleted = 0');


        if ($params['area_list']) {
            $select->where('p.area_id IN (?)', $params['area_list']);
        }

        if ($params['area_id']) {
            $select->where('p.area_id = ?', $params['area_id']);
        }

        if ($params['season']) {
            $select->where('p.season = ?', $params['season']);
        }

        if ($params['year']) {
            $select->where('p.year = ?', $params['year']);
        }

        $result = $db->fetchOne($select);

        return $result;
    }


    public function getStatisticCategory($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['s' => 'store'], [
                        'd.category_id',
                         'quantity' => 'SUM(d.quantity)'
                     ])
                    ->join(['a' => DATABASE_TRADE.'.app_checkshop'], 's.id = a.store_id AND a.is_lock = 1', [])
                    ->join(['d' => DATABASE_TRADE.'.app_checkshop_detail'], 'a.id = d.checkshop_id', [])
                    ->joinLeft(['r' => 'regional_market'], 's.regional_market = r.id', [])
                    ->where('s.del IS NULL OR s.del = 0')
                   ->group(['d.category_id']);

        if ($params['area_id']) {
            $select->where('r.area_id = ?', $params['area_id']);
        }
        if ($params['list_area']) {
            $select->where('r.area_id IN (?)', $params['list_area']);
        }

        if ($params['staff_id']) {
            $userStorage    = Zend_Auth::getInstance()->getStorage()->read();
            if ($userStorage->title == SALES_LEADER_TITLE) {
                $select->joinLeft(array('p' => 'store_leader_log'), 'p.store_id = s.id');
                $select->where('p.staff_id = ?', $params['staff_id']);
                $select->where('p.released_at IS NULL ');

            } else {
                $select->joinLeft(array('p' => 'store_staff_log'),  'p.store_id = s.id');
                $select->where('p.staff_id = ?', $params['staff_id']);
                $select->where('p.released_at IS NULL ');
            }

        }

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['category_id']] = $element['quantity'];
        }

        return $list;

    }

    public function getOrderAirCost($params = Null)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['s' => DATABASE_TRADE.'.order_air_stage'], [
                         'total' => 'SUM(d.price * a.order_quantity)'
                     ])
                    ->joinLeft(['d' => DATABASE_TRADE.'.order_air_detail'], 's.id = d.order_air_stage_id', [])
                    ->joinLeft(['a' => DATABASE_TRADE.'.order_air_area'], 'd.id = a.order_air_detail_id', [])
                   ->joinLeft(['t' => DATABASE_TRADE.'.order_air_area_status'], 's.id = t.order_air_stage_id AND a.area_id = t.area_id', [])
//                     ->where('t.status > ?', 2)
                   ->where('s.is_deleted IS NULL')
                    ->where('a.order_quantity > ?', 0);

        if ($params['area_list']) {
            $select->where('a.area_id IN (?)', $params['area_list']);
        }
        if ($params['area_id']) {
            $select->where('a.area_id = ?', $params['area_id']);
        }
        if ($params['from_date']) {
            $select->where("DATE_FORMAT(s.to_date,'%Y-%m-%d') >= ?", $params['from_date']);
        }

        if ($params['to_date']) {
            $select->where("DATE_FORMAT(s.to_date,'%Y-%m-%d') <= ?", $params['to_date']);
        }
        $result = $db->fetchOne($select);

        return $result;


    }
    
    function sendNotification($data){
        
        /* data mẫu
        $data_noti = [
            'title' => "Bạn có 1 thông báo từ OPPO Trade Marketing",
            'message' => "Message Info",
            'link' => "/trade/view-checkshop?id=3",
            'staff_id' => 5899
        ];
        */
        
        $QAppNoti = new Application_Model_AppNoti();
        $QStaff = new Application_Model_Staff();
        $url = 'https://fcm.googleapis.com/fcm/send';
        $my_key = 'AAAA8TRDzYk:APA91bF4AK4SiTJMGr8jG672KG5ykN_9cNB0b8RJL3HXGUD3RxYJwFlFsgPvC7fR44c-NeWbZ8tf88SJcC_cX5bqjsEz9ofSWyaiTHGSBlb0Qxs2bpNLJesGFkXeUKZfsN42mK-fSkfJ';
        
        $result = [
            'success' => 1,
            'message' => 'Done'
        ];
        
        $db = Zend_Registry::get('db');
        try {
//            $db->beginTransaction();
            
            $id = $QAppNoti->insert($data);
            $staff = $QStaff->fetchRow(['id = ?' => $data['staff_id']]);
            
            if(empty($staff['token_erp_app'])){
                $result = [
                    'success' => 2,
                    'message' => 'Staff is not use App.' . 'id = ' . $staff['id']
                ];
                return $result;
            }
            
            $user_token = $staff['token_erp_app'];
            $fields = array(
                "to" => $user_token,
                "priority" => "high",
                "notification"  =>      array(
                       "title"             =>  $data['title'],
                       "body"              =>  $data['message'],
                       "sound"             =>  "default",
                       "click_action"      =>  "FCM_PLUGIN_ACTIVITY",
                       "icon"              =>  "fcm_push_icon",
                                       'badge' => 2,
                ),
                "data" => array(
                    "title"     => $data['title_name'],
                    "message"   => $data['message'],
                    "count" => 8,
                                    "id_notification" => $id,
                                    "user_token" => $user_token,
                                    "forceStart" => "1"
                ),
            );
            $fields = json_encode($fields);
            $headers = array(
                'Authorization: key=' . $my_key,
                'Content-Type: application/json'
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

            $send_result = curl_exec($ch);
            curl_close($ch);
            
            $send_result = (array)json_decode($send_result);
            
            $result = [
                'success' => $send_result['success'],
                'message' => json_encode($send_result)
            ];
            
//            $db->commit();
            
            return $result;
           
        } catch (Exception $e) {

            $db->rollback();
            
            $result = [
                'success' => 0,
                'message' => $e->getMessage()
            ];
            return $result;
        }
    }
    
    function sendNotificationErp($data){
        
        /* data mẫu
        $data_noti = [
            'title' => "Bạn có 1 thông báo từ OPPO Trade Marketing",
            'message' => "Message Info",
            'link' => "/trade/view-checkshop?id=3",
            'staff_id' => 5899
        ];
        */
        
        $QAppNoti = new Application_Model_AppNoti();
        $QStaff = new Application_Model_Staff();
        $url = 'https://fcm.googleapis.com/fcm/send';
        $my_key = 'AAAALwRnKhM:APA91bHqb_rWFGzIyqlPSuk0ylIz7cY6ViUu8aZ8-a7wOt37w9nZLbQk2fh_Zy6K2Ym_qetq0jKo4yOznZEpPH-aRgar2SSejwNKNfwDRcT7N469Uy5U1FaDAAXq4KYyvHGS8GxQRKSx';
        
        $result = [
            'success' => 1,
            'message' => 'Done'
        ];
        
        $db = Zend_Registry::get('db');
        try {
//            $db->beginTransaction();
            
            $id = $QAppNoti->insert($data);
            $staff = $QStaff->fetchRow(['id = ?' => $data['staff_id']]);
            
            if(empty($staff['token_erp_app'])){
                $result = [
                    'success' => 2,
                    'message' => 'Staff is not use App.' . 'id = ' . $staff['id']
                ];
                return $result;
            }
            
            $user_token = $staff['token_erp_app'];
            $fields = array(
                "to" => $user_token,
                "priority" => "high",
                "notification"  =>      array(
                       "title"             =>  $data['title'],
                       "body"              =>  $data['message'],
                       "sound"             =>  "default",
                       "click_action"      =>  "FCM_PLUGIN_ACTIVITY",
                       "icon"              =>  "fcm_push_icon",
                                       'badge' => 2,
                ),
                "data" => array(
                    "title"     => $data['title_name'],
                    "message"   => $data['message'],
                    "count" => 8,
                                    "id_notification" => $id,
                                    "user_token" => $user_token,
                                    "forceStart" => "1"
                ),
            );
            $fields = json_encode($fields);
            $headers = array(
                'Authorization: key=' . $my_key,
                'Content-Type: application/json'
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

            $send_result = curl_exec($ch);
            curl_close($ch);
            
            $send_result = (array)json_decode($send_result);
            
            $result = [
                'success' => $send_result['success'],
                'message' => json_encode($send_result)
            ];
            
//            $db->commit();
            
            return $result;
           
        } catch (Exception $e) {

            $db->rollback();
            
            $result = [
                'success' => 0,
                'message' => $e->getMessage()
            ];
            return $result;
        }
    }
    
    function sendNotificationNoTransaction($data){
        
        /* data mẫu
        $data_noti = [
            'title' => "Bạn có 1 thông báo từ OPPO Trade Marketing",
            'message' => "Message Info",
            'link' => "/trade/view-checkshop?id=3",
            'staff_id' => 5899
        ];
        */
        
        $QAppNoti = new Application_Model_AppNoti();
        $QStaff = new Application_Model_Staff();
        $url = 'https://fcm.googleapis.com/fcm/send';
        $my_key = 'AAAA8TRDzYk:APA91bF4AK4SiTJMGr8jG672KG5ykN_9cNB0b8RJL3HXGUD3RxYJwFlFsgPvC7fR44c-NeWbZ8tf88SJcC_cX5bqjsEz9ofSWyaiTHGSBlb0Qxs2bpNLJesGFkXeUKZfsN42mK-fSkfJ';
        
        $result = [
            'success' => 1,
            'message' => 'Done'
        ];
            
        $id = $QAppNoti->insert($data);
        $staff = $QStaff->fetchRow(['id = ?' => $data['staff_id']]);

        if(empty($staff['token_erp_app'])){
            $result = [
                'success' => 2,
                'message' => 'Staff is not use App.' . 'id = ' . $staff['id']
            ];
            return $result;
        }

        $user_token = $staff['token_erp_app'];
        $fields = array(
            "to" => $user_token,
            "priority" => "high",
            "notification"  =>      array(
                   "title"             =>  $data['title'],
                   "body"              =>  $data['message'],
                   "sound"             =>  "default",
                   "click_action"      =>  "FCM_PLUGIN_ACTIVITY",
                   "icon"              =>  "fcm_push_icon",
                                   'badge' => 2,
            ),
            "data" => array(
                "title"     => $data['title_name'],
                "message"   => $data['message'],
                "count" => 8,
                                "id_notification" => $id,
								"user_token" => $user_token,
                                "forceStart" => "1"
            ),
        );
        $fields = json_encode($fields);
        $headers = array(
            'Authorization: key=' . $my_key,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        $send_result = curl_exec($ch);
        curl_close($ch);

        $send_result = (array)json_decode($send_result);

        $result = [
            'success' => $send_result['success'],
            'message' => json_encode($send_result)
        ];

        return $result;
    }
    
    function sendNotificationNoTransactionERP($data){
        
        /* data mẫu
        $data_noti = [
            'title' => "Bạn có 1 thông báo từ OPPO Trade Marketing",
            'message' => "Message Info",
            'link' => "/trade/view-checkshop?id=3",
            'staff_id' => 5899
        ];
        */
        
        $QAppNoti = new Application_Model_AppNoti();
        $QStaff = new Application_Model_Staff();
        $url = 'https://fcm.googleapis.com/fcm/send';
        $my_key = 'AAAALwRnKhM:APA91bHqb_rWFGzIyqlPSuk0ylIz7cY6ViUu8aZ8-a7wOt37w9nZLbQk2fh_Zy6K2Ym_qetq0jKo4yOznZEpPH-aRgar2SSejwNKNfwDRcT7N469Uy5U1FaDAAXq4KYyvHGS8GxQRKSx';
        
        $result = [
            'success' => 1,
            'message' => 'Done'
        ];
            
        $id = $QAppNoti->insert($data);
        $staff = $QStaff->fetchRow(['id = ?' => $data['staff_id']]);

        if(empty($staff['token_erp_app'])){
            $result = [
                'success' => 2,
                'message' => 'Staff is not use App.' . 'id = ' . $staff['id']
            ];
            return $result;
        }

        $user_token = $staff['token_erp_app'];
        $fields = array(
            "to" => $user_token,
            "priority" => "high",
            "notification"  =>      array(
                   "title"             =>  $data['title'],
                   "body"              =>  $data['message'],
                   "sound"             =>  "default",
                   "click_action"      =>  "FCM_PLUGIN_ACTIVITY",
                   "icon"              =>  "fcm_push_icon",
                                   'badge' => 2,
            ),
            "data" => array(
                "title"     => $data['title_name'],
                "message"   => $data['message'],
                "count" => 8,
                                "id_notification" => $id,
								"user_token" => $user_token,
                                "forceStart" => "1"
            ),
        );
        $fields = json_encode($fields);
        $headers = array(
            'Authorization: key=' . $my_key,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        $send_result = curl_exec($ch);
        curl_close($ch);

        $send_result = (array)json_decode($send_result);

        $result = [
            'success' => $send_result['success'],
            'message' => json_encode($send_result)
        ];

        return $result;
    }
    
    function getInfoNoti($id_notification){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id', 
            'title_name' => 'p.title', 
            'p.message', 
            'p.staff_id', 
            'p.link', 
            's.dingtalk_id',
            's.token_erp_app'
        );

        $select->from(array('p' => 'app_notification'), $arrCols);
        $select->joinLeft(array('s' => 'staff'), 's.id = p.staff_id', array());

        $select->where('p.id = ?', $id_notification);
        $result = $db->fetchRow($select);

        return $result;
    }

    public function getStoreInvalid($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['s' => 'store'], [
                's.id',
                's.name'
            ])
            ->joinLeft(['d' => 'regional_market'], 's.district = d.id', [])
            ->joinLeft(['r' => 'regional_market'], 'd.parent = r.id', [])
            ->where('s.del = 0 OR s.del IS NULL');

        if ($params['list_area_id']) {
            $select->where('r.area_id IN (?)', $params['list_area_id']);
        }

        if ($params['change_picture_stage']) {
            $select->join(['a' => DATABASE_TRADE . '.change_picture_assign_store'], 'a.store_id = s.id AND a.stage_id = ' . $params['change_picture_stage'], []);
        }

        $list_own_store = $db->fetchAll($select);
  
        foreach ($list_own_store as $store) {
            $list_own_store_id [] = $store['id'];
        }

        foreach ($params['array_store'] as $store_id) {
            if (! in_array($store_id, $list_own_store_id) ) {
                $list_invalid_store_id [] = $store_id;
            }
        }


        return $list_invalid_store_id;

    }


    //Lấy shop đang hoạt động theo staff
    public function getListShopByStaff(){

        $userStorage    = Zend_Auth::getInstance()->getStorage()->read();
        $group_id = $userStorage->group_id;
        $title_id = $userStorage->title;
        $staff_id = $userStorage->id;

        $result = array();
        if((!empty($staff_id))&&(!empty($title_id))){

            $db = Zend_Registry::get('db');
            $select = $db->select();

            if($title_id==SALES_TITLE){//sale
                $select = " SELECT `p`.`id`, `p`.`name`, p.store_code
                            FROM `store` AS `p`
                            LEFT JOIN `store_staff_log` AS `st` ON st.store_id = p.id
                            WHERE  (p.del is NULL or p.del= 0) 
                            AND ( st.released_at IS NULL AND st.is_leader = 1 AND st.staff_id = ".$staff_id." )
                            ORDER BY `p`.`id` DESC ";
                $result = $db->fetchAll($select);

            }elseif($title_id==SALES_LEADER_TITLE){//leader
                $select = " SELECT `p`.`id`, `p`.`name`, p.store_code
                            FROM `store` AS `p`
                            LEFT JOIN store_leader_log AS sll ON sll.store_id = p.id
                            WHERE  (p.del is NULL or p.del= 0) 
                            AND ( sll.released_at IS NULL AND sll.staff_id = ".$staff_id." ) 
                            GROUP BY `p`.`id`
                            ORDER BY `p`.`id` DESC ";
                $result = $db->fetchAll($select);

            }elseif(in_array($title_id, [179, 181, 308, 391, 392, 462, 463, 464, 465, 466, 467, 471])){//ASM
                $select = " SELECT `p`.`id`, `p`.`name`, p.store_code
                            FROM `store` AS `p`
                            LEFT JOIN regional_market AS rm ON rm.id = p.regional_market
                            LEFT JOIN asm ON asm.area_id = rm.area_id
                            LEFT JOIN staff AS s ON s.id = asm.staff_id
                            WHERE  (p.del is NULL or p.del= 0)
                            AND ( asm.area_id is NOT NULL AND asm.staff_id = ".$staff_id.")
                            GROUP BY `p`.`id`
                            ORDER BY `p`.`id` DESC  ";
                $result = $db->fetchAll($select);

            }elseif ($staff_id == SUPERADMIN_ID || in_array($group_id, array(ADMINISTRATOR_ID )) ){
                $select = " SELECT `p`.`id`, `p`.`name`, p.store_code
                            FROM `store` AS `p`
                            WHERE  (p.del is NULL or p.del= 0) ";
                $result = $db->fetchAll($select);

            }

        }
        return $result;
    }

    public function getCountCheckshop($params)
    {
        $from_date = date('Y-m-01 00:00:00');
        $to_date = date('Y-m-t 23:59:59');
        $team_trade_marketing = TEAM_TRADE_MARKETING;

        $sub_select_count_checkshop = "
       	SELECT 
        h.store_id,
        COUNT(DISTINCT h.legal_date) as count_legal_checkshop
    
        FROM 
        (
            SELECT 
    
            @legal_date :=
            IF (g.store_id <> @store_id  , g.date_checkshop , 
                IF( DATE_SUB(g.date_checkshop ,INTERVAL 1 DAY) >= @legal_date, g.date_checkshop, @legal_date)
            )
             as legal_date,
            @store_id := g.store_id as store_id
            
            FROM 
            (
                SELECT a.store_id,
                DATE(a.created_at) as date_checkshop
                
                FROM trade_marketing.app_checkshop a 
                LEFT JOIN hr.staff st ON a.created_by = st.id 
                
                WHERE (a.created_at BETWEEN '$from_date' AND '$to_date')
                AND st.team <> '$team_trade_marketing'
                
                GROUP BY a.store_id, date_checkshop 
                ORDER BY a.store_id, date_checkshop ASC
            ) g
            
            JOIN (SELECT @store_id := 0, @legal_date := '0000-00-00') t
        ) h
    
        GROUP BY h.store_id

        ";




        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['s' => 'store'], [
                'total_store' => "COUNT(s.id)",
                'store_done' => "SUM(IF(ch.count_legal_checkshop >= 2, 1, 0))",
                'total_store_ka' => "SUM( IF(d.is_ka = 1, 1, 0) )",
                'store_ka_done' => "SUM( IF(d.is_ka = 1, IF(ch.count_legal_checkshop >= 2, 1, 0) ,0 ) )",
                'total_store_ind' => "SUM( IF(d.is_ka = 0 OR d.is_ka IS NULL, 1, 0) )",
                'store_ind_done' => "SUM( IF(d.is_ka = 0 OR d.is_ka IS NULL, IF(ch.count_legal_checkshop >= 2, 1, 0) ,0 ) )"
            ])
            ->joinLeft(['d' => WAREHOUSE_DB . '.distributor'], 's.d_id = d.id', [])
            ->joinLeft(['ch' => new Zend_Db_Expr('(' . $sub_select_count_checkshop . ')')], 's.id = ch.store_id', [])

        ->where('s.del = 0 OR s.del IS NULL');


        if ($params['type_staff'] == 1) { // nếu là sale , pg sup
            $sub_select_sale = $db->select()
                ->from(['l' => 'store_staff_log'], [
                    'l.store_id'
                ])
                ->where('l.staff_id = ?', $params['staff_id'])
                ->where('l.is_leader IN (?)', [1, 8]) // sale , PGs supervisor
                ->where("l.released_at IS NULL OR FROM_UNIXTIME(l.released_at,'%Y-%m-%d') > CURDATE()")
                ->group('l.store_id');

            // select main
            $select->join(['sale' => $sub_select_sale], 's.id = sale.store_id', []);
        }


        if ($params['type_staff'] == 2) { // nếu là sale leader
            $sub_select_leader = $db->select()
                ->from(['l' => 'store_leader_log'], [
                    'l.store_id'
                ])
                ->where('l.staff_id = ?', $params['staff_id'])
                ->where("l.released_at IS NULL OR FROM_UNIXTIME(l.released_at,'%Y-%m-%d') > CURDATE()")
                ->group('l.store_id');

            $sub_select_sale = $db->select()
                ->from(['l' => 'store_staff_log'], [
                    'l.store_id'
                ])
                ->where('l.staff_id = ?', $params['staff_id'])
                ->where('l.is_leader IN (?)', [1]) // sale 
                ->where("l.released_at IS NULL OR FROM_UNIXTIME(l.released_at,'%Y-%m-%d') > CURDATE()")
                ->group('l.store_id');

            $sub_select_leader_union = $db->select()->union(array($sub_select_leader, $sub_select_sale));

            // select main
            $select->join(['leader' => $sub_select_leader_union], 's.id = leader.store_id', []);
        }

        if ($params['type_staff'] == 3) { // các title còn lại
            $select->joinLeft(['di' => 'regional_market'], 's.district = di.id', []);
            $select->joinLeft(['p' => 'regional_market'], 'di.parent = p.id', []);
            $select->where('p.area_id IN (?)', $params['area_list']);
        }

        $result = $db->fetchRow($select);

        return $result;

        }


    public function checkDiffMonth($date1, $date2)
    {
        $year1 = date('Y', strtotime($date1));
        $year2 = date('Y', strtotime($date2));

        $month1 = date('m', strtotime($date1));
        $month2 = date('m', strtotime($date2));

        $diff_month= (($year2 - $year1) * 12) + ($month2 - $month1) + 1;

        return $diff_month ;

    }
    
    


}


