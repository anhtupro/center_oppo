<?php

class Application_Model_OrderAirStage extends Zend_Db_Table_Abstract
{
    protected $_name = 'order_air_stage';
    protected $_schema = DATABASE_TRADE;

    public function fetchPagination($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(['r' => DATABASE_TRADE.'.order_air_stage'], [
                new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT r.id'),
                'r.name',
                'r.from_date',
                'r.to_date',
                'cost' => 'SUM(d.price * a.order_quantity)',
                'r.type'
            ])
            ->joinLeft(['d' => DATABASE_TRADE.'.order_air_detail'], 'r.id = d.order_air_stage_id', [])
            ->joinLeft(['a' => DATABASE_TRADE.'.order_air_area'], 'd.id = a.order_air_detail_id', [])
            ->joinLeft(['t' => DATABASE_TRADE.'.order_air_area_status'], 'r.id = t.order_air_stage_id AND a.area_id = t.area_id', [])
//            ->where('a.order_quantity > ?', 0)
//            ->where('t.status > ?', 3)
            ->where('r.is_deleted = 0 OR r.is_deleted IS NULL')
            ->group('r.id')
             ->order('r.id DESC');

        if ($params['from_date']) {
            $select->where("DATE_FORMAT(r.to_date,'%Y-%m-%d') >= ?", $params['from_date']);
        }
        if ($params['to_date']) {
            $select->where("DATE_FORMAT(r.to_date,'%Y-%m-%d') <= ?", $params['to_date']);
        }
        if ($params['area_list']) {
            $select->where('a.area_id IN (?)', $params['area_list']);
        }
        if ($params['area_id']) {
            $select->where('a.area_id = ?', $params['area_id']);
        }

        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    public function getTotalDetail($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['o' => DATABASE_TRADE.'.order_air_stage'], [
                        'e.*'
                     ])
                    ->joinLeft(['d' => DATABASE_TRADE.'.order_air_detail'], 'o.id = d.order_air_stage_id', [])
                    ->joinLeft(['e' => DATABASE_TRADE.'.order_air_area'], 'd.id = e.order_air_detail_id', [])

                     ->where('o.id = ?', $params['stage_id'])
                    ->where('e.area_id IN (?)', $params['area_list']);

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['order_air_detail_id']] ['order_quantity'] += $element['order_quantity'];
            $list [$element['order_air_detail_id']] ['receive_quantity'] += $element['receive_quantity'];
        }

        return $list;
    }
}    