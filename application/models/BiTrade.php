<?php
class Application_Model_BiTrade extends Zend_Db_Table_Abstract
{
    protected $_name = 'bi_trade';

    function getAirInvestment($params){
		$db = Zend_Registry::get('db');
        $select = $db->select();
		
        $col = array(
            'area_id'       => 'a.id',
        	'area_name' 	=> 'a.name' , 
        	'store_name'  	=> 'p.name', 
        	'category_name' => 'c.name', 
        	'i.cate_id', 
        	'i.quantity', 
        	'i.created_at',
        	'total_price'	=> 'SUM(i.total_price)'
        );

        $category = $this->getCategoryBi($params);
        foreach ($category as $key => $value) {
            $col['total_price_'.$value['id']]   = "MAX(CASE WHEN `i`.`cate_id` = ".$value['id']." THEN i.total_price END)";
            $col['quantity_'.$value['id']]      = "MAX(CASE WHEN `i`.`cate_id` = ".$value['id']." THEN i.quantity END)";
        }

        $sub_select = "(SELECT p.store_id, p.cate_id, p.quantity, p.created_at, p.money total_price
							FROM `".DATABASE_TRADE."`.investment p
							UNION ALL
							SELECT p.store_id, p.cat_id, p.num, p.created_at, IF( p.price_final = 0, p.price, p.price_final) total_price
							FROM `".DATABASE_TRADE."`.air p
							WHERE p.status = 28
						)";

        $select->from(array('p' => 'store'), $col);
        $select->joinLeft(array('i'=> new Zend_Db_Expr($sub_select) ), 'p.id = i.store_id', array());
        $select->joinLeft(array('c'=> DATABASE_TRADE.'.category'), 'c.id = i.cate_id', array());
        $select->joinLeft(array('g'=> DATABASE_TRADE.'.group_bi'), 'g.id = c.group_bi', array());
        $select->joinLeft(array('r'=> 'regional_market'), 'r.id = p.regional_market', array() );
		$select->joinLeft(array('a'=> 'area'), 'a.id = r.area_id', array() );
        
        $select->where('i.cate_id IS NOT NULL', NULL);
        $select->where('a.id IS NOT NULL', NULL);
        $select->group(array('a.id'));

        return $db->fetchAll($select);        
    }

    function getCategoryBi($params){
        $db = Zend_Registry::get('db');
        $select = $db->select();
        
        $col = array(
            'p.id',
            'p.name',
            //'p.price', 
        );

        $select->from(array('p' => DATABASE_TRADE.'.group_bi'), $col);
        
        //$select->where('p.id IN (1,2,6,43,44)', NULL);

        return $db->fetchAll($select);        
    }

    function getCategoryBiStore($params){
        $db = Zend_Registry::get('db');
        $select = $db->select();
        
        $col = array(
            'c.id',
            'c.name',
            'c.price', 
        );

        $sub_select = "(SELECT p.store_id, p.cate_id, p.quantity, p.created_at, p.money total_price
                            FROM `".DATABASE_TRADE."`.investment p
                            UNION ALL
                            SELECT p.store_id, p.cat_id, p.num, p.created_at, IF( p.price_final = 0, p.price, p.price_final) total_price
                            FROM `".DATABASE_TRADE."`.air p
                            WHERE p.status = 28
                        )";

        $select->from(array('p' => new Zend_Db_Expr($sub_select)), $col);
        $select->joinLeft(array('s'=> 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('d'=> WAREHOUSE_DB.'.distributor'), 'd.id = s.d_id', array());
        $select->joinLeft(array('c'=> DATABASE_TRADE.'.category'), 'c.id = p.cate_id', array());
        $select->joinLeft(array('g'=> DATABASE_TRADE.'.group_bi'), 'g.id = c.id', array());

        $select->where('p.cate_id IN (1,2,6,43,44)', NULL);

        if(isset($params['store_id']) and $params['store_id']){
            $select->where('p.store_id = ?', $params['store_id']);
        }

        if(isset($params['distributor_id']) and $params['distributor_id']){
            $select->where('d.id = ?', $params['distributor_id']);
        }

        $select->group('g.id');

        return $db->fetchAll($select);        
    }

    function get_province(){
        $db = Zend_Registry::get('db');
        $select = $db->select();
        
        $col = array(
            'id',
            'name', 
            'area_id', 
            'office_type', 
            'provine', 
            'ip_machine', 
            'parent',
            'province_id'
        );


        $select->from(array('p' => 'regional_market'), $col);
        
        $select->where('p.parent = 0', NULL);

        return $db->fetchAll($select);    
    }
	
    function getAirInvestmentDealer($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');
        $select = $db->select();

        $col = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
            'dealer_id'     => 'd.id',
            'dealer_name'   => 'd.title',
            'area_id'       => 'a.id',
            'area_name'     => 'a.name' , 
            'store_name'    => 'p.name', 
            'category_name' => 'c.name', 
            'c.group_bi', 
            'i.quantity', 
            'i.created_at',
            'total_price'   => 'SUM(i.value)'
        );

        $category = $this->getCategoryBi($params);
        foreach ($category as $key => $value) {
            $col['total_price_'.$value['id']]   = "MAX(CASE WHEN c.group_bi = ".$value['id']." THEN i.value END)";
            $col['quantity_'.$value['id']]      = "MAX(CASE WHEN c.group_bi = ".$value['id']." THEN i.quantity END)";
        }

        $select->from(array('p' => 'store'), $col);
        $select->joinLeft(array('d'=> WAREHOUSE_DB.'.distributor'), 'd.id = p.d_id', array() );
        $select->joinLeft(array('i'=> 'bi_trade'), 'p.id = i.store_id', array());
        $select->joinLeft(array('c'=> DATABASE_TRADE.'.category'), 'c.id = i.category_id', array());
        $select->joinLeft(array('r'=> 'regional_market'), 'r.id = p.regional_market', array() );
        $select->joinLeft(array('a'=> 'area'), 'a.id = r.area_id', array() );
        
        if( isset($params['area_id']) and $params['area_id'] ){
            $select->where('a.id = ?', $params['area_id']);
        }

        $select->where('d.id IS NOT NULL', NULL);
        $select->group(array('d.id'));

        if ($limit){
            $select->limitPage($page, $limit);
        }

        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;      
    }

    function getAirInvestmentStore($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');
        $select = $db->select();

        $col = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
            'store_id'      => 'p.id',
            'dealer_id'     => 'd.id',
            'dealer_name'   => 'd.title',
            'area_id'       => 'a.id',
            'area_name'     => 'a.name' , 
            'store_name'    => 'p.name', 
            'category_name' => 'c.name', 
            'c.group_bi', 
            'i.quantity', 
            'i.created_at',
            'total_price'   => 'SUM(i.value)'
        );

        $category = $this->getCategoryBi($params);
        foreach ($category as $key => $value) {
            $col['total_price_'.$value['id']]   = "MAX(CASE WHEN c.group_bi = ".$value['id']." THEN i.value END)";
            $col['quantity_'.$value['id']]      = "MAX(CASE WHEN c.group_bi = ".$value['id']." THEN i.quantity END)";
        }

        $select->from(array('p' => 'store'), $col);
        $select->joinLeft(array('d'=> WAREHOUSE_DB.'.distributor'), 'd.id = p.d_id', array() );
        $select->joinLeft(array('i'=> 'bi_trade'), 'p.id = i.store_id', array());
        $select->joinLeft(array('c'=> DATABASE_TRADE.'.category'), 'c.id = i.category_id', array());
        $select->joinLeft(array('r'=> 'regional_market'), 'r.id = p.regional_market', array() );
        $select->joinLeft(array('a'=> 'area'), 'a.id = r.area_id', array() );
        
        if( isset($params['dealer_id']) and $params['dealer_id'] ){
            $select->where('d.id = ?', $params['dealer_id']);
        }

        $select->group(array('p.id'));

        if ($limit){
            $select->limitPage($page, $limit);
        }

        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;      
    }

    public function getDealerById($dealer_id){
        $db = Zend_Registry::get('db');
        $select = $db->select();

        $col = array(
            'id'     => 'p.id',
            'name'   => 'p.title',
            'p.add'
        );


        $select->from(array('p' => WAREHOUSE_DB.'.distributor'), $col);
        
        $select->where('p.id = ?', $dealer_id);

        $result = $db->fetchRow($select);

        return $result;
    }


    public function functionName($params){
        
        return $result;
    }

    public function getDistrictByArea($params){

        if( empty($params['area_list']) ){
            return false;
        }

        $db = Zend_Registry::get('db');
        $select = $db->select();

        $col = array(
            'id'        => 'r.id',
            'name'      => 'r.name',
            'parent'    => 'r.parent',
        );


        $select->from(array('p' => 'regional_market'), $col);
        $select->joinLeft(array('r' => 'regional_market'), 'r.parent = p.id', array() );

        if( isset($params['area_list']) and $params['area_list']){
            $select->where('p.area_id IN (?)', $params['area_list'] );
        }

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getTradeByMonth($params){
        $db = Zend_Registry::get('db');
        
        $col = array(
            'month'     =>  "MONTH(p.invested_at)",
            'year'      =>  "YEAR(p.invested_at)",
            'area_id'   =>  'IFNULL(c.area_id, r.area_id)',
            'total_value'  => "SUM(p.`value`)"
        );

        $select = $db->select()
            ->from( array('p'=> 'bi_trade'), $col )
            ->joinLeft(array('s'=> 'store'), 's.id = p.store_id', array())
            ->joinLeft(array('i' => 'v_store_channel' ), 'i.store_id = s.id', array())
            ->joinLeft(array('r'=> 'regional_market'), 'r.id = s.regional_market', array())
            ->joinLeft(array('c'=> 'store_area_chanel'), 'c.store_id = s.id', array())
            ->joinLeft(array('a'=> 'area'), 'a.id = IFNULL(c.area_id, r.area_id)', array())
            ;
        
        if( isset($params['area_list']) AND $params['area_list'] ){
            $select->where('a.id IN (?)', $params['area_list'] );
        }

        if( isset($params['province_list']) AND $params['province_list'] ){
            $select->where('r.id IN (?)', $params['province_list'] );
        }

        if( isset($params['district_list']) AND $params['district_list'] ){
            $select->where('s.district IN (?)', $params['district_list'] );
        }

        if( isset($params['channel_id']) AND $params['channel_id'] ){
            $select->where('i.parent IN (?)', $params['channel_id'] );
        }
        
        $select->group( "DATE_FORMAT(p.invested_at, '%Y-%m')" );
        
        $result = $db->fetchAll($select);

        $data = [];
        foreach($result as $key=>$value){
            $data[$value['month']][$value['year']]['money'] = $value['total_value'];
        }

        return $data;
    }

    public function loadGoodByMonthAll($params){
        if(isset($params['from']) and $params['from'] and isset($params['to']) and $params['to']){
        
            $db     = Zend_Registry::get("db");
            $select = $db->select();

            $arrCols = array(
                'channel'       => 'c.channel',
                'month_date'    => "p.month",
                'year_date'     => "p.year",
                'num'           => 'SUM(p.sellout)',
                'total_value'   => 'SUM(p.sellout_value*8/10)'
            );

            $select->from(array('p'=> 'v_kpi_store_month'), $arrCols);
            $select->joinLeft(array('s' => 'store'), 's.id = p.store_id',array());
            $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market',array());
            $select->joinLeft(array('c' => 'v_store_channel'), 'c.store_id = p.store_id',array());
            
            
            if(isset($params['area_list']) and $params['area_list']){
                $select->where('r.area_id IN (?)', $params['area_list']);
            }

            if( isset($params['channel_id']) AND $params['channel_id'] ){
                $select->where('c.parent IN (?)', $params['channel_id'] );
            }
            
            $select->group(array('c.channel', 'p.month', 'p.year'));

            $result  = $db->fetchAll($select);
            
            return $result;
        }
    }

    public function getQuantityTrade($params){
        $db = Zend_Registry::get('db');
        
        $col = array(
            'month'         =>  "MONTH(p.invested_at)",
            'year'          =>  "YEAR(p.invested_at)",
            'cate_id'       =>  'g.id',
            'quantity'      =>  'SUM(p.quantity)',
            'money'         =>  "SUM(p.`value`)"
        );

        $select = $db->select()
            ->from( array('p'=> 'bi_trade'), $col )
            ->joinLeft(array('cat'=> DATABASE_TRADE.'.category'), 'cat.id = p.category_id', array())
            ->joinLeft(array('g'=> DATABASE_TRADE.'.group_bi'), 'g.id = cat.group_bi', array())
            ->joinLeft(array('s'=> 'store'), 's.id = p.store_id', array())
            ->joinLeft(array('v'=> 'v_store_channel'), 'v.store_id = s.id', array())
            ->joinLeft(array('r'=> 'regional_market'), 'r.id = s.regional_market', array())
            ->joinLeft(array('c'=> 'store_area_chanel'), 'c.store_id = s.id', array())
            ->joinLeft(array('a'=> 'area'), 'a.id = IFNULL(c.area_id, r.area_id)', array())
            ;

        if( isset($params['area_list']) And $params['area_list'] ){
            $select->where("a.id IN (?)", $params['area_list']);
        }

        if(isset($params['province_list']) AND $params['province_list'])
            $select->where( 'r.province_id IN (?)', $params['province_list'] );

        if(isset($params['district_list']) AND $params['district_list']){
            $select->where( 'r.id IN (?)', $params['district_list'] );
        }

        if( isset($params['channel_id']) AND $params['channel_id'] ){
            $select->where('v.parent IN (?)', $params['channel_id'] );
        }

        $select->group( array("DATE_FORMAT(p.invested_at, '%Y-%m')", "g.id") );

        $result = $db->fetchAll($select);

        $data = [];
        foreach ($result as $key => $value) {
            $data[$value['month']][$value['year']][$value['cate_id']]['money'] =  $value['money'];
            $data[$value['month']][$value['year']][$value['cate_id']]['quantity'] =  $value['quantity'];
        }
        
        return $data;
    }

    public function getChannelTrade($params){
        $db = Zend_Registry::get('db');
        
        $col = array(
            'month'  => 'MONTH(p.invested_at)', 
            'year'   => 'YEAR(p.invested_at)',
            'd.channel',
            "total_value" => "SUM(p.`value`)" 
        );

        $nestedSelect = "SELECT p.id, IFNULL( IFNULL( l.`name`, channel.`name`) , 'NORMAL' ) channel
                         FROM warehouse.distributor p
                         LEFT JOIN dealer_loyalty d ON d.dealer_id = p.id AND d.from_date = (SELECT MAX(p.from_date) FROM dealer_loyalty p)
                         LEFT JOIN loyalty_plan l ON l.`id` = d.loyalty_plan_id
                         LEFT JOIN channel_dealer c ON c.d_id = p.id OR c.d_id = p.parent 
                         LEFT JOIN channel channel ON channel.id = c.channel_id
                            ";

        $select = $db->select()
            ->from( array('p'=> 'bi_trade') , $col )
            ->joinLeft(array('s'=> 'store'), 's.id = p.store_id', array())
            ->joinLeft(array('r'=> 'regional_market'), 'r.id = s.regional_market', array())
            ->joinLeft(array('c'=> 'store_area_chanel'), 'c.store_id = s.id', array())
            ->joinLeft(array('a'=> 'area'), 'a.id = IFNULL(c.area_id, r.area_id)', array())
            ->joinLeft(array('d'=> new Zend_Db_Expr('(' . $nestedSelect . ')')), 'd.id = s.d_id', array())
            ;

        if( isset($params['area_list']) And $params['area_list'] ){
            $select->where( 'a.id IN (?)', $params['area_list'] );
        }

        $select->group( array("d.channel", "DATE_FORMAT(p.invested_at, '%Y-%m')") );

        $result = $db->fetchAll($select);

        $data = array();
        foreach($result as $key => $value){
            $data[$value['month']][$value['year']][$value['channel']]['total_value'] = $value['total_value'];
        }

        return $data;
    }

    public function getFromToSO($params){

        if(empty($params['store_id']) AND empty($params['distributor_id'])){
            return false;
        }

        $db = Zend_Registry::get('db');
        
        $col = array(
            'from_date'        =>  "DATE_FORMAT( MIN(p.timing_date) , '%Y-%m-%d')",
            'to_date'          =>  "DATE_FORMAT( MAX(p.timing_date) , '%Y-%m-%d')",
            'sellout'          =>  "COUNT(p.imei_sn)",
            'total_value'      =>  "SUM(p.`value`)",
        );

        $select = $db->select()
            ->from( array('p'=> 'imei_kpi'), $col )
            ;

        if(isset($params['store_id']) and $params['store_id']){
            $select->where('p.store_id = ?', $params['store_id']);
        }

        if(isset($params['distributor_id']) and $params['distributor_id']){
            $select->where('p.distributor_id = ?', $params['distributor_id']);
        }

        $result = $db->fetchRow($select);

        return $result;
    }

    public function getSelloutByStore($params){
        if(empty($params['store_id']) AND empty($params['distributor_id'])){
            return false;
        }

        $db = Zend_Registry::get('db');
        
        $col = array(
            'month'         =>  "MONTH(p.timing_date)",
            'year'          =>  "YEAR(p.timing_date)",
            'sellout'       =>  "COUNT(p.imei_sn)",
            'total_value'   =>  "SUM(`value`)",
        );

        $select = $db->select()
            ->from( array('p'=> 'imei_kpi'), $col )
            ;

        if(isset($params['store_id']) and $params['store_id']){
            $select->where('p.store_id = ?', $params['store_id']);
        }

        if(isset($params['distributor_id']) and $params['distributor_id']){
            $select->where('p.distributor_id = ?', $params['distributor_id']);
        }

        $select->group("DATE_FORMAT(p.timing_date, '%Y-%m')");

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getTradeByStore($params){

        if( empty($params['store_id']) AND empty($params['distributor_id']) ){
            return false;
        }

        $db = Zend_Registry::get('db');
        
        $col = array(
            'month'           =>  "MONTH(p.invested_at)",
            'year'            =>  "YEAR(p.invested_at)",
            'total_value'     =>  'SUM(p.value)'
        );

        $select = $db->select()
            ->from( array('p'=> 'bi_trade'), $col )
            ->joinLeft(array('s'=> 'store'), 's.id = p.store_id', array())
            ->joinLeft(array('c'=> DATABASE_TRADE.'.category'), 'c.id = p.category_id', array())
            ->group("DATE_FORMAT(p.invested_at, '%Y-%m')")
            ;

        if(isset($params['store_id']) and $params['store_id']){
            $select->where('p.store_id = ?', $params['store_id']);
        }

        if(isset($params['distributor_id']) and $params['distributor_id']){
            $select->where('s.d_id = ?', $params['distributor_id']);
        }

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getAreaTrade($params){

        $db = Zend_Registry::get('db');
        
        $col = array(
            "area_id"       => "IFNULL(c.area_id, r.area_id)",
            "p.category_id",
            'quantity'      => 'SUM(p.quantity)',
            "total_value"   => "SUM(p.`value`)"
        );

        $select = $db->select()
            ->from( array('p'=> 'bi_trade'), $col )
            ->joinLeft(array('s'=> 'store'), 's.id = p.store_id', array())
            ->joinLeft(array('r'=> 'regional_market'), 'r.id = s.regional_market', array())
            ->joinLeft(array('c'=> 'store_area_chanel'), 'c.store_id = s.id', array())
            ->group("IFNULL(c.area_id, r.area_id)")
            ;

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getDistrictTrade($params){

        $db = Zend_Registry::get('db');
        
        $col = array(
            "district_id"       => "s.district",
            "p.category_id",
            'quantity'          => 'SUM(p.quantity)',
            "total_value"       => "SUM(p.`value`)"
        );

        $select = $db->select()
            ->from( array('p'=> 'bi_trade'), $col )
            ->joinLeft(array('s'=> 'store'), 's.id = p.store_id', array())
            ->joinLeft(array('r'=> 'regional_market'), 'r.id = s.regional_market', array())
            ->joinLeft(array('c'=> 'store_area_chanel'), 'c.store_id = s.id', array());

        if(!empty($params['area_list'])){
            $select->where('IFNULL(c.area_id, r.area_id) IN (?)', $params['area_list']);
        }

        $select->group("s.district");

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getAreaSO($params){

        $db = Zend_Registry::get('db');
        
        $col = array(
            "area_id"       => "IFNULL(c.area_id, r.area_id)",
            "quantity"      => "SUM(p.quantity)",
            'value'         => "SUM(`value`)*8/10",
        );

        $select = $db->select()
            ->from( array('p'=> 'kpi_month_store'), $col )
            ->joinLeft(array('s'=> 'store'), 's.id = p.store_id', array())
            ->joinLeft(array('r'=> 'regional_market'), 'r.id = s.regional_market', array())
            ->joinLeft(array('c'=> 'store_area_chanel'), 'c.store_id = s.id', array())
            ->group("IFNULL(c.area_id, r.area_id)")
            ;

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getDistrictSO($params){

        $db = Zend_Registry::get('db');
        
        $col = array(
            "district_id"   => "s.district",
            "quantity"      => "SUM(p.quantity)",
            'value'         => "SUM(`value`)*8/10",
        );

        $select = $db->select()
            ->from( array('p'=> 'kpi_month_store'), $col )
            ->joinLeft(array('s'=> 'store'), 's.id = p.store_id', array())
            ->joinLeft(array('r'=> 'regional_market'), 'r.id = s.regional_market', array())
            ->joinLeft(array('c'=> 'store_area_chanel'), 'c.store_id = s.id', array());

        if(!empty($params['area_list'])){
            $select->where('IFNULL(c.area_id, r.area_id) IN (?)', $params['area_list']);
        }

        $select->group("s.district");
       
        $result = $db->fetchAll($select);

        return $result;
    }

    public function getCategoryTrade($params){

        $db = Zend_Registry::get('db');
        
        $col = array(
            "area_id"       => "IFNULL(c.area_id, r.area_id)",
            "quantity"      => "SUM(p.quantity)",
            'value'         => "SUM(p.`value`)",
        );

        $select = $db->select()
            ->from( array('p'=> 'bi_trade'), $col )
            ->joinLeft(array('cat'=> DATABASE_TRADE.'.category'), 'cat.id = p.category_id', array())
            ->joinLeft(array('g'=> DATABASE_TRADE.'.group_bi'), 'g.id = cat.group_bi', array())
            ->joinLeft(array('s'=> 'store'), 's.id = p.store_id', array())
            ->joinLeft(array('r'=> 'regional_market'), 'r.id = s.regional_market', array())
            ->joinLeft(array('c'=> 'store_area_chanel'), 'c.store_id = s.id', array())
            ->joinLeft(array('a'=> 'area'), 'a.id = IFNULL(c.area_id, r.area_id)', array());

        $select->where('a.status = ?', 1);

        if( isset($params['group_cat_id']) and $params['group_cat_id'] ){
            $select->where('g.id = ?', $params['group_cat_id']);
        }

        if(isset($params['from_date'])  and $params['from_date']){
            $select->where('p.invested_at >= ?', $params['from_date']);
        }

        if(isset($params['to_date'])  and $params['to_date']){
            $select->where('p.invested_at <= ?', $params['to_date']);
        }

        $select->group("IFNULL(c.area_id, r.area_id)");

        $result = $db->fetchAll($select);

        $data = array();

        foreach($result as $key=>$value){
            $data[$value['area_id']] = array(
                                        'quantity'  => $value['quantity'],
                                        'value'     => $value['value']
                                        );
        }

        return $data;
    }

    public function getCategoryTradeDistrict($params){

        $db = Zend_Registry::get('db');
        
        $col = array(
            "district_id"   => "s.district",
            "quantity"      => "SUM(p.quantity)",
            'value'         => "SUM(p.`value`)",
        );

        $select = $db->select()
            ->from( array('p'=> 'bi_trade'), $col )
            ->joinLeft(array('cat'=> DATABASE_TRADE.'.category'), 'cat.id = p.category_id', array())
            ->joinLeft(array('g'=> DATABASE_TRADE.'.group_bi'), 'g.id = cat.group_bi', array())
            ->joinLeft(array('s'=> 'store'), 's.id = p.store_id', array())
            ->joinLeft(array('r'=> 'regional_market'), 'r.id = s.regional_market', array())
            ->joinLeft(array('c'=> 'store_area_chanel'), 'c.store_id = s.id', array())
            ->joinLeft(array('a'=> 'area'), 'a.id = IFNULL(c.area_id, r.area_id)', array());

        $select->where('a.status = ?', 1);

        if( isset($params['group_cat_id']) and $params['group_cat_id'] ){
            $select->where('g.id = ?', $params['group_cat_id']);
        }

        if(isset($params['from_date'])  and $params['from_date']){
            $select->where('p.invested_at >= ?', $params['from_date']);
        }

        if(isset($params['to_date'])  and $params['to_date']){
            $select->where('p.invested_at <= ?', $params['to_date']);
        }

        if(isset($params['area_list'])  and $params['area_list']){
            $select->where('IFNULL(c.area_id, r.area_id) IN (?)', $params['area_list']);
        }

        $select->group("s.district");

        $result = $db->fetchAll($select);

        $data = array();

        foreach($result as $key=>$value){
            $data[$value['district_id']] = array(
                                        'quantity'  => $value['quantity'],
                                        'value'     => $value['value']
                                        );
        }

        return $data;
    }

    public function getCategoryTotal($params){
        $db = Zend_Registry::get('db');
        
        $col = array(
            "g.id", 
            "quantity" => "SUM(p.quantity)", 
            "total_value" => "SUM(p.`value`)" ,
        );

        $select = $db->select()
            ->from( array('p'=> 'bi_trade'), $col )
            ->joinLeft(array('s'=> 'store'), 's.id = p.store_id', array())
            ->joinLeft(array('r'=> 'regional_market'), 'r.id = s.regional_market', array())
            ->joinLeft(array('c'=> 'store_area_chanel'), 'c.store_id = s.id', array())
            ->joinLeft(array('a'=> 'area'), 'a.id = IFNULL(c.area_id, r.area_id)', array())
            ->joinLeft(array('cat'=> DATABASE_TRADE.'.category'), 'cat.id = p.category_id', array())
            ->joinLeft(array('g'=> DATABASE_TRADE.'.group_bi'), 'g.id = cat.group_bi', array())
            ;

        if(!empty($params['area_list'])){
            $select->where('IFNULL(c.area_id, r.area_id) IN (?)', $params['area_list']);
        }

        $select->group("g.id");

        $result = $db->fetchAll($select);

        $data = array();

        foreach($result as $key=>$value){
            $data[$value['id']] = array(
                                        'quantity'  => $value['quantity'],
                                        'value'     => $value['value']
                                        );
        }

        return $data;
    }

    public function getCheckshop($params){
        $db = Zend_Registry::get('db');
        
        $col = array(
            'id'             =>  "g.id",
            'quantity'       =>  "SUM(p.number)",
        );

        $nestedSelect = "SELECT p.object_id, p.good_id, p.number, p.created_at
                         FROM `".DATABASE_TRADE."`.store_good p
                         WHERE `status` = 1
                         UNION 
                         SELECT p.object_id, p.good_id, 1, p.created_at
                         FROM `".DATABASE_TRADE."`.imei p
                         WHERE p.del <> 1 OR p.del IS NULL";

        $select = $db->select()
            ->from( array('p'=> new Zend_Db_Expr('(' . $nestedSelect . ')')), $col )
            ->joinLeft(array('s'=> 'store'), 's.id = p.object_id', array())
            ->joinLeft(array('r'=> 'regional_market'), 'r.id = s.regional_market', array())
            ->joinLeft(array('c'=> 'store_area_chanel'), 'c.store_id = s.id', array())
            ->joinLeft(array('a'=> 'area'), 'a.id = IFNULL(c.area_id, r.area_id)', array())
            ->joinLeft(array('cat'=> DATABASE_TRADE.'.category'), 'cat.id = p.good_id', array())
            ->joinLeft(array('g'=> DATABASE_TRADE.'.group_bi'), 'g.id = cat.group_bi', array());

        if(!empty($params['area_list'])){
            $select->where('IFNULL(c.area_id, r.area_id) IN (?)', $params['area_list']);
        }

        $select->group("g.id");

        $result = $db->fetchAll($select);

        $data = array();

        foreach($result as $key=>$value){
            $data[$value['id']] = array(
                                        'quantity'  => $value['quantity'],
                                        );
        }

        return $data;
    }

    public function getGroupBi($params){
        $db = Zend_Registry::get('db');
        
        $col = array(
            "p.id", 
            "p.name"
        );

        $select = $db->select()
            ->from( array('p'=> DATABASE_TRADE.'.group_bi'), $col )
            ;

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getListArea(){
        $db = Zend_Registry::get('db');
        
        $col = array(
            "p.id", 
            "p.name"
        );

        $select = $db->select()
            ->from( array('p'=> 'area'), $col )
            ->where( 'status = ?', 1 )
            ;

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getDataDoanhthuArea($params){

        $db = Zend_Registry::get('db');
        
        $col = array(
            "area_id"       => "IFNULL(c.area_id, r.area_id)",
            "quantity"      => "SUM(p.quantity)",
            'value'         => "SUM(`value`)*8/10",
        );

        $select = $db->select()
            ->from( array('p'=> 'kpi_month_store'), $col )
            ->joinLeft(array('s'=> 'store'), 's.id = p.store_id', array())
            ->joinLeft(array('r'=> 'regional_market'), 'r.id = s.regional_market', array())
            ->joinLeft(array('c'=> 'store_area_chanel'), 'c.store_id = s.id', array());

        if(isset($params['from_date'])  and $params['from_date']){
            $select->where('p.date >= ?', $params['from_date']);
        }

        if(isset($params['to_date'])  and $params['to_date']){
            $select->where('p.date <= ?', $params['to_date']);
        }

        $select->group("IFNULL(c.area_id, r.area_id)");
        $result = $db->fetchAll($select);

        $data = array();
        foreach($result as $key=>$value){
            $data[$value['area_id']] = $value['value'];
        }

        return $data;
    }

    public function getDataDoanhthuDistrict($params){

        $db = Zend_Registry::get('db');
        
        $col = array(
            "district_id"   => "s.district",
            "quantity"      => "SUM(p.quantity)",
            'value'         => "SUM(`value`)*8/10",
        );

        $select = $db->select()
            ->from( array('p'=> 'kpi_month_store'), $col )
            ->joinLeft(array('s'=> 'store'), 's.id = p.store_id', array())
            ->joinLeft(array('r'=> 'regional_market'), 'r.id = s.regional_market', array())
            ->joinLeft(array('c'=> 'store_area_chanel'), 'c.store_id = s.id', array());

        if(isset($params['from_date'])  and $params['from_date']){
            $select->where('p.date >= ?', $params['from_date']);
        }

        if(isset($params['to_date'])  and $params['to_date']){
            $select->where('p.date <= ?', $params['to_date']);
        }

        if(isset($params['area_list'])  and $params['area_list']){
            $select->where('IFNULL(c.area_id, r.area_id) IN (?)', $params['area_list']);
        }

        $select->group("s.district");
        $result = $db->fetchAll($select);

        $data = array();
        foreach($result as $key=>$value){
            $data[$value['district_id']] = $value['value'];
        }

        return $data;
    }

    public function getDataDautuArea($params){

        $db = Zend_Registry::get('db');
        
        $col = array(
            "area_id"       => "IFNULL(c.area_id, r.area_id)",
            "p.category_id",
            'quantity'      => 'SUM(p.quantity)',
            "total_value"   => "SUM(p.`value`)"
        );

        $select = $db->select()
            ->from( array('p'=> 'bi_trade'), $col )
            ->joinLeft(array('s'=> 'store'), 's.id = p.store_id', array())
            ->joinLeft(array('r'=> 'regional_market'), 'r.id = s.regional_market', array())
            ->joinLeft(array('c'=> 'store_area_chanel'), 'c.store_id = s.id', array())
            ->group("IFNULL(c.area_id, r.area_id)")
            ;

        if(isset($params['from_date'])  and $params['from_date']){
            $select->where('p.invested_at >= ?', $params['from_date']);
        }

        if(isset($params['to_date'])  and $params['to_date']){
            $select->where('p.invested_at <= ?', $params['to_date']);
        }

        if(isset($params['area_list'])  and $params['area_list']){
            $select->where('IFNULL(c.area_id, r.area_id) IN (?)', $params['area_list']);
        }

        $result = $db->fetchAll($select);

        $data = array();
        foreach($result as $key=>$value){
            $data[$value['area_id']] = $value['total_value'];
        }

        return $data;
    }

    public function getDataDautuDistrict($params){

        $db = Zend_Registry::get('db');
        
        $col = array(
            "district_id"       => "s.district",
            "p.category_id",
            'quantity'          => 'SUM(p.quantity)',
            "total_value"       => "SUM(p.`value`)"
        );

        $select = $db->select()
            ->from( array('p'=> 'bi_trade'), $col )
            ->joinLeft(array('s'=> 'store'), 's.id = p.store_id', array())
            ->joinLeft(array('r'=> 'regional_market'), 'r.id = s.regional_market', array())
            ->joinLeft(array('c'=> 'store_area_chanel'), 'c.store_id = s.id', array())
            ->group("s.district")
            ;

        if(isset($params['from_date'])  and $params['from_date']){
            $select->where('p.invested_at >= ?', $params['from_date']);
        }

        if(isset($params['to_date'])  and $params['to_date']){
            $select->where('p.invested_at <= ?', $params['to_date']);
        }

        if(isset($params['area_list'])  and $params['area_list']){
            $select->where('IFNULL(c.area_id, r.area_id) IN (?)', $params['area_list']);
        }

        $result = $db->fetchAll($select);

        $data = array();
        foreach($result as $key=>$value){
            $data[$value['district_id']] = $value['total_value'];
        }

        return $data;
    }

    public function getTradeAreaCategory($params){
        $db = Zend_Registry::get('db');
        
        $col = array(
            "area_id"       => "IFNULL(c.area_id, r.area_id)",
            "quantity"      => "SUM(p.quantity)",
            'value'         => "SUM(p.`value`)",
            'cat_id'        => 'g.id'
        );

        $select = $db->select()
            ->from( array('p'=> 'bi_trade'), $col )
            ->joinLeft(array('cat'=> DATABASE_TRADE.'.category'), 'cat.id = p.category_id', array())
            ->joinLeft(array('g'=> DATABASE_TRADE.'.group_bi'), 'g.id = cat.group_bi', array())
            ->joinLeft(array('s'=> 'store'), 's.id = p.store_id', array())
            ->joinLeft(array('r'=> 'regional_market'), 'r.id = s.regional_market', array())
            ->joinLeft(array('c'=> 'store_area_chanel'), 'c.store_id = s.id', array())
            ->joinLeft(array('a'=> 'area'), 'a.id = IFNULL(c.area_id, r.area_id)', array());

        $select->where('a.status = ?', 1);

        if( isset($params['group_cat_id']) and $params['group_cat_id'] ){
            $select->where('g.id = ?', $params['group_cat_id']);
        }

        if(isset($params['from_date'])  and $params['from_date']){
            $select->where('p.invested_at >= ?', $params['from_date']);
        }

        if(isset($params['to_date'])  and $params['to_date']){
            $select->where('p.invested_at <= ?', $params['to_date']);
        }

        $select->group(array("IFNULL(c.area_id, r.area_id)", "g.id"));

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getCategoryTradeArea($params){

        $db = Zend_Registry::get('db');
        
        $col = array(
            "district_id"   => "s.district",
            "quantity"      => "SUM(p.quantity)",
            'value'         => "SUM(p.`value`)",
        );

        $select = $db->select()
            ->from( array('p'=> 'bi_trade'), $col )
            ->joinLeft(array('cat'=> DATABASE_TRADE.'.category'), 'cat.id = p.category_id', array())
            ->joinLeft(array('g'=> DATABASE_TRADE.'.group_bi'), 'g.id = cat.group_bi', array())
            ->joinLeft(array('s'=> 'store'), 's.id = p.store_id', array())
            ->joinLeft(array('r'=> 'regional_market'), 'r.id = s.regional_market', array())
            ->joinLeft(array('c'=> 'store_area_chanel'), 'c.store_id = s.id', array())
            ->joinLeft(array('a'=> 'area'), 'a.id = IFNULL(c.area_id, r.area_id)', array());

        $select->where('a.status = ?', 1);

        if( isset($params['group_cat_id']) and $params['group_cat_id'] ){
            $select->where('g.id = ?', $params['group_cat_id']);
        }

        if(isset($params['from_date'])  and $params['from_date']){
            $select->where('p.invested_at >= ?', $params['from_date']);
        }

        if(isset($params['to_date'])  and $params['to_date']){
            $select->where('p.invested_at <= ?', $params['to_date']);
        }

        if(!empty($params['area_list'])){
            $select->where('IFNULL(c.area_id, r.area_id) IN (?)', $params['area_list']);
        }

        $select->group("s.district");

        $result = $db->fetchAll($select);

        $data = array();

        foreach($result as $key=>$value){
            $data[$value['district_id']] = array(
                                        'quantity'  => $value['quantity'],
                                        'value'     => $value['value']
                                        );
        }

        return $data;
    }


    public function getDataByStore($store_id){
        if(empty($store_id)){
            return false;
        }

        $db = Zend_Registry::get('db');
        
        $col = array(
            "store_id"         => "p.store_id",
            "category_id"      => "p.category_id",
            "quantity"         => "p.quantity",
            "value"            => "p.value",
            "invested_at"      => "p.invested_at",
            "store_name"       => "s.name",
            "category_name"    => "c.name",
            "group_bi"         => "c.group_bi"
        );

        $select = $db->select()
            ->from( array('p'=> 'bi_trade'), $col )
            ->joinLeft(array('s'=> 'store'), 's.id = p.store_id', array())
            ->joinLeft(array('c'=> DATABASE_TRADE.'.category'), 'c.id = p.category_id', array());

        $select->where('p.store_id = ?', $store_id);


        $result = $db->fetchAll($select);

        return $result;

    }

    public function getSelloutLastMonth($store_id){
        if(empty($store_id)){
            return false;
        }

        $db = Zend_Registry::get('db');
        
        $col = array(
            "quantity"         => "COUNT(DISTINCT p.imei_sn)"
        );

        $select = $db->select()
            ->from( array('p'=> 'imei_kpi'), $col );

        $select->where('p.store_id = ?', $store_id);
        $select->where("p.timing_date >= DATE_FORMAT( CURRENT_DATE - INTERVAL 1 MONTH, '%Y-%m-01 00:00:00' )", NULL);
        $select->where("p.timing_date <= DATE_FORMAT( CURRENT_DATE - INTERVAL 1 MONTH, '%Y-%m-d 23:59:59' )", NULL);

        $result = $db->fetchRow($select);

        return $result;

    }

    public function getCheckshopByStore($store_id){
        if(empty($store_id)){
            return false;
        }

        $db = Zend_Registry::get('db');
        
        $col = array(
            "store_id"         => "p.store_id",
            "category_id"      => "p.category_id",
            "quantity"         => "SUM(p.quantity)",
            "value"            => "p.value",
            "invested_at"      => "p.invested_at",
            "store_name"       => "s.name",
            "category_name"    => "c.name",
            "group_bi"         => "c.group_bi"
        );

        $select = $db->select()
            ->from( array('p'=> 'bi_trade'), $col )
            ->joinLeft(array('s'=> 'store'), 's.id = p.store_id', array())
            ->joinLeft(array('c'=> DATABASE_TRADE.'.category'), 'c.id = p.category_id', array());

        $select->where('p.store_id = ?', $store_id);

        $select->where('c.group_bi IS NOT NULL', NULL);

        $select->group('c.id');
        //echo $select; exit;
        $result = $db->fetchAll($select);

        return $result;

    }

	
}