<?php

/**
 * Class Application_Model_Province
 * Danh sach tinh theo nha nuoc
 *
 */
class Application_Model_ProvinceDistrict extends Zend_Db_Table_Abstract
{
	protected $_name   = 'province_district';

    function get_cache(){
        $db = Zend_Registry::get('db');
        $select = $db->select()->from(array('p' => $this->_name),
                    array('p.*'))->group('p.province_code')
                ;
        
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if (!$result) {

            $data =  $db->fetchAll($select);

            $result = array();
            if ($data){
                foreach ($data as $k => $item){
                    $result[$item['province_code']] = $item['province_name'];
                }
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }
    
    function get_province_by_district($district_id){
        $db = Zend_Registry::get('db');
        $select = $db->select()->from(array('p' => $this->_name),
            array('p.province_code'))->where('p.district_id = ?',$district_id)
            ;
            $data = $db->fetchRow($select);
            return $data;
    }
    /**
     * List of all districts
     * @return [type] [description]
     */
    function get_district_by_province_cache($province_code = null)
    {
        $cache = Zend_Registry::get('cache');
        $result = $cache->load($this->_name . '_district_by_province_code_cache');

        if ($result === false) {
            $db = Zend_Registry::get('db');
            $select = $db->select()->from(array('p' => $this->_name),
                    array('p.*'))->where('p.province_code = ?',$province_code)
                ;
            $data =  $db->fetchAll($select);

            $result = array();

            if ($data) {
                foreach ($data as $k => $item) {

                    $result[$item['district_id']] = $item['district_center'];
                
                }
            }

            $cache->save($result, $this->_name . '_district_by_province_code_cache', array(), null);
        }

        return $result;
        
    }
    
    function get_district_by_province($province_code = null)
    {
    
            $db = Zend_Registry::get('db');
            $select = $db->select()->from(array('p' => $this->_name),
                array('p.*'))->where('p.province_code = ?',$province_code)
                ;
                $data =  $db->fetchAll($select);
    
                $result = array();
    
                if ($data) {
                    foreach ($data as $k => $item) {
    
                        $result[$item['district_id']] = $item['district_center'];
    
                    }
                }
    
        return $result;
    
    }
    
	
}