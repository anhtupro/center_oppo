<?php
class Application_Model_Supplier extends Zend_Db_Table_Abstract
{
    protected $_name = 'supplier';

    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if ($result === false) {

            $db = Zend_Registry::get('db');
            $select = $db->select()
                    ->from(array('p' => $this->_name), array('p.*'));

            $select->where('p.del IS NULL OR p.del = ?', 0);
            $select->order(new Zend_Db_Expr('p.`title` COLLATE utf8_unicode_ci'));
            $data = $db->fetchAll($select);
            
            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item['id']] = $item['title'];
                }
            }
            
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }
    
    function get_cache_all(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if ($result === false) {

            $data = $this->fetchAll(null, 'name');

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->title;
                }
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }
    
        public function getSupplierDetailForProduct($code_id = null)
        {
            $db = Zend_Registry::get('db');
            $data = array();             
            $sql=null;
            if($code_id){
                //NCC có set giá trong setting price thì nằm trên đầu danh sách. còn lại ở sau
                $sql = "SELECT s.* FROM `supplier` s
                        LEFT JOIN code_supplier cs ON s.id = cs.supplier_id AND cs.code_id = ".$code_id." 
                        where s.del = 0
                        GROUP BY s.id
                        ORDER BY cs.id DESC, s.title";
                    }
            else{
                $sql = "SELECT s.* FROM `supplier` s
                LEFT JOIN code_supplier cs ON s.id = cs.supplier_id 
                where s.del = 0
                GROUP BY s.id
                ORDER BY cs.id DESC, s.title";
            }
            if(!empty($sql)){
                $stmt = $db->prepare($sql);
                $stmt->execute();
                $data = $stmt->fetchAll();
                $stmt->closeCursor();
                $stmt = $db = null;
            }



            return $data;
        }
}