<?php
class Application_Model_Salary extends Zend_Db_Table_Abstract{
	protected $_name = 'salary';

	/**
	 * Danh sách lương bảo hiểm
	 * @param  [int] $page   [description]
	 * @param  [int] $limit  [description]
	 * @param  [int] &$total [description]
	 * @param  [array] $params [description]
	 * @return [type]         [description]
	 *
	 */
	public function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        $cols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.id'),
            'a.*',
            'staff_code' => 'b.code',
            'staff_name' => 'CONCAT(b.firstname," ",b.lastname)',
            'created_by_name' => 'CONCAT(c.firstname," ",c.lastname)',
            'area_name' => 'e.name'
        );

        $select = $db->select()
            ->from(array('a'=>$this->_name),$cols)
         	->join(array('b'=>'staff'),'b.id = a.staff_id',array())
         	->join(array('c'=>'staff'),'c.id = a.created_by',array())
            ->join(array('d'=>'regional_market'),'d.id = b.regional_market',array())
            ->join(array('e'=>'area'),'e.id = d.area_id',array())
         	;

        if(isset($params['name']) AND $params['name']){
            $select->where('CONCAT(b.firstname," ",b.lastname) LIKE ?','%'.$params['name'].'%');
        }

        if(isset($params['code']) AND $params['code']){
            $select->where('b.code LIKE ?','%'.$params['code'].'%');
        }

        if(isset($params['created_from']) AND $params['created_from']){
            $created_from = My_Date::normal_to_mysql($params['created_from']);
            $select->where('a.created_at >= ?',$created_from);
        }

        if(isset($params['created_to']) AND $params['created_to']){
            $created_to = My_Date::normal_to_mysql($params['created_to']);
            $select->where('a.created_at <= ?',$created_to);
        }     

        if( isset($params['sort']) AND $params['sort']){
            $desc     = ( isset($params['desc']) AND $params['desc'] == 1) ? ' desc ':' asc ';
            $order_by = '';
            $collate  = '';

            if( in_array($params['sort'],array('name','code')) ){
                $collate = ' COLLATE utf8_unicode_ci ';    
            }

            if($params['sort'] == 'name'){
                $order_by .= 'CONCAT(b.firstname," ",b.lastname)';    
            }elseif($params['sort'] == 'code'){
                $order_by .= 'b.code'; 
            }elseif($params['sort'] == 'time_effective'){
                $order_by .= 'a.time_effective'; 
            }else{
                $order_by .= 'a.created_by';
            }

            $order_by .= $collate.$desc;
			$select->order($order_by);
        }else{
        	$select->order('created_at DESC');
        }

        if(isset($limit) AND $limit AND !$params['export']){
            $select->limitPage($page,$limit);
        }
        PC::debug($select->__toString());
//        echo $select->__toString();
//        exit;
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

}