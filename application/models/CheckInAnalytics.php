<?php

class Application_Model_CheckInAnalytics extends Zend_Db_Table_Abstract
{
    protected $_name = 'check_in';

    public function get_check_in_list($params)
    {
        $db = Zend_Registry::get('db');

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $userCode = null;

        $stmt = $db->prepare("CALL `get_check_in_list`(:name, :code, :from_date, :to_date, :limit, :offset, :user_code)");

        $stmt->bindParam("name", $params['name'], PDO::PARAM_STR);
        $stmt->bindParam("code", $params['code'], PDO::PARAM_STR);
        $stmt->bindParam("from_date", $params['from_date'], PDO::PARAM_STR);
        $stmt->bindParam("to_date", $params[ 'to_date'], PDO::PARAM_STR);
        
        if($userStorage->group_id == HR_ID)
        {
            $stmt->bindParam("user_code", $userCode, PDO::PARAM_STR);
        }
        else
        {
            $stmt->bindParam("user_code", $userStorage->code, PDO::PARAM_STR);
        }

        $stmt->bindParam("limit", $params['limit'], PDO::PARAM_INT);
        $stmt->bindParam("offset", $params['offset'], PDO::PARAM_INT);

        $stmt->execute();

        $data = array();

        $data['data'] = $stmt->fetchAll();

        $stmt->closeCursor();

        $data['total'] = $db->query("SELECT FOUND_ROWS()")->fetchColumn();
        $db = $stmt = null;
        
        return $data; 
        
    }

    public function getListCodePermission($code)
    {
        $db = Zend_Registry::get('db');

        $stmt = $db->prepare('call `get_code_permission`(:code)');

        $stmt->bindParam('code', $code, PDO::PARAM_STR);
        
        $stmt->execute();
        $data = $stmt->fetch();
        $stmt->closeCursor();
        $stmt = $db = null;
        return $data['list_code_permission'];
    }

    public function xuat_luoi($params = array())
    {
        $db = Zend_Registry::get('db');

        $sql = "SELECT
                    st.code as `code`,
                    CONCAT(st.firstname, ' ', st.lastname) AS `staff_name`,
                    t.name AS `department`,
                    ad.date as `check_in_day`,
                    cid.check_in_at as `check_in_at`,
                    cid.check_out_at as `check_out_at`,
                    cid.check_in_late_time as `late`,
                    cid.check_out_soon_time as `soon`,
                    IF(cgm.company_group = 6 AND ad.is_off = 0,
                        1,
                        IF( 
                            (TIME(cid.check_in_at) BETWEEN cis.begin_break_time AND cis.end_break_time) OR (TIME(cid.check_out_at) BETWEEN cis.begin_break_time AND cis.end_break_time), 0.5,
                            TIME_TO_SEC(TIMEDIFF( IF(TIME(cid.check_out_at)>cis.end OR DATE(cid.check_out_at) > cid.check_in_day ,cis.end, TIME(cid.check_out_at)), IF(TIME(cid.check_in_at)<cis.begin,cis.begin, TIME(cid.check_in_at)) )) / TIME_TO_SEC(TIMEDIFF(cis.end, cis.begin))
                        )
                    ) as `cong`,
                    TIME_TO_SEC(cid.realtime)/3600 as `total`,
                    (TIME_TO_SEC(TIMEDIFF(cis.end, cis.begin)) - TIME_TO_SEC(TIMEDIFF(cis.end_break_time, cis.begin_break_time)))/3600 as `time_per_day`
                FROM `check_in_machine_map` cimm
                JOIN `staff` AS st ON st.code = cimm.staff_code
                LEFT JOIN `company_group_map` AS cgm ON cgm.title = st.title 
                JOIN `all_date` ad ON ad.date BETWEEN :from_date AND :to_date
                JOIN `check_in_detail` cid ON cid.staff_code = st.code AND cid.check_in_day = ad.date
                LEFT JOIN `check_in_shift` cis ON cis.staff_code = st.code
                LEFT JOIN
                    check_in_shift cis2
                    ON cid.staff_code = cis2.staff_code
                        AND cis.date = cis2.date
                JOIN `team` t ON st.department = t.id
                WHERE st.status = 1 AND (cis2.id IS NULL OR cis.date IS NOT NULL) AND t.id = 153
                GROUP BY st.id, ad.date
                ORDER BY st.id, ad.date";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('from_date', $params['from_date'], PDO::PARAM_STR);
        $stmt->bindParam('to_date', $params['to_date'], PDO::PARAM_STR);

        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $stmt = $db = null;

        return $data;
    }

    public function getDateInfo($params = array())
    {
        $db = Zend_Registry::get('db');

        $sql = "SELECT *
                FROM `all_date`
                WHERE `date` BETWEEN :from_date AND :to_date";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('from_date', $params['from'], PDO::PARAM_STR);
        $stmt->bindParam('to_date', $params['to'], PDO::PARAM_STR);
        $stmt->execute();

        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $stmt = $db = null;
        return $data;
    }

    public function getByDepartment($params = array())
    {
        $db = Zend_Registry::get('db');

        $sql = "SELECT
                    st.code as `code`,
                    CONCAT(st.firstname, ' ', st.lastname) AS `staff_name`,
                    t.name AS `title`,
                    st.department AS `department_id`,
                    t2.name AS `department_name`,
                    ad.date as `check_in_day`,
                    IF( (cgm.company_group = 6 AND ad.is_off = 0) OR (ti.status = 1 AND ti.shift = 0 AND ti.off <> 1),
                        1,
                        IF( 
                            (TIME(cid.check_in_at) BETWEEN cis.begin_break_time AND cis.end_break_time) OR (TIME(cid.check_out_at) BETWEEN cis.begin_break_time AND cis.end_break_time), 0.5,
                            TIME_TO_SEC(TIMEDIFF( IF(TIME(cid.check_out_at)>cis.end OR DATE(cid.check_out_at) > cid.check_in_day ,cis.end, TIME(cid.check_out_at)), IF(TIME(cid.check_in_at)<cis.begin,cis.begin, TIME(cid.check_in_at)) )) / TIME_TO_SEC(TIMEDIFF(cis.end, cis.begin))
                        )
                    ) as `cong`
                FROM `check_in_machine_map` cimm
                JOIN `staff` AS st ON st.code = cimm.staff_code
                LEFT JOIN `company_group_map` AS cgm ON cgm.title = st.title 
                LEFT JOIN `all_date` ad ON ad.date BETWEEN :from_date AND :to_date
                LEFT JOIN `check_in_detail` cid ON cid.staff_code = st.code AND cid.check_in_day = ad.date
                LEFT JOIN `check_in_shift` cis ON cis.staff_code = cid.staff_code
                LEFT JOIN `time` AS ti ON ad.date = DATE(ti.created_at) AND ti.staff_id = st.id
                JOIN `team` t ON st.title = t.id
                JOIN `team` t2 ON st.department = t2.id
                WHERE st.status = 1";

        $stmt = $db->prepare($sql);
        $stmt->bindParam('from_date', $params['from'], PDO::PARAM_STR);
        $stmt->bindParam('to_date', $params['to'], PDO::PARAM_STR);

        $stmt->execute();
        $data = $stmt->fetchAll();

        $stmt->closeCursor();
        $stmt = $db = null;

        $data_map = array();

        foreach ($data as $key => $value) 
        {
            if(empty($data_map[$value['department_id']]['name']))
            {
                $data_map[$value['department_id']]['name'] = $value['department_name'];
                $data_map[$value['department_id']]['list_staff'] = array();
            }

            if(empty($data_map[$value['department_id']]['list_staff'][$value['code']]['name']))
            {
                $data_map[$value['department_id']]['list_staff'][$value['code']]['name'] = $value['staff_name'];
                $data_map[$value['department_id']]['list_staff'][$value['code']]['title'] = $value['title'];
                $data_map[$value['department_id']]['list_staff'][$value['code']]['code'] = $value['code'];
                $data_map[$value['department_id']]['list_staff'][$value['code']]['list_time'] = array();
            }

            $data_map[$value['department_id']]['list_staff'][$value['code']]['list_time'][intval(date('d', strtotime($value['check_in_day'])))] = $value['cong'];
        }

        return $data_map;
    }
}