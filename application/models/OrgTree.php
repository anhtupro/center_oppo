<?php

class Application_Model_OrgTree extends Zend_Db_Table_Abstract {

    protected $_name = "org_tree";

    public function getListTree($id_flag,$level_chart) {
        
            
        $db = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('ot' => $this->_name), array('ot.id', 'ot.parent_id', 'ot.left', 'ot.right',  'ot.color_label','ot.flag_org_tree'))
                ->joinLeft(array('dot' => "detail_org_tree"),"dot.id_org_tree = ot.id",array("dot.group"))
                ->joinLeft(array('st' => "staff")," st.id = dot.staff_id", array('CONCAT( st.firstname, " ", st.lastname ) AS name','st.gender'))
                ->joinLeft(array('tn' => "team_new")," tn.id = dot.id_label",array("tn.name as title","tn.id as id_title"));
        if(!empty($id_flag)) {
            $select->where("ot.flag_org_tree = ? ", $id_flag);
        }
        $select->where("ot.level_chart = ? ", $level_chart);
        $select->order('ot.order_paint');
        $result = $db->fetchAll($select);
        return $result;
    }
    
   

    public function getStaff($input = '') {
        $db = Zend_Registry::get('db');

        $sql = "SELECT st.id as `id`, st.code as `code`, CONCAT(st.firstname, ' ', st.lastname) as `name`
                    FROM staff st
                    WHERE st.code = :input_code
                        OR st.email = :input_email 
                        OR CONCAT(st.firstname, ' ', st.lastname) LIKE :input_name";

        $input_code = $input;
        $input_email = $input . "@oppomobile.vn";
        $input_name = "%" . $input . "%";

        $stmt = $db->prepare($sql);

        $stmt->bindParam("input_code", $input_code, PDO::PARAM_STR);
        $stmt->bindParam("input_email", $input_email, PDO::PARAM_STR);
        $stmt->bindParam("input_name", $input_name, PDO::PARAM_STR);

        $stmt->execute();

        $data = $stmt->fetchAll();

        $stmt->closeCursor();
        $stmt = $db = null;

        return $data;
    }
    
    
    public function addNodeTree($color_label_node , $parent_id_node , $flag_id,$level_chart){
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare("call `add_node_org_tree`( :color_label_node, :parent_id_node, :flag_id, :level_chart)");
        $stmt->bindParam("color_label_node", $color_label_node, PDO::PARAM_STR);
        $stmt->bindParam("parent_id_node", $parent_id_node, PDO::PARAM_INT);
        $stmt->bindParam("flag_id", $flag_id, PDO::PARAM_INT);
        $stmt->bindParam("level_chart", $level_chart, PDO::PARAM_INT);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data[0]["last_id"];
    }
    
    public function deleteNode($flag_id , $my_id,$level_chart){
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare("call `delete_node`(:flag_id, :my_id,:level_chart_node)");
        $stmt->bindParam("flag_id", $flag_id, PDO::PARAM_INT);
        $stmt->bindParam("my_id", $my_id, PDO::PARAM_INT);
        $stmt->bindParam("level_chart_node", $level_chart, PDO::PARAM_INT);
        $stmt->execute();
    }
    public function getNodeParent($id_node){
        $db = Zend_Registry::get('db');
        
    }
    public function getDetailTreeByFlag($flag_id)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('ot' => $this->_name), array())
            ->joinLeft(array('dot' => "detail_org_tree"), "dot.id_org_tree = ot.id", array("dot.id_org_tree", "dot.id_label", "dot.staff_id", "dot.group"));
        $select->where("ot.flag_org_tree = ? ", $flag_id);
        $result = $db->fetchAll($select);
        return $result;
    }
}
