<?php
class Application_Model_AppCheckShopFile extends Zend_Db_Table_Abstract
{
	protected $_name = 'app_checkshop_file';

	protected $_schema = DATABASE_TRADE;

    /**
     * @param $params
     * @return bool|array
     * @throws Zend_Exception
     */
    function getFileCheckshop($params){
        if(empty($params['store_id']) ){
            return false;
        }
        $db = Zend_Registry::get('db');
        $arrCols = array(
            'f.url',
            'f.type'
        );
        $select = $db->select()
            ->from(array('f'=> DATABASE_TRADE.'.app_checkshop_file'), $arrCols);
        $select->where('f.checkshop_id = (SELECT MAX(id) FROM `'.DATABASE_TRADE.'`.`app_checkshop` WHERE store_id = ?)', $params['store_id']);
        $result = $db->fetchAll($select);
        return $result;
    }


     public function getImgCheckshop($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();
        $arrCols = array(
           "url"   => "f.url",
           "type"  => "f.type"
        );

        $select->from(array('p' => DATABASE_TRADE.'.app_checkshop'), $arrCols);
        $select->joinLeft(array('f' => DATABASE_TRADE.'.app_checkshop_file'), 'f.checkshop_id = p.id', array());
        $select->where('p.store_id = ?', $params['store_id']);
        $select->where('p.updated_at IS NOT NULL');
        $select->where('p.updated_at = (
                        SELECT MAX(p.updated_at)
                        FROM `'.DATABASE_TRADE.'`.app_checkshop p
                        WHERE p.store_id = '.$params['store_id'].' AND p.updated_at IS NOT NULL)');
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getImageByMonth($store_id, $from_date, $to_date)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE . '.app_checkshop'], [
                'f.url',
                'f.type',
                'month' => 'MONTH(a.created_at)',
                'a.created_at',
                'f.checkshop_id'
            ])
            ->joinLeft(['f' => DATABASE_TRADE . '.app_checkshop_file'], 'a.id = f.checkshop_id', [])
            ->where('a.created_at >= ?', $from_date)
            ->where('a.created_at <= ?', $to_date)
            ->where('a.store_id = ?', $store_id);
        
        $result = $db->fetchAll($select);

        $list = [];
        foreach ($result as $item) {
            $list [$item['month']] [$item['checkshop_id']] ['list_image'] []= $item;
            $list [$item['month']] [$item['checkshop_id']] ['created_at'] = $item['created_at'];
        }

        return $list;
    }
}