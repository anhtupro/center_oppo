<?php

class Application_Model_CheckShopOutsideDetail extends Zend_Db_Table_Abstract
{
    protected $_name = 'check_shop_outside_detail';
    protected $_schema = DATABASE_TRADE;

    public function getDetail($checkShopId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['o' => DATABASE_TRADE . '.check_shop_outside'], [
                'd1.category_id',
                'category_name' => 'c.name',
                'new_quantity' => 'd1.quantity', // the latest check shop
                'old_quantity' => 'd2.quantity' // before the latest check shop
            ])
            ->joinLeft(['d1' => DATABASE_TRADE.'.check_shop_outside_detail'], 'o.id = d1.check_shop_outside_id', [])
            ->joinLeft(['d2' => DATABASE_TRADE.'.check_shop_outside_detail'], 'o.last_check_shop_id = d2.check_shop_outside_id AND d1.category_id = d2.category_id', [])
            ->joinLeft(['c' => DATABASE_TRADE . '.category'], 'd1.category_id = c.id', [])
            ->where('o.id = ?', $checkShopId);

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getDataExport($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['s' => DATABASE_TRADE.'.store_outside'], [
                         'store_id' => 's.id',
                         'store_name' => 's.name',
                         'category_name' => 'c.name',
                         'd.quantity',
                         'brand_name' => 'b.name',
                         'created_by' => "CONCAT(st.firstname, ' ', st.lastname)",
                         'area_name' => 'a.name',
                         'last_date_checkshop' => 'o.created_at'
                     ])
            ->join(['o' => DATABASE_TRADE.'.check_shop_outside'], 's.id = o.store_id AND o.created_at = (SELECT MAX(created_at) FROM trade_marketing.check_shop_outside WHERE store_id = s.id)', [])
            ->joinLeft(['d' => DATABASE_TRADE.'.check_shop_outside_detail_brand'], 'o.id = d.checkshop_id', [])
            ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'd.category_id = c.id', [])
            ->joinLeft(['b' => 'brand'], 'd.brand_id = b.id', [])
            ->joinLeft(['r' => 'regional_market'], 's.regional_market = r.id', [])
            ->joinLeft(['a' => 'area'], 'r.area_id = a.id', [])
            ->joinLeft(['st' => 'staff'], 'o.created_by = st.id', []);

        if ($params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        if ($params['area_id']) {
            $select->where('r.area_id = ?', $params['area_id']);
        }

        if ($params['regional_market']) {
            $select->where('r.id = ?', $params['regional_market']);
        }

        if ($params['district']) {
            $select->where('r.id = ?', $params['district']);
        }

        $result = $db->fetchALl($select);

        return $result;
    }

    public function export($params)
    {
        include 'PHPExcel.php';

        $data = $this->getDataExport($params);

        $objExcel = new PHPExcel();
        $objExcel->setActiveSheetIndex(0);

        $sheet = $objExcel->getActiveSheet()->setTitle('Check shop outside');

        $rowCount = 2;
        $index = 1;
        $sheet->setCellValue('A1', 'STT');
        $sheet->setCellValue('B1', 'ID shop');
        $sheet->setCellValue('C1', 'Tên shpo');
        $sheet->setCellValue('D1', 'Khu vực');
        $sheet->setCellValue('E1', 'Ngày check shop gần nhất');
        $sheet->setCellValue('F1', 'Người check shop');
        $sheet->setCellValue('G1', 'Hạng mục');
        $sheet->setCellValue('H1', 'Số lượng');
        $sheet->setCellValue('I1', 'Hãng');

        foreach ($data as $element) {
            $sheet->setCellValue('A' . $rowCount, $index);
            $sheet->setCellValue('B' . $rowCount, $element['store_id']);
            $sheet->setCellValue('C' . $rowCount, $element['store_name']);
            $sheet->setCellValue('D' . $rowCount, $element['area_name']);
            $sheet->setCellValue('E' . $rowCount, date('d/m/Y H:i:s', strtotime($element['last_date_checkshop'])));
            $sheet->setCellValue('F' . $rowCount, $element['created_by']);
            $sheet->setCellValue('G' . $rowCount, $element['category_name'] );
            $sheet->setCellValue('H' . $rowCount, $element['quantity'] );
            $sheet->setCellValue('I' . $rowCount, $element['brand_name'] );

            ++$index;
            ++$rowCount;
        }

        //style sheet
        $style_border = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $sheet->getStyle('A0:I' . $rowCount)->applyFromArray($style_border);

        for ($i = 'A'; $i < 'I' ; ++$i) {
            $sheet->getStyle($i. 1)->getFont()->setBold(true);
        }


        $objWriter = new PHPExcel_Writer_Excel2007($objExcel);
        $filename = 'Check shop outside';
        $objWriter->save($filename);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Set-Cookie: fileLoading=true');
        readfile($filename);
        exit;
    }
}