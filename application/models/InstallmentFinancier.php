<?php

class Application_Model_InstallmentFinancier extends Zend_Db_Table_Abstract
{
    protected $_name = 'installment_financier';

    public function getAll()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['f' => DATABASE_CENTER . '.installment_financier'], [
                'f.*'
            ])
            ->where('f.del = ?', 0);

        $result = $db->fetchAll($select);

        return $result;
    }
}