<?php

class Application_Model_AdditionalPartCost extends Zend_Db_Table_Abstract
{
    protected $_name = 'additional_part_cost';
    protected $_schema = DATABASE_TRADE;

    public function getCost($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE.'.additional_part_cost'], [
                'a.*'
            ])
        ->joinLeft(['p' => DATABASE_TRADE.'.additional_part'], 'a.additional_part_id = p.id', []);
        
        if ($params['additional_part_type']) {
            $select->where('p.type IN (?)', $params['additional_part_type']);
        }

        if ($params['season']) {
            $select->where('a.season = ?', $params['season']);
        }

        if ($params['year']) {
            $select->where('a.year = ?', $params['year']);
        }

        if ($params['month']) {
            $select->where('a.month = ?', $params['month']);
        }

//        if ($params['from_date']) {
//            $select->where('a.date >= ?', $params['from_date']);
//        }
//
//        if ($params['to_date']) {
//            $select->where('a.date <= ?', $params['to_date']);
//        }

        if ($params['from_date']) {
            if ($params['from_date'] <= '2019-03-31' && $params['to_date'] <= '2019-03-31') {
                $select->where("a.date BETWEEN '2019-01-01' AND '2019-03-31'");
            } else {
                $select->where('a.date >= ?', $params['from_date']);
            }
        }

        if ($params['to_date']) {
            if ($params['from_date'] <= '2019-03-31' && $params['to_date'] <= '2019-03-31') {
                $select->where("a.date BETWEEN '2019-01-01' AND '2019-03-31'");
            } else {
                $select->where('a.date <= ?', $params['to_date']);
            }
        }

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['area_id']] [$element['additional_part_id']] ['cost'] += $element['cost'];
            $list [$element['area_id']] [$element['additional_part_id']] ['date'] += $element['date'];
        }
        return $list;
    }
}    