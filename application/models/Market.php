<?php
class Application_Model_Market extends Zend_Db_Table_Abstract
{
    protected $_name = 'market';
    protected $_schema = WAREHOUSE_DB;
    
    public function reportSellIn($params)
    {
        if (!isset($params['from']) || !$params['from'])
            return false;

        if (!isset($params['to']) || !$params['to'])
            return false;

        $from = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
        $to = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");
        
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('m' => WAREHOUSE_DB.'.'.$this->_name), array('sum_acc' => 'SUM(m.num)'))
            ->join(array('d' => WAREHOUSE_DB.'.distributor'), 'd.id = m.d_id', array())
            ->join(array('r' => 'regional_market'), 'r.id = d.district', array())
            ->join(array('r2' => 'regional_market'), 'r2.id = r.parent', array())
            ->join(array('a' => 'area'), 'r2.area_id = a.id', array('name' => 'a.name', 'area_id' => 'a.id'));
               
        //m.ty : 1:for retailer, 2:for demo, 3:for staff
        $select->where('m.type = ?',1);
        $select->where('m.isbacks = ?',0);
        $select->where('m.canceled = ?',0);
        
        if (isset($params['from']) and $params['from']) {
            $select->where('m.print_time >= ?',$from);
        }
        
        if (isset($params['to']) and $params['to']) {
            $select->where('m.print_time <= ?',$to);
        }
        
        if (isset($params['area']) and $params['area']) {
            $select->where('a.id = ?', $params['area']);
        }
        
        if (isset($params['area']) and $params['area']) {
            $select->where('a.id = ?', $params['area']);
        }
        
        if (isset($params['list_area']) and $params['list_area']) {
            $select->where('a.id IN (?)', $params['list_area']);
        }        
        
        //M?c d?nh l� t�nh IA
        if (isset($params['type']) and $params['type']) {
            $select->where('d.is_ka = ?',$params['type']);
        }
        else{
            $select->where('d.is_ka = ?',0);
        }        
        
        //m?c d?nh l� t�nh cho ph? ki?n
        if (isset($params['cat']) and $params['cat']) {
            $select->where('m.cat_id = ?',$params['cat']);
        }
        else{
            $select->where('m.cat_id = ?',ACCESS_CAT_ID);
        }
        
        
        $select->group('a.id');     
        $result = $db->fetchAll($select);

        return $result;
    }
    
    
    public function showSellInByArea($params)
    {
        if (!isset($params['from']) || !$params['from'])
            return false;

        if (!isset($params['to']) || !$params['to'])
            return false;
            
        if (!isset($params['area']) || !$params['area'])
            return false;

        $from = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
        $to = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");
        
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('m' => WAREHOUSE_DB.'.'.$this->_name), array('count_good' => 'SUM(m.num)'))
            ->join(array('d' => WAREHOUSE_DB.'.distributor'), 'd.id = m.d_id', array())
            ->join(array('r' => 'regional_market'), 'r.id = d.district', array())
            ->join(array('r2' => 'regional_market'), 'r2.id = r.parent', array())
            ->join(array('a' => 'area'), 'r2.area_id = a.id', array('a.name'))
            ->join(array('g' => WAREHOUSE_DB.'.good'), 'g.id = m.good_id', array('name' => 'g.desc'));
        
        $select->where('m.cat_id = ?',ACCESS_CAT_ID);
        $select->where('d.is_ka = ?',0);
        $select->where('m.isbacks = ?',0);
        $select->where('m.canceled = ?',0);
        
        if (isset($params['from']) and $params['from']) {
            $select->where('m.print_time >= ?',$from);
        }
        
        if (isset($params['to']) and $params['to']) {
            $select->where('m.print_time <= ?',$to);
        }
        
        if (isset($params['area']) and $params['area']) {
            $select->where('a.id = ?', $params['area']);
        }
        
        $select->group('m.good_id');
        $select->order('count_good DESC');
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function onlineSellIn($params)
    {  
        if (!isset($params['from']) || !$params['from']) {
            return false;
        }

        if (!isset($params['to']) || !$params['to']) {
            return false;
        }
        
        $from = DateTime::createFromFormat('Y-m-d', $params['from']);
        $from = $from->format('Y-m-d 00:00:00');
     
        $to = DateTime::createFromFormat('Y-m-d', $params['to']);
        $to = $to->format('Y-m-d 23:59:59');
        
        $db = Zend_Registry::get('db');
        
        $arrCols = array(            
            'channel' => "IF(`d`.`parent` = 0, d.id, d.`parent`)",           
        );
        $sub_query_imei = "
            SELECT COUNT(DISTINCT imei_sn) total_activated, good_id, good_color, into_date   
            FROM " . WAREHOUSE_DB . ".imei
            WHERE (into_date BETWEEN '" . $from . " 00:00:00' AND '" . $to . " 23:59:59') AND activated_date IS NOT NULL 
            GROUP BY good_id, good_color, DATE(into_date)
        ";
        $select = $db->select()                
            ->from  (   array('m' => WAREHOUSE_DB.'.'.$this->_name), 
                        array(
                            'month' => 'MONTH(m.add_time)', 
                            'sum_acc' => 'SUM(m.num)', 
                            'total_price' => 'SUM(m.total*0.8)',
                            'total_activated' => 'SUM(IF(m.status =  1, m.num, IF(i.total_activated IS NOT NULL, i.total_activated, 0)))'
                        )
                           
                    )
            ->joinLeft(array('i' => new Zend_Db_Expr('(' . $sub_query_imei . ')')),'m.good_id = i.good_id '
                                                                                    . 'AND m.good_color = i.good_color '
                                                                                    . 'AND DATE(m.invoice_time) = DATE(i.into_date)',array())
            ->join(array('d' => WAREHOUSE_DB.'.distributor'), 'd.id = m.d_id', $arrCols);           
        $select->where('m.type = ?',1);
        $select->where('m.invoice_number IS NOT NULL');
        $select->where('d.online_sale = ?',1);
        
        if (isset($params['from']) and $params['from'] and isset($params['to']) and $params['to']) {
            $select->where("m.invoice_time >= '" . $params['from'] . " 00:00:00' AND m.invoice_time <= '" . $params['to'] . " 23:59:59'");
        }
        
        if (isset($params['channel']) and $params['channel']) {
                $select->where("IF(`d`.`parent` = 0, d.id, d.`parent`) IN (?)", $params['channel']);
        } 
        
        $select->group('IF(`d`.`parent` = 0, d.id, d.`parent`)');
        $select->group('MONTH(m.add_time)'); 
        $result = $db->fetchAll($select);
        return $result;
    }    
    
    public function fetchOnlineSellIn($params) {

        if (!isset($params['from']) || !$params['from']) {
            return false;
        }
        
        if (!isset($params['to']) || !$params['to']) {
            return false;
        }
    
        if (!isset($params['channel']) || !$params['channel']) {
            return false;
        }
        
        $from = $params['from'];
        $to = $params['to'];

        $db     = Zend_Registry::get('db');
        $select = $db->select();
        
        $arrCols = array(
            'd_id'       => 'p.d_id',
            'channel'    => 'IF(d.parent = 0, d.id, d.parent)',
            'sellin'     => 'p.sellin',
            'active'     => 't.active',
            'total_price'  => new Zend_Db_Expr('p.total_price * 0.8'),
            'total_active' => new Zend_Db_Expr('t.active*(p.total_price/p.sellin) * 0.8')
        );
        
        $SellinQuery = "SELECT `d_id`, SUM(num) AS `sellin`, SUM( `price`*`num`) AS `total_price` 
                FROM ".WAREHOUSE_DB.".`market` 
                WHERE (type = 1) 
                AND (invoice_time >= '".$params['from']." 00:00:00') 
                AND (invoice_time <= '".$params['to']." 23:59:59') 
                AND (invoice_number IS NOT NULL) 
                GROUP BY `d_id` ORDER BY `sellin` DESC";
        
        $ActiveQuery = "SELECT `distributor_id` AS `d_id`, COUNT(activated_date) AS `active` 
                FROM ".WAREHOUSE_DB.".`imei` 
                WHERE (distributor_id IS NOT NULL) 
                AND (distributor_id > 0) 
                AND (activated_date >= '".$params['from']." 00:00:00') 
                AND (activated_date <= '".$params['to']." 23:59:59') 
                GROUP BY IFNULL(distributor_id,0)";
     
      
        $select->from(array('d' => WAREHOUSE_DB . '.distributor'), $arrCols);
        $select->joinLeft(array('p'=> new Zend_Db_Expr('(' . $SellinQuery . ')')), 'd.id = p.d_id', array());
        $select->join(array('t'=> new Zend_Db_Expr('(' . $ActiveQuery . ')')), 't.d_id = d.id',array());
        
        $select->where('IF(d.parent = 0, d.id, d.parent) IN (?)', $params['channel']);
        
        return $db->fetchAll($select);
    }
    
    public function OnlineGoodByMonth($params) {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $from = date('Y-m-01', strtotime($params['to'] . ' -6 month'));

        $arrCols = array(
            'good_id'           => "p.good_id",
            'sellin'            => "SUM(p.num)",
            'month'             => "MONTH(invoice_time)",
            'year'              => "YEAR(invoice_time)",
            'sellin_value'      => "SUM(p.total*8/10)",
            'sellin_value_100'  => "SUM(p.total)"            
        );

        $select->from(array('p' => WAREHOUSE_DB.'.'.$this->_name), $arrCols);        
        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = p.d_id', array());
        $select->where('invoice_time >= ?', $from. ' 00:00:00');
        $select->where('invoice_time <= ?', $params['to']. '23:59:59');
        $select->where('IF(d.parent = 0, d.id, d.parent) = ?', $params['channel']);
        $select->where('p.type = ?',1);
        $select->where('p.invoice_number IS NOT NULL');
        $select->where('d.online_sale = ?',1);
        $select->group(['MONTH(invoice_time)']);
        
        $select->order(['MONTH(invoice_time) ASC']);
        
        
        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function onlineGoodByTop($params) {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'good_id' => "p.good_id",
            'sellin' => "SUM(p.num)",
        );

        $select->from(array('p' => WAREHOUSE_DB.'.'.$this->_name), $arrCols);        
        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = p.d_id', array());
        $select->where('invoice_time >= ?', $params['from']. ' 00:00:00');
        $select->where('invoice_time <= ?', $params['to']. ' 23:59:59');
        $select->where('IF(d.parent = 0, d.id, d.parent) IN (?)', $params['channel']);
        $select->where('p.type = ?',1);
        $select->where('p.invoice_number IS NOT NULL');
        $select->where('d.online_sale = ?',1);
        $select->group('p.good_id');
        $result = $db->fetchAll($select);
        
        return $result;
    }
    
    public function onlineGood($params) {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'good_id' => "p.good_id",
            'sellin' => "SUM(p.num)",
            'date'    => "DATE(p.invoice_time)"
        );

        $select->from(array('p' => WAREHOUSE_DB.'.'.$this->_name), $arrCols);        
        $select->joinLeft(array('d' => WAREHOUSE_DB . '.distributor'), 'd.id = p.d_id', array());
        $select->where('invoice_time >= ?', $params['from']. ' 00:00:00');
        $select->where('invoice_time <= ?', $params['to']. ' 23:59:59');
        $select->where('IF(d.parent = 0, d.id, d.parent) = ?', $params['channel']);
        $select->where('p.type = ?',1);
        $select->where('p.invoice_number IS NOT NULL');
        $select->where('d.online_sale = ?',1);
        $select->group(['p.good_id', 'DATE(p.invoice_time)']);
        $result = $db->fetchAll($select);
 
        return $result;
    }
    
    //19/10/2020
    public function listChannelDetails($page, $limit, &$total, $params){
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $nestedSelect = "SELECT `title`, `id`, IF(parent = 0, id, parent) AS `channel`
                          FROM ".WAREHOUSE_DB.".`distributor`
                          WHERE `online_sale` = 1";
        
        $arrCols = array(
            'id'       => 'p.id',
            'title'     => 'p.title'
        );
        if(isset($params['channel']) and $params['channel']){
            $select->where('p.channel IN (?)', $params['channel']);
        }
        $select->from(array('p'=> new Zend_Db_Expr('(' . $nestedSelect . ')')),$arrCols);
        
        $result  = $db->fetchAll($select);
               
        return $result;
    }
    
    public function getSellInChannelDetails($params){
        $db     = Zend_Registry::get("db");
        $select = $db->select();
        
        $arrCols = array(
            'd_id'       => 'p.d_id',
            'sellin'     => 'SUM(p.num)'
            
        );
        
        $select->from(array('p'=> WAREHOUSE_DB.'.market'),$arrCols);
        $select->where('p.type = ?', 1);

        if(isset($params['from']) and $params['from']){
            $select->where('p.invoice_time >= ?', $params['from'].' 00:00:00');
        }
        
        if(isset($params['to']) and $params['to']){
            $select->where('p.invoice_time <= ?', $params['to'].' 23:59:59');
        }
        
        $select->where('p.invoice_number IS NOT NULL',null);
        
        $select->group('p.d_id');
        $select->order('sellin DESC');
        
       
        $result = $db->fetchAll($select);   

        return $result;
    }
    
    public function getActiveAllChannelDetails($params){
        $db     = Zend_Registry::get("db");
        $select = $db->select();
             
        $arrCols = array(
            'd_id'       => 'distributor_id',            
            'active'     => 'COUNT(activated_date)'
        );
        
        $select->from(array('i' => WAREHOUSE_DB.'.imei'),$arrCols);
        $select->where('i.distributor_id IS NOT NULL', null);
        $select->where('i.distributor_id > ?', 0);

        if(isset($params['from']) and $params['from']){
            $select->where('i.activated_date >= ?', $params['from'].' 00:00:00');
        }
        
        if(isset($params['to']) and $params['to']){
            $select->where('i.activated_date <= ?', $params['to'].' 23:59:59');
        }

        $select->group('IFNULL(i.distributor_id,0)');
   
        $result = $db->fetchAll($select);
        return $result;
    }
}