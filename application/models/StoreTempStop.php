<?php

class Application_Model_StoreTempStop extends Zend_Db_Table_Abstract
{
    protected $_name = 'store_temp_stop';
    protected $_schema = DATABASE_TRADE;

    public function getData($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['s' => 'store'], [
                        'area_name' => 'a.name',
                        'province_name' => 'r.name',
                        'district_name' => 'd.name',
                        'store_name' => 's.name',
                        'store_id' => 's.id',
                        'p.status' ,
                         'sale_name' => "CONCAT(f.firstname, ' ', f.lastname)",
                         'channel'           => "(CASE WHEN (l.loyalty_plan_id IS NOT NULL AND di.is_ka = 0) THEN plan.name
                                    WHEN (di.is_ka = 1) THEN 'KA'
                                    WHEN (l.loyalty_plan_id IS NULL AND di.is_ka = 0) THEN 'SHOP THƯỜNG'
                                    END)",
                     ])
            ->joinLeft(array('di' => WAREHOUSE_DB.'.distributor'), 'di.id = s.d_id', array())
            ->joinLeft(array('l' => 'dealer_loyalty'), 'l.dealer_id = (IF((di.parent = 0 OR di.parent IS NULL), di.id, di.parent)) AND l.is_last = 1', array())
              ->joinLeft(array('plan' => 'loyalty_plan'), 'plan.id = l.loyalty_plan_id', array())

            ->joinLeft(['d' => 'regional_market'], 's.district = d.id', [])
        ->joinLeft(['r' => 'regional_market'], 'd.parent = r.id', [])
        ->joinLeft(['a' => 'area'], 'r.area_id = a.id', [])
            ->joinLeft(['p' => DATABASE_TRADE.'.store_temp_stop'], 'p.store_id = s.id', [])
         //    ->joinLeft(['v' => 'v_store_staff_leader_log'], 'v.store_id = s.id', [])
            ->joinLeft(['f' => 'staff'], 'v.staff_id = f.id', [])
        ->where('s.del IS NULL OR s.del = 0')
//        ->where('di.parent IN (2316, 10007, 2363, 2318)'); // tgdđ, viettel, fpt
        ->where('di.is_ka = 1 OR (l.loyalty_plan_id IS NOT NULL AND di.is_ka = 0)');

        if ($params['list_area']) {
            $select->where('a.id IN (?)', $params['list_area']);
        }

        if ($params['staff_id']) {
            $select->where('f.id = ?', $params['staff_id']);
        }

        if ($params['area_id']) {
            $select->where('a.id = ?', $params['area_id']);
        }

        $result = $db->fetchAll($select);

        return $result;
    }

    public function export($params)
    {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $data = $this->getData($params);
    

        $heads = array(
            'Store ID',
            'Channel',
            'Tên shop',
            'Khu vực',
            'Tỉnh',
            'Quận huyện',
            'Sale quản lý',
            'Trạng thái'
        );


        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($data as $item){
            $alpha    = 'A';
            if ($item['status'] == 1) {
                $status_name = 'Shop đã tạm đóng';
            } elseif ($item['status'] == 2) {
                $status_name = 'Shop đã rút demo';
            } elseif ($item['status'] == 3) {
                $status_name = 'Shop đã trở lại hoạt động';
            } else {
                $status_name = '';
            }

            $sheet->setCellValue($alpha++.$index, $item['store_id']);
            $sheet->setCellValue($alpha++.$index, $item['channel']);
            $sheet->setCellValue($alpha++.$index, $item['store_name']);
            $sheet->setCellValue($alpha++.$index, $item['area_name']);
            $sheet->setCellValue($alpha++.$index, $item['province_name']);
            $sheet->setCellValue($alpha++.$index, $item['district_name']);
            $sheet->setCellValue($alpha++.$index, $item['sale_name']);
            $sheet->setCellValue($alpha++.$index, $status_name);

            $index++;
        }

        $filename = 'Trang thai shop CORONA' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }


    public function statisticShopKA($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['a' => DATABASE_TRADE.'.store_temp_stop'], [
                        'd.parent',
                         'count_stop' => "SUM(IF(a.status = 1, 1, 0))",
                         'count_demo' => "SUM(IF(a.status = 2, 1, 0))"
                     ])
        ->joinLeft(['s' => 'store'], 'a.store_id = s.id', [])
             ->joinLeft(array('d' => WAREHOUSE_DB.'.distributor'), 'd.id = s.d_id', array())
            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'di.parent = r.id', [])
            ->joinLeft(['e' => 'area'], 'r.area_id = e.id', [])
         //    ->joinLeft(['v' => 'v_store_staff_leader_log'], 'v.store_id = s.id', [])

            ->where('s.del IS NULL OR s.del = 0')
            ->where('d.parent IN (2316, 10007, 2363, 2318)') // tgdđ, viettel, fpt
        ->group(['d.parent']);

        if ($params['list_area']) {
            $select->where('e.id IN (?)', $params['list_area']);
        }

        if ($params['staff_id']) {
            $select->where('v.staff_id = ?', $params['staff_id']);
        }

        if ($params['area_id']) {
            $select->where('e.id = ?', $params['area_id']);
        }

        if ($_GET['dev']) {
            echo $select->__toString();
            die;
        }

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['parent']] = $element;
        }

        return $list;
    }

    public function statisticShopIND($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['a' => DATABASE_TRADE.'.store_temp_stop'], [
                         'count_stop' => "SUM(IF(a.status = 1, 1, 0))",
                         'count_demo' => "SUM(IF(a.status = 2, 1, 0))"
                     ])
        ->joinLeft(['s' => 'store'], 'a.store_id = s.id', [])
             ->joinLeft(array('d' => WAREHOUSE_DB.'.distributor'), 'd.id = s.d_id', array())
              ->joinLeft(array('l' => 'dealer_loyalty'), 'l.dealer_id = (IF((d.parent = 0 OR d.parent IS NULL), d.id, d.parent)) AND l.is_last = 1', array())

            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'di.parent = r.id', [])
            ->joinLeft(['e' => 'area'], 'r.area_id = e.id', [])
        //     ->joinLeft(['v' => 'v_store_staff_leader_log'], 'v.store_id = s.id', [])

            ->where('s.del IS NULL OR s.del = 0')
            ->where('l.loyalty_plan_id IS NOT NULL AND d.is_ka = 0');


        if ($params['list_area']) {
            $select->where('e.id IN (?)', $params['list_area']);
        }

        if ($params['staff_id']) {
            $select->where('v.staff_id = ?', $params['staff_id']);
        }

        if ($params['area_id']) {
            $select->where('e.id = ?', $params['area_id']);
        }


        $result = $db->fetchRow($select);
   
        return $result;
    }
}