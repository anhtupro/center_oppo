<?php
class Application_Model_Inform extends Zend_Db_Table_Abstract
{
	protected $_name = 'inform';

    function fetchPagination($page, $limit, &$total, $params) {
        $db = Zend_Registry::get('db');

        if (isset($params['group_cat']) and $params['group_cat']){
            $select = $db->select();
            $select_fields = array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'), 'p.*' );

            if (isset($params['get_fields']) and is_array($params['get_fields']))
                foreach ($params['get_fields'] as $get_field)
                    array_push($select_fields, $get_field);
            else
                array_push($select_fields, 'p.*');

            $select
                ->from(array('p' => $this->_name),
                    $select_fields
                )
                ->joinLeft(array('o' => 'inform_title'), 'o.id = p.id', array());
                if(isset($params['allow_inform']) and $params['allow_inform'] == 1){
                    $select->joinLeft(array('o1' => 'inform_title'), 'o1.id = p.id', array());
                }
                $select->group('p.category_id');

        } 
        else
        {
             $select = $db->select()
            ->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'))
            ->joinLeft(array('o' => 'inform_title'), 'o.id = p.id', array());
            if(isset($params['allow_inform']) and $params['allow_inform'] == 1){
                $select->joinLeft(array('o1' => 'inform_title'), 'o1.id = p.id', array());
            }
             $select->group('p.id');
        }
       
        if (isset($params['allow_inform']) and $params['allow_inform'] == 1) {
            
            if (isset($params['title_id_all']) and $params['title_id_all']) {
                $select->where('o.object_id = ' . My_Util::escape_string($params['title_id_all']) . ' and o.type = ' . My_Notification::TITLE);
            } else {
                $select->where('o.object_id = -1');
            }
            if (isset($params['area_id_all']) and $params['area_id_all']) {
                $select->where('o1.object_id = ' . My_Util::escape_string($params['area_id_all']) . ' and o1.type = ' . My_Notification::AREA);
            } else {
                $select->where('o1.object_id = -1');
            }
        }
        
        if (isset($params['id']) and $params['id']){
            $select->where('p.id = ?', $params['id']);
        }

        if (isset($params['title']) and $params['title']){
            $select->where('p.title LIKE ?', '%'.$params['title'].'%');
        }

        if (isset($params['content']) and $params['content']){
            $select->where('p.content LIKE ?', '%'.$params['content'].'%');
        }

        $select_tmp = $db->select();

        if (isset($params['team']) && $params['team'])
           {
              $select->where('o.object_id = ? ', $params['team']);
           }
           
          

        if (isset($conditions) && count($conditions))
            $select->where(implode(' ', $conditions));
        

        if (isset($params['created_from']) && $params['created_from']) {
            $select->where('p.created_at >= ?', $params['created_from']);
        }

        if (isset($params['created_to']) && $params['created_to']) {
            $select->where('p.created_at <= ?', $params['created_to']);
        }
		
		if (isset($params['category']) && $params['category']) {
            $select->where('p.category_id = ?', $params['category']);
        }
		
        if (isset($params['status']) && $params['status'] != -1){
            $select->where('p.status = ?', $params['status']);
        }

        if(isset($params['sort']) && $params['sort']) {
            $desc = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';
            $collate = ' COLLATE utf8_unicode_ci ';
            $order_str = 'p.`'.$params['sort']. '` ' . $collate . $desc;

            $select->order(new Zend_Db_Expr($order_str));
        } else {
        	$select->order('created_at DESC');
        }

        if($_GET['dev']){
            echo $select->__toString();
            exit;
        }

    //    PC::debug($select->__toString());
		 $select->limitPage($page, $limit);
	
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }
	
	  function getListFaq($page, $limit, &$total, $params){
        $staff_id   = $params['staff_id'];
        $category_id = $params['category_id'];
        $db         = Zend_Registry::get('db');
        $select     = $db->select()->from(array('p' => $this->_name),
                                    array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));
        $select_filter = array(
            'c_area'       => new Zend_Db_Expr(sprintf('SUM(CASE WHEN o.object_id = a.id AND o.type = %d THEN 1 ELSE 0 END)', My_Notification::AREA)),
            'c_title'      => new Zend_Db_Expr(sprintf('SUM(CASE WHEN o.object_id = s.title AND o.type = %d THEN 1 ELSE 0 END)', My_Notification::TITLE))
        );
        $select ->joinLeft(array('o' => 'inform_title'), 'o.id=p.id', $select_filter)
                ->joinLeft(array('s' => 'staff'), "1 = 1", array())
                ->join(array('r' => 'regional_market'), 'r.id=s.regional_market', array())
                ->join(array('a' => 'area'), 'a.id=r.area_id', array());
        $select ->where('s.id = ?',$staff_id);
        $select ->where('p.category_id = ?',$category_id);
        $select ->group('p.id');
		$select ->where('p.status = 1');
        $select ->having('(c_area > ? AND c_title > ?)', 0);
        
        // echo $select->__toString();
        $result = $db->fetchAll($select);
        $total  = $db->fetchOne("select FOUND_ROWS()");

        if($_GET['dev'] == 1){
            echo $select->__toString();
        }

        return $result;
         
    }
	
	function listFaq($page, $limit, &$total, $params){
            $staff_id   = $params['staff_id'];
            $db         = Zend_Registry::get('db');
            $select = $db->select();
            $select_fields = array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'), 'p.*' );

            if (isset($params['get_fields']) and is_array($params['get_fields']))
                foreach ($params['get_fields'] as $get_field)
                    array_push($select_fields, $get_field);
            else
                array_push($select_fields, 'p.*');
                $select_filter = array(
                    'c_area'       => new Zend_Db_Expr(sprintf('SUM(CASE WHEN o.object_id = a.id AND o.type = %d THEN 1 ELSE 0 END)', My_Notification::AREA)),
                    'c_title'      => new Zend_Db_Expr(sprintf('SUM(CASE WHEN o.object_id = s.title AND o.type = %d THEN 1 ELSE 0 END)', My_Notification::TITLE))
                );
            $select
                ->from(array('p' => $this->_name),
                    $select_fields
                )
                ->joinLeft(array('o' => 'inform_title'), 'o.id = p.id', $select_filter)
                ->joinLeft(array('s' => 'staff'), "1 = 1", array())
                ->join(array('r' => 'regional_market'), 'r.id=s.regional_market', array())
                ->join(array('a' => 'area'), 'a.id=r.area_id', array());
            $select ->where('s.id = ?',$staff_id);
            $select ->where('p.status = 1');
            $select->group('p.category_id');
//            $select ->having('(c_area > ? AND c_title > ?)', 0);
            $select->limitPage($page, $limit);
           // echo $select->__toString();
            $result = $db->fetchAll($select);
            return $result;
         
    }
	
}
