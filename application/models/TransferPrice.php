<?php

class Application_Model_TransferPrice extends Zend_Db_Table_Abstract
{
    protected $_name = 'transfer_price';
    protected $_schema = DATABASE_TRADE;

    public function getTransferQuotation($params)
    {

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $select->from(array('r' => DATABASE_TRADE.'.transfer'), ['r.id', 'tq.*'])
            ->joinLeft(array('tq' => DATABASE_TRADE.'.transfer_price'), 'r.id = tq.transfer_id', [])
            ->where('r.id = ?', $params['transfer_id']);

        if (isset($params['price_type']) && $params['price_type'] == 0) {
            $select->where('tq.status = ?', $params['price_type']);
        }
        if (isset($params['price_type']) && $params['price_type'] != 0) {
            $select->where('tq.status = ?', $params['price_type']);
        }


        $result = $db->fetchAll($select);

        $total_price = 0; 
        
        foreach ($result as $element) {
            $list_price [] = $element;
            $total_price += $element['total_price'] + $element['total_price'] * VAT;
        }

        $result = [
            'list_price' => $list_price,
            'total_price' =>  $total_price
        ];

        return $result;
    }

}