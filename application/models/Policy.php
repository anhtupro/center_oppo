<?php
class Application_Model_Policy extends Zend_Db_Table_Abstract
{
	protected $_name = 'policy';

	private function dbconnect(){
        return Zend_Registry::get('db');
    }

	public function insertPolicy($params = array())
	{
		$data = array(
				'title' => $params['title'], 
				'note' => $params['note'], 
				'create_at' => $params['create_at'],
				'area' => $params['area'],
			);
		
		try
		{
			$this->insert($data);
			return array('message' => 'Thêm policy thành công', 'status' => true);
		}
		catch(Exception $e)
		{
			return array('message' => $e->getMessage(), 'status' => false);
		}
	}

	public function selectPolicy($params = array())
	{
		$db = $this->dbconnect();

		$sql = "
			select FOUND_ROWS() AS total, id, title, note, create_at, update_at, area
			from policy
			where del <> 1
		";

		if(isset($params['limit']) && isset($params['offset']))
		{
			$sql .= " limit :limit offset :offset";
		}

		$stmt = $db->prepare($sql);

		if(isset($params['limit']) && isset($params['offset']))
		{
			$stmt->bindParam('limit', $params['limit'], PDO::PARAM_INT);
        	$stmt->bindParam('offset', $params['offset'], PDO::PARAM_INT);
		}
		
        try
        {
        	$stmt->execute();
            $data['data'] = $stmt->fetchAll();
            $stmt->closeCursor();
            if($data['data']){
                $data['total'] = $db->query('SELECT FOUND_ROWS() AS total')->fetchColumn();
            }
            $data['message'] = 'Done';
            $data['status'] = true;
            $db = $stmt = null;
        }
        catch(Exception $e)
        {
        	$data['status'] = false;
        	$data['message'] = $e->getMessage();
        }

        return $data;
	}

	public function selectAllPolicy($params = array())
	{
		$db = $this->dbconnect();

		$sql = "
			select id, title, note
			from policy
		";

		$stmt = $db->prepare($sql);

        try
        {
        	$stmt->execute();
            $data['data'] = $stmt->fetchAll();
            $stmt->closeCursor();
            $db = $stmt = null;
        }
        catch(Exception $e)
        {
        	
        }

        return $data;
	}

	public function selectById($params = array())
	{
		$db = $this->dbconnect();

		$sql = "
			select id, title, note, create_at, update_at, area
			from policy
			where id = :id
			limit 1
		";

		$stmt = $db->prepare($sql);
        $stmt->bindParam('id', $params['id'], PDO::PARAM_INT);

        try
        {
        	$stmt->execute();
            $data['data'] = $stmt->fetch();
            $stmt->closeCursor();
            $db = $stmt = null;
        }
        catch(Exception $e)
        {
        	$data['message'] = $e->getMessage();
        }

        return $data;
	}

	public function updatePolicy($params = array())
	{
		$data = array(
				'title' => $params['title'], 
				'note' => $params['note'], 
				'update_at' => $params['update_at'],
				'area' => $params['area'],
			);
		
		try
		{
            $where = $this->getAdapter()->quoteInto('id = ?',$params['id']);
            $this->update($data,$where);
			return array('message' => 'Cập nhật policy thành công', 'status' => true);
		}
		catch(Exception $e)
		{
			return array('message' => $e->getMessage(), 'status' => false);
		}
	}

	public function deletePolicy($params = array())
	{
		try
		{
			if(is_numeric($params['id']) && isset($params['id']) && !empty($params['id']))
			{
				$where = $this->getAdapter()->quoteInto('id = ?',$params['id']);
            	$this->delete($where);
			}
		}
		catch(Exception $e)
		{
			
		}
	}
}