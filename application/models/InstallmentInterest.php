<?php

class Application_Model_InstallmentInterest extends Zend_Db_Table_Abstract
{
    protected $_name = 'installment_interest';

    public function getAll()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['p' => DATABASE_CENTER . '.installment_interest'], [
                'p.*'
            ]);
        $result = $db->fetchAll($select);

        return $result;
    }
}