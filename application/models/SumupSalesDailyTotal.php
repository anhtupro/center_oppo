<?php
class Application_Model_SumupSalesDailyTotal extends Zend_Db_Table_Abstract
{
	protected $_name = 'sumup_sales_daily_total';

	public function getListNeedToReport($staff_id, $date)
	{
        $db = Zend_Registry::get('db');
        $select = $db->select()
                    ->from(['s1' => 'store_staff_log'],[
                        's1.store_id',
                        'store_name' => 's2.name',
                        'ad.date',
                        's3.total_quantity'
                    ])
                    ->join(['s2' => 'store'], 's1.store_id = s2.id AND s2.is_sumup_daily = 1', [])
                    ->joinCross(['ad' => 'all_date'])
                    ->joinLeft(['s3' => 'sumup_sales_daily_total'], "s1.store_id = s3.store_id AND s3.date = ad.date", [])

                    ->where('s1.staff_id = ?',$staff_id)
                    ->where('s1.released_at IS NULL', '')
                    ->where('s1.is_leader = ?', 0 )
                    ->where('ad.date IN (?)', $date)
                    ->where('s3.total_quantity IS NULL')
                    ->where("DATE_FORMAT(FROM_UNIXTIME(s1.joined_at), '%Y-%m-%d') < ?", date('Y-m-d'))
                    ->where('DATE_FORMAT(FROM_UNIXTIME(s1.joined_at), \'%Y-%m-%d\') <= ad.date');

        $remainTotalReportBrand = $db->fetchAll($select);

        return  $remainTotalReportBrand;
	}
	
}