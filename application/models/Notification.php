<?php

class Application_Model_Notification extends Zend_Db_Table_Abstract {

    protected $_name = 'notification_new';

    function fetchPagination($page, $limit, &$total, $params) {
//        return array();
        $db                    = Zend_Registry::get('db');
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $title_notification_pg = "Th�ng b�o v? vi?c chuy?n doanh s? b�n";
        $title_delete_imei     = "Danh s�ch IMEI b? x�a ng�y";
        $salary                = "Phi?u luong";

        $select = $db->select()
                ->from(array('p' => $this->_name), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));
        if (isset($params['id']) and $params['id'])
            $select->where('p.id = ?', $params['id']);

        if (isset($params['title']) and $params['title'])
            $select->where('p.title LIKE ?', '%' . $params['title'] . '%');

        if (isset($params['content']) and $params['content'])
            $select->where('p.content LIKE ?', '%' . $params['content'] . '%');

        // filter who can see the notification
        if (isset($params['filter']) && $params['filter'])
            $select->having('(c_area > ? AND c_department > ? AND c_team > ? AND c_title > ? AND c_company > ?) OR c_staff > ? OR c_all > ?', 0);
        ////////////////////////////

        if (isset($params['read'])) {
            if ($params['read'])
                $select->where('nr.notification_id IS NOT NULL', 1);
            else
                $select->where('nr.notification_id IS NULL', 1);
        }

        if (isset($params['created_from']) && $params['created_from']) {
            $select->where('p.created_at >= ?', $params['created_from']);
        }

        if (isset($params['created_to']) && $params['created_to']) {
            $select->where('p.created_at <= ?', $params['created_to']);
        }

          if (isset($params['filter']) && $params['filter']) {
        $today = date('Y-m-d H:i:s');
        $select->where('p.show_from IS NULL OR p.show_from <= ?', $today);
        $select->where('p.show_to IS NULL OR p.show_to >= ?', $today);
         }

        if (isset($params['staff_id']) && $params['staff_id'])
            $select->where('s.id = ?', $params['staff_id']);

        if (isset($params['filter_display']) && $params['filter_display']) {
            $select->where('p.type =  ?', NOTIFICATION_PRIMARY_TYPE);
        }


        if (isset($params['status']) && $params['status'])
            $select->where('p.status = ?', $params['status']);

        if (isset($params['pop_up']) && $params['pop_up'])
            $select->where('p.pop_up = ?', $params['pop_up']);
        
        $select->where('p.created_by = ?',$userStorage->id );
        
        $select->group('p.id');

        if (isset($params['sort']) && $params['sort']) {
            $desc      = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';
            $collate   = ' COLLATE utf8_unicode_ci ';
            $order_str = 'p.`' . $params['sort'] . '` ' . $collate . $desc;

            $select->order(new Zend_Db_Expr($order_str));
        } else {
            $select->order('created_at DESC');
        }
        //echo $select->__toString();
   
        if ($limit)
            $select->limitPage($page, $limit);


        $result = $db->fetchAll($select);
        $total  = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

}
