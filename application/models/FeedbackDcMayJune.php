<?php
class Application_Model_FeedBackDcMayJune extends Zend_Db_Table_Abstract
{
    protected $_name = 'feedback_dc_may_june';
    protected $_schema = DATABASE_CENTER;

    function getFeedBackDcMayJune($list_area = array()) {
        $db = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('f' => DATABASE_CENTER . '.' . $this->_name), 'f.*')
                ->join(array('a' => DATABASE_CENTER . '.area'), 'a.id = f.id_map_area', array('area_map' => 'a.name'));
        if (!empty($list_area)) {
            $select->where('f.id_map_area IN(?)', $list_area);
        }
        $result = $db->fetchAll($select);
        return $result;
    }
}                                                      
