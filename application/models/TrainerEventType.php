<?php
class Application_Model_TrainerEventType extends Zend_Db_Table_Abstract
{
	protected $_name = 'trainer_event_type';

	public function getKeyValue(){
		$list = $this->fetchAll();
		$result = array();
		foreach($list as $key => $value):
			$result[$value['id']] = $value['name'];
		endforeach;	
		return $result;
	}
}