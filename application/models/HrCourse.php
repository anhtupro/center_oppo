<?php
class Application_Model_HrCourse extends Zend_Db_Table_Abstract
{
    protected $_name = 'hr_course';

    
    public function fetchPagination($page, $limit, &$total, $params){
        
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            "p.*", 'cs.number_of_staff',
            "created_by_name" => "CONCAT(s.firstname, ' ', s.lastname)"
        );
        $gets['number_of_staff']  = new Zend_Db_Expr("COUNT(DISTINCT c.staff_id)");
        $gets[]                   = 'c.course_id';

        $select_staff = $db->select()
            ->from(array('c' => 'hr_course_staff'), $gets)
            ->where('c.del = ?', 0)
            ->group('c.course_id');

        $select->from(array('p' => 'hr_course'), $arrCols);
        $select->joinLeft(array('s' => 'staff'), 's.id = p.created_by', array());
        $select->joinLeft(array('cs' => $select_staff), 'cs.course_id = p.id', array());
        $select->where('p.del = ?', 0);

        if(!empty($params['name'])){
            $select->where("p.name LIKE ?", "%".$params['name']."%");
        }
        
        if(!empty($params['min_number'])){
            $select->where("cs.number_of_staff >= ?", $params['min_number']);
        }

        if(!empty($params['max_number'])){
            $select->where("cs.number_of_staff <= ?", $params['max_number']);
        }

        if(!empty($params['student_type'])){
            $select->where("p.student_type = ?", $params['student_type']);
        }

        if (isset($params['from_date']) AND $params['from_date']) {
            $select->where('p.from_date >= ?', $params['from_date']);
        }

        if (isset($params['to_date']) AND $params['to_date']) {
            $select->where('p.to_date <= ?', $params['to_date']);
        }
        $select->order('p.from_date DESC');
        if(empty($params['export'])){
            $select->limitPage($page, $limit);
        }

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
    
    public function getListStaff($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id',
            'fullname' => "CONCAT(p.firstname, ' ', p.lastname)",
        );

        $select->from(array('p' => 'staff'), $arrCols);
        $select->where('p.off_date IS NULL');
        $select->where('p.is_officer = ?', 1);
        $select->where('p.department = ?', $params['department_id']);
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getListStaffCourse($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            's.id',
            'fullname' => "CONCAT(s.firstname, ' ', s.lastname)",
        );

        $select->from(array('p' => 'hr_course_staff'), $arrCols);
        $select->joinLeft(array('s' => 'staff'), 's.id = p.staff_id', array());
        
        
        $select->where('s.off_date IS NULL');
        $select->where('s.is_officer = ?', 1);
        $select->where('p.course_id = ?', $params['course_id']);
        $select->where('p.del = 0');
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getListCourse($staff_id, $type){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "c.id",
            "c.content", 
            "c.note", 
            "c.name", 
            "c.file",
            "c.from_date",
            'c.type'
        );

        $select->from(array('p' => 'hr_course_staff'), $arrCols);
        $select->joinLeft(array('c' => 'hr_course'), 'c.id = p.course_id', array());

        $select->where('p.del = ?', 0);
        $select->where('p.staff_id = ?', $staff_id);
        $select->where('c.del = ?', 0);
        $select->order('c.from_date DESC');
        if(!empty($type) and $type == 1) {
            $select->where('YEAR(c.from_date) >= YEAR(CURRENT_DATE())');
            $select->where('YEAR(c.to_date)   >= YEAR(CURRENT_DATE())');
        }
        $result = $db->fetchAll($select);

        return $result;
    }
    
    
}