<?php

class Application_Model_GoodKpiLog2 extends Zend_Db_Table_Abstract{

	public function GetAll($pars = array()){
		$pars = array_merge(
			array(
				'product_name' => null,
				'policy' => null,
				'from_date' => null,
				'to_date' => null,
				'title' => null,
				'limit' => null,
				'offset' => null
			),
			$pars
		);
		$db = Zend_Registry::get('db');
		$stmt = $db->prepare('CALL SP_good_kpi_log_GetAll(:product_name, :policy, :from_date, :to_date, 
		 :title, :limit, :offset, @total)');
		$stmt->bindParam('product_name', $pars['product_name'], PDO::PARAM_STR);
		$stmt->bindParam('policy', $pars['policy'], PDO::PARAM_INT);
		$stmt->bindParam('from_date', $pars['from_date'], PDO::PARAM_STR);
		$stmt->bindParam('to_date', $pars['to_date'], PDO::PARAM_STR);
		$stmt->bindParam('title', $pars['title'], PDO::PARAM_INT);
		$stmt->bindParam('limit', $pars['limit'], PDO::PARAM_INT);
		$stmt->bindParam('offset', $pars['offset'], PDO::PARAM_INT);
		$stmt->execute();
		$res['data'] = $stmt->fetchAll();
		$stmt->closeCursor();
		if($res['data']){
			$t = $db->query('SELECT @total AS total')->fetch();
			$res['total'] = $t['total'];
		}
		$db = $stmt = null;
		return $res;
	}

}