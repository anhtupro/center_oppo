<?php

class Application_Model_InventoryCategoryPosmChild extends Zend_Db_Table_Abstract
{
    protected $_name = 'inventory_category_posm_child';
    protected $_schema = DATABASE_TRADE;

    public function get($parentId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['d' => DATABASE_TRADE.'.inventory_category_posm_child'], [
                         'category_id' => 'd.category_id',
                         'category_name' => 'c.name'
                     ])
                    ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'd.category_id = c.id', [])
                    ->where('d.parent_id = ?', $parentId);

        $result = $db->fetchAll($select);

        return $result;
    }
}