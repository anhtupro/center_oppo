<?php

class Application_Model_FlagOrgTree extends Zend_Db_Table_Abstract {

    protected $_name = "flag_org_tree";
    
    public function fetchOneData($id_flag) {
        $db = Zend_Registry::get('db');
        $select = $db->select()
        ->from(array('fot' => $this->_name), array('fot.name'))
        ->where("fot.id = ? ", $id_flag);
        $result = $db->fetchOne($select);
        return $result;
    }
    
}
