<?php
class Application_Model_Lunar2020 extends Zend_Db_Table_Abstract
{
    protected $_name = 'lunar_2020';
    public function check_staff($staff_code){
    	$db = Zend_Registry::get('db');
        $sql = 'SELECT *
				FROM lunar_2020 
				WHERE code = :code';
        $stmt = $db->prepare($sql);
        $stmt->bindParam("code",  $staff_code, PDO::PARAM_STR);
        $stmt->execute();

        $data = $stmt->fetch();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $data;
    }

   
}
