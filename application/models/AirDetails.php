<?php
class Application_Model_AirDetails extends Zend_Db_Table_Abstract
{
    protected $_name = 'air_details';
     protected $_schema = DATABASE_TRADE;

      public function getAirDetails($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.air_id", 
            "p.cate_id", 
            "p.quantity", 
            // "type_title" => "GROUP_CONCAT(DISTINCT type.title)", 
            // "tp_id" => "GROUP_CONCAT(DISTINCT t.air_tp_id)", 
            "category_name" => "c.name", 
            // "p.imei_sn",
            "file_url"  => "GROUP_CONCAT(DISTINCT file.url)",
            "file_contructor"  => "GROUP_CONCAT(DISTINCT file_contructor.url)",
            "file_upload"  => "GROUP_CONCAT(DISTINCT file_upload.url)",
            "file_accept"  => "GROUP_CONCAT(DISTINCT file_accept.url)",
            "file_document"  => "GROUP_CONCAT(DISTINCT file_document.url)",
            "file_nghiemthu"  => "GROUP_CONCAT(DISTINCT file_nghiemthu.url)",
            "image_design"  => "GROUP_CONCAT(DISTINCT image_design.url)",
            "p.note",
            // "p.contructors_id",
            "p.price",
             "p.final_price",
             "p.air_date",
            "contructors_name"  => "contructors.name",
            "p.width",
            "p.wide",
            "p.deep",
            "p.total",
            'p.review_note',
            'p.review_cost',
            'p.month_debt',
            'p.contract_number',

        );

        $select->from(array('p' => DATABASE_TRADE.'.air_details'), $arrCols);
        $select->joinLeft(array('c' => DATABASE_TRADE.'.category'), 'c.id = p.cate_id', array());
        // $select->joinLeft(array('t' => DATABASE_TRADE.'.air_details_type'), 't.repair_details_id = p.id', array());
        // $select->joinLeft(array('type' => DATABASE_TRADE.'.repair_type'), 'type.id = t.repair_type_id', array());
        // $select->joinLeft(array('tp' => DATABASE_TRADE.'.repair_details_tp'), 'tp.repair_details_id = p.id', array());
        $select->joinLeft(array('file' => DATABASE_TRADE.'.air_details_file'), 'file.air_details_id = p.id AND file.type = 1', array());
        $select->joinLeft(array('file_contructor' => DATABASE_TRADE.'.air_details_file'), 'file_contructor.air_details_id = p.id AND file_contructor.type = 2', array());
        $select->joinLeft(array('file_upload' => DATABASE_TRADE.'.air_details_file'), 'file_upload.air_details_id = p.id AND file_upload.type = 0', array());
        $select->joinLeft(array('file_accept' => DATABASE_TRADE.'.air_details_file'), 'file_accept.air_details_id = p.id AND file_accept.type = 3', array()); //anh nghiem thu
        $select->joinLeft(array('file_document' => DATABASE_TRADE.'.air_details_file'), 'file_document.air_details_id = p.id AND file_document.type = 4', array());
        $select->joinLeft(array('image_design' => DATABASE_TRADE.'.air_details_file'), 'image_design.air_details_id = p.id AND image_design.type = 5', array());
        $select->joinLeft(array('file_nghiemthu' => DATABASE_TRADE.'.air_details_file'), 'file_nghiemthu.air_details_id = p.id AND file_nghiemthu.type = 6', array()); // file ngiem thu
        $select->joinLeft(array('contructors' => DATABASE_TRADE.'.contructors'), 'contructors.id = p.contractor_id', array());
        
        
        $select->where('p.air_id = ?', $params['id']);
        $select->where('p.quantity > ?', 0);
        $select->group('p.id');
        
        $result = $db->fetchAll($select);
       
        return $result;
    }

    public function getContract($params){
         $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "store"    => "s.name",
            "category"      => "c.name",
            "p.id", 
            "p.air_id", 
            "p.cate_id", 
            "p.final_price",
            "p.quantity",
            "p.contract_id"
        );

        $select->from(array('p' => DATABASE_TRADE.'.air_details'), $arrCols);
         $select->joinLeft(array('a' => DATABASE_TRADE.'.app_air'), 'a.id = p.air_id', array());
        $select->joinLeft(array('c' => DATABASE_TRADE.'.category'), 'c.id = p.cate_id', array());
         $select->joinLeft(array('s' => 'store'), 's.id = a.store_id', array());
        
        $select->where('p.contractor_id = ?', $params['contractor_id']);
        $select->where('a.id IS NOT NULL');
        $result = $db->fetchAll($select);
       
        return $result;
    }

    public function transferToShop($store_id, $air_details)
    {

        $QAppCheckshop = new Application_Model_AppCheckshop();
        $QAppCheckshopDetail = new Application_Model_AppCheckshopDetail();
        $QAppCheckshopDetailChild = new Application_Model_AppCheckshopDetailChild();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $checkshop_store_is_lock = $QAppCheckshop->fetchRow([
            'store_id = ?' => $store_id,
            'is_lock = ?' => 1
        ]);

        if (!$checkshop_store_is_lock) {
            $result = [
                'code' => 2,
                'message' => 'Shop chưa được xác nhận số lượng!'
            ];
            return $result;
        }

        foreach ($air_details as $detail) {
            // insert chi tiết trong app_checkshop_detail_child
            $QAppCheckshopDetailChild->insert([
                'checkshop_id' => $checkshop_store_is_lock['id'],
                'category_id' => $detail['cate_id'],
                'quantity' => $detail['quantity']
            ]);

            // update quantity tổng trong app_checkshop_detail
            $existed_category = $QAppCheckshopDetail->fetchRow([
                'checkshop_id = ?' => $checkshop_store_is_lock['id'],
                'category_id = ?' => $detail['cate_id']
            ]);

            if ($existed_category) {
                $QAppCheckshopDetail->update([
                    'quantity' => $existed_category['quantity'] + $detail['quantity']
                ], ['id = ?' => $existed_category['id']]);
            } else {
                $QAppCheckshopDetail->insert([
                    'checkshop_id' => $checkshop_store_is_lock['id'],
                    'category_id' => $detail['cate_id'],
                    'quantity' => $detail['quantity']
                ]);
            }
        }


        $result = [
            'code' => 1,
            'message' => 'Done'
        ];

        return $result;

    }
}