<?php
class Application_Model_AppraisalSaleMember extends Zend_Db_Table_Abstract
{
    protected $_name = 'appraisal_sale_member';
    protected $_primary = 'asp_id';

    /**
     * @param $planId
     * @param $staffId
     * @return array
     */
    public function getByPlanAndStaff($planId, $staffId)
    {
        return $this->fetchAll([
            $this->getAdapter()->quoteInto('asm_staff_id = ? and asm_is_deleted = 0', $staffId),
            $this->getAdapter()->quoteInto('asp_id = ? ', $planId),
        ])->toArray();
    }

    public function getCommentForStaff($planId, $staffId)
    {
        return $this->fetchAll([
            $this->getAdapter()->quoteInto('asm_staff_be_appraisal = ? and asm_is_deleted = 0', $staffId),
            $this->getAdapter()->quoteInto('asp_id = ? ', $planId),
            $this->getAdapter()->quoteInto('asm_type != 0', null),
            $this->getAdapter()->quoteInto('(length(asm_promote) > 0 or length(asm_improve) > 0)', null),
        ], ['asm_type'])->toArray();
    }

    public function getSubMember($planId, $bossId)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('asm' => 'appraisal_sale_member'), array('sub_member_id' => 'asm.asm_staff_id', 'sub_member_name' => 'CONCAT(st.firstname," ", st.lastname)', 'plan_id' => 'asm.asp_id'))
            ->joinLeft(array('st' => 'staff'), 'asm.asm_staff_id = st.id', array())
            ->where('asm.asm_staff_be_appraisal = ?', $bossId)
            ->where('asm.asm_type = ?', 2)
            ->where('asm.asp_id = ?', $planId);

        $subMembers = $db->fetchAll($select);

        return $subMembers;
    }

}