<?php

class Application_Model_CheckShopEntireDisplayProductDetail extends Zend_Db_Table_Abstract
{
    protected $_name = 'check_shop_entire_display_product_detail';
    protected $_schema = DATABASE_TRADE;

    public function get($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['p' => DATABASE_TRADE.'.check_shop_entire_display_product'], [
                'd.*',
                'category_name' => 'c.name'
            ])
            ->joinLeft(['d' => DATABASE_TRADE.'.check_shop_entire_display_product_detail'], 'p.id = d.display_product_id', [])
            ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'd.category_id = c.id', [])
            ->where('p.check_shop_id = ?', $params['check_shop_id']);
        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['display_product_id']] [] = $element;
        }

        return $list;
    }
}    