<?php
class Application_Model_Store extends Zend_Db_Table_Abstract
{
	protected $_name = 'store';

    const status_on = 0;
    const status_disable = 1;
    const status_all = 2;

    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->distinct()
            ->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'))
            ->join(array('vsa'=>'v_store_area'),'vsa.store_id = p.id',array())
        ;

        $select->joinLeft(array('g' => 'store_staff'),
            'p.id = g.store_id',
            array());

        $select->joinLeft(array('r' => 'regional_market'),
            'vsa.regional_market = r.id',
            array('regional_market_name'=>'r.name'));

        $select->joinLeft(array('d' => 'regional_market'),
            'd.id = p.district',
            array('district_name'=>'d.name'));

        $select->joinLeft(array('a' => 'area'),
            'r.area_id = a.id',
            array('area_name'=>'a.name'));

        if  (
                (isset($params['staff_email']) and $params['staff_email']) 
                    || (isset($params['export']) and $params['export'])
                    || (isset($params['staff_id']) AND $params['staff_id'])
            ){

            $select->joinLeft(array('ss' => 'store_staff'),
                'p.id = ss.store_id',
                array());

            $select->joinLeft(array('s' => 'staff'),
                'ss.staff_id = s.id',
                array('s.firstname', 's.lastname'));

            if (isset($params['staff_email']) and $params['staff_email'])
                $select->where('s.email LIKE ?', '%'.$params['staff_email'].'%');

            if(isset($params['staff_id']) AND $params['staff_id']){
                $select->where('s.id = ?',$params['staff_id']);
            }
        }

        if (isset($params['asm']) and $params['asm']) {
            $QAsm = new Application_Model_Asm();
            $list_regions = $QAsm->get_cache($params['asm']);
            $list_district = isset($list_regions['district']) && is_array($list_regions['district']) ? $list_regions['district'] : array();
            $list_regional_market = isset($list_regions['province']) && is_array($list_regions['province']) ? $list_regions['province'] : array();
            //
            /*
            if($params['asm'] == 159){
                $select_hn_2  = $db->select()
                ->from(array('a'=>'regional_market'),'a.id')
                ->where('a.parent = ?',3433);
                $hn2 = $db->fetchAll($select_hn_2);

                $arr_hn2 = array();
                if(count($hn2)){
                    foreach($hn2 as $value){
                        $arr_hn2[] = $value;
                    }
                    $list_regions = array_merge($list_regions,$arr_hn2);
                }

            }*/

            if (count($list_regions)){
                $select->where('vsa.district IN (?)', $list_district); // lọc staff thuộc regional_market trên
                $select->where('vsa.regional_market IN (?)',$list_regional_market);
            }else
                $select->where('1=0', 1);
        }

        if (isset($params['store_id']) and $params['store_id'])
            $select->where('p.id = ? ', $params['store_id']);

        if (isset($params['partner_id']) and $params['partner_id'] != '')
            $select->where('p.partner_id = ? ', $params['partner_id']);

        if (isset($params['d_id']) and $params['d_id'])
            $select->where('p.d_id = ? ', $params['d_id']);

        if (isset($params['address']) and $params['address'])
            $select->where('p.company_address LIKE ? OR p.shipping_address LIKE ? ', '%'.$params['address'].'%');

        if (isset($params['name']) and $params['name']){
            $select->where('(p.name LIKE ?', '%'.$params['name'].'%');
            $select->orwhere('p.short_name LIKE ?)', '%'.$params['name'].'%');
        }

        // if (isset($params['area_id']) and $params['area_id']) {
        //     if (is_array($params['area_id']) && $params['area_id'])

        //     elseif (is_numeric($params['area_id']) && intval($params['area_id']))
        //         $select->where('a.id = ?', $params['area_id']);
        // }

        if (isset($params['district']) && $params['district']) {
            if (is_array($params['district']) && count($params['district']))
                $select->where('p.district IN (?)', $params['district']);
            elseif (is_numeric($params['district']) && $params['district'])
                $select->where('p.district = ?', intval($params['district']));
            else
                $select->where('1=0', 1);

        } elseif (isset($params['regional_market']) && $params['regional_market']) {
            $district_arr = array();

            if (is_array($params['regional_market']) && count($params['regional_market'])) {
                foreach ($params['regional_market'] as $_key => $_id)
                    $this->get_districts_by_province($_id, $district_arr);

            } elseif (is_numeric($params['regional_market']) && $params['regional_market']) {
                $this->get_districts_by_province($params['regional_market'], $district_arr);
            }

            if (count($district_arr))
                $select->where('p.district IN (?)', $district_arr);
            else
                $select->where('1=0', 1);

        } elseif (isset($params['area_id']) && $params['area_id']) {
            $district_arr = array();

            if (is_array($params['area_id']) && count($params['area_id'])) {
                foreach ($params['area_id'] as $_key => $_area_id)
                    $this->get_districts_by_area($_area_id, $district_arr);

            } elseif (is_numeric($params['area_id']) && $params['area_id']) {
                $this->get_districts_by_area($params['area_id'], $district_arr);
            }

            if (count($district_arr))
                $select->where('p.district IN (?)', $district_arr);
            else
                $select->where('1=0', 1);
        }

        // if (isset($params['regional_market']) and $params['regional_market'])
        //     $select->where('p.regional_market = ?', $params['regional_market']);

        // if (isset($params['district']) and $params['district'])
        //     $select->where('p.district = ?', $params['district']);

        if ( isset( $params['have_pg'] ) and $params['have_pg'] == 1 and  isset( $params['have_sales'] ) and $params['have_sales'] == 1 )
            $select->where('g.staff_id IS NOT NULL');

        elseif ( isset( $params['have_pg'] ) and $params['have_pg'] == 1 and ! ( isset( $params['have_sales'] ) and $params['have_sales'] == 1 ) )
            $select->where('g.staff_id IS NOT NULL AND g.is_leader = 0');

        elseif ( isset( $params['have_sales'] ) and $params['have_sales'] == 1 and !( isset( $params['have_pg'] ) and $params['have_pg'] == 1 ) )
            $select->where('g.staff_id IS NOT NULL AND g.is_leader = 1');

        elseif ( isset( $params['no_staff'] ) and $params['no_staff'] == 1 )
            $select->where('g.staff_id IS NULL');

        elseif(isset($params['sales_direct']) and $params['sales_direct'] == 1){
            //những shop mà sale/sale leader quản lý trự tiếp
            $select->where('g.id IS NOT NULL');
            $select->having('SUM(g.is_leader) = 1');
            $select->having('COUNT(g.id) = 1');
            
        }

// if(!empty($_GET['dev'])){
//     echo $select->__toString();
//     exit;
// }

        if (isset($params['diamond']) && $params['diamond']) {
           
            $select->join(array('dm' => 'dealer_loyalty'), 'p.d_id=dm.dealer_id', array());

            $select->joinLeft(array('dm2' => 'loyalty_plan'),  'dm.loyalty_plan_id = dm2.id', array('loyalty_plan' => 'dm2.name'));


            if (isset($params['diamond_level']) && intval($params['diamond_level']))
                $select->where('dm.loyalty_plan_id = ?', intval($params['diamond_level']));

             if (isset($params['diamond_store_id']) && intval($params['diamond_store_id']))
                $select->where('p.id = ?', intval($params['diamond_store_id']));



            if (isset($params['diamond_month']) && date_create_from_format('d/m/Y', $params['diamond_month']))
                $select
                    ->where('dm.from_date <= ?', date_create_from_format('d/m/Y', $params['diamond_month'])->format('Y-m'))
                    ->where('dm.to_date >= ?', date_create_from_format('d/m/Y', $params['diamond_month'])->format('Y-m'));
        }

        if (isset($params['sort']) and $params['sort']) {
            $order_str = $collate = ' ';
            $desc = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';

            if ($params['sort'] == 'name'){
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= ' p.name '.$collate . $desc;
            } elseif ( $params['sort'] == 'area' ) {
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= 'a.name' . $collate . $desc;
            } elseif ( $params['sort'] == 'district' ) {
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= 'd.name' . $collate . $desc;
            } elseif ( $params['sort'] == 'regional_market' ) {
                $order_str =  ' r.name ' . $collate . $desc;
            } else
                $order_str =  ' p.'.$params['sort'].' ' . $collate . $desc;

            if ($order_str)
                $select->order(new Zend_Db_Expr($order_str));
        }

        $select->group('p.id');
        
        if ( $limit && ! ( isset($params['export']) && $params['export'] ) )
            $select->limitPage($page, $limit);
        else
            $select->where('p.del IS NULL OR p.del <> ?', 1);

        PC::debug($select->__toString());
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    function fetchPaginationLeader($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        $select->joinLeft(array('g' => 'store_leader'),
            'p.id = g.store_id',
            array('status' => 'g.status', 'staff_id' => 'g.staff_id'));

        $select->joinLeft(array('lg' => 'store_leader_log'),
            'g.staff_id = lg.staff_id AND g.store_id = lg.store_id AND released_at IS NULL',
            array('joined_at' => 'lg.joined_at', 'log_id' => 'lg.id'));

        $select->join(array('r' => 'regional_market'),
            'p.regional_market = r.id',
            array('regional_market_name'=>'r.name'));

        $select->join(array('a' => 'area'),
            'r.area_id = a.id',
            array('area_name'=>'a.name'));

        $select->joinLeft(array('s' => 'staff'),
                'g.staff_id = s.id',
                array( 's.firstname', 's.lastname', 's.email'));

        if (isset($params['staff_email']) and $params['staff_email'])
            $select->where('s.email LIKE ?', '%'.$params['staff_email'].'%');

        if (isset($params['address']) and $params['address'])
            $select->where('p.company_address LIKE ? OR p.shipping_address LIKE ? ', '%'.$params['address'].'%');

        if (isset($params['name']) and $params['name'])
            $select->where('p.name LIKE ?', '%'.$params['name'].'%');

        if (isset($params['area_id']) and $params['area_id'])
            $select->where('a.id = ?', $params['area_id']);

        if (isset($params['regional_market']) and $params['regional_market'])
            $select->where('p.regional_market = ?', $params['regional_market']);

        if ( isset( $params['have_leader'] ) and $params['have_leader'] == 1 ) {
            $select
                ->where('g.status = 1')
                ->where('g.staff_id IS NOT NULL');
            
        }

        elseif ( isset( $params['no_leader'] ) and $params['no_leader'] == 1 )
            $select->where('g.staff_id IS NULL');

        elseif ( isset( $params['pending'] ) and $params['pending'] == 1 ) {
            $select
                ->where('g.status = 0')
                ->where('g.staff_id IS NOT NULL');
        }

        $QRegion = new Application_Model_RegionalMarket();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        if (isset($params['for_asm']) && $params['for_asm']) {
            $QAsm = new Application_Model_Asm();
            $list_regions = $QAsm->get_cache();
            $list_regions = isset($list_regions['district']) && is_array($list_regions['district']) ? $list_regions['district'] : array();

            if (count($list_regions))
                $select->where('p.district IN (?)', $list_regions);
            else
                $select->where("1=0", 1);
        }

        if (isset($params['for_leader']) && $params['for_leader']) {
            $region = $QRegion->find($userStorage->regional_market);
            $region = $region->current();

            if ($region) {
                $where = $QRegion->getAdapter()->quoteInto('area_id = ?', $region['area_id']);
                $regions = $QRegion->fetchAll($where);
                
                $region_list = array();

                foreach ($regions as $reg)
                    $region_list[] = $reg['id'];
                
                if (is_array($region_list) && count($region_list) > 0)
                    $select->where('p.regional_market IN (?)', $region_list);
                else
                    $select->where("1=0", 1);
            } else {
                $select->where("1=0", 1);
            }
        }

        $select->where('p.del = 0 OR p.del IS NULL');

        if (isset($params['sort']) and $params['sort']) {
            $order_str = $collate = ' ';
            $desc = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';

            if ($params['sort'] == 'name'){
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= ' p.name '.$collate . $desc;

            } elseif ( $params['sort'] == 'area' ) {
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= 'a.name' . $collate . $desc;

            } elseif ( $params['sort'] == 'leader' ) {
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= ' CONCAT(s.firstname, " ",s.lastname) ' . $collate . $desc;

            } elseif ( $params['sort'] == 'company_address' ) {
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= ' p.company_address ' . $collate . $desc;

            } elseif ( $params['sort'] == 'regional_market' ) {
                $order_str =  ' r.name ' . $collate . $desc;
                
            } else
                $order_str =  ' p.'.$params['sort'].' ' . $collate . $desc;

            if ($order_str)
                $select->order(new Zend_Db_Expr($order_str));
        }

        $select->group('p.id');

        if ( $limit && ! ( isset($params['export']) && $params['export'] ) )
            $select->limitPage($page, $limit);
        else
            $select->where('p.del IS NULL OR p.del <> ?', 1);

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if ($result === false) {

            $data = $this->fetchAll();

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->name;
                }
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }

    function get_assigned_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_assigned_cache');

        if ($result === false) {

            $data = $this->fetchAll();

            $result = array();

            $db = Zend_Registry::get('db');


            if ($data){
                foreach ($data as $item){
                    $select = $db->select()
                        ->from(array('p' => 'store_staff'),
                            array('p.*'))
                        ->join(array('s' => 'staff'),
                            's.id = p.staff_id',
                            array('s.firstname', 's.lastname', 's.group_id'));

                    $select->where('p.store_id = ?', $item->id);

                    $staffs = $db->fetchAll($select);

                    $sales = $PGs = '';

                    //get staffs
                    if ($staffs){
                        foreach ($staffs as $staff){
                            if ($staff['group_id']==SALES_ID){
                                $break_line = $sales !== '' ? "\n" : '';
                                $sales .= $break_line . $staff['firstname'].' '.$staff['lastname'];

                            }elseif ($staff['group_id']==PGPB_ID){

                                $break_line = $PGs !== '' ? "\n" : '';
                                $PGs .= $break_line . $staff['firstname'].' '.$staff['lastname'];
                            }

                        }
                    }

                    $result[$item->id] = array(
                        'sales' => $sales,
                        'PGs' => $PGs,
                    );
                }
            }
            $cache->save($result, $this->_name.'_assigned_cache', array(), null);
        }
        return $result;
    }

    function compare($full_store_name)
    {
        $full_store_name = My_String::trim($full_store_name);
        $check_list = array();

        $db = Zend_Registry::get('db');

        $store_name_split = explode('-', $full_store_name);

        if (is_array($store_name_split) && !empty($store_name_split[0])) {
            $store_name = $store_name_split[0];
        } else {
            $store_name = $full_store_name;
        }
        
        $where = array();
        $where[] = $this->getAdapter()->quoteInto('del = 0 OR del IS NULL', 1);
        $where[] = $this->getAdapter()->quoteInto('name LIKE ?', '%'.$store_name.'%');
        $check_store_name = $this->fetchAll($where, 'name', 5);

        foreach ($check_store_name as $key => $store) {
            $check_list[ $store['id'] ] = $store['name'];
        }

        //
        
        $full_store_name_like = str_replace(array('đường', 'số', 'ngõ', 'hẻm', 'huyện', 'quận', 'xã', 'phường', 'thôn', 'ấp', 'xóm', 'thành phố', 'tp', 'tỉnh', ',', '-', '(', ')'), 
                                                '', mb_strtolower($full_store_name, 'UTF-8'));
        
        $full_store_name_like = preg_replace('/[\s]+/', '%', $full_store_name_like);
        $full_store_name_like = preg_replace('/[\%]+/', '%', $full_store_name_like);

        $where = array();
        $where[] = $this->getAdapter()->quoteInto('del = 0 OR del IS NULL', 1);
        $where[] = $this->getAdapter()->quoteInto('name LIKE ?', '%'.$full_store_name_like.'%');

        $check_store_name = $this->fetchAll($where);

        $count = 0;
        foreach ($check_store_name as $key => $store) {
            if ($count++ > 10) break;

            if (!isset($check_list[ $store['id'] ]))
                $check_list[ $store['id'] ] = $store['name'];
        }

        //

        $store_list = $this->get_cache();

        $count = 0;
        foreach ($store_list as $_store_id => $_store_name) {
            if ($count++ > 10) break;

            if ( similar_text($_store_name, $full_store_name) > 75 && !isset($check_list[ $_store_id ]))
                $check_list[ $_store_id ] = $_store_name;
        }

        
        return $check_list;
    }

    public function get_by_district_id_list($district_id, $status = self::status_all)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('s' => $this->_name), array('s.id', 's.name'));

        if (is_array($district_id) && count($district_id)){
            $select->where('s.district IN (?)', $district_id);
        }elseif (is_numeric($district_id) && $district_id){
            $select->where('s.district = ?', intval($district_id));
        }

        if($status == 0){
            $select->where('s.del = ? OR s.del IS NULL',0);
        }elseif($status == 1){
            $select->where('s.del = ?',1);
        }else{
            //tất cả
        }


        $select->order('s.name');
        $list = $db->fetchAll($select);
        $result = array();
        if($list and count($list)){
            foreach($list as $item){
                $result[$item['id']] = $item;
            }
        }

        return $result;
    }

    private function get_districts_by_province($province_id, &$district_arr)
    {
        $QRegion = new Application_Model_RegionalMarket();
        $district_cache = $QRegion->get_district_by_province_cache($province_id);

        if ($district_cache)
            foreach ($district_cache as $key => $value)
                $district_arr[] = intval($key);
    }

    private function get_districts_by_area($area_id, &$district_arr)
    {
        $QRegion = new Application_Model_RegionalMarket();
        $district_cache = $QRegion->get_district_by_area_cache($area_id);

        if ($district_cache)
            foreach ($district_cache as $key => $value)
                $district_arr[] = intval($value);
    }

    public function get_store_random($params)
    {
        $db = Zend_Registry::get('db');

        $stmt = $db->query("CALL sp_get_saleout_data(?,?,?,?,?,?)", array($params['total_shop'], $params['saleout_from'], $params['saleout_to'], $params['saleout_date'], date('Y-m-d'), $params['shop_group']));
        //$stmt = $db->query("CALL sp_get_saleout_data(?,?,?,?,?,?,?)", array(40, 1, 40, '2015-10-01', '2016-02-29', '1'));
        $result = $stmt->fetchAll();
        $stmt->closeCursor();

        return $result;
    }
	
	public function get_store_by_id($staff_id){
		if(!$staff_id){
            return false;
        }
		$db = Zend_Registry::get('db');
		
		$select = $db->select()
            ->from(array('a'=>'store_staff'),array('a.store_id'))
            ->joinLeft(array('s'=>'store'), 's.id = a.store_id',array())
            ->joinLeft(array('r'=>'regional_market'), 'r.id = s.regional_market',array('r.area_id'))
            ->where('a.staff_id = ?',$staff_id);
        $result = $db->fetchAll($select);
        return $result;
	}
	
    public function get_pg_store_sale($staff_id)
    {
        if(!$staff_id){
            return false;
        }

        $db = Zend_Registry::get('db');

        $select_title = $db->select()
            ->from(array('a'=>'staff'),array('a.title'))
            ->where('a.id = ?',$staff_id);
        $title = $db->fetchOne($select_title);

        if(!$title){
            return false;
        }

        $select = $db->select()
            ->from(array('a'=>$this->_name),array('c.id'))
            ->join(array('b'=> 'store_staff'),'a.id = b.store_id',array())
            ->join(array('d'=>'store_staff'),'a.id = d.store_id',array())
            ->join(array('c'=>'staff'),'c.id = d.staff_id',array())
            ->join(array('e'=>'team'),'e.id = c.title',array())
            ->where('b.staff_id = ?',$staff_id)
            ->where('a.del IS NULL OR a.del = ?',0)
            ->group('c.id')
        ;

        if(in_array($title, My_Staff_Title::getLeader())){
            $array_permission_merge  = array_merge(My_Staff_Title::getSale() , My_Staff_Title::getPg());
            $select->where('c.title IN (?)',$array_permission_merge);
        }elseif(in_array($title,My_Staff_Title::getSale()) || in_array($title,My_Staff_Title::getPbSale())){
            $select->where('c.title IN (?)',My_Staff_Title::getPg());
            $select->where('d.is_leader = ?',0);
        }
        $result = $db->fetchAll($select);

        $data   = array();

        if(isset($result) and $result)
        {
            foreach($result as $k => $item)
                $data[] = $item['id'];
        }

        return $result;
    }

    public function get_sale_pg_leader($staff_id){
        if(!$staff_id){
            return false;
        }

        $db = Zend_Registry::get('db');

        $select_title = $db->select()
            ->from(array('a'=>'staff'),array('a.title'))
            ->where('a.id = ?',$staff_id);
        $title = $db->fetchOne($select_title);


        if(!$title){
            return false;
        }

        $cols = array(
                'staff_name' => 'CONCAT(c.firstname," ",c.lastname)',
                'email' => 'c.email',
                'title_name'=>'e.name'
            );
        if(in_array($title,My_Staff_Title::getSale())){
            $tbl_b = 'store_staff';
        }else{
            $tbl_b = 'store_leader';
        }

        $select = $db->select()
            ->from(array('a'=>$this->_name),$cols)
            ->join(array('b'=>$tbl_b),'a.id = b.store_id',array())
            ->join(array('d'=>'store_staff'),'a.id = d.store_id',array())
            ->join(array('c'=>'staff'),'c.id = d.staff_id',array())
            ->join(array('e'=>'team'),'e.id = c.title',array())
            ->where('b.staff_id = ?',$staff_id)
            ->where('a.del IS NULL OR a.del = ?',0)
            ->group('c.id')
            ;

        if(in_array($title, My_Staff_Title::getLeader())){
            //Nếu truyền id leader
            $select->where('c.title IN (?)',My_Staff_Title::getSale());
            $select->where('d.is_leader = ?',1);
        }elseif(in_array($title,My_Staff_Title::getSale())){
            //nếu truyền id sale
            $select->where('c.title IN (?)',My_Staff_Title::getPg());
            $select->where('d.is_leader = ?',0);
        }else{

        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function getImageByStore($store_id){
        $db  = Zend_Registry::get('db');
        error_reporting(1);
        require_once 'My'.DIRECTORY_SEPARATOR.'nusoap'.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'nusoap.php';
        $this->wssURI = 'http://trade-marketing.opposhop.vn/wss';
        $this->namespace = 'OPPOVN';

        $client = new nusoap_client($this->wssURI);
        $client->soap_defencoding = 'UTF-8';
        $client->decode_utf8 = false;
        $wsParams = array(
            'store_id' => $store_id
        );

        try {
            $result = $client->call("getImage", $wsParams);
            echo "<pre>";
            var_dump($result);
            echo '</pre>';
            exit;

        } catch (Exception $e) {

        }
        return array('code'=>1,'message'=>'Done');
    }

    public function getStoreByDistrict($params){

    }
}
