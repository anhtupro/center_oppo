<?php
class Application_Model_MiniGame2018 extends Zend_Db_Table_Abstract
{
	protected $_name = 'mini_game_2018';

	public function getIdStaff($params){
		$db = Zend_Registry::get("db");
        $select = $db->select();

       $arrCols = array(
            'p.staff_id',
            'id'
        );
        $select->from(array('p' => 'mini_game_2018'), $arrCols);        
        $select->where('p.staff_id = ?', $params['staff_id']);
        $select->where('p.round = ?', $params['round']);
        $result = $db->fetchAll($select);
        return $result;
	}

    public function getResult($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

       $arrCols = array(
            'd.id',
            'd.question_id',
            'q.answer',
            'd.user_choose',
            'd.diff',
            'd.point',
           'd.is_right'
        );
        $select->from(array('p' => 'mini_game_2018'), $arrCols);   
        $select->joinleft(array('d'=>'mini_game_2018_detail'),'d.master_id = p.id', array ());    
        $select->joinleft(array('q'=>'mini_game_answer_2018'),'d.question_id = q.question_id', array ());
        $select->where('p.staff_id = ?', $params['staff_id']);
        $select->where('p.round = ?', $params['round']);
        $select->group('d.question_id');
        $select->order('d.id ASC');
//        echo $select->__toString();die;
        $result = $db->fetchAll($select);

        return $result;
    }

    public function getReward($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.staff_id',
            's.code',
            // 'point' => "SUM(if(d.round = ".MINIGAME_ROUND_1." or d.round = ".MINIGAME_ROUND_2.", d.point, 0))",
            // 'point_1' => "SUM(if(d.round = ".MINIGAME_ROUND_1.", d.point, 0))",
            // 'point_2' => "SUM(if(d.round = ".MINIGAME_ROUND_2.", d.point, 0))",
            // 'rights' => "COUNT(if(d.is_right <> 0, 1, NULL))",
            // 'rights_1' => "COUNT(if(d.is_right <> 0 and d.round = ".MINIGAME_ROUND_1.", 1, NULL))",
            // 'rights_2' => "COUNT(if(d.is_right <> 0 and d.round = ".MINIGAME_ROUND_2.", 1, NULL))",
            'fullname' => "CONCAT(s.firstname,' ', s.lastname)",
            'department' => 't.name',
            'team' => 'tt.name',
            'title' => 'ttt.name',
            'name_reward' => "IFNULL(r.reward,'Chúc bạn may mắn lần sau !')"

        );
        $select->from(array('p' => 'mini_game_2018'), $arrCols);   
        $select->joinleft(array('d'=>'mini_game_2018_detail'),'d.master_id = p.id', array ());
        $select->joinleft(array('l'=>'mini_game_reward_log'),'l.staff_id = p.staff_id AND l.round = '.MINIGAME_ROUND.'', array ());
        $select->joinleft(array('r'=>'mini_game_reward'),'r.id = l.reward ', array ());
        $select->joinleft(array('s'=>'staff'),'s.id = p.staff_id', array ());
        $select->joinleft(array('t'=>'team'),'t.id = s.department', array ());
        $select->joinleft(array('tt'=>'team'),'tt.id = s.team', array ());
         $select->joinleft(array('ttt'=>'team'),'ttt.id = s.title', array ());
        $select->where('p.round in (?)', array(MINIGAME_ROUND));
        $select->where('l.reward NOT IN (?)', [17,21]);
//        $select->group(array('p.staff_id','p.round'));
        $select->group(array('p.staff_id'));
        // $select->limit(30);
         
        $result = $db->fetchAll($select);
      
        return $result;
    }
    public function getAnswer($ques, $round=1){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.answer'
        );
        $select->from(array('p' => 'mini_game_answer_2018'), $arrCols);   
         $select->where('p.question_id = ?', $ques);
         $select->where('p.round = ?', $round);
         
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getTotalCorrectAnswer($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'd.id',
            'd.question_id',
            'q.answer',
            'd.user_choose',
            'd.diff',
            'd.point'
        );
        $select->from(array('p' => 'mini_game_2018'), $arrCols);
        $select->join(array('d'=>'mini_game_2018_detail'),'d.master_id = p.id', array ());
        $select->join(array('q'=>'mini_game_answer_2018'),'d.question_id = q.question_id', array ());
        $select->where('p.staff_id = ?', $params['staff_id']);
        $select->where('p.user_choose = q.answer' );
        print "<pre>".print_r($select->__toString());die;
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getRotationReward($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.staff_id',
            'fullname' => "CONCAT(s.firstname,' ', s.lastname)",
            'department' => 't.name',
            'team' => 'tt.name',
            'title' => 'ttt.name',
            'reward' => 'r.reward',
            'regional_market' => 'm.name',

        );
        $select->from(array('p' => 'mini_game_reward_log'), $arrCols);
        $select->joinleft(array('s'=>'staff'),'s.id = p.staff_id', array ());
        $select->joinleft(array('m'=>'regional_market'),'m.id = s.regional_market', array ());
        $select->joinleft(array('t'=>'team'),'t.id = s.department', array ());
        $select->joinleft(array('tt'=>'team'),'tt.id = s.team', array ());
        $select->joinleft(array('ttt'=>'team'),'ttt.id = s.title', array ());
        $select->joinleft(array('r'=>'mini_game_reward'),'p.reward = r.id', array ());
        $select->where('p.round in (?)', array(MINIGAME_ROUND));
        $select->where('r.id <> 9', null);
//        $select->group(array('p.staff_id','p.round'));
        $select->group(array('p.staff_id'));
//         echo $select->__toString();die;
        $result = $db->fetchAll($select);
        //echo $select; exit;
        return $result;
    }
}
