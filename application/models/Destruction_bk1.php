<?php
class Application_Model_Destruction extends Zend_Db_Table_Abstract
{
    protected $_name = 'destruction';
    protected $_schema = DATABASE_TRADE;

    public function GetAll(){
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare('
            SELECT id, name FROM '. $this->_name
        );
        $stmt->execute();
        $data = $stmt->fetchAll();
        $db = $stmt = null;
        return $data;
    }
    
    public function fetchPagination($page, $limit, &$total, $params){
        
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'id' => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            'store_name' => "s.name",
            'created_at' => "p.create_at",
            'created_by' => "p.staff_id",
            'status'    => "p.status",
            'reject'    => "p.reject",
            'reject_note'    => "p.reject_note",
            'total_price' => "SUM(q.total_price) + SUM(q.total_price) * 0.1" // VAT = 10%
        );

        $select->from(array('p' => DATABASE_TRADE.'.destruction'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 's.regional_market = r.id', array());
        $select->joinLeft(array('l' => 'v_store_staff_leader_log'), 'l.store_id = p.store_id AND l.released_at IS NULL', array());
        $select->joinLeft(array('a' => DATABASE_TRADE.'.app_status'), 'a.status = p.status AND a.type = 12', array());
        if(!empty($params['title'])) {
            $select->joinLeft(array('t' => DATABASE_TRADE . '.app_status_title'), 't.status_id = p.status AND t.type = 12', array());
        }
    	$select->joinLeft(array('d' => DATABASE_TRADE.'.destruction_details'), 'p.id = d.destruction_id', array());
    	$select->joinLeft(array('q' => DATABASE_TRADE.'.destruction_quotation'), 'd.id = q.destruction_details_id AND q.status = (SELECT MAX(status) FROM trade_marketing.destruction_quotation WHERE destruction_details_id = d.id AND status <> 1 AND status <> 3)', array());
        $select->where('p.remove IS NULL OR p.remove = 0');

        if ($params['status_finish']) {
            $select->where('p.status = ?', $params['status_finish']);
        }

        if(!empty($params['staff_id'])){
            $select->where('p.staff_id = ?', $params['staff_id']);
        }
        
        if(!empty($params['area_id'])){
            $select->where('r.area_id IN (?)', $params['area_id']);
        }
        //SEARCH
         if(!empty($params['area_id_search'])){
            $select->where('r.area_id IN (?)', $params['area_id_search']);
         }
         if(!empty($params['sale_id'])){
            $select->where('l.staff_id = ?', $params['sale_id']);
         }
        if(!empty($params['status_id'])){
            $select->where('p.status IN (?)', $params['status_id']);
            $select->where('p.reject IS NULL OR p.reject = 0');
        }
         if(!empty($params['name_search'])){
             $select->where('s.name LIKE ?', '%'.$params['name_search'].'%');
         }

        if(!empty($params['from_date'])){
            $from_date = str_replace('/', '-', $params['from_date']);
            $from_date = date('Y-m-d', strtotime($from_date));

            $select->where('DATE(p.create_at) >= ?', $from_date);
        }

        if(!empty($params['to_date'])){
            $to_date = str_replace('/', '-', $params['to_date']);
            $to_date = date('Y-m-d', strtotime($to_date));
            $select->where('DATE(p.create_at) <= ?', $to_date);
        }
        
        if(!empty($params['title'])){
    		$select->where('t.title = ? AND (p.reject IS NULL OR p.reject = 0)', $params['title']);
    	}

        if ($params['month']) {
            $select->where('MONTH(p.create_at) = ?', $params['month']);
        }

        if ($params['year']) {
            $select->where('YEAR(p.create_at) = ?', $params['year']);
        }
        
        $select->group('p.id');

        if ($params['has_fee']) {
            $select->having('SUM(q.total_price) > 0');
        }
        //END SEARCH

        $select->limitPage($page, $limit);

        $select->order('p.id DESC');
        
        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");
        
        return $result;
    }
    
    public function GetList($id){
    
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare('
            SELECT R.id id,C.id category_id,C.name categoryname,R.quotation_id,S.id store_id,S.name storename,R.quantity quantity,R.imei imei,R.status status, R.note note,R.staff_id staff_id ,R.type type,R.contructors_id contructors, Q.price price , R.create_at ngaytao, R.update_at ngaycapnhat, AP.name status_name  , AST.title  staff_title  FROM '.DATABASE_TRADE.'.'.$this->_name 
            .' R left join '.DATABASE_TRADE.'.category C on C.id = R.category_id'
            . ' left join '.DATABASE_CENTER.'.store S on S.id =R.store_id '
            . ' left join '.DATABASE_TRADE.'.quotation Q on Q.id =R.quotation_id '
            .' left join '.DATABASE_TRADE.'.app_status AP on R.status = AP.id'
            .' left join '.DATABASE_TRADE.'.app_status_title AST on AST.status_id=AP.id'
            .' where R.id = '.$id
        );

        $stmt->execute();
        $data = $stmt->fetchAll();
        $db = $stmt = null;
        return $data;
    }
    
    public function getDestruction($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.store_id", 
            "p.create_at", 
            "p.staff_id", 
            "p.status", 
            "store_name" => "s.name", 
            "fullname" => "CONCAT(staff.firstname, ' ', staff.lastname)",
            "p.reject",
            "p.manager_approve",
            "p.remove",
            "p.remove_note"
        );

        $select->from(array('p' => DATABASE_TRADE.'.destruction'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('staff' => 'staff'), 'staff.id = p.staff_id', array());
        $select->where('p.id = ?', $params['id']);

        $result = $db->fetchRow($select);

        return $result;
    }
    
    public function destructionRemove($store_id, $category_id, $quantity, $imei_sn){
        
        $result = [
            'code'  => 1,
            'message' => 'Done'
        ];
        
        $QAppCheckshop = new Application_Model_AppCheckshop();
        $QAppCheckshopDetail = new Application_Model_AppCheckshopDetail();
        
        $where = [];
        $where[] = $QAppCheckshop->getAdapter()->quoteInto('store_id = ?', $store_id);
        $where[] = $QAppCheckshop->getAdapter()->quoteInto('is_lock = ?', 1);
        $lock_from = $QAppCheckshop->fetchRow($where);
        
        
        if(empty($lock_from)){
            $result = [
                'code'  => 2,
                'message' => 'Shop is not lock.'
            ];
            return $result;
        }
        
        $where_from = [];
        $where_from[] = $QAppCheckshopDetail->getAdapter()->quoteInto('checkshop_id = ?', $lock_from['id']);
        $where_from[] = $QAppCheckshopDetail->getAdapter()->quoteInto('category_id = ?', $category_id);
        $from_details = $QAppCheckshopDetail->fetchRow($where_from);
        
        if(empty($from_details)){
            $result = [
                'code'  => 2,
                'message' => 'Shop details is null.'
            ];
            return $result;
        }
        
        $data_update = [];
        $data_update['quantity'] = $quantity - $from_details['quantity'];
        
        if(!empty($imei_sn)){
            $imei_transfer = $from_details['imei_sn'];
            
            $imei_exp = explode(',', $imei_sn);
            foreach($imei_exp as $k=>$v){
                $imei = str_replace($v.", ", "", $imei_transfer);
                $imei = rtrim(str_replace($v, "", $imei),", ");
                $imei_transfer = $imei;
            }
            $data_update['imei_sn'] = $imei_transfer;
        }
        $QAppCheckshopDetail->update($data_update, $where_from);
        
        
        return $result;
        
    }

    public function getStatistic($params = null)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select();

        $arrCols = array(
            'channel' => "CASE 
                            WHEN (loyalty.loyalty_plan_id IS NOT NULL AND distributor.is_ka = 0) THEN plan.name
                            WHEN (loyalty.loyalty_plan_id IS NULL AND distributor.is_ka = 0) THEN 'Shop Thường'
                            WHEN (distributor.is_ka = 1) THEN 'KA'
                          END",
            'destruction.store_id',
            'store_name' => 's.name',
            'dealer_id' => 'IF(distributor.parent IS NULL OR distributor.parent = 0, distributor.id, distributor.parent)',
            'distributor.partner_id',
            'area' => 'a.name',
            'category' => 'c.name',
            'type' => 'CASE 
                            WHEN (tp.tp_id = 1) THEN "Tiêu hủy tính phí"
                            WHEN (tp.tp_id = 2) THEN "Tiêu hủy không phí"
                       END',
            'fp.first_price',
            'lp.last_price',
            'staff_name' => "CONCAT(st.firstname, ' ', st.lastname)",
            'destruction.create_at',
            'status' => 'status.name',
            'quantity' => 'detail.quantity'
        );

        // first price subquery
        $nestedSelectFirstPrice = $db->select();
        $arrCols_nest = [
            'destruction_details_id',
            'first_price' => 'SUM(total_price) + SUM(total_price) * 0.1'
        ];
        $nestedSelectFirstPrice->from(array('quo'=> DATABASE_TRADE.'.destruction_quotation'), $arrCols_nest)
            ->where('status = 0')
            ->group('destruction_details_id');

        // last price subquery
        $nestedSelectLastPrice = $db->select();
        $arrCols_nest = [
            'destruction_details_id',
            'last_price' => 'SUM(total_price) + SUM(total_price) * 0.1'
        ];
        $nestedSelectLastPrice->from(array('quo'=> DATABASE_TRADE.'.destruction_quotation'), $arrCols_nest)
            ->where('status = 2')
            ->group('destruction_details_id');


        $select->from(['destruction' => DATABASE_TRADE.'.destruction'], $arrCols);
        $select->join(['s' => 'store'], 'destruction.store_id = s.id', []);
        $select->joinLeft(array('r' => 'regional_market'), 's.regional_market = r.id', array());
        $select->joinLeft(array('a' => 'area'), 'r.area_id = a.id', array());
        $select->joinLeft(array('l' => 'store_staff_log'), 'l.store_id = s.id AND l.released_at IS NULL AND l.is_leader = 1', array());
        $select->joinLeft(array('distributor' => WAREHOUSE_DB.'.distributor'), 's.d_id = distributor.id', array());
        $select->joinLeft(array('loyalty' => 'dealer_loyalty'), 'loyalty.dealer_id = IF (distributor.parent IS NULL OR distributor.parent = 0, distributor.id, distributor.parent)  AND loyalty.is_last = 1', array());
        $select->joinLeft(array('plan' => 'loyalty_plan'), 'loyalty.loyalty_plan_id = plan.id', array());

        $select->joinLeft(array('detail' => DATABASE_TRADE.'.destruction_details'), 'destruction.id = detail.destruction_id', array());
        $select->joinLeft(array('c' => DATABASE_TRADE.'.category'), 'detail.category_id = c.id', array());
        $select->joinLeft(array('tp' => DATABASE_TRADE.'.destruction_details_tp'), 'detail.id = tp.destruction_details_id', array());
        $select->joinLeft(array('st' => 'staff'), 'destruction.staff_id = st.id', array());
        $select->joinLeft(array('status' => DATABASE_TRADE.'.app_status'), 'destruction.status = status.status AND status.type = 12', array());

        $select->joinLeft(array('fp' => new Zend_Db_Expr('(' . $nestedSelectFirstPrice . ')')), 'detail.id = fp.destruction_details_id',array());
        $select->joinLeft(array('lp' => new Zend_Db_Expr('(' . $nestedSelectLastPrice . ')')), 'detail.id = lp.destruction_details_id',array());

        $select->where('s.del IS NULL OR s.del = 0');
        $select->where('destruction.remove IS NULL OR destruction.remove = 0');
        $select->group(['s.id', 'detail.id']);

        if(!empty($params['staff_id'])){
            $select->where('l.staff_id = ?', $params['staff_id']);
        }

        if(!empty($params['area_id'])){
            $select->where('r.area_id IN (?)', $params['area_id']);
        }

        $result = $db->fetchAll($select);

        return $result;

    }

    public function export($statistics)
    {
        include 'PHPExcel.php';
        $QRepair = new Application_Model_Repair();

        $objExcel = new PHPExcel();
        $objExcel->setActiveSheetIndex(0);

        $sheet = $objExcel->getActiveSheet()->setTitle('Destruction');

        $rowCount = 2;
        $index = 1;
        $sheet->setCellValue('A1', 'STT');
        $sheet->setCellValue('B1', 'KÊNH');
        $sheet->setCellValue('C1', 'ID SHOP');
        $sheet->setCellValue('D1', 'DEALER ID');
        $sheet->setCellValue('E1', 'PARTNER ID');
        $sheet->setCellValue('F1', 'TÊN SHOP');
        $sheet->setCellValue('G1', 'KHU VỰC');
        $sheet->setCellValue('H1', 'TÌNH TRẠNG');
        $sheet->setCellValue('I1', 'HẠNG MỤC TIÊU HỦY');
        $sheet->setCellValue('J1', 'LOẠI TIÊU HỦY');
        $sheet->setCellValue('K1', 'SỐ LƯỢNG');
        $sheet->setCellValue('L1', 'CHI PHÍ ĐẦU');
        $sheet->setCellValue('M1', 'CHI PHÍ CUỐI');
        $sheet->setCellValue('N1', 'NGÀY TẠO');
        $sheet->setCellValue('O1', 'NGƯỜI TẠO');

        foreach ($statistics as $statistic) {
            $sheet->setCellValue('A' . $rowCount, $index);
            $sheet->setCellValue('B' . $rowCount, $statistic['channel']);
            $sheet->setCellValue('C' . $rowCount, $statistic['store_id']);
            $sheet->setCellValue('D' . $rowCount, $statistic['dealer_id']);
            $sheet->setCellValue('E' . $rowCount, $statistic['partner_id']);
            $sheet->setCellValue('F' . $rowCount, $statistic['store_name']);
            $sheet->setCellValue('G' . $rowCount, $statistic['area'] );
            $sheet->setCellValue('H' . $rowCount, $statistic['status'] );
            $sheet->setCellValue('I' . $rowCount, $statistic['category'] );
            $sheet->setCellValue('J' . $rowCount, $statistic['type'] );
            $sheet->setCellValue('K' . $rowCount, $statistic['quantity'] );
            $sheet->setCellValue('L' . $rowCount, $statistic['first_price'] );
            $sheet->setCellValue('M' . $rowCount, $statistic['last_price'] );
            $sheet->setCellValue('N' . $rowCount, date('d-m-Y H:i:s', strtotime($statistic['create_at'])) );
            $sheet->setCellValue('O' . $rowCount, $statistic['staff_name'] );


            ++$index;
            ++$rowCount;
        }

        //style sheet
        $style_border = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $sheet->getStyle('A0:O' . $rowCount)->applyFromArray($style_border);

        for ($i = 'A'; $i < 'O' ; ++$i) {
            $sheet->getStyle($i. 1)->getFont()->setBold(true);
        }


        $objWriter = new PHPExcel_Writer_Excel2007($objExcel);
        $filename = 'Destruction';
        $objWriter->save($filename);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Set-Cookie: fileLoading=true');
        readfile($filename);
        exit;
    }

    public function getCost($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['d' => DATABASE_TRADE . '.destruction'], [
                'r.area_id',
                'total_cost' => 'SUM(q.total_price) + SUM(q.total_price) * 0.1'
            ])
            ->join(['s' => 'store'], 'd.store_id = s.id', [])
            ->joinLeft(['r' => 'regional_market'], 's.regional_market = r.id', [])
            ->joinLeft(['detail' => DATABASE_TRADE . '.destruction_details'], 'd.id = detail.destruction_id', [])
            ->joinLeft(['q' => DATABASE_TRADE . '.destruction_quotation'], 'detail.id = q.destruction_details_id AND q.status = (
                          SELECT MAX(status) FROM trade_marketing.destruction_quotation where destruction_details_id = detail.id AND status <> 3 AND status <> 1
                      )', [])
            ->where('d.remove =  0 OR d.remove is null')
            ->where('q.id IS NOT NULL');

        if ($params['status_finish']) {
            $select->where('d.status = ?', $params['status_finish']);
        }

        if ($params['area_list']) {
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        if ($params['area_id']) {
            $select->where('r.area_id = ?', $params['area_id']);
        }

        if(!empty($params['staff_id'])){
            $select->where('d.staff_id = ?', $params['staff_id']);
        }

        if ($params['month']) {
            $select->where('MONTH(d.create_at) = ?', $params['month']);
        }

        if ($params['year']) {
            $select->where('YEAR(d.create_at) = ?', $params['year']);
        }

        if ($params['from_date']) {
            $select->where("DATE_FORMAT(d.create_at,'%Y-%m-%d') >= ?", $params['from_date']);
        }

        if ($params['to_date']) {
            $select->where("DATE_FORMAT(d.create_at,'%Y-%m-%d') <= ?", $params['to_date']);
        }

        $select->group('r.area_id');

        $result = $db->fetchAll($select);
        
        foreach ($result as $element) {
            $destructionCost [$element['area_id']] = $element['total_cost'];
        }

        return $destructionCost;
    }

}