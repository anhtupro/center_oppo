<?php

class Application_Model_SpAssessmentScores extends Zend_Db_Table_Abstract
{
    protected $_name = 'sp_assessment_scores';

    public function getOtherScores($staff_id){
        
        $db = Zend_Registry::get("db");
        $select = $db->select();
    
        $arrCols = array(
            "p.scores",
            "p.title",
            "p.staff_id",
            "p.created_by",
            "p.created_at",
        );

        $select->from(array('p' => 'sp_assessment_scores'), $arrCols);
        
        $select->where('p.is_del = 0');
        $select->where('p.staff_id = ?', $staff_id);
        
        $result = $db->fetchAll($select);
        
        return $result;
    }
}    