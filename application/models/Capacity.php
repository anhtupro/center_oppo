<?php
class Application_Model_Capacity extends Zend_Db_Table_Abstract
{
    protected $_name = 'capacity';
    
    function fetchPagination($page, $limit, &$total, $params){
        
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.from_staff'),
            'p.to_staff',
            'title_id' => 's.title', 
            'title_name' => 't.name', 
            'team_id' => 't2.id', 
            'team_name' => 't2.name',
            'department_id' => 't3.id',
            'department_name' => 't3.name',
            'capacity_id' => 'c.id',
            'is_lock' => 'c.is_lock',
            'field_number' => 'COUNT( f.id )',
        );

        $select->from(array('p' => 'appraisal_office_to_do_root'), $arrCols);
        $select->joinLeft(array('s' => 'staff'), 's.id = p.to_staff', array());
        $select->joinLeft(array('t' => 'team'), 't.id = s.title', array());
        $select->joinLeft(array('t2' => 'team'), 't2.id = t.parent_id', array());
        $select->joinLeft(array('t3' => 'team'), 't3.id = t2.parent_id', array());
        $select->joinLeft(array('c' => 'capacity'), 'c.title_id = t.id AND c.is_deleted = 0', array());
        $select->joinLeft(array('f' => 'capacity_field'), 'f.capacity_id = c.id AND f.is_deleted = 0', array());
        
        //$select->where('t.is_hidden = 0');
        $select->where('s.off_date IS NULL', NULL);
        $select->where('p.to_staff IS NOT NULL', NULL);
        $select->where('p.is_del = 0 OR p.is_del IS NULL', NULL);
        //$select->where('p.is_prd = 1', NULL);
        
        if(!empty($params['title'])){
            $select->where("t.id IN (?)", $params['title']);
        }
        
        if(!empty($params['team'])){
            $select->where("t2.id IN (?)", $params['team']);
        }
        
        if(!empty($params['department'])){
            $select->where("t3.id IN (?)", $params['department']);
        }
        
        if($params['level'] == 2){
            $select->where("p.from_staff = ?", $params['staff_id']);
        }
        
        $select->group('s.title');
        $select->order(['t3.id', 't2.id', 't.name ASC']);

        if($_GET['dev'] == 1){
            echo $select;exit;
        }

        if (! $params['export']) {
            $select->limitPage($page, $limit);
        }
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        
        return $result;
    }
    
    function fetchPaginationStaff($page, $limit, &$total, $params){
        
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.from_staff'),
            'fullname' => "CONCAT(s.firstname, ' ', s.lastname)",
            "id" => 'p.to_staff',
            'title_id' => 's.title', 
            'title_name' => 't.name', 
            'team_id' => 't2.id', 
            'team_name' => 't2.name',
            'department_id' => 't3.id',
            'department_name' => 't3.name',
            'capacity_id' => 'c.id',
            'is_lock' => 'c.is_lock',
            'field_number' => 'COUNT( f.id )',
            'plan_title' => 'GROUP_CONCAT(DISTINCT plan.title)',
            'plan_title2' => 'GROUP_CONCAT(DISTINCT plan2.title)',
            'plan_id' => 'GROUP_CONCAT(DISTINCT plan.id)',
            'plan_id2' => 'GROUP_CONCAT(DISTINCT plan2.id)',
        );

        $select->from(array('p' => 'appraisal_office_to_do_root'), $arrCols);
        $select->joinLeft(array('s' => 'staff'), 's.id = p.to_staff', array());
        $select->joinLeft(array('t' => 'team'), 't.id = s.title', array());
        $select->joinLeft(array('t2' => 'team'), 't2.id = t.parent_id', array());
        $select->joinLeft(array('t3' => 'team'), 't3.id = t2.parent_id', array());
        $select->joinLeft(array('c' => 'capacity'), 'c.title_id = t.id AND c.is_deleted = 0', array());
        $select->joinLeft(array('f' => 'capacity_field'), 'f.capacity_id = c.id AND f.is_deleted = 0', array());
        $select->joinLeft(array('sc' => 'staff_capacity'), 'sc.staff_id = s.id AND sc.is_deleted = 0 AND sc.is_head_appraisal = 1', array());
        $select->joinLeft(array('plan' => 'capacity_plan'), 'plan.id = sc.plan_id', array());
        $select->joinLeft(array('sc2' => 'staff_capacity'), 'sc2.staff_id = s.id AND sc2.is_deleted = 0 AND sc2.is_staff_appraisal = 1', array());
        $select->joinLeft(array('plan2' => 'capacity_plan'), 'plan2.id = sc2.plan_id', array());
        
        $select->where('s.off_date IS NULL', NULL);
        $select->where('p.to_staff IS NOT NULL', NULL);
        $select->where('p.is_del = 0 OR p.is_del IS NULL', NULL);
        //$select->where('p.is_prd = 1', NULL);
        
        if(!empty($params['title'])){
            $select->where("t.id IN (?)", $params['title']);
        }
        
        if(!empty($params['team'])){
            $select->where("t2.id IN (?)", $params['team']);
        }
        
        if(!empty($params['department'])){
            $select->where("t3.id IN (?)", $params['department']);
        }
        
        if($params['level'] == 2){
            $select->where("p.from_staff = ?", $params['staff_id']);
        }
        
        $select->group('s.id');
        $select->order(['t3.id','t2.id', 't.id', 's.firstname ASC']);
        
        
        if (! $params['export']) {
            $select->limitPage($page, $limit);
        }
        
        if($_GET['dev']){
            echo $select;exit;
        }
        
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        
        return $result;
    }
    
    public function getField($title_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "capacity_id" => "p.id", 
            "field_id" => "f.id", 
            "f.title",
            "f.kpi",
            "f.desc",
            "f.ratio",
        );

        $select->from(array('p' => 'capacity'), $arrCols);
        $select->joinLeft(array('f' => 'capacity_field'), 'f.capacity_id = p.id', array());
        
        $select->where('p.title_id = ?', $title_id);
        $select->where('p.is_deleted = 0', NULL);
        $select->where('f.is_deleted = 0', NULL);
        $result = $db->fetchAll($select);

        return $result;
    }
    
    
    public function getFieldByStaff($staff_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "capacity_id" => "p.id", 
            "field_id" => "f.id", 
            "f.title", 
            "f.kpi",
            "f.desc",
            "f.ratio",
        );

        $select->from(array('p' => 'capacity'), $arrCols);
        $select->joinLeft(array('f' => 'capacity_field'), 'f.capacity_id = p.id', array());
        
        $select->where('p.staff_id = ?', $staff_id);
        $select->where('f.is_deleted = 0', NULL);
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getDictionary($title_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "d.id", 
            "d.title", 
            "d.type", 
            "p.level", 
            "p.share",
            "p.field_id",
            'dictionary_field_id' => 'p.id',
            'p.dictionary_id'
        );

        $select->from(array('p' => 'capacity_field_dictionary'), $arrCols);
        $select->joinLeft(array('d' => 'capacity_dictionary'), 'd.id = p.dictionary_id', array());
        $select->joinLeft(array('f' => 'capacity_field'), 'f.id = p.field_id', array());
        $select->joinLeft(array('c' => 'capacity'), 'c.id = f.capacity_id', array());

        $select->where('c.title_id = ?', $title_id);
        $select->where('p.is_deleted = 0', NULL);
        $select->where('d.is_deleted = 0 OR d.is_deleted IS NULL', NULL);
        $select->order(['d.type', 'd.title']);
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getDictionaryByStaff($staff_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "d.id", 
            "d.title", 
            "d.type", 
            "p.level", 
            "p.share",
            "p.field_id",
            'dictionary_field_id' => 'p.id',
            'p.dictionary_id'
        );

        $select->from(array('p' => 'capacity_field_dictionary'), $arrCols);
        $select->joinLeft(array('d' => 'capacity_dictionary'), 'd.id = p.dictionary_id', array());
        $select->joinLeft(array('f' => 'capacity_field'), 'f.id = p.field_id', array());
        $select->joinLeft(array('c' => 'capacity'), 'c.id = f.capacity_id', array());

        $select->where('c.staff_id = ?', $staff_id);
        $select->where('p.is_deleted = 0', NULL);
        $select->where('d.is_deleted = 0 OR d.is_deleted IS NULL', NULL);
        $select->order(['d.type', 'd.title']);
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getScience($title_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.capacity_field_id",
            "p.desc",
            "p.share",
            "p.level",
            "capacity_science_id" => "p.id",
            "field_id" => "p.capacity_field_id",
        );

        $select->from(array('p' => 'capacity_science'), $arrCols);
        $select->joinLeft(array('f' => 'capacity_field'), 'f.id = p.capacity_field_id', array());
        $select->joinLeft(array('c' => 'capacity'), 'c.id = f.capacity_id', array());

        $select->where('c.title_id = ?', $title_id);
        $select->where('p.is_deleted = 0', NULL);
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getScienceByStaff($staff_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.capacity_field_id",
            "p.desc",
            "p.share",
            "p.level",
            "capacity_science_id" => "p.id",
            "field_id" => "p.capacity_field_id",
        );

        $select->from(array('p' => 'capacity_science'), $arrCols);
        $select->joinLeft(array('f' => 'capacity_field'), 'f.id = p.capacity_field_id', array());
        $select->joinLeft(array('c' => 'capacity'), 'c.id = f.capacity_id', array());

        $select->where('c.staff_id = ?', $staff_id);
        $select->where('p.is_deleted = 0', NULL);
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getListLevel($capacity_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "dictionary.id", 
            "p.title_id",
            "title_name" => "dictionary.title", 
            "type" => '(CASE WHEN (dictionary.type = 1) THEN "Thái độ" 
                                       WHEN (dictionary.type = 2) THEN "Kỹ năng" ELSE "" END)', 
            "d.share", 
            "d.level", 
            "level_name" => "l.title",
            'dictionary_id' => 'dictionary.id',
            'field_id' => 'f.id',
        );

        $select->from(array('p' => 'capacity'), $arrCols);
        $select->joinLeft(array('f' => 'capacity_field'), 'f.capacity_id = p.id AND f.is_deleted = 0 ', array());
        $select->joinLeft(array('d' => 'capacity_field_dictionary'), 'd.field_id = f.id AND d.is_deleted = 0', array());
        $select->joinLeft(array('dictionary' => 'capacity_dictionary'), 'dictionary.id = d.dictionary_id', array());
        $select->joinLeft(array('l' => 'capacity_dictionary_level'), 'l.dictionary_id = dictionary.id AND l.level = d.level', array());

        $select->where('dictionary.id IS NOT NULL ', NULL);
        $select->where('p.id = ?', $capacity_id);
        $select->order(['dictionary.type', 'dictionary.id', 'dictionary.title']);
        
        $result = $db->fetchAll($select);
        

        return $result;
    }
    
    public function getListLevelGroup($capacity_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "dictionary.id", 
            "p.title_id",
            "title_name" => "dictionary.title", 
            "type" => '(CASE WHEN (dictionary.type = 1) THEN "Thái độ" 
                                       WHEN (dictionary.type = 2) THEN "Kỹ năng" ELSE "" END)', 
            "type_id" => "dictionary.type",
            "d.share", 
            "d.level", 
            "level_name" => "l.title",
            'dictionary_id' => 'dictionary.id',
            'field_id' => 'f.id',
        );

        $select->from(array('p' => 'capacity'), $arrCols);
        $select->joinLeft(array('f' => 'capacity_field'), 'f.capacity_id = p.id', array());
        $select->joinLeft(array('d' => 'capacity_field_dictionary'), 'd.field_id = f.id', array());
        $select->joinLeft(array('dictionary' => 'capacity_dictionary'), 'dictionary.id = d.dictionary_id', array());
        $select->joinLeft(array('l' => 'capacity_dictionary_level'), 'l.dictionary_id = dictionary.id AND l.level = d.level', array());
        
        $select->where('f.is_deleted = 0 ', NULL);
        $select->where('(d.is_deleted = 0 OR d.is_deleted IS NULL) AND dictionary.id IS NOT NULL', NULL);
        $select->where('p.id = ?', $capacity_id);
        $select->where('dictionary.is_deleted = 0 OR dictionary.is_deleted IS NULL', NULL);
        //$select->group('dictionary.id');
        $select->order(['dictionary.type', 'dictionary.id', 'dictionary.title']);
        $result = $db->fetchAll($select);
        

        return $result;
    }
    
    public function getListLevelGroupBy($capacity_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "dictionary.id", 
            "p.title_id",
            "title_name" => "dictionary.title", 
            "type" => '(CASE WHEN (dictionary.type = 1) THEN "Tư duy" 
                                       WHEN (dictionary.type = 2) THEN "Kỹ năng" ELSE "" END)', 
            "type_id" => "dictionary.type",
            "d.share", 
            "level" => "MAX(d.level)", 
            "level_name" => "l.title",
            'dictionary_id' => 'dictionary.id',
            'field_id' => 'f.id',
        );

        $select->from(array('p' => 'capacity'), $arrCols);
        $select->joinLeft(array('f' => 'capacity_field'), 'f.capacity_id = p.id', array());
        $select->joinLeft(array('d' => 'capacity_field_dictionary'), 'd.field_id = f.id', array());
        $select->joinLeft(array('dictionary' => 'capacity_dictionary'), 'dictionary.id = d.dictionary_id', array());
        $select->joinLeft(array('l' => 'capacity_dictionary_level'), 'l.dictionary_id = dictionary.id AND l.level = d.level', array());
        
        $select->where('f.is_deleted = 0 ', NULL);
        $select->where('(d.is_deleted = 0 OR d.is_deleted IS NULL) AND dictionary.id IS NOT NULL', NULL);
        $select->where('p.id = ?', $capacity_id);
        $select->where('dictionary.is_deleted = 0 OR dictionary.is_deleted IS NULL', NULL);
        $select->group('dictionary.id');
        $select->order(['dictionary.type', 'dictionary.title']);
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getListScience($capacity_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "type" => "('Kiến thức')",
            "title_name" => "p.desc", 
            "p.level", 
            "f.capacity_id",
            'field_id' => 'capacity_field_id',
            'capacity_science_id' => 'p.id'
        );

        $select->from(array('p' => 'capacity_science'), $arrCols);
        $select->joinLeft(array('f' => 'capacity_field'), 'f.id = p.capacity_field_id', array());

        $select->where('f.capacity_id = ? ', $capacity_id);
        $select->where('p.is_deleted = 0', NULL);
        $select->where('f.is_deleted = 0', NULL);
        $result = $db->fetchAll($select);
        
        return $result;
    }
    
    
    public function getInfoCapacity($capacity_id) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.title_id", 
            "p.staff_id",
            "title_name" => "t.name", 
            "team_name" => "t2.name", 
            "department_name" => "t3.name" 
        );

        $select->from(array('p' => 'capacity'), $arrCols);
        $select->joinLeft(array('t' => 'team'), 't.id = p.title_id', array());
        $select->joinLeft(array('t2' => 'team'), 't2.id = t.parent_id', array());
        $select->joinLeft(array('t3' => 'team'), 't3.id = t2.parent_id', array());

        $select->where('p.id = ?', $capacity_id);
        $result = $db->fetchRow($select);

        return $result;
    }
    
    public function getInfoCapacityStaff($staff_id) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "staff_id" => "p.id",
            "title_id" => "p.title",
            "title_name" => "t.name",
            "team_name" => "t2.name",
            "department_name" => "t3.name",
            "fullname" => "CONCAT(p.firstname, ' ', p.lastname)"
        );

        $select->from(array('p' => 'staff'), $arrCols);
        $select->joinLeft(array('t' => 'team'), 't.id = p.title', array());
        $select->joinLeft(array('t2' => 'team'), 't2.id = t.parent_id', array());
        $select->joinLeft(array('t3' => 'team'), 't3.id = t2.parent_id', array());

        $select->where('p.id = ?', $staff_id);
        $result = $db->fetchRow($select);

        return $result;
    }
    
    public function getCapacityByStaff($staff_id) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "fullname" => "CONCAT(p.firstname, ' ', p.lastname)" , 
            "title_id" => "p.title" , 
            "capacity_id" => "c.id" ,
        );

        $select->from(array('p' => 'staff'), $arrCols);
        $select->joinLeft(array('c' => 'capacity'), 'c.title_id = p.title', array());

        $select->where('p.id = ?', $staff_id);
        $result = $db->fetchRow($select);

        return $result;
    }
    
    public function getCapacityByStaffDetails($staff_id) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "fullname" => "CONCAT(p.firstname, ' ', p.lastname)" , 
            "title_id" => "p.title" , 
            "capacity_id" => "c.id" ,
        );

        $select->from(array('p' => 'staff'), $arrCols);
        $select->joinLeft(array('c' => 'capacity'), 'c.staff_id = p.id AND c.is_deleted = 0', array());

        $select->where('c.staff_id = ?', $staff_id);
        $result = $db->fetchRow($select);

        return $result;
    }
    
    public function getInfoStaffCapacity($staff_id) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.staff_id", 
            "p.title_id", 
            "p.is_deleted", 
            "p.created_at", 
            "created_by" => "CONCAT(s.firstname,' ', s.lastname)",
        );

        $select->from(array('p' => 'staff_capacity'), $arrCols);
        $select->joinLeft(array('s' => 'staff'), 's.id = p.created_by', array());

        $select->where('p.staff_id = ?', $staff_id);
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getInfoStaff($staff_id) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "fullname" => "CONCAT(p.firstname, ' ', p.lastname)" , 
            "title_name" => "t.name", 
            "team_name" => "t2.name", 
            "department_name" => "t3.name",
        );

        $select->from(array('p' => 'staff'), $arrCols);
        $select->joinLeft(array('t' => 'team'), 't.id = p.title', array());
        $select->joinLeft(array('t2' => 'team'), 't2.id = p.team', array());
        $select->joinLeft(array('t3' => 'team'), 't3.id = p.department', array());

        $select->where('p.id = ?', $staff_id);
        $result = $db->fetchRow($select);

        return $result;
    }
    
    function fetchPaginationResult($page, $limit, &$total, $params){
        
        $db = Zend_Registry::get("db");
        $select = $db->select();
        
        $sub_select = "
            SELECT p.field_id, p.dictionary_id, p.level, p.share, p.appraisal_head_level, p.appraisal_staff_level
            FROM `staff_capacity_field_dictionary`p
            WHERE p.is_deleted = 0
            UNION
            SELECT p.capacity_field_id, 15, p.level, p.share, p.appraisal_head_level, p.appraisal_staff_level
            FROM staff_capacity_science p
            WHERE p.is_deleted = 0
        ";
        
        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            "fullname" => "CONCAT(s.firstname, ' ', s.lastname)", 
            "p.staff_id",
            "p.plan_id",
            "s.code",
            "title_name" => "t.name",
            "team_name" => "t2.name", 
            "department_name" => "t3.name", 
            "plan_title" => "c.title",
            "point" => "(SUM(d.appraisal_head_level/d.level*d.`share`)  / SUM(d.`share`) * 100)",
            "point_staff" => "(SUM(d.appraisal_staff_level/d.level*d.`share`)  / SUM(d.`share`) * 100)",
            "staff_capacity_id" => "f.capacity_id",
            "p.is_staff_appraisal",
            "p.is_head_appraisal"
        );

        $select->from(array('p' => 'staff_capacity'), $arrCols);
        $select->joinLeft(array('s' => 'staff'), 's.id = p.staff_id', array());
        $select->joinLeft(array('root' => 'appraisal_office_to_do_root'), 'root.to_staff = s.id', array());
        $select->joinLeft(array('t' => 'team'), 't.id = s.title', array());
        $select->joinLeft(array('t2' => 'team'), 't2.id = t.parent_id', array());
        $select->joinLeft(array('t3' => 'team'), 't3.id = t2.parent_id', array());
        $select->joinLeft(array('c' => 'capacity_plan'), 'c.id = p.plan_id', array());
        $select->joinLeft(array('f' => 'staff_capacity_field'), 'f.capacity_id = p.id AND f.is_deleted = 0', array());
        $select->joinLeft(array('d' =>  new Zend_Db_Expr('(' . $sub_select . ')')), 'd.field_id = f.id', array());
        
        $select->where('p.is_deleted = 0', NULL);
        $select->where('s.off_date IS NULL', NULL);
        
        if(!empty($params['title'])){
            $select->where("p.id IN (?)", $params['title']);
        }
        
        if(!empty($params['team'])){
            $select->where("t2.id IN (?)", $params['team']);
        }
        
        if(!empty($params['department'])){
            $select->where("t3.id IN (?)", $params['department']);
        }
        
        if(!empty($params['plan_id'])){
            $select->where("c.id = ?", $params['plan_id']);
        }
        
        if($params['level'] == 2){
            $select->where("root.from_staff = ?", $params['staff_id']);
        }
        
        if($params['level'] == 3){
            $select->where("s.id = ?", $params['staff_id']);
        }
        
        $select->group('p.id');
        $select->order(['t2.id', 't.id', 'p.id']);
        
        if(empty($params['export'])){
            $select->limitPage($page, $limit);
        }
        
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        
        return $result;
    }
    
    function getStaffViewCapacity($staff_capacity_id){
        
        $db = Zend_Registry::get("db");
        $select = $db->select();
        
        $sub_select = "
            SELECT p.field_id, p.dictionary_id, p.level, p.share, p.appraisal_head_level, p.appraisal_staff_level, d.title dictionary_title, l.title level_name, d.type
            FROM `staff_capacity_field_dictionary` p
            LEFT JOIN capacity_dictionary d ON d.id = p.dictionary_id
            LEFT JOIN capacity_dictionary_level l ON l.dictionary_id = p.dictionary_id AND l.level = p.level
            WHERE p.is_deleted = 0 AND (d.is_deleted = 0 OR d.is_deleted IS NULL)
            UNION
            SELECT p.capacity_field_id, 15, p.level, p.share, p.appraisal_head_level, p.appraisal_staff_level, p.desc dictionary_title, l.title level_name, 0
            FROM staff_capacity_science p
            LEFT JOIN capacity_dictionary d ON d.id = 15
            LEFT JOIN capacity_dictionary_level l ON l.dictionary_id = 15 AND l.level = p.level
            WHERE p.is_deleted = 0
        ";
        
        $arrCols = array(
            "p.id",
            "fullname" => "CONCAT(s.firstname, ' ', s.lastname)", 
            "p.staff_id", 
            "title_name" => "t.name",
            "team_name" => "t2.name", 
            "department_name" => "t3.name", 
            "plan_title" => "c.title",
            "staff_capacity_id" => "f.capacity_id",
            "dictionary_id" => "d.dictionary_id",
            "dictionary_title" => "d.dictionary_title",
            "level_name" => "d.level_name",
            "type" => "d.type",
            "appraisal_staff_level" => "d.appraisal_staff_level",
            "appraisal_head_level" => "d.appraisal_head_level",
        );

        $select->from(array('p' => 'staff_capacity'), $arrCols);
        $select->joinLeft(array('s' => 'staff'), 's.id = p.staff_id', array());
        $select->joinLeft(array('t' => 'team'), 't.id = s.title', array());
        $select->joinLeft(array('t2' => 'team'), 't2.id = t.parent_id', array());
        $select->joinLeft(array('t3' => 'team'), 't3.id = t2.parent_id', array());
        $select->joinLeft(array('c' => 'capacity_plan'), 'c.id = p.plan_id', array());
        $select->joinLeft(array('f' => 'staff_capacity_field'), 'f.capacity_id = p.id AND f.is_deleted = 0', array());
        $select->joinLeft(array('d' =>  new Zend_Db_Expr('(' . $sub_select . ')')), 'd.field_id = f.id', array());
        
        $select->where('p.is_deleted = 0', NULL);
        $select->where('d.dictionary_id IS NOT NULL', NULL);
        
        $select->where('p.id = ?', $staff_capacity_id);
        
        $select->order(['d.type', 'd.dictionary_title']);
        
        $result = $db->fetchAll($select);
        
        return $result;
    }
    
    function getStaffViewCapacityGroup($staff_capacity_id){
        
        $db = Zend_Registry::get("db");
        $select = $db->select();
        
        $sub_select = "
            SELECT p.field_id, p.dictionary_id, p.level, p.share, p.appraisal_head_level, p.appraisal_staff_level, d.title dictionary_title, l.title level_name, d.type
            FROM `staff_capacity_field_dictionary` p
            LEFT JOIN capacity_dictionary d ON d.id = p.dictionary_id
            LEFT JOIN capacity_dictionary_level l ON l.dictionary_id = p.dictionary_id AND l.level = p.level
            WHERE p.is_deleted = 0 AND (d.is_deleted = 0 OR d.is_deleted IS NULL)
            UNION
            SELECT p.capacity_field_id, 15, p.level, p.share, p.appraisal_head_level, p.appraisal_staff_level, p.desc dictionary_title, l.title level_name, 0
            FROM staff_capacity_science p
            LEFT JOIN capacity_dictionary d ON d.id = 15
            LEFT JOIN capacity_dictionary_level l ON l.dictionary_id = 15 AND l.level = p.level
            WHERE p.is_deleted = 0
        ";
        
        $arrCols = array(
            "p.id",
            "fullname" => "CONCAT(s.firstname, ' ', s.lastname)", 
            "p.staff_id", 
            "title_name" => "t.name",
            "team_name" => "t2.name", 
            "department_name" => "t3.name", 
            "plan_title" => "c.title",
            "staff_capacity_id" => "f.capacity_id",
            "dictionary_id" => "d.dictionary_id",
            "dictionary_title" => "d.dictionary_title",
            "level_name" => "d.level_name",
            "type" => "d.type",
            "appraisal_staff_level" => "d.appraisal_staff_level",
            "appraisal_head_level" => "d.appraisal_head_level",
        );

        $select->from(array('p' => 'staff_capacity'), $arrCols);
        $select->joinLeft(array('s' => 'staff'), 's.id = p.staff_id', array());
        $select->joinLeft(array('t' => 'team'), 't.id = s.title', array());
        $select->joinLeft(array('t2' => 'team'), 't2.id = t.parent_id', array());
        $select->joinLeft(array('t3' => 'team'), 't3.id = t2.parent_id', array());
        $select->joinLeft(array('c' => 'capacity_plan'), 'c.id = p.plan_id', array());
        $select->joinLeft(array('f' => 'staff_capacity_field'), 'f.capacity_id = p.id AND f.is_deleted = 0', array());
        $select->joinLeft(array('d' =>  new Zend_Db_Expr('(' . $sub_select . ')')), 'd.field_id = f.id', array());
        
        $select->where('p.is_deleted = 0', NULL);
        $select->where('d.dictionary_id IS NOT NULL', NULL);
        
        $select->where('p.id = ?', $staff_capacity_id);
        
        $select->group(['d.dictionary_id']);
        
        $select->order(['d.type', 'd.dictionary_title']);
        
        $result = $db->fetchAll($select);
        
        return $result;
    }
    
    function getDictionaryStaff($params){
        
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.field_id", 
            "p.dictionary_id", 
            "p.level", 
            "p.appraisal_staff_level", 
            "p.appraisal_head_level",
            "p.share",
            "f.capacity_id", 
            "c.staff_id",
            "dictionary_name" => "d.title"
        );

        $select->from(array('p' => 'staff_capacity_field_dictionary'), $arrCols);
        $select->joinLeft(array('d' => 'capacity_dictionary'), 'd.id = p.dictionary_id', array());
        $select->joinLeft(array('f' => 'staff_capacity_field'), 'f.id = p.field_id', array());
        $select->joinLeft(array('c' => 'staff_capacity'), 'c.id = f.capacity_id', array());
        
        $select->where('p.is_deleted = 0', NULL);
        $select->where('f.is_deleted = 0', NULL);
        $select->where('c.is_deleted = 0', NULL);
        
        if(!empty($params['list_staff'])){
            $select->where("c.staff_id IN (?)", $params['list_staff']);
        }
        
        $select->order(['d.type']);
        
        
        $result = $db->fetchAll($select);
        
        return $result;
    }
    
    function getScienceStaff($params){
        
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "field_id" => "p.capacity_field_id",  
            "p.level", 
            "p.appraisal_staff_level", 
            "p.appraisal_head_level",
            "p.share",
            "f.capacity_id", 
            "c.staff_id"
        );

        $select->from(array('p' => 'staff_capacity_science'), $arrCols);
        $select->joinLeft(array('f' => 'staff_capacity_field'), 'f.id = p.capacity_field_id', array());
        $select->joinLeft(array('c' => 'staff_capacity'), 'c.id = f.capacity_id', array());
        
        $select->where('p.is_deleted = 0', NULL);
        $select->where('f.is_deleted = 0', NULL);
        $select->where('c.is_deleted = 0', NULL);
        
        if(!empty($params['list_staff'])){
            $select->where("c.staff_id IN (?)", $params['list_staff']);
        }
        
        $result = $db->fetchAll($select);
        
        return $result;
    }
    
    function getDictionaryScience(){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'id',
            'dictionary_id',
            'title',
            'level'
        );

        $select->from(array('p' => 'capacity_dictionary_level'), $arrCols);
        $select->where('p.dictionary_id = ?', 15);
        $select->order('p.level DESC');
        $result = $db->fetchAll($select);

        return $result;
    }
    
    
    function getStaffList($title_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "title_id" => "p.title", 
            "fullname" => "CONCAT(p.firstname, ' ', p.lastname)",
            "capacity_id" => "c.id"
        );

        $select->from(array('p' => 'staff'), $arrCols);
        $select->joinLeft(array('c' => 'capacity'), 'c.staff_id = p.id AND c.is_deleted = 0', array());

        $select->where('p.title = ?', $title_id);
        $select->where('p.off_date IS NULL', NULL);
        $result = $db->fetchAll($select);

        return $result;
    }
    
    function getInfoTitle($title_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "title_id" => "p.id", 
            "title_name" => "p.name",
            "team_id" => "t.id",
            "team_name" => "t.name",
            "department_id" => "t2.id",
            "department_name" => "t2.name",
        );

        $select->from(array('p' => 'team'), $arrCols);
        $select->joinLeft(array('t' => 'team'), 't.id = p.parent_id', array());
        $select->joinLeft(array('t2' => 'team'), 't2.id = t.parent_id', array());

        $select->where('p.id = ?', $title_id);
        $result = $db->fetchRow($select);

        return $result;
    }

    public function getDataExportTitle($params)
    {
        $db = Zend_Registry::get("db");
        $select_dictionary = $db->select();
        $select_science = $db->select();

        $sub_select = "
            SELECT d.id, MAX(d.level) as level, d.field_id, MAX(d.share) as share,  d.dictionary_id from capacity c 
            JOIN capacity_field f ON c.id = f.capacity_id AND f.is_deleted = 0
            JOIN capacity_field_dictionary d ON f.id = d.field_id AND d.is_deleted = 0
            WHERE c.is_deleted = 0 
            GROUP BY c.id, dictionary_id
        ";

        // select dictionary
        $arrColsDictionary = array(
            'title_id' => 't.id',
            'capacity_id' => 'c.id',
            'field_id' => 'f.id',
            'title_name' => 't.name',
            'team_name' => 't2.name',
            'department_name' => 't3.name',
            'is_lock' => 'c.is_lock',
            'dictionary' => 'i.title',
            'd.level',
            'd.share'
        );
        
        $select_dictionary->from(array('p' => 'appraisal_office_to_do_root'), $arrColsDictionary);
        $select_dictionary->joinLeft(array('s' => 'staff'), 's.id = p.to_staff', array());
        $select_dictionary->joinLeft(array('t' => 'team'), 't.id = s.title', array());
        $select_dictionary->joinLeft(array('t2' => 'team'), 't2.id = t.parent_id', array());
        $select_dictionary->joinLeft(array('t3' => 'team'), 't3.id = t2.parent_id', array());
        $select_dictionary->join(array('c' => 'capacity'), 'c.title_id = t.id AND c.is_deleted = 0', array());
        $select_dictionary->join(array('f' => 'capacity_field'), 'f.capacity_id = c.id AND f.is_deleted = 0', array());
//        $select_dictionary->join(array('d' => 'capacity_field_dictionary'), 'f.id = d.field_id AND d.is_deleted = 0', array());

        $select_dictionary->join(array('d' => new Zend_Db_Expr('(' . $sub_select . ')')), 'f.id = d.field_id', array());

        $select_dictionary->join(array('i' => 'capacity_dictionary'), 'd.dictionary_id = i.id AND (i.is_deleted IS NULL OR i.is_deleted = 0)', array());
        $select_dictionary->where('s.off_date IS NULL', NULL);
        $select_dictionary->group('d.id');


        // select science
        $arrColsScience = array(
            'title_id' => 't.id',
            'capacity_id' => 'c.id',
            'field_id' => 'f.id',
            'title_name' => 't.name',
            'team_name' => 't2.name',
            'department_name' => 't3.name',
            'is_lock' => 'c.is_lock',
            'dictionary' => 'd.desc',
            'd.level',
            'd.share'
        );
        
        
        $select_science->from(array('p' => 'appraisal_office_to_do_root'), $arrColsScience);
        $select_science->joinLeft(array('s' => 'staff'), 's.id = p.to_staff', array());
        $select_science->joinLeft(array('t' => 'team'), 't.id = s.title', array());
        $select_science->joinLeft(array('t2' => 'team'), 't2.id = t.parent_id', array());
        $select_science->joinLeft(array('t3' => 'team'), 't3.id = t2.parent_id', array());
        $select_science->joinLeft(array('c' => 'capacity'), 'c.title_id = t.id AND c.is_deleted = 0', array());
        $select_science->joinLeft(array('f' => 'capacity_field'), 'f.capacity_id = c.id AND f.is_deleted = 0', array());
        $select_science->joinLeft(array('d' => 'capacity_science'), 'f.id = d.capacity_field_id AND d.is_deleted = 0', array());
        $select_science->where('s.off_date IS NULL', NULL);
        $select_science->group('d.id');


        if(!empty($params['title'])){
            $select_dictionary->where("t.id IN (?)", $params['title']);
            $select_science->where("t.id IN (?)", $params['title']);
        }

        if(!empty($params['team'])){
            $select_dictionary->where("t2.id IN (?)", $params['team']);
            $select_science->where("t2.id IN (?)", $params['team']);
        }

        if(!empty($params['department'])){
            $select_dictionary->where("t3.id IN (?)", $params['department']);
            $select_science->where("t3.id IN (?)", $params['department']);
        }

        if($params['level'] == 2){
            $select_dictionary->where("p.from_staff = ?", $params['staff_id']);
            $select_science->where("p.from_staff = ?", $params['staff_id']);
        }

        $select_union = $db->select()->union(array($select_dictionary, $select_science), Zend_Db_Select::SQL_UNION_ALL)->order('title_id');

        $result = $db->fetchAll($select_union);

        return $result;
    }
    
    public function getDataExportTitleSetup($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();
        
        $sub_select_type_1 = "
            SELECT p.field_id, p.id, p.dictionary_id,  p.`level`, p.`share`
            FROM capacity_field_dictionary p
            LEFT JOIN capacity_dictionary d ON d.id = p.dictionary_id
            WHERE d.type = 1 AND p.is_deleted = 0
        ";
        
        $sub_select_type_2 = "
            SELECT p.field_id, p.id, p.dictionary_id,  p.`level`, p.`share`
            FROM capacity_field_dictionary p
            LEFT JOIN capacity_dictionary d ON d.id = p.dictionary_id
            WHERE d.type = 2 AND p.is_deleted = 0
        ";
        
        $arrCols = array(
            "p.id", 
            "p.title_id",
            "title_name" => "t.name",
            "team_name" => "t2.name",
            "department_name" => "t3.name",
            "title_field" => "f.title", 
            "kpi_field" => "f.kpi",
            "desc_field" => "f.desc",
            "f.kpi", 
            "f.desc", 
            "f.ratio", 
            "dictionary_type_1" => "GROUP_CONCAT(DISTINCT CONCAT(d.id,'-',d.dictionary_id,'-',d.level,'-',d.share) SEPARATOR ';')", 
            "dictionary_type_2" => "GROUP_CONCAT(DISTINCT CONCAT(d2.id,'-',d2.dictionary_id,'-',d2.level,'-',d2.share) SEPARATOR ';')",
            "science_desc" => "s.desc", 
            "science_level" => "s.level", 
            "science_share" => "s.share"
        );

        $select->from(array('p' => 'capacity'), $arrCols);
        $select->joinLeft(array('t' => 'team'), 't.id = p.title_id', array());
        $select->joinLeft(array('t2' => 'team'), 't2.id = t.parent_id', array());
        $select->joinLeft(array('t3' => 'team'), 't3.id = t2.parent_id', array());
        $select->joinLeft(array('f' => 'capacity_field'), 'f.capacity_id = p.id', array());
        $select->joinLeft(array('s' => 'capacity_science'), 's.capacity_field_id = f.id AND s.is_deleted = 0', array());
        $select->joinLeft(array('d' => new Zend_Db_Expr('(' . $sub_select_type_1 . ')')), 'd.field_id = f.id', array());
        $select->joinLeft(array('d2' => new Zend_Db_Expr('(' . $sub_select_type_2 . ')')), 'd2.field_id = f.id', array());
        
        $select->where('f.is_deleted = 0');
        $select->where('p.is_deleted = 0');
        $select->where('p.title_id IS NOT NULL');
        
        if(!empty($params['title'])){
            $select->where("t.id IN (?)", $params['title']);
        }

        if(!empty($params['team'])){
            $select->where("t2.id IN (?)", $params['team']);
        }

        if(!empty($params['department'])){
            $select->where("t3.id IN (?)", $params['department']);
        }

        if($params['level'] == 2){
            $select->where("p.from_staff = ?", $params['staff_id']);
        }
        
        $select->group('f.id');
        $select->order('p.id');
        
        $result = $db->fetchAll($select);
        
        return $result;
    }
    
    public function getDataExportStaffSetup($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();
        
        $sub_select_type_1 = "
            SELECT p.field_id, p.id, p.dictionary_id,  p.`level`, p.`share`
            FROM capacity_field_dictionary p
            LEFT JOIN capacity_dictionary d ON d.id = p.dictionary_id
            WHERE d.type = 1 AND p.is_deleted = 0
        ";
        
        $sub_select_type_2 = "
            SELECT p.field_id, p.id, p.dictionary_id,  p.`level`, p.`share`
            FROM capacity_field_dictionary p
            LEFT JOIN capacity_dictionary d ON d.id = p.dictionary_id
            WHERE d.type = 2 AND p.is_deleted = 0
        ";
        
        $arrCols = array(
            "p.id", 
            "p.title_id",
            "title_name" => "t.name",
            "team_name" => "t2.name",
            "department_name" => "t3.name",
            "title_field" => "f.title", 
            "kpi_field" => "f.kpi",
            "desc_field" => "f.desc",
            "fullname" => "CONCAT(staff.firstname,' ',staff.lastname)",
            "f.kpi", 
            "f.desc", 
            "f.ratio", 
            "dictionary_type_1" => "GROUP_CONCAT(DISTINCT CONCAT(d.id,'-',d.dictionary_id,'-',d.level,'-',d.share) SEPARATOR ';')", 
            "dictionary_type_2" => "GROUP_CONCAT(DISTINCT CONCAT(d2.id,'-',d2.dictionary_id,'-',d2.level,'-',d2.share) SEPARATOR ';')",
            "science_desc" => "s.desc", 
            "science_level" => "s.level", 
            "science_share" => "s.share"
        );

        $select->from(array('p' => 'capacity'), $arrCols);
        $select->joinLeft(array('staff' => 'staff'), 'staff.id = p.staff_id', array());
        $select->joinLeft(array('t' => 'team'), 't.id = staff.title', array());
        $select->joinLeft(array('t2' => 'team'), 't2.id = t.parent_id', array());
        $select->joinLeft(array('t3' => 'team'), 't3.id = t2.parent_id', array());
        $select->joinLeft(array('f' => 'capacity_field'), 'f.capacity_id = p.id', array());
        $select->joinLeft(array('s' => 'capacity_science'), 's.capacity_field_id = f.id AND s.is_deleted = 0', array());
        $select->joinLeft(array('d' => new Zend_Db_Expr('(' . $sub_select_type_1 . ')')), 'd.field_id = f.id', array());
        $select->joinLeft(array('d2' => new Zend_Db_Expr('(' . $sub_select_type_2 . ')')), 'd2.field_id = f.id', array());
        
        $select->where('f.is_deleted = 0');
        $select->where('p.is_deleted = 0');
        $select->where('p.staff_id IS NOT NULL');
        $select->where('staff.off_date IS NULL');
        
        if(!empty($params['title'])){
            $select->where("t.id IN (?)", $params['title']);
        }

        if(!empty($params['team'])){
            $select->where("t2.id IN (?)", $params['team']);
        }

        if(!empty($params['department'])){
            $select->where("t3.id IN (?)", $params['department']);
        }

        if($params['level'] == 2){
            $select->where("p.from_staff = ?", $params['staff_id']);
        }
        
        $select->group('f.id');
        $select->order('p.id');
        
        $result = $db->fetchAll($select);
        
        return $result;
    }

    public function exportTitle($params)
    {
        require_once 'PHPExcel.php';

        $data = $this->getDataExportTitle($params);
        
        $PHPExcel = new PHPExcel();
        $heads = array(
            'TITLE ID',
            'PHÒNG BAN',
            'BỘ PHẬN',
            'CHỨC DANH',
            'TƯ DUY',
            'LEVEL',
            'TRỌNG SỐ'
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($data as $item){

            if ($item['dictionary'] && $item['level'] && $item['share']) {
                $alpha    = 'A';
    //            $sheet->setCellValue($alpha++.$index, $i++);
                $sheet->setCellValue($alpha++.$index, $item['title_id']);
                $sheet->setCellValue($alpha++.$index, $item['department_name']);
                $sheet->setCellValue($alpha++.$index, $item['team_name']);
                $sheet->setCellValue($alpha++.$index, $item['title_name']);
                $sheet->setCellValue($alpha++.$index, $item['dictionary']);
                $sheet->setCellValue($alpha++.$index, $item['level']);
                $sheet->setCellValue($alpha++.$index, $item['share']);

                $index++;
            }

        }

        $filename = 'Khung năng lực các vị trí';
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }


    public function getDataExportStaff($params)
    {
        $db = Zend_Registry::get("db");
        $select_dictionary = $db->select();
        $select_science = $db->select();

        $sub_select = "
            SELECT d.id, MAX(d.level) as level, d.field_id, MAX(d.share) as share,  d.dictionary_id, i.title from capacity c 
            JOIN capacity_field f ON c.id = f.capacity_id AND f.is_deleted = 0
            JOIN capacity_field_dictionary d ON f.id = d.field_id AND d.is_deleted = 0
            JOIN capacity_dictionary i ON d.dictionary_id = i.id AND (i.is_deleted IS NULL OR i.is_deleted = 0)
            WHERE c.is_deleted = 0 
            GROUP BY c.id, dictionary_id
        ";

        $sub_select_staff = "SELECT staff_id FROM capacity WHERE staff_id IS NOT NULL AND is_deleted = 0"; // danh sách những nhân viên được set riêng



        // select dictionary
        $arrColsDictionary = array(
            'staff_id' => 'p.to_staff',
            'staff_name' => "CONCAT(s.firstname, ' ', s.lastname)",
            'title_id' => 't.id',
            'capacity_id' => 'c.id',
            'field_id' => 'f.id',
            'title_name' => 't.name',
            'team_name' => 't2.name',
            'department_name' => 't3.name',
            'is_lock' => 'c.is_lock',
            'dictionary' => 'd.title',
            'd.level',
            'd.share'
        );

        $select_dictionary->from(array('p' => 'appraisal_office_to_do_root'), $arrColsDictionary);
        $select_dictionary->joinLeft(array('s' => 'staff'), 's.id = p.to_staff', array());
        $select_dictionary->joinLeft(array('t' => 'team'), 't.id = s.title', array());
        $select_dictionary->joinLeft(array('t2' => 'team'), 't2.id = t.parent_id', array());
        $select_dictionary->joinLeft(array('t3' => 'team'), 't3.id = t2.parent_id', array());
        $select_dictionary->join(array('c' => 'capacity'), 'IF(c.title_id IS NULL, c.staff_id = p.to_staff, c.title_id = t.id ) AND c.is_deleted = 0', array());
        $select_dictionary->joinLeft(array('h' => new Zend_Db_Expr('(' . $sub_select_staff . ')')), 'p.to_staff = h.staff_id AND c.staff_id IS NULL', array());
        $select_dictionary->join(array('f' => 'capacity_field'), 'f.capacity_id = c.id AND f.is_deleted = 0', array());
//        $select_dictionary->join(array('d' => 'capacity_field_dictionary'), 'f.id = d.field_id AND d.is_deleted = 0', array());
        $select_dictionary->join(array('d' => new Zend_Db_Expr('(' . $sub_select . ')')), 'f.id = d.field_id', array());
//        $select_dictionary->join(array('i' => 'capacity_dictionary'), 'd.dictionary_id = i.id AND (i.is_deleted IS NULL OR i.is_deleted = 0)', array());
        $select_dictionary->where('s.off_date IS NULL', NULL);
        $select_dictionary->where('p.is_del = 0');
        $select_dictionary->where('h.staff_id IS NULL');



        // select science
        $arrColsScience = array(
            'staff_id' => 'p.to_staff',
            'staff_name' => "CONCAT(s.firstname, ' ', s.lastname)",
            'title_id' => 't.id',
            'capacity_id' => 'c.id',
            'field_id' => 'f.id',
            'title_name' => 't.name',
            'team_name' => 't2.name',
            'department_name' => 't3.name',
            'is_lock' => 'c.is_lock',
            'dictionary' => 'd.desc',
            'd.level',
            'd.share'
        );


        $select_science->from(array('p' => 'appraisal_office_to_do_root'), $arrColsScience);
        $select_science->joinLeft(array('s' => 'staff'), 's.id = p.to_staff', array());
        $select_science->joinLeft(array('t' => 'team'), 't.id = s.title', array());
        $select_science->joinLeft(array('t2' => 'team'), 't2.id = t.parent_id', array());
        $select_science->joinLeft(array('t3' => 'team'), 't3.id = t2.parent_id', array());
        $select_science->joinLeft(array('c' => 'capacity'), 'IF(c.title_id IS NULL, c.staff_id = p.to_staff, c.title_id = t.id ) AND c.is_deleted = 0', array());
        $select_science->joinLeft(array('h' => new Zend_Db_Expr('(' . $sub_select_staff . ')')), 'p.to_staff = h.staff_id AND c.staff_id IS NULL', array());
        $select_science->joinLeft(array('f' => 'capacity_field'), 'f.capacity_id = c.id AND f.is_deleted = 0', array());
        $select_science->joinLeft(array('d' => 'capacity_science'), 'f.id = d.capacity_field_id AND d.is_deleted = 0', array());
        $select_science->where('s.off_date IS NULL', NULL);
        $select_science->where('p.is_del = 0');
        $select_science->where('h.staff_id IS NULL');


        if(!empty($params['title'])){
            $select_dictionary->where("t.id IN (?)", $params['title']);
            $select_science->where("t.id IN (?)", $params['title']);
        }

        if(!empty($params['team'])){
            $select_dictionary->where("t2.id IN (?)", $params['team']);
            $select_science->where("t2.id IN (?)", $params['team']);
        }

        if(!empty($params['department'])){
            $select_dictionary->where("t3.id IN (?)", $params['department']);
            $select_science->where("t3.id IN (?)", $params['department']);
        }

        if($params['level'] == 2){
            $select_dictionary->where("p.from_staff = ?", $params['staff_id']);
            $select_science->where("p.from_staff = ?", $params['staff_id']);
        }

        $select_union = $db->select()->union(array($select_dictionary, $select_science), Zend_Db_Select::SQL_UNION_ALL)->order('staff_id');
      
        $result = $db->fetchAll($select_union);

        return $result;
    }
    
    public function getCapacityByListStaff($params) {
        $db = Zend_Registry::get("db");
        $select = $db->select();
        
        $sub_select = "
            SELECT p.field_id, p.dictionary_id, p.level, p.share, p.appraisal_head_level, p.appraisal_staff_level
            FROM `staff_capacity_field_dictionary`p
            WHERE p.is_deleted = 0
            UNION
            SELECT p.capacity_field_id, 15, p.level, p.share, p.appraisal_head_level, p.appraisal_staff_level
            FROM staff_capacity_science p
            WHERE p.is_deleted = 0
        ";
        
        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            "fullname" => "CONCAT(s.firstname, ' ', s.lastname)", 
            "p.staff_id",
            "p.plan_id",
            "s.code",
            "title_name" => "t.name",
            "team_name" => "t2.name", 
            "department_name" => "t3.name", 
            "plan_title" => "c.title",
            //"point" => "(SUM(d.appraisal_head_level/d.level*d.`share`)  / SUM(d.`share`) * 100)",
            //"point_staff" => "(SUM(d.appraisal_staff_level/d.level*d.`share`)  / SUM(d.`share`) * 100)",
            "point" => "(SUM(IF(d.appraisal_head_level > d.level, d.level, d.appraisal_head_level)/d.level*d.`share`)  / SUM(d.`share`) * 100)",
            "point_staff" => "(SUM(IF(d.appraisal_staff_level > d.level, d.level, d.appraisal_staff_level)/d.level*d.`share`)  / SUM(d.`share`) * 100)",
            "staff_capacity_id" => "f.capacity_id",
            "p.is_staff_appraisal",
            "p.is_head_appraisal"
        );

        $select->from(array('p' => 'staff_capacity'), $arrCols);
        $select->joinLeft(array('s' => 'staff'), 's.id = p.staff_id', array());
        $select->joinLeft(array('root' => 'appraisal_office_to_do_root'), 'root.to_staff = s.id', array());
        $select->joinLeft(array('t' => 'team'), 't.id = s.title', array());
        $select->joinLeft(array('t2' => 'team'), 't2.id = t.parent_id', array());
        $select->joinLeft(array('t3' => 'team'), 't3.id = t2.parent_id', array());
        $select->joinLeft(array('c' => 'capacity_plan'), 'c.id = p.plan_id', array());
        $select->joinLeft(array('f' => 'staff_capacity_field'), 'f.capacity_id = p.id AND f.is_deleted = 0', array());
        $select->joinLeft(array('d' =>  new Zend_Db_Expr('(' . $sub_select . ')')), 'd.field_id = f.id', array());
        
        $select->where('p.is_deleted = 0', NULL);
        $select->where('s.off_date IS NULL', NULL);
        
        if(!empty($params['list_staff'])){
            $select->where("p.staff_id IN (?)", $params['list_staff']);
        }
        
        
        $select->group('p.id');
        $select->order(['t2.id', 't.id', 'p.id']);
        
        
        $result = $db->fetchAll($select);
        
        return $result;
    }
    
    public function getCapacity($params) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.plan_id", 
            "p.staff_id",
            "p.is_head_appraisal", 
            "p.is_staff_appraisal",
        );

        $select->from(array('p' => 'staff_capacity'), $arrCols);
        
        $select->where('p.staff_id = ?', $params['staff_id']);
        $select->where('p.plan_id = ?', $params['plan_id']);
        $select->where('p.is_staff_appraisal = 1');
        $select->where('p.is_deleted = 0');
        
        $result = $db->fetchRow($select);

        return $result;
    }

    public function exportStaff($params)
    {
        require_once 'PHPExcel.php';

        $data = $this->getDataExportStaff($params);

        $PHPExcel = new PHPExcel();
        $heads = array(
            'STAFF ID',
            'HỌ TÊN',
            'PHÒNG BAN',
            'BỘ PHẬN',
            'CHỨC DANH',
            'TƯ DUY',
            'LEVEL',
            'TRỌNG SỐ'
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($data as $item){

            if ($item['dictionary'] && $item['level'] && $item['share']) {
                $alpha    = 'A';
                //            $sheet->setCellValue($alpha++.$index, $i++);
                $sheet->setCellValue($alpha++.$index, $item['staff_id']);
                $sheet->setCellValue($alpha++.$index, $item['staff_name']);
                $sheet->setCellValue($alpha++.$index, $item['department_name']);
                $sheet->setCellValue($alpha++.$index, $item['team_name']);
                $sheet->setCellValue($alpha++.$index, $item['title_name']);
                $sheet->setCellValue($alpha++.$index, $item['dictionary']);
                $sheet->setCellValue($alpha++.$index, $item['level']);
                $sheet->setCellValue($alpha++.$index, $item['share']);

                $index++;
            }

        }

        $filename = 'Khung năng lực nhân viên';
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }
    
    
    public function exportTitleSetup($params)
    {
        require_once 'PHPExcel.php';

        $data = $this->getDataExportTitleSetup($params);
        
        $QCapacityDictionary = new Application_Model_CapacityDictionary();
        
        $get_dictionary = $QCapacityDictionary->fetchAll();
        $dictionary = [];
        foreach($get_dictionary as $key=>$value){
            $dictionary[$value['id']] = $value['title'];
        }
        
        $PHPExcel = new PHPExcel();
        $heads = array(
            'ID',
            'Phòng ban',
            'Bộ phận',
            'Chức danh',
            'KRA',
            'KPI',
            'Nhiệm vụ và hoạt động',
            'Tỷ trọng',
            'Kiến thức',
            'Kiến thức Level',
            'Kiến thức trọng số',
            'Tư duy',
            'Tư duy Level',
            'Tư duy trọng số',
            'Kỹ năng',
            'Kỹ năng Level',
            'Kỹ năng trọng số',
        );

        $PHPExcel->setActiveSheetIndex(0);
        
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;

        $i = 1;
        
        $startRow = 2;
        $start_name = 0;
        
        $start_row_id = 2;
        $previous_id = 0;
        
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            )
        );
        
        foreach($data as $item){
            $alpha    = 'A';
//            $sheet->setCellValue($alpha++.$index, $i++);
            $sheet->setCellValue($alpha++.$index, $item['id']);
            $sheet->setCellValue($alpha++.$index, $item['department_name']);
            $sheet->setCellValue($alpha++.$index, $item['team_name']);
            $sheet->setCellValue($alpha++.$index, $item['title_name']);
            $sheet->setCellValue($alpha++.$index, $item['title_field']);
            $sheet->setCellValue($alpha++.$index, $item['kpi_field']);
            $sheet->setCellValue($alpha++.$index, $item['desc_field']);
            $sheet->setCellValue($alpha++.$index, $item['ratio']);
            
            //Science
            $sheet->setCellValue($alpha++.$index, $item['science_desc']);
            $sheet->setCellValue($alpha++.$index, $item['science_level']);
            $sheet->setCellValue($alpha++.$index, $item['science_share']);
            //END Science
            
            
            //$sheet->setCellValue($alpha++.$index, $item['dictionary_type_1']);
            //$sheet->setCellValue($alpha++.$index, $item['dictionary_type_2']);
            
            $dictionary_type_1 = explode(';', $item['dictionary_type_1']);
            $dictionary_type_2 = explode(';', $item['dictionary_type_2']);
            
            ///Merge ID
            $end_row_id = $index - 1;
            if($previous_id != 0 AND $previous_id != $item['id'] AND $start_row_id != $end_row_id){
                $cellToMerge = 'A'.$start_row_id.':A'.$end_row_id;
                $sheet->mergeCells($cellToMerge);
                
                $sheet->getStyle($cellToMerge)->getAlignment()->setWrapText(true);
                $sheet->getStyle($cellToMerge)->applyFromArray($style);
                
                $cellToMerge = 'B'.$start_row_id.':B'.$end_row_id;
                $sheet->mergeCells($cellToMerge);
                
                $sheet->getStyle($cellToMerge)->getAlignment()->setWrapText(true);
                $sheet->getStyle($cellToMerge)->applyFromArray($style);
                
                $cellToMerge = 'C'.$start_row_id.':C'.$end_row_id;
                $sheet->mergeCells($cellToMerge);
                
                $sheet->getStyle($cellToMerge)->getAlignment()->setWrapText(true);
                $sheet->getStyle($cellToMerge)->applyFromArray($style);
                
                $cellToMerge = 'D'.$start_row_id.':D'.$end_row_id;
                $sheet->mergeCells($cellToMerge);
                
                $sheet->getStyle($cellToMerge)->getAlignment()->setWrapText(true);
                $sheet->getStyle($cellToMerge)->applyFromArray($style);
                
            }
            
            if($previous_id != $item['id']){
                $start_row_id = $index;
            }
            $previous_id = $item['id'];
            /// END Merge ID
            
            
            $type_main = [];
            if(count($dictionary_type_1) > count($dictionary_type_2)){
                $type_main = $dictionary_type_1;
            }
            else{
                $type_main = $dictionary_type_2;
            }
            
            $alpha_type_1 = $alpha++;
            $alpha_type_1_level = $alpha++;
            $alpha_type_1_share = $alpha++;
            
            
            $alpha_type_2 = $alpha++;
            $alpha_type_2_level = $alpha++;
            $alpha_type_2_share = $alpha++;
            
            
            foreach($type_main as $key=>$value){
                
                $dictionary_type_details = explode('-', $dictionary_type_1[$key]);
                $dictionary_name = $dictionary[$dictionary_type_details[1]];
                $dictionary_level = $dictionary_type_details[2];
                $dictionary_share = $dictionary_type_details[3];
                
                
                $dictionary_type_details_2 = explode('-', $dictionary_type_2[$key]);
                $dictionary_name_2 = $dictionary[$dictionary_type_details_2[1]];
                $dictionary_level_2 = $dictionary_type_details_2[2];
                $dictionary_share_2 = $dictionary_type_details_2[3];
                
                $sheet->setCellValue($alpha_type_1.$index, $dictionary_name);
                $sheet->setCellValue($alpha_type_1_level.$index, $dictionary_level);
                $sheet->setCellValue($alpha_type_1_share.$index, $dictionary_share);
                
                $sheet->setCellValue($alpha_type_2.$index, $dictionary_name_2);
                $sheet->setCellValue($alpha_type_2_level.$index, $dictionary_level_2);
                $sheet->setCellValue($alpha_type_2_share.$index, $dictionary_share_2);
                
                $index++;
            }
            
            
            $alpha_type_3 = $alpha++;
            $alpha_type_4 = $alpha++;
            
            $end_row = $index - 1;
            
            $cellToMerge = 'E'.$startRow.':E'.$end_row;
            $sheet->mergeCells($cellToMerge);
            
            $sheet->getStyle($cellToMerge)->getAlignment()->setWrapText(true);
            $sheet->getStyle($cellToMerge)->applyFromArray($style);
            
            $cellToMerge = 'F'.$startRow.':F'.$end_row;
            $sheet->mergeCells($cellToMerge);
            
            $sheet->getStyle($cellToMerge)->getAlignment()->setWrapText(true);
            $sheet->getStyle($cellToMerge)->applyFromArray($style);
            
            $cellToMerge = 'G'.$startRow.':G'.$end_row;
            $sheet->mergeCells($cellToMerge);
            
            $sheet->getStyle($cellToMerge)->getAlignment()->setWrapText(true);
            $sheet->getStyle($cellToMerge)->applyFromArray($style);
            
            $cellToMerge = 'H'.$startRow.':H'.$end_row;
            $sheet->mergeCells($cellToMerge);
            
            $sheet->getStyle($cellToMerge)->getAlignment()->setWrapText(true);
            $sheet->getStyle($cellToMerge)->applyFromArray($style);
            
            $startRow = $index;
            
        }
        
        $PHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $PHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
        $PHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
        $PHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $PHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(50);
        $PHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(50);
        $PHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(50);
        
        $PHPExcel->getActiveSheet()->getColumnDimensionByColumn('E')->setAutoSize(true);
        $PHPExcel->getActiveSheet()->getColumnDimensionByColumn('F')->setAutoSize(true);
        $PHPExcel->getActiveSheet()->getColumnDimensionByColumn('G')->setAutoSize(true);

        $filename = 'Khung năng lực các vị trí';
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }
    
    public function exportStaffSetup($params)
    {
        require_once 'PHPExcel.php';

        $data = $this->getDataExportStaffSetup($params);
        
        $QCapacityDictionary = new Application_Model_CapacityDictionary();
        
        $get_dictionary = $QCapacityDictionary->fetchAll();
        $dictionary = [];
        foreach($get_dictionary as $key=>$value){
            $dictionary[$value['id']] = $value['title'];
        }
        
        $PHPExcel = new PHPExcel();
        $heads = array(
            'ID',
            'Name',
            'Phòng ban',
            'Bộ phận',
            'Chức danh',
            'KRA',
            'KPI',
            'Nhiệm vụ và hoạt động',
            'Tỷ trọng',
            'Kiến thức',
            'Kiến thức Level',
            'Kiến thức trọng số',
            'Tư duy',
            'Tư duy Level',
            'Tư duy trọng số',
            'Kỹ năng',
            'Kỹ năng Level',
            'Kỹ năng trọng số',
        );

        $PHPExcel->setActiveSheetIndex(0);
        
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;

        $i = 1;
        
        $startRow = 2;
        $start_name = 0;
        
        $start_row_id = 2;
        $previous_id = 0;
        
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            )
        );
        
        foreach($data as $item){
            $alpha    = 'A';
//            $sheet->setCellValue($alpha++.$index, $i++);
            $sheet->setCellValue($alpha++.$index, $item['id']);
            $sheet->setCellValue($alpha++.$index, $item['fullname']);
            $sheet->setCellValue($alpha++.$index, $item['department_name']);
            $sheet->setCellValue($alpha++.$index, $item['team_name']);
            $sheet->setCellValue($alpha++.$index, $item['title_name']);
            $sheet->setCellValue($alpha++.$index, $item['title_field']);
            $sheet->setCellValue($alpha++.$index, $item['kpi_field']);
            $sheet->setCellValue($alpha++.$index, $item['desc_field']);
            $sheet->setCellValue($alpha++.$index, $item['ratio']);
            
            //Science
            $sheet->setCellValue($alpha++.$index, $item['science_desc']);
            $sheet->setCellValue($alpha++.$index, $item['science_level']);
            $sheet->setCellValue($alpha++.$index, $item['science_share']);
            //END Science
            
            
            //$sheet->setCellValue($alpha++.$index, $item['dictionary_type_1']);
            //$sheet->setCellValue($alpha++.$index, $item['dictionary_type_2']);
            
            $dictionary_type_1 = explode(';', $item['dictionary_type_1']);
            $dictionary_type_2 = explode(';', $item['dictionary_type_2']);
            
            ///Merge ID
            $end_row_id = $index - 1;
            if($previous_id != 0 AND $previous_id != $item['id'] AND $start_row_id != $end_row_id){
                $cellToMerge = 'A'.$start_row_id.':A'.$end_row_id;
                $sheet->mergeCells($cellToMerge);
                
                $sheet->getStyle($cellToMerge)->getAlignment()->setWrapText(true);
                $sheet->getStyle($cellToMerge)->applyFromArray($style);
                
                $cellToMerge = 'B'.$start_row_id.':B'.$end_row_id;
                $sheet->mergeCells($cellToMerge);
                
                $sheet->getStyle($cellToMerge)->getAlignment()->setWrapText(true);
                $sheet->getStyle($cellToMerge)->applyFromArray($style);
                
                $cellToMerge = 'C'.$start_row_id.':C'.$end_row_id;
                $sheet->mergeCells($cellToMerge);
                
                $sheet->getStyle($cellToMerge)->getAlignment()->setWrapText(true);
                $sheet->getStyle($cellToMerge)->applyFromArray($style);
                
                $cellToMerge = 'D'.$start_row_id.':D'.$end_row_id;
                $sheet->mergeCells($cellToMerge);
                
                $sheet->getStyle($cellToMerge)->getAlignment()->setWrapText(true);
                $sheet->getStyle($cellToMerge)->applyFromArray($style);
                
                $cellToMerge = 'E'.$start_row_id.':E'.$end_row_id;
                $sheet->mergeCells($cellToMerge);
                
                $sheet->getStyle($cellToMerge)->getAlignment()->setWrapText(true);
                $sheet->getStyle($cellToMerge)->applyFromArray($style);
                
                
            }
            
            if($previous_id != $item['id']){
                $start_row_id = $index;
            }
            $previous_id = $item['id'];
            /// END Merge ID
            
            
            $type_main = [];
            if(count($dictionary_type_1) > count($dictionary_type_2)){
                $type_main = $dictionary_type_1;
            }
            else{
                $type_main = $dictionary_type_2;
            }
            
            $alpha_type_1 = $alpha++;
            $alpha_type_1_level = $alpha++;
            $alpha_type_1_share = $alpha++;
            
            
            $alpha_type_2 = $alpha++;
            $alpha_type_2_level = $alpha++;
            $alpha_type_2_share = $alpha++;
            
            
            foreach($type_main as $key=>$value){
                
                $dictionary_type_details = explode('-', $dictionary_type_1[$key]);
                $dictionary_name = $dictionary[$dictionary_type_details[1]];
                $dictionary_level = $dictionary_type_details[2];
                $dictionary_share = $dictionary_type_details[3];
                
                
                $dictionary_type_details_2 = explode('-', $dictionary_type_2[$key]);
                $dictionary_name_2 = $dictionary[$dictionary_type_details_2[1]];
                $dictionary_level_2 = $dictionary_type_details_2[2];
                $dictionary_share_2 = $dictionary_type_details_2[3];
                
                $sheet->setCellValue($alpha_type_1.$index, $dictionary_name);
                $sheet->setCellValue($alpha_type_1_level.$index, $dictionary_level);
                $sheet->setCellValue($alpha_type_1_share.$index, $dictionary_share);
                
                $sheet->setCellValue($alpha_type_2.$index, $dictionary_name_2);
                $sheet->setCellValue($alpha_type_2_level.$index, $dictionary_level_2);
                $sheet->setCellValue($alpha_type_2_share.$index, $dictionary_share_2);
                
                $index++;
            }
            
            
            $alpha_type_3 = $alpha++;
            $alpha_type_4 = $alpha++;
            
            $end_row = $index - 1;
            
            $cellToMerge = 'F'.$startRow.':F'.$end_row;
            $sheet->mergeCells($cellToMerge);
            
            $sheet->getStyle($cellToMerge)->getAlignment()->setWrapText(true);
            $sheet->getStyle($cellToMerge)->applyFromArray($style);
            
            $cellToMerge = 'G'.$startRow.':G'.$end_row;
            $sheet->mergeCells($cellToMerge);
            
            $sheet->getStyle($cellToMerge)->getAlignment()->setWrapText(true);
            $sheet->getStyle($cellToMerge)->applyFromArray($style);
            
            $cellToMerge = 'H'.$startRow.':H'.$end_row;
            $sheet->mergeCells($cellToMerge);
            
            $sheet->getStyle($cellToMerge)->getAlignment()->setWrapText(true);
            $sheet->getStyle($cellToMerge)->applyFromArray($style);
            
            $cellToMerge = 'I'.$startRow.':I'.$end_row;
            $sheet->mergeCells($cellToMerge);
            
            $sheet->getStyle($cellToMerge)->getAlignment()->setWrapText(true);
            $sheet->getStyle($cellToMerge)->applyFromArray($style);
            
            $startRow = $index;
            
        }
        
        $PHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $PHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
        $PHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
        $PHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $PHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $PHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(50);
        $PHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(50);
        $PHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(50);
        

        $filename = 'Khung năng lực các vị trí';
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }

}