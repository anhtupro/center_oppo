<?php

class Application_Model_StaffContract extends Zend_Db_Table_Abstract {

    protected $_name = 'staff_contract';

    public function StaffContractNew($page, $limit, &$total, $params) {
        $db              = Zend_Registry::get('db');
        $expired_to      = (isset($params['expired_to']) OR $params['expired_to'] != '') ? My_Date::normal_to_mysql($params['expired_to']) : NULL;
        $signed_to       = (isset($params['signed_to']) AND $params['signed_to']) ? My_Date::normal_to_mysql($params['signed_to']) : NULL;
        $strAreaId       = ( count($params['area_id']) > 0 ) ? implode(',', $params['area_id']) : '';
        $strDepartment   = ( count($params['department']) > 0 ) ? implode(',', $params['department']) : '';
        $strTeam         = ( count($params['team']) > 0 ) ? implode(',', $params['team']) : '';
        $strTitle        = ( count($params['title']) > 0 ) ? implode(',', $params['title']) : '';
        $strContractTerm = ( count($params['contract_term']) > 0) ? implode(',', $params['contract_term']) : '';
        $strCompanyId    = ( count($params['company_id']) > 0 ) ? implode(',', $params['company_id']) : '';
        $userStorage     = Zend_Auth::getInstance()->getStorage()->read();

        $sql_params = array(
            $userStorage->id,
            $params['name'],
            $params['code'],
            $params['office'],
            $params['print_type'],
            $signed_to,
            $strAreaId,
            $strDepartment,
            $strTeam,
            $strTitle,
            $params['off'],
            $strContractTerm,
            $strCompanyId,
            $page,
            $limit
        );
        $sql        = 'CALL p_contract_new(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,@total)';
        $stmt       = $db->query($sql, $sql_params);
        $list       = $stmt->fetchAll();

        $stmt->closeCursor();
        $total = $db->fetchOne('select @total');
        return $list;
    }

    public function history($page, $limit, &$total, $params) {
        $db                 = Zend_Registry::get('db');
        $expired_from       = (isset($params['expired_from']) OR $params['expired_from'] != '') ? My_Date::normal_to_mysql($params['expired_from']) : NULL;
        $expired_to         = (isset($params['expired_to']) OR $params['expired_to'] != '') ? My_Date::normal_to_mysql($params['expired_to']) : NULL;
        $signed_from        = (isset($params['signed_from']) AND $params['signed_from']) ? My_Date::normal_to_mysql($params['signed_from']) : NULL;
        $signed_to          = (isset($params['signed_to']) AND $params['signed_to']) ? My_Date::normal_to_mysql($params['signed_to']) : NULL;
        $return_letter_from = (isset($params['return_letter_from']) AND $params['return_letter_from'] ) ? My_Date::normal_to_mysql($params['return_letter_from']) : NULL;
        $return_letter_to   = ($params['return_letter_to']) ? My_Date::normal_to_mysql($params['return_letter_to']) : NULL;
        $send_letter_from   = ($params['send_letter_from']) ? My_Date::normal_to_mysql($params['send_letter_from']) : NULL;
        $send_letter_to     = ($params['send_letter_to']) ? My_Date::normal_to_mysql($params['send_letter_to']) : NULL;
        $strAreaId          = ( count($params['area_id']) > 0 ) ? implode(',', $params['area_id']) : '';
        $strDepartment      = ( count($params['department']) > 0 ) ? implode(',', $params['department']) : '';
        $strTeam            = ( count($params['team']) > 0 ) ? implode(',', $params['team']) : '';
        $strTitle           = ( count($params['title']) > 0 ) ? implode(',', $params['title']) : '';
        $strContractTerm    = ( count($params['contract_term']) > 0) ? implode(',', $params['contract_term']) : '';
        $strCompanyId       = ( count($params['company_id']) > 0 ) ? implode(',', $params['company_id']) : '';
        $userStorage        = Zend_Auth::getInstance()->getStorage()->read();



        $sql_params = array(
            $userStorage->id,
            $params['name'],
            $params['code'],
            $params['office'],
            $params['print_type'],
            $signed_from,
            $signed_to,
            $expired_from,
            $expired_to,
            $strAreaId,
            $strDepartment,
            $strTeam,
            $strTitle,
            $params['off'],
            $strContractTerm,
            $strCompanyId,
            $page,
            $limit,
            $return_letter_from,
            $return_letter_to,
            $send_letter_from,
            $send_letter_to,
            $params['return_letter'],
            $params['is_upload_file'],
        );

        $sql   = 'CALL p_contract_history(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,@total,?,?,?,?,?,?)';
        $stmt  = $db->query($sql, $sql_params);
        $list  = $stmt->fetchAll();
        $stmt->closeCursor();
        $total = $db->fetchOne('select @total');
        return $list;
    }

    public function renewable($page, $limit, &$total, $params) {

        $db = Zend_Registry::get('db');

        $signed_to = (isset($params['signed_to']) AND $params['signed_to']) ? My_Date::normal_to_mysql($params['signed_to']) : NULL;

        $strAreaId       = ( count($params['area_id']) > 0 ) ? implode(',', $params['area_id']) : '';
        $strDepartment   = ( count($params['department']) > 0 ) ? implode(',', $params['department']) : '';
        $strTeam         = ( count($params['team']) > 0 ) ? implode(',', $params['team']) : '';
        $strTitle        = ( count($params['title']) > 0 ) ? implode(',', $params['title']) : '';
        $strContractTerm = ( count($params['contract_term']) > 0) ? implode(',', $params['contract_term']) : '';
        $strCompanyId    = ( count($params['company_id']) > 0 ) ? implode(',', $params['company_id']) : '';
        $userStorage     = Zend_Auth::getInstance()->getStorage()->read();

        $sql_params = array(
            $userStorage->id,
            $params['name'],
            $params['code'],
            $params['office'],
            $signed_to,
            $strAreaId,
            $strDepartment,
            $strTeam,
            $strTitle,
            $strContractTerm,
            $strCompanyId,
            $page,
            $limit,
        );
        $sql        = 'CALL p_contract_renewable(?,?,?,?,?,?,?,?,?,?,?,?,?,@total)';
        $stmt       = $db->query($sql, $sql_params);
        $list       = $stmt->fetchAll();

        $stmt->closeCursor();

        $total = $db->fetchOne('select @total');
        return $list;
    }

    public function renewableoff($page, $limit, &$total, $params) {

        $db = Zend_Registry::get('db');

        $signed_to   = (isset($params['signed_to']) AND $params['signed_to']) ? My_Date::normal_to_mysql($params['signed_to']) : NULL;
        $signed_from = (isset($params['signed_from']) AND $params['signed_from']) ? My_Date::normal_to_mysql($params['signed_from']) : NULL;

        $strAreaId       = ( count($params['area_id']) > 0 ) ? implode(',', $params['area_id']) : '';
        $strDepartment   = ( count($params['department']) > 0 ) ? implode(',', $params['department']) : '';
        $strTeam         = ( count($params['team']) > 0 ) ? implode(',', $params['team']) : '';
        $strTitle        = ( count($params['title']) > 0 ) ? implode(',', $params['title']) : '';
        $strContractTerm = ( count($params['contract_term']) > 0) ? implode(',', $params['contract_term']) : '';
        $strCompanyId    = ( count($params['company_id']) > 0 ) ? implode(',', $params['company_id']) : '';
        $userStorage     = Zend_Auth::getInstance()->getStorage()->read();

        $sql_params = array(
            $userStorage->id,
            $params['name'],
            $params['code'],
            $params['office'],
            $signed_to,
            $signed_from,
            $strAreaId,
            $strDepartment,
            $strTeam,
            $strTitle,
            $strContractTerm,
            $strCompanyId,
            $page,
            $limit,
        );
        $sql        = 'CALL p_contract_renewable_off(?,?,?,?,?,?,?,?,?,?,?,?,?,?,@total)';
        $stmt       = $db->query($sql, $sql_params);
        $list       = $stmt->fetchAll();

        $stmt->closeCursor();
        $total = $db->fetchOne('select @total');
        return $list;
    }

    public function renewablePrint($page, $limit, &$total, $params) {
        $db          = Zend_Registry::get('db');
        $signed_to   = (isset($params['signed_to']) AND $params['signed_to']) ? My_Date::normal_to_mysql($params['signed_to']) : NULL;
        $select      = $db->select()
                ->from(array('c' => $this->_name), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT c.id'), 'c.*', 'different_sen' => new Zend_Db_Expr('case when ss.work_cost_salary <> max_sen.max_work_cost  then 1 else 0 end'), 'different' => new Zend_Db_Expr('case when s.title <> c.title OR s.regional_market <> c.regional_market OR s.company_id <> c.company_id then 1 else 0 end')))
                ->join(array('s' => 'staff'), 's.id = c.staff_id', array('full_name' => 'concat(s.firstname," ",s.lastname)', 's.code', 'staff_status' => 's.status'))
                ->join(array('r' => 'regional_market'), 'r.id = c.regional_market', array('tinh' => 'r.name'))
                ->join(array('ar' => 'area'), 'ar.id = r.area_id', array('area' => 'ar.name'))
                ->join(array('t' => 'team'), 't.id = c.title', array('title' => 't.name'))
                ->join(array('f' => 'team'), 'f.id = t.parent_id', array('team' => 'f.name'))
                ->join(array('g' => 'team'), 'g.id = f.parent_id', array('department' => 'g.name'))
                ->join(array('cp' => 'company'), 'cp.id = c.company_id', array('company_name' => 'cp.name'))
                ->joinleft(array('h' => 'hospital'), 'h.id = c.hospital_id', array('hospital' => 'h.name'))
                ->joinleft(array('rm2' => 'regional_market'), 'rm2.id = c.district_id', array('quan_huyen' => 'rm2.name'))
                ->joinleft(array('max_sen' => 'v_max_seniority'), 'max_sen.staff_id = c.staff_id', array('max_sen_val' => 'max_sen.max_work_cost'))
                ->joinleft(array('ss' => 'staff_salary'), 'ss.id = c.salary_id', array('insurance_salary', 'work_cost_salary'))
                ->where('c.status = 2 and c.is_next = 2 and c.is_expired =0 and c.print_status=0 and c.print_type=1 and c.contract_term not in (2,3,17) ');
        if (!empty($params['area_id'])) {
            $select->where('ar.id in (?)', $params['area_id']);
        }
        if (!empty($params['code'])) {
            $select->where('s.code LIKE ? ', "%" . $params['code'] . "%");
        }
        if ($params['office'] != -1) {
            if ($params['office'] == 0) {
                $select->where('c.title IN (?)', TITLE_NON_OFFICE);
            } else {
                $select->where('c.title NOT IN (?)', TITLE_NON_OFFICE);
            }
        }
        if (!empty($params['title'])) {
            $select->where('c.title in (?)', $params['title']);
        }
        if (!empty($params['team'])) {
            $select->where('f.id in (?)', $params['team']);
        }
        if (!empty($params['department'])) {
            $select->where('g.id in (?)', $params['department']);
        }
        if (!empty($params['contract_term'])) {
            $select->where('c.contract_term in (?)', $params['contract_term']);
        }
        if ($params['off'] != -1) {
            if ($params['off'] == 0) {
                $select->where('s.status <> 0');
            } elseif ($params['off'] == 1) {
                $select->where('s.status = 0');
            }
        }
        if (!empty($params['company_id'])) {
            $select->where('c.company_id in (?)', $params['company_id']);
        }
        if (!empty($params['name'])) {
            $select->where(' CONCAT(s.firstname," ",s.lastname) COLLATE utf8_unicode_ci LIKE ? ', "%" . $params['name'] . "%");
        }
        if (!empty($signed_to)) {
            $select->where('c.from_date <= ?', $signed_to);
        }
        if (!empty($limit)) {
            $select->limitPage($page, $limit);
        }
//        echo "<pre>";
//        print_r($select->__toString());
//        die;
        $result = $db->fetchAll($select);
        $total  = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    public function appendixPrint($page, $limit, &$total, $params) {
        $db          = Zend_Registry::get('db');
        $signed_to   = (isset($params['signed_to']) AND $params['signed_to']) ? My_Date::normal_to_mysql($params['signed_to']) : NULL;
        $signed_from = (isset($params['signed_from']) AND $params['signed_from']) ? My_Date::normal_to_mysql($params['signed_from']) : NULL;
        $select      = $db->select()
                ->from(array('c' => $this->_name), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT c.id'), 'c.*', 'different_sen' => new Zend_Db_Expr('case when c.title =183 and ss.work_cost_salary <> max_sen_sale.max_work_cost  then 1  when c.title <> 183 and ss.work_cost_salary <> max_sen.max_work_cost  then 1  else 0 end'), 'different' => new Zend_Db_Expr('case when s.title <> c.title OR s.regional_market <> c.regional_market OR s.company_id <> c.company_id then 1 else 0 end')))
                ->join(array('s' => 'staff'), 's.id = c.staff_id', array('full_name' => 'concat(s.firstname," ",s.lastname)', 's.code', 'staff_status' => 's.status', 's.joined_at', 's.ID_number', 's.ID_date', 's.dob'))
                ->join(array('r' => 'regional_market'), 'r.id = c.regional_market', array('tinh' => 'r.name'))
                ->join(array('ar' => 'area'), 'ar.id = r.area_id', array('area' => 'ar.name'))
                ->join(array('t' => 'team'), 't.id = c.title', array('title' => 't.name'))
                ->join(array('f' => 'team'), 'f.id = t.parent_id', array('team' => 'f.name'))
                ->join(array('g' => 'team'), 'g.id = f.parent_id', array('department' => 'g.name'))
                ->join(array('cp' => 'company'), 'cp.id = c.company_id', array('company_name' => 'cp.name'))
                ->joinleft(array('h' => 'hospital'), 'h.id = c.hospital_id', array('hospital' => 'h.name'))
                ->join(array('ss' => 'staff_salary'), 'ss.id = c.salary_id', array('insurance_salary', 'salary_insurance_temp' => 'insurance_salary', 'work_cost_salary'))
                // lay thong tin thuong
                ->join(array('sa' => 'staff_address'), 'sa.staff_id = s.id and sa.address_type=3', array('permanent' => "CONCAT( IF(sa.address <> '', CONCAT(sa.address,', '), '') ,
                        IF(w.name <> '', CONCAT(w.name,', '), ''),IF(pd.district_center <> '', CONCAT(pd.district_center,', '), '') , pr.name )"))
                ->joinleft(array('w' => 'ward'), 'w.id = sa.ward_id', array('permanent_ward' => 'w.name'))
                ->joinleft(array('pd' => 'province_district'), 'pd.district_id = sa.district', array())
                ->joinleft(array('rma' => 'regional_market'), 'rma.id = sa.district', array())
                 ->joinleft(array('pro2' => 'province'), 'pro2.id = s.id_citizen_province', array())
                ->joinleft(array('pro' => 'province'), 'pro.id = s.id_place_province', array('ID_place' => 'IFNULL(pro.name,IFNULL(pro2.name," "))'))
                ->joinleft(array('rmb' => 'regional_market'), 'rmb.id = rma.parent', array())
                ->joinleft(array('pr' => 'province'), 'pr.id = rmb.province_id', array())
                // lay thong tin noi sinh
                ->join(array('sab' => 'staff_address'), 'sab.staff_id = s.id and sab.address_type=1', array())
                ->joinleft(array('rmaa' => 'regional_market'), 'rmaa.id = sab.district', array())
                ->joinleft(array('rmbb' => 'regional_market'), 'rmbb.id = rmaa.parent', array())
                ->joinleft(array('pra' => 'province'), 'pra.id = rmbb.province_id', array('birth_place' => 'pra.name'))
                ->joinleft(array('max_sen' => 'v_max_seniority'), 'max_sen.staff_id = c.staff_id', array('max_sen_val' => 'max_sen.max_work_cost'))
                ->joinleft(array('max_sen_sale' => 'v_max_seniority_sale'), 'max_sen_sale.staff_id = c.staff_id', array('max_sen_sale_val' => 'max_sen_sale.max_work_cost'))
                ->joinleft(array('rm2' => 'regional_market'), 'rm2.id = c.district_id', array('quan_huyen' => 'rm2.name'))
                ->where('c.status = 2 and (c.parent_id is not null AND c.parent_id <> 0) and c.is_expired =0 and c.print_status=0 and c.is_disable=0');
        if (!empty($params['area_id'])) {
            $select->where('ar.id in (?)', $params['area_id']);
        }
        if (!empty($params['code'])) {
            $select->where('s.code LIKE ? ', "%" . $params['code'] . "%");
        }
        if ($params['office'] != -1) {
            if ($params['office'] == 0) {
                $select->where('c.title IN (?)', TITLE_NON_OFFICE);
            } else {
                $select->where('c.title NOT IN (?)', TITLE_NON_OFFICE);
            }
        }
        if (!empty($params['title'])) {
            $select->where('c.title in (?)', $params['title']);
        }
        if (!empty($params['team'])) {
            $select->where('s.team in (?)', $params['team']);
        }
        if (!empty($params['department'])) {
            $select->where('s.department in (?)', $params['department']);
        }
        if (!empty($params['contract_term'])) {
            $select->where('c.contract_term in (?)', $params['contract_term']);
        }
        if (!empty($params['company_id'])) {
            $select->where('c.company_id in (?)', $params['company_id']);
        }
        if (!empty($params['name'])) {
            $select->where(' CONCAT(s.firstname," ",s.lastname) COLLATE utf8_unicode_ci LIKE ? ', "%" . $params['name'] . "%");
        }
        if (!empty($signed_to)) {
            $select->where('c.from_date <= ?', $signed_to);
        }
        if (!empty($signed_from)) {
            $select->where('c.from_date >= ?', $signed_from);
        }
        if ($params['off'] != -1) {
            if ($params['off'] == 0) {
                $select->where('s.status <> 0');
            } elseif ($params['off'] == 1) {
                $select->where('s.status = 0');
            }
        }
        if (!empty($limit)) {
            $select->limitPage($page, $limit);
        }
//        echo "<pre>";
//        print_r($select->__toString());
//        die;
        $result = $db->fetchAll($select);
        $total  = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public function confirmContract($store_params) {
        $db = Zend_Registry::get('db');
        try {
            $stmt    = $db->query('CALL p_contract_confirm(?,?,?,?,?)', $store_params);
            $arrCode = $stmt->fetchAll()[0];
//             if ($_SERVER['REMOTE_ADDR'] == '14.161.22.196') {
//                echo "<pre>";
//                print_r($arrCode);
//                die; 
//            }

            $stmt->closeCursor();

            foreach ($arrCode as $value):

                $code = intval($value);
            endforeach;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }

        return $code;
    }

    public function confirmContractOff($store_params) {
        $db = Zend_Registry::get('db');
        try {
            $stmt    = $db->query('CALL p_contract_confirm_off(?,?,?,?,?)', $store_params);
            $arrCode = $stmt->fetchAll()[0];
//             $arrCode = $stmt->fetchAll();
//             if ($_SERVER['REMOTE_ADDR'] == '14.161.22.196') {
//                echo "<pre>";
//                print_r($arrCode);
//                die; 
//            }

            $stmt->closeCursor();

            foreach ($arrCode as $value):

                $code = intval($value);
            endforeach;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }

        return $code;
    }

    public function printList($ids) {
        $db     = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('c' => $this->_name), array('c.*'))
                ->join(array('s' => 'staff'), 's.id = c.staff_id', array('staff_name' => 'concat(s.firstname," ",s.lastname)', 's.birth_place', 's.code', 's.ID_number', 's.ID_date', 'staff_status' => 's.status', 's.dob', 'c.contract_term', 's.office_id'))
                ->join(array('cp' => 'company'), 'cp.id = c.company_id', array('company_name' => 'cp.name'))
                ->join(array('r' => 'regional_market'), 'r.id = c.regional_market', array('tinh' => 'r.name'))
                ->joinleft(array('r2' => 'regional_market'), 'r2.id = c.district_id', array('district' => 'r2.name'))
                ->join(array('ar' => 'area'), 'ar.id = r.area_id', array('area' => 'ar.name'))
                ->joinLeft(array('ss' => 'staff_salary'), 'ss.id = c.salary_id', array('insurance_salary', 'work_cost_salary'))
                ->join(array('sa' => 'staff_address'), 'sa.staff_id = s.id and sa.address_type=3', array('permanent' => "CONCAT( IF(sa.address <> '', CONCAT(sa.address,', '), '') ,
                        IF(w.name <> '', CONCAT(w.name,', '), ''),IF(pd.district_center <> '', CONCAT(pd.district_center,', '), '') , pr.name )"))
                ->joinleft(array('w' => 'ward'), 'w.id = sa.ward_id', array('permanent_ward' => 'w.name'))
                ->joinleft(array('pd' => 'province_district'), 'pd.district_id = sa.district', array())
                ->join(array('ct' => 'contract_types'), 'ct.id = c.contract_term', array('contract_term_name' => 'ct.paper_name'))
                ->joinleft(array('rma' => 'regional_market'), 'rma.id = sa.district', array())
                ->joinleft(array('pro2' => 'province'), 'pro2.id = s.id_citizen_province', array())
                ->joinleft(array('pro' => 'province'), 'pro.id = s.id_place_province', array('ID_place' => 'IFNULL(pro.name,IFNULL(pro2.name," "))'))
                ->joinleft(array('rmb' => 'regional_market'), 'rmb.id = rma.parent', array())
                ->joinleft(array('pr' => 'province'), 'pr.id = rmb.province_id', array())
                ->join(array('sab' => 'staff_address'), 'sab.staff_id = s.id and sab.address_type=1', array())
                ->joinleft(array('rmaa' => 'regional_market'), 'rmaa.id = sab.district', array())
                ->joinleft(array('rmbb' => 'regional_market'), 'rmbb.id = rmaa.parent', array())
                ->joinleft(array('pra' => 'province'), 'pra.id = rmbb.province_id', array('birth_place' => 'pra.name'))
                ->joinleft(array('sp' => 'salary_policy_detail'), 'c.salary_policy_detail_id = sp.id', array('basic_tv' => 'sp.basic_salary1', 'contract_allowance' => 'sp.allowance', 'contract_kpi' => 'sp.salary_kpi', 'contract_total_salary' => 'sp.total_company_salary'))
                ->where('c.id IN (?)', $ids)
                ->order('ar.name')
        ;
        $result = $db->fetchAll($select);

        return $result;
    }

    public function printListAppendix($ids) {
        $db     = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('c' => $this->_name), array('c.*'))
                ->join(array('s' => 'staff'), 's.id = c.staff_id', array('staff_name' => 'concat(s.firstname," ",s.lastname)', 's.birth_place', 's.code', 's.ID_number', 's.ID_date', 'staff_status' => 's.status', 's.dob', 'c.contract_term', 's.office_id'))
                ->join(array('cp' => 'company'), 'cp.id = c.company_id', array('company_name' => 'cp.name'))
                ->join(array('r' => 'regional_market'), 'r.id = c.regional_market', array('tinh' => 'r.name'))
                ->joinleft(array('r2' => 'regional_market'), 'r2.id = c.district_id', array('district' => 'r2.name'))
                ->join(array('ar' => 'area'), 'ar.id = r.area_id', array('area' => 'ar.name'))
                ->joinLeft(array('ss' => 'staff_salary'), 'ss.id = c.salary_id', array('insurance_salary', 'work_cost_salary'))
                ->join(array('sa' => 'staff_address'), 'sa.staff_id = s.id and sa.address_type=3', array('permanent' => "CONCAT( IF(sa.address <> '', CONCAT(sa.address,', '), '') ,
                        IF(w.name <> '', CONCAT(w.name,', '), ''),IF(pd.district_center <> '', CONCAT(pd.district_center,', '), '') , pr.name )"))
                ->joinleft(array('w' => 'ward'), 'w.id = sa.ward_id', array('permanent_ward' => 'w.name'))
                ->joinleft(array('pd' => 'province_district'), 'pd.district_id = sa.district', array())
                ->join(array('c2' => 'staff_contract'), 'c2.id = c.parent_id', array('parent_from_date' => 'c2.from_date'))
                ->join(array('ct' => 'contract_types'), 'ct.id = c.contract_term', array('contract_term_name' => 'ct.name'))
                ->joinleft(array('rma' => 'regional_market'), 'rma.id = sa.district', array())
                ->joinleft(array('pro2' => 'province'), 'pro2.id = s.id_citizen_province', array())
                ->joinleft(array('pro' => 'province'), 'pro.id = s.id_place_province', array('ID_place' => 'IFNULL(pro.name,IFNULL(pro2.name," "))'))
                ->joinleft(array('rmb' => 'regional_market'), 'rmb.id = rma.parent', array())
                ->joinleft(array('pr' => 'province'), 'pr.id = rmb.province_id', array())
                ->join(array('sab' => 'staff_address'), 'sab.staff_id = s.id and sab.address_type=1', array())
                ->joinleft(array('rmaa' => 'regional_market'), 'rmaa.id = sab.district', array())
                ->joinleft(array('rmbb' => 'regional_market'), 'rmbb.id = rmaa.parent', array())
                ->joinleft(array('pra' => 'province'), 'pra.id = rmbb.province_id', array('birth_place' => 'pra.name'))
                ->join(array('c3' => $this->_name), 'c3.id = c.parent_id', array('current_contract_term' => 'c3.contract_term'))
                ->joinleft(array('sp' => 'salary_policy_detail'), 'c.salary_policy_detail_id = sp.id', array('basic_tv' => 'sp.basic_salary1', 'contract_allowance' => 'sp.allowance', 'contract_kpi' => 'sp.salary_kpi', 'contract_total_salary' => 'sp.total_company_salary'))
//                ->joinleft(array('sp' => 'salary_policy_detail'), 'sp.title_id <> 182 and sp.title_id =c.title and sp.province_id=c.regional_market AND sp.from_date <=c.from_date AND ( sp.to_date >= c.to_date OR sp.to_date IS NULL) ', array('basic_tv' => new Zend_Db_Expr('CASE when c2.contract_term = 2 then sp.basic_salary1 else  sp.basic_company_salary END'),'contract_allowance'=>'sp.allowance','contract_kpi'=>'sp.salary_kpi','contract_total_salary'=>'sp.total_company_salary'))
                ->where('c.id IN (?)', $ids)
                ->order('ar.name');
        $result = $db->fetchAll($select);

        return $result;
    }

    public function getDetailForEdit($id) {
        $db     = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('c' => $this->_name), array('c.*'))
                ->where('c.is_next=1 and c.is_expired=0 and c.id = ?', $id);
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getDetailAppendixForEdit($id) {
        $db     = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('c' => $this->_name), array('c.*'))
                ->join(array('s' => 'staff_salary'), 'c.salary_id=s.id ', array('s.insurance_salary', 's.work_cost_salary'))
                ->where('c.parent_id is not null and c.print_type=2 and c.is_expired=0 and c.id = ?', $id);
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getDetailForNew($id) {
        $db     = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('c' => $this->_name), array('c.*'))
                ->join(array('ss' => 'staff'), 'ss.id = c.staff_id', array('staff_name' => 'concat(ss.firstname," ",ss.lastname)', 'ss.code', 'c.regional_market', 'staff_id' => 'ss.id'))
                ->joinLeft(array('s' => 'staff_salary'), 's.id = c.salary_id', array('s.work_cost_salary', 's.insurance_salary'))
                ->joinLeft(array('r' => 'regional_market'), 'r.id = c.regional_market', array('r.province_id'))
                ->where('c.is_next=2 and c.is_expired=0 and c.id = ?', $id);
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getParentContract($staff_id) {
        $db   = Zend_Registry::get('db');
        $sql  = 'SELECT ac.* , t.`name`,ct.name as contract_name
      FROM staff_contract AS ac 
      INNER JOIN team AS t  ON ac.title         = t.id
      INNER JOIN contract_types AS ct on ct.id= ac.contract_term
      WHERE ac.print_type = 1 and is_next <>1 and is_expired=0
      AND ac.staff_id     = ' . $staff_id;
        $stmt = $db->query($sql);
        $list = $stmt->fetchAll();
        $stmt->closeCursor();
        return $list;
    }

    public function getSalaryDetailForPGByTransfer($transfer_id) {
        $db  = Zend_Registry::get('db');
        $sql = "SELECT f.*,sp.* FROM (
        SELECT t.* ,CASE WHEN t.`salary_policy_id` =3 AND t.is_tgdd <> 0 AND (t.is_not_tgdd=0) THEN 4 
        WHEN t.`salary_policy_id` =5 AND t.is_tgdd <> 0 AND (t.is_not_tgdd=0) THEN 6 
        ELSE t.`salary_policy_id`  END AS salary_id_filter_after
        FROM (
        SELECT 182 as title,v.regional_market,s.district,
        case when v.title in (419,420) and v.`regional_market`in(3439,4186,4187,4188,4192,3433,4189,4190) then 1
        when v.title in (419,420) then 3 
        else sr.`salary_policy_id` END as salary_policy_id,hr.fn_check_tgdd_pg(v.`staff_id`) AS 'is_tgdd' ,hr.fn_check_not_tgdd_pg(v.`staff_id`) AS 'is_not_tgdd'  FROM v_staff_transfer_fix v 
        JOIN salary_policy_mapping_regional_market sr ON sr.`regional_market`= v.`regional_market` and sr.to_date is null
        join staff s on s.id=v.staff_id
        WHERE v.transfer_id = :transfer_id) AS t
        ) f ";

        $sql .= "JOIN  salary_policy_detail sp ON sp.`title_id`= f.title AND sp.`salary_policy_id` = f.salary_id_filter_after AND sp.`province_id`=f.regional_market AND sp.to_date is null";
//        $sql  .= "JOIN  salary_policy_detail sp ON sp.`title_id`= 182 AND sp.`salary_policy_id` = 7 AND sp.`district_id`=f.district AND sp.to_date is null"; // default chinh sach cty 2018-04-01
        $stmt = $db->prepare($sql);
        $stmt->bindParam('transfer_id', $transfer_id, PDO::PARAM_INT);
        $stmt->execute();

        $data = $stmt->fetch();
        $stmt->closeCursor();
        return $data;
    }

    public function getSalaryDetailForPGByNew($staff_id) {
        $db  = Zend_Registry::get('db');
        $sql = "SELECT f.*,sp.* FROM (
        SELECT t.* ,CASE WHEN t.`salary_policy_id` =3 AND t.is_tgdd <> 0 AND (t.is_not_tgdd=0) THEN 4 
        WHEN t.`salary_policy_id` =5 AND t.is_tgdd <> 0 AND (t.is_not_tgdd=0) THEN 6 
        ELSE t.`salary_policy_id`  END AS salary_id_filter_after
        FROM (
        SELECT 182 as title,v.regional_market,v.district,
        case when v.title in (419,420) and v.`regional_market`in(3439,4186,4187,4188,4192,3433,4189,4190) then 1
        when v.title in (419,420) then 3
        else sr.`salary_policy_id` END as salary_policy_id,hr.fn_check_tgdd_pg(v.`id`) AS 'is_tgdd' ,hr.fn_check_not_tgdd_pg(v.`id`) AS 'is_not_tgdd'  FROM staff v 
        JOIN salary_policy_mapping_regional_market sr ON sr.`regional_market`= v.`regional_market` and sr.to_date is null
        WHERE v.id = :id) AS t
        ) f ";

        $sql .= "JOIN  salary_policy_detail sp ON sp.`title_id`= f.title AND sp.`salary_policy_id` = f.salary_id_filter_after AND sp.`province_id`=f.regional_market AND sp.to_date is null";
//        $sql  .= "JOIN  salary_policy_detail sp ON sp.`title_id`= 182 AND sp.`salary_policy_id` = 7 AND sp.`district_id`= case when f.district is null then 4246 else f.district end  AND sp.to_date is null"; // default chinh sach cty 2018-04-01
        $stmt = $db->prepare($sql);
        $stmt->bindParam('id', $staff_id, PDO::PARAM_INT);
        $stmt->execute();

        $data = $stmt->fetch();
        $stmt->closeCursor();
        return $data;
    }

    public function getSalaryDetailByTitleDistrict($title, $district) {


        $db   = Zend_Registry::get('db');
        $sql  = "SELECT * from salary_policy_detail sp where sp.title_id= :title and sp.to_date is null and sp.district_id = :district ";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('title', $title, PDO::PARAM_INT);
        $stmt->bindParam('district', $district, PDO::PARAM_INT);
        $stmt->execute();

        $data = $stmt->fetch();
        $stmt->closeCursor();
        return $data;
    }

    public function UpdateNextContractByStaff($staff_id) {
        $db = Zend_Registry::get('db');
        try {
            $stmt = $db->query('CALL p_contract_update_next(?)', $staff_id);
            //$arrCode = $stmt->fetchAll()[0];
            $stmt->closeCursor();
        } catch (Exception $e) {

            return FALSE;
        }
        return true;
    }

    public function UpdateCurrentContractProbationByStaff($staff_id) {
        $db = Zend_Registry::get('db');
        try {

            $stmt = $db->query('CALL p_contract_update_probation_not_print(?)', $staff_id);

            $stmt->closeCursor();
        } catch (Exception $e) {

            return FALSE;
        }
        return true;
    }

    public function UpdateContractById($contract_id) {
        $db = Zend_Registry::get('db');
        try {
            $stmt = $db->query('CALL p_contract_update_list_print_renewable(?)', $contract_id);
//              $arrCode =
//               $stmt->fetchAll();

            $stmt->closeCursor();
        } catch (Exception $e) {

            return FALSE;
        }
        return true;
    }

    public function UpdateContractProbationById($contract_id) {
        $db = Zend_Registry::get('db');
        try {
            $stmt = $db->query('CALL p_contract_update_list_print_probation(?)', $contract_id);
//              $result = $stmt->fetchAll();
            $stmt->closeCursor();
        } catch (Exception $e) {
            return FALSE;
        }
        return true;
    }

    public function UpdateNextContractByListStaff($list_staff) {
        $db = Zend_Registry::get('db');
        try {

            $stmt = $db->query('CALL p_contract_update_list_next(?)', $list_staff);
            //$arrCode = $stmt->fetchAll()[0];
            $stmt->closeCursor();
        } catch (Exception $e) {

            return FALSE;
        }
        return true;
    }

    public function UpdateNextContractAll() {
        $db = Zend_Registry::get('db');
        try {

            $stmt = $db->query('CALL p_contract_update_list_next_all()');
            $stmt->closeCursor();
        } catch (Exception $e) {

            return FALSE;
        }
        return true;
    }

    public function createNextContractByStaff($staff_id) {
        $db   = Zend_Registry::get('db');
        $sql  = "INSERT INTO staff_contract (staff_id,contract_term,from_date,`to_date`,hospital_id,title,regional_market,district_id,company_id,is_next,salary_id,print_type,salary_insurance_temp,note,system_note,old_contract_id,from_date_payment)
SELECT c.staff_id,tn.`id` AS next_contract,
case when c.contract_term in(3,4) then DATE_ADD(c.`to_date`,INTERVAL (tn.`start_date`+4) DAY) else DATE_ADD(c.`to_date`,INTERVAL (tn.`start_date`) DAY) END AS next_start,
CASE WHEN c.contract_term IN(3,4) THEN DATE_ADD( DATE_ADD(c.`to_date`,INTERVAL (tn.`start_date`+4) DAY),INTERVAL tn.`term` DAY) ELSE DATE_ADD( DATE_ADD(c.`to_date`,INTERVAL tn.`start_date` DAY),INTERVAL tn.`term` DAY) END AS next_end, 
c.`hospital_id`,ss.`title`,ss.`regional_market`,ss.district, ss.`company_id`,1 AS is_next,c.`salary_id`, 1 AS print_type
,s.`insurance_salary` AS temp_salary,'system auto generate on function' AS note,'system auto generate on function' AS system_note,c.id as old_contract_id,DATE_ADD(c.`to_date`,INTERVAL(tn.`start_date`) DAY) as from_date_payment
FROM staff_contract c  
JOIN staff ss ON c.staff_id= ss.id
JOIN  contract_types t ON c.`contract_term`=t.id 
JOIN contract_types tn ON t.`next_contract`=tn.id
LEFT JOIN staff_salary s ON s.id= c.`salary_id`
WHERE c.is_next=2 AND c.is_expired=0 AND c.staff_id=:staff_id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
        $stmt->execute();
        $stmt->closeCursor();
        return true;
    }
    
    public function createNextContractByStaffLocal($staff_id,$contract_term) {
        $db   = Zend_Registry::get('db');
        $sql  = "INSERT INTO staff_contract (staff_id,contract_term,from_date,`to_date`,hospital_id,title,regional_market,district_id,company_id,is_next,salary_id,print_type,salary_insurance_temp,note,system_note,old_contract_id,from_date_payment)
                SELECT c.staff_id,tn.`id` AS next_contract,
                case when c.contract_term in(3,4) then DATE_ADD(c.`to_date`,INTERVAL (tn.`start_date`+4) DAY) else DATE_ADD(c.`to_date`,INTERVAL (tn.`start_date`) DAY) END AS next_start,
                CASE WHEN c.contract_term IN(3,4) THEN DATE_ADD( DATE_ADD(c.`to_date`,INTERVAL (tn.`start_date`+4) DAY),INTERVAL tn.`term` DAY) ELSE DATE_ADD( DATE_ADD(c.`to_date`,INTERVAL tn.`start_date` DAY),INTERVAL tn.`term` DAY) END AS next_end, 
                c.`hospital_id`,ss.`title`,ss.`regional_market`,ss.district, ss.`company_id`,1 AS is_next,c.`salary_id`, 1 AS print_type
                ,s.`insurance_salary` AS temp_salary,'system auto generate on function' AS note,'system auto generate on function' AS system_note,c.id as old_contract_id,DATE_ADD(c.`to_date`,INTERVAL(tn.`start_date`) DAY) as from_date_payment
                FROM staff_contract c  
                JOIN staff ss ON c.staff_id= ss.id
                JOIN  contract_types t ON t.id = :contract_term
                JOIN contract_types tn ON t.`next_contract`=tn.id
                LEFT JOIN staff_salary s ON s.id= c.`salary_id`
                WHERE c.is_next=2 AND c.is_expired=0 AND c.staff_id=:staff_id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('contract_term', $contract_term, PDO::PARAM_INT);
        $stmt->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
        $stmt->execute();
        $stmt->closeCursor();
        return true;
    }

    public function checkTransferToContract($transfer_id) {
        $db   = Zend_Registry::get('db');
        $sql  = "select * from staff_contract c where  c.is_next <> 1  AND c.is_expired=0 AND (c.is_disable=0 OR (c.is_disable= 1 and c.district_confirm = 0 and c.district_id=4246)) AND c.transfer_id=:transfer_id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('transfer_id', $transfer_id, PDO::PARAM_INT);
        $stmt->execute();
        $data = $stmt->fetch();
        $stmt->closeCursor();
        return $data;
    }

    public function getMaxSeniority($staff_id) {
        $db   = Zend_Registry::get('db');
        $sql  = "select * from v_max_seniority v where v.staff_id=:staff_id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
        $stmt->execute();
        $data = $stmt->fetch();
        $stmt->closeCursor();
        return $data;
    }

    public function getCurrentContract($staff_id) {
        $db   = Zend_Registry::get('db');
        $stmt = $db->prepare('SELECT * FROM staff_contract WHERE staff_id = :staff_id AND print_type = 1 and is_disable = 0 AND is_expired = 0 AND is_next = 2 ORDER BY from_date DESC LIMIT 1');
        $stmt->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
        $stmt->execute();
        $res  = $stmt->fetch();
        $stmt->closeCursor();
        $db   = $stmt = null;
        return $res;
    }
    
    public function getAppendixByProbation($id_probation) {// lấy phụ lục trường hợp điều chuyển sinh ra phụ lục + thử việc 2 tháng
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare('CALL hr.p_check_contract_probation_appendix (:id_probation)');
        $stmt->bindParam('id_probation', $id_probation, PDO::PARAM_INT);
        $stmt->execute();
        $data = $stmt->fetch();
        $stmt->closeCursor();
        return $data;
    }
}
