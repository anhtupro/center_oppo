<?php
class Application_Model_Air extends Zend_Db_Table_Abstract
{
	protected $_name = 'air';
    protected $_schema = DATABASE_TRADE;

    public function GetAll(){
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare('
            SELECT id, name FROM '. $this->_name
        );
        $stmt->execute();
        $data = $stmt->fetchAll();
        $db = $stmt = null;
        return $data;
    }
    
    public function fetchPagination($page, $limit, &$total, $params){
        
        $db = Zend_Registry::get("db");
        $select = $db->select();
    //DISTINCT
        $arrCols = array(
            'id' => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            'store_name' => "s.name",
            'created_at' => "p.created_at",
            'created_by' => "p.created_by",
            'status'    => "p.status",
            'reject'    => "p.reject"
        );

        $select->from(array('p' => DATABASE_TRADE.'.air'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 's.regional_market = r.id', array());
        

        if(!empty($params['staff_id'])){
            $select->where('p.created_by = ?', $params['staff_id']);
        }
      
        if(!empty($params['area_id']) && empty($params['area_id_search'])){

            $select->where('r.area_id IN (?)', $params['area_id']);
        }
         //---T SEARCH
        if(!empty($params['area_id_search'])){
            $select->where('r.area_id = ?', $params['area_id_search']);
        }
        
        if(!empty($params['status_id'])){
            $select->where('p.status = ?', $params['status_id']);
        }
         if(!empty($params['name_search'])){
             $select->where('s.name LIKE ?', '%'.$params['name_search'].'%');
         }

        if(!empty($params['from_date'])){
            $from_date = str_replace('/', '-', $params['from_date']);
            $from_date = date('Y-m-d', strtotime($from_date));

            $select->where('DATE(p.created_at) >= ?', $from_date);
        }

        if(!empty($params['to_date'])){
            $to_date = str_replace('/', '-', $params['to_date']);
            $to_date = date('Y-m-d', strtotime($to_date));
            $select->where('DATE(p.created_at) <= ?', $to_date);
        }
        //---T END SEARCH
        $select->limitPage($page, $limit);

        $select->order('p.id DESC');
        
        $result = $db->fetchAll($select);
         
        $total = $db->fetchOne("select FOUND_ROWS()");
        
        return $result;
    }
    
    public function GetList($id){
    
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare('
            SELECT R.id id,C.id category_id,C.name categoryname,R.quotation_id,S.id store_id,S.name storename,R.quantity quantity,R.imei imei,R.status status, R.note note,R.staff_id staff_id ,R.type type,R.contructors_id contructors, Q.price price , R.create_at ngaytao, R.update_at ngaycapnhat, AP.name status_name  , AST.title  staff_title  FROM '.DATABASE_TRADE.'.'.$this->_name 
            .' R left join '.DATABASE_TRADE.'.category C on C.id = R.category_id'
            . ' left join '.DATABASE_CENTER.'.store S on S.id =R.store_id '
            . ' left join '.DATABASE_TRADE.'.quotation Q on Q.id =R.quotation_id '
            .' left join '.DATABASE_TRADE.'.app_status AP on R.status = AP.id'
            .' left join '.DATABASE_TRADE.'.app_status_title AST on AST.status_id=AP.id'
            .' where R.id = '.$id
        );

        $stmt->execute();
        $data = $stmt->fetchAll();
        $db = $stmt = null;
        return $data;
    }
    
    public function getAir($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.store_id", 
            "p.created_at", 
            "p.created_by", 
            "p.status", 
            "store_name" => "s.name", 
            "fullname" => "CONCAT(staff.firstname, ' ', staff.lastname)",
            "p.reject",
        );

        $select->from(array('p' => DATABASE_TRADE.'.air'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('staff' => 'staff'), 'staff.id = p.created_by', array());
        $select->where('p.id = ?', $params['id']);

        $result = $db->fetchRow($select);

        return $result;
    }
}