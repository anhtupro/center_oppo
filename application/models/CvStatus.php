<?php
class Application_Model_CvStatus extends Zend_Db_Table_Abstract
{
    protected $_name = 'cv_status';
   
    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if ($result === false) {

            $data = $this->fetchAll(null, 'title');

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->title;
                }
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }

}