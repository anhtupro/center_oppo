<?php
class Application_Model_TransferDetails extends Zend_Db_Table_Abstract
{
	protected $_name = 'transfer_details';
    protected $_schema = DATABASE_TRADE;
    
    public function getTransferDetails($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.transfer_id", 
            "p.category_id", 
            "p.quantity", 
            "reason_title" => "r.title", 
            "category_name" => "c.name", 
            "p.imei_sn",
            "file_from"  => "GROUP_CONCAT(DISTINCT file_from.url)",
            "file_to"  => "GROUP_CONCAT(DISTINCT file_to.url)",
            "file_contructor"  => "GROUP_CONCAT(DISTINCT file_contructor.url)",
            "file_accept"  => "GROUP_CONCAT(DISTINCT file_accept.url)",
            "file_document"  => "GROUP_CONCAT(DISTINCT file_document.url)",
            "p.note",
            "p.transfer_type",
            "contructors_name"  => "con.name",
            "p.price",
            "p.price_final",
            "p.transfer_date"
        );

        $select->from(array('p' => DATABASE_TRADE.'.transfer_details'), $arrCols);
        $select->joinLeft(array('c' => DATABASE_TRADE.'.category'), 'c.id = p.category_id', array());
        $select->joinLeft(array('r' => DATABASE_TRADE.'.transfer_reason'), 'r.id = p.transfer_reason_id', array());
        $select->joinLeft(array('con' => DATABASE_TRADE.'.contructors'), 'con.id = p.contructors_id', array());
        $select->joinLeft(array('file_from' => DATABASE_TRADE.'.transfer_file'), 'file_from.transfer_details_id = p.id AND file_from.type = 1', array());
        $select->joinLeft(array('file_to' => DATABASE_TRADE.'.transfer_file'), 'file_to.transfer_details_id = p.id AND file_to.type = 2', array());
        $select->joinLeft(array('file_contructor' => DATABASE_TRADE.'.transfer_file'), 'file_contructor.transfer_details_id = p.id AND file_contructor.type = 3', array());
        $select->joinLeft(array('file_accept' => DATABASE_TRADE.'.transfer_file'), 'file_accept.transfer_details_id = p.id AND file_accept.type = 4', array());
        $select->joinLeft(array('file_document' => DATABASE_TRADE.'.transfer_file'), 'file_document.transfer_details_id = p.id AND file_document.type = 5', array());
        
        $select->where('p.transfer_id = ?', $params['id']);
        $select->group('p.id');
        
        $result = $db->fetchAll($select);
        
        return $result;
    }
    
     public function getContract($params){
         $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "store"    => "s.name",
            "category"      => "c.name",
            "p.id", 
            "p.transfer_id", 
            "p.category_id", 
            "final_price"      => "p.price_final",
            "p.quantity",
            "p.contract_id"
        );

        $select->from(array('p' => DATABASE_TRADE.'.transfer_details'), $arrCols);
         $select->joinLeft(array('a' => DATABASE_TRADE.'.transfer'), 'a.id = p.transfer_id', array());
        $select->joinLeft(array('c' => DATABASE_TRADE.'.category'), 'c.id = p.category_id', array());
         $select->joinLeft(array('s' => 'store'), 's.id = a.store_from', array());
        
        $select->where('p.contructors_id = ?', $params['contractor_id']);
        $select->where('a.id IS NOT NULL');
        $result = $db->fetchAll($select);
        return $result;
    }
    
    
    
}