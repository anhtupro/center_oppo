<?php
class Application_Model_AppraisalOfficeCrossReview extends Zend_Db_Table_Abstract
{
    protected $_name = 'appraisal_office_cross_review';
    protected $_primary = 'aocr_id';

    public function deleteByTaskDeleted()
    {
        $userId = Zend_Auth::getInstance()->getStorage()->read()->id;
        $query = sprintf(
                    "update appraisal_office_task t
                          join appraisal_office_cross_review cr on t.aot_id = cr.aot_id
                          set aocr_is_deleted = 1, aocr_updated_at = '%s', aocr_updated_by = %s 
                          where aot_is_deleted = 1",
                    date('Y-m-d H:i:s'),
                    $userId
                );
        $stmt = $this->getAdapter()->query($query);
        $stmt->execute();
        $stmt->closeCursor();
    }
}