<?php

class Application_Model_RequestOfficeBudget extends Zend_Db_Table_Abstract
{
    protected $_name = 'request_office_budget';
    
    function listDepartmentBudget($params) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.department_id", 
            "p.quater", 
            "total_cost" => "SUM(p.cost)",
        );

        $select->from(array('p' => 'request_office_budget'), $arrCols);

        $select->where('p.`year` = ?', $params['year']);
        
        $select->group(['p.department_id']);
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
}