<?php

class Application_Model_InstallmentPackage extends Zend_Db_Table_Abstract
{
    protected $_name = 'installment_package';

    public function getDefaultPackage($channel_id, $good_price_log_id, $pay_term_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['p' => DATABASE_CENTER .'.installment_package'], [
                'p.pay_term_id',
                'p.prepay_rate_id'
            ])
            ->where('p.channel_id = ?', $channel_id)
            ->where('p.good_price_log_id = ?', $good_price_log_id)
            ->where('p.from_date <= CURDATE()')
            ->where('p.to_date IS NULL OR p.to_date >= CURDATE()')
            ->order('p.pay_term_id ASC')
            ->limit(1);

        if ($pay_term_id) {
            $select->where('p.pay_term_id = ?', $pay_term_id);
        }

        $result = $db->fetchRow($select);

        return $result;
    }


    public function getData($channel_id, $good_price_log_id, $pay_term_id, $prepay_rate_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['p' => DATABASE_CENTER .'.installment_package'], [
                'p.*',
                'l.price',
                'prepay_rate' => 'pr.rate',
                'pay_term' => 'pt.term',
                'f.collection_fee',
                'insurance_rate' => 'IFNULL(su.insurance_rate, 0)',
                'i.interest',
                'insurance_money' => new Zend_Db_Expr("(l.price - l.price * pr.rate) * IFNULL(su.insurance_rate, 0)"),
                'prepay_money' => new Zend_Db_Expr('l.price * pr.rate'),
                'pay_each_month' => new Zend_Db_Expr("(l.price - l.price * pr.rate) * i.interest + (l.price - l.price * pr.rate) / pt.term"),
                'total_pay_each_month' => new Zend_Db_Expr("(l.price - l.price * pr.rate) * i.interest 
                                                                    + (l.price - l.price * pr.rate) / pt.term 
                                                                    + f.collection_fee 
                                                                    + (l.price - l.price * pr.rate) * IFNULL(su.insurance_rate, 0)
                                                                    "),
                'total_pay' => new Zend_Db_Expr("(
                (
                    l.price - l.price * pr.rate) * i.interest 
                    + (l.price - l.price * pr.rate) / pt.term 
                    + f.collection_fee 
                    + (l.price - l.price * pr.rate) * IFNULL(su.insurance_rate, 0)
                ) * pt.term 
                + l.price * pr.rate"),

            ])
            ->joinLeft(['l' => WAREHOUSE_DB . '.good_price_log'], 'p.good_price_log_id = l.id', [])
            ->joinLeft(['pr' => DATABASE_CENTER . '.installment_prepay_rate'], 'p.prepay_rate_id = pr.id', [])
            ->joinLeft(['pt' => DATABASE_CENTER . '.installment_pay_term'], 'p.pay_term_id = pt.id', [])
            ->joinLeft(['f' => DATABASE_CENTER . '.installment_financier'], 'f.id = p.financier_id', [])
            ->joinLeft(['i' => DATABASE_CENTER . '.installment_interest'], 'i.id = p.interest_id', [])
            ->joinLeft(['su' => DATABASE_CENTER . '.installment_insurance'], 'p.channel_id = su.channel_id AND p.financier_id = su.financier_id', [])
            ->where('p.channel_id = ?', $channel_id)
            ->where('p.good_price_log_id = ?', $good_price_log_id)
            ->where('p.pay_term_id = ?', $pay_term_id)
            ->where('p.prepay_rate_id = ?', $prepay_rate_id)
            ->where('p.from_date <= CURDATE()')
            ->where('p.to_date IS NULL OR p.to_date >= CURDATE()');
//echo $select;
        $result = $db->fetchAll($select);

        $list = [];
        foreach ($result as $item) {
            $list [$item['financier_id']] = $item;
        }

        return $list;

    }

    public function getPackage($channel_id, $good_price_log_id, $pay_term_id, $prepay_rate_id)
    {
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare("CALL `sp_view_package_installment`(:p_channel_id, :p_good_price_log_id, :p_pay_term_id, :p_prepay_rate_id)");
        $stmt->bindParam('p_channel_id', $channel_id);
        $stmt->bindParam('p_good_price_log_id', $good_price_log_id);
        $stmt->bindParam('p_pay_term_id', $pay_term_id);
        $stmt->bindParam('p_prepay_rate_id', $prepay_rate_id);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();

        $list = [];
        foreach ($data as $item) {
            $list [$item['financier_id']] = $item;
        }

        return $list;
    }
    public function getAllByChannel($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['p' => DATABASE_CENTER .'.installment_package'], [
                'channel_name' => 'ch.name',
                 'package_id' => 'p.id',
                'p.channel_id',
                'p.financier_id',
                'financier_name' => 'f.name',
                'p.good_price_log_id',
                'good_price_log_name' => "IF(c.name IS NOT NULL
                                             ,CONCAT(g.desc, ' - ', c.name)
                                             ,g.desc 
                                          )",
                'good_price_log_price' => 'l.price',
                'p.prepay_rate_id',
                'prepay_rate_name' => 'pr.name',
                'p.pay_term_id',
                'pay_term_name' => 'pt.name',
                'p.interest_id',
                'interest_name' => 'i.name',
                'p.from_date',
                'p.to_date'

            ])
            ->joinLeft(['l' => WAREHOUSE_DB . '.good_price_log'], 'p.good_price_log_id = l.id', [])
            ->joinLeft(['g' => WAREHOUSE_DB . '.good'], 'l.good_id = g.id', [])
            ->joinLeft(['c' => WAREHOUSE_DB . '.good_color'], 'l.color_id = c.id', [])
            ->joinLeft(['pr' => DATABASE_CENTER . '.installment_prepay_rate'], 'p.prepay_rate_id = pr.id', [])
            ->joinLeft(['pt' => DATABASE_CENTER . '.installment_pay_term'], 'p.pay_term_id = pt.id', [])
            ->joinLeft(['f' => DATABASE_CENTER . '.installment_financier'], 'f.id = p.financier_id', [])
            ->joinLeft(['i' => DATABASE_CENTER . '.installment_interest'], 'i.id = p.interest_id', [])
            ->joinLeft(['ch' => DATABASE_CENTER . '.channel'], 'p.channel_id = ch.id', [])
            ->where('p.from_date <= ?', $params['to_date'])
            ->where('p.to_date IS NULL OR p.to_date >= ?', $params['from_date'] )
            ->order('p.financier_id')
          ;

        if ($params['list_channel_id']) {
            $select->where('p.channel_id IN (?)', $params['list_channel_id']);
        }

        if ($params['list_good_price_log_id']) {
            $select->where('p.good_price_log_id IN (?)', $params['list_good_price_log_id']);
        }

        $result = $db->fetchAll($select);

        if ($params['export']) {
            return $result;
        }

        $list = [];
        foreach ($result as $item) {
            $list [$item['channel_id']] [] = $item;
        }

        return $list;

    }

    public function export($data)
    {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $heads = array(
            'Kênh',
            'Công ty',
            'Sản phẩm',
            'Giá bán lẻ',
            'Mức trả trước',
            'Kỳ hạn',
            'Lãi suất',
            'Từ ngày',
            'Đến ngày'
        );

        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;
        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($data as $item){
            $alpha    = 'A';
            $sheet->setCellValue($alpha++.$index, $item['channel_name']);
            $sheet->setCellValue($alpha++.$index, $item['financier_name']);
            $sheet->setCellValue($alpha++.$index, $item['good_price_log_name']);
            $sheet->setCellValue($alpha++.$index, $item['good_price_log_price']);
            $sheet->setCellValue($alpha++.$index, $item['prepay_rate_name']);
            $sheet->setCellValue($alpha++.$index, $item['pay_term_name']);
            $sheet->setCellValue($alpha++.$index, $item['interest_name']);
            $sheet->setCellValue($alpha++.$index, $item['from_date']);
            $sheet->setCellValue($alpha++.$index, $item['to_date']);

            $index++;
        }

        $filename = 'Danh sách gói trả góp';
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }

    public function checkDuplicatePackage($package_id, $channel_id, $financier_id, $good_price_log_id, $prepay_rate_id, $pay_term_id, $interest_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['p2' => DATABASE_CENTER . '.installment_package'], [
                'p2.*'
            ])
            ->joinLeft(['p1' => DATABASE_CENTER . '.installment_package'], 'p1.id = '. $package_id, [])
            ->where('p2.channel_id = ?', $channel_id)
            ->where('p2.financier_id = ?', $financier_id)
            ->where('p2.good_price_log_id = ?', $good_price_log_id)
            ->where('p2.prepay_rate_id = ?', $prepay_rate_id)
            ->where('p2.pay_term_id = ?', $pay_term_id)
            ->where('p2.interest_id = ?', $interest_id)
            ->where('p2.from_date <= p1.to_date OR p1.to_date IS NULL')
            ->where('p2.to_date >= p1.from_date OR p2.to_date IS NULL');
        $result = $db->fetchAll($select);

        return count($result) > 1 ? true : false;
    }

    public function copyPackage($channel_id_from, $channel_id_to)
    {
        $db = Zend_Registry::get('db');
        $query = "
            INSERT INTO hr.installment_package(channel_id, good_price_log_id, financier_id, pay_term_id, prepay_rate_id, interest_id, from_date, to_date)
            SELECT '$channel_id_to', good_price_log_id, financier_id, pay_term_id, prepay_rate_id, interest_id, from_date, to_date
            FROM hr.installment_package
            WHERE channel_id = '$channel_id_from'
        ";

        $stmt = $db->prepare($query);
        $stmt->execute();
        $stmt->closeCursor();
        return;
    }

    public function getChannelByTitle()
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $title = $userStorage->title;
        $staff_id = $userStorage->id;

        $db = Zend_Registry::get("db");
        // pg
        if (in_array($title, [PGPB_TITLE, CHUYEN_VIEN_BAN_HANG_TITLE, PG_LEADER_TITLE, PGS_SUPERVISOR])) {
            if (in_array($title, [PGPB_TITLE, CHUYEN_VIEN_BAN_HANG_TITLE, PG_LEADER_TITLE])) {
                $is_leader = 0;
            }
            if (in_array($title, [PGS_SUPERVISOR])) {
                $is_leader = 8;
            }
            $select = $db->select()
                ->from(['l' => DATABASE_CENTER . '.store_staff_log'], [
                    'id' => 'v.channel_id',
                    'name' => 's.name'

                ])
                ->joinLeft(['s' => DATABASE_CENTER . '.store'], 'l.store_id = s.id', [])
                ->joinLeft(['v' => DATABASE_CENTER . '.v_channel_dealer'], 's.d_id = v.d_id', [])
                ->where('l.released_at IS NULL')
                ->where('l.is_leader = ?', $is_leader)
                ->where('l.staff_id = ?', $staff_id);

            $result = $db->fetchAll($select);

            return $result;
        }

        // sales
        if (in_array($title, [SALES_TITLE])) {
            $is_leader = 1;
            $select = $db->select()
                ->from(['l' => DATABASE_CENTER . '.store_staff_log'], [
                    'id' => 'v.channel_id',
                    'name' => 'c.name'
                ])
                ->joinLeft(['s' => DATABASE_CENTER . '.store'], 'l.store_id = s.id', [])
                ->joinLeft(['v' => DATABASE_CENTER . '.v_channel_dealer'], 's.d_id = v.d_id', [])
                ->joinLeft(['c' => DATABASE_CENTER . '.channel'], 'v.channel_id = c.id', [])
                ->where('l.released_at IS NULL')
                ->where('l.is_leader = ?', $is_leader)
                ->where('l.staff_id = ?', $staff_id)
                ->group('v.channel_id');

            $result = $db->fetchAll($select);

            return $result;
        }


        // leader
        if (in_array($title, [SALES_LEADER_TITLE])) {
            $select = $db->select()
                ->from(['l' => DATABASE_CENTER . '.store_leader_log'], [
                    'id' => 'v.channel_id',
                    'name' => 'c.name'
                ])
                ->joinLeft(['s' => DATABASE_CENTER . '.store'], 'l.store_id = s.id', [])
                ->joinLeft(['v' => DATABASE_CENTER . '.v_channel_dealer'], 's.d_id = v.d_id', [])
                ->joinLeft(['c' => DATABASE_CENTER . '.channel'], 'v.channel_id = c.id', [])
                ->where('l.released_at IS NULL')
                ->where('l.staff_id = ?', $staff_id)
                ->group('v.channel_id');

            $result = $db->fetchAll($select);

            return $result;
        }

        // asm , HO
        if (in_array($userStorage->title, [INSTALLMENT_SUPERVISOR_TITLE, TECHNOLOGY_MANAGER_TITLE, SALES_SUPERVISOR_TITLE, RM_TITLE, SALES_ASSISTANT_TITLE, RM_STANDBY_TITLE]) || in_array($userStorage->team, [KEY_ACCOUNT_TEAM])) {
            $QChannel = new Application_Model_Channel();
            $result = $QChannel->getAll();
            return $result;
        }



    }
}