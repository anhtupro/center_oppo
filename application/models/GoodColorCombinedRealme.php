<?php
class Application_Model_GoodColorCombinedRealme extends Zend_Db_Table_Abstract
{
    protected $_name = 'good_color_combined';
    protected $_schema = WAREHOUSE_DB_RM;

    function getColor($good_id){
        $cols = array(
            'id' => 'g.id',
            'name' => 'g.name'
        );
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('p'=> WAREHOUSE_DB.'.'.$this->_name),$cols)
            ->join(array('g'=> WAREHOUSE_DB.'.good_color'),'p.good_color_id = g.id',array())
            ->where('p.good_id = ?',$good_id);
        $result = $db->fetchPairs($select);
        return $result;
    }

    function getGoodColorName($good_id,$color_id){
        $db = Zend_Registry::get('db');
        $cols = array(
            'name' => 'CONCAT(g.desc," - ",c.name)'
        );
        $select = $db->select()
            ->from(array('p'=>WAREHOUSE_DB.'.'.$this->_name),$cols)
            ->join(array('g'=>WAREHOUSE_DB.'.'.'good'),'p.good_id = g.id',array())
            ->join(array('c'=>WAREHOUSE_DB.'.'.'good_color'),'p.good_color_id = c.id',array())
            ->where('p.good_id = ?',$good_id)
            ->where('p.good_color_id = ?',$color_id);
        $result = $db->fetchOne($select);
        return $result;
    }


}
