<?php
class Application_Model_PurchasingCode extends Zend_Db_Table_Abstract
{
	protected $_name = 'pmodel_code';

	function fetchCode($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        $select = $db->select()->from(array('p' => $this->_name), array(new Zend_Db_Expr
                ('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        $select->join(array('a' => 'pcategory'), 'p.category_id = a.id', array('category' =>
                'a.name'));

        if (isset($params['code']) and $params['code'])
            $select->where('p.code = ?', $params['code']);

        if (isset($params['name']) and $params['name'])
            $select->where('p.name LIKE ?', '%' . $params['name'] . '%');

        if (isset($params['desc']) && $params['desc'])
            $select->where('p.desc LIKE ?', '%' . $params['desc'] . '%');

        if (isset($params['category_id']) && $params['category_id'])
            $select->where('a.id = ?', $params['category_id']);

        $select->order('p.name', 'COLLATE utf8_unicode_ci ASC');

        if ($limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);
        // echo "<pre>";print_r($result);die;
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if ($result === false) {

            $data = $this->fetchAll(null, 'name');

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->code;
                }
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }
    
    function getCategoryList(){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.name",
        );

        $select->from(array('p' => DATABASE_TRADE.'.category'), $arrCols);

        $result = $db->fetchAll($select);

        return $result;
    }
    
    
}