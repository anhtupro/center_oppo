<?php

class Application_Model_StaffUnionFunding extends Zend_Db_Table_Abstract {

    protected $_name = 'staff_union_funding';
    
    public function getStaffUnionFunding($time_lock) {
        $db = Zend_Registry::get('db');
        $sql = "SELECT st.id
        FROM `staff_union_funding` suf
        JOIN (
                SELECT tuf2.`id`
                FROM `time_union_funding` tuf2
                JOIN (
                  SELECT MAX(tuf.`time_upload`) time_upload
                  FROM `time_union_funding` tuf
                  WHERE tuf.`time_upload` < :time_lock
                ) tuf3 ON tuf3.time_upload = tuf2.`time_upload`
        ) tuf4 ON tuf4.id = suf.`id_time`
        JOIN hr.`staff` st ON st.`code` = suf.`staff_code`";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('time_lock', $time_lock, PDO::PARAM_STR);
        $stmt->execute();
        $data_staff = $stmt->fetchAll();
        $stmt->closeCursor();
        $data_return = [];
        foreach ($data_staff as $key => $value) {
            $data_return[] = $value['id'];
        }
        return $data_return;
    }
    public function getDataInsert($time){
        $db = Zend_Registry::get('db');
        $sql = "
        SELECT suf.staff_code
        FROM `staff_union_funding` suf
        JOIN (
                SELECT tuf2.`id`
                FROM `time_union_funding` tuf2
                JOIN (
                  SELECT MAX(tuf.`time_upload`) time_upload
                  FROM `time_union_funding` tuf
                  WHERE tuf.`time_upload` < :time_up
                ) tuf3 ON tuf3.time_upload = tuf2.`time_upload`
        ) tuf4 ON tuf4.id = suf.`id_time`";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('time_up', $time, PDO::PARAM_STR);
        $stmt->execute();
        $list_data = $stmt->fetchAll();
        $stmt->closeCursor();
        $data_return = [];
        foreach ($list_data as $key => $value) {
            $data_return[] = $value['staff_code'];
        }
        return $data_return;
    }
    
    public function exportSalaryUnionFunding($month,$year,$lock_time){
        $tbl_staff = "staff_". (int)$month . '_' . $year;
        $sql_tmp = new Zend_Db_Expr("( SELECT suf.staff_code
        FROM `staff_union_funding` suf
        JOIN (
                SELECT tuf2.`id`
                FROM `time_union_funding` tuf2
                JOIN (
                  SELECT MAX(tuf.`time_upload`) time_upload
                  FROM `time_union_funding` tuf
                  WHERE tuf.`time_upload` <= '$lock_time'
                ) tuf3 ON tuf3.time_upload = tuf2.`time_upload`
        ) tuf4 ON tuf4.id = suf.`id_time` )");
        $db = Zend_Registry::get('db');
        $select = $db->select()->from(array('li' => 'lock_insurance'), ['salary'=>'li.salary', 'salary_congdoan'=> new Zend_Db_Expr("IF(li.salary >= 14900000,149000,  li.salary * 0.01)") , 'unit_code_id' => 'li.unit_code_id'])
                ->join(array('st' => DATABASE_SALARY.'.'.$tbl_staff), 'li.staff_id = st.id', ['staff_id'=>'st.id','code'=>'st.code','fullname'=>new Zend_Db_Expr("CONCAT(st.firstname, ' ',st.lastname)")])
                ->join(array('su' => $sql_tmp), 'su.staff_code = st.code', array())
                ->join(array('d' => 'team'), 'd.id = st.department', ['department' => 'd.name'])
                ->join(array('te' => 'team'), 'te.id = st.team', ['team' => 'te.name'])
                ->join(array('ti' => 'team'), 'ti.id = st.title', ['title' => 'ti.name'])
                ->join(array('rm' => 'regional_market'), 'rm.id = st.regional_market', array())
                ->join(array('re' => 'area'), 're.id = rm.area_id', ['area' => 're.name'])
                ->join(array('uc' => 'unit_code'), 'uc.id = li.unit_code_id', array())
                ->join(array('g' => 'company_group'), 'g.id = ti.company_group', ['group_name' => 'g.name'])
                ->joinLeft(array('o' => 'office'), 'o.id = st.office_id', ['office_name' => 'o.office_name'])
                ->joinLeft(array('pb' => 'lock_insurance_passby'), 'pb.staff_id = li.staff_id AND  pb.lock_time = li.lock_time', array())
                ->where('li.lock_time = ?', $lock_time)
                ->where('li.number_of_month > ?', 0)
                ->where('li.lock_type = ?', 1)
                ->where('li.not_link_to_salary = ?', 0)
                ->where('st.id_place_province NOT IN (?)', [64,65])
                ->where('pb.staff_id IS NULL');
        $select->group('st.id');
        $select->order('st.department');
        $result = $db->fetchAll($select);
        return $result;
    }
}
