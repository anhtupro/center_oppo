<?php

class Application_Model_HeroProductGood extends Zend_Db_Table_Abstract
{
    protected $_name = 'hero_product_good';

    public function getGood($month, $year)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['h' => 'hero_product_good'], [
                'good_name' => 'g.desc'
            ])
            ->join(['g' => WAREHOUSE_DB . '.good'], 'h.good_id = g.id', [])
            ->where('h.month = ?', $month)
            ->where('h.year = ?', $year);

        $result = $db->fetchCol($select);

        return $result;
    }
}