<?php
class Application_Model_ShopSizeType extends Zend_Db_Table_Abstract
{
    protected $_name = 'shop_size_type';
    
    public function getShopSizeOld($store_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.store_id", 
            "p.created_at", 
            "p.created_by", 
            "title_name" => "t.title", 
            "t.type", 
            "t.id"
        );

        $select->from(array('p' => 'shop_size'), $arrCols);
        $select->joinLeft(array('t' => 'shop_size_type'), 't.id = p.shop_size_type_id', array());

        $select->where('p.is_del = 0');
        $select->where('p.store_id = ?', $store_id);
        $select->order('p.created_at DESC');
        $result = $db->fetchAll($select);
        
        $data = [];
        foreach($result as $value){
            $data[$value['type']] = $value['id'];
        }

        return $data;
        
    }
    
}                                                      

