<?php

class Application_Model_AppraisalOfficeView extends Zend_Db_Table_Abstract
{
    const TITLE_RSM_ID = 308;
    const TITLE_ASM_ID = 179;
    const TITLE_ASM_STANDBY_ID = 181;
    const TITLE_SALE_LEADER_ID = 190;
    const TITLE_SALE_ID = 183;
    const TITLE_PG_ID = 182;

    const TITLE_RSM = 'RSM';
    const TITLE_ASM = 'ASM';
    const TITLE_ASM_STANDBY = 'ASM';
    const TITLE_SALE_LEADER = 'LEADER';
    const TITLE_SALE = 'SALE';
    const TITLE_PG = 'PG';

    protected $_name = 'v_appraisal_sale';
    protected $_primary = 'id';

    /**
     * @param $staffId
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getToDoSurvey($staffId, $planId)
    {
        $data = [
            'superior' => [],
            'self' => [],
            'subordinate' => []
        ];
        
        // Get data
        $QToDo = new Application_Model_AppraisalOfficeToDo();
        $result = $QToDo->getToDoSurvey($planId, $staffId);
        
        
        foreach ($result as $todo) {
            $tmp = [
                'id' => $todo['to_staff'],
                'title' => $todo['to_title'],
                'survey_id' => $todo['survey_id']
            ];
            switch ($todo['type']) {
                case 0:
                    $data['self'][] = $tmp;
                    break;
                case 1:
                    $data['subordinate'][] = $tmp;
                    break;
                case 2:
                    $data['superior'][] = $tmp;
                    break;
            }
        }
        return $data;
    }

    /**
     * @param $userId
     * @return bool
     * @throws Zend_Exception
     */
    public function hasSurveyToDo($userId)
    {
        $QPlan = new Application_Model_AppraisalSalePlan();
        $plan = $QPlan->getLastPlan();
        
        
        
        if ($plan === null) {
            return false;
        }
        $toDo = $this->getToDoSurvey($userId);
        
        
        
        $QMember = new Application_Model_AppraisalSaleMember();
        $members = $QMember->getByPlanAndStaff($plan['asp_id'], $userId);
        $total = array_sum(array_map('count', $toDo));
        
        return $total != count($members);
    }

    /**
     * @param $staffId string
     * @param $title
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getTotalAppraisalSale($staffId, $title)
    {
        $sql = 'select p.asp_id, p.asp_name, ifnull(round(sum(m.asm_total_point / m.asm_count_question) / count(*), 1), 0) total
from (select distinct if(td.to_title = :title, td.to_staff, if(td1.to_title = :title, td1.to_staff, if(td2.to_title = :title, td2.to_staff, td3.to_staff))) staff_id, td.asp_id
      from tmp_appraisal_to_do td
        left join tmp_appraisal_to_do td1 on td.asp_id = td1.asp_id and td.to_staff = td1.from_staff and td.to_title = td1.from_title and td.type = 1
        left join tmp_appraisal_to_do td2 on td1.asp_id = td2.asp_id and td1.to_staff = td2.from_staff and td1.to_title = td2.from_title and td1.type = 1
        left join tmp_appraisal_to_do td3 on td2.asp_id = td3.asp_id and td2.to_staff = td3.from_staff and td2.to_title = td3.from_title and td2.type = 1
      where (td.to_title = :title or td1.to_title = :title or td2.to_title = :title or td3.to_title = :title) and (:staff_id = 0 or td.from_staff = :staff_id)) l
  join tmp_appraisal_to_do td on l.asp_id = td.asp_id and l.staff_id = td.to_staff and td.from_title != 182
  join (select * from appraisal_sale_plan where asp_status != 0 and asp_is_deleted = 0 order by asp_id desc limit 4) p on l.asp_id = p.asp_id
  left join appraisal_sale_member m on td.asp_id = m.asp_id and td.to_staff = m.asm_staff_be_appraisal and td.from_staff = m.asm_staff_id and m.asm_is_deleted = 0
group by l.asp_id
order by l.asp_id desc';
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare($sql);
        $stmt->bindParam('staff_id', $staffId, PDO::PARAM_INT);
        $stmt->bindParam('title', $title, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return array_reverse($result);
    }

    /**
     * @param $title
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getTotalAppraisalByTitle($title)
    {
        $sql = "select p.asp_id, p.asp_name, ifnull(round(sum(result) / count(*), 1), 0) total
from (select td.asp_id, to_staff, ifnull(round(sum(m.asm_total_point / m.asm_count_question) / count(*), 1), 0) result
  from tmp_appraisal_to_do td
    left join appraisal_sale_member m on td.asp_id = m.asp_id and td.from_staff = m.asm_staff_id and td.to_staff = m.asm_staff_be_appraisal and m.asm_is_deleted = 0
  where to_title = :title
  group by td.asp_id, to_staff) tmp
join appraisal_sale_plan p on tmp.asp_id = p.asp_id
group by p.asp_id
order by p.asp_id desc
limit 4";
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare($sql);
        $stmt->bindParam('title', $title, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return array_reverse($result);
    }

    public function getTotalAppraisalSubMember($boss_id)
    {
        $db = Zend_Registry::get('db');

        $sql = "SELECT a.`asp_id`,
                        asp.asp_name AS asp_name,
                        ROUND(SUM(IFNULL(a.`asm_total_point`, 0) / IF(a.`asm_count_question` IS NULL OR a.`asm_count_question` = 0, 1, a.`asm_count_question`)) / COUNT(*), 1) AS total
                FROM `appraisal_sale_member` AS a
                JOIN (
                        SELECT `asm_staff_id`, `asm_staff_be_appraisal`, `asp_id`
                        FROM `appraisal_sale_member`
                        WHERE `asm_staff_be_appraisal` = $boss_id AND `asm_type` = 2
                     ) AS b ON a.`asm_staff_be_appraisal` = b.`asm_staff_id` AND a.`asp_id` = b.`asp_id` 
                LEFT JOIN appraisal_sale_plan as asp ON a.`asp_id` = asp.asp_id
                WHERE  asp.asp_status != 0 AND asp.asp_is_deleted = 0
                GROUP BY a.`asp_id` 
                LIMIT 4 ";

        $stmt = $db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();

        return $result;
    }

    public function getTotalAppraisalSaleLeaderByAsm($asm_id)
    {
        $sql = "select asp_id, asp_name, round(sum(asm_total_point / asm_count_question) / count(*), 1) total
from (select distinct s.asp_id, p.asp_name, s.asm_id, s.leader_id, m.asm_count_question, m.asm_total_point
from tmp_appraisal_sale s
join appraisal_sale_plan as p ON s.asp_id = p.asp_id AND p.asp_status != 0 AND p.asp_is_deleted = 0
  left join appraisal_sale_member m on s.leader_id = m.asm_staff_be_appraisal and s.asp_id = m.asp_id
where s.asm_id = :staff_id and s.leader_id is not null) tmp
group by asp_id desc
limit 4";
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare($sql);
        $stmt->bindParam('staff_id', $asm_id, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return array_reverse($result);
    } // Done

    public function getTotalAppraisalSaleByAsm($asm_id)
    {
        $sql = "select asp_id, asp_name, round(sum(asm_total_point / asm_count_question) / count(*), 1) total
from (select distinct s.asp_id, p.asp_name, s.asm_id, s.sale_id, m.asm_count_question, m.asm_total_point
from tmp_appraisal_sale s
join appraisal_sale_plan as p ON s.asp_id = p.asp_id AND p.asp_status != 0 AND p.asp_is_deleted = 0
  left join appraisal_sale_member m on s.sale_id = m.asm_staff_be_appraisal and s.asp_id = m.asp_id
where s.asm_id = :staff_id and s.sale_id is not null) tmp
group by asp_id desc
limit 4";
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare($sql);
        $stmt->bindParam('staff_id', $asm_id, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();

        return array_reverse($result);
    } // Done

    public function getLeaderByAsm($asmId, $planId)
    {
        $sql = "select distinct st.id sub_member_id, concat(st.firstname,' ', st.lastname) AS sub_member_name
from tmp_appraisal_sale s
left join staff AS st ON s.leader_id = st.id
where s.asm_id = :staff_id and s.asp_id = :plan_id and s.leader_id is not null";
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare($sql);
        $stmt->bindParam('staff_id', $asmId, PDO::PARAM_INT);
        $stmt->bindParam('plan_id', $planId, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();

        return array_reverse($result);
    }

    public function getSaleByAsm($asmId, $planId)
    {
        $sql = "select distinct st.id sub_member_id, concat(st.firstname,' ', st.lastname) AS sub_member_name
from tmp_appraisal_sale s
left join staff AS st ON s.sale_id = st.id
where s.asm_id = :staff_id and s.asp_id = :plan_id and s.sale_id is not null";
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare($sql);
        $stmt->bindParam('staff_id', $asmId, PDO::PARAM_INT);
        $stmt->bindParam('plan_id', $planId, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();

        return array_reverse($result);
    }

    /**
     * @param $planId
     * @param $userId
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getDetailAppraisal($planId, $userId)
    {
        $QPlan = new Application_Model_AppraisalSalePlan();
        $plan = $QPlan->getPlanById($planId);

        $surveyId = $plan['asp_survey_rsm_asm'];
        $QToDo = new Application_Model_AppraisalSaleToDo();
        if (in_array($QToDo->getTitleById($userId, $planId), [self::TITLE_SALE_LEADER_ID, self::TITLE_SALE_ID])) {
            $surveyId = $plan['asp_survey_sale_leader'];
        }
        if (in_array($QToDo->getTitleById($userId, $planId), [self::TITLE_PG_ID])) {
            $surveyId = $plan['asp_survey_pg'];
        }
        $answersSale = self::getResultAppraisalSale($planId, $surveyId, $userId);
        $QMember = new Application_Model_AppraisalSaleMember();
        $memberComment = $QMember->getCommentForStaff($planId, $userId);
        $dataComment = [];
        foreach ($memberComment as $member) {
            $tmp = [
                'name' => '---',
                'asm_promote' => $member['asm_promote'],
                'asm_improve' => $member['asm_improve'],
                'asm_staff_title' => $member['asm_staff_title'],
            ];
            if ($member['asm_type'] == 1) { // Cấp trên
                $QStaff = new Application_Model_Staff();
                $staff = $QStaff->fetchRow($QStaff->getAdapter()->quoteInto('id = ?', $member['asm_staff_id']));
                if ($staff) {
                    $tmp['name'] = $staff['firstname'] . ' ' . $staff['lastname'];
                }
            }
            $dataComment[] = $tmp;
        }
        $result = [
            'data_sale' => $answersSale,
            'data_comment' => $dataComment
        ];
        if ($QToDo->getTitleById($userId, $planId) == self::TITLE_SALE_ID) {
            $answersPg = self::getResultPGAppraisalSale($planId, $userId);
            $result['data_pg'] = $answersPg;
        }
        return $result;
    }

    /**
     * @param $planId
     * @param $surveyId
     * @param $staffId
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getResultAppraisalSale($planId, $surveyId, $staffId)
    {
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $query = "select q.dsq_id, q.dsq_title question, ifnull(round(sum(a.dsa_value) / count(*), 1), 0) result
from dynamic_survey ds
  join dynamic_survey_question q on ds.ds_id = q.fk_ds and ds.ds_id = :survey_id and q.dsq_type in ('range', 'radio', 'checkbox', 'select')
  join tmp_appraisal_to_do td on td.asp_id = :plan_id and td.to_staff = :staff_id and td.from_title != 182
  left join appraisal_sale_member m on td.asp_id = m.asp_id and td.from_staff = m.asm_staff_id and td.to_staff = m.asm_staff_be_appraisal and m.asm_is_deleted = 0
  left join dynamic_survey_result_detail rd on m.fk_dsr = rd.fk_dsr and q.dsq_id = rd.fk_dsq
  left join dynamic_survey_answer a on rd.fk_dsa = a.dsa_id and q.dsq_id = a.fk_dsq
group by q.dsq_id";
        /** @var \Zend_Db_Statement $stmt */
        $stmt = $db->prepare($query);
        $stmt->bindParam('staff_id', $staffId, PDO::PARAM_INT);
        $stmt->bindParam('plan_id', $planId, PDO::PARAM_INT);
        $stmt->bindParam('survey_id', $surveyId, PDO::PARAM_INT);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        return $data;
    }

    /**
     * @param $planId
     * @param $staffId
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getResultPGAppraisalSale($planId, $staffId)
    {
        $query = "select dsq_title question, ifnull(round(sum(dsa_value) / count(*), 1), 0) result
from dynamic_survey_result_detail rd
join dynamic_survey_question q on rd.fk_dsq = q.dsq_id
join dynamic_survey_answer a on rd. fk_dsa = a.dsa_id
join appraisal_sale_member m on rd.fk_dsr = m.fk_dsr and m.asp_id = :plan_id and m.asm_staff_be_appraisal = :staff_id and m.asm_staff_title = 182
right join tmp_appraisal_to_do td on td.from_title = 182 and m.asm_staff_id = td.from_staff and m.asm_is_deleted = 0
group by dsq_id
having dsq_id is not null";
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare($query);
        $stmt->bindParam('staff_id', $staffId, PDO::PARAM_INT);
        $stmt->bindParam('plan_id', $planId, PDO::PARAM_INT);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        return $data;
    }

    /**
     * @param $bossId
     * @param $subTitle
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getResultByLevel($bossId, $subTitle)
    {
        $query = "select l.asp_id, l.staff_id, concat(s.firstname, ' ', s.lastname) staff_name, ifnull(round(sum(m.asm_total_point / m.asm_count_question) / count(*), 1), 0) result
from (select distinct if(td.to_title = :title, td.to_staff, if(td1.to_title = :title, td1.to_staff, if(td2.to_title = :title, td2.to_staff, td3.to_staff))) staff_id, td.asp_id
      from tmp_appraisal_to_do td
        left join tmp_appraisal_to_do td1 on td.asp_id = td1.asp_id and td.to_staff = td1.from_staff and td.to_title = td1.from_title and td.type = 1
        left join tmp_appraisal_to_do td2 on td1.asp_id = td2.asp_id and td1.to_staff = td2.from_staff and td1.to_title = td2.from_title and td1.type = 1
        left join tmp_appraisal_to_do td3 on td2.asp_id = td3.asp_id and td2.to_staff = td3.from_staff and td2.to_title = td3.from_title and td2.type = 1
      where (td.to_title = :title or td1.to_title = :title or td2.to_title = :title or td3.to_title = :title) and td.from_staff = :staff_id) l
  join tmp_appraisal_to_do td on l.asp_id = td.asp_id and l.staff_id = td.to_staff and td.from_title != 182
  join (select * from appraisal_sale_plan where asp_status != 0 and asp_is_deleted = 0 order by asp_id desc limit 4) p on l.asp_id = p.asp_id
  join staff s on l.staff_id = s.id
  left join appraisal_sale_member m on td.asp_id = m.asp_id and td.to_staff = m.asm_staff_be_appraisal and td.from_staff = m.asm_staff_id and m.asm_is_deleted = 0
group by l.asp_id, l.staff_id";
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare($query);
        $stmt->bindParam('title', $subTitle, PDO::PARAM_INT);
        $stmt->bindParam('staff_id', $bossId, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();

        $data = [];
        foreach ($result as $value) {
            if ($data[$value['asp_id']] == null) {
                $data[$value['asp_id']] = [];
            }
            if ($value['staff_id'] != null) {
                if ($data[$value['asp_id']][$value['staff_id']] == null) {
                    $data[$value['asp_id']][$value['staff_id']] = [
                        'staff_name' => '',
                        'result' => 0,
                    ];
                }
                $data[$value['asp_id']][$value['staff_id']]['staff_name'] = $value['staff_name'];
                $data[$value['asp_id']][$value['staff_id']]['result'] += $value['result'];
            }
        }
        return $data;
    }

    /**
     * @param $planId
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getResultRsmRegion($planId)
    {
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');

        $sql = "select reg.id, reg.name, reg.rsm_id staff_id, round(sum(ifnull(mem.asm_total_point, 0)) / sum(if(asm_count_question != 0 and asm_count_question is not null, asm_count_question, 1)), 1) as result
from region reg
  left join appraisal_sale_member mem on reg.id = mem.asm_to_cache and mem.asm_staff_be_appraisal_title = 308 and mem.asp_id = :plan_id
where reg.del = 0 and reg.status = 1
group by reg.id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('plan_id', $planId, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();

        $data = [];
        foreach ($result as $region) {
            $data[] = [
                'region_id' => $region['id'],
                'region_name' => $region['name'],
                'staff_id' => $region['staff_id'],
                'result' => $region['result'],
            ];
        }

        return $data;
    }

    /**
     * @param $planId
     * @param $regionId
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getResultAsmArea($planId, $regionId)
    {
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');

        $sql = "select area.id, area.name, area.asm_id staff_id, round(sum(ifnull(mem.asm_total_point, 0)) / sum(if(asm_count_question != 0 and asm_count_question is not null, asm_count_question, 1)), 1) as result
from area
  left join appraisal_sale_member mem on area.id = mem.asm_to_cache and mem.asm_staff_be_appraisal_title = 179 and mem.asp_id = :plan_id
where area.name is not null and area.region_id = :region_id and area.del = 0 and area.status = 1
group by area.id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('plan_id', $planId, PDO::PARAM_INT);
        $stmt->bindParam('region_id', $regionId, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();

        $data = [];
        foreach ($result as $region) {
            $data[] = [
                'area_id' => $region['id'],
                'area_name' => $region['name'],
                'staff_id' => $region['staff_id'],
                'result' => $region['result'],
            ];
        }
        return $data;
    }

    /**
     * @param array $data
     * @param int $total
     * @param int $page
     * @param bool $export
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getResult($data, &$total, $page = 0, $export = false)
    {
        
        $limit = 20;
        
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.from_staff'),
            'p.fk_plan', 
            'p.from_title', 
            'p.to_staff', 
            'p.to_title', 
            'p.survey_id', 
            'from_name' => "CONCAT(s.firstname, ' ', s.lastname)", 
            'to_name' => "CONCAT(s2.firstname, ' ', s2.lastname)", 
            'from_title' => 't.name', 
            'to_title' => 't2.name',
            'created_at' => 'c.aoc_created_at',
            "point" => "SUM(d.fk_dsa) / COUNT(DISTINCT d.fk_dsq)"
        );

        $select->from(array('p' => 'appraisal_office_to_do'), $arrCols);
        $select->joinLeft(array('s' => 'staff'), 's.id = p.from_staff', array());
        $select->joinLeft(array('s2' => 'staff'), 's2.id = p.to_staff', array());
        $select->joinLeft(array('t' => 'team'), 't.id = p.from_title', array());
        $select->joinLeft(array('t2' => 'team'), 't2.id = p.to_title', array());
        $select->joinLeft(array('c' => 'appraisal_office_capacity'), 'c.fk_staff = p.from_staff AND c.fk_staff_be_appraisal = p.to_staff AND c.fk_aop = '.$data['plan_id'], array());
        $select->joinLeft(array('d' => 'dynamic_survey_result_detail'), 'd.fk_dsr = c.fk_dsr', array());
        
        if(!empty($data['from_name'])){
            $select->where("CONCAT(s.firstname, ' ', s.lastname) LIKE ?", "%".$data['from_name']."%");
        }
        
        if(!empty($data['to_name'])){
            $select->where("CONCAT(s2.firstname, ' ', s2.lastname) LIKE ?", "%".$data['to_name']."%");
        }
        $select->where('p.fk_plan = ?', $data['plan_id']);
        $select->group('p.aotd_id');
        $select->order('c.aoc_created_at DESC');
        
        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        

        return $result;
    }

    /**
     * @param $planId
     * @param $fromStaff
     * @param $toStaff
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getResultDetail($planId, $fromStaff, $toStaff)
    {
        $query = "SELECT p.fk_aop, p.fk_dsr, p.fk_staff, p.fk_staff_be_appraisal, d.fk_dsq, d.fk_dsa, q.dsq_title question, a.dsa_title value
FROM appraisal_office_capacity p 
LEFT JOIN dynamic_survey_result_detail d ON d.fk_dsr = p.fk_dsr
LEFT JOIN dynamic_survey_question q ON q.dsq_id = d.fk_dsq
LEFT JOIN dynamic_survey_answer a ON a.fk_dsq = q.dsq_id AND a.dsa_value = d.fk_dsa
WHERE p.fk_aop = :plan_id AND p.fk_staff = :from_staff AND p.fk_staff_be_appraisal = :to_staff";

        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare($query);
        $stmt->bindParam('plan_id', $planId, PDO::PARAM_INT);
        $stmt->bindParam('from_staff', $fromStaff, PDO::PARAM_INT);
        $stmt->bindParam('to_staff', $toStaff, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }
    
    public function getResultChart($planId)
    {
        $query = "SELECT p.fk_aop, p.fk_dsr, fk_staff_be_appraisal, d.fk_dsq, d.fk_dsa, COUNT(d.fk_dsa) total, q.dsq_title
from appraisal_office_capacity p
LEFT JOIN dynamic_survey_result r ON r.dsr_id = p.fk_dsr
LEFT JOIN dynamic_survey_result_detail d ON d.fk_dsr = r.dsr_id
LEFT JOIN dynamic_survey_question q ON q.dsq_id = d.fk_dsq
WHERE p.fk_aop = :plan_id AND r.fk_ds = 22 AND  d.fk_dsq IS NOT NULL AND q.dsq_id IS NOT NULL AND p.fk_staff = p.fk_staff_be_appraisal
GROUP BY d.fk_dsq, d.fk_dsa
ORDER BY d.fk_dsq
";

        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare($query);
        
        $stmt->bindParam('plan_id', $planId, PDO::PARAM_INT);
        
        /*
        $stmt->bindParam('from_staff', $fromStaff, PDO::PARAM_INT);
        $stmt->bindParam('to_staff', $toStaff, PDO::PARAM_INT);
         * */
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }
}