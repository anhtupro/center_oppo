<?php
class Application_Model_TrainerCourse extends Zend_Db_Table_Abstract
{
    protected $_name = 'trainer_course';

    function fetchPagination($page, $limit, &$total, $params){

        $db = Zend_Registry::get('db');
        $cols  = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
            'p.*',
            'if (e.id IS NOT NULL,1,0) as have_assign',
            'quantity_pg' => 'SUM(IF(e.id IS NULL,0,1))',
            'quantity_pass' => 'SUM(IF(e.id IS NOT NULL AND e.result = 1,1,0))',
        );
        $select = $db->select()
            ->from(array('p' => $this->_name),$cols);

        $select->joinLeft(array('e'=>'trainer_course_detail'),'e.course_id = p.id AND (e.del = 0 OR e.del IS NULL)',array('e.id as course_detail_id'));

        if(isset($params['name']) and $params['name'])
            $select->where('p.name LIKE ?','%'.$params['name'].'%');

        if(isset($params['type']) and $params['type'])
            $select->where('p.type = ?', $params['type']);

        if(isset($params['from_date']) and $params['from_date'])
            $select->where('p.from_date >= ?',$params['from_date']);

        if(isset($params['created_by']) and $params['created_by']){
            if(is_array($params['created_by']) AND count($params['created_by'])){
                $select->where('p.created_by IN (?)',$params['created_by']);    
            }elseif(is_numeric($params['created_by'])){
                $select->where('p.created_by = ?',$params['created_by']);    
            }
        }

        if(isset($params['to_date']) and $params['to_date'])
            $select->where('p.to_date <= ?',$params['to_date']);

        $select->where('p.del = ? OR p.del IS NULL',0); 
        $select->order('p.from_date DESC');
        $select->group('p.id');

        if($limit)
        $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    function getCourse($userStorage = null,$date = null)
    {
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),array('*'));

        $select->where('p.del = ? OR p.del IS NULL',0);

        if(isset($userStorage) and $userStorage)
        {
           $select->where('p.created_by = ?',$userStorage->id);
        }
        if(isset($date) and $date)
        {
          $select->where('p.from_date <= ?',$date);
          $select->where('p.to_date >= ?',$date);
        }

        $select->order('p.id');
        $result = $db->fetchAll($select);

        return $result;

    }
    /* function nay dung de check coi co khoa hoc nao dang ky trung khong */
    function checkCourse()
    {
      $db = Zend_Registry::get('db');
      $select = $db->select()
          ->from(array('p' => $this->_name),array('max(p.from_date) as max_from_date','max(p.to_date) as max_to_date'));
      $select->where('p.del = ? OR p.del IS NULL',0);
      $result = $db->fetchRow($select);
      $arrayResult = array();
      if($result)
      {
        $arrayResult['from'] = $result['max_from_date'];
        $arrayResult['to'] = $result['max_to_date'];
      }
      return $arrayResult;
    }

    public function fetchPaginationCourseFinished($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');
        $cols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.id'),
            'a.*',
            'locked_by_name' => new Zend_Db_Expr('CONCAT(c.firstname," ",c.lastname)'),
            'finished' => new Zend_Db_Expr('CASE WHEN b.id IS NULL THEN 0 ELSE 1 END') ,

        );
        $select = $db->select()
            ->from(array('a' => $this->_name),$cols)
            ->joinLeft(array('b'=> 'trainer_course_lock'),'YEAR(a.to_date) = b.`year` AND MONTH(a.to_date) = b.`month`',array())
            ->joinLeft(array('c'=>'staff'),'c.id = a.locked_by',array())
            ->where('a.del = 0 OR a.del IS NULL')
        ;

        if(isset($params['name']) AND $params['name']){
            $select->where('a.name LIKE ?','%'.$params['name'].'%');
        }

        if(isset($params['from_date']) and $params['from_date'] and isset($params['to_date']) and $params['to_date']){
            $select->where("
                (a.from_date >= '".$params['from_date']. "' AND a.to_date <= '".$params['to_date']."')
                OR ( '".$params['from_date']."' BETWEEN a.from_date AND a.to_date)
                OR ( '".$params['to_date']."' BETWEEN a.from_date AND a.to_date)
            ");

        }

        if($limit)
            $select->limitPage($page, $limit);

        $order_str = '';

        if (isset($params['sort']) and $params['sort']) {
            $collate = ' ';
            $desc  = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';
            if ($params['sort'] == 'name'){
                $collate   = ' COLLATE utf8_unicode_ci ';
                $order_str .= ' a.name '.$collate . $desc;
            }else {
                $order_str = 'a.`'.$params['sort'] . '` ' . $collate . $desc;
            }

            $select->order(new Zend_Db_Expr($order_str));
        }else{
            $select->order('p.created_at DESC');
        }
//        echo $select;
//        exit;
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    /**
     * Lấy danh sách trainer được quản lý bởi leader
     * @param  [type] $staff_id [description]
     * @return [type]           [description]
     */
    public function getTrainerByLeader($staff_id){
        $db = Zend_Registry::get("db");
        $QStaff = new Application_Model_Staff();
        $staff = $QStaff->find($staff_id)->current();
        if(!$staff){
            return false;
        }

        if($staff->group_id != TRAINING_LEADER_ID){
            return array($staff->id);
        }

        if($staff->group_id == TRAINING_LEADER_ID AND $staff->title != 383){
            $select = $db->select()
                ->from(array('a'=>'asm'),array('a.area_id','b.id'))
                ->join(array('b'=>'staff'),'a.staff_id = b.id',array())
                ->where('b.id = ?',$staff->id);
            $main_select = $db->select()
                ->from(array('a'=>'asm'),array('staff_id'=>'b.id'))
                ->join(array('b'=>'staff'),'a.staff_id = b.id',array())
                ->join(array('c'=>$select),'c.area_id = a.area_id',array())
                ->where('b.group_id IN (?)',array(TRAINING_LEADER_ID,TRAINING_TEAM_ID))
                ->group('a.staff_id')
                ;
            $result = $db->fetchAll($main_select);
            $data = array();
            if($result){
                foreach($result as $item){
                    $data[] = $item['staff_id'];
                }
            }
            return $data;
        }

        return false;
    }
}

