<?php

class Application_Model_StoreCapacityAssign extends Zend_Db_Table_Abstract
{
    protected $_name = 'store_capacity_assign';
    protected $_schema = DATABASE_TRADE;

    public function getCapacity($stage_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['d' => DATABASE_TRADE.'.store_capacity_assign'], [
                'c.*'
            ])
            ->joinLeft(['c' => DATABASE_TRADE.'.store_capacity'], 'd.capacity_id = c.id', [])
            ->where('d.stage_id = ?', $stage_id);

        $result = $db->fetchAll($select);

        return $result;
    }



}