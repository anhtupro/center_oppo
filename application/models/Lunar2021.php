<?php
class Application_Model_Lunar2021 extends Zend_Db_Table_Abstract
{
    protected $_name = 'lunar_2021';
    public function check_staff($staff_code){
        $db     = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('l' => 'lunar_2021'), '*');
        $select->join(array('r' => 'regional_market'), 'r.id = l.district' , ['id_district'=>'r.id','name_district'=>'r.name']);
        $select->join(array('w' => 'ward'), 'w.id = l.ward' , ['id_ward' => 'w.id','name_ward'=> 'w.name']);
        $select->where('l.code = ?', $staff_code);
        $result = $db->fetchRow($select);
        return $result;
    }
    public function check_staff_lunar($staff_id){
        $db     = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('l' => 'staff_lunar_new_year2021'), '*');
        $select->where('l.id = ?', $staff_id);
        $result = $db->fetchRow($select);
        return empty($result) ? 0 : 1;
    }
}
