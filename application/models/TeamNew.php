<?php
class Application_Model_TeamNew extends Zend_Db_Table_Abstract
{
	protected $_name = 'team_new';

    public function getListTeam($id_label){
        $db = Zend_Registry::get('db');
        $selectManager = $db->select()
            ->from(array('t' => $this->_name),array("t.name","t.id"))
            ->where('id =?',$id_label);
        $manager=$db->fetchAll($selectManager);

        //get id of chid this manange
        $this->getChildIdAction($id_label,$childIds);

        if(count($childIds)){
            $selectChilds = $db->select()
                ->from(array('t' => $this->_name),array("t.name","t.id"))
                ->where('id IN (?)',$childIds);
            $listChilds = $db->fetchAll($selectChilds);
        }
        $result=array_merge($manager,$listChilds);

        return $result;
    }

    public function getChildIdAction($parentId,&$arr = NULL){

        if(!$arr) $arr = array();
        $QTeamNew=new Application_Model_TeamNew();

        foreach ($QTeamNew->fetchChildId($parentId) as $row){
            $arr[] = $row['id'];
            $arr = $this->getChildIdAction($row['id'],$arr);
        }
        return $arr;
    }

    function getListTeam1($id_label){
         $db = Zend_Registry::get('db');
            
            $select = $db->select()
            ->from(array('tn' => $this->_name),array("tn.id","tn.name"))
            ->where('tn.parent_id = ?',$id_label)
            ->where('del = ?',0);
            $list_team = $db->fetchAll($select);
            
            foreach($list_team as $team){
                $select = $db->select()
                ->from(array('tn1' => $this->_name),array("tn1.id","tn1.name"))
                ->where('tn1.parent_id = ?',$team['id'])
                ->where('del = ?',0);  
                $list_title_teamp = $db->fetchAll($select);
                
                $list_team = array_merge($list_team,$list_title_teamp);
                foreach($list_title_teamp as $team1){
                    $select = $db->select()
                    ->from(array('tn3' => $this->_name),array("tn3.id","tn3.name"))
                    ->where('tn3.parent_id = ?',$team1['id'])
                    ->where('del = ?',0);  
                    $list_team = array_merge($list_team,$db->fetchAll($select));
                }
            }
            
            $select = $db->select()
            ->from(array('tn2' => $this->_name),array("tn2.id","tn2.name"))
            ->where('tn2.id = ?',$id_label)
            ->where('del = ?',0);

    
            return $list_team;
        }
         
         
        
        function getTypeNameLabel($idLabel){
            $db = Zend_Registry::get('db');   
             $select = $db->select()
            ->from(array('tn' => $this->_name),array("tn.type","tn.name"))
            ->where('tn.id = ?',$idLabel)
            ->where('del = ?',0);
            $result = $db->fetchAll($select);
            return $result;
        }
        function getListWithType($type){
            $db = Zend_Registry::get('db');   
            $select = $db->select()
            ->from(array('tn' => $this->_name),array("tn.id","tn.name"))
            ->where('tn.type = ?',$type)
            ->where('del = ?',0);  
            $result = $db->fetchAll($select);
            return $result;
        }
        
        function getTeamAndTitleByDepartment($department_id){
            $db = Zend_Registry::get('db');   
            $select = $db->select()
            ->from(array('tn' => $this->_name),array("tn.id","tn.name"))
            ->where('tn.parent_id = ?',$department_id)
            ->where('del = ?',0);  
            $list_team = $db->fetchAll($select);
            $i = 0 ;
            foreach($list_team as $team){
                $select = $db->select()
                ->from(array('tn1' => $this->_name),array("tn1.id","tn1.name"))
                ->where('tn1.parent_id = ?',$team['id'])
                ->where('del = ?',0);  
                $list_title = $db->fetchAll($select);
                $list_team[$i]['list-title'] = $list_title;
                $i++;
            }
            return $list_team;
        }
    //cong danh
    	function fetchGroup(){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array('p.name', 'p.id'));

        $select->where('p.parent_id=?', 0);
        $select->where('p.type=?', 0);
        $select->where('p.del=?', 0);

        $result = $db->fetchAll($select);
        return $result;
    }

    function fetchCompany(){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array('p.name', 'p.id'));

        $select->where('p.parent_id=?', 1);
        $select->where('p.type=?', 1);
        $select->where('p.del=?', 0);

        $result = $db->fetchAll($select);
        return $result;
    }

    function fetchDivision(){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array('p.name', 'p.id'));

        $select->where('p.parent_id=?', 2);
        $select->where('p.type=?', 2);
        $select->where('p.del=?', 0);

        $result = $db->fetchAll($select);
        return $result;
    }


    function fetchCompanyOfGroup($groupId){
        $db = Zend_Registry::get('db');
        $select = $db->select()->from(array('t' => $this->_name),
                array('t.name', 't.id'));
        $select->where('t.parent_id=?', $groupId);
        $select->where('t.type=?', 1);
        $select->where('t.del=?', 0);

        $result = $db->fetchAll($select);
        return $result;
    }

    function fetchDivisionOfCompany($companyId){
        $db = Zend_Registry::get('db');
        $select = $db->select()->from(array('t' => $this->_name),
            array('t.name', 't.id'));
        $select->where('t.parent_id=?', $companyId);
        $select->where('t.type=?', 2);
        $select->where('t.del=?', 0);

        $result = $db->fetchAll($select);
        return $result;
    }

    function fetchDepartmentOfDivision($divisionId){
        $db = Zend_Registry::get('db');
        $select = $db->select()->from(array('t' => $this->_name),
            array('t.name', 't.id','t.team_id'));
        $select->where('t.parent_id=?', $divisionId);
        $select->where('t.type=?', 3);
        $select->where('t.del=?', 0);

        //return $qr=$select->__toString();

        $result = $db->fetchAll($select);
        return $result;
    }

    function fetchTeamOfDepartment($departmentId){
        $db = Zend_Registry::get('db');
        $select = $db->select()->from(array('t' => $this->_name),
            array('t.name', 't.id','t.team_id'));
        $select->where('t.parent_id=?', $departmentId);
        $select->where('t.type=?', 4);
        $select->where('t.del=?', 0);

        $result = $db->fetchAll($select);
        return $result;
    }


    function fetchTitleOfTeam($teamId){
        $db = Zend_Registry::get('db');
        $select = $db->select()->from(array('t' => $this->_name),
            array('t.name', 't.id','t.team_id'));
        $select->where('t.parent_id=?', $teamId);
        $select->where('t.type=?', 5);
        $select->where('t.del=?', 0);

        $result = $db->fetchAll($select);
        return $result;
    }

    function fetchChildId($parentId){
        $db = Zend_Registry::get('db');
        $select = $db->select()->from(array('t' => $this->_name),
            array('t.id'));
        $select->where('t.parent_id=?', $parentId);

        $result = $db->fetchAll($select);
        return $result;
    }

    function fetchAllData(){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('t' => $this->_name),array('*'));
        $select->where('t.del=?', 0);

        $result = $db->fetchAll($select);
        return $result;
    }
    //end cong danh
        
}