<?php
class Application_Model_StaffChannel extends Zend_Db_Table_Abstract
{
	protected $_name = 'staff_channel';

    function get_staff_channel($staff_id){
		
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.name",
        );

        $select->from(array('p' => 'channel_ka'), $arrCols);
        $select->joinLeft(array('s' => 'channel_ka_staff'), 's.channel_ka_id = p.id', array());

        $select->where('p.status = ?', 1);
        $select->where('s.staff_id = ?', $staff_id);
        
        $data = $db->fetchAll($select);

        $result = array();
        if ($data){
            foreach ($data as $item){
                $result[$item['id']] = $item['name'];
            }
        }
        
        return $result;
    }
        function get_channel(){
        $db = Zend_Registry::get("db");
        $select = $db->select();
        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.*'));
        $select->from(array('p'=> 'staff_channel'), $arrCols);
        $select->group('p.channel');
        $result = $db->fetchAll($select);
        return $result;
    }
    
    function get_channel_ka($channel_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();
        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.*'));
        $select->from(array('p'=> 'channel_ka'), $arrCols);
        $select->where("p.id = ?", $channel_id);
        $result = $db->fetchRow($select);
        return $result;
    }
    
}                                                      
