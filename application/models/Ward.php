<?php

/**
 * Created by PhpStorm.
 * User: Hac
 * Date: 1/5/2016
 * Time: 9:39 AM
 */
class Application_Model_Ward extends Zend_Db_Table_Abstract {

    protected $_name = 'ward';

    public function getWardByDistrict($district_id) {

        if (!$district_id) {
            return false;
        }

        $db     = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('a' => $this->_name), array('a.*'))
                ->where('a.district_id = ?', $district_id)
                ->where('a.code_insurance IS NOT NULL')
                ->where('a.is_hidden <> 1');

        $rows   = $db->fetchAll($select);
        $result = array();
        foreach ($rows as $row) {
            $result[$row['id']] = str_pad($row['code_insurance'], 5, 0, STR_PAD_LEFT) . ' - ' . $row['name'];
        }

        return $result;
    }
    
    public function getWardByDistrictAll($district_id) {

        if (!$district_id) {
            return false;
        }

        $db     = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('a' => $this->_name), array('a.*'))
                ->where('a.district_id = ?', $district_id)
                ->where('a.code_insurance IS NOT NULL');

        $rows   = $db->fetchAll($select);
        $result = array();
        foreach ($rows as $row) {
            $result[$row['id']] = str_pad($row['code_insurance'], 5, 0, STR_PAD_LEFT) . ' - ' . $row['name'];
        }

        return $result;
    }

    function get_cache() {
        $cache  = Zend_Registry::get('cache');
        $result = $cache->load($this->_name . '_cache');

        if ($result === false) {

            $data = $this->fetchAll(null, 'name');

            $result = array();
            if ($data) {
                foreach ($data as $item) {
                    $result[$item->id] = str_pad($item->code, 3, 0, STR_PAD_LEFT) . ' - ' . $item->name;
                }
            }
            $cache->save($result, $this->_name . '_cache', array(), null);
        }
        return $result;
    }
    function get_cache2() {
        $cache  = Zend_Registry::get('cache');
        $result = $cache->load($this->_name . '_cache2');

        if ($result === false) {

            $data = $this->fetchAll(null, 'name');

            $result = array();
            if ($data) {
                foreach ($data as $item) {
                    $result[$item->id] = $item->name;
                }
            }
            $cache->save($result, $this->_name . '_cache2', array(), null);
        }
        return $result;
    }

    public function nget_ward_delivery_by_district($district_delivery = null) {
        $db = Zend_Registry::get('db');

        if (!empty($district_delivery)) {

            $select      = $db->select()->from(array('p' => "ward_delivery"), array(new Zend_Db_Expr
                        ('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));
            $select->where('p.del = 0', null);
            $select->where('p.district_id IN (?)', $district_delivery);
            $select->order('p.ward_name', 'COLLATE utf8_unicode_ci ASC');
            $result_temp = $db->fetchAll($select);

            if (!empty($result_temp)) {
                foreach ($result_temp as $key => $value) {
                    $result[$value['id']] = $value['ward_name'];
                }
            }

            return $result;
        } else {
            return null;
        }
    }

    function get_cache_ward_delivery() {
        $cache  = Zend_Registry::get('cache');
        $result = $cache->load('cache_ward_delivery');
        if ($result === false) {

            $db = Zend_Registry::get('db');

            $select = $db->select()
                    ->from(array('p' => 'ward_delivery'), array('p.id', 'p.ward_name'));
            $select->order(new Zend_Db_Expr('p.`ward_name` COLLATE utf8_unicode_ci'));
          
            $data = $db->query($select->__toString());
          
            $result = array();
            if ($data) {
                foreach ($data as $item) {
                    $result[$item['id']] = $item['ward_name'];
                }
            }
           
            $cache->save($result, 'cache_ward_delivery', array(), null);
        }
        return $result;
    }

}
