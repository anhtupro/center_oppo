<?php
class Application_Model_UtilitiesTool extends Zend_Db_Table_Abstract
{
	public function UpdateRegionalMarket($regional_id){
		$db			= Zend_Registry::get('db');
		$db->query('
			CREATE TABLE IF NOT EXISTS `backup`.regional_market_'.date('d_m_Y_H_i_s').' LIKE hr.regional_market;
			CREATE TABLE IF NOT EXISTS `backup`.distributor_'.date('d_m_Y_H_i_s').' LIKE warehouse.distributor;
			CREATE TABLE IF NOT EXISTS `backup`.staff_'.date('d_m_Y_H_i_s').' LIKE hr.staff;
		');
		$stmt = $db->prepare('Call SP_Update_Regional_Market(:regional_id)');
		$stmt->bindParam("regional_id", $regional_id, PDO::PARAM_INT);
		$res = $stmt->execute();
		
		$stmt->closeCursor();
		$db			= $stmt = null;
		return $res;

	}
}