<?php
class Application_Model_MiniGame2018Detail extends Zend_Db_Table_Abstract
{
	protected $_name = 'mini_game_2018_detail';

	public function getisComplete($params){
		$db = Zend_Registry::get("db");
        $select = $db->select();

       $arrCols = array(
            'p.question_id'
        );
        $select->from(array('p' => 'mini_game_2018_detail'), $arrCols);        
        $select->where('p.master_id = ?', $params['id_master']);
        $select->where('p.round = ?', $params['round']);
        $result = $db->fetchAll($select);
        //echo $select; exit;
        return $result;
	}

    public function getTimeStartQuestion($params){
		$db = Zend_Registry::get("db");
        $select = $db->select();

       $arrCols = array(
            'p.started_at'
        );
        $select->from(array('p' => 'mini_game_2018_detail'), $arrCols);        
        $select->where('p.master_id = ?', $params['id_master']);
        $select->where('p.question_id = ?', $params['question_id']);
        $select->where('p.round = ?', $params['round']);
        $result = $db->fetchAll($select);
        //echo $select; exit;
        return $result;
	}
}
