<?php
class Application_Model_RequestOfficeDetails extends Zend_Db_Table_Abstract
{
    protected $_name = 'request_office_details';
    
    function getListDetails($id) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.request_id',
            'p.project',
            'p.request_type_group',
            'p.request_type',
            'p.cost',
            'p.currency',
            'p.del',
            "year_month" => "GROUP_CONCAT(CONCAT(m.year,'-',m.month))"
        );

        $select->from(array('p' => 'request_office_details'), $arrCols);
        $select->joinLeft(array('m' => 'request_office_details_month'), 'm.details_id = p.id', array());
        $select->where('p.del = 0');
        
        $select->where('p.request_id = ?', $id);
        $select->group('p.id');
        $result = $db->fetchAll($select);

        return $result;
    }
}