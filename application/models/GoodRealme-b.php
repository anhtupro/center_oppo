<?php
class Application_Model_GoodRealme extends Zend_Db_Table_Abstract
{
    protected $_name = 'good';
    protected $_schema = WAREHOUSE_DB_RM;

    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => WAREHOUSE_DB_RM.'.'.$this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        if (isset($params['name']) and $params['name'])
            $select->where('p.desc LIKE ?', '%'.$params['name'].'%');

        $select->order('p.desc', 'COLLATE utf8_unicode_ci ASC');

        $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load(WAREHOUSE_DB_RM.'_'.$this->_name.'_cache');

        if (!$result) {

            $where = array();
            $where[] = $this->getAdapter()->quoteInto('cat_id = ?', PHONE_CAT_ID);
            $where[] = $this->getAdapter()->quoteInto('del = ?', 0);

            $data = $this->fetchAll($where, 'desc');

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->desc;
                }
            }
            $cache->save($result, WAREHOUSE_DB_RM.'_'.$this->_name.'_cache', array(), null);
        }
        return $result;
    }
    function get_cache_color(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load(WAREHOUSE_DB_RM.'_good_color_cache__');

        if (!$result) {

            $where = array();
            $where[] = $this->getAdapter()->quoteInto('cat_id = ?', PHONE_CAT_ID);
            $where[] = $this->getAdapter()->quoteInto('del = ?', 0);

            $data = $this->fetchAll($where, 'desc');

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->desc;
                }
            }
            $cache->save($result, WAREHOUSE_DB_RM.'_good_color_cache__', array(), null);
        }
        return $result;
    }


    function get_color_cache_all($id_cat){

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'id' => "p.id",
            'name' => "p.name"
        );

        $select->from(array('p' => WAREHOUSE_DB_RM.'.good_color'), $arrCols);

        $result = $db->fetchAll($select);

        $data = [];
        foreach($result as $key=>$value){
            $data[$value['id']] = $value['name'];
        }

        return $data;
    }

    function get_cache2(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load(WAREHOUSE_DB_RM.'_'.$this->_name.'_cache2');

        if (!$result) {

            $where = array();
            $where[] = $this->getAdapter()->quoteInto('cat_id = ?', PHONE_CAT_ID);
            $where[] = $this->getAdapter()->quoteInto('del = ?', 0);

            $data = $this->fetchAll($where, 'desc');

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item;
                }
            }
            $cache->save($result, WAREHOUSE_DB_RM.'_'.$this->_name.'_cache2', array(), null);
        }
        return $result;
    }

    function get_cat_model_color_cache()
    {
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load(WAREHOUSE_DB_RM.'_'.$this->_name.'_cat_model_color_cache');

        if (!$result) {

            $db = Zend_Registry::get('db');
            $select = $db->select()
                ->distinct()
                ->from(array('p' => WAREHOUSE_DB_RM.'.'.$this->_name), array('product_id' => 'p.id', 'product_name' => 'p.name', 'product_desc' => 'p.desc'))
                ->join(array('c' => WAREHOUSE_DB_RM.'.'.'good_category'), 'p.cat_id=c.id', array('cat_id' => 'c.id', 'cat_name' => 'c.name'))
                ->join(array('cc' => WAREHOUSE_DB_RM.'.'.'good_color_combined'), 'p.id=cc.good_id', array())
                ->join(array('gc' => WAREHOUSE_DB_RM.'.'.'good_color'), 'cc.good_color_id=gc.id', array('color_id' => 'gc.id','color_name' => 'gc.name'));

            $data = $db->fetchAll($select);

            $result = array();

            if ($data) {
                foreach ($data as $key => $value) {
                    if ( !isset($result[ $value['cat_id'] ]) )
                        $result[ $value['cat_id'] ] = array(
                            'name' => $value['cat_name'],
                            'children' => array(),
                        );

                    if ( ! isset( $result[ $value['cat_id'] ]
                        ['children']
                        [ $value['product_id'] ] ) )
                        $result[ $value['cat_id'] ]
                        ['children']
                        [ $value['product_id'] ] = array(
                            'name' => $value['product_name'],
                            'desc' => $value['product_desc'],
                            'children' => array(),
                        );

                    if ( ! isset( $result[ $value['cat_id'] ]
                        ['children']
                        [ $value['product_id'] ]
                        ['children']
                        [ $value['color_id'] ] ) )
                        $result[ $value['cat_id'] ]
                        ['children']
                        [ $value['product_id'] ]
                        ['children']
                        [ $value['color_id'] ] = array(
                            'name' => $value['color_name']
                        );
                }
            }

            $cache->save($result, WAREHOUSE_DB_RM.'_'.$this->_name.'_cat_model_color_cache', array(), null);
        }
        return $result;
    }

    function getColor($good_id){
        $db = Zend_Registry::get('db');
    }

    function get_cache_access(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load(WAREHOUSE_DB_RM.'_'.$this->_name.'_cache_access');

        if (!$result) {

            $where = array();
            $where[] = $this->getAdapter()->quoteInto('cat_id = ?', ACCESS_CAT_ID);
            $where[] = $this->getAdapter()->quoteInto('del = ?', 0);

            $data = $this->fetchAll($where, 'desc');

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->name;
                }
            }
            $cache->save($result, WAREHOUSE_DB_RM.'_'.$this->_name.'_cache_access', array(), null);
        }
        return $result;
    }

    function get_all(){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => WAREHOUSE_DB_RM.'.'.$this->_name),array('p.*'));
        $result = $db->fetchAll($select);
        $data = array();
        foreach($result as $item){
            $data[$item['id']] = $item;
        }

        return $data;
    }

    function get_cache_all($id_cat){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load(WAREHOUSE_DB_RM.'_'.$this->_name.'_cache_all'.$id_cat);

        if (!$result) {

            $where = array();
            if (isset($id_cat) and $id_cat)
                $where[] = $this->getAdapter()->quoteInto('cat_id = ?', $id_cat);
            $where[] = $this->getAdapter()->quoteInto('del = ?', 0);
            $data = $this->fetchAll($where, 'desc');

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->desc.' -  '.$item->name;
                }
            }
            $cache->save($result, WAREHOUSE_DB_RM.'_'.$this->_name.'_cache_all'.$id_cat, array(), null);
        }
        return $result;
    }
}
