<?php

class Application_Model_StoreCategoryDeployment extends Zend_Db_Table_Abstract
{
    protected $_name = 'store_category_deployment';
    protected $_schema = DATABASE_TRADE;

    public function get($store_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['s' => DATABASE_TRADE.'.store_category_deployment'], [
                       's.*'
                     ])
        ->where('s.store_id = ?', $store_id);

        $result = $db->fetchAll($select);

        return $result;
    }
}