<?php

class Application_Model_RecheckShopFile extends Zend_Db_Table_Abstract
{
    protected $_name = 'recheck_shop_file';
    protected $_schema = DATABASE_TRADE;

    public function get($recheckShopId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['f' => DATABASE_TRADE . '.recheck_shop_file'], [
                'url' => 'f.url',
                'type' => 'f.type'
            ])
            ->where('f.recheck_shop_id = ?', $recheckShopId)
            ->where('f.type IN (?)', [0, 1, 2])
            ->where('f.is_deleted = ?', 0);

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getImgDifferent($recheckShopId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.url',
            'p.type'
        );
        $select->from(array('p' => DATABASE_TRADE . '.recheck_shop_file'), $arrCols);
        $select->where('p.recheck_shop_id = ?', $recheckShopId);
        $select->where('p.type NOT IN (?)', array(0, 1, 2));
        $select->order('p.type ASC');
        $result = $db->fetchAll($select);
        return $result;
    }
}    