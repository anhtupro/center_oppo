<?php
class Application_Model_InformTitle extends Zend_Db_Table_Abstract
{
    protected $_name = 'inform_title';

    function check_view($notification_id)
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
            $staff_id   = $userStorage->id;
            $db         = Zend_Registry::get('db');
            $select = $db->select();
            $select_fields = array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'), 'p.*' );

                $select_filter = array(
                    'c_area'       => new Zend_Db_Expr(sprintf('SUM(CASE WHEN o.object_id = a.id AND o.type = %d THEN 1 ELSE 0 END)', My_Notification::AREA)),
                    'c_title'      => new Zend_Db_Expr(sprintf('SUM(CASE WHEN o.object_id = s.title AND o.type = %d THEN 1 ELSE 0 END)', My_Notification::TITLE))
                );
            $select
                ->from(array('p' => 'inform'),
                    $select_fields
                )
                ->joinLeft(array('o' => 'inform_title'), 'o.id = p.id', $select_filter)
                ->joinLeft(array('s' => 'staff'), "1 = 1", array())
                ->join(array('r' => 'regional_market'), 'r.id=s.regional_market', array())
                ->join(array('a' => 'area'), 'a.id=r.area_id', array());
            $select ->where('s.id = ?',$staff_id);
			 $select ->where('p.id = ?',$notification_id);
            $select ->where('p.status = 1');
            $select->group('p.category_id');
            $select ->having('(c_area > ? AND c_title > ?)', 0);
            $result = $db->fetchAll($select);
            return $result;
    }
}