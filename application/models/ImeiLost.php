<?php 
class Application_Model_ImeiLost extends Zend_Db_Table_Abstract{
	protected $_name = 'imei_lost';


	public function fetchPagination($page, $limit, &$total, $params){
		$db = Zend_Registry::get('db');
		$cols = array(
				new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.id'),
				'a.imei_sn',
				'good_name'       => 'c.name',
				'good_color_name' => 'd.name',
				'type_group'      => 'GROUP_CONCAT(a.type)',
				'dismiss'         => 'MIN(dismiss)',
				'lasted'          => 'MAX(a.created_at)'
			);
		$select = $db->select()
					->from(array('a'=>'imei_lost'),$cols)
					->joinLeft(array('b'=>WAREHOUSE_DB.'.warehouse'),'b.id = a.warehouse_id',array())
					->joinLeft(array('c'=>WAREHOUSE_DB.'.good'),'c.id = a.good_id',array())
					->joinLeft(array('d'=>WAREHOUSE_DB.'.good_color'),'d.id = a.good_color',array())
					;
		if(isset($params['type']) AND $params['type']){
			$select->where('a.type = ?',$params['type']);
		}

		if(isset($params['imei_sn']) AND $params['imei_sn']){
			$select->where('a.imei_sn = ?',$params['imei_sn']);
		}

		if(isset($params['dismiss']) AND $params['dismiss'] >= 0){
			$select->where('a.dismiss = ?',$params['dismiss']);
		}

		if(isset($params['good_id']) AND $params['good_id']){
			$select->where('a.good_id = ?',$params['good_id']);
		}

		$select->group('a.imei_sn');

		if( isset($params['sort']) AND $params['sort']){

		}else{
			$select->order('MAX(created_at) DESC');
		}
		
		if(isset($limit) AND $limit){
            $select->limitPage($page,$limit);
        }
		$result = $db->fetchAll($select);

		$total = $db->fetchOne("select FOUND_ROWS()");

		return $result;
	}

	public function dismiss($imei_sn){
		if(!$imei_sn){
			return array('code'=>-1,'message'=>'Please select a imei');
		}

		try {
			$where = $this->getAdapter()->quoteInto('imei_sn = ?',$imei_sn);
			$this->update(array('dismiss'=>1),$where);
			return array('code'=>1,'message'=>'Done');
		} catch (Exception $e) {
			return array('code'=>-1,'message'=>$e->getMessage());
		}
	}

	public function detail($imei_sn){
		$db = Zend_Registry::get('db');
		$imei_sn = trim($imei_sn);
		if(!$imei_sn){
			return array('code'=>-1,'message'=>'Please select a imei');
		}

		$cols = array(
				'a.*',
				'warehouse_name' => 'b.name',
				'good_name'      => 'c.name',
				'color_name'     => 'd.name',
				'store_name'     => 'e.name',
				'staff_name'     => 'CONCAT(f.firstname," ",f.lastname)'
			);
		$select = $db->select()
			->from(array('a'=>'imei_lost'),$cols)
			->joinLeft(array('b'=>WAREHOUSE_DB.'.warehouse'),'b.id = a.warehouse_id',array())
			->joinLeft(array('c'=>WAREHOUSE_DB.'.good'),'c.id = a.good_id',array())
			->joinLeft(array('d'=>WAREHOUSE_DB.'.good_color'),'d.id = a.good_color',array())
			->joinLeft(array('e'=>'store'),'e.id = a.store_id',array())
			->joinLeft(array('f'=>'staff'),'f.id = a.staff_id',array())
			->where('a.imei_sn = ?',$imei_sn)
			->order('a.created_at ASC')
			;
		$result = $db->fetchAll($select);
		return $result;
	}

	public function checkImeiLostTiming($imei){
		$db = Zend_Registry::get('db');
		$select = $db->select()
			->from(array('a'=>WAREHOUSE_DB.'.imei'),array('a.*'))
			->where('a.distributor_id IS NULL OR a.distributor_id = ?',0)
			->where('a.return_sn IS NULL')
			->where('a.imei_sn = ?',trim($imei))
			;
		$row = $db->fetchRow($select);
		$userStorage = Zend_Auth::getInstance()->getStorage()->read();
		if($row){
			$db->beginTransaction();
            try {
            	$data = array(
						'imei_sn'        => trim($imei),
						'type'           => IMEI_LOST_IMEI_CHECK,
						'staff_id'       => $userStorage->id,
						'created_at'     => date('Y-m-d H:i:s'),
						'warehouse_id'   => $row['warehouse_id'],
						'warehouse_type' => 1,
						'good_id'        => $row['good_id'],
						'good_color'     => $row['good_color'],
						'activated_date' => $row['activated_date']
            		);
            	$selectExist = $db->select()
            		->from(array('a'=>$this->_name),array('a.*'))
            		->where('a.imei_sn = ?',$imei)
            		->where('a.type = ?',IMEI_LOST_IMEI_CHECK);
            	$rowExist = $db->fetchRow($selectExist);
            	if(!$rowExist){
            		$this->insert($data);	
            	}
            	$db->commit();
            } catch (Exception $e) {
            	$db->rollBack();
            }
		}
	}

	/**
	 * chạy crobtab
	 */
	public function checkImeiFromCs(){
		$db  = Zend_Registry::get('db');
		error_reporting(1);
        require_once 'My'.DIRECTORY_SEPARATOR.'nusoap'.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'nusoap.php';
        $this->wssURI = 'http://cs.opposhop.vn/wss?wsdl';
        $this->namespace = 'OPPOVN';

		$client = new nusoap_client($this->wssURI);
        $client->soap_defencoding = 'UTF-8';
        $client->decode_utf8 = false;
        $wsParams = array(
          	'type' => 1
        );

        try {

            $result = $client->call("getListIMEIInCS", $wsParams);    
            
            $imei_list = array();
            foreach($result as $key => $value){
            	$imei_list[] = '"'.$value['imei_sn'].'"';
            }
            
            $sql_params = implode(',', array_unique($imei_list));
            $sql = '
            	INSERT INTO imei_lost(imei_sn,type,created_at,warehouse_id, warehouse_type,activated_date,good_id, good_color, time_check_in)
				SELECT 
					a.imei_sn,
					'.IMEI_LOST_WARRANTY.',
					NOW(),
					a.warehouse_id,
					1 as warehouse_type,
					a.activated_date,
					a.good_id,
					a.good_color,
					NOW()
				FROM '.WAREHOUSE_DB.'.imei a 
				LEFT JOIN imei_lost b ON b.imei_sn = a.imei_sn 
					AND b.type = '.IMEI_LOST_WARRANTY.'
				WHERE  (a.return_sn IS NULL OR a.return_sn = "")
					AND a.activated_date IS NOT NULL
					AND (a.out_date IS NULL)
					AND a.imei_sn IN ('.$sql_params.')
					AND b.id IS NULL
					AND DATE(a.activated_date) >= "'.IMEI_LOST_START.'"
            ';

            $stmt = $db->query($sql);
            $stmt->execute();
			
        } catch (Exception $e) {
        	return array('code'=>-1,'message'=>$e->getMessage());
            var_dump($e);
            exit;
        }
        return array('code'=>1,'message'=>'Done');

	}

	public function checkImeiCustomerFromCs(){
		$db  = Zend_Registry::get('db');
		error_reporting(1);
        require_once 'My'.DIRECTORY_SEPARATOR.'nusoap'.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'nusoap.php';
        $this->wssURI = 'http://cs.opposhop.vn/wss?wsdl';
        $this->namespace = 'OPPOVN';

		$client = new nusoap_client($this->wssURI);
        $client->soap_defencoding = 'UTF-8';
        $client->decode_utf8 = false;
        $wsParams = array(
          	'type' => 2
        );

        try {

            $result = $client->call("getListIMEIInCS", $wsParams);    
            //return $result;
            $imei_list = array();
            foreach($result as $key => $value){
            	$imei_list[] = '"'.$value['imei_sn'].'"';
            }
            
            $sql_params = implode(',', array_unique($imei_list));
            $sql = '
            	INSERT INTO imei_lost(imei_sn,type,created_at,warehouse_id, warehouse_type,activated_date,good_id, good_color, time_check_in)
				SELECT 
					a.imei_sn,
					'.IMEI_LOST_CUSTOMER.',
					NOW(),
					a.warehouse_id,
					1 as warehouse_type,
					a.activated_date,
					a.good_id,
					a.good_color,
					NOW()
				FROM '.WAREHOUSE_DB.'.imei a 
				LEFT JOIN imei_lost b ON b.imei_sn = a.imei_sn 
					AND b.type = '.IMEI_LOST_CUSTOMER.'
				WHERE (a.distributor_id IS NULL OR a.distributor_id = 0)
					AND (a.return_sn IS NULL OR a.return_sn = "")
					AND (a.out_date IS NULL)
					AND a.imei_sn IN ('.$sql_params.')
					AND b.id IS NULL';
            $stmt = $db->query($sql);
            $stmt->execute();
			
        } catch (Exception $e) {
        	return array('code'=>-1,'message'=>$e->getMessage());
            var_dump($e);
            exit;
        }
        return array('code'=>1,'message'=>'Done');

	}
}