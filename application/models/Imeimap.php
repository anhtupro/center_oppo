<?php

class Application_Model_Imeimap extends Zend_Db_Table_Abstract{

	public function GetAll(){
		$db = Zend_Registry::get('db');
		$stmt = $db->prepare('
			SELECT dso.title dealer_sell_out_name, dsi.title dealer_sell_in_name, im.status status, im.id id
			FROM imei_map im
			JOIN warehouse.distributor dso ON im.distributor_id = dso.id
			JOIN warehouse.distributor dsi ON im.get_imei_from = dsi.id
		');
		$stmt->execute();
		$res = $stmt->fetchAll();
		$stmt->closeCursor();
		$db = $stmt = null;
		return $res;
	}



	public function CheckImei($pars = array()){
		$db = Zend_Registry::get('db');
		$stmt = $db->prepare('Call SP_imei_map_CheckImei(:imei, :store_id)');
		$stmt->bindParam('imei', $pars['imei'], PDO::PARAM_INT);
		$stmt->bindParam('store_id', $pars['store_id'], PDO::PARAM_INT);
		$stmt->execute();
		$res = $stmt->fetchAll();
		$stmt->closeCursor();
		$db = $stmt = null;
		return $res;
	}

	public function updateImeimap($params = array())
	{
		$db = Zend_Registry::get('db');

		$sql = "UPDATE `imei_map`
				SET status = :status
				WHERE id = :id";

		$stmt = $db->prepare($sql);
		$stmt->bindParam('status', $params['status'], PDO::PARAM_INT);
		$stmt->bindParam('id', $params['id'], PDO::PARAM_INT);
		$res = $stmt->execute();
		$stmt->closeCursor();
		$stmt = $db = null;
		return $res;
	}

	public function Insert(){
		$db = Zend_Registry::get('db');
		$stmt = $db->prepare();
		$res = $stmt->execute();
		$stmt->closeCursor();
		$db = $stmt = null;
		return $res;
	}

	public function GetWarehouseDistributor($params = array())
	{
		$db = Zend_Registry::get('db');

		if(!empty($params['name']))
		{
			$name = '%' . $params['name'] . '%';
			$sql = "SELECT 
						d.id as `id`,
						d.title as `text`
					FROM `warehouse`.`distributor` d
					WHERE d.title LIKE :name ";

			$stmt  = $db->prepare($sql);
			$stmt->bindParam('name', $name, PDO::PARAM_STR);

			$stmt->execute();
			$data = $stmt->fetchAll();
			$stmt->closeCursor();
			$stmt = $db = null;

			return $data;
		}

		return array();
	}

	public function insertImeimap($params = array())
	{
		$db = Zend_Registry::get('db');

		$sql = "INSERT INTO `imei_map` (`distributor_id`, `get_imei_from`, `status`)
					VALUES (:distributor_id, :get_imei_from, :status)
				ON DUPLICATE KEY UPDATE `status` = VALUES(`status`)";

		$stmt = $db->prepare($sql);
		$stmt->bindParam('distributor_id', $params['distributor_id'], PDO::PARAM_STR);
		$stmt->bindParam('get_imei_from', $params['get_imei_from'], PDO::PARAM_STR);
		$stmt->bindParam('status', $params['status'], PDO::PARAM_INT);

		$res = $stmt->execute();
		$stmt->closeCursor();
		$stmt = $db = null;
		return $res;
	}

}