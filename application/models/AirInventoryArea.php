<?php

class Application_Model_AirInventoryArea extends Zend_Db_Table_Abstract
{
    protected $_name = 'inventory_area';
    protected $_schema = DATABASE_TRADE;

    public function fetchPagination($page, $limit, &$total, $params)
    {

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $columns = [
            'id' => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS i.id'),
            'category_id' => "i.category_id",
            'category_name' => "c.name",
            'quantity' => "i.quantity",
            'area_name' => "r.name",
            'area_id' => "i.area_id"
        ];

        $select->from(array('i' => DATABASE_TRADE . '.inventory_area'), $columns);
        $select->joinLeft(array('c' => DATABASE_TRADE . '.category'), 'c.id = i.category_id', array());
        $select->joinLeft(array('r' => 'area'), 'r.id = i.area_id', array());

        if ($params['area_id']) {
            $select->where('i.area_id = ?', $params['area_id']);
        }

        if ($params['category_id']) {
            $select->where('i.category_id = ?', $params['category_id']);
        }

        if (!$params['export']) {
            $select->limitPage($page, $limit);
        }

        $select->order('i.id');

        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    public function export($data)
    {
        include_once 'PHPExcel.php';

        $objExcel = new PHPExcel();
        $objExcel->setActiveSheetIndex(0);
        $sheet = $objExcel->getActiveSheet();

        $sheet->setCellValue('A1', 'Stt');
        $sheet->setCellValue('B1', 'Khu vực');
        $sheet->setCellValue('C1', 'Tên hạng mục');
        $sheet->setCellValue('D1', 'Số lượng');

        // insert data to excel
        $rowCount = 2;
        $index = 1;
        foreach ($data as $item) {
            $sheet->setCellValue('A' . $rowCount, $index);
            $sheet->setCellValue('B' . $rowCount, $item['area_name']);
            $sheet->setCellValue('C' . $rowCount, $item['category_name']);
            $sheet->setCellValue('D' . $rowCount, $item['quantity']);

            ++$rowCount;
            ++$index;
        }

        //style sheet
        $style_border = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $sheet->getStyle('A0:D' . $rowCount)->applyFromArray($style_border);

        for ($i = 'A'; $i < 'E'; ++$i) {
            $sheet->getStyle($i . 1)->getFont()->setBold(true);
        }

        //download
        $filename = 'Quản lý kho';
        $objWriter = new PHPExcel_Writer_Excel2007($objExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }

    public function getExistedInventory($categoryId, $areaId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['i' => DATABASE_TRADE . '.inventory_area'], ['i.*'])
            ->where('i.category_id = ?', $categoryId)
            ->where('i.area_id = ?', $areaId);
        $result = $db->fetchRow($select);

        return $result ? $result : false;
    }

    public function getInfoWarehouse($areaId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['i' => DATABASE_TRADE . '.app_checkshop_detail_child'], [
                'category_name' => 'c.name',
                'quantity' => '(i.quantity)',
                'category_id' => 'i.category_id',
                'area_id' => 'i.area_id',
//                'inventory_area_imei_id' => 'm.id',
                'app_checkshop_detail_child_id' => 'i.id',
                'category_type_name' => 't.name',
                'i.height',
                'i.width',
                'c.has_type',
                'c.has_width',
                'c.has_painting',
                'c.has_material',
                'i.in_processing',
                'material_name' => 'm.name'

            ])
            ->joinLeft(['t' => DATABASE_TRADE.'.category_inventory_type'], 'i.category_type = t.id', [])
            ->joinLeft(['c' => DATABASE_TRADE . '.category'], 'i.category_id = c.id', [])
            ->joinLeft(['m' => DATABASE_TRADE.'.category_material'], 'i.material_id = m.id', [])
            ->where('c.group_bi IN (?)', [1,2,3,4,6])
            ->where('i.area_id = ?', $areaId);

        $select->where('i.quantity > 0');
//        ->group(['m.category_id', 'm.category_type', 'm.width', 'm.height']);

        $result = $db->fetchAll($select);

        return $result;
    }
    public function getInfoWarehouseTotal($areaId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['i' => DATABASE_TRADE . '.app_checkshop_detail_child'], [
                'category_name' => 'c.name',
                'quantity' => 'SUM(i.quantity)',
                'category_id' => 'i.category_id',
                'area_id' => 'i.area_id',
//                'inventory_area_imei_id' => 'm.id',
                'app_checkshop_detail_child_id' => 'i.id',
                'category_type_name' => 't.name',
                'i.height',
                'i.width',
                'c.has_type',
                'c.has_width',
                'c.has_painting',
                'c.has_material',
                'i.in_processing',
                'material_name' => 'm.name'

            ])
            ->joinLeft(['t' => DATABASE_TRADE.'.category_inventory_type'], 'i.category_type = t.id', [])
            ->joinLeft(['c' => DATABASE_TRADE . '.category'], 'i.category_id = c.id', [])
            ->joinLeft(['m' => DATABASE_TRADE.'.category_material'], 'i.material_id = m.id', [])
            ->where('c.group_bi IN (?)', [1,2,3,4,6])
            ->where('i.area_id = ?', $areaId)
            ->group('c.id');

        $select->where('i.quantity > 0');
//        ->group(['m.category_id', 'm.category_type', 'm.width', 'm.height']);

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getCategory($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['o' => DATABASE_TRADE.'.inventory_area_imei'], [
                'category_id' => 'o.category_id',
                'category_name' => 'c.name',
                'quantity' => 'o.quantity',
                'o.imei_sn',
                'group_bi' => 'c.group_bi',
                'c.has_type',
                'c.has_width',
                'o.height',
                'o.width',
                'o.category_type',
                'category_type_name' => 'i.name',
                'c.has_material',
                'c.has_painting'
            ])
            ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'o.category_id = c.id', [])
        ->joinLeft(['i' => DATABASE_TRADE.'.category_inventory_type'], 'o.category_type = i.id', [])
        ->where('o.quantity > 0');
//            ->group(['o.area_id', 'i.category_id']);

        if($params['area_id']) {
            $select->where('o.area_id = ?', $params['area_id']);
        }

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getCategoryForView($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['i' => DATABASE_TRADE.'.inventory_area'], [
                         'category_id' => 'i.category_id',
                         'category_name' => 'c.name',
                         'quantity' => 'i.quantity',
                         'c.has_type',
                         'c.has_width'
                     ])
            ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'i.category_id = c.id', [])
        ->where('i.quantity > 0');

        if ($params['area_id']) {
            $select->where('i.area_id = ?', $params['area_id']);
        }

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getCategoryDetail($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['m' => DATABASE_TRADE.'.inventory_area_imei'], [
                         'm.*',
                         'category_type_name' => 't.name'
                     ])
        ->joinLeft(['t' => DATABASE_TRADE.'.category_inventory_type'], 'm.category_type = t.id', [])
        ->where('m.quantity > 0');

        if ($params['area_id']) {
            $select->where('m.area_id = ?', $params['area_id']);
        }

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $listCategoryDetail [$element['category_id']] [] = $element;
        }

        return $listCategoryDetail;
    

    }


}