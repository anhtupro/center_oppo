<?php

class Application_Model_CheckShopEntirePointTotal extends Zend_Db_Table_Abstract
{
    protected $_name = 'check_shop_entire_point_total';
    protected $_schema = DATABASE_TRADE;

    public function get($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['p' => DATABASE_TRADE.'.check_shop_entire_point_total'], [
                        'p.*'
                     ])
        ->where('p.check_shop_id = ?', $params['check_shop_id']);

        $result = $db->fetchRow($select);

        return $result;

    }
}