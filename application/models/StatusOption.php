<?php
class Application_Model_StatusOption extends Zend_Db_Table_Abstract{
	protected $_name = 'status_option';

	public function get_all(){
		$db = Zend_Registry::get('db');
		$select = $db->select()
			->from(array('p'=>$this->_name),array('p.id','p.name'));
		$result = $db->fetchPairs($select);
		return $result;
	}
}