<?php

class Application_Model_Tree extends Zend_Db_Table_Abstract {

    protected $_name = "tree";
    
    public function fetchOneData($id_flag) {
        $db = Zend_Registry::get('db');
        $select = $db->select()
        ->from(array('fot' => $this->_name),array("fot.name","fot.content"))
        ->where("fot.id = ? ", $id_flag);
        $result = $db->fetchRow($select);
        return $result;
    }
    
    public function updateContent($tree_id , $content , $created_time , $created_by){
        $db = Zend_Registry::get('db');
        $data = array(
            'content' => $content,
            'created_by' => $created_by,
            'created_at' => $created_time,
        );
        $where = array('id = ?' => $tree_id);
        return $db->update($this->_name, $data, $where);
    }
    
}
