<?php

class Application_Model_TotalTimeByMonth extends Zend_Db_Table_Abstract {

    protected $_name = 'total_time_by_month';

    function fetchPagination($page, $limit, &$total, $params) {
        $db = Zend_Registry::get('db');

        $select = $db->select()
                ->from(array('p' => $this->_name), array( 'p.id',
                    'p.staff_code','p.name','p.identity','p.title','p.area','p.company','p.company_group','p.department','p.team','p.policy_group'
                ,'p.join_date','p.off_date','p.congthuong','p.created_at','p.created_by','p.total_after','p.total_before','p.month','p.year','p.sunday','p.saturday','p.holidayX3','p.holidayX4'
,'p.congthuong','p.congthuongtinhluong','p.phep','p.ungphep','p.nghiviecrienghuongluong','p.editing_date','p.nghiviechuongluong'));

        if (isset($params['year']) and $params['year'])
//            $select->where("p.year = ?", $params['year']);
        if (isset($params['month']) and $params['month'])
            $select->where("p.month = ?", $params['month']);
        $result = $db->fetchAll($select);
        return $result;
    }

}
