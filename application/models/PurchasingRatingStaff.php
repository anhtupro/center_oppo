<?php

class Application_Model_PurchasingRatingStaff extends Zend_Db_Table_Abstract{
    protected $_name = "purchasing_rating_staff";    
    
    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(
                    array('p'=>'purchasing_rating_staff'),
                    array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
                            'p.point',
                            'p.created_at', 
                            'p.created_by',
                            'p.note',
                            'created_by_staff'  => "CONCAT(t1.firstname,  ' ',t1.lastname)",
                            'rating_staff_name' => "CONCAT(t2.firstname,  ' ',t2.lastname)",
                            'rating_time_name'  => 'pt.name',
                            'point_details'     => "GROUP_CONCAT(CONCAT(pd.survey_id, '-', pd.point))",  
                            'department_name'   => 'tm.name',
                         
                        ))  
            ->joinLeft(array('pd'   =>'purchasing_rating_staff_detail'),'pd.id_rating_staff = p.id',array())
            ->joinLeft(array('rd'   =>'purchasing_rating_survey_detail'),'rd.id = pd.survey_id',array())
            ->joinLeft(array('pt'   =>'purchasing_rating_times'),'pt.id = p.id_rating_times',array())
            ->joinLeft(array('t1'   =>'staff'),'t1.id = p.created_by',array())
            ->joinLeft(array('t2'   =>'staff'),'t2.id = p.staff_id',array())
            ->joinLeft(array('tm'   =>'team'),'tm.id = t1.department',array())
            ->group('p.id')
            ->order('p.id DESC');
        if(!empty($params['id_rating_times'])){
            $select->where('p.id_rating_times = ?', $params['id_rating_times']);
        }
        $result = $db->fetchAll($select);
        if ($limit) {
            $total = $db->fetchOne("select FOUND_ROWS()");
        }
        return $result;
    }
    
    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');
        
        if ($result === false) {
            
            $db = Zend_Registry::get('db');
            $select = $db->select()
                    ->from(array('p' => 'staff'), array('p.id', 'name' => "CONCAT(p.firstname,  ' ',p.lastname)"));
            $select->where('p.off_date IS NULL');
            $select->where('p.department = ?', 400);
            $data = $db->fetchAll($select);
            $result = array(); 
           
            if ($data){
                foreach ($data as $k => $item){
                    $result[$item['id']] = $item['name'];                    
                }
            }
            
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }
   
}

