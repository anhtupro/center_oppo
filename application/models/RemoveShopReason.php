<?php
class Application_Model_RemoveShopReason extends Zend_Db_Table_Abstract
{
    protected $_name = 'remove_shop_reason';
 
    
    
    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if ($result === false) {

            $data = $this->fetchAll();

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item['id']] = $item['name'];
                }
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }

        return $result;
    }
    
    function get_cache_active(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache_active');

        if ($result === false) {

            $db = Zend_Registry::get('db');

            $select = $db->select()
                ->from(array('p' => $this->_name),
                    array('p.*'));
            $select->where('p.del = 0',null);

            $select->order(new Zend_Db_Expr('p.`name` COLLATE utf8_unicode_ci'));

            $data = $db->fetchAll($select, 'name');

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item['id']] = $item['name'];
                }
            }
            $cache->save($result, $this->_name.'_cache_active', array(), null);
        }

        return $result;
    }
}