<?php

class Application_Model_Menu2 extends Zend_Db_Table_Abstract{

    protected $_name = 'leave_detail';

	private function dbconnect(){
		return Zend_Registry::get('db');
	}

    public function getMenuPermission()
	{
		$db = Zend_Registry::get('db');
		$menu_time = array(
            array(
                'id' => 999,
                'parent_id' => 0,
                'title' => 'Time Leave',
                'title_vie' => 'Time Leave',
                'url' => '/staff-time/staff-view',
                'class' => 'icon-arrow-down'
            ),
            array(
                'id' => 1000,
                'parent_id' => 999,
				'postion' => 1,
                'title' => 'My Check In',
                'title_vie' => 'My Check In',
                'url' => '/staff-time/staff-view',
                'class' => ''
            ),

			array(
                'id' => 1001,
                'parent_id' => 999,
				'postion' => 10,
                'title' => 'Create Leave',
                'title_vie' => 'Create Leave',
                'url' => '/leave/create',
                'class' => ''
            ),

			array(
                'id' => 1002,
                'parent_id' => 999,
				'postion' => 11,
                'title' => 'My Leave',
                'title_vie' => 'My Leave',
                'url' => '/leave/list-my-leave',
                'class' => ''
            ),
        );
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if(in_array($userStorage->group_id, array(HR_ID, ADMINISTRATOR_ID)))
        {
            $menu_time[] =  array(
                'id' => 1003,
                'parent_id' => 999,
				'postion' => 2,
                'title' => 'List Staff Time',
                'title_vie' => 'List Staff Time',
                'url' => '/staff-time/list-staff-check-in',
                'class' => ''
            );

			$menu_time[] =  array(
                'id' => 1004,
                'parent_id' => 999,
				'postion' => 2,
                'title' => 'Approve Time / Leave',
                'title_vie' => 'Approve Time / Leave',
                'url' => '/staff-time/list-staff-approve',
                'class' => ''
            );

			$menu_time[] =  array(
				'id' => 1005,
				'parent_id' => 999,
				'postion' => 3,
				'title' => 'Staff Time Permission',
				'title_vie' => 'Staff Time Permission',
				'url' => '/staff-permission',
				'class' => ''
			);

			$menu_time[] =  array(
				'id' => 1008,
				'parent_id' => 999,
				'postion' => 3,
				'title' => 'Chuyên cần',
				'title_vie' => 'Chuyên cần',
				'url' => '/time-machine/late-list',
				'class' => ''
			);

			// $menu_time[] =  array(
			// 		'id' => 1010,
			// 		'parent_id' => 999,
			// 		'postion' => 4,
			// 		'title' => 'Time Machine',
			// 		'title_vie' => 'Máy chấm công',
			// 		'url' => '/time-machine/luoi',
			// 		'class' => ''
			// );

			$menu_time[] =  array(
				'id' => 1011,
				'parent_id' => 999,
				'postion' => 5,
				'title' => 'Check In GPS',
				'title_vie' => 'Check In GPS',
				'url' => '/time/pg-time-create',
				'class' => ''
			);

			$menu_time[] =  array(
					'id' => 1012,
					'parent_id' => 999,
					'postion' => 6,
					'title' => 'Check Out GPS',
					'title_vie' => 'Check Out GPS',
					'url' => '/time/pg-check-out',
					'class' => ''
			);

			$menu_time[] =  array(
				'id' => 1013,
				'parent_id' => 999,
				'postion' => 5,
				'title' => 'Date Special Setting',
				'title_vie' => 'Date Special Setting',
				'url' => '/date/list-date-setting',
				'class' => ''
			);

			$menu_time[] =  array(
					'id' => 1014,
					'parent_id' => 999,
					'postion' => 6,
					'title' => 'Total Time',
					'title_vie' => 'Total Time',
					'url' => '/staff-time/total',
					'class' => ''
			);

			$menu_time[] =  array(
					'id' => 1015,
					'parent_id' => 999,
					'postion' => 6,
					'title' => 'List Staff Leave',
					'title_vie' => 'List Staff Leave',
					'url' => '/leave/list-leave-detail',
					'class' => ''
			);
        }
        else
        {
            $sql_check_permission = "SELECT id 
                                        FROM `staff_permission`
                                        WHERE staff_code = :staff_code
                                        AND (is_leader > 0 OR is_manager > 0)";
            $stmt = $db->prepare($sql_check_permission);
            $stmt->bindParam('staff_code', $userStorage->code, PDO::PARAM_STR);
            $stmt->execute();

            $data_check = $stmt->fetch();
			$stmt->closeCursor();

            if(!empty($data_check))
            {

                $menu_time[] =  array(
					'id' => 1001,
					'parent_id' => 999,
					'postion' => 2,
					'title' => 'List Staff Time',
					'title_vie' => 'List Staff Time',
					'url' => '/staff-time/list-staff-check-in',
					'class' => ''
				);

				$menu_time[] =  array(
					'id' => 1002,
					'parent_id' => 999,
					'postion' => 2,
					'title' => 'Approve Time / Leave',
					'title_vie' => 'Approve Time / Leave',
					'url' => '/staff-time/list-staff-approve',
					'class' => ''
				);

				$menu_time[] =  array(
					'id' => 1000,
					'parent_id' => 999,
					'postion' => 1,
					'title' => 'Time Machine',
					'title_vie' => 'Máy chấm công',
					'url' => '/time-machine/luoi',
					'class' => ''
				);

				$menu_time[] =  array(
						'id' => 1015,
						'parent_id' => 999,
						'postion' => 6,
						'title' => 'List Staff Leave',
						'title_vie' => 'List Staff Leave',
						'url' => '/leave/list-leave-detail',
						'class' => ''
				);
            }
			else
			{
				// if(in_array($userStorage->title, array(PGPB_TITLE, PB_SALES_TITLE, CHUYEN_VIEN_BAN_HANG_TITLE)))
				// {
				// 	$menu_time[] =  array(
				// 		'id' => 1002,
				// 		'parent_id' => 999,
				// 		'postion' => 5,
				// 		'title' => 'Check In GPS',
				// 		'title_vie' => 'Check In GPS',
				// 		'url' => '/time/pg-time-create',
				// 		'class' => ''
				// 	);

				// 	$menu_time[] =  array(
				// 			'id' => 1000,
				// 			'parent_id' => 999,
				// 			'postion' => 6,
				// 			'title' => 'Check Out GPS',
				// 			'title_vie' => 'Check Out GPS',
				// 			'url' => '/time/pg-check-out',
				// 			'class' => '');
					
				// 	$menu_time[] =  array(
				// 			'id' => 1015,
				// 			'parent_id' => 999,
				// 			'postion' => 6,
				// 			'title' => 'List Staff Leave',
				// 			'title_vie' => 'List Staff Leave',
				// 			'url' => '/leave/list-leave-detail',
				// 			'class' => ''
				// 	);
				// }

				$sql_check_group = "SELECT id
				 					FROM `group`
				 					WHERE CONCAT(',',menu,',') LIKE '%,73,%'
				 						AND id = :group_id
				 						AND id <> 4";
				$stmt = $db->prepare($sql_check_group);
				$stmt->bindParam('group_id', $userStorage->group_id, PDO::PARAM_INT);
				$stmt->execute();
				$data_group_check = $stmt->fetch();
				$stmt->closeCursor();

				

				if(in_array($userStorage->group_id, array(1, 5, 8, 9, 16, 18, 19, 21, 22, 24, 26, 28, 29, 30,14)))
				{
					$menu_time[] =  array(
						'id' => 1001,
						'parent_id' => 999,
						'postion' => 1,
						'title' => 'List Staff Time',
						'title_vie' => 'List Staff Time',
						'url' => '/staff-time/list-staff-check-in',
						'class' => ''
					);

					$menu_time[] =  array(
						'id' => 1002,
						'parent_id' => 999,
						'postion' => 2,
						'title' => 'Approve Time / Leave',
						'title_vie' => 'Approve Time / Leave',
						'url' => '/staff-time/list-staff-approve',
						'class' => ''
					);

					$menu_time[] =  array(
						'id' => 1015,
						'parent_id' => 999,
						'postion' => 6,
						'title' => 'List Staff Leave',
						'title_vie' => 'List Staff Leave',
						'url' => '/leave/list-leave-detail',
						'class' => ''
					);
				}
			}

			$sql_check_machine= "SELECT id FROM `check_in_machine_map` WHERE staff_code = :staff_code AND status = 1";
			$stmt = $db->prepare($sql_check_machine);
			$stmt->bindParam('staff_code', $userStorage->code, PDO::PARAM_STR);
			$stmt->execute();
			$data_machine_check = $stmt->fetch();
			$stmt->closeCursor();

			if(empty($data_machine_check))
			{
				$menu_time[] =  array(
					'id' => 1002,
					'parent_id' => 999,
					'postion' => 5,
					'title' => 'Check In GPS',
					'title_vie' => 'Check In GPS',
					'url' => '/time/pg-time-create',
					'class' => ''
				);

				$menu_time[] =  array(
						'id' => 1000,
						'parent_id' => 999,
						'postion' => 6,
						'title' => 'Check Out GPS',
						'title_vie' => 'Check Out GPS',
						'url' => '/time/pg-check-out',
						'class' => '');
				
				
				$sql_check_group = "SELECT id
				 					FROM `group`
				 					WHERE CONCAT(',',menu,',') LIKE '%,73,%'
				 						AND id = :group_id
				 						AND id <> 4";
				$stmt = $db->prepare($sql_check_group);
				$stmt->bindParam('group_id', $userStorage->group_id, PDO::PARAM_INT);
				$stmt->execute();
				$data_group_check = $stmt->fetch();
				$stmt->closeCursor();
				if(!empty($data_group_check))
				{
					$menu_time[] =  array(
							'id' => 1015,
							'parent_id' => 999,
							'postion' => 6,
							'title' => 'List Staff Leave',
							'title_vie' => 'List Staff Leave',
							'url' => '/leave/list-leave-detail',
							'class' => ''
					);
				}
			}

            $stmt = $db = null;
        }
		return $menu_time;
	}
    
}
