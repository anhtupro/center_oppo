<?php
class Application_Model_AppAreaIn extends Zend_Db_Table_Abstract
{
    protected $_name = 'app_area_in';
    protected $_schema = DATABASE_TRADE;
    
    public function checkImeiArea($imei){

        $db     = Zend_Registry::get("db");
        $select = $db->select();
            
        $arrCols = array(
            'p.contractor_id',
            'p.campaign_id',
            'p.category_id',
            'p.imei_sn',
            'd.area_id',
            'd.shipment_id',
            'imei_in_area' => 'i.imei_sn' 
        );
        
        $select->from(array('p'=> DATABASE_TRADE.'.app_contractor_out'), $arrCols);
        $select->joinLeft(array('d' => DATABASE_TRADE.'.app_shipment_details'), 'p.app_shipment_details = d.id', array());
        $select->joinLeft(array('i' => DATABASE_TRADE.'.app_area_in'), 'i.imei_sn = p.imei_sn', array());
        $select->where('p.imei_sn = ?', $imei);
        
        $result  = $db->fetchRow($select);
        
        return $result;
    }

    public function getListImeiArea($params){

        $db     = Zend_Registry::get("db");
        $select = $db->select();
            
        $arrCols = array(
            'p.area_id', 
            'p.campaign_id', 
            'p.category_id', 
            'p.imei_sn', 
            'p.created_at', 
            'p.created_by'
        );
        
        $select->from(array('p'=> DATABASE_TRADE.'.app_area_in'), $arrCols);
        $select->where('p.campaign_id = ?', $params['campaign_id']);
        $select->where('p.category_id = ?', $params['category_id']);
        $select->where('p.area_id IN (?)', $params['area_id']);
        
        $result  = $db->fetchAll($select);
        
        return $result;
    }
}