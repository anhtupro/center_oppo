<?php

class Application_Model_AirInventoryAreaTest extends Zend_Db_Table_Abstract
{
    protected $_name = 'inventory_area_test';
    protected $_schema = DATABASE_TRADE;

    public function getCategory($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['i' => DATABASE_TRADE.'.inventory_area_test'], [
                'category_id' => 'i.category_id',
                'category_name' => 'c.name',
                'quantity' => 'i.quantity',
                'imei_sn' => "GROUP_CONCAT(o.imei_sn SEPARATOR ', ')",
                'group_bi' => 'c.group_bi'
            ])
            ->joinLeft(['o' => DATABASE_TRADE.'.inventory_area_imei_test'], 'i.area_id = o.area_id AND i.category_id = o.category_id', [])
            ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'i.category_id = c.id', [])
            ->group(['i.area_id', 'i.category_id']);

        if($params['area_id']) {
            $select->where('i.area_id = ?', $params['area_id']);
        }

        $result = $db->fetchAll($select);

        return $result;
    }
}