<?php
class Application_Model_TransferFile extends Zend_Db_Table_Abstract
{
	protected $_name = 'transfer_file';
    protected $_schema = DATABASE_TRADE;
    
    public function getTransferFile($params){
        $db     = Zend_Registry::get("db");
        $select = $db->select();
            
        $arrCols = array(
            'transfer_id'    => 'p.transfer_id',
            'url'    		 => 'p.url',
            'type'       	 => 'p.type',
        );

        $select->from(array('p'=> DATABASE_TRADE.'.transfer_file'), $arrCols);

        $select->where('p.transfer_id = ?', $params['id']);

        $select->where('p.type = ?', $params['type']);

        $result  = $db->fetchAll($select);

        return $result;
    }

}