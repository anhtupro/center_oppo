<?php
class Application_Model_GroupLevel2 extends Zend_Db_Table_Abstract
{
	protected $_name = 'group_level2';

	function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        if (isset($params['name']) and $params['name'])
            $select->where('p.name LIKE ?', '%'.$params['name'].'%');


		$select->where('p.del is null');
        // $select->order('p.id DESC');
        // $select->order('p.system');
        $select->order('p.name', 'COLLATE utf8_unicode_ci ASC');

        $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
	}
	
	function getGroupAndGroupBasic($id){
		$db = Zend_Registry::get('db');

		$col = array(
			'id' => 'gl2.id',
			'gl2_name' => 'gl2.name',
			'gb_menu' => 'gb.menu',
			'gb_access' => 'gb.access',
			'gb_name' => 'gb.name',
			'gb_id' => 'gb.id',
		);

        $select = $db->select()
            ->from(array('gl2' => $this->_name),
				array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS gl2.id')));
		$select->joinLeft(array('gm' => 'group_mapping'), 'gl2.id = gm.group_level2');
		$select->joinLeft(array('gb' => 'group_basic'), 'gm.group_basic = gb.id');

		$select->where('gl2.id = ?', $id);
		$select->where('gb.del is null');

		$select->order('gb.name', 'COLLATE utf8_unicode_ci ASC');

        $result = $db->fetchAll($select);
        return $result;
	}

	function getGroupByStaffId($id_staff){
		$db = Zend_Registry::get('db');

		$col = array(
			'gl2_id' => 'gl2.id',
			'gl2_name' => 'gl2.name',
		);

        $select = $db->select()
            ->from(array('gl2' => $this->_name),$col);
		$select->join(array('gm' => 'group_mapping_title_staff'), 'gl2.id = gm.group_level2', array());
		$select->join(array('sta' => 'staff'), 'gm.staff = sta.id', array());

		$select->where('sta.id = ?', $id_staff);
		$select->where('gm.type = 1');
		$select->where('gl2.del is null');

		$select->order('gl2.name', 'COLLATE utf8_unicode_ci ASC');

        $result = $db->fetchAll($select);
        return $result;
	}

	function getGroupByTitleId($id_title){
		$db = Zend_Registry::get('db');

		$col = array(
			'gl2_id' => 'gl2.id',
			'gl2_name' => 'gl2.name',
		);

        $select = $db->select()
            ->from(array('gl2' => $this->_name),$col);
		$select->join(array('gm' => 'group_mapping_title_staff'), 'gl2.id = gm.group_level2', array());
		$select->join(array('t' => 'team'), 'gm.title = t.id', array());

		$select->where('t.id = ?', $id_title);
		$select->where('gm.type = 2');
		$select->where('gl2.del is null');

		$select->order('gl2.name', 'COLLATE utf8_unicode_ci ASC');

        $result = $db->fetchAll($select);
        return $result;
	}

	function getAllGroup(){
        $db = Zend_Registry::get('db');

        $select = $db->select()
        ->from(array('p' => $this->_name),
            array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

		$select->where('p.del is null');
        $select->order('p.name', 'COLLATE utf8_unicode_ci ASC');

        $result = $db->fetchAll($select);
        return $result;
    }

    function getGroupByName($name, $id_str){
        $db = Zend_Registry::get('db');

        $select = $db->select()
        ->from(array('p' => $this->_name), 'p.*');

        if(isset($name) and $name)
            $select->where('p.name LIKE ?', '%'.$name.'%');

        if(isset($id_str) and $id_str)
            $select->where("find_in_set(p.id, '".$id_str."') = 0");

		$select->where('p.del is null');
        $select->order('p.name', 'COLLATE utf8_unicode_ci ASC');

        $result = $db->fetchAll($select);
        return $result;
	}
	
	function getAccessByStaffId($id_staff){
		$db = Zend_Registry::get('db');

		$col = array(
			'sta_id' => 'sta.id',
			'gb_id' => 'gb.id',
			'gb_name' => 'gb.name',
			'access' => 'gbm.access',
			'menu' => 'gb.menu',
		);

        $select = $db->select()
            ->from(array('gl2' => $this->_name),$col);
		$select->join(array('gmts' => 'group_mapping_title_staff'), 'gl2.id = gmts.group_level2', array());
		$select->join(array('sta' => 'staff'), 'gmts.staff = sta.id', array());
		$select->join(array('gm' => 'group_mapping'), 'gl2.id = gm.group_level2', array());
		$select->join(array('gb' => 'group_basic'), 'gm.group_basic = gb.id', array());
		$select->join(array('gbm' => 'group_basic_mapping'), 'gb.id = gbm.id_group_basic', array());

		$select->where('sta.id = ?', $id_staff);
		$select->where('gmts.type = 1');
		$select->where('gl2.del is null');
		$select->where('gb.del is null');
		$select->where('gbm.system = 1');
		$select->group('gl2.id');
		$select->group('gb.id');

        $result = $db->fetchAll($select);
        return $result;
	}

	function getAccessByTitleId($id_title){
		$db = Zend_Registry::get('db');

		$col = array(
			'title_id' => 't.id',
			'gb_id' => 'gb.id',
			'gb_name' => 'gb.name',
			'access' => 'gbm.access',
			'menu' => 'gb.menu',
		);

        $select = $db->select()
            ->from(array('gl2' => $this->_name),$col);
		$select->join(array('gmts' => 'group_mapping_title_staff'), 'gl2.id = gmts.group_level2', array());
		$select->join(array('t' => 'team'), 'gmts.title = t.id', array());
		$select->join(array('gm' => 'group_mapping'), 'gl2.id = gm.group_level2', array());
		$select->join(array('gb' => 'group_basic'), 'gm.group_basic = gb.id', array());
		$select->join(array('gbm' => 'group_basic_mapping'), 'gb.id = gbm.id_group_basic', array());

		$select->where('t.id = ?', $id_title);
		$select->where('gmts.type = 2');
		$select->where('gl2.del is null');
		$select->where('gb.del is null');
		$select->where('gbm.system = 1');
		$select->group('gl2.id');
		$select->group('gb.id');

        $result = $db->fetchAll($select);
        return $result;
	}

	//export
	function getAllGroupAndStaffTitle(){
		$db = Zend_Registry::get('db');

		$col = array(
			'group_id'   => 'gl2.id',
			'group_name' => 'gl2.name',
			'staff_id'   => 'sta.id',
			'staff_code' => 'sta.code',
			'staff_name' => 'concat(sta.firstname, " ", sta.lastname)',
			'title_id' 	 => 't.id',
			'title_name' => 't.name'
		);

		$select = $db->select()
			->from(array('gl2' => $this->_name),$col);
		$select->joinLeft(array('gmts' => 'group_mapping_title_staff'), 'gl2.id = gmts.group_level2', array());
		$select->joinLeft(array('sta' => 'staff'), 'gmts.staff = sta.id', array());
		$select->joinLeft(array('t' => 'team'), 'gmts.title = t.id', array());

		$select->where('gl2.del is null');
		$select->where('sta.off_date is null');

		$select->order('gl2.name', 'COLLATE utf8_unicode_ci ASC');

		$result = $db->fetchAll($select);
        return $result;
	}

	function checkGroupOfStaffOrTitle($id_group_level2, $id_staff) {
		$db = Zend_Registry::get('db');

		$col = array(
			'sta_id' => 'sta.id',
			'title_id' => 'sta.id',
			'gl2_id' => 'gl2.id',
			'gl2_name' => 'gl2.name',
		);

        $select = $db->select()
            ->from(array('sta' => 'staff'),$col);
		$select->joinLeft(array('gmts' => 'group_mapping_title_staff'), 'sta.id = gmts.staff', array());
		$select->joinLeft(array('gmts2' => 'group_mapping_title_staff'), 'sta.title = gmts2.title', array());
		$select->joinLeft(array('gl2' => 'group_level2'), 'gmts.group_level2 = gl2.id or gmts2.group_level2 = gl2.id', array());

		$select->where('sta.id = ?', $id_staff);
		$select->where('gl2.id = ?', $id_group_level2);
		$select->where('gl2.del is null');
		$select->group('gl2.id');

		$result = $db->fetchAll($select);

		if($result)
			return true;
		else 
			return false;
	}
}                                                      
