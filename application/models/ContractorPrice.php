<?php
class Application_Model_ContractorPrice extends Zend_Db_Table_Abstract
{
    protected $_name = 'contractor_price';
    protected $_schema = DATABASE_TRADE;

    public function fetchPagination($page, $limit, &$total, $params){
	$db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
            'p.name', 
            'p.status', 
            'p.price', 
            'p.desc',
            'p.photo',
            'p.type',
            'c.contractor_id',
            'contructor_id'     => 'con.id',
            'contructor_code'   => 'GROUP_CONCAT(con.code)'
        );

        $select->from(array('p'=> DATABASE_TRADE.'.category'), $arrCols);
        $select->joinLeft(array('c' => DATABASE_TRADE.'.contractor_price'), 'c.category_id = p.id', array());
        $select->joinLeft(array('con' => DATABASE_TRADE.'.contructors'), 'con.id = c.contractor_id',array());
        
        $select->where('c.to_date IS NULL', NULL);
        
        if(!empty($params['name'])){
            $select->where("p.name LIKE ?", '%'.$params['name'].'%');
        }
        
        if(!empty($params['contructor_name'])){
            $select->where("con.name LIKE ?", '%'.$params['contructor_name'].'%');
        }
        
        if(!empty($params['has_contructor']) AND $params['has_contructor'] == 1){
            $select->where("con.id IS NOT NULL", NULL);
        }
        
        if(!empty($params['has_contructor']) AND $params['has_contructor'] == 2){
            $select->where("con.id IS NULL", NULL);
        }
        
        if(!empty($params['code'])){
            $select->where("con.code LIKE ?", '%'.$params['code'].'%');
        }

        $select->group('p.id');
        
        $select->order('p.name DESC');
        
        $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    public function getContractorPrice($params){
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id', 
            'p.contractor_id', 
            'p.category_id', 
            'p.price',
            'p.from_date',
            'p.to_date',
            'c.short_name',
            'category_name' => 'cat.name',
            'category_code' => 'cat.code',
            'category_price' => 'cat.price',
            'c.code'
        );

        $select->from(array('p'=> DATABASE_TRADE.'.contractor_price'), $arrCols);
        $select->joinLeft(array('c' => DATABASE_TRADE.'.contructors'), 'c.id = p.contractor_id', array());
        $select->joinLeft(array('cat' => DATABASE_TRADE.'.category'), 'cat.id = p.category_id',array());
        
        $select->where('p.to_date IS NULL', NULL);
        
        if(!empty($params['contractor_id'])){
            $select->where('p.contractor_id = ?', $params['contractor_id']);
        }
        
        if(!empty($params['category_id'])){
            $select->where('p.category_id = ?', $params['category_id']);
        }
        
        $select->order('p.category_id DESC');

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getPrice($params = null){
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id', 
            'p.contractor_id', 
            'p.category_id', 
            'p.price',
            'p.from_date',
            'p.to_date'
        );

        $select->from(array('p'=> DATABASE_TRADE.'.contractor_price'), $arrCols);

        $select->where('p.to_date IS NULL', NULL);

        $result = $db->fetchAll($select);

        $data = [];
        foreach($result as $key=>$value){
            $data[$value['contractor_id']][$value['category_id']] = $value['price'];
        }

        return $data;
    }

    public function getContractorDefault($params = null){

        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id', 
            'p.contractor_id', 
            'p.category_id', 
            'p.price',
            'c.code',
            'p.area_id'
        );

        $select->from(array('p'=> DATABASE_TRADE.'.contractor_category'), $arrCols);
        $select->joinLeft(array('c' => DATABASE_TRADE.'.contructors'), 'c.id = p.contractor_id', array());
        $select->where('p.status = 1', NULL);
        
        if ($params['campaign_id']) {
            $select->where('p.campaign_id = ?', $params['campaign_id']);
        }

        $result = $db->fetchAll($select);

        $data = [];
        foreach($result as $key=>$value){
            $data[$value['area_id']][$value['category_id']] = $value['code'];
        }

        return $data;
    }
    
    public function getContractorCode($params){
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id', 
            'p.code', 
            'p.short_name', 
            'p.address',
        );

        $select->from(array('p'=> DATABASE_TRADE.'.contructors'), $arrCols);
        $select->where('p.code IS NOT NULL', NULL);
        
        if(!empty($params['not_contractor'])){
            $select->where('p.id NOT IN (?)', $params['not_contractor']);
        }
        
        $result = $db->fetchAll($select);
        
        return $result;
    }
    public function getCategoryCode($params){
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id', 
            'p.code', 
            'p.name', 
            'p.price',
        );

        $select->from(array('p'=> DATABASE_TRADE.'.category'), $arrCols);
        $select->where('p.type = 1');
        
        if(!empty($params['not_category'])){
            $select->where('p.id NOT IN (?)', $params['not_category']);
        }
        
        $result = $db->fetchAll($select);
        
        return $result;
    }
    public function getHistory($params){
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id', 
            'contractor_name'   => 'c.short_name',
            'p.contractor_id', 
            'category_name'     => 'cat.name',
            'p.category_id', 
            'p.price',
            'p.from_date',
            'p.to_date'
        );

        $select->from(array('p'=> DATABASE_TRADE.'.contractor_price'), $arrCols);
        $select->joinLeft(array('c' => DATABASE_TRADE.'.contructors'), 'c.id = p.contractor_id', array());
        $select->joinLeft(array('cat' => DATABASE_TRADE.'.category'), 'cat.id = p.category_id',array());
        $select->where('p.contractor_id = ?', $params['contractor_id']);
        $select->where('p.category_id = ?', $params['category_id']);

        $result = $db->fetchAll($select);
        return $result;
    }


    public function getContractorCategoryPrice($params){
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id', 
            'p.contractor_id', 
            'p.category_id', 
            'p.price',
            'p.from_date',
            'p.to_date',
            'c.short_name',
            'category_name' => 'cat.name',
            'category_code' => 'cat.code',
            'category_price' => 'cat.price',
            'c.code',
            'validated_price' => 'q.price'
        );
        $nestedSelect = "SELECT p.category_id, p.contractor_id, p.from_date, p.to_date, p.price
                         FROM trade_marketing.contractor_price p
                         WHERE (p.contractor_id = '".$params['contractor_id']."') AND (p.to_date IS NULL OR p.to_date >= CURDATE()) AND p.from_date <= CURDATE()";

        $select->from(array('p'=> DATABASE_TRADE.'.contractor_price'), $arrCols);
        $select->joinLeft(array('c' => DATABASE_TRADE.'.contructors'), 'c.id = p.contractor_id', array());
        $select->joinLeft(array('cat' => DATABASE_TRADE.'.category'), 'cat.id = p.category_id',array());

         
        $select->joinLeft(array('q' => new Zend_Db_Expr('(' . $nestedSelect . ')')),'q.category_id = p.category_id',array());
       
        if(!empty($params['contractor_id'])){
            $select->where('p.contractor_id = ?', $params['contractor_id']);
        }
        
        if(!empty($params['category_id'])){
            $select->where('p.category_id = ?', $params['category_id']);
        }
            
        $select->group('p.category_id');
        $result = $db->fetchAll($select);

        return $result;
    }

}