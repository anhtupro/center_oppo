<?php
class Application_Model_CapacityRecommend extends Zend_Db_Table_Abstract
{
    protected $_name = 'capacity_recommend';
    
    function getRecommend($capacity_id) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "d.dictionary_id", 
            "dictionary.type", 
            "title_name" => "dictionary.title",
        );

        $select->from(array('p' => 'capacity_recommend'), $arrCols);
        $select->joinLeft(array('d' => 'capacity_recommend_details'), 'd.recommend_id = p.id', array());
        $select->joinLeft(array('dictionary' => 'capacity_dictionary'), 'dictionary.id = d.dictionary_id', array());

        $select->where('p.staff_capacity_id = ?', $capacity_id);
        $select->where('p.is_del = 0');
        $select->group('d.dictionary_id');
        $result = $db->fetchAll($select);

        return $result;
    }
    
    function getRecommendStaff($capacity_id) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "d.dictionary_id", 
            "dictionary.type", 
            "title_name" => "dictionary.title",
        );

        $select->from(array('p' => 'capacity_recommend'), $arrCols);
        $select->joinLeft(array('d' => 'capacity_recommend_details'), 'd.recommend_id = p.id', array());
        $select->joinLeft(array('dictionary' => 'capacity_dictionary'), 'dictionary.id = d.dictionary_id', array());

        $select->where('p.staff_capacity_id = ?', $capacity_id);
        $select->where('p.is_del = 0');
        $select->where('p.is_staff_appraisal = 1');
        $result = $db->fetchAll($select);

        return $result;
    }
    
}