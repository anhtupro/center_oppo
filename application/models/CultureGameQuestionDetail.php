<?php
class Application_Model_CultureGameQuestionDetail extends Zend_Db_Table_Abstract
{
	protected $_name = 'culture_game_question_detail';


     function get_cache() {
        $cache = Zend_Registry::get('cache');
        $result = $cache->load($this->_name . '_cache');

        if ($result === false) {

            $db = Zend_Registry::get('db');

            $select = $db->select()
                    ->from(array('p' => $this->_name), array('p.*'));
             $select->order(new Zend_Db_Expr('p.`id`'));

            $data = $db->fetchAll($select);

            $result = array();
            if ($data) {
                foreach ($data as $item) {
                    $result[$item['question_id']][] = $item;
                }
            }
            $cache->save($result, $this->_name . '_cache', array(), null);
        }
        return $result;
    }
}
