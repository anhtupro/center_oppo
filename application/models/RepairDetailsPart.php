<?php
class Application_Model_RepairDetailsPart extends Zend_Db_Table_Abstract
{
    protected $_name = 'repair_details_part';
	protected $_schema = DATABASE_TRADE;

    public function getPart($repairId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'r.category_id',
            "p.quantity"
        );

        $select->from(array('p' => DATABASE_TRADE.'.repair_details_part'), $arrCols);
        $select->joinLeft(array('r' => DATABASE_TRADE.'.repair_part'), 'r.id = p.repair_part_id', array());
        $select->joinLeft(array('d' => DATABASE_TRADE.'.repair_details'), 'd.id = p.repair_details_id', array());
        $select->joinLeft(array('c' => DATABASE_TRADE.'.category'), 'r.category_id = c.id', array());
        $select->where('d.repair_id = ?', $repairId);

        $result = $db->fetchAll($select);

        return $result;
	}
	
}