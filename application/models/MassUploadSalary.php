<?php

class Application_Model_MassUploadSalary extends Zend_Db_Table_Abstract {

    protected $_name = 'mass_upload_salary';

    function fetchPagination($page, $limit, &$total, $params) {
        $db = Zend_Registry::get('db');

        $select = $db->select()
                ->from(array('p' => $this->_name), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));
        $select->join(array('s' => 'staff'), 's.code = p.staff_code', array('fullname' => "concat(s.firstname,' ',s.lastname)"));
        $select->join(array('s2' => 'staff'), 's2.id = p.created_by', array('username' => "concat(s2.firstname,' ',s2.lastname)"));
        if (isset($params['name']) and $params['name'])
            $select->where("CONCAT(s.firstname,' ',s.lastname) COLLATE utf8_unicode_ci LIKE ", '%' . $params['name'] . '%');
        if (isset($params['code']) and $params['code'])
            $select->where("p.staff_code = ?", $params['code']);
        if (isset($params['year']) and $params['year'])
            $select->where("p.year = ?", $params['year']);
        if (isset($params['month']) and $params['month'])
            $select->where("p.month = ?", $params['month']);
        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total  = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

}
