<?php
class Application_Model_PurchasingRatingTimes extends Zend_Db_Table_Abstract
{
    protected $_name = "purchasing_rating_times";
    
    function fetchPagination($page, $limit, &$total, $params){

        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(
                    array('p'=>'purchasing_rating_times'),
                    array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
                            'p.*',                         
                        ))
            ->order('p.id DESC');
        if (isset($params['name']) and $params['name']) {
            $select->where('p.name LIKE ?', '%' . $params['name'] . '%');
        }        
        $result = $db->fetchAll($select);
        if ($limit) {
            $total = $db->fetchOne("select FOUND_ROWS()");
        }
        return $result;
    }
}
