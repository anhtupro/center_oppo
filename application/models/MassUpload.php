<?php

class Application_Model_MassUpload extends Zend_Db_Table_Abstract {

    protected $_name = 'massupload';

    function fetchPagination($page, $limit, &$total, $params) {
        $db = Zend_Registry::get('db');
        $select = $db->select()
                ->from(
                        array('p' => $this->_name),
                        array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
                            'p.*',
                ))->join(['a'=>'massupload_access'] , 'p.id = a.id_massupload' , [])
                ->joinLeft(['st'=>'staff'] , 'st.id = p.created_by' , ['full_name'=> new Zend_Db_Expr("CONCAT(st.firstname,' ',st.lastname)")])
                ->order('p.id DESC')
                ->where('p.parent_id =?', 0)
                ->where('p.del =?', 0);
        if (isset($params['name']) and $params['name']) {
            $select->where('p.name LIKE ?', '%' . $params['name'] . '%');
        }
        if (isset($params['status']) && ($params['status'] == 0 || $params['status'] == 1)) {
            $select->where('p.status_final =? ', $params['status']);
        }
        if ((isset($params['id_user']) and $params['id_user']) || (isset($params['access_group']) and $params['access_group'])) {
            $select->where('p.created_by = '.$params['id_user'].' or a.id_group_access = '.$params['access_group']);
        }
        if ($limit) {
            $select->limitPage($page, $limit);
        }
        $result = $db->fetchAll($select);
        if ($limit) {
            $total = $db->fetchOne("select FOUND_ROWS()");
        }

        return $result;
    }

}
