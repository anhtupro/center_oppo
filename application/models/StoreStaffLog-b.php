<?php
class Application_Model_StoreStaffLog extends Zend_Db_Table_Abstract
{
    protected $_name = 'store_staff_log';

    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        $select
            ->joinLeft(array('s' => 'staff'), 'p.staff_id = s.id', array('s.email'))
            ->join(array('st' => 'store'), 'st.id=p.store_id', array('store_id' => 'st.id', 'store_name' => 'st.name', 'st.district'));

        if (isset($params['email']) and $params['email'])
            $select->where('s.email LIKE ?', str_replace(EMAIL_SUFFIX, '', $params['email']).EMAIL_SUFFIX);

        if (isset($params['staff_id']) and $params['staff_id'])
            $select->where('s.id = ?', $params['staff_id']);

        if (isset($params['from']) and $params['from'] && isset($params['to']) and $params['to']) {
            $select->where('FROM_UNIXTIME(p.joined_at, "%Y-%m-%d") <= ?', $params['to']);
            $select->where('p.released_at IS NULL OR FROM_UNIXTIME(p.released_at, "%Y-%m-%d") > ?', $params['from']);
        }

        $order_str = ' released_at IS NULL DESC, released_at DESC, joined_at DESC';

        $select->order(new Zend_Db_Expr($order_str));

        if ($limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);

        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    /**
     * Kiểm tra xem chấm công vào lúc timing_time, tại cửa hàng store_id có...
     *     ...thuộc quản lý của staff_id hay không
     * @param  int  $staff_id    - id của người muốn check hàng
     * @param  int  $store_id    - id cửa hàng
     * @param  datetime  $timing_time - thời gian của chấm công (timing for)
     * @param  boolean $is_leader - true: là sales;
     *                            false: là PG
     *                            NULL: không quan tâm, chỉ cần biết nó thuộc cửa hàng
     * @return boolean              - true: thuộc thằng đó;
     *                                false: không thuộc
     */
    public function belong_to($staff_id, $store_id, $timing_time, $is_leader = NULL)
    {
        $where = array();
        $where[] = $this->getAdapter()->quoteInto('store_id = ?', $store_id);
        $where[] = $this->getAdapter()->quoteInto('staff_id = ?', $staff_id);

        if ( ! is_null( $is_leader ) )
            if ($is_leader)
                $where[] = $this->getAdapter()->quoteInto('is_leader = ?', 1);
            else
                $where[] = $this->getAdapter()->quoteInto('is_leader = ?', 0);

        $where[] = $this->getAdapter()
            ->quoteInto('? >= FROM_UNIXTIME(joined_at, \'%Y-%m-%d\') AND ( ? < FROM_UNIXTIME(released_at, \'%Y-%m-%d\') OR released_at IS NULL OR released_at = 0 )',
                date('Y-m-d', strtotime($timing_time)));

        return $this->fetchRow($where) ? true : false;
    }

    /**
     * Lấy danh sách các store mà staff đó thuộc tại thời điểm $time
     * @param  int $staff_id  -  staff id
     * @param  datetime $time - thời gian để check
     * @return array          - mảng các store id
     */
    public function get_stores($staff_id, $time)
    {
        $where = array();
        $where[] = $this->getAdapter()->quoteInto('staff_id = ?', $staff_id);
        $where[] = $this->getAdapter()->quoteInto('FROM_UNIXTIME(joined_at, \'%Y-%m-%d\') <= ?', $time);
        $where[] = $this->getAdapter()->quoteInto('released_at IS NULL OR released_at = 0 OR FROM_UNIXTIME(released_at, \'%Y-%m-%d\') > ?', $time);

        $result = $this->fetchAll($where);

        $stores = array();

        foreach ($result as $value)
            $stores[] = $value['store_id'];

        return count($stores) > 0 ? $stores : false;
    }

    /**
     * Lấy danh sách các store mà staff đó quản lý từ $from đến $to
     * @param  int $staff_id  -  staff id
     * @param  datetime $time - thời gian để check
     * @return array          - mảng các store id
     */
    public function get_stores_cache($staff_id, $from, $to)
    {
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if ($result === false) {
            $db = Zend_Registry::get('db');
            $select = $db->select()
                ->distinct()
                ->from(array('p' => $this->_name), array('p.store_id', 'p.staff_id'))
                ->where('FROM_UNIXTIME(joined_at, \'%Y-%m-%d\') <= ?', $to)
                ->where('released_at IS NULL OR released_at = 0 OR FROM_UNIXTIME(released_at, \'%Y-%m-%d\') > ?', $from);

            $data = $db->fetchAll($select);

            $result = array();

            foreach ($data as $value)
                $result[ $value['staff_id'] ][] = $value['store_id'];

            $cache->save($result, $this->_name.'_cache', array(), null);
        }

        return isset($result[ $staff_id ]) && count($result[ $staff_id ]) ? $result[ $staff_id ] : false;
    }

    /**
     * Khi PG/Sales chấm công, hàm này giúp lấy...
     *     ...ID sales quản lý cửa hàng đó, vào ngày chấm công
     * @param  int $store_id - ID cửa hàng
     * @param  datetime $time     - Ngày chấm công
     * @return int           - ID sales quản lý cửa hàng
     */
    public function get_sales_man($store_id, $time)
    {
        $where = array();
        $where[] = $this->getAdapter()->quoteInto('is_leader = ?', 1);
        $where[] = $this->getAdapter()->quoteInto('store_id = ?', $store_id);

        $where[] = $this->getAdapter()->quoteInto(
            'FROM_UNIXTIME(joined_at, \'%Y-%m-%d\') <= ?', date('Y-m-d', strtotime($time))
            );

        $where[] = $this->getAdapter()->quoteInto(
            'released_at IS NULL OR released_at = 0 OR FROM_UNIXTIME(released_at, \'%Y-%m-%d\') > ?',
             date('Y-m-d', strtotime($time))
             );

        $result = $this->fetchRow($where);

        return $result && intval($result['staff_id']) > 0 ? $result['staff_id'] : false;
    }

    public function getSaleManList($from, $to)
    {
        $db = Zend_Registry::get('db');
        $sql = "SELECT DISTINCT store_staff_log.staff_id, store_staff_log.store_id, staff.firstname, staff.lastname
            FROM store_staff_log
            INNER JOIN staff ON staff.id=store_staff_log.staff_id
            WHERE DATE(FROM_UNIXTIME(store_staff_log.joined_at)) <= ?
            AND (store_staff_log.released_at IS NULL OR DATE(FROM_UNIXTIME(store_staff_log.released_at)) > ?)
            AND store_staff_log.is_leader=1";

        $result = $db->query($sql, array($to, $from));

        $list = array();

        foreach ($result as $key => $value) {
            if (!isset($list[ $value['store_id'] ])) $list[ $value['store_id'] ] = '';
            $list[ $value['store_id'] ] .= $value['firstname'] .' ' .$value['lastname'] . ', ';
        }

        return $list;
    }
    
    /**
    *   Lấy danh sách sale + pg đứng shop
    */
    public function get_sales_list($store_id, $time)
    {
        $where = array();
        $where[] = $this->getAdapter()->quoteInto('store_id = ?', $store_id);

        $where[] = $this->getAdapter()->quoteInto(
            'FROM_UNIXTIME(joined_at, \'%Y-%m-%d\') <= ?', date('Y-m-d', strtotime($time))
            );

        $where[] = $this->getAdapter()->quoteInto(
            'released_at IS NULL OR released_at = 0 OR FROM_UNIXTIME(released_at, \'%Y-%m-%d\') > ?',
             date('Y-m-d', strtotime($time))
             );

        $result = $this->fetchAll($where);

        return $result && intval($result['staff_id']) > 0 ? $result : false;
    }
    
    /**
    *   Lấy tất cả danh sách sales đứng shop
    */
    public function get_list_sale($store_id, $time)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id, s.firstname, s.lastname, p.is_leader')));
        
        $select->joinLeft(array('s' => 'staff'), 'p.staff_id = s.id', 
            array( 'firstname' => 's.firstname',
                   'lastname'  => 's.lastname',
                   'email'     => 's.email',
                   'phone_number' => 's.phone_number',
                   'title' => 's.title',
                   'staff_id' => 's.id' )); 

        $select->joinLeft(array('t' => 'team'), 't.id = s.title', 
            array( 'title_name' => 't.name' )); 
        
        $select->where('FROM_UNIXTIME(p.joined_at, \'%Y-%m-%d\') <= ?',date('Y-m-d', strtotime($time)));
        $select->where('p.released_at IS NULL OR p.released_at = 0 OR FROM_UNIXTIME(released_at, \'%Y-%m-%d\') > ?',date('Y-m-d', strtotime($time)));
        if (is_array($store_id) && count($store_id) > 0) {
            $select->where('p.store_id IN (?)', $store_id);
        }else{
            $select->where('p.store_id = ?', $store_id);
        }
        $select->where('s.title IN (?)', array(PGPB_TITLE,SALES_TITLE,CHUYEN_VIEN_BAN_HANG_TITLE,PB_SALES_TITLE));
        
        $result = $db->fetchAll($select);
        return $result;

    }

     /**
    *   Lấy tất cả danh sách sales leader đứng shop
    */
    public function get_list_sale_leader($store_id, $time)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('p' => 'store_leader_log'),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id, s.firstname, s.lastname')));
        
        $select->joinLeft(array('s' => 'staff'), 'p.staff_id = s.id', 
            array( 'firstname' => 's.firstname',
                   'lastname'  => 's.lastname',
                   'email'     => 's.email',
                   'phone_number' => 's.phone_number',
                   'title' => 's.title',
                   'staff_id' => 's.id' )); 

        $select->joinLeft(array('t' => 'team'), 't.id = s.title', 
            array( 'title_name' => 't.name' )); 
        
        $select->where('FROM_UNIXTIME(p.joined_at, \'%Y-%m-%d\') <= ?',date('Y-m-d', strtotime($time)));
        $select->where('p.released_at IS NULL OR p.released_at = 0 OR FROM_UNIXTIME(released_at, \'%Y-%m-%d\') > ?',date('Y-m-d', strtotime($time)));
        $select->where('p.store_id = ?', $store_id);
        
        $result = $db->fetchAll($select);
        return $result;

    }
    public function findPrimaykey($id){
        return $this->fetchRow($this->select()->where('id = ?',$id));
    }
    public function findStaffId($id){
        return $this->fetchRow($this->select()->where('staff_id = ?',$id));
    }
    
}
