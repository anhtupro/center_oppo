<?php

class Application_Model_InventoryCampaignBrand extends Zend_Db_Table_Abstract
{
    protected $_name = 'inventory_campaign_brand';

    public function getAll()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['b' => 'inventory_campaign_brand'], [
                'b.*'
            ])
         ->where('b.status = ?', 1);

        $result = $db->fetchAll($select);

        foreach ($result as $item) {
            $list [$item['id']] = $item['name'];
        }

        return $list;

    }
}