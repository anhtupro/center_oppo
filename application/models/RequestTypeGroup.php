<?php

class Application_Model_RequestTypeGroup extends Zend_Db_Table_Abstract
{
    protected $_name = 'request_type_group';
    
    public function getList($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "id" => "p.id",
            "title" => "p.title",
            "desc" => "p.desc",
            "department_id" => "p.department_id",
            "is_del" => "p.is_del",
        );

        $select->from(array('p' => 'request_type_group'), $arrCols);
        $select->where('p.is_del = 0');

        $select->where('p.department_id = ?', $params['department_id']);
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getListTypeGroup($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "title_name" => "p.title", 
            "p.desc", 
            "p.department_id"
        );

        $select->from(array('p' => 'request_type_group'), $arrCols);
        $select->where("p.is_del = 0");
        $select->where('p.department_id = ?', $params['department_id']);
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getListType($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "title_name" => "p.title", 
            "p.desc", 
            "g.department_id", 
            "p.group_id"
        );

        $select->from(array('p' => 'request_type'), $arrCols);
        $select->joinLeft(array('g' => 'request_type_group'), 'g.id = p.group_id', array());
        $select->where("p.is_del = 0 AND g.is_del = 0");
        $select->where('g.department_id = ?', $params['department_id']);
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getListTypeBudget($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "request_type_id" => "p.id", 
            "title_name" => "p.title", 
            "p.desc", 
            "g.department_id", 
            "p.group_id",
            "b.quater",
            "b.year",
            "b.cost",
            "b.project",
            "budget_id" => "b.id"
        );

        $select->from(array('p' => 'request_type'), $arrCols);
        $select->joinLeft(array('g' => 'request_type_group'), 'g.id = p.group_id', array());
        $select->joinLeft(array('b' => 'request_office_budget'), 'b.request_type = p.id', array());
        $select->where("p.is_del = 0 AND g.is_del = 0");
        $select->where('g.department_id = ?', $params['department_id']);
        $select->where('b.year = ?', $params['year']);
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getListProject($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.project_name",
            "p.department_id"
        );

        $select->from(array('p' => 'request_project'), $arrCols);

        $select->where('p.department_id = ?', $params['department_id']);
        $select->where('p.status = ?', 1);
        $result = $db->fetchAll($select);

        return $result;
    }

    public function getBudget($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.request_type",
            "cost" => "SUM(p.cost)",
            //"p.quarter",
            "p.year",
        );

        $select->from(array('p' => 'request_office_budget'), $arrCols);
        $select->joinLeft(array('t' => 'request_type'), 't.id = p.request_type', array());
        $select->joinLeft(array('g' => 'request_type_group'), 'g.id = t.group_id', array());
        
        $select->where('g.department_id = ?', $params['department_id']);
        //$select->where('p.quarter IN (?)', $params['quarter']);
        
        $select->group('t.id');
        $result = $db->fetchAll($select);
        
        return $result;
    }
    
    public function getUse($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.company_id", 
            "p.category_id", 
            "p.department_id", 
            "p.request_type", 
            "total_use" => "SUM(p.cost_temp)"
        );

        $select->from(array('p' => DATABASE_SALARY.'.request_office'), $arrCols);
        $select->joinLeft(array('c' => 'request_confirm'), 'c.request_id = p.id	AND c.title_confirm = "Chờ BOD duyệt"', array());
        $select->where('p.del = 0');
        //$select->where('c.is_confirm = 1');
        $select->where('p.department_id = ?', $params['department_id']);
        //$select->where('p.month_fee IN (?)', $params['months']);
        //$select->where('p.year_fee = ?', 2020);
        
        $select->group('p.request_type');
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getListPaymentUse($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.company_id", 
            "p.category_id", 
            "p.department_id", 
            "p.request_type", 
            "p.cost_before",
            "p.content",
            "p.status",
            "is_confirm" => "IF(c.is_confirm = 1, 1, 0)"
        );

        $select->from(array('p' => DATABASE_SALARY.'.request_office'), $arrCols);
        $select->joinLeft(array('c' => 'request_confirm'), 'c.request_id = p.id	AND c.title_confirm = "Chờ BOD duyệt"', array());
        
        $select->where('p.del = 0');
        $select->where('p.department_id = ?', $params['department_id']);
        $select->where('p.month_fee IN (?)', [1,2,3,4]);
        $select->where('p.year_fee = ?', 2020);
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getDepartment(){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.name",
        );

        $select->from(array('p' => 'team'), $arrCols);
        
        $select->where('p.parent_id = 0');
        $select->where('p.is_hidden = 0');
        $select->where('p.del = 0');
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getListRequestType($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.title",
            "p.group_id",
            "p.is_del"
        );

        $select->from(array('p' => 'request_type'), $arrCols);
        $select->joinLeft(array('g' => 'request_type_group'), 'g.id = p.group_id', array());
        
        $select->where('g.department_id = ?', $params['department_id']);
        $select->where('g.is_del = ?', 0);
        
        $result = $db->fetchAll($select);

        return $result;
    }

    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');
        if ($result === false) {
            $data = $this->fetchAll();
            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->title;
                }                
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }
}