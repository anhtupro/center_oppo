<?php

class Application_Model_SelloutPartner extends Zend_Db_Table_Abstract {	

    protected $_name = 'sellout_partner';

    public function fetchPagination($page, $limit, &$total, $params) {
        $db = Zend_Registry::get('db');

        $select = $db->select()
                ->from(array('p' => 'sellout_partner'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'))
                ->join(array('i'=>'warehouse.imei'),'i.imei_sn = p.imei',array())
                ->joinLeft(array('d' => 'warehouse.distributor'), 'CAST( p.`warehouse_id` AS CHAR( 100 ) ) COLLATE utf8_general_ci = d.partner_id', array('d.*'))
                ->joinLeft(array('r' => 'regional_market'), ' d.region=r.id', array('r.*'))
        ;
        if (!empty($params['imei'])) {
            $select->where('p.imei = ?', $params['imei']);
        }
         if (!empty($params['good_id'])) {
            $select->where('i.good_id in(?)', $params['good_id']);
        }

        if (!empty($params['warehouse_id'])) {
            $select->where('p.warehouse_id = ?', $params['warehouse_id']);
        }

        if (!empty($params['bill_date_from'])) {
            $select->where('p.bill_date >= ?', $params['bill_date_from']);
        }

        if (!empty($params['bill_date_to'])) {
            $select->where('p.bill_date <= ?', $params['bill_date_to']);
        }

        if (!empty($params['created_at_from'])) {
            $select->where('p.created_at >= ?', $params['created_at_from']);
        }

        if (!empty($params['created_at_to'])) {
            $select->where('p.created_at <= ?', $params['created_at_to']);
        }
        if (!empty($params['created_at_to'])) {
            $select->where('p.created_at <= ?', $params['created_at_to']);
        }
        if (!empty($params['area_id'])) {
            $select->where('r.area_id in (?)', $params['area_id']);
        }

        $select->order('created_at DESC');

        if (empty($params['export'])) {
            $select->limitPage($page, $limit);
        } else {
            return $select;
        }
        
        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    public function fetchMap($params) {
        $db = Zend_Registry::get('db');

        $select = $db->select()
                ->from(array('st' => 'store'), array('store_id'=>'st.id' ,'sellout'=>'COUNT(DISTINCT p.imei_sn)','store_name'=> 'st.name','sale_photo'=>'s.photo' ,'sale_id'=>'s.id','sale_name'=>'CONCAT(s.firstname," ",s.lastname)','s.phone_number','pg_photo'=>'GROUP_CONCAT(DISTINCT concat(s2.id,"/",s2.photo))'
                    ,'pg_id'=>'GROUP_CONCAT(DISTINCT s2.id)','dealer'=> 'd.title'
,'st.latitude','st.longitude' ,'d.is_ka','store_trade'=>'GROUP_CONCAT(DISTINCT ip.photo)','store_trade'=>'GROUP_CONCAT(DISTINCT ip.photo)','trade_imei'=>'GROUP_CONCAT(DISTINCT ip.imei_sn)'))
                ->joinLeft(array('p' => 'imei_kpi'), 'st.id=p.store_id and timing_date >= "' . $params['sell_out_from'] . '" AND p.timing_date <= "' . $params['sell_out_to'] .'"', array())
                ->join(array('r' => 'regional_market'), 'r.id=st.regional_market', array())
                ->join(array('d' => 'warehouse.distributor'), ' d.id=st.d_id', array())
                ->joinLeft(array('l' => 'store_staff_log'), 'l.store_id= st.id AND  l.is_leader=1 AND l.released_at IS NULL', array())
                ->joinLeft(array('s' => 'staff'), 'l.staff_id=s.id', array())
                ->joinLeft(array('l2' => 'store_staff_log'), 'l2.store_id= st.id AND  l2.is_leader=0 AND l2.released_at IS NULL', array())
                ->joinLeft(array('s2' => 'staff'), 'l2.staff_id=s2.id', array())
                 ->joinLeft(array('ti' => 'trade_marketing.imei'), 'ti.object_id= st.id ', array())
                ->joinLeft(array('ip' => 'trade_marketing.imei_photo'), 'ip.imei_sn=ti.imei_sn  and ip.type=3 and ip.photo is not null', array())
        ;
        $select->where('st.latitude IS NOT NULL AND (st.del IS NULL OR st.del=0) AND (d.del IS NULL OR d.del=0)');
         if (!empty($params['area_id'])) {
            $select->where('r.area_id in (?)', $params['area_id']);
        }
        $select->group('st.id');
        if (!empty($params['min'])) {
            $select->having('sellout >= ?', $params['min']);
        }
       

        $result = $db->fetchAll($select);


        return $result;
    }
	
	public function bulkInsertImei($params) {
		$array_params = array();
		foreach($params as $value)
		{
			$array_params[] = "('" . $value['partner_account'] . "', '" . $value['ref'] . "', '" . $value['model'] . "', '" . $value['imei'] . "', '" . $value['bill_number'] . "', '" . $value['bill_date'] . "', '" . $value['warehouse_name']
				. "', '" . $value['warehouse_id'] . "', '" . $value['customer_name'] . "', '" . $value['customer_address']. "', '" . $value['customer_phone'] . "', '" . $value['note'] . "', '" . $value['created_at'] . "')";
		}
        $db = Zend_Registry::get('db');
		$sql_insert = "INSERT INTO `sellout_partner` (`partner_account`, `ref`, `model`, `imei`, `bill_number`, `bill_date`, `warehouse_name`, `warehouse_id`, `customer_name`, `customer_address`, `customer_phone`, `note`, `created_at`)
                        VALUES " . implode(",", $array_params) . "
                        ";
		//echo '<pre>'; print_r($sql_insert); die;
        $db->query($sql_insert);
	}

}

