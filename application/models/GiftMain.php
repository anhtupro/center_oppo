<?php
class Application_Model_GiftMain extends Zend_Db_Table_Abstract
{
    protected $_name = 'gift_main';

    public function fetchPagination($page, $limit, &$total, $params){
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $db         = Zend_Registry::get("db");
        $select     = $db->select();
        $arrCols    = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            "p.name", "p.desc", "p.from_date", "p.to_date",
            "fullname"      => "CONCAT(s.firstname, ' ', s.lastname)",
            "gift_storage"  => "COUNT(m.main_id)"
        );
        $select->from(array('p' => $this->_name), $arrCols);
        $select->joinLeft(array('s' => 'staff'), 's.id = p.created_by', array());
        $select->joinLeft(array('m' => 'gift_main_details'), 'm.main_id = p.id', array());
        $select->where('p.del = ?', 0);
        $select->group('m.main_id');
        $select->order('p.created_at DESC');

        if($params['sales_admin']){
            $select->where('p.from_date < ?', date('Y-m-d H:i:s'));
            $select->where('p.to_date > ?', date('Y-m-d H:i:s'));
        }

        if(empty($params['export'])){
            $select->limitPage($page, $limit);
        }
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }
}