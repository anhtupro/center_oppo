<?php

class Application_Model_ExportKpi extends Zend_Db_Table_Abstract
{
    public function exportPolicySummary($params)
    {

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();


        // get data
        $db = Zend_Registry::get("db");

        $stmt_out = $db->prepare("CALL `sp_export_policy_summary`(:p_from_date, :p_to_date, :p_from_date_time, :p_to_date_time)");
        $stmt_out->bindParam('p_from_date', $params['from_date']);
        $stmt_out->bindParam('p_to_date', $params['to_date']);
        $stmt_out->bindParam('p_from_date_time', $params['from_date_time']);
        $stmt_out->bindParam('p_to_date_time', $params['to_date_time']);
        $stmt_out->execute();
        $data = $stmt_out->fetchAll();
        $stmt_out->closeCursor();
        $stmt_out = null;




    // export
        $heads = array(
            'TITLE',
            'POLICY',
            'SELL-OUT',
            'KPI'
        );


        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;

        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($data as $item){
            $alpha    = 'A';

            $sheet->setCellValue($alpha++.$index, $item['title']);
            $sheet->setCellValue($alpha++.$index, $item['policy']);
            $sheet->setCellValue($alpha++.$index, $item['sell_out']);
            $sheet->setCellValue($alpha++.$index, $item['kpi']);

            $index++;
        }

        $filename = 'Sellout - Policy Summary ' . $params['time'];
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }

    public function exportPolicy($params)
    {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();


        // get data
        $db = Zend_Registry::get("db");

        $stmt_out = $db->prepare("CALL `sp_export_policy`(:p_from_date, :p_to_date, :p_from_date_time, :p_to_date_time)");
        $stmt_out->bindParam('p_from_date', $params['from_date']);
        $stmt_out->bindParam('p_to_date', $params['to_date']);
        $stmt_out->bindParam('p_from_date_time', $params['from_date_time']);
        $stmt_out->bindParam('p_to_date_time', $params['to_date_time']);
        $stmt_out->execute();
        $data = $stmt_out->fetchAll();
        $stmt_out->closeCursor();
        $stmt_out = null;



        // export
        $heads = array(
            'Staff_id',
            'Code',
            'Area',
            'Staff',
            'Chính sách  PG - TGDD',
            'Chính sách PG - Other KA+IND',
            'Chính sách SALES - TGDD',
            'Chính sách SALES - OTHER KA+IND',
            'Chính sách Store Leader - Brandshop không trong Mall',
            'Chính sách Store Leader - Brandshop trong Mall',
            'Chính sách Product Consultant - Brandshop không trong Mall',
            'Chính sách Product Consultant - Brandshop trong Mall',
            'Total'
        );


        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;

        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($data as $item){
            $alpha    = 'A';

            $sheet->setCellValue($alpha++.$index, $item['staff_id']);
            $sheet->setCellValue($alpha++.$index, $item['code']);
            $sheet->setCellValue($alpha++.$index, $item['area']);
            $sheet->setCellValue($alpha++.$index, $item['staff']);
            $sheet->setCellValue($alpha++.$index, $item['policy_pg_tgdd']);
            $sheet->setCellValue($alpha++.$index, $item['policy_pg_diffrent_tgdd']);
            $sheet->setCellValue($alpha++.$index, $item['policy_sale_tgdd']);
            $sheet->setCellValue($alpha++.$index, $item['policy_sale_diffrent_tgdd']);
            $sheet->setCellValue($alpha++.$index, $item['policy_store_leader']);
            $sheet->setCellValue($alpha++.$index, $item['policy_store_leader_in_mall']);
            $sheet->setCellValue($alpha++.$index, $item['policy_consultant']);
            $sheet->setCellValue($alpha++.$index, $item['policy_consultant_in_mall']);
            $sheet->setCellValue($alpha++.$index, $item['total']);

            $index++;
        }

        $filename = 'Sellout - Policy ' . $params['time'];
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }

    public function exportStaffDetail($params)
    {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        // get data
        $db = Zend_Registry::get("db");

        $stmt_out = $db->prepare("CALL `sp_export_staff_detail`(:p_from_date, :p_to_date, :p_from_date_time, :p_to_date_time)");
        $stmt_out->bindParam('p_from_date', $params['from_date']);
        $stmt_out->bindParam('p_to_date', $params['to_date']);
        $stmt_out->bindParam('p_from_date_time', $params['from_date_time']);
        $stmt_out->bindParam('p_to_date_time', $params['to_date_time']);
        $stmt_out->execute();
        $data = $stmt_out->fetchAll();
        $stmt_out->closeCursor();
        $stmt_out = null;


        // export
        $heads = array(
            'Staff_id',
            'Code',
            'Staff',
            'Area',
            'Title',
            'Team',
            'Model',
            'Color',
            'Policy',
            'Sell-out',
            'KPI',
            'Total'
        );


        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;

        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($data as $item){
            $alpha    = 'A';

            $sheet->setCellValue($alpha++.$index, $item['staff_id']);
            $sheet->setCellValue($alpha++.$index, $item['code']);
            $sheet->setCellValue($alpha++.$index, $item['staff']);
            $sheet->setCellValue($alpha++.$index, $item['area']);
            $sheet->setCellValue($alpha++.$index, $item['title']);
            $sheet->setCellValue($alpha++.$index, $item['team']);
            $sheet->setCellValue($alpha++.$index, $item['model']);
            $sheet->setCellValue($alpha++.$index, $item['color']);
            $sheet->setCellValue($alpha++.$index, $item['policy']);
            $sheet->setCellValue($alpha++.$index, $item['sell_out']);
            $sheet->setCellValue($alpha++.$index, $item['kpi']);
            $sheet->setCellValue($alpha++.$index, $item['total']);


            $index++;
        }

        $filename = 'Sellout - Staff Detail ' . $params['time'];
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }

    public function exportStaff($params)
    {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();


        // get data
        $db = Zend_Registry::get("db");

        $stmt_out = $db->prepare("CALL `sp_export_staff`(:p_from_date, :p_to_date, :p_from_date_time, :p_to_date_time)");
        $stmt_out->bindParam('p_from_date', $params['from_date']);
        $stmt_out->bindParam('p_to_date', $params['to_date']);
        $stmt_out->bindParam('p_from_date_time', $params['from_date_time']);
        $stmt_out->bindParam('p_to_date_time', $params['to_date_time']);
        $stmt_out->execute();
        $data = $stmt_out->fetchAll();
        $stmt_out->closeCursor();
        $stmt_out = null;



        // export
        $heads = array(
            'Staff_id',
            'Code',
            'Staff',
            'Area',
            'KPI PG',
            'Sell-out PG',
            'KPI SALES',
            'Sell-out SALES',
            'KPI STORE LEADER',
            'Sell-out STORE LEADER',
            'KPI CONSULTANT',
            'Sell-out CONSULTANT',

        );


        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;

        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($data as $item){
            $alpha    = 'A';

            $sheet->setCellValue($alpha++.$index, $item['staff_id']);
            $sheet->setCellValue($alpha++.$index, $item['code']);
            $sheet->setCellValue($alpha++.$index, $item['staff']);
            $sheet->setCellValue($alpha++.$index, $item['area']);

            $sheet->setCellValue($alpha++.$index, $item['kpi_pg']);
            $sheet->setCellValue($alpha++.$index, $item['sell_out_pg']);
            $sheet->setCellValue($alpha++.$index, $item['kpi_sale']);
            $sheet->setCellValue($alpha++.$index, $item['sell_out_sale']);
            $sheet->setCellValue($alpha++.$index, $item['kpi_store_leader']);
            $sheet->setCellValue($alpha++.$index, $item['sell_out_store_leader']);
            $sheet->setCellValue($alpha++.$index, $item['kpi_consultant']);
            $sheet->setCellValue($alpha++.$index, $item['sell_out_consultant']);

            $index++;
        }

        $filename = 'Sellout - Staff ' . $params['time'];
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;



    }

    public function exportKpiSaleOnline($params)
    {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();


        // get data
        $db = Zend_Registry::get("db");

        $stmt_out = $db->prepare("CALL `sp_export_kpi_sale_online`(:p_from_date_time, :p_to_date_time)");
        $stmt_out->bindParam('p_from_date_time', $params['from_date_time']);
        $stmt_out->bindParam('p_to_date_time', $params['to_date_time']);
        $stmt_out->execute();
        $data = $stmt_out->fetchAll();
        $stmt_out->closeCursor();
        $stmt_out = null;


        // export
        $heads = array(
           'sn',
            'Model Code',
            'Model',
            'Color',
            'Imei',
            'Warehouse',
            'Distributor',
            'Distributor code',
            'Partner ID',
            'Area',
            'Province',
            'Out date',
            'Activated date',
            'Giá thực tế',
            'Invoice number',
            'Invoice time',
            'Invoice sign',
            'Return sn',
            'Remark',
            'Value',
            'Value 80%'
        );


        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;

        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($data as $item){
            $alpha    = 'A';

            $sheet->getCell($alpha++ . $index)->setValueExplicit($item['sn'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alpha++.$index, $item['model_code']);
            $sheet->setCellValue($alpha++.$index, $item['model']);
            $sheet->setCellValue($alpha++.$index, $item['color']);
            $sheet->getCell($alpha++ . $index)->setValueExplicit($item['imei_sn'], PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue($alpha++.$index, $item['warehosue']);
            $sheet->setCellValue($alpha++.$index, $item['distrubutor']);
            $sheet->setCellValue($alpha++.$index, $item['distributor_code']);
            $sheet->setCellValue($alpha++.$index, $item['partner_id']);
            $sheet->setCellValue($alpha++.$index, $item['area']);
            $sheet->setCellValue($alpha++.$index, $item['provine']);
            $sheet->setCellValue($alpha++.$index, $item['out_date']);
            $sheet->setCellValue($alpha++.$index, $item['activated_date']);
            $sheet->setCellValue($alpha++.$index, $item['gia_thuc_te']);
            $sheet->setCellValue($alpha++.$index, $item['invoice_number']);
            $sheet->setCellValue($alpha++.$index, $item['invoice_time']);
            $sheet->setCellValue($alpha++.$index, $item['invoice_sign']);
            $sheet->setCellValue($alpha++.$index, $item['return_sn']);
            $sheet->setCellValue($alpha++.$index, $item['remark']);
            $sheet->setCellValue($alpha++.$index, $item['value']);
            $sheet->setCellValue($alpha++.$index, $item['value_80']);


            $index++;
        }

        $filename = 'Sellout SALE ONLINE ' . $params['time'];
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }

    public function exportFinalStorePg($params)
    {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();


        // get data
        $db = Zend_Registry::get("db");

        $stmt_out = $db->prepare("CALL `sp_export_final_store_pg`(:p_from_date, :p_to_date)");
        $stmt_out->bindParam('p_from_date', $params['from_date']);
        $stmt_out->bindParam('p_to_date', $params['to_date']);
        $stmt_out->execute();
        $data = $stmt_out->fetchAll();
        $stmt_out->closeCursor();
        $stmt_out = null;


        // export
        $heads = array(
            'Staff ID',
            'Code',
            'Staff',
            'Title',
            'Final Store',
            'Final Store_ID',
            'Final district',
            'Final province',
            'Final area',
            'Max sellout'
        );


        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;

        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($data as $item){
            $alpha    = 'A';

            $sheet->setCellValue($alpha++.$index, $item['staff_id']);
            $sheet->setCellValue($alpha++.$index, $item['code']);
            $sheet->setCellValue($alpha++.$index, $item['staff_name']);
            $sheet->setCellValue($alpha++.$index, $item['title']);
            $sheet->setCellValue($alpha++.$index, $item['final_store']);
            $sheet->setCellValue($alpha++.$index, $item['final_store_id']);
            $sheet->setCellValue($alpha++.$index, $item['final_district']);
            $sheet->setCellValue($alpha++.$index, $item['final_province']);
            $sheet->setCellValue($alpha++.$index, $item['final_area']);
            $sheet->setCellValue($alpha++.$index, $item['max_sellout']);

            $index++;
        }

        $filename = 'PG đứng shop có sellout nhiều nhất ' . $params['time'];
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }


    public function exportChannel($params)
    {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();


        // get data
        $db = Zend_Registry::get("db");

        $stmt_out = $db->prepare("CALL `sp_export_channel`(:p_from_date_time, :p_to_date_time)");
        $stmt_out->bindParam('p_from_date_time', $params['from_date_time']);
        $stmt_out->bindParam('p_to_date_time', $params['to_date_time']);
        $stmt_out->execute();
        $data = $stmt_out->fetchAll();
        $stmt_out->closeCursor();
        $stmt_out = null;


        // export
        $heads = array(
            'Staff ID',
            'Code',
            'Staff',
            'Title',
            'Area',
            'Province',
            'District',
            'Store',
            'Store Address',
            'Dealer',
            'Dealer Address',
            'Dealer ID',
            'Dealer Type',
            'Joined at',
            'Released at',
            'From date',
            'To date'
        );


        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;

        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($data as $item){
            $alpha    = 'A';

            $sheet->setCellValue($alpha++.$index, $item['staff_id']);
            $sheet->setCellValue($alpha++.$index, $item['staff_code']);
            $sheet->setCellValue($alpha++.$index, $item['staff_name']);
            $sheet->setCellValue($alpha++.$index, $item['title']);
            $sheet->setCellValue($alpha++.$index, $item['area_name']);
            $sheet->setCellValue($alpha++.$index, $item['province_name']);
            $sheet->setCellValue($alpha++.$index, $item['district_name']);
            $sheet->setCellValue($alpha++.$index, $item['store_name']);
            $sheet->setCellValue($alpha++.$index, $item['store_address']);
            $sheet->setCellValue($alpha++.$index, $item['dealer_name']);
            $sheet->setCellValue($alpha++.$index, $item['d_add']);
            $sheet->setCellValue($alpha++.$index, $item['dealer_id']);
            $sheet->setCellValue($alpha++.$index, $item['dealer_type']);
            $sheet->setCellValue($alpha++.$index, $item['joined_at']);
            $sheet->setCellValue($alpha++.$index, $item['released_at']);
            $sheet->setCellValue($alpha++.$index, $item['from_date']);
            $sheet->setCellValue($alpha++.$index, $item['to_date']);


            $index++;
        }

        $filename = 'Channel ' . $params['time'];
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }


    public function getDataChannelSystem($params)
    {
        $from_date_time = $params['from_date_time'];
        $to_date_time = $params['to_date_time'];
        $table_staff = 'salary.staff_' . intval($params['month']) . '_' . $params['year'];


        $db = Zend_Registry::get("db");

        $query = "
                          
                SELECT 
                s.code,
                t.name AS title,
                CONCAT(s.firstname,\" \",s.lastname) AS staff_name,
                st.*,
                rm.name AS district,
                rm2.name AS province, 
                ar.name AS 'area', 
                rm3.name AS center_district,
                MAX(ss.name) AS final_store
                
                FROM 
                (
                SELECT s.id as staff_id,
                IFNULL(k.name,'NA') as store_name,
                SUM(IFNULL(k.total_store,0)) AS total_store
                , ROUND(MAX((k.total*100000000 + k.basic_insurance_salary)*10000 + k.district)%10000) AS final_district
                , MONTH('$from_date_time') AS `month`,YEAR('$from_date_time') AS `year`
                FROM
                staff s 
                LEFT JOIN 
                 (
                SELECT v.staff_id,v.name,v.district,v.district_name,SUM(v.diff) AS total,v.basic_insurance_salary
                 ,COUNT(DISTINCT v.store_id) AS total_store
                FROM(
                SELECT staff_id
                    ,spd.basic_insurance_salary
                    ,sl.store_id,s.district,rm.name AS district_name,FROM_UNIXTIME(`joined_at`) AS START,FROM_UNIXTIME(`released_at`) AS END
                ,@start:=CASE WHEN '$from_date_time' > FROM_UNIXTIME(`joined_at`) THEN DATE_FORMAT(DATE('$from_date_time'),'%Y-%m-%d %H:%i:%s') ELSE FROM_UNIXTIME(`joined_at`) END AS start_in_time
                ,@end:= CASE WHEN '$to_date_time' < FROM_UNIXTIME(`released_at`) OR `released_at` IS NULL THEN DATE_ADD('$to_date_time', INTERVAL 1 DAY) ELSE FROM_UNIXTIME(`released_at`) END AS end_in_time 
                ,@diff:=DATEDIFF(@end, @start) AS diff
                 ,s.name
                FROM `store_staff_log` sl
                JOIN store s ON s.id=sl.store_id
                JOIN regional_market rm ON rm.id= s.district
                JOIN salary_policy_detail spd ON spd.district_id= s.district AND spd.title_id=183 AND spd.to_date IS NULL
                JOIN (SELECT @diff :=0,@start:='$from_date_time',@end :='$to_date_time') AS diff
                WHERE 1=1 
                AND sl.`is_leader`IN(0,3,7)
                AND sl.`joined_at` < UNIX_TIMESTAMP('$to_date_time') AND (`released_at` IS NULL OR `released_at` >UNIX_TIMESTAMP('$from_date_time'))
                ) AS v
                GROUP BY v.staff_id ,v.district
                    ) AS k  ON s.id= k.staff_id
                WHERE k.staff_id IS NOT NULL
                GROUP BY k.staff_id
                ) st
                -- JOIN hr.staff s ON s.id=st.id 
                 JOIN $table_staff s ON s.id = st.staff_id
                JOIN hr.regional_market rm ON rm.id=st.`final_district`
                JOIN hr.regional_market rm2 ON rm2.id=rm.parent
                JOIN hr.`area` ar ON ar.id=rm2.area_id
                JOIN hr.regional_market rm3 ON rm3.id=s.district
                JOIN hr.team t ON t.id= s.title
                LEFT JOIN store_staff_log sl ON sl.`staff_id`= s.`id` AND  sl.`joined_at` <= UNIX_TIMESTAMP('$to_date_time') AND (sl.`released_at` IS NULL OR sl.`released_at` >=UNIX_TIMESTAMP('$from_date_time'))
                LEFT JOIN store ss ON ss.id= sl.`store_id` AND ss.district= st.`final_district`
                GROUP BY s.id
        ";


        $stmt = $db->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $db = $stmt = null;


        return $data;


    }

    public function exportChannelSystem($params)
    {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $data = $this->getDataChannelSystem($params);



        // export
        $heads = array(
            'Staff ID',
            'Code',
            'Staff',
            'Title',
           'Store',
            'Total store',
            'Final district',
            'Month',
            'Year',
            'District',
            'Province',
            'Area',
            'Center district',
            'Final Store'
        );


        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;

        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($data as $item){
            $alpha    = 'A';

            $sheet->setCellValue($alpha++.$index, $item['staff_id']);
            $sheet->setCellValue($alpha++.$index, $item['code']);
            $sheet->setCellValue($alpha++.$index, $item['staff_name']);
            $sheet->setCellValue($alpha++.$index, $item['title']);
            $sheet->setCellValue($alpha++.$index, $item['store_name']);
            $sheet->setCellValue($alpha++.$index, $item['total_store']);
            $sheet->setCellValue($alpha++.$index, $item['final_district']);
            $sheet->setCellValue($alpha++.$index, $item['month']);
            $sheet->setCellValue($alpha++.$index, $item['year']);
            $sheet->setCellValue($alpha++.$index, $item['district']);
            $sheet->setCellValue($alpha++.$index, $item['province']);
            $sheet->setCellValue($alpha++.$index, $item['area']);
            $sheet->setCellValue($alpha++.$index, $item['center_district']);
            $sheet->setCellValue($alpha++.$index, $item['final_store']);


            $index++;
        }

        $filename = 'Channel System ' . $params['time'];
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }
}