<?php
class Application_Model_CheckInShift extends Zend_Db_Table_Abstract
{
    protected $_name = 'check_in_shift';

    public function insertCheckInShift($values)
    {
        $sql = "INSERT INTO `check_in_shift`
                (`staff_code`, `begin`, `end`, `date`, `status`)
                VALUES " . $values . "
                ON duplicate key UPDATE `status` = VALUES(`status`)";
        $db = Zend_Registry::get("db");
        $stmt = $db->prepare($sql);

        $stmt->execute();
        $stmt->closeCursor();

        $stmt = $db = null;
    }

    public function updateCheckInShift($values)
    {
        $sql = "INSERT INTO `check_in_shift`
                (`id`, `staff_code`, `begin`, `end`, `date`, `status`)
                VALUES " . $values . "
                ON duplicate key UPDATE `status` = VALUES(`status`),
                                        `staff_code` = VALUES(`staff_code`),
                                        `begin` =  VALUES(`begin`),
                                        `end` = VALUES(`end`),
                                        `date` = VALUES(`date`)";
                                        
        $db = Zend_Registry::get("db");
        $stmt = $db->prepare($sql);

        $stmt->execute();
        $stmt->closeCursor();

        $stmt = $db = null;
    }

    public function fetchPagination($param = array())
    {
        $db = Zend_Registry::get("db");
        $stmt = $db->prepare("CALL `get_check_in_shift`(:code, :date, :status, :limit, :offset)");

        $stmt->bindParam('code', $param['code'], PDO::PARAM_STR);
        $stmt->bindParam('date', $param['date'], PDO::PARAM_STR);
        $stmt->bindParam('status', $param['status'], PDO::PARAM_INT);
        $stmt->bindParam('limit', $param['limit'], PDO::PARAM_INT);
        $stmt->bindParam('offset', $param['offset'], PDO::PARAM_INT);

        $stmt->execute();

        $data['data'] = $stmt->fetchAll();

        $stmt->closeCursor();

        $data['total'] =$db->query("SELECT FOUND_ROWS()")->fetchColumn();

        $db = $stmt = null;

        return $data;
    }
}