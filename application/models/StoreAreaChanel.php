<?php
class Application_Model_StoreAreaChanel extends Zend_Db_Table_Abstract
{
	protected $_name = 'store_area_chanel';

    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');
        
        $currentTime = date('Y-m-d H:i:s');
        
        $select = $db->select()
            ->from(array('a' => $this->_name), array('from'=>'a.from_date','to'=>'a.to_date','area_id'=>'a.area_id'))
            ->joinLeft(array('b' => 'store'), 'b.id = a.store_id', array('store_name'=>'b.name', 'company_address'=>'b.company_address', 'store_id'=>'b.id' ))
            ->joinLeft(array('c' => 'area'), 'c.id = a.area_id', array('area_chanel'=>'c.name'))
            ->joinLeft(array('d' => 'regional_market'), 'd.id = b.district', array('district_name'=>'d.name'))
            ->joinLeft(array('e' => 'regional_market'), 'e.id = d.parent', array())
            ->joinLeft(array('f' => 'area'), 'f.id = e.area_id', array('area_name'=>'f.name'));
        
        //Area chanel
        if(isset($params['area_id']) && $params['area_id'])
        {
            $select->where('a.area_id = ?', $params['area_id']);
        }
        
        if(isset($params['store']) && $params['store'])
        {
            $select->where('b.name like ?', "%".$params['store']."%");
        }
        
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

}
