<?php 
class Application_Model_SurveyCollect extends Zend_Db_Table_Abstract
{
    protected $_name = 'survey_collect';

    public function getAllRecord($survey_id)
    {
    	$db = Zend_Registry::get('db');
    	
    	$sql = "SELECT CONCAT(st.firstname, ' ', st.lastname) AS fullname, sc.question_index,
		    	st.code AS code, 
		    	st.id_number AS id_number,
		    	GROUP_CONCAT(answer SEPARATOR ' ; ') AS answer,
		    	d.name AS department,
		    	t.name AS team,
		    	c.name AS title,
		    	st.joined_at AS join_date,
		    	a.name AS area,
		    	st.id AS staff_id,
		    	cp.name AS company,
		    	sc.time AS time,
		    	sc.survey_history_id,
		    	sc.created_at

		    	FROM survey_collect AS sc
		    	LEFT JOIN staff AS st ON sc.staff_id = st.id
		    	LEFT JOIN team AS d ON st.department = d.id
		    	LEFT JOIN team AS t ON st.team = t.id
		    	LEFT JOIN team AS c ON st.title = c.id
		    	LEFT JOIN regional_market AS rm ON st.regional_market = rm.id
		    	LEFT JOIN area AS a ON rm.area_id = a.id
		    	LEFT JOIN company AS cp ON st.company_id = cp.id

		    	WHERE survey_id = $survey_id AND sc.checkbox_option IS NULL
		    	
		    	GROUP BY staff_id, question_index, time
		    	ORDER BY sc.id";

    	$stmt = $db->prepare($sql);    
    	$stmt->execute();
    	$result = $stmt->fetchAll();
    	$stmt->closeCursor();

    	return $result;
    }

    public function countTimesSubmit($staff_id, $survey_id)
    {
    	$db = Zend_Registry::get('db');

    	$sql = " SELECT COUNT(id) AS times
			    FROM
			    	(
			    	SELECT id 
			    	FROM survey_collect 
			    	WHERE staff_id = $staff_id
			    	AND survey_id = $survey_id
			    	GROUP BY staff_id, time
			    	) AS c ";

	    $stmt = $db->prepare($sql);    
	    $stmt->execute();
	    $result = $stmt->fetchAll();  
	    $stmt->closeCursor();

	    $times = $result[0]['times'];
	    
	    return $times;
    }

    public function getSortAnswer($survey_id)
    {
        $db = Zend_Registry::get('db');

    	$sql = " SELECT CONCAT(st.firstname, ' ', st.lastname) AS fullname, sc.question_index,
		    	st.code AS code, 
		    	st.id_number AS id_number,
		    	sc.answer,
		    	d.name AS department,
		    	t.name AS team,
		    	c.name AS title,
		    	st.joined_at AS join_date,
		    	a.name AS area,
		    	st.id AS staff_id,
		    	cp.name AS company,
		    	sc.time AS time

		    	FROM survey_collect AS sc
		    	LEFT JOIN staff AS st ON sc.staff_id = st.id
		    	LEFT JOIN team AS d ON st.department = d.id
		    	LEFT JOIN team AS t ON st.team = t.id
		    	LEFT JOIN team AS c ON st.title = c.id
		    	LEFT JOIN regional_market AS rm ON st.regional_market = rm.id
		    	LEFT JOIN area AS a ON rm.area_id = a.id
		    	LEFT JOIN company AS cp ON st.company_id = cp.id

		    	WHERE survey_id = 1 AND sc.checkbox_option IS NOT NULL ";

	    $stmt = $db->prepare($sql);    
	    $stmt->execute();
	    $answers = $stmt->fetchAll();  
	    $stmt->closeCursor();
	    
	    return $answers;
    }

    public function getSubmittedUsers($survey_id)
    {
        $db = Zend_Registry::get('db');

    	$sql = " SELECT staff_id
				FROM survey_collect
				WHERE survey_id = $survey_id
				GROUP BY staff_id ,survey_id";

	    $stmt = $db->prepare($sql);    
	    $stmt->execute();
	    $result = $stmt->fetchAll();  
	    $stmt->closeCursor();
	    
	    foreach ($result as $key => $value) {
	    	$submitted_users [] = $value['staff_id'];
	  	}

	    return $submitted_users;
    }
    
    
}    