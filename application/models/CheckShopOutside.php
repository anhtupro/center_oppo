<?php

class Application_Model_CheckShopOutside extends Zend_Db_Table_Abstract
{
    protected $_name = 'check_shop_outside';
    protected $_schema = DATABASE_TRADE;

    public function getHistory($storeId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['c' => DATABASE_TRADE . '.check_shop_outside'], [
                'check_shop_id' => 'c.id',
                'c.store_id',
                'created_at' => "DATE_FORMAT(c.created_at, '%d/%m/%d %H:%i:%s')"
            ])
            ->where('c.store_id = ?', $storeId)
            ->where('c.is_deleted = ?', 0)
            ->order('c.created_at DESC')
            ->limit(20);

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getCheckShopOutside($id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['c' => DATABASE_TRADE.'.check_shop_outside'], [
                         'district_name' => 'd.name',
                         'area_name' => 'a.name',
                         'province_name' => 'r.name',
                         'store_name' => 's.name',
                         'created_by' => "CONCAT(st.firstname, ' ', st.lastname)",
                         'c.created_at',
                         'c.last_check_shop_id'
                     ])
                    ->joinLeft(['st' => 'staff'], 'c.created_by = st.id', [])
                    ->joinLeft(['s' => DATABASE_TRADE.'.store_outside'], 'c.store_id = s.id', [])
                    ->joinLeft(['d' => 'regional_market'], 's.district = d.id', [])
                    ->joinLeft(['r' => 'regional_market'], 's.regional_market = r.id', [])
                    ->joinLeft(['a' => 'area'], 'r.area_id = a.id', [])
                    ->where('c.id = ?', $id);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getLatestCheckShop($storeId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['s' => DATABASE_TRADE.'.store_outside'], [
                'district_name' => 'd.name',
                'area_name' => 'a.name',
                'province_name' => 'r.name',
                'store_name' => 's.name',
                'created_by' => "CONCAT(st.firstname, ' ', st.lastname)",
                'c.created_at',
                'c.id'
            ])
            ->joinLeft(['c' => DATABASE_TRADE.'.check_shop_outside'], 's.id = c.store_id AND c.created_at = (SELECT max(created_at) FROM trade_marketing.check_shop_outside WHERE store_id = s.id)', [])
            ->joinLeft(['st' => 'staff'], 'c.created_by = st.id', [])
            ->joinLeft(['d' => 'regional_market'], 's.district = d.id', [])
            ->joinLeft(['r' => 'regional_market'], 's.regional_market = r.id', [])
            ->joinLeft(['a' => 'area'], 'r.area_id = a.id', [])
            ->where('s.id = ?', $storeId);

        $result = $db->fetchRow($select);
        return $result;
    }

    public function getImageCheckshop($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE.'.check_shop_outside'], [
                'checkshop_id' => 'a.id',
                'url' =>  "CONCAT('/', f.url)"
            ])
            ->joinLeft(['f' => DATABASE_TRADE.'.check_shop_outside_file'], 'a.id = f.check_shop_outside_id', []);

        if($params['checkshop_id']) {
            $select->where('a.id = ?', $params['checkshop_id']);
        }

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getInforShopMap($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE.'.check_shop_outside'], [
                'store_name' => 's.name',
                'area_name' => 'e.name',
                'province_name' => 'r.name',
                'district_name' => 'd.name'
//                         'sales_name' => "CONCAT(st.firstname, ' ', st.lastname)"
            ])
            ->joinLeft(['s' => DATABASE_TRADE.'.store_outside'], 'a.store_id = s.id', [])
//                     ->joinLeft(['v' => 'v_store_staff_leader_log'], 's.id = v.store_id AND v.is_leader = 1', [])
//                     ->joinLeft(['st' => 'staff'], 'v.staff_id = st.id', [])
            ->joinLeft(['r' => 'regional_market'], 's.regional_market = r.id', [])
            ->joinLeft(['e' => 'area'], 'r.area_id = e.id', [])
            ->joinLeft(['d' => 'regional_market'], 's.district = d.id', [])
            ->where('a.id = ?', $params['checkshop_id']);

        $result = $db->fetchAll($select);

        return $result[0];
    }
}