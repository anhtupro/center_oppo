<?php
/**
* @author buu.pham
* @create 2015-09-30T15:51:59+07:00
* @filename SalesRegion.php
*/
class Application_Model_SalesRegion extends Zend_Db_Table_Abstract
{
    
    protected $_name = 'sales_region';

  public function listAll(){
    return $this->fetchAll()->toArray();
  }
  public function findPrimaykey($id){
    // echo $id;die;
    return $this->fetchRow($this->select()->where('id = ?',$id));
  }

    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.name', 'p.parent', 'p.region_id', 'p.shared', 'p.email'));

        if (isset($params['name']) and $params['name'])
            $select->where('p.name LIKE ?', '%'.My_String::trim($params['name']).'%');

        if (isset($params['parent']) and $params['parent']) {
            if (is_array($params['parent']) && count($params['parent']))
                $select->where('p.parent IN (?)', $params['parent']);

            elseif (is_numeric($params['parent']) && intval($params['parent']))
                $select->where('p.parent = ?', intval($params['parent']));

            else
                $select->where('1=0', 1);
        }

        if (isset($params['region_id']) and $params['region_id']) {
            if (is_array($params['region_id']) && count($params['region_id']))
                $select->where('p.region_id IN (?)', $params['region_id']);

            elseif (is_numeric($params['region_id']) && intval($params['region_id']))
                $select->where('p.region_id = ?', intval($params['region_id']));

            else
                $select->where('1=0', 1);
        }

        if (isset($params['email']) and $params['email']) {
            if (is_array($params['email']) && count($params['email']))
                $select->where('p.email IN (?)', $params['email']);

            else
                $select->where('TRIM(BOTH FROM p.email) LIKE ?', My_String::trim(str_replace(EMAIL_SUFFIX, '', $params['email'])).EMAIL_SUFFIX);
        }

        $select->order('p.name', 'COLLATE utf8_unicode_ci ASC');

        if (isset($params['export']) && $params['export'])
            return $select->__toString();

        if ($limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);

        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    function get_name_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_name_cache');

        if ($result === false) {

            $data = $this->fetchAll(null, 'name');

            $result = array();
            if ($data)
                foreach ($data as $item)
                    $result[$item->id] = $item->name;

            $cache->save($result, $this->_name.'_name_cache', array(), null);
        }
        return $result;
    }

    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if ($result === false) {

            $data = $this->fetchAll(null, 'name');

            $result = array();
            if ($data)
                foreach ($data as $item)
                    $result[$item->id] = $item;

            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }

    public function _insert($params = array()){
        $params = array_merge(
            array(
                'name' => null,
                'parent' => null,
                'shared' => null,
                'type' => null,
                'from_date' => null,
                'to_date' => null,
                'full_name' => null
            ),
            $params
        );
        // echo "<pre>";print_r($params);die;
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare('CALL SP_sales_region_Insert(:name, :parent, :shared, :type, :from_date, :to_date, :full_name)');
        $stmt->bindParam('name', $params['name'], PDO::PARAM_STR);
        $stmt->bindParam('parent', $params['parent'], PDO::PARAM_INT);
        $stmt->bindParam('shared', $params['shared'], PDO::PARAM_STR);
        $stmt->bindParam('type', $params['type'], PDO::PARAM_STR);
        $stmt->bindParam('from_date', $params['from_date'], PDO::PARAM_STR);
        $stmt->bindParam('to_date', $params['to_date'], PDO::PARAM_STR);
        $stmt->bindParam('full_name', $params['full_name'], PDO::PARAM_STR);
        $stmt->execute();
        $res = $stmt->fetch();
        $res = $res['status'];
        // $res['status'] = $stmt->fetch();
        // $id = $db->query('select last_insert_id() id')->fetch();
        // $res['last_id'] = $id['id'];
        $stmt->closeCursor();
        $db = $stmt = null;
        return $res;
    }

    public function _update($params = array()){
        $params = array_merge(
            array(
                'id' => null,
                'parent' => null,
                'shared' => null,
                'to_date' => null
            ),
            $params
        );
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare('CALL SP_sales_region_Update(:id, :parent, :shared, :to_date)');
        $stmt->bindParam('id', $params['id'], PDO::PARAM_INT);
        $stmt->bindParam('parent', $params['parent'], PDO::PARAM_INT);
        $stmt->bindParam('shared', $params['shared'], PDO::PARAM_INT);
        $stmt->bindParam('to_date', $params['to_date'], PDO::PARAM_STR);
        $stmt->execute();
        $res = $stmt->fetch();
        $res = $res['status'];
        $stmt->closeCursor();
        $db = $stmt = null;
        return $res;
    }

    public function GetTotalShared($params = array()){
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare('
            SELECT SUM(s.shared) AS shared, a.region_share AS `limit`
            FROM sales_region s
            RIGHT JOIN area a ON s.parent = a.id
            LEFT JOIN sales_region_staff_log sr ON sr.sales_region_id = s.id
            WHERE parent = :parent AND sr.to_date IS NULL
        ');
        $stmt->bindParam('parent', $params['parent'], PDO::PARAM_INT);
        $stmt->execute();
        $res = $stmt->fetch();
        $db = $stmt = null;
        return $res;
    }

    public function _delete($params = array()){
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare('
            DELETE FROM sales_region WHERE id = :id
        ');
        $stmt->bindParam('id', $params['id'], PDO::PARAM_INT);
        $res = $stmt->execute();
        $db = $stmt = null;
        return $res;
    }
}