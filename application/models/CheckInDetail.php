<?php

    class Application_Model_CheckInDetail extends Zend_Db_Table_Abstract
    {
        public function fetchPagination($params = array())
        {
            $db = Zend_Registry::get('db');

            $userStorage = Zend_Auth::getInstance()->getStorage()->read();

            $stmt = $db->prepare("CALL `get_check_in_list`(:name, :code, :from_date, :to_date, :limit, :offset, :user_code)");

            $stmt->bindParam("name", $params['name'], PDO::PARAM_STR);
            $stmt->bindParam("code", $params['code'], PDO::PARAM_STR);
            $stmt->bindParam("from_date", $params['from_date'], PDO::PARAM_STR);
            $stmt->bindParam("to_date", $params[ 'to_date'], PDO::PARAM_STR);
            $stmt->bindParam("user_code", $userStorage->code, PDO::PARAM_STR);
            $stmt->bindParam("limit", $params['limit'], PDO::PARAM_INT);
            $stmt->bindParam("offset", $params['offset'], PDO::PARAM_INT);

            $stmt->execute();

            $data = array();

            $data['data'] = $stmt->fetchAll();

            $stmt->closeCursor();

            $data['total'] = $db->query("SELECT FOUND_ROWS()")->fetchColumn();
            $db = $stmt = null;
            
            return $data; 
        }

        public function getDetailByDate($date, $code)
        {
            $db = Zend_Registry::get('db');
            
            $sql = "SELECT *
                    FROM `check_in_detail` cd
                    WHERE cd.check_in_day = :date
                        AND cd.staff_code = :code";
            $stmt = $db->prepare($sql);
            $stmt->bindParam('date', $date, PDO::PARAM_STR);
            $stmt->bindParam('code', $code, PDO::PARAM_STR);

            $stmt->execute();

            $data = $stmt->fetch();
            $stmt->closeCursor();
            $db = $stmt = null;
            return $data;
        }
    }