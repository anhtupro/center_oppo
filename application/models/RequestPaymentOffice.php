<?php
class Application_Model_RequestPaymentOffice extends Zend_Db_Table_Abstract
{
    protected $_name = 'request_payment_office';
    public function getRealCost($idRequest){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SUM(p.payment_cost) as cost_real_sum'));
        $select->from(array('p' => 'request_payment_office'), $arrCols);
        $select->joinLeft(array('request_office' => DATABASE_SALARY.'.request_office'), 'p.request_id = request_office.id');
        $select->where('p.request_id=?', $idRequest);
        $select->group('p.request_id');
        $select->order('p.id DESC');
        $result = $db->fetchRow($select);
        return $result;
    }
    
     public function getInfoPayment($payment_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "request_id" => "r.id", 
            "r.payee", 
            "r.note", 
            "r.content",
            "p.payment_cost", 
            "p.payment_date", 
            "r.payment_type", 
            "currency" => "IF(r.currency = 2, 'USD', 'đồng')",
            "currency_name" => "IF(r.currency = 2, 'USD', 'VNĐ')",
            "created_by" => "CONCAT(s.firstname, ' ',s.lastname)" , 
            "department_name" => "t.name",
            "list_confirm" => "(GROUP_CONCAT(
                    DISTINCT CONCAT(CONCAT(s2.firstname, ' ', s2.lastname))
                    ORDER BY c.step
                    SEPARATOR '<br>'
            ))",
            "list_purcharsing_confirm" => "CONCAT('<br>', s3.firstname, ' ', s3.lastname,' (PR)<br>', s4.firstname, ' ', s4.lastname, ' (PR)')",
        );

        $select->from(array('p' => 'request_payment_office'), $arrCols);
        $select->joinLeft(array('r' => DATABASE_SALARY.'.request_office'), 'r.id = p.request_id', array());
        $select->joinLeft(array('s' => 'staff'), 's.id = r.created_by', array());
        $select->joinLeft(array('c' => 'request_confirm'), 'c.request_id = r.id AND c.is_confirm = 1', array());
        $select->joinLeft(array('s2' => 'staff'), 's2.id = c.staff_id', array());
        $select->joinLeft(array('t' => 'team'), 't.id = s.department', array());
        $select->joinLeft(array('purcharsing' => 'purchasing_request'), 'purcharsing.sn = r.pr_sn', array());
        $select->joinLeft(array('s3' => 'staff'), 's3.id = purcharsing.confirm_by', array());
        $select->joinLeft(array('s4' => 'staff'), 's4.id = purcharsing.approved_by', array());

        $select->where('p.id = ?', $payment_id);
        $result = $db->fetchRow($select);
        

        return $result;
    }
}