<?php
class Application_Model_SurveyStaff extends Zend_Db_Table_Abstract
{
    protected $_name = 'survey_staff';

    public function check($staff_id)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->distinct()
            ->from(array('so' => $this->_name), array('so.survey_id'))
            ->join(array('su' => 'survey'), 'su.id=so.survey_id AND su.status=1', array('su.type'))
            ->join(array('s' => 'staff'), 'so.object_id = s.id AND s.id = ' . intval($staff_id), array())
            ->joinLeft(array('sr' => 'survey_response'), 'su.id=sr.survey_id AND sr.staff_id = s.id', array())// servey làm 1 lần (khảo sát)
            ->where('sr.id IS NULL')
            ->where('su.id IN (18,19,20,21,22,23,28,29)',null)//khảo sát 2018
            ->limitPage(1, 1);

        $result = $db->fetchOne($select);//fetchOne lấy survey_id
        
        if($result){
            return $result;
        }

        $select_2 = $db->select()
            ->distinct()
            ->from(array('so' => $this->_name), array('so.survey_id','response_at'=>'DATE(sr.created_at)'))
            ->join(array('su' => 'survey'), 'su.id=so.survey_id AND su.status=1', array())
            ->join(array('s' => 'staff'), 'so.object_id = s.id AND s.id = ' . intval($staff_id), array())
            ->joinLeft(array('sr' => 'survey_response'), 'su.id = sr.survey_id AND sr.staff_id = s.id AND DATE(sr.created_at) = CURRENT_DATE()', array())// 1 ngày làm 1 lần
            ->where('sr.id IS NULL')
            ->where('su.id = ?',9)
            ->limitPage(1, 1);
        $result = $db->fetchRow($select_2);
        if($result){
            if(!$result['response_at']){
                return $result['survey_id'];
            }

        }
        return null;
    }

}
