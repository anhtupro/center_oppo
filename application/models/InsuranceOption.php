<?php
class Application_Model_InsuranceOption extends Zend_Db_Table_Abstract
{
    protected $_name = 'insurance_option';
    
    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');
    
        if ($result === false) {
    
//            $where = $this->getAdapter()->quoteInto('1 = 1', null);
    
            $data = $this->fetchAll(null);
    
            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->name;
                }
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }
    
}