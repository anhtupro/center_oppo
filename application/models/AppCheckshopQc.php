<?php
class Application_Model_AppCheckshopQc extends Zend_Db_Table_Abstract
{
    protected $_name = 'app_checkshop_qc';

    protected $_schema = DATABASE_TRADE;
    
    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if ($result === false) {

            $data = $this->fetchAll();

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->title;
                }
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }


}