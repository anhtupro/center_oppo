<?php
class Application_Model_PaySlipsTemp extends Zend_Db_Table_Abstract
{
    protected $_name = 'pay_slips_temp';

    function fetchPagination($page, $limit, &$total, $params){

        $db = Zend_Registry::get('db');

        $select = $db->select();

        $select->from(array('p'=> 'pay_slips'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id, p.*')));

        if(isset($params['maso_nv']) and $params['maso_nv']){
        	$select->where('p.maso_nv = ?', $params['maso_nv']);
        }

        if(isset($params['ho_ten']) and $params['ho_ten']){
        	$select->where("p.ho_ten LIKE ?", '%'.$params['ho_ten'].'%');
        }

        if(isset($params['month']) and $params['month']){
        	$select->where("p.month = ?", $params['month']);
        }

        if(isset($params['year']) and $params['year']){
        	$select->where("p.year = ?", $params['year']);
        }

        $select->order('p.created_at DESC');

        if(!empty($params['upload_data']) and $params['upload_data'] == 1){

        }
        else{
            if($limit) {
                $select->limitPage($page, $limit);
            }
        }

        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    function getData($params){
        $db = Zend_Registry::get('db');

        $select = $db->select();

        $select->from(array('p'=> 'pay_slips_temp'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id, p.*')));

        if(isset($params['email']) and $params['email']){
            $select->where('p.email = ?', $params['email']);
        }

        if(isset($params['month']) and $params['month']){
            $select->where('p.month = ?', $params['month']);
        }

        if(isset($params['year']) and $params['year']){
            $select->where('p.year = ?', $params['year']);
        }

        $select->group(array('p.email', 'p.month', 'p.year'));

        $select->order('p.created_at DESC');

        $result = $db->fetchAll($select);

        return $result;
    }

    function getDataView($params){
        $db = Zend_Registry::get('db');

        $select = $db->select();

        $select->from(array('p'=> 'pay_slips_temp'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id, p.*')));

        if(isset($params['email']) and $params['email']){
            $select->where('p.email = ?', $params['email']);
        }

        if(isset($params['month']) and $params['month']){
            $select->where('p.month = ?', $params['month']);
        }

        if(isset($params['year']) and $params['year']){
            $select->where('p.year = ?', $params['year']);
        }

        $select->order(array('p.created_at DESC', 'p.id DESC'));

        $result = $db->fetchAll($select);

        return $result;
    }
    
}















