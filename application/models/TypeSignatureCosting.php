<?php
class Application_Model_TypeSignatureCosting extends Zend_Db_Table_Abstract
{
	//protected $_schema = DATABASE_COST;
    protected $_name = 'type_signature_costing';

    public function getData()
    {

        $db     = Zend_Registry::get("db");
        $select = $db->select();
        $arrCols = array(

            'p.id', 
            'name'         => 'p.name'
        );
            
        $select->from(array('p'=> 'type_signature_costing'), $arrCols);
       // $select->joinLeft(array('c' => 'channel_costing'), 'c.id = p.chanel_id', array());
        $result = $db->fetchAll($select);
        return $result;
    }
}