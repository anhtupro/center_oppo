<?php

class Application_Model_ConstructObs extends Zend_Db_Table_Abstract
{
    protected $_name = 'construct_obs';
    protected $_schema = DATABASE_TRADE;

    public function fetchPagination($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['p' => DATABASE_TRADE.'.construct_obs'], [
                'id' => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
                'p.*',
                'store_name' => 's.name',
                'status_name' => 'a.name',
                'total_price' => "SUM(q.total_price) + SUM(q.total_price) * 0.1", // VAT = 10%,
                'review_cost' => "p.review_cost",
            ])
            ->joinLeft(['s' => DATABASE_CENTER . '.store'], 'p.store_id = s.id', [])
            ->joinLeft(['a' => DATABASE_TRADE . '.app_status'], 'a.type = 20 AND p.status = a.status', [])
            ->joinLeft(array('q' => DATABASE_TRADE . '.construct_obs_price'), 'p.id = q.construct_obs_id AND q.status = (SELECT MAX(status) FROM trade_marketing.construct_obs_price WHERE construct_obs_id = p.id AND status <> 1 AND status <> 3 AND status <> 5)', array());


        $select->limitPage($page, $limit);

        $select->order('p.id DESC');

        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;

    }

    public function get($id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['o' => DATABASE_TRADE . '.construct_obs'], [
                'o.*',
                'contractor_name' => 'c.name',
                'status_name' => 's.name'
            ])
            ->joinLeft(['c' => DATABASE_TRADE . '.contructors'], 'o.contractor_id = c.id', [])
            ->joinLeft(['s' => DATABASE_TRADE . '.app_status'], 's.type = 20 AND o.status = s.status', [])
            ->where('o.id = ?', $id);

        $result = $db->fetchRow($select);

        return $result;

    }

}