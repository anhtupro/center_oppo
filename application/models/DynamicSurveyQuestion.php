<?php
/**
 * Created by PhpStorm.
 * User: lphuctai
 * Date: 09/04/2019
 * Time: 19:24
 */

class Application_Model_DynamicSurveyQuestion extends Zend_Db_Table_Abstract
{
    protected $_name = 'dynamic_survey_question';

    /**
     * @param $surveyId
     * @return array
     */
    public function getQuestionBySurvey($surveyId)
    {
        return $this->fetchAll([
            $this->getAdapter()->quoteInto('fk_ds = ?', $surveyId),
            $this->getAdapter()->quoteInto('dsq_type not in (?)', ['title', 'title-bold']),
        ], 'dsq_id asc')->toArray();
    }
}