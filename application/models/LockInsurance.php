<?php

class Application_Model_LockInsurance extends Zend_Db_Table_Abstract {

    protected $_name = 'lock_insurance';

    public function getListLock() {
        $db = Zend_Registry::get('db');

        $sql = "SELECT lock_time FROM `lock_insurance` GROUP BY lock_time";

        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();

        $stmt->closeCursor();
        $stmt = $db = null;

        return $data;
    }

    public function save($data) {
        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
            $select = $db->select()
                    ->from(array('l' => 'lock_insurance'), 'l.*')
            ;

            if (isset($data['staff_id']) AND $data['staff_id']) {
                $select->where('l.staff_id = ?', $data['staff_id']);
            }
            if (isset($data['lock_type']) AND $data['lock_type']) {
                $select->where('l.lock_type = ?', $data['lock_type']);
            }
            if (isset($data['lock_time']) AND $data['lock_time']) {
                $select->where('l.lock_time = ?', $data['lock_time']);
            }
            $row = $db->fetchRow($select);

//            if ($row) {
//                $arr = array('code' => -2, 'message' => 'lock insurance is existed');
//                return $arr;
//            } else {
//               
//            }
            $this->insert($data);
            $arr = array('code' => 1, 'message' => 'Done');
            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            $arr = array('code' => -1, 'message' => $e->getMessage());
        }
        return $arr;
    }

}
