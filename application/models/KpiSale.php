<?php
    class Application_Model_KpiSale extends Zend_dB_Table_Abstract{
        protected $_schema = DATABASE_CENTER;
        protected $_name   = 'pgpb_sales';
        protected $_primary= 'pgpb_sales_id';
        
        public function insertData($arrData){
            $db = Zend_Registry::get('db');
            $db->insert($this->_schema.'.'.$this->_name,$arrData);
        }
        public function selectSalesProduct($dataUser = [],$curPage,$limitItems,$params){
            $db = Zend_Registry::get('db');
            $select = $db->select()
                        ->from($this->_schema.'.'.$this->_name.' as p',
                        ['pgpb_sales_amount','pgpb_sales_id','pgpb_sales_date','(pgpb_sales_amount*bonus_amount) AS pgpb_sales_total'])
                        ->join(WAREHOUSE_DB.'.good as g','g.id = p.product_id',['desc'])
                        ->join($this->_schema.'.bonus as b','p.product_id = b.product_id',['bonus_amount'])
                        ->join($this->_schema.'.staff as s','p.staff_id = s.id',
                        ['s.firstname','s.lastname','CONCAT(s.firstname," ",s.lastname) as fullname','s.email'])
                        ->order('pgpb_sales_id ASC');
            if($dataUser['title'] != 252){
                $select = $select->where('s.id = ?',$dataUser['id'],STRING);
            }
            if(!empty($params['submit']) || !empty($params['export']) ){
                if(!empty($params['date-start'])){
                    $select = $select->where("pgpb_sales_date >= ?",$params['date-start']);
                }
                if(!empty($params['date-end'])){
                    $select = $select->where("pgpb_sales_date <= ?",$params['date-end']);
                }
                if(!empty($params['product-name'])){
                    $select = $select->where('g.desc LIKE ?', '%'.trim($params['product-name']).'%');
                }
                if(!empty($params['email'])){
                    $select = $select->where('s.email LIKE ?', '%'.trim($params['email']).'%');
                }
                if(!empty($params['staff-name'])){
                    $select = $select->where('CONCAT(s.firstname," ",s.lastname) LIKE ?', '%'.trim($params['staff-name']).'%');
                }  
            }
            $result['totalItems']   = count($db->fetchAll($select));
            $result['totalDetail'] = $db->fetchAll($select);
            $select = $select->limitPage($curPage,$limitItems);             
            $result['detailPerPage']       = $db->fetchAll($select);
            return $result;
        }
        public function selectTotalSalesProduct($dataUser = []){
            $db = Zend_Registry::get('db');
            $select = $db->select()
                         ->from($this->_schema.'.'.$this->_name,['COUNT('.$this->_primary.') as totalItems']);
            if($dataUser['title'] != 252){
               $select = $select->where( 'staff_id = ?',$dataUser['id'],STRING);
            }
            $result = $db->fetchOne($select);
            return $result;
        }
        public function selectStoreByStaff($idStaff){
            $db = Zend_Registry::get('db');
            $select = $db->select()
                         ->from($this->_schema.'.staff_store as ss',['staff_store_id'])
                         ->join($this->_schema.'.store as s','ss.store_id = s.id',['name','id'])
                         ->where('ss.staff_id = ?',$idStaff,INTEGER);
            $result = $db->fetchAll($select);
            return $result;
        }
        public function deletePgpbSalesById($id){
            $db = Zend_Registry::get('db');
            $where = $db->quoteInto('pgpb_sales_id = ?',$id,INTEGER);
            $db->delete($this->_name,$where);
        }
        public function exportFileExcel($data,$fileName,$sheetName){
            require_once 'PHPExcel.php';
            $PHPExcel = new PHPExcel();
            $PHPExcel->setActiveSheetIndex(0);
            $sheet    = $PHPExcel->getActiveSheet()->setTitle($sheetName);    
            $rowCount = 1;
        
            $sheet->setCellValue('A'.$rowCount,'STT');
            $sheet->setCellValue('B'.$rowCount,'HỌ & TÊN');
            $sheet->setCellValue('C'.$rowCount,'EMAIL');
            $sheet->setCellValue('D'.$rowCount,'NGÀY BÁN');
            $sheet->setCellValue('E'.$rowCount,'SẢN PHẨM');
            $sheet->setCellValue('F'.$rowCount,'TIỀN THƯỞNG');
            $sheet->setCellValue('G'.$rowCount,'SỐ LƯỢNG');
            $sheet->setCellValue('H'.$rowCount,'TỔNG TIỀN THƯỞNG');

            $sheet->getColumnDimension('A')->setAutoSize(true);    
            $sheet->getColumnDimension('B')->setAutoSize(true);    
            $sheet->getColumnDimension('C')->setAutoSize(true);    
            $sheet->getColumnDimension('D')->setAutoSize(true);    
            $sheet->getColumnDimension('E')->setAutoSize(true);    
            $sheet->getColumnDimension('F')->setAutoSize(true);    
            $sheet->getColumnDimension('G')->setAutoSize(true);    
            $sheet->getColumnDimension('H')->setAutoSize(true); 
            

            foreach($data as $key => $value){
                ++$rowCount;
                $sheet->setCellValue('A'.$rowCount,$rowCount-1);
                $sheet->setCellValue('B'.$rowCount,$value['fullname']);
                $sheet->setCellValue('C'.$rowCount,$value['email']);
                $sheet->setCellValue('D'.$rowCount,$value['pgpb_sales_date']);
                $sheet->setCellValue('E'.$rowCount,$value['desc']);
                $sheet->setCellValue('F'.$rowCount,number_format($value['bonus_amount']).' VNĐ');
                $sheet->setCellValue('G'.$rowCount,$value['pgpb_sales_amount']);
                $sheet->setCellValue('H'.$rowCount,number_format($value['pgpb_sales_total']).' VNĐ');
            }
                
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            $fileName  = $fileName.'.xlsx';
            header('Content-Disposition: attachment; filename="'. $fileName .'"');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            $objWriter->save('php://output'); 
            exit();
        }
        //get total revenue per area
        public function analyticsArea(){
           $indexOffset = ($curPage-1) * $limitItems;
           $db     = Zend_Registry::get('db');
           //View totalRevenue
           $select = $db->select()
                        ->from(DATABASE_CENTER.'.pgpb_sales AS p',['p.pgpb_sales_id'])
                        ->join(WAREHOUSE_DB.'.good as g','p.product_id = g.id',['SUM(g.price_5*p.pgpb_sales_amount) as  totalRevenue']);
           $data   = $db->fetchRow($select);
           $result['totalRevenueAllArea'] = $data['totalRevenue'] ;
            $result['totalItems']         = count($db->fetchAll($select));
            //Detail Items
            $stmt_detail                  = $db->query('CALL `select_info_kpi_area`()');
            $result['detail']             = $stmt_detail->fetchAll();      
            return $result;
        }
        public function getDetailArea($id,$curPage,$limitItem){
            $db = Zend_Registry::Get('db'); 
            $select = $db->select()
                        ->from(DATABASE_CENTER .'.store AS s',[])
                        ->join(DATABASE_CENTER.'.regional_market AS d','d.id = s.district',[])
                        ->join(DATABASE_CENTER.'.regional_market AS p','p.id = d.parent',[])
                        ->join(DATABASE_CENTER.'.pgpb_sales AS pg','pg.store_id = s.id',['SUM(pg.pgpb_sales_amount) AS totalAmountProduct'])
                        ->join(WAREHOUSE_DB.'.good AS g','g.id = pg.product_id',['desc','SUM(g.price_5*pg.pgpb_sales_amount) AS totalRevenueProduct'])
                        ->join(DATABASE_CENTER.'.area AS a','a.id = p.area_id',[])
                        ->where('a.id =?', $id,INTEGER)
                        ->group('g.id')
                        ->order('totalRevenueProduct DESC');
            $result['totalItems'] = count($db->fetchAll($select));
            $select = $select->limitPage($curPage,$limitItem);
            $result['detail'] = $db->fetchAll($select);
            return $result;
        }
        public function analyticsStaff($curPage,$limitItems){
            $db     = Zend_Registry::get('db');
            $select = $db->select()
                         ->from(DATABASE_CENTER.'.staff AS s',['CONCAT(firstname," ",lastname) AS fullname','email','id as idStaff'])
                         ->join(DATABASE_CENTER.'.pgpb_sales AS p','p.staff_id = s.id',['SUM(p.pgpb_sales_amount) AS totalAmount','SUM(p.pgpb_sales_total) AS totalBonus'])
                         ->join(WAREHOUSE_DB.'.good AS g','g.id = p.product_id',['SUM(g.price_5) AS totalRevenue'])
                         ->group('s.id')
                         ->order('totalBonus DESC');
            $result['totalItems'] = count($db->fetchAll($select));
            $select               = $select->limitPage($curPage,$limitItems);
            $result['detail']     = $db->fetchAll($select);   
            return $result;                      
        }
        public function analyticsProduct($id){
            $db     = Zend_Registry::get('db');
            $select = $db->select()
                         ->from(WAREHOUSE_DB . '.good AS g',['id AS idProduct','desc'])
                         ->join(DATABASE_CENTER . '.pgpb_sales AS p','g.id = p.product_id',
                         ['pgpb_sales_total','SUM(p.pgpb_sales_amount) AS amountlProduct','SUM(p.pgpb_sales_total) AS totalBonusProduct'])
                         ->join(DATABASE_CENTER . '.staff AS s','s.id = p.staff_id',['id AS idStaff','CONCAT(firstname," ",lastname) AS fullname'])
                         ->where('s.id = ?',$id,INTEGER)
                         ->group('g.id');
            $result['detail']     = $db->fetchAll($select); 
            return $result;
        }
        public function templateExcel($name, $infoColumn = [], $colorRGB = '99FFCC'){
            require_once 'PHPExcel.php';
            
            $PHPExcel = new PHPExcel();
            $PHPExcel->setActiveSheetIndex(0);
            $sheet = $PHPExcel->getActiveSheet()->setTitle('Template');
            $alpha = 'A';
            $filter = new Zend_Filter_StringToUpper();
            $filter->setEncoding('UTF-8');
            foreach($infoColumn as $key => $value){
                $value = $filter->filter($value);
                $sheet->setCellValue($alpha.'1',$value);
                $sheet->getColumnDimension($alpha)->setAutoSize(true); 
                $oldAlpha  = $alpha;
                $alpha++;              
            }
            $sheet->getStyle('A1:'.$oldAlpha.'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => $colorRGB)
                    )
                )
            );
            
            $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
            $fileName = $name .'.xlsx';
            header('Content-Disposition: attachment; filename="'. $fileName .'"');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            $objWriter->save('php://output'); 
            exit();
        }
        public function getIdStaffByCodeStaff($codeStaff){
            $db     = Zend_Registry::get('db');
            $select = $db->select()
                         ->from(DATABASE_CENTER.'.staff',['id AS idStaff'])
                         ->where('staff.code = ?',$codeStaff,STRING);
            $result = $db->fetchOne($select);
            return $result;
        }
        public function getIdGoodByGoodName($goodName){
            $db     = Zend_Registry::get('db');
            $select = $db->select()
                         ->from(WAREHOUSE_DB.'.good',['id AS idGood'])
                         ->where('good.name = ?',$goodName,STRING);
            $result = $db->fetchOne($select);
            return $result;  
        }
        function validateDate($date, $format = 'Y-m-d')
        {
            $d = DateTime::createFromFormat($format, $date);
            return $d && $d->format($format) == $date;
        }
        public function isExsitsStoreId($storeID){
            $db = Zend_Registry::get('db');
            $select = $db->select()
                         ->from(DATABASE_CENTER.'.store',['id'])
                         ->where('store.id = ?',$storeID,INTEGER);
            $result = $db->fetchOne($select);
            return !empty($result);
        }               
        public function importFileExcel($file){
            include 'PHPExcel/IOFactory.php';
            $message  = [];
            $flag     = true;
            $QPgpb_sales= new Application_Model_KpiSale();
            $objReader  = PHPExcel_IOFactory::createReaderForFile($file);
            $objReader->setLoadSheetsOnly('Template');
            $objExcel   = $objReader->load($file);
            $sheetData  = $objExcel->getActiveSheet()->toArray('null',true,true,true);
          
            $highestRow = $objExcel->setActiveSheetIndex()->getHighestRow();
            foreach($sheetData as $row => $value){
                if($row == 1){
                    continue;
                }else{
                    if(in_array('null',$value)){
                        $message[] = 'Bạn chưa điền thông tin dòng : '.$row;
                        $flag      = false;
                    }else{
                        $idStaff   = $this->getIdStaffByCodeStaff($value['C']);
                        $idGood    = $this->getIdGoodByGoodName($value['D']);
                        $isStoreId = $this->isExsitsStoreId($value['A']);
                        $amount    = $value['E'];
                        //check format date file excel
                        if(!$this->validateDate($value['B'])){
                            $flag      = false;
                            $message[] = 'Ngày bán dòng thứ '.$row.' không đúng định dạng';
                        }
                        if(!$idStaff){
                            $flag      = false;
                            $message[] = 'Mã nhân viên dòng thứ '.$row.' không tồn tại';
                        }
                        if(!$idGood){
                            $flag      = false;
                            $message[] = 'Mã sản phẩm dòng thứ '.$row.' không tồn tại';
                        }
                        if(!$isStoreId){
                            $flag      = false;
                            $message[] = 'Store id dòng thứ '.$row.' không tồn tại';
                        }
                        if(!is_numeric($amount)){
                            $flag      = false;
                            $message[] = 'Số lượng dòng thứ '.$row.' không đúng định dạng';
                        }
                        if(!$flag){
                            continue;
                        }else{
                            $data['store_id']   = $value['A'];
                            $data['product_id'] = $idGood;
                            $data['staff_id']   = $idStaff;
                            $data['pgpb_sales_date']   = $value['B'];
                            $data['pgpb_sales_amount'] = $value['E'];
                            $result[] = $data;     
                        }
                       
                    }
                }
                
            }
            if(!$flag){
                return [
                    'flag'    => $flag,
                    'message' => $message
                ];
            }else{  
                foreach($result as $key => $value){
                    $QPgpb_sales->insertData($value);
                }
                return [
                    'flag'    => $flag,
                    'message' => 'Import dữ liệu thành công ! '
                ];
            }
           
        }

    }

?>