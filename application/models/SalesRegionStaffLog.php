<?php
/**
* @author buu.pham
* @create 2015-09-30T16:14:14+07:00
* @filename SalesRegionStaffLog.php
*/
class Application_Model_SalesRegionStaffLog extends Zend_Db_Table_Abstract
{
    
    protected $_name = 'sales_region_staff_log';
   public function findPrimaykey($id){
        return $this->fetchRow($this->select()->where('sales_region_id = ?',$id));
    }
    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');
        $cols = array(
                'staff_id'      => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.id'), 
                'a.*',
                'staff_name'    => 'CONCAT(b.firstname," ",b.lastname)', 
                'b.code', 'b.email',
                'parent_shared' => 'd.region_share',
                'parent_name'   => 'd.name',
                'region_name'   => 'c.name',
                'region_shared' => 'c.shared',
                'region_id'     => 'c.id',
                'parent_id'     => 'd.id'
            );

        $select = $db->select()
            ->from(array('c'=>'sales_region'),$cols)
            ->joinLeft(array('a'=> new Zend_Db_Expr('(
                    SELECT a.* FROM sales_region_staff_log a
                    INNER JOIN (
                        SELECT MAX(id) id FROM sales_region_staff_log
                        GROUP BY sales_region_id
                    ) b ON b.id = a.id
                )')),'c.id = a.sales_region_id',array())
            ->joinLeft(array('b'=>'staff'),'b.id = a.staff_id',array())
            ->joinLeft(array('d'=>'area'),'d.id = c.parent',array());
        
        //$select->where('b.status = ?', My_Staff_Status::On);

        if (isset($params['leader_name']) and $params['leader_name']){
            $select->where('CONCAT(TRIM(BOTH FROM b.firstname), \' \', TRIM(BOTH FROM b.lastname)) LIKE ?', '%'.My_String::trim($params['leader_name']).'%');
        }

        if (isset($params['region_name']) and $params['region_name'])
            $select->where('TRIM(BOTH FROM d.name) LIKE ?', '%'.My_String::trim($params['region_name']).'%');

        if (isset($params['parent']) and $params['parent']) {
            if (is_array($params['parent']) && count($params['parent']))
                $select->where('c.parent IN (?)', $params['parent']);
            elseif (is_numeric($params['parent']) && intval($params['parent']))
                $select->where('c.parent = ?', intval($params['parent']));
            else
                $select->where('1 = ?', 0);
        }

        if (isset($params['region_id']) and $params['region_id']) {
            if (is_array($params['region_id']) && count($params['region_id']))
                $select->where('c.id IN (?)', $params['region_id']);
            elseif (is_numeric($params['region_id']) && intval($params['region_id']))
                $select->where('c.id = ?', intval($params['region_id']));
            else
                $select->where('1 = ?', 0);
        }

        if (isset($params['area_id']) and $params['area_id']) {
            if (is_array($params['area_id']) && count($params['area_id']))
                $select->where('d.id IN (?)', $params['area_id']);

            elseif (is_numeric($params['area_id']) && intval($params['area_id']))
                $select->where('d.id = ?', intval($params['area_id']));

            else
                $select->where('1 = ?', 0);
        }

        if (isset($params['email']) and $params['email']) {
            if (is_array($params['email']) && count($params['email']))
                $select->where('TRIM(BOTH FROM b.email) IN (?)', $params['email']);

            else
                $select->where('TRIM(BOTH FROM b.email) LIKE ?', My_String::trim(str_replace(EMAIL_SUFFIX, '', $params['email'])).EMAIL_SUFFIX);
        }

        if (isset($params['code']) and $params['code']) {
            if (is_array($params['code']) && count($params['code']))
                $select->where('TRIM(BOTH FROM b.code) IN (?)', $params['code']);

            else
                $select->where('TRIM(BOTH FROM b.code) LIKE ?', My_String::trim($params['code']));
        }

        $select->order(array('d.name', 'c.name'), 'COLLATE utf8_unicode_ci ASC');

        if (isset($params['export']) && $params['export'])
            return $select->__toString();
        //PC::db($select->__toString());
        if ($limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);

        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }


    function save($params){
        $QSalesRegion    = new Application_Model_SalesRegion();
        $Qstaff          = new Application_Model_Staff();
        $sales_region_id = $params['sales_region_id'];
        $current_staff   = $params['current_staff'];
        $remove          = $params['remove'];
        $new_staff       = $params['new_staff'];
        $to_date         = $params['to_date'];
        $from_date       = $params['from_date'];
        $db = Zend_Registry::get('db');

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        if(!$sales_region_id){
            return array('code'=>'1','message'=>'sales region is not exit');
        }

        $sales_region = $QSalesRegion->find($sales_region_id)->current();
        if(!$sales_region){
            return array('code'=>'1','message'=>'sales region is not exit');
        }

        if($remove == 1){
            if(!$to_date){
                return array('code'=>'1','message'=>'Please input time release'); 
            }

            $to_date        = My_Date::normal_to_mysql($to_date);
            $data_update    = array('to_date' => $to_date);
            $where_update[] = $this->getAdapter()->quoteInto('staff_id = ?',$current_staff);
            $where_update[] = $this->getAdapter()->quoteInto('to_date IS NULL',1);
            $this->update($data_update,$where_update);
        }

        if($new_staff){
            $staff = $Qstaff->find($new_staff)->current();
            if(!$staff){
                return array('code'=>'1','message'=>'new staff not exist');
            }

            if($from_date){
                $from_date = My_Date::normal_to_mysql($from_date);

                if(!My_Date::isMysqlDateFormat($from_date)){
                    return array('code'=>'1','message'=>'Joined at is incorrect format');
                }

                $select = $db->select()
                                ->from(array('a'=>'sales_region_staff_log'),array('a.*'))
                                ->where('a.sales_region_id = ?',$sales_region_id)
                                ->where('a.to_date IS NULL');
                $result = $db->fetchRow($select);

                if($result){
                    return array('code'=>'1','message'=>'Please remove staff before assige');
                }

                $data = array(
                    'staff_id'        => intval($new_staff),
                    'from_date'       => $from_date,
                    'sales_region_id' => $sales_region_id,
                    'created_at'      => date('Y-m-d H:i:s'),
                    'created_by'      => $userStorage->id,
                    'type'            => ($staff['title'] == LEADER_TITLE) ? 0 : 1
                );
                $this->insert($data);
            }
        }

        return array('code'=>0,'message'=>'Done');


    }    
}