<?php
class Application_Model_AppContract extends Zend_Db_Table_Abstract
{
    protected $_name = 'app_contract';
    protected $_schema = DATABASE_TRADE;

    public function fetchPagination($page, $limit, &$total, $params){
        
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'id' => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            'title' => "p.title",
            'created_at' => "p.created_at",
            'status'    => "p.status",
            'type'		=> "p.type"
        );

        $select->from(array('p' => DATABASE_TRADE.'.app_contract'), $arrCols);

         if(!empty($params['name_search'])){
             $select->where('p.title LIKE ?', '%'.$params['name_search'].'%');
         }

        if(!empty($params['from_date'])){
            $from_date = str_replace('/', '-', $params['from_date']);
            $from_date = date('Y-m-d', strtotime($from_date));

            $select->where('DATE(p.created_at) >= ?', $from_date);
        }

        if(!empty($params['to_date'])){
            $to_date = str_replace('/', '-', $params['to_date']);
            $to_date = date('Y-m-d', strtotime($to_date));
            $select->where('DATE(p.created_at) <= ?', $to_date);
        }

        
        //END SEARCH
        $select->limitPage($page, $limit);
        $select->order('p.id DESC');
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        
        return $result;
    }


    public function getDataContract($params){
    	$db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.title",
            "p.created_at",
            "p.type",
            "p.contructors_id",
            "a.url",
            "a.file_name"
           
        );

        $select->from(array('p' => DATABASE_TRADE.'.app_contract'), $arrCols);
        $select->joinLeft(array('a' => DATABASE_TRADE.'.app_contract_file'), 'p.id = a.contract_id', array());
        // $select->joinLeft(array('staff' => 'staff'), 'staff.id = p.staff_id', array());
        $select->where('p.id = ?', $params['id']);

        $result = $db->fetchRow($select);
        return $result;
    }
}