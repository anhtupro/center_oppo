<?php
class Application_Model_RankByMonth extends Zend_Db_Table_Abstract
{
    protected $_name = 'rank_by_month';

    function getAll($params){
    	$db     = Zend_Registry::get("db");

    	$area_q1 = 'AND p.area_id IS NOT NULL';
    	$area_q2 = 'AND p.area_id IS NULL';

    	$area_list = $params['area_list'];
    	if(count($area_list) > 0){
	    	$area = "'0'";
	    	foreach ($area_list as $key => $value) {
	    		$area .= ",'$value'";
	    	}

	    	if(count($area_list) > 0){
	    		$area_q1 = "AND p.area_id IN (".$area.") AND p.province_id IS NULL";
	    		$area_q2 = "AND p.area_id IN (".$area.") AND p.province_id IS NULL";
	    	}

	    	if(isset($params['province']) and $params['province']){
	    		$area_q1 = "AND p.province_id IN (".$params['province'].")";
	    		$area_q2 = "AND p.province_id IN (".$params['province'].")";
	    	}
    	}
		
        $date = date('Y-m-1');
        $date_last_from = date('Y-m-1', strtotime(date('Y-m')." -1 month"));
        $date_last_to   = date('Y-m-t', strtotime(date('Y-m')." -1 month"));
        $m = intval(date('m', strtotime($date_last_from)));
        $y = intval(date('Y', strtotime($date_last_from)));

        
        $select = $db->select();        
        $nestedSelect = "SELECT p.rank, COUNT(DISTINCT p.store_id) sum, COUNT(DISTINCT staff.id) count_staff, MONTH(p.timing_date) `month`, YEAR(p.timing_date) `year`
						FROM
						(
							SELECT DISTINCT p.store_id, SUM(p.qty) sum, p.timing_date, p.area_id, 
							(
							CASE 
							WHEN SUM(p.qty) >= 50 THEN '1'
							WHEN SUM(p.qty) >= 40 AND SUM(p.qty) <= 49 THEN '2'
							WHEN SUM(p.qty) >= 30 AND SUM(p.qty) <= 39 THEN '3'
							WHEN SUM(p.qty) >= 20 AND SUM(p.qty) <= 29 THEN '4'
							WHEN SUM(p.qty) >= 10 AND SUM(p.qty) <= 19 THEN '5'
							WHEN SUM(p.qty) < 10 THEN '6'
							END
							) `rank`
							FROM kpi_by_model p 
							LEFT JOIN store s ON s.id = p.store_id
							LEFT JOIN warehouse.distributor d ON d.id = s.d_id
							WHERE p.timing_date >= '".$date."' AND d.is_ka <> 1 ".$area_q1."
							GROUP BY p.store_id
						) p
						LEFT JOIN store_staff_log l ON l.store_id = p.store_id AND (l.released_at IS NULL OR l.released_at > UNIX_TIMESTAMP())
						LEFT JOIN staff ON staff.id = l.staff_id AND staff.title IN (182,293) 
						GROUP BY p.rank

						UNION
						SELECT p.rank, SUM(p.sum_store), SUM(p.sum_pg), p.`month`, p.`year`
						FROM rank_by_month p
						WHERE p.id IS NOT NULL ".$area_q2."
						GROUP BY p.rank, p.month, p.`year`";

        
        $select->from(array('p'=> new Zend_Db_Expr('(' . $nestedSelect . ')')), array('p.*'));
        $result  = $db->fetchAll($select);
  		
        return $result;
    }

    function getAllArea($params){
    	$db     = Zend_Registry::get("db");

    	$area_q1 = 'AND p.area_id IS NOT NULL';
    	$area_q2 = 'AND p.area_id IS NULL';

    	$area_list = $params['area_list'];
    	if(count($area_list) > 0){
	    	$area = "'0'";
	    	foreach ($area_list as $key => $value) {
	    		$area .= ",'$value'";
	    	}

	    	if(count($area_list) > 0){
	    		$area_q1 = "AND p.area_id IN (".$area.")";
	    		$area_q2 = "AND p.area_id IN (".$area.")";
	    	}
    	}
		
        $date = date('Y-m-01');
        $date_last_from = date('Y-m-1', strtotime(date('Y-m')." -1 month"));
        $date_last_to   = date('Y-m-t', strtotime(date('Y-m')." -1 month"));
        $m = intval(date('m', strtotime($date_last_from)));
        $y = intval(date('Y', strtotime($date_last_from)));

        
        $select = $db->select();        
        $nestedSelect = "SELECT p.area_id, p.rank, COUNT(DISTINCT p.store_id) sum, COUNT(DISTINCT staff.id) count_staff, MONTH(p.timing_date) `month`, YEAR(p.timing_date) `year`
						FROM
						(
							SELECT DISTINCT p.store_id, count(DISTINCT p.imei_sn) sum, p.timing_date, p.area_id, 
							(
							CASE WHEN COUNT(p.imei_sn) >= 50 THEN '1'
							WHEN COUNT(p.imei_sn) >= 40 AND COUNT(p.imei_sn) <= 49 THEN '2'
							WHEN COUNT(p.imei_sn) >= 30 AND COUNT(p.imei_sn) <= 39 THEN '3'
							WHEN COUNT(p.imei_sn) >= 20 AND COUNT(p.imei_sn) <= 29 THEN '4'
							WHEN COUNT(p.imei_sn) >= 10 AND COUNT(p.imei_sn) <= 19 THEN '5'
							WHEN COUNT(p.imei_sn) < 10 THEN '6'
							END
							) rank
							FROM imei_kpi p 
							LEFT JOIN store s ON s.id = p.store_id
							LEFT JOIN warehouse.distributor d ON d.id = s.d_id
							WHERE p.timing_date >= '".$date."' AND d.is_ka <> 1 ".$area_q1."
							GROUP BY p.store_id
						) p
						LEFT JOIN store_staff_log l ON l.store_id = p.store_id AND (l.released_at IS NULL OR l.released_at > UNIX_TIMESTAMP())
						LEFT JOIN staff ON staff.id = l.staff_id AND staff.title IN (182,293) 
						GROUP BY p.rank, p.area_id

						UNION
						SELECT p.area_id, p.rank, SUM(p.sum_store), SUM(p.sum_pg), p.`month`, p.`year`
						FROM rank_by_month p
						WHERE p.id IS NOT NULL 
						GROUP BY p.rank, p.month, p.area_id";
        
        $select->from(array('p'=> new Zend_Db_Expr('(' . $nestedSelect . ')')), array('p.*'));
        $result  = $db->fetchAll($select);
  		
        return $result;
    }
}