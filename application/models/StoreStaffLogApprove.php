<?php
class Application_Model_StoreStaffLogApprove extends Zend_Db_Table_Abstract
{
    protected $_name = 'store_staff_log_approve';
    
    public function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            "p.staff_id",
            "p.store_id",
            "p.is_leader",
            "joined_at" => "FROM_UNIXTIME(p.joined_at)",
            "released_at" => "FROM_UNIXTIME(p.released_at)",
            "p.parent",
            "p.updated_at",
            'fullname' => "CONCAT(s.firstname, ' ', s.lastname)",
            "s.code",
            "s.status",
            'team_name' => "t.name",
            'title_name' => "t2.name",
            'store_name' => "store.name",
            'province_name' => "r.name",
            'area_name' => "a.name",
            "approve.approve_by",
            "approve.approve_at",
            "approve_type" => "approve.type",
        );

        $select->from(array('p' => 'store_staff_log'), $arrCols);
        $select->joinLeft(array('s' => 'staff'), 's.id = p.staff_id', array());
        $select->joinLeft(array('t' => 'team'), 't.id = s.team', array());
        $select->joinLeft(array('t2' => 'team'), 't2.id = s.title', array());
        $select->joinLeft(array('store' => 'store'), 'store.id = p.store_id', array());
        $select->joinLeft(array('d' => WAREHOUSE_DB.'.distributor'), 'd.id = store.d_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = store.regional_market', array());
        $select->joinLeft(array('a' => 'area'), 'a.id = r.area_id', array());
        $select->joinLeft(array('approve' => 'store_staff_log_approve'), 'approve.store_staff_log_id = p.id', array());
        $select->where("(p.released_at IS NULL OR FROM_UNIXTIME(p.released_at) >= '2019-01-01') AND p.is_leader = 0");

        if(!empty($params['area_list'])){
            $select->where('a.id IN (?)',$params['area_list']);
        }
        
        if(!empty($params['name'])){
            $select->where('CONCAT(s.firstname, " ",s.lastname) LIKE ?', '%'.$params['name'].'%');
        }
        
        if(!empty($params['code'])){
            $select->where('s.code = ?',$params['code']);
        }
        
        if(!empty($params['email'])){
            $select->where('s.email LIKE ?', '%'.$params['email'].'%');
        }

        if(!empty($params['has_image']) AND $params['has_image'] != -1){
            if($params['has_image'] == 1 ){
            $select->where('s.image_body IS NULL');
            }elseif($params['has_image'] == 2){
            $select->where('s.image_body IS NOT NULL');
            }
        }
        if(!empty($params['is_approved']) AND $params['is_approved'] != -1){
            if($params['is_approved']==1){ //NO
            $select->where('approve.type IS NULL');
            }elseif($params['is_approved']==2){ //YES
            $select->where('approve.type IS NOT NULL');
            }
        }

        if(!empty($params['area_id'])){
            $select->where('a.id IN (?)',$params['area_id']);
        }
        // echo $select->__toString();
        $select->where('IF(d.parent = 0, d.id, d.parent) IN (?)', [2363, 2316]);
        
        $select->order('id DESC');
        
        if ($limit)
            $select->limitPage($page, $limit);
        
        $result = $db->fetchAll($select);
        
        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
    
    public function getStoreStaffInfo($id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.staff_id",
            "p.store_id",
            "p.is_leader",
            "joined_at" => "FROM_UNIXTIME(p.joined_at)",
            "released_at" => "FROM_UNIXTIME(p.released_at)",
            "p.parent",
            "p.updated_at",
            'fullname' => "CONCAT(s.firstname, ' ', s.lastname)",
            "s.code",
            "s.status",
            "s.height",
            "s.weight",
            "s.image_body",
            'team_name' => "t.name",
            'title_name' => "t2.name",
            'store_name' => "store.name",
            'province_name' => "r.name",
            'area_name' => "a.name",
            "approve.approve_by",
            "approve.approve_at",
            "approve.note",
            'approve_name' => "CONCAT(staff.firstname, ' ', staff.lastname)",
            "type_name" => "IF(approve.type = 1, 'Approve', 'Reject')",
        );

        $select->from(array('p' => 'store_staff_log'), $arrCols);
        $select->joinLeft(array('s' => 'staff'), 's.id = p.staff_id', array());
        $select->joinLeft(array('t' => 'team'), 't.id = s.team', array());
        $select->joinLeft(array('t2' => 'team'), 't2.id = s.title', array());
        $select->joinLeft(array('store' => 'store'), 'store.id = p.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = store.regional_market', array());
        $select->joinLeft(array('a' => 'area'), 'a.id = r.area_id', array());
        $select->joinLeft(array('approve' => 'store_staff_log_approve'), 'approve.store_staff_log_id = p.id', array());
        $select->joinLeft(array('staff' => 'staff'), 'staff.id = approve.approve_by', array());
        $select->where("p.id = ?", $id);

        $result = $db->fetchRow($select);
        

        return $result;
    }
    
    
    public function getStoreImage($store_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "f.url",
            "f.checkshop_id",
        );

        $select->from(array('p' => DATABASE_TRADE.'.app_checkshop'), $arrCols);
        $select->joinLeft(array('f' => DATABASE_TRADE.'.app_checkshop_file'), 'f.checkshop_id = p.id', array());
        
        $select->where("p.store_id = ?", $store_id);
        $select->where("f.type IN (0,1,2)");
        $select->where("p.id = (SELECT MAX(id) FROM trade_marketing.app_checkshop WHERE store_id = ?)", $store_id);

        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function checkIsApprove($id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
        );

        $select->from(array('p' => 'staff'), $arrCols);
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());
        
        $select->where("p.id = ?", $id);
        $select->where("r.area_id IN (?)", [1,24,25,26,34,10,32,33,51,7,13,4]);

        $result = $db->fetchRow($select);

        return $result;
    }
}