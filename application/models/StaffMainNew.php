<?php
class Application_Model_StaffMainNew extends Zend_Db_Table_Abstract
{
    protected $_name = 'staff_main_new';


    public function getStaffMain($cmnd)
    {
            $db     = Zend_Registry::get("db");
    $select = $db->select();

    $arrCols = array(
        'ID_number'       => 's.ID_number'
    );
    $select->from(array('s'=> 'staff_main_new'), $arrCols);
    $select->where('s.ID_number = ?', $cmnd);
    $result  = $db->fetchAll($select);
    return $result;
    }

    public function getTypePriviledge($params){

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        if($userStorage->id == 2123){
            $type_priviledge = 3;
            return $type_priviledge;
        }


        if($userStorage->group_id = TRAINEE_STAFF_ID){
            $type_priviledge = 1;
            return $type_priviledge;
        }

        $QStaffMainNew = new Application_Model_StaffMainNew();

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $where = $QStaffMainNew->getAdapter()->quoteInto('ID_number = ?', $params['ID_number']);
        $staff_main_new = $QStaffMainNew->fetchRow($where);


        $type_priviledge = 0;
        if($staff_main_new){
            if( in_array($userStorage->title, [PG_BRANDSHOP, SENIOR_PROMOTER_BRANDSHOP]) || $userStorage->id == 910 || $userStorage->id == 2123){
                $type_priviledge = 3;
            }
            elseif($userStorage->title == PGPB_PARTTIME_TITLE){
                $type_priviledge = 5;
            }
            else{
                $type_priviledge = 1;
            }
        }
        else{
            $type_priviledge = 2;
        }

        return $type_priviledge;

    }
    
    public function getListChannelByPgsId($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.staff_id", 
            "p.store_id", 
            "p.is_leader", 
            "c.channel_id"
        );

        $select->from(array('p' => 'store_staff_log'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('c' => 'v_store_channel_id'), 'c.store_id = s.id', array());
        $select->where('p.released_at IS NULL', NULL);
        $select->where('p.staff_id = ?', $params['staff_id']);

        $result = $db->fetchAll($select);
        
        $data = [];
        foreach($result as $channel){
            $data[] = $channel['channel_id'];
        }
                if ($_GET['zzzz']==3) {
            echo "<pre>";
            echo $select->__toString();
        }

        return $data;
    }

    public function getTotalStaffByListChannel($params=null){

        $db = Zend_Registry::get("db");
        $select = $db->select();
        $nestedSelect = $db->select();
        $arrCols = array(
            "p.staff_id", 
            "c.channel_id"
        );

        $nestedSelect->from(array('staff'),array());
        $nestedSelect->joinLeft(array('p' => 'store_staff_log'),'staff.id=p.staff_id', $arrCols);
        $nestedSelect->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $nestedSelect->joinLeft(array('c' => 'v_store_channel_id'), 'c.store_id = s.id', array());
        $nestedSelect->where('staff.status =?', 1);
        $nestedSelect->where('p.released_at IS NULL OR p.id is null', NULL);
        if(!empty($params['title'])){
            $nestedSelect->where('staff.title IN(?)', $params['title']);
        }
        if(!empty($params['channel'])){
            $nestedSelect->where('c.channel_id IN(?)', $params['channel']);
        }
        if(!empty($params['officialStaff'])){
            $nestedSelect->where('staff.contract_term NOT IN(?)', $params['officialStaff']);
        }

        $select->joinLeft(array('temp' => new Zend_Db_Expr('('.$nestedSelect.')')),'',array('total_staff' => new Zend_Db_Expr('COUNT(DISTINCT temp.staff_id)')) );
        // $select->group('temp.staff_id');
        $result = $db->fetchRow($select);

            // echo $select->__toString();
            // # code...
            // exit;

        return $result['total_staff'];
    }
    public function getStaffWithChannel($params=null){

        $db = Zend_Registry::get("db");
        // $select = $db->select();
        $nestedSelect = $db->select();
        $arrCols = array(
            "id"=>"p.staff_id", 
            "staff.ID_number", 
            "c.channel_id"
        );

        $nestedSelect->from(array('staff'),$arrCols);
        $nestedSelect->joinLeft(array('rmk' => 'regional_market'),'rmk.id=staff.regional_market', ['area'=>'rmk.area_id']);
        $nestedSelect->joinLeft(array('p' => 'store_staff_log'),'staff.id=p.staff_id', []);
        $nestedSelect->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $nestedSelect->joinLeft(array('c' => 'v_store_channel_id'), 'c.store_id = s.id', array());
        $nestedSelect->where('staff.status =?', 1);
        $nestedSelect->where('p.released_at IS NULL OR p.id is null', NULL);
        if(!empty($params['title'])){
            $nestedSelect->where('staff.title IN(?)', $params['title']);
        }
        if(!empty($params['channel'])){
            $nestedSelect->where('c.channel_id IN(?)', $params['channel']);
        }
        if(!empty($params['staffTermNotIn'])){
            $nestedSelect->where('staff.contract_term NOT IN(?)', $params['staffTermNotIn']);
        }
        $nestedSelect->group('staff.id');
        // $select->joinLeft(array('temp' => new Zend_Db_Expr('('.$nestedSelect.')')),'',array('total_staff' => new Zend_Db_Expr('COUNT(DISTINCT temp.staff_id)')) );
        // $select->group('temp.staff_id');
        $result = $db->fetchAll($nestedSelect);

            // echo $nestedSelect->__toString();
            // # code...
            // exit;

        return $result;
    }
    
}