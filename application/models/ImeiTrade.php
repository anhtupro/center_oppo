<?php
class Application_Model_ImeiTrade extends Zend_Db_Table_Abstract
{
	protected $_name = 'imei';
	protected $_schema = DATABASE_TRADE;

    public function GetId($params){
         $db = Zend_Registry::get('db');
        $select = $db->select()
        ->from(array('I'=> DATABASE_TRADE.'.imei'),array('category_id'=>'I.good_id','store_id'=>'I.object_id'))
        ->joinLeft(array('C'=>DATABASE_TRADE.'.category'),'C.id = I.good_id')
         ->where('C.group_bi is not null and C.group_bi >0');
        if (isset($params['id']) and $params['id'])
            $select->where('I.imei_sn = ?', $params['id']);

        $result = $db->fetchRow($select);
        return $result;
    }
}                                                      
