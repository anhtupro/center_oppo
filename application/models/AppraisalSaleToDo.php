<?php

class Application_Model_AppraisalSaleToDo extends Zend_Db_Table_Abstract
{
    const TITLE_RSM_ID = 308;
    const TITLE_ASM_ID = 179;
    const TITLE_SALE_LEADER_ID = 190;
    const TITLE_SALE_ID = 183;
    const TITLE_PG_ID = 182;

    const TITLE_RSM = 'RSM';
    const TITLE_ASM = 'ASM';
    const TITLE_SALE_LEADER = 'LEADER';
    const TITLE_SALE = 'SALE';
    const TITLE_PG = 'PG';

    protected $_name = 'tmp_appraisal_to_do';
    protected $_primary = 'from_staff';

    /**
     * @param $planId
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function initPlan($planId)
    {
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $sql = "call sp_create_cache_appraisal_sale(:plan_id);" ;
        $stmt = $db->prepare($sql);
        $stmt->bindParam('plan_id', $planId, PDO::PARAM_INT);
        $stmt->execute();
        $stmt->closeCursor();
    }

    /**
     * @param $planId
     * @param $staffId
     * @return array
     */
    public function getToDoSurvey($planId, $staffId)
    {
        return $this->fetchAll([
            $this->getAdapter()->quoteInto('asp_id = ?', $planId),
            $this->getAdapter()->quoteInto('from_staff = ?', $staffId),
        ])->toArray();
    }

    /**
     * @param $titleId
     * @return string
     */
    public function convertTitleFromId($titleId)
    {
        switch ($titleId) {
            case self::TITLE_RSM_ID:
                return self::TITLE_RSM;
            case self::TITLE_ASM_ID:
                return self::TITLE_ASM;
            case self::TITLE_SALE_LEADER_ID:
                return self::TITLE_SALE_LEADER;
            case self::TITLE_SALE_ID:
                return self::TITLE_SALE;
        }
    }

    /**
     * @param $userId
     * @param $planId
     * @return int|null
     */
    public function getTitleById($userId, $planId)
    {
        $data = $this->fetchAll(
            $this->select()
            ->distinct()
            ->from($this->_name, ['from_title'])
            ->where('from_staff = ?', $userId)
            ->where('asp_id = ?', $planId)
        )->toArray();
        $titleId = null;
        foreach ($data as $datum) {
            $titleId = $datum['from_title'];
            break;
        }
        return $titleId;
    }

}