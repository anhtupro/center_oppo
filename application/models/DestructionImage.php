<?php

class Application_Model_DestructionImage extends Zend_Db_Table_Abstract
{
    protected $_name = 'destruction_image';
    protected $_schema = DATABASE_TRADE;

    public function getImage($destructionId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['i' => DATABASE_TRADE.'.destruction_image'], [
                'i.*'
            ])
            ->where('i.destruction_id = ?', $destructionId);

        $result = $db->fetchAll($select);

        return $result;
    }


}