<?php
class Application_Model_SupplierContructors extends Zend_Db_Table_Abstract
{
    protected $_name = 'supplier_contructors';
    
    function fetchPage($page, $limit, &$total, $params){
        
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            "contructors_name" => "p.name", 
            "contructors_address" => "p.address", 
            "supplier_id" => "s.id", 
            "supplier_title" => "s.title", 
            "supplier_name" => "s.name",
        );

        $select->from(array('p' => DATABASE_TRADE.'.contructors'), $arrCols);
        $select->joinLeft(array('c' => 'supplier_contructors'), 'c.contructors_id = p.id', array());
        $select->joinLeft(array('s' => 'supplier'), 's.id = c.supplier_id', array());
        
        if(!empty($params['contructors_name'])){
            $select->where('p.name LIKE ?', '%'.$params['contructors_name'].'%');
        }
        
        if(!empty($params['supplier_title'])){
            $select->where('s.title LIKE ?', '%'.$params['supplier_title'].'%');
        }
        
        if(!empty($params['supplier_name'])){
            $select->where('s.name LIKE ?', '%'.$params['supplier_name'].'%');
        }
        
        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

}