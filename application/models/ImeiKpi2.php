<?php

class Application_Model_ImeiKpi2 extends Zend_Db_Table_Abstract{

	public function GetAll($pars = array()){
		$pars = array_merge(
			array(
				'product_name' => null,
				'policy' => null,
				'from_date' => null,
				'to_date' => null,
				'title' => null,
				'limit' => null,
				'offset' => null
			),
			$pars
		);

		$db = Zend_Registry::get('db');
		$stmt = $db->prepare('CALL SP_report_imei_kpi_GetAll(:product_name, :policy, :from_date, :to_date, :title, :limit, :offset, @total)');
		$stmt->bindParam('product_name', $pars['product_name'], PDO::PARAM_STR);
		$stmt->bindParam('policy', $pars['policy'], PDO::PARAM_INT);
		$stmt->bindParam('from_date', $pars['from_date'], PDO::PARAM_STR);
		$stmt->bindParam('to_date', $pars['to_date'], PDO::PARAM_STR);
		$stmt->bindParam('title', $pars['title'], PDO::PARAM_INT);
		$stmt->bindParam('limit', $pars['limit'], PDO::PARAM_INT);
		$stmt->bindParam('offset', $pars['offset'], PDO::PARAM_INT);
		$stmt->execute();
		$res['data'] = $stmt->fetchAll();
		$stmt->closeCursor();
		if($res['data']){
			$t = $db->query('SELECT @total AS total')->fetch();
			$res['total'] = $t['total'];
		}
		$db = $stmt = null;
		return $res;
	}

	public function GetAllPolicy($limit = null, $offset = null, $pars = array()){
		$db = Zend_Registry::get('db');
		$stmt = $db->prepare('CALL SP_imei_kpi_ReportGetAll(:policy, :title, :name, :code, :area, :from_date, :to_date, :limit, :offset, @total)');

		$pars['area'] = empty($pars['area'])?null:$pars['area'];
		$pars['title'] = empty($pars['title'])?null:$pars['title'];
		$pars['policy'] = empty($pars['policy'])?null:$pars['policy'];

		$stmt->bindParam('policy', $pars['policy'], PDO::PARAM_INT);
		$stmt->bindParam('area', $pars['area'], PDO::PARAM_INT);
		$stmt->bindParam('title', $pars['title'], PDO::PARAM_INT);
		$stmt->bindParam('name', $pars['name'], PDO::PARAM_STR);
		$stmt->bindParam('code', $pars['code'], PDO::PARAM_STR);
		$stmt->bindParam('from_date', $pars['from_date'], PDO::PARAM_STR);
		$stmt->bindParam('to_date', $pars['to_date'], PDO::PARAM_STR);
		$stmt->bindParam('limit', $limit, PDO::PARAM_INT);
		$stmt->bindParam('offset', $offset, PDO::PARAM_INT);
		$stmt->execute();
		
		$data['data'] = $stmt->fetchAll();
		$stmt->closeCursor();
		$data['total'] = $db->query("SELECT FOUND_ROWS()")->fetchColumn();
		$db = $stmt = null;
		return $data;
	}

	public function GetAllByStaff($limit = 0, $offset = 0,$pars = array())
	{
		$db = Zend_Registry::get('db');
		// echo "<pre>";print_r($pars);die;
		// $stmt = $db->prepare('CALL SP_imei_kpi_ReportByTotalKpi(:name, :code, :area, :title, :from_date, :to_date, :limit, :offset, @total)');
		$stmt = $db->prepare('CALL SP_imei_kpi_ReportByTotalKpiNew(:from_date, :to_date,:limit, :offset,@total)');
		// $pars['area'] = empty($pars['area'])?null:$pars['area'];
		// $pars['title'] = empty($pars['title'])?null:$pars['title'];

		// $stmt->bindParam('name', $pars['name'], PDO::PARAM_STR);
		// $stmt->bindParam('code', $pars['code'], PDO::PARAM_STR);
		// $stmt->bindParam('area', $pars['area'], PDO::PARAM_INT);
		// $stmt->bindParam('title', $pars['title'], PDO::PARAM_INT);
		$stmt->bindParam('from_date', $pars['from_date'], PDO::PARAM_STR);
		$stmt->bindParam('to_date', $pars['to_date'], PDO::PARAM_STR);
	
		$stmt->bindParam('limit', $limit, PDO::PARAM_INT);
		$stmt->bindParam('offset', $offset, PDO::PARAM_INT);

		$stmt->execute();

		$data['data'] = $stmt->fetchAll();

		// echo "<pre>";print_r($data['data']);die;
		$stmt->closeCursor();
		$data['total'] = $db->query("SELECT FOUND_ROWS()")->fetchColumn();
		$db = $stmt = null;
		
		return $data;
	}

	public function GroupByPolicy($limit, $offset, $pars = array())
	{
		$db = Zend_Registry::get('db');
		// $stmt = $db->prepare('CALL SP_imei_kpi_GetOverview(:from_date, :to_date, :policy, :title, :limit, :offset, @total)');
		$stmt = $db->prepare('CALL SP_imei_kpi_GetOverview(:from_date, :to_date)');

		$pars['policy'] = empty($pars['policy'])?null:$pars['policy'];
		$pars['title'] = empty($pars['title'])?null:$pars['title'];

		$stmt->bindParam('from_date', $pars['from_date'], PDO::PARAM_STR);
		$stmt->bindParam('to_date', $pars['to_date'], PDO::PARAM_STR);
		// $stmt->bindParam('policy', $pars['policy'], PDO::PARAM_INT);
		// $stmt->bindParam('title', $pars['title'], PDO::PARAM_INT);
		// $stmt->bindParam('limit', $limit, PDO::PARAM_INT);
		// $stmt->bindParam('offset', $offset, PDO::PARAM_INT);
		
		$stmt->execute();
		$data = array();
		$data['data'] = $stmt->fetchAll();
		$stmt->closeCursor();
		// $data['total'] = $db->query("SELECT FOUND_ROWS()")->fetchColumn();
		$db = $stmt = null;
		
		return $data;
	}

	// public function ReportByPolicy($limit, $offset, $pars = array())
	// {
	// 	$db = Zend_Registry::get('db');
	// 	$array_sql = array();
	// 	$array_where = array();
	// 	if(isset($pars['name']) && !empty($pars['name']))
	// 	{
	// 		$array_where[] = "rp.staff_name like '%" . $pars['name'] . "%'";
	// 	}

	// 	if(isset($pars['area']) && !empty($pars['area']))
	// 	{
	// 		$array_where[] = "rp.area_id = " . $pars['area'];
	// 	}

	// 	if(isset($pars['code']) && !empty($pars['code']))
	// 	{
	// 		$array_where[] = "st.code = " . $pars['code'];
	// 	}

	// 	if(isset($pars['from_date']) && !empty($pars['from_date']) && isset($pars['to_date']) && !empty($pars['to_date']))
	// 	{
	// 		$array_where[] = "rp.timing_date >= '" . $pars['from_date'] ." 00:00:00' and rp.timing_date <= '" . $pars['to_date']." 23:59:59'" ;
	// 	}


	// 	$sql_where = empty($array_where)?'':' WHERE ' . implode(" and ", $array_where);
	// 	$sql_where .=  " GROUP BY staff_name, policy_title ";

	// 	$sql_pg = '(
	// 				SELECT
	// 					rp.staff_name AS `staff_name`,
	// 					rp.area_name AS `area_name`,
	// 					"PG" `title`,
	// 					rp.policy_id AS `policy_id`,
	// 					rp.policy_title AS `policy_title`,
	// 					SUM(rp.kpi) AS total_kpi,
 //                        st.code AS `code`
	// 				FROM
	// 					report_kpi_pg rp
 //                    JOIN staff st ON rp.staff_id = st.id '
	// 				. $sql_where .
	// 				')';

	// 	$sql_pb_sale = '(
	// 				SELECT
	// 					rp.staff_name AS `staff_name`,
	// 					rp.area_name AS `area_name`,
	// 					"PB Sale" `title`,
	// 					rp.policy_id AS `policy_id`,
	// 					rp.policy_title AS `policy_title`,
	// 					SUM(rp.kpi) AS total_kpi,
 //                        st.code AS `code`
	// 				FROM
	// 					report_kpi_pb_sale rp
 //                    JOIN staff st ON rp.staff_id = st.id '
	// 				. $sql_where .
	// 				')';
		
	// 	$sql_sale = '(
	// 				SELECT
	// 					rp.staff_name AS `staff_name`,
	// 					rp.area_name AS `area_name`,
	// 					"Sale" `title`,
	// 					rp.policy_id AS `policy_id`,
	// 					rp.policy_title AS `policy_title`,
	// 					SUM(rp.kpi) AS total_kpi,
 //                        st.code AS `code`
	// 				FROM
	// 					report_kpi_sale rp
 //                    JOIN staff st ON rp.staff_id = st.id '
	// 				. $sql_where .
	// 				')';
	// 	if(isset($pars['title']) && in_array($pars['title'], array('pg', 'pb_sale', 'sale')))
	// 	{
	// 		if(isset($pars['title']) && $pars['title'] == 'pg')
	// 		{
	// 			$main_sql = '(
	// 							SELECT
	// 								SQL_CALC_FOUND_ROWS rp.staff_name AS `staff_name`,
	// 								rp.area_name AS `area_name`,
	// 								"PG" `title`,
	// 								rp.policy_id AS `policy_id`,
	// 								rp.policy_title AS `policy_title`,
	// 								SUM(rp.kpi) AS total_kpi,
	// 								st.code AS `code`
	// 							FROM
	// 								report_kpi_pg rp
	// 							JOIN staff st ON rp.staff_id = st.id '
	// 							. $sql_where .
	// 							')';

	// 		}

	// 		if(isset($pars['title']) && $pars['title'] == 'pb_sale')
	// 		{
	// 			$main_sql = '(
	// 							SELECT
	// 								SQL_CALC_FOUND_ROWS rp.staff_name AS `staff_name`,
	// 								rp.area_name AS `area_name`,
	// 								"PB Sale" `title`,
	// 								rp.policy_id AS `policy_id`,
	// 								rp.policy_title AS `policy_title`,
	// 								SUM(rp.kpi) AS total_kpi,
	// 								st.code AS `code`
	// 							FROM
	// 								report_kpi_pb_sale rp
	// 							JOIN staff st ON rp.staff_id = st.id '
	// 							. $sql_where .
	// 							')';
	// 		}

	// 		if(isset($pars['title']) && $pars['title'] == 'sale')
	// 		{
	// 			$main_sql = '(
	// 						SELECT
	// 							SQL_CALC_FOUND_ROWS rp.staff_name AS `staff_name`,
	// 							rp.area_name AS `area_name`,
	// 							"Sale" `title`,
	// 							rp.policy_id AS `policy_id`,
	// 							rp.policy_title AS `policy_title`,
	// 							SUM(rp.kpi) AS total_kpi,
	// 							st.code AS `code`
	// 						FROM
	// 							report_kpi_sale rp
	// 						JOIN staff st ON rp.staff_id = st.id '
	// 						. $sql_where .
	// 						')';
	// 		}
	// 	}
	// 	else
	// 	{
	// 		$main_sql = $sql_pg . ' UNION ALL ' . $sql_pb_sale . ' UNION ALL ' . $sql_sale;
	// 	}
		
	// 	if(isset($limit) && isset($offset))
	// 	{
	// 		if(isset($pars['title']) && in_array($pars['title'], array('pg', 'pb_sale', 'sale')))
	// 		{
	// 			$main_sql = $main_sql . " LIMIT " . $limit . " OFFSET " . $offset;
	// 		}
	// 		else
	// 		{
	// 			$main_sql = "SELECT SQL_CALC_FOUND_ROWS k.* FROM (" . $main_sql .") AS k LIMIT " . $limit . " OFFSET " . $offset;
	// 		}
	// 	}
	// 	$stmt = $db->prepare($main_sql);
	// 	$stmt->execute();

	// 	$data['data'] = $stmt->fetchAll();
	// 	$data['total'] = $db->query("SELECT FOUND_ROWS() AS total")->fetch()['total'];
	// 	$stmt->closeCursor();

	// 	$stmt = $db = null;
	// 	return $data;
	// }

	public function GetByTitle($limit, $offset, $pars = array()){
		$db = Zend_Registry::get('db');
		$pars = array_merge(
			array(
				'from_date' => null,
				'to_date' => null,
				'name' => null,
				'code' => null,
				'title' => null,
				'area' => null,
				'limit' => null,
				'offset' => null
			),
			$pars
		);

		// echo "<pre>";print_r($pars);die;

		$pars['area'] = empty($pars['area'])?null:$pars['area'];
		$pars['title'] = empty($pars['title'])?null:$pars['title'];

		// $stmt = $db->prepare('CALL SP_imei_kpi_GetByTitle(:from_date, :to_date, :name, :code, :area, :title, :limit, :offset, @total)');
		
		$stmt = $db->prepare('CALL SP_report_by_policy(:from_date, :to_date, :limit, :offset, @total)');


		$stmt->bindParam('from_date', $pars['from_date'], PDO::PARAM_STR);
		$stmt->bindParam('to_date', $pars['to_date'], PDO::PARAM_STR);
		// $stmt->bindParam('name', $pars['name'], PDO::PARAM_STR);
		// $stmt->bindParam('code', $pars['code'], PDO::PARAM_STR);
		// $stmt->bindParam('area', $pars['area'], PDO::PARAM_INT);
		// $stmt->bindParam('title', $pars['title'], PDO::PARAM_STR);
		$stmt->bindParam('limit', $limit, PDO::PARAM_INT);
		$stmt->bindParam('offset', $offset, PDO::PARAM_INT);
		$stmt->execute();
		
		$data['data'] = $stmt->fetchAll();
		
		$stmt->closeCursor();
		$data['total'] = $db->query("SELECT FOUND_ROWS()")->fetchColumn();
		$db = $stmt = null;
		return $data;
	}

	public function DetailStaff($pars = array())
	{
		$pars = array_merge(
                  array(
                        'from_date'   => null,
                        'to_date' => null,
                        'code'   => null
                  ),
                  $pars
            );

		// var_dump($pars);die;
		$db = Zend_Registry::get('db');

		// $sql = '
		// Select kpi.*, s.code as `code`
		// From
		// (SELECT staff_id, staff_name, area_name, "PG" title, good_name, color_name, policy_title, kpi, COUNT(imei_sn) sell_out
		// FROM report_kpi_pg
		// WHERE timing_date BETWEEN :from_date AND :to_date
		// GROUP BY staff_id, good_id, color_id, policy_id, kpi
		// UNION ALL
		// SELECT staff_id, staff_name, area_name, "Sale" title, good_name, color_name, policy_title, kpi, COUNT(imei_sn) sell_out
		// FROM report_kpi_sale
		// WHERE timing_date BETWEEN :from_date AND :to_date
		// GROUP BY staff_id, good_id, color_id, policy_id, kpi
		// UNION ALL
		// SELECT staff_id, staff_name, area_name, "PB Sale" title, good_name, color_name, policy_title, kpi, COUNT(imei_sn) sell_out
		// FROM report_kpi_pb_sale
		// WHERE timing_date BETWEEN :from_date AND :to_date
		// GROUP BY staff_id, good_id, color_id, policy_id, kpi)
		// AS `kpi`
		// JOIN staff s ON s.id = kpi.staff_id
		// WHERE TRUE
		// ';
		
		$sql = '
			SELECT
				CONCAT(s.firstname," ",s.lastname) staff_name
				, kpi.kpi
				, kpi.sell_out
				, s.code as `code`
				, p.title policy_title
				, g.`desc` good_name
				, kpi.title
				, a.`name` area_name
				, "" color_name
				, t.`name` team
				, total
			FROM (

				SELECT pg_id staff_id, area_id, "PGPB" title, good_id, policy_id_pg policy, kpi_pg kpi, COUNT(imei_sn) sell_out, (kpi_pg * COUNT(imei_sn)) total
				FROM kpi_log 
				WHERE timing_date BETWEEN :from_date AND :to_date AND pg_id > 0 AND kpi_pg > 0
				GROUP BY staff_id, policy_id_pg, good_id, kpi

				UNION ALL

				SELECT sale_id staff_id, area_id, "Sales" title, good_id, policy_id_sale policy, kpi_sale kpi, COUNT(imei_sn) sell_out, (kpi_sale * COUNT(imei_sn)) total
				FROM kpi_log 
				WHERE timing_date BETWEEN :from_date AND :to_date AND sale_id > 0 AND kpi_sale > 0
				GROUP BY staff_id, policy_id_sale, good_id, kpi

				UNION ALL

				SELECT store_leader_id staff_id, area_id, "Store Leader" title, good_id, policy_id_store_leader policy, kpi_store_leader kpi, COUNT(imei_sn) sell_out, (kpi_store_leader * COUNT(imei_sn)) total
				FROM kpi_log 
				WHERE timing_date BETWEEN :from_date AND :to_date AND store_leader_id > 0 AND kpi_store_leader > 0
				GROUP BY staff_id, policy_id_store_leader, good_id, kpi

				UNION ALL

				SELECT sales_brs_id staff_id, area_id, "Sale Leader Brs" title, good_id, policy_id_sales_brs policy, kpi_sales_brs kpi, COUNT(imei_sn) sell_out, (kpi_sales_brs * COUNT(imei_sn)) total
				FROM kpi_log 
				WHERE timing_date BETWEEN :from_date AND :to_date AND sales_brs_id > 0 AND kpi_sales_brs > 0
				GROUP BY staff_id, policy_id_sales_brs, good_id, kpi
                                    
                                UNION ALL
                                
                                SELECT iks.staff_id , kl.area_id, "CONSULTANT Brs" title, kl.good_id , 16 policy , iks.kpi, COUNT(iks.imei_sn) sell_out , ( iks.kpi * COUNT(iks.imei_sn)) total      
                                FROM imei_kpi_staff iks       
                                JOIN kpi_log kl ON iks.imei_sn = kl.imei_sn
                                WHERE iks.staff_id > 0 and iks.kpi > 0 and iks.timing_date BETWEEN :from_date AND :to_date
                                GROUP BY iks.staff_id, iks.type, kl.good_id, iks.kpi
			)
			AS `kpi`
			JOIN staff s ON s.id = kpi.staff_id
			JOIN policy p ON p.id = kpi.policy
			JOIN team t ON t.id = s.team
			JOIN warehouse.good g ON g.id = kpi.good_id
			JOIN area a ON a.id = kpi.area_id
			GROUP BY kpi.staff_id, title, kpi.good_id, kpi.policy, kpi.kpi
		';

		if(!empty($pars['code']))
		{
			$sql .= " HAVING s.code = '" . $pars['code'] . "'";
		}


		$stmt = $db->prepare($sql);
		$pars['from_date'] .= " 00:00:00";
		$pars['to_date'] .= " 23:59:59";
		$stmt->bindParam('from_date', $pars['from_date'] , PDO::PARAM_STR);
		$stmt->bindParam('to_date', $pars['to_date'], PDO::PARAM_STR);

		$stmt->execute();
		$data = $stmt->fetchAll();

		$stmt->closeCursor();
		$db = $stmt = null;
		return $data;
	}

	public function storeByStaff($pars)
	{
		$db = Zend_Registry::get('db');

		$sql = "SELECT
				sto.name as `store`,
				COUNT(ik.imei_sn) AS `sell_out`
			FROM staff sta
			JOIN `store_staff_log` sstl ON sta.id = sstl.staff_id 
				AND (sstl.released_at > ". strtotime($pars['from_date']) ." OR sstl.released_at IS NULL )
				AND sstl.joined_at <= ". strtotime($pars['to_date']) ." 
			JOIN store sto ON sstl.store_id = sto.id 
			LEFT JOIN imei_kpi ik ON ik.store_id = sto.id 
				AND sstl.staff_id IN (ik.pg_id, ik.pb_sale_id, ik.sale_id, ik.leader_id) 
				AND ik.timing_date >= '" . $pars['from_date'] . " 00:00:00' 
				AND ik.timing_date <= '" . $pars['to_date'] . " 23:59:59'
			WHERE sta.id = " . $pars['staff_id'] . "
			GROUP BY sstl.store_id";
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll();
		$stmt->closeCursor();

		$stmt = $db = null;
		return $data;
	}

	public function SearchStaff($input = '')
	{
		$db = Zend_Registry::get('db');
		$sql = "SELECT 
					s.id as `id`,
					s.code as `code`,
					CONCAT(s.firstname,' ',s.lastname,' | ',s.code) as `text`
				FROM staff s
				WHERE CONCAT(s.firstname,' ',s.lastname) like '%" . $input ."%'
					OR s.code = '" . $input . "'
					OR s.email = '" . $input ."@oppomobile.vn'";
		
		$stmt = $db->prepare($sql);
		$stmt->execute();
		
		$data = $stmt->fetchAll();
		$stmt->closeCursor();

		$stmt = $db = null;
		return $data;
	}

	public function getDetailKpiStaffByPolicy($pars = array())
	{
		$db = Zend_Registry::get('db');

		$sql = "call `SP_imei_kpi_Detail_by_policy`(:table_name, :staff_id, :from_date, :to_date)";

		$stmt = $db->prepare($sql);

		$stmt->bindParam('table_name', $pars['table_name'], PDO::PARAM_STR);
		$stmt->bindParam('staff_id', $pars['staff_id'], PDO::PARAM_INT);
		$stmt->bindParam('from_date', $pars['from_date'], PDO::PARAM_STR);
		$stmt->bindParam('to_date', $pars['to_date'], PDO::PARAM_STR);
		$stmt->execute();
		$data = $stmt->fetchAll();

		$stmt->closeCursor();

		$stmt = $db = null;

		return $data;
	}

	public function report_kpi_asm($date)
	{
		$db = Zend_Registry::get('db');
		$sql = "SELECT * FROM report_kpi_asm WHERE kpi_date = '".date('m-Y', strtotime($date))."'";
		
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$asm = $stmt->fetchAll();

		$stmt->closeCursor();

		$stmt = $db = null;

		return $asm;
	}

	public function report_kpi_leader($date)
	{

		$db = Zend_Registry::get('db');
		$sql = " SELECT * FROM report_kpi_leader WHERE kpi_date = '".$date."' ";
		
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$leader = $stmt->fetchAll();
		
		$stmt->closeCursor();

		$stmt = $db = null;

		return $leader;
	}

	public function report_by_sellout($date)
	{
		$db = Zend_Registry::get('db');
		$sql = " SELECT * FROM report_by_sellout WHERE kpi_date = '".$date."' ";
		

		$stmt = $db->prepare($sql);
		$stmt->execute();
		$sell_out = $stmt->fetchAll();

		$stmt->closeCursor();

		$stmt = $db = null;

		return $sell_out;
	}

	public function report_by_good($date)
	{
		$db = Zend_Registry::get('db');
		$sql = " SELECT * FROM report_by_good WHERE kpi_date = '".$date."' ";
		
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$good = $stmt->fetchAll();

		$stmt->closeCursor();

		$stmt = $db = null;

		return $good;
	}

	public function getKPIByPg($from,$to,$area)
	{
		$db = Zend_Registry::get('db');

		$sql = "call `SP_get_kpi_by_pg`(:from, :to, :area)";

		$stmt = $db->prepare($sql);

		
		$stmt->bindParam('area', $area, PDO::PARAM_INT);
		$stmt->bindParam('from', $from, PDO::PARAM_STR);
		$stmt->bindParam('to', $to, PDO::PARAM_STR);
		$stmt->execute();
		$data = $stmt->fetchAll();

		$stmt->closeCursor();

		$stmt = $db = null;

		return $data;
	}
	public function getKPIByPgImei($from,$to,$area)
	{
		$db = Zend_Registry::get('db');

		$sql = "call `SP_get_kpi_by_pg_imei`(:from, :to, :area)";

		$stmt = $db->prepare($sql);

		
		$stmt->bindParam('area', $area, PDO::PARAM_INT);
		$stmt->bindParam('from', $from, PDO::PARAM_STR);
		$stmt->bindParam('to', $to, PDO::PARAM_STR);
		$stmt->execute();
		$data = $stmt->fetchAll();

		$stmt->closeCursor();

		$stmt = $db = null;

		return $data;
	}
	public function getKPIBySale($from,$to,$area)
	{
		$db = Zend_Registry::get('db');

		$sql = "call `SP_get_kpi_by_sale`(:from, :to, :area)";

		$stmt = $db->prepare($sql);

		
		$stmt->bindParam('area', $area, PDO::PARAM_INT);
		$stmt->bindParam('from', $from, PDO::PARAM_STR);
		$stmt->bindParam('to', $to, PDO::PARAM_STR);
		$stmt->execute();
		$data = $stmt->fetchAll();

		$stmt->closeCursor();

		$stmt = $db = null;

		return $data;
	}

	public function getKPIByLeader($from,$to,$area)
	{
		$db = Zend_Registry::get('db');

		$sql = "call `SP_get_kpi_by_leader`(:from, :to, :area)";

		$stmt = $db->prepare($sql);

		
		$stmt->bindParam('area', $area, PDO::PARAM_INT);
		$stmt->bindParam('from', $from, PDO::PARAM_STR);
		$stmt->bindParam('to', $to, PDO::PARAM_STR);
		$stmt->execute();
		$data = $stmt->fetchAll();

		$stmt->closeCursor();

		$stmt = $db = null;

		return $data;
	}

	public function getKPIByStoreLeader($from,$to,$area)
	{
		$db = Zend_Registry::get('db');

		$sql = "call `SP_get_kpi_by_store_leader`(:from, :to, :area)";

		$stmt = $db->prepare($sql);

		
		$stmt->bindParam('area', $area, PDO::PARAM_INT);
		$stmt->bindParam('from', $from, PDO::PARAM_STR);
		$stmt->bindParam('to', $to, PDO::PARAM_STR);
		$stmt->execute();
		$data = $stmt->fetchAll();

		$stmt->closeCursor();

		$stmt = $db = null;

		return $data;
	}
	
	public function getModelLeader($params)
	{
		$db = Zend_Registry::get('db');

        $stmt_out = $db->prepare("CALL `sp_get_model_leader`(:p_from_date, :p_to_date, :p_staff_id)");
        $stmt_out->bindParam('p_from_date', $params['from_date'], PDO::PARAM_STR);
        $stmt_out->bindParam('p_to_date', $params['to_date'], PDO::PARAM_STR);
        $stmt_out->bindParam('p_staff_id', $params['staff_id'], PDO::PARAM_STR);
        $stmt_out->execute();
        $data = $stmt_out->fetchAll();
        $stmt_out->closeCursor();

		return $data;
	}

	public function getModelArea($params)
	{

		$db = Zend_Registry::get('db');

        if ($params['company'] == 1) { // oppo
            $sql = "call `sp_get_model_area`(:from, :to, :area_id)";
        } elseif ($params['company'] == 2) { // realme
            $sql = "call `sp_get_model_area_realme`(:from, :to, :area_id)";
        }

		$stmt = $db->prepare($sql);

		$params['from'] = $params['from'].' 00:00:00';
		$params['to'] = $params['to'].' 23:59:59';
		
		$stmt->bindParam('from', $params['from'], PDO::PARAM_STR);
		$stmt->bindParam('to', $params['to'], PDO::PARAM_STR);
		$stmt->bindParam('area_id', $params['area_id'], PDO::PARAM_INT);
		$stmt->execute();
		$data = $stmt->fetchAll();

		$stmt->closeCursor();

		$stmt = $db = null;

		return $data;
	}
}