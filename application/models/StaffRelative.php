<?php
class Application_Model_StaffRelative extends Zend_Db_Table_Abstract
{
    protected $_name = 'staff_relative';

    public function getRelative($id_staff) {
        $db   = Zend_Registry::get('db');
        $select       = $db->select()
            ->from(array('str'=>$this->_name),array('str.relative_type','str.full_name','str.gender','str.birth_year','str.job','str.work_place'))
            ->where('staff_id = ?',$id_staff);
        $list_relative = $db->fetchAll($select);
        return $list_relative;
    }
	
	public function exportByStaffId($listStaffId) {
        $db   = Zend_Registry::get('db');
        $select       = $db->select()
            ->from(array('str'=>$this->_name),
                            array('relative_type'=>new Zend_Db_Expr('CASE 
                                                            WHEN str.relative_type=1 THEN "Vợ"
                                                            WHEN str.relative_type=2 THEN "Chồng"
                                                            WHEN str.relative_type=3 THEN "Bố"
                                                            WHEN str.relative_type=4 THEN "Mẹ"
                                                            WHEN str.relative_type=5 THEN "Anh"
                                                            WHEN str.relative_type=6 THEN "Chị"
                                                            WHEN str.relative_type=7 THEN "Con"
                                                            WHEN str.relative_type=8 THEN "Ông bà"
                                                            ELSE "Khác" END'),
                        'rlt_full_name'=>'str.full_name',
                        'gender'=>new Zend_Db_Expr('CASE WHEN str.gender=1 THEN "Nam"
                                                                   ELSE "Nữ"
                                                                   END'),
                        'owner_house' =>  'str.owner_house',
                        'birth_year'=>'str.birth_year',
                        'job'=>'str.job',
                        'work_place'=>'str.work_place',
                        'id_card_number'=>'str.id_card_number',
                    ))
            ->joinInner(array('st' => 'staff'), 'str.staff_id=st.id',
                array('code'=>'st.code',
                    'name'=> new Zend_Db_Expr('CONCAT(st.firstname," ",st.lastname)'),
                    'joined_at'=>'st.joined_at',
                    'off_date'=>'st.off_date',
                ))
            ->joinInner(array('t' => 'team'), 'st.department=t.id',
                array('department'=>'t.name'))
            ->joinInner(array('t1' => 'team'), 'st.team=t1.id',
                array('team'=>'t1.name'))
            ->joinInner(array('t2' => 'team'), 'st.title=t2.id',
                array('title'=>'t2.name'))
            ->joinInner(array('rm' => 'regional_market'), 'st.regional_market=rm.id',
                array())
            ->joinInner(array('a' => 'area'), 'rm.area_id=a.id',
                array('area_name'=>'a.name'))
            ->where('str.staff_id IN (?)', $listStaffId);
        $list_relative = $db->fetchAll($select);
        return $list_relative;
    }

}