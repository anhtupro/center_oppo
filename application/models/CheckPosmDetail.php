<?php

class Application_Model_CheckPosmDetail extends Zend_Db_Table_Abstract
{
    protected $_name = 'check_posm_detail';
    protected $_schema = DATABASE_TRADE;

    public function getListCategory($stage_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['d' => DATABASE_TRADE.'.check_posm_detail'], [
                         'c.*'
                     ])
                      ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'd.category_id = c.id', [])
                    ->where('d.stage_id = ?', $stage_id)
                    ->group('d.category_id');

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getCategory($stage_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['d' => DATABASE_TRADE.'.check_posm_detail'], [
                'c.*'
            ])
            ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'd.category_id = c.id', [])
            ->where('d.stage_id = ?', $stage_id);

        $result = $db->fetchAll($select);

        return $result;
    }
}