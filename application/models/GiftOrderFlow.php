<?php
class Application_Model_GiftOrderFlow extends Zend_Db_Table_Abstract
{
    protected $_name = 'gift_order_flow';

    function fetchAllData(){
        $db = Zend_Registry::get('db');

        $cols = array(
            'p.order_id',
            'approved_staff_id'         => "st.id",
            'approved_staff_title'      => "st.title",
            'approved_staff_team'       => "st.team",
            'approved_staff_department' => "st.department",
            "approved_staff_name"       => "CONCAT(st.firstname, ' ',st.lastname)",
            "p.has_rejected"
        );

        $select = $db->select()->from(array('p' => $this->_name), $cols);
        $select->joinLeft(array('st'=>'staff'),'st.id = p.created_by',array());
        $select->where('p.step != ?', 0);
        $data = $db->fetchAll($select);

        $result = array();

        foreach ($data as $key => $value) {
            $result[$value['order_id']][] = array(
                'approved_staff_id'         => $value["approved_staff_id"],
                'approved_staff_title'      => $value["approved_staff_title"],
                'approved_staff_team'       => $value["approved_staff_team"],
                'approved_staff_department' => $value["approved_staff_department"],
                "approved_staff_name"       => $value["approved_staff_name"],
                "has_rejected"              => $value["has_rejected"]
            );
        }

        return $result;
    }

    function fetchMiniData($id){
        $db = Zend_Registry::get('db');
        $cols = array(
            'approved_staff_title'      => "st.title",
            'approved_staff_team'       => "st.team",
            'approved_staff_department' => "st.department",
            "approved_staff_name"       => "CONCAT(st.firstname, ' ',st.lastname)",
            "p.has_rejected", "p.has_approved"
        );
        $select = $db->select()->from(array('p' => $this->_name), $cols);
        $select->joinLeft(array('st'=>'staff'),'st.id = p.created_by',array());
        $select->where('p.step != ?', 0);
        $select->where('p.order_id = ?', $id);
        $result = $db->fetchAll($select);
        return $result;
    }
}