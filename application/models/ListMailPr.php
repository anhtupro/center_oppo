<?php
class Application_Model_ListMailPr extends Zend_Db_Table_Abstract
{
    protected $_name = 'list_mail_pr';
    protected $_schema = 'temp';
    public function fetchAllData() {
        $db = Zend_Registry::get('db');
        $select = $db->select()
        ->from(array('lmk' => 'temp.'.$this->_name),array("lmk.id","lmk.gender","lmk.name","lmk.content","lmk.ka","lmk.email","lmk.link_img"));
        $result = $db->fetchAll($select);
        return $result;
    }
}
?>