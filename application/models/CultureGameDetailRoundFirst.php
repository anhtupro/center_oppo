<?php
class Application_Model_CultureGameDetailRoundFirst extends Zend_Db_Table_Abstract
{
	protected $_name = 'culture_game_detail_round_first';


	public function getTimeStartQuestion($params){
		$db = Zend_Registry::get("db");
        $select = $db->select();

       $arrCols = array(
            'p.started_at'
        );

        $select->from(array('p' => 'culture_game'), $arrCols);        
        $select->where('p.id = ?', $params['id_master']);
        $select->where('p.round = ?', $params['round']);
        $result = $db->fetchAll($select);
      
        return $result;
	}
    public function getTimeStartQuestionRound4($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

       $arrCols = array(
            'p.started_at'
        );

        $select->from(array('p' => 'culture_game'), $arrCols);        
        $select->where('p.staff_id = ?', $params['staff_id']);
        $select->where('p.round = ?', $params['round']);
        $result = $db->fetchAll($select);
      
        return $result;
    }
}