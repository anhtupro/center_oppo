<?php
class Application_Model_AppraisalSalePlan extends Zend_Db_Table_Abstract
{
    protected $_name = 'appraisal_sale_plan';
    protected $_primary = 'asp_id';

    /**
     * @return null|array
     */
    public function getLastPlan()
    {
        if($_GET['dev'] == 1){
            $plan = $this->fetchRow([
                $this->getAdapter()->quoteInto('asp_id = 4',  null),
            ], ['asp_id desc']);
            if($plan) {
                $plan = $plan->toArray();
            }
            return $plan;
        }
        
        $plan = $this->fetchRow([
            $this->getAdapter()->quoteInto('asp_is_deleted = 0 and asp_status = 1',  null),
            $this->getAdapter()->quoteInto('asp_from <= ?', date('Y-m-d')),
        ], ['asp_id desc']);
        if($plan) {
            $plan = $plan->toArray();
        }
        return $plan;
    }

    /**
     * @return null|array
     */
    public function getLastPlanData()
    {
        $plan = $this->fetchRow($this->getAdapter()->quoteInto('asp_is_deleted = 0 and asp_status != 0',  null), ['asp_id desc']);
        if($plan) {
            $plan = $plan->toArray();
        }
        return $plan;
    }

    /**
     * @param $planId
     * @return array|null|Zend_Db_Table_Row_Abstract
     */
    public function getPlanById($planId)
    {
        $plan = $this->fetchRow($this->getAdapter()->quoteInto('asp_id = ?', $planId));
        if($plan) {
            $plan = $plan->toArray();
        }
        return $plan;
    }

    /**
     * @return array
     */
    public function getLastTopTenPlans()
    {
        $plans = $this->fetchAll($this->getAdapter()->quoteInto('asp_is_deleted = 0',  null), ['asp_id desc'], 10)->toArray();
        $tmp = [];
        foreach ($plans as $plan) {
            $tmp[] = [
                'asp_id' => $plan['asp_id'],
                'asp_name' => $plan['asp_name'],
            ];
        }
        return $tmp;
    }
}