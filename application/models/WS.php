<?php
class Application_Model_WS
{
    private $wssCSURI;
    private $namespace;

    public function __construct()
    {
        error_reporting(0);

        require_once 'My'.DIRECTORY_SEPARATOR.'nusoap'.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'nusoap.php';

        $this->wssCSURI = WSS_CS_URI;
		$this->wssTKURI = WSS_TK_URI;
        $this->namespace = 'OPPOVN';
    }

    public function getListMenus()
    {
        $client = new nusoap_client($this->wssCSURI);
        try {
            $result = $client->call("getListMenus");
            return $result;

        } catch (Exception $e) {
        }
        return null;
    }

    public function getListActions()
    {
        $client = new nusoap_client($this->wssCSURI);

        try {
            $result = $client->call("getListActions");
            return $result;

        } catch (Exception $e) {
        }
        return null;
    }

    public function getAllShowroom()
    {
        $client = new nusoap_client($this->wssCSURI);
        $client->decode_utf8 = false;
        try {
            $result = $client->call("getAllShowroom", array(
                'latitude'  =>0,
                'longitude' =>0,
                'distance'  =>0,
                'statusBit' =>-1,
            ));
            return $result;

        } catch (Exception $e) {
        }
        return null;
    }

    public function getUser($username)
    {
        $client = new nusoap_client($this->wssCSURI);
        $client->setCredentials(WS_USERNAME, WS_PASSWORD, "basic");
        $result = $client->call("getUser", array('username'=>$username));
        $err=$client->getError();
       // var_dump($err);exit;
        return $result;
    }

    public function truncateTable($table)
    {
        set_time_limit(0);

        $client = new nusoap_client($this->wssCSURI);

        try {
            $result = $client->call("truncateTable", array('table' => $table));
            return $result;

        } catch (Exception $e) {}

        return null;
    }

    public function syncTable($data, $options)
    {
        set_time_limit(0);

        $client = new nusoap_client($this->wssCSURI);

        try {
            $result = $client->call("syncTable", array('data' => $data, 'options' => $options));
            return $result;

        } catch (Exception $e) {}

        return null;
    }
	
	public function getQueryTimeKeeping()
    {
        $client = new nusoap_client($this->wssTKURI);

        try {
            $password = md5('abcdef');
            $result = $client->call("wsGetTimekeeping", array('System' => 'CENTER', 'Password' => "$password"));
            return $result;

        } catch (Exception $e) {
        }
        return null;
    }
}