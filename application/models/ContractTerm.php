<?php
class Application_Model_ContractTerm extends Zend_Db_Table_Abstract
{
	protected $_name = 'contract_term';

    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        if (isset($params['name']) and $params['name'])
            $select->where('p.name LIKE ?', '%'.$params['name'].'%');

        $select->order('p.name', 'COLLATE utf8_unicode_ci ASC');

        $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }


    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if ($result === false) {

            $data = $this->fetchAll(null,'level');

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->name;
                }
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }

    function get_cache_all(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_all_cache');

        if ($result === false) {

            $data = $this->fetchAll(null,'level');

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item;
                }
            }
            $cache->save($result, $this->_name.'_all_cache', array(), null);
        }
        return $result;
    }

    public static function get_next_contract($current_contract_id){
        $db = Zend_Registry::get('db');
        $current_contract_id = intval($current_contract_id);
        $sql = "SELECT b.*, c.id nid, c.addday nadd_day, c.`level` nlevel
            	FROM
            	(
            		SELECT a.id, MIN(b.id) nid
            		FROM contract_term a
            		LEFT JOIN contract_term b ON a.level < b.level
            		GROUP BY a.id
            	) a INNER JOIN contract_term b ON a.id = b.id
            	LEFT JOIN contract_term c ON a.nid = c.id
            	WHERE b.id = ?
                ";
        $stmt = $db->query($sql,array($current_contract_id));
        $result = $stmt->fetch();
        return $result;
    }
}                                                      
