<?php
class Application_Model_StaffDateoffReason extends Zend_Db_Table_Abstract
{
    protected $_name = 'staff_offdate_reason';

    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if ($result === false) {
            $where   = [];
            $where[] = $this->getAdapter()->quoteInto('is_hidden = ? ', 0);
            $data = $this->fetchAll($where, 'name');
//            $data = $this->fetchAll(null, 'name');

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->name;
                }
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }
}