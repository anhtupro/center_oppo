<?php
class Application_Model_DuplicatedImei extends Zend_Db_Table_Abstract
{
	protected $_name = 'duplicated_imei';

    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));


        $select->joinLeft(['img' => 'duplicated_imei_file'], 'p.id = img.duplicated_id', ['list_image' => "GROUP_CONCAT( img.url SEPARATOR '***' )"]);
        // lấy thông tin nhà sellin

        $select->joinLeft(['i' => WAREHOUSE_DB . '.imei'], 'p.imei = i.imei_sn', []);
        $select->joinLeft(['d' => WAREHOUSE_DB . '.distributor'], 'i.distributor_id = d.id', ['distributor_name' => 'd.title']);

        // theo hiện tại
        $select->joinLeft(['ts' => 'timing_sale'], 'p.imei = ts.imei', []);
        $select->joinLeft(['t' => 'timing'], 'ts.timing_id = t.id', [
            'real_timing_date' => 't.from'
        ]);
        $select->joinLeft(['rs' => 'store'], 't.store = rs.id', [
            'real_store_name' => 'rs.name'
        ]);
        $select->joinLeft(['rst' => 'staff'], 'rst.id = t.staff_id', [
            'real_staff_name' => "CONCAT(rst.firstname, ' ', rst.lastname)",
            'real_staff_email' => 'rst.email'
        ]);


        // theo first timing
        $select->joinLeft(['s1' => 'store'], 'p.store_id_first = s1.id', []);
        $select->joinLeft(['dis1' => 'regional_market'], 's1.district = dis1.id', []);
        $select->joinLeft(['pro1' => 'regional_market'], 'dis1.parent = pro1.id', []);
        $select->joinLeft(['a1' => 'area'], 'pro1.area_id = a1.id', ['area_name_first' => 'a1.name', 'area_id_first' => 'a1.id']);


        // Tìm theo ngày chấm công
        $select->where('p.date >= ?', $params['from_date']);
        $select->where('p.date <= ?', $params['to_date']);

        // tìm theo IMEI
        if (isset($params['imei']) and $params['imei'])
            $select->where('p.imei = ?', $params['imei']);

        // For Second Timing ///////////////////////////////////////////////////////////////////////
        $select->joinLeft(array('s'=>'staff'), 's.id=p.staff_id', array('s.firstname', 's.lastname', 's.email', 's.phone_number'));
        $select->joinLeft(array('st'=>'store'), 'st.id=p.store_id', array('st.name'));
        $select->joinLeft(['dis2' => 'regional_market'], 'st.district = dis2.id', []);
        $select->joinLeft(['pro2' => 'regional_market'], 'dis2.parent = pro2.id', []);
        $select->joinLeft(['a2' => 'area'], 'pro2.area_id = a2.id', ['area_name_second' => 'a2.name', 'area_id_second' => 'a2.id']);

        if (isset($params['staff_id']) and $params['staff_id'])
            $select->where('p.staff_id = ?', $params['staff_id']);

        if (isset($params['name']) and $params['name'])
            $select->where('CONCAT(s.firstname, " ",s.lastname) LIKE ?', '%'.$params['name'].'%');

        if (isset($params['email']) and $params['email']) {
            $params['email'] = preg_replace('/'.EMAIL_SUFFIX.'/', '', $params['email']);
            $select->where('s.email LIKE ?', $params['email'].EMAIL_SUFFIX);
        }

        if (isset($params['phone_number']) and $params['phone_number']) {
            $select->where('s.phone_number = ?', $params['phone_number']);
        }

        //
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $uid = $userStorage->id;
        $group_id = $userStorage->group_id;
        $title_id = $userStorage->title;
        // echo $group_id;die;
        // $title_id == BOARD_ID || $title_id == SALES_EXT_ID
        // Nếu sếp vào xem thì
        if (  in_array($title_id, [SALES_ASSISTANT_TITLE, SALES_MANAGER_TITLE, SALES_SUPERVISOR_TITLE]) || $uid == SUPERADMIN_ID ) {

            // chỉ lấy các báo cáo mà ASM đã duyệt
            $select->where('p.asm_check = 1');
            // $select->where('p.asm_check = 0');

            // Tìm các báo cáo đã/chưa xử lý dành cho sếp
            if ( isset($params['solved']) && $params['solved'] == 1 ) {
                $select->where('p.solved = ?', 1);
            } elseif ( isset($params['solved']) && $params['solved'] == 0 ) {
                $select->where('p.solved = ?', 0);
            }

            // Nếu ASM thì
        } elseif (  in_array( $group_id, array(ASM_ID) )  || in_array($title_id, [SALES_ADMIN_TITLE, RM_TITLE, RM_STANDBY_TITLE])) {
            $QModel = new Application_Model_Asm();
            $list_regions = $QModel->get_cache($uid);

            if ($list_regions && isset($list_regions['district']))
                $list_regions = $list_regions['district'];

            if($group_id == SALES_ADMIN_ID){
                $select->where(' ( p.asm_check = 1) or (p.solved = 0 AND p.asm_check = 0)');
            } else
            {
                $select->where(' ( p.asm_check = 1) or (p.solved = 0 AND p.asm_check = 0 )');
            }

            // lấy các báo cáo ASM chưa duyệt


            // Tìm các báo cáo đã/chưa xử lý dành cho ASM
            if ( isset($params['solved']) && $params['solved'] == 1 ) {
                $select->where('p.asm_check = ?', 1);
            } elseif ( isset($params['solved']) && $params['solved'] == 0 ) {
                $select->where('p.asm_check = ?', 0);
            }
        } else {
            echo 'Không có quyền xem báo cáo imei trùng'; die;
            // Tìm các báo cáo đã/chưa xử lý dành cho SUPERADMIN
//            if ( isset($params['solved']) && $params['solved'] == 1 ) {
//                $select->where('p.solved = ?', 1);
//            } elseif ( isset($params['solved']) && $params['solved'] == 0 ) {
//                $select->where('p.solved = ?', 0);
//            }
        }

        $QRegionalMarket = new Application_Model_RegionalMarket();

        if (  in_array( $group_id, array(ASM_ID) )  || in_array($title_id, [SALES_ADMIN_TITLE, RM_TITLE, RM_STANDBY_TITLE])) {
            if (is_array($list_regions) && count($list_regions) > 0) {
//                $select->where('st.district IN (?)', $list_regions);
                $select->where('a1.id IN (?) OR a2.id IN (?)', $userStorage->list_area);
            } else {
                $select->where('1=0');
            }


        } elseif ( in_array($title_id, [SALES_ASSISTANT_TITLE]) || $uid == SUPERADMIN_ID) {
        }

        if (isset($params['area_id']) && $params['area_id']) {
            $select->where('a2.id = ?', $params['area_id']);
//            $list_district = $QRegionalMarket->get_district_by_area_cache($params['area_id']);
//
//            if (is_array($list_district) && count($list_district) > 0)
//                $select->where('st.district IN (?)', $list_district);
//            else
//                $select->where('1=0');
        }

        // tìm theo tỉnh
        if (isset($params['regional_market']) and intval($params['regional_market']) > 0)
            $select->where('st.regional_market = ?', $params['regional_market']);

        // tìm theo store
        if (isset($params['store']) and $params['store'])
            $select->where('p.store_id = ?', $params['store']);
        // END Second Timing ///////////////////////////////////////////////////////////////////////

        // For First Timing ///////////////////////////////////////////////////////////////////////
        $select->joinLeft(array('ss'=>'staff'), 'ss.id=p.staff_id_first', array(
            'firstname_first'    => 'ss.firstname',
            'lastname_first'     => 'ss.lastname',
            'email_first'        => 'ss.email',
            'phone_number_first' => 'ss.phone_number',
        ));

        // tìm tên
        if (isset($params['name_first']) and $params['name_first'])
            $select->where('CONCAT(ss.firstname, " ",ss.lastname) LIKE ?', '%'.$params['name_first'].'%');

        // tìm email
        if (isset($params['email_first']) and $params['email_first']) {
            $params['email_first'] = preg_replace('/'.EMAIL_SUFFIX.'/', '', $params['email_first']);
            $select->where('ss.email LIKE ?', $params['email_first'].EMAIL_SUFFIX);
        }

        // tìm số dt
        if (isset($params['phone_number_first']) and $params['phone_number_first']) {
            $select->where('ss.phone_number = ?', $params['phone_number_first']);
        }

        // tìm theo area
        if (isset($params['area_id_first']) and $params['area_id_first']){
//            $QRegionalMarket = new Application_Model_RegionalMarket();
//
//            if (is_array($params['area_id_first'])) {
//                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id IN (?)', $params['area_id_first']);
//            } else {
//                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $params['area_id_first']);
//            }
//
//            $regional_markets = $QRegionalMarket->fetchAll($where);
//            $tem = array();
//            foreach ($regional_markets as $regional_market)
//                $tem[] = $regional_market->id;

//            $select->where('ss.regional_market IN (?)', $tem);
            $select->where('a1.id = ?', $params['area_id_first']);
        }

        // tìm theo tỉnh
        if (isset($params['regional_market_first']) and intval($params['regional_market_first']) > 0)
            $select->where('ss.regional_market = ?', $params['regional_market_first']);

        // tìm theo store
        if (isset($params['store_first']) and $params['store_first']) {
            $QTiming = new Application_Model_Timing();
            $where = $QTiming->getAdapter()->quoteInto('store = ?', $params['store_first']);
            $timings = $QTiming->fetchAll($where);

            $timings_arr = array();
            foreach ($timings as $t_id => $timing) {
                $timings_arr[] = $timing['id'];
            }

            $QTimingSales = new Application_Model_TimingSale();
            $where = $QTimingSales->getAdapter()->quoteInto('timing_id IN (?)', $timings_arr);
            $timing_sales = $QTimingSales->fetchAll($where);

            $timing_sales_arr = array();
            foreach ($timing_sales as $ts_id => $timing_sale) {
                $timing_sales_arr[] = $timing_sale['id'];
            }

            $select->where('p.timing_sales_first IN (?)', $timing_sales_arr);
        }
        if ($params['duplicate_type']) {
            $select->where('p.duplicate_type = ?', $params['duplicate_type']);
        }
        // END First Timing ///////////////////////////////////////////////////////////////////////
        $select->group('p.id');
        $select->order(array('p.id DESC'));

        if(!empty($_GET['dev'])){
            echo $select->__toString();
            exit;
        }

        if( ( ( isset($params['export']) && $params['export'] != 1 ) || !isset($params['export']) ) && $limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }


    function get_timing_sales_info_cache($id = 0)
    {  
		$cache  = Zend_Registry::get('cache');
		$result = $cache->load('timing_sales_info_cache');
        // echo "<pre>";print_r($result);die;

		if (!$result || empty($result[$id])) {

	    	$sql = "SELECT
						st.`name`,
						t.`from`,
						s.firstname,
						s.lastname,
						s.email,
						s.phone_number,
						ts.photo,
						t.created_at,
						t.approved_at,
						ss.firstname AS a_firstname,
						ss.lastname AS a_lastname,
						c.`name` AS customer_name,
						c.address AS customer_address,
						c.phone_number AS customer_phone
					FROM
						timing_sale ts
					INNER JOIN timing t ON ts.timing_id = t.id
					AND ts.id = ?
					LEFT JOIN staff s ON s.id = t.staff_id
					LEFT JOIN staff ss ON t.approved_by = ss.id
					LEFT JOIN store st ON t.store = st.id
					LEFT JOIN customer c ON ts.customer_id = c.id";

			$db = Zend_Registry::get('db');
			$res = $db->query($sql, array($id));
            // echo "<pre>";print_r($res);die;
			if (!$res) {
				return false;
			} else {
				$res = $res->fetch();

				if (!$res) {
					$sql = "SELECT
								st.`name`,
								t.`from`,
								s.firstname,
								s.lastname,
								s.email,
								s.phone_number,
								ts.photo,
								t.created_at,
								t.approved_at,
								ss.firstname AS a_firstname,
								ss.lastname AS a_lastname,
								c.`name` AS customer_name,
								c.address AS customer_address,
								c.phone_number AS customer_phone
							FROM
								timing_sale_log ts
							INNER JOIN timing t ON ts.timing_id = t.id
							AND ts.id = ?
							LEFT JOIN staff s ON s.id = t.staff_id
							LEFT JOIN staff ss ON t.approved_by = ss.id
							LEFT JOIN store st ON t.store = st.id
							LEFT JOIN customer c ON ts.customer_id = c.id";

					$res = $db->query($sql, array($id));

					$res = $res->fetch();
				}
			}

			if (!$result) {
				$result = array();
			}
			
			$result[$id] = array(
				'name'              => $res['name'],
				'from'              => $res['from'],
				'firstname'         => $res['firstname'],
				'lastname'          => $res['lastname'],
				'email'             => $res['email'],
				'phone_number'      => $res['phone_number'],
				'photo'             => $res['photo'],
				'customer_name'     => $res['customer_name'],
				'customer_phone'    => $res['customer_phone'],
				'customer_address'  => $res['customer_address'],
				'created_at'        => $res['created_at'],
				'approved_at'       => $res['approved_at'],
				'approve_firstname' => $res['a_firstname'],
				'approve_lastname'  => $res['a_lastname'],
			);
			
			$cache->save($result, 'timing_sales_info_cache', array(), null);
		}

		return $result[$id];
        echo "<pre>";print_r($result);die;
    }

    function get_imei_model_cache($imei = 0)
    {
    	$cache  = Zend_Registry::get('cache');
		$result = $cache->load('imei_model_cache');

    	

		if (!$result || empty($result[$imei]) || empty($result[$imei]['activated_at'])) {

	    	$sql = "SELECT
						i.imei_sn,
						p.`desc` AS product_name,
						c.`name` AS color_name
					FROM
						".WAREHOUSE_DB.'.'."`imei` i
					INNER JOIN ".WAREHOUSE_DB.'.'."good p ON i.good_id = p.id
					INNER JOIN ".WAREHOUSE_DB.'.'."good_color c ON i.good_color = c.id
					WHERE
						i.imei_sn = ?";

			$db = Zend_Registry::get('db');
			$res = $db->query($sql, array($imei));

			if (!$res) {
				return false;
			} else {
				$res = $res->fetch();
			}

			if (!$result) {
				$result = array();
			}
			
			$result[$imei] = array(
				'product_name' => $res['product_name'],
				'color_name'   => $res['color_name'],
			);


	    	$sql = "SELECT i.activated_date as activated_at FROM ".WAREHOUSE_DB.'.'."imei i
					WHERE i.imei_sn = ?";

			$res = $db->query($sql, array($imei));
			$res = $res->fetch();

			if ($res) {
				$result[$imei]['activated_at'] = $res['activated_at'];
			}
			
			$cache->save($result, 'imei_model_cache', array(), null);
		}

		return $result[$imei];
    }

    function get_model($imei = 0)
    {
    	$sql = "
            SELECT p.id AS product_id, c.id AS color_id
            FROM ".WAREHOUSE_DB.'.'."imei i
                INNER JOIN ".WAREHOUSE_DB.'.'."good_color c
                    ON i.good_color=c.id
                INNER JOIN ".WAREHOUSE_DB.'.'."good p
                    ON p.id = i.good_id
			WHERE i.imei_sn=?
			";

		$db = Zend_Registry::get('db');
		$model = $db->query($sql, array($imei));
		$model = $model->fetch();

		if ($model) {
			return $model;
		} else {
			return null;
		}
    }

    // function get_cache(){
    //     $cache      = Zend_Registry::get('cache');
    //     $result     = $cache->load($this->_name.'_cache');

    //     if ($result === false) {

    //         $data = $this->fetchAll();

    //         $result = array();
    //         if ($data){
    //             foreach ($data as $item){
    //                 $result[$item->id] = $item->name;
    //             }
    //         }
    //         $cache->save($result, $this->_name.'_cache', array(), null);
    //     }
    //     return $result;
    // }
	
	//update staff first
	public function updateStaffFirst($data, $params) {
        $db = Zend_Registry::get('db');
        $sql = "
			UPDATE duplicated_imei d
			SET d.staff_win = d.staff_id_first , d.`director_note` = :director_note, d.`solved` = :solved, d.`solved_by` = :solved_by, d.`solved_at` = :solved_at
			WHERE d.imei = :imei AND d.asm_check = 1 AND d.date >= :from_date AND d.date <= :to_date
			";

        $stmt = $db->prepare($sql);
        $stmt->bindParam('director_note', $data['director_note'], PDO::PARAM_STR);
        $stmt->bindParam('solved', $data['solved']);
        $stmt->bindParam('solved_by', $data['solved_by']);
        $stmt->bindParam('solved_at', $data['solved_at']);
        $stmt->bindParam('imei',  $data['imei'], PDO::PARAM_STR);
        $stmt->bindParam('from_date', $params['from_date']);
        $stmt->bindParam('to_date', $params['to_date']);

        $stmt->execute();
        $stmt->closeCursor();
        $db = $stmt = null;
    }
	
	//get timing sale log
	public function getTimingSaleLog($value){
        $db = Zend_Registry::get('db');
		$sql = "SELECT MAX(ts.id_main) id_main_max, ts.id
				FROM timing_sale_log ts
				WHERE ts.`imei` = :imei
			";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('imei', $value, PDO::PARAM_STR);
        $stmt->execute();
        $getData = $stmt->fetchAll();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $getData;
    }
	
	//get data duplicate sau khi update
	public function getDupliImei($imei, $params){
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['d' => 'duplicated_imei'], [
              'd.*'
            ])
            ->where('d.imei = ?', $imei)
            ->where('d.asm_check = ?', 1)
            ->where('d.date >= ?', $params['from_date'])
            ->where('d.date <= ?', $params['to_date']);
         $result = $db->fetchRow($select);

         return $result;

    }
	
	//get data duplicate sau khi update
	public function getModel($value){
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['i' => WAREHOUSE_DB . '.imei'], [
                'product_id' => 'p.id',
                'color_id' => 'c.id'
            ])
            ->join(['c' => WAREHOUSE_DB . '.good_color'], 'i.good_color = c.id', [])
            ->join(['p' => WAREHOUSE_DB . '.good'], 'p.id = i.good_id', [])
            ->where('i.imei_sn = ?', $value);

        $result = $db->fetchAll($select);

        return $result;
    }
	
	//insert into timing sale log
	public function insertTimingSaleLog($data){
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare("
			INSERT INTO `timing_sale_log`(`id`, `product_id`, `timing_id`, `quantity`, `model_id`, `price`, `customer_id`, `imei`, `photo`, `reimport`, `turn`, `customer_name`, `address`, `phone_number`, `amortization`, `created_at`, `note`)
			SELECT ts.`id`, ts.`product_id`, ts.`timing_id`, ts.`quantity`, ts.`model_id`, ts.`price`, ts.`customer_id`, ts.`imei`, ts.`photo`, ts.`reimport`, ts.`turn`, ts.`customer_name`, ts.`address`, ts.`phone_number`, ts.`amortization`, :create_at, :note
			FROM timing_sale ts
			WHERE ts.`imei` = :imei
        ");

        $stmt->bindParam('imei', $data['imei'], PDO::PARAM_STR);
        $stmt->bindParam('create_at', $data['create_at']);
        $stmt->bindParam('note', $data['note'], PDO::PARAM_STR);

        $res = $stmt->execute();
        $stmt->closeCursor();
        $stmt = null;
        return $res;
    }
	
	//update staff second
	public function updateStaffSecond($data, $params) {
        $db = Zend_Registry::get('db');
        $sql = "
			UPDATE duplicated_imei d
			SET d.`director_note` = :director_note, d.`solved` = :solved, d.`solved_by` = :solved_by, d.`solved_at` = :solved_at, d.`staff_win` = d.`staff_id`, d.timing_sale_log_id = :timing_sale_log_id
			WHERE d.imei = :imei AND d.asm_check = 1 AND d.date >= :from_date AND d.date <= :to_date
			";

        $stmt = $db->prepare($sql);
        $stmt->bindParam('director_note', $data['director_note'], PDO::PARAM_STR);
        $stmt->bindParam('solved', $data['solved']);
        $stmt->bindParam('solved_by', $data['solved_by']);
        $stmt->bindParam('timing_sale_log_id', $data['timing_sale_log_id']);
        $stmt->bindParam('solved_at', $data['solved_at']);
        $stmt->bindParam('imei',  $data['imei'], PDO::PARAM_STR);
        $stmt->bindParam('from_date', $params['from_date']);
        $stmt->bindParam('to_date', $params['to_date']);

        $stmt->execute();
        $stmt->closeCursor();
        $db = $stmt = null;
    }
	
	//check imei được báo lần 2 chưa
	public function checkSecondImei($data) {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['d' => 'duplicated_imei'], [
                'd.*'
            ])
            ->joinLeft(['s' => 'store'], 'd.store_id = s.id', [])
            ->joinLeft(['dis' => 'regional_market'], 's.district = dis.id', [])
            ->joinLeft(['p' => 'regional_market'], 'dis.parent = p.id', [])

            ->join(['s2' => 'store'], 's2.id = ' . $data['store_id'], [])
            ->joinLeft(['dis2' => 'regional_market'], 's2.district = dis2.id', [])
            ->joinLeft(['p2' => 'regional_market'], 'dis2.parent = p2.id', [])

            ->where('MONTH(d.date) = ?', $data['month_check'])
            ->where('d.imei = ?', $data['imei_check'])
            ->where('p.area_id = p2.area_id' );

        $result = $db->fetchRow($select);

        return $result;
    }


    //check imei được báo trùng cùng store
    public function checkSameStore($imei, $store) {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['ts' => 'timing_sale'], [
                'ts.imei'
            ])
            ->join(['t' => 'timing'], 'ts.timing_id = t.id', [])
            ->where('ts.imei = ?', $imei)
            ->where('t.store = ?', $store);

        $result = $db->fetchOne($select);

        return $result;
    }

	
	//check imei được báo dup >1
	public function checkDupImei($imei, $params) {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['di' => 'duplicated_imei'], [
                 'di.*',
                 'count' => "COUNT(di.imei)"
             ])
            ->where('di.imei = ?', $imei)
            ->where('di.asm_check = ?', 1)
            ->where('di.solved = ?', 0)
            ->where('di.date >= ?', $params['from_date'])
            ->where('di.date <= ?', $params['to_date'])
            ->group(['di.imei'])
            ->having('count > 1');
//        echo $select; die;
        $getData = $db->fetchAll($select);

        return $getData;
    }


    public function checkValidImei($imei, $params) {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['di' => 'duplicated_imei'], [
                'di.id'
            ])
            ->where('di.imei = ?', $imei)
            ->where('di.asm_check = ?', 1)
            ->where('di.solved = ?', 0)
            ->where('di.date >= ?', $params['from_date'])
            ->where('di.date <= ?', $params['to_date']);

//        echo $select; die;
        $getData = $db->fetchAll($select);

        return count($getData) > 0 ? true : false;
    }
	
	public function getIdPgTimingSaleRm($imei) {
        $db = Zend_Registry::get('db');
        $sql = "SELECT ts.`id` timing_sales_first, t.`staff_id` staff_id_first
				FROM realme_hr.timing_sale_realme ts
				INNER JOIN realme_hr.timing_realme t ON t.`id` = ts.`timing_id`
				WHERE ts.`imei` = :imei";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('imei', $imei, PDO::PARAM_STR);
        $stmt->execute();
        $getData = $stmt->fetchAll();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $getData;
    }

    public function export($dup_imeis, $imei_model, $points, $timing_sales)
    {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        // export
        $heads = array(
            'IMEI',
            'Model',
            'Activated At',
            'Dealer Sellin',
            '#',
            'Real staff',
            'Real email',
            'Real store',
            'Real timing date',
            '#',
            'First Staff',
            '1st Email',
            '1st Phone Number',
            '1st Timing For',
            '1st Store',
            '1st Customer Name',
            '1st Customer Phone',
            '1st Customer Address',
//            '1st Have Photo',

            '#',
            'Second Staff',
            '2nd Email',
            '2nd Phone Number',
            '2nd Timing For',
            '2nd Store',
            '2nd Customer Name',
            '2nd Customer Phone',
            '2nd Customer Address',
//            '2nd Have Photo',
            '2nd Note',
            'Approve For',
            'Hệ thống Note',
        );


        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;

        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($dup_imeis as $imei){
            $alpha    = 'A';
            $tmp = $timing_sales[ $imei['timing_sales_first'] ];


            $sheet->getCell($alpha++ . $index)->setValueExplicit($imei['imei'], PHPExcel_Cell_DataType::TYPE_STRING);

            $sheet->setCellValue($alpha++.$index, $imei_model[ $imei['imei'] ]['product_name'] . '/'.$imei_model[ $imei['imei'] ]['color_name']);
            $sheet->setCellValue($alpha++.$index, date('d/m/Y H:i:s', strtotime($imei_model[ $imei['imei'] ]['activated_at'])));
            $sheet->setCellValue($alpha++.$index, $imei['distributor_name'] ? $imei['distributor_name'] : '');

            $sheet->setCellValue($alpha++.$index, '|');

            $sheet->setCellValue($alpha++.$index, $imei['real_staff_name'] ? $imei['real_staff_name'] : '');
            $sheet->setCellValue($alpha++.$index, $imei['real_staff_email'] ? $imei['real_staff_email'] : '');
            $sheet->setCellValue($alpha++.$index, $imei['real_store_name'] ? $imei['real_store_name'] : '');
            $sheet->setCellValue($alpha++.$index, $imei['real_timing_date'] ?  date('d/m/Y', strtotime($imei['real_timing_date'])) : '');

            $sheet->setCellValue($alpha++.$index, '|');

            $sheet->setCellValue($alpha++.$index, $tmp['firstname'].' '.$tmp['lastname']);
            $sheet->setCellValue($alpha++.$index, $tmp['email']);
            $sheet->setCellValue($alpha++.$index, $tmp['phone_number']);
            $sheet->setCellValue($alpha++.$index, $tmp['from'] ? date('d/m/Y', strtotime($tmp['from'])) : '');
            $sheet->setCellValue($alpha++.$index, $tmp['name']);
            $sheet->setCellValue($alpha++.$index, $tmp['customer_name']);
            $sheet->setCellValue($alpha++.$index, $tmp['customer_phone']);
            $sheet->setCellValue($alpha++.$index, $tmp['customer_address']);
//            $sheet->setCellValue($alpha++.$index, !empty($tmp['photo']) ? 'X' : '');

            $sheet->setCellValue($alpha++.$index, '|');

            $sheet->setCellValue($alpha++.$index, $imei['firstname'].' '.$imei['lastname']);
            $sheet->setCellValue($alpha++.$index, $imei['email']);
            $sheet->setCellValue($alpha++.$index, $imei['phone_number']);
            $sheet->setCellValue($alpha++.$index, $imei['date'] ? date('d/m/Y', strtotime($imei['date'])) : '');
            $sheet->setCellValue($alpha++.$index, $imei['name']);
            $sheet->setCellValue($alpha++.$index, $imei['customer_name']);
            $sheet->setCellValue($alpha++.$index, $imei['customer_phone']);
            $sheet->setCellValue($alpha++.$index, $imei['customer_address']);
//            $sheet->setCellValue($alpha++.$index, $imei['photo']);
            $sheet->setCellValue($alpha++.$index, $imei['note']);
            $sheet->setCellValue($alpha++.$index, $imei['staff_win'] ? ($imei['staff_id'] == $imei['staff_win'] ? '2' : ( $imei['staff_id_first'] == $imei['staff_win'] ? '1' : '' ) ) : '');
            $sheet->setCellValue($alpha++.$index, $imei['director_note']);

            $index++;
        }

        $filename = 'Duplicate Imei' ;
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;
    }

    public function getStatistic($params)
    {
        $db = Zend_Registry::get("db");
        $select_area_2 = $db->select()
            ->from(['d' => DATABASE_CENTER . '.duplicated_imei'], [
                'p.area_id',
                'quantity' => 'count(*)'
            ])
            ->joinLeft(['s' => DATABASE_CENTER . '.store'], 'd.store_id = s.id', [])
            ->joinLeft(['di' => DATABASE_CENTER . '.regional_market'], 's.district = di.id', [])
            ->joinLeft(['p' => DATABASE_CENTER . '.regional_market'], 'di.parent = p.id', [])
            
            ->where('d.date >= ?', $params['from_date'])
            ->where('d.date <= ?', $params['to_date'])
            ->where('d.asm_check = ?', 1)
            ->group('p.area_id');



        $select_area_1 = $db->select()
            ->from(['d' => DATABASE_CENTER . '.duplicated_imei'], [
                'area_id' => 'IFNULL(p.area_id, pi.area_id)',
                'quantity' => 'count(*)'
            ])
            ->joinLeft(['s' => DATABASE_CENTER . '.store'], 'd.store_id_first = s.id', [])
            ->joinLeft(['di' => DATABASE_CENTER . '.regional_market'], 's.district = di.id', [])
            ->joinLeft(['p' => DATABASE_CENTER . '.regional_market'], 'di.parent = p.id', [])

            ->joinLeft(['i' => WAREHOUSE_DB . '.imei'], 'd.imei = i.imei_sn', [])
            ->joinLeft(['dt' => WAREHOUSE_DB . '.distributor'], 'i.distributor_id = dt.id', [])
            ->joinLeft(['dti' => DATABASE_CENTER . '.regional_market'], 'dt.district = dti.id', [])
            ->joinLeft(['pi' => DATABASE_CENTER . '.regional_market'], 'dti.parent = pi.id', [])

            ->where('d.date >= ?', $params['from_date'])
            ->where('d.date <= ?', $params['to_date'])
            ->where('d.asm_check = ?', 1)
            ->group('area_id');

        // main select
        $select = $db->select()
            ->from(['a' => DATABASE_CENTER. '.area'], [
                'area_id' => 'a.id',
                'area_name' => 'a.name',
                'quantity_proposed' => 'a2.quantity',
                'quantity_be_proposed' => 'a1.quantity'
            ])
            ->joinLeft(['a1' => $select_area_1], 'a.id = a1.area_id', [])
            ->joinLeft(['a2' => $select_area_2], 'a.id = a2.area_id', []);

            if ($params['list_area']) {
                $select->where('a.id IN (?)', $params['list_area']);
            } else {
                $select->where('1 = 0');
            }

            $result = $db->fetchAll($select);

        $total = [];
        foreach ($result as $item) {
            $total['quantity_proposed'] += $item['quantity_proposed'];
            $total['quantity_be_proposed'] += $item['quantity_be_proposed'];
        }

        $data = [
          'detail' => $result,
          'total' => $total
        ];

        return $data;

    }

    public function checkImeiHasSolved($imei)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['d' => DATABASE_CENTER . '.duplicated_imei'], [
                'd.imei'
            ])
            ->where('d.imei = ?', $imei)
            ->where('d.staff_win > 0');

        $result = $db->fetchOne($select);

        return $result ? true : false;

    }

}                                                      
