<?php
class Application_Model_MiniGameRewardLog extends Zend_Db_Table_Abstract
{
	protected $_name = 'mini_game_reward_log';

	public function getLogRewardOfPlayer($params){
		$db = Zend_Registry::get("db");
        $select = $db->select();

       $arrCols = array(
            'p.id', 'p.reward'
        );
        $select->from(array('p' => $this->_name), $arrCols);
        $select->where('p.staff_id = ?', $params['staff_id']);
        $select->where('p.round = ?', $params['round']);
        $result = $db->fetchAll($select);
        //echo $select; exit;
        return $result;
	}

    public function getLogRewardOfItem($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id', 'p.reward','total'=>'COUNT(p.id)'
        );
        $select->from(array('p' => $this->_name), $arrCols);
        $select->where('p.round = ?', $params['round']);
        $select->group('p.reward');
        $result = $db->fetchAll($select);
        //echo $select; exit;
        return $result;
    }
}
