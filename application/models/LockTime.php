<?php
class Application_Model_LockTime extends Zend_Db_Table_Abstract
{
	protected $_name = 'lock_time';
        public function getLockTimeLastest(){
            $db = Zend_Registry::get('db');
            $select = $db->select()
                    ->from(array('l' => $this->_name), array('l.month','l.year'))
                    ->order('l.id DESC')
                    ->limit(1);
            $result = $db->fetchRow($select);
            if(empty($result)){
                return -1;
            }
            return $result;
        }
        
        public function setLockTime($month , $year){
            $db   = Zend_Registry::get('db');
            $stmt   = $db->prepare("CALL `PR_lock_time`(:month, :year)");
            $stmt->bindParam('month', $month , PDO::PARAM_INT);
            $stmt->bindParam('year', $year, PDO::PARAM_INT);
            $stmt->execute();
            $stmt->closeCursor(); 
            $stmt = $db = null;
            return 1;
        }
        
        public function setLockTimeSalary($month , $year){
            $db   = Zend_Registry::get('db');
            $time_tmp = strtotime($year . "-" . $month . "-" . "01");
            $from_date = date("Y-m-d", $time_tmp);
            $to_date = date("Y-m-t", $time_tmp);
            $stmt   = $db->prepare("CALL `PR_log_total_staff_time`(:p_from_date, :p_to_date,:p_month,:p_year,:p_locked)");
            $stmt->bindParam('p_from_date', $from_date , PDO::PARAM_STR);
            $stmt->bindParam('p_to_date', $to_date, PDO::PARAM_STR);
            $stmt->bindParam('p_month', $month, PDO::PARAM_INT);
            $stmt->bindParam('p_year', $year, PDO::PARAM_INT);
            $stmt->bindParam('p_locked', date("Y-m-d H:i:s"), PDO::PARAM_STR);
            $stmt->execute();
            $stmt->closeCursor(); 
            $stmt = $db = null;
            return 1;
        }
}