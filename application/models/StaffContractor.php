<?php
class Application_Model_StaffContractor extends Zend_Db_Table_Abstract
{
	protected $_name = 'staff_contractor';

	function stripUnicode($str){
	  if(!$str) return false;
	   $unicode = array(
	      'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',
	      'd'=>'đ',
	      'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
	      'i'=>'í|ì|ỉ|ĩ|ị',
	      'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
	      'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
	      'y'=>'ý|ỳ|ỷ|ỹ|ỵ',
	   );
	foreach($unicode as $nonUnicode=>$uni) $str = preg_replace("/($uni)/i",$nonUnicode,$str);
	return $str;
	}

	public function fetchPagination($page, $limit, &$total, $params){
        
        $db = Zend_Registry::get("db");
        $select = $db->select();
    //DISTINCT
        $arrCols = array(
            'id' => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS e.id'),
            'fullname' => "CONCAT(p.firstname,' ',p.lastname)",
            'email' => "p.email",
            'id_number'	=> "p.ID_number",
            'phone'		=> "p.phone_number",
            'name'=> "e.name",
            'shortname'  => "e.short_name",
            'address'  => "e.address",
            'code'		=> "e.code",
            'desc'		=> "e.desc",
            'cate_code' => 'GROUP_CONCAT(d.code)'
        );

        $select->from(array('e'=>DATABASE_TRADE.'.contructors'), $arrCols);
         $select->joinLeft(array('p' => DATABASE_CENTER.'.staff_contractor'),'p.id = e.user',array());

        $select->joinLeft(array('c' => DATABASE_TRADE.'.contractor_price'), 'c.contractor_id = e.id AND (c.to_date IS NULL OR c.to_date >= CURDATE())', array());
        $select->joinLeft(array('d' => DATABASE_TRADE.'.category'), 'd.id = c.category_id', array());
       if(!empty($params['name_search'])){
            $select->where('e.name LIKE ?', '%'.$params['name_search'].'%');
        }
        if(!empty($params['email'])){
            $select->where('p.email LIKE ?','%'.$params['email'].'%');
        }
         
         if(!empty($params['code'])){
            $select->where('e.code = ?',$params['code']);
        }
         if(!empty($params['address'])){
            $select->where('e.address LIKE ?','%'.$params['address'].'%');
        }
         if(!empty($params['description'])){
            $select->where('e.desc LIKE ?', '%'.$params['description'].'%');
        }
       //  //---T END SEARCH
         if (!empty($params['export'])){
            
         }else{
              $select->limitPage($page, $limit);
         }

         $select->group('e.id');
        $select->order('p.id DESC');

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
       
        return $result;
    }


    public function getDataEdit($id){
        
        $db = Zend_Registry::get("db");
        $select = $db->select();
    //DISTINCT
        $arrCols = array(
            'id' =>  "e.id",
            'fullname' => "CONCAT(p.firstname,' ',p.lastname)",
            'email' => "p.email",
            'id_number'	=> "p.ID_number",
            'phone'		=> "p.phone_number",
            'name'=> "e.name",
            'shortname'  => "e.short_name",
            'address'  => "e.address",
            'code'		=> "e.code",
            'desc'		=> "e.desc",
            'id_user'   => "e.user"
        );

        $select->from(array('e'=>DATABASE_TRADE.'.contructors'), $arrCols);
        $select->joinLeft(array('p' => 'staff_contractor'),'p.id = e.user',array());
        $select->where('e.id = ?', $id);
        $result = $db->fetchAll($select);
        return $result;
    }


}