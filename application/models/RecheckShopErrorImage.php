<?php

class Application_Model_RecheckShopErrorImage extends Zend_Db_Table_Abstract
{
    protected $_name = 'recheck_shop_error_image';
    protected $_schema = DATABASE_TRADE;

    public function getError($recheckId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['e' => DATABASE_TRADE.'.recheck_shop_error_image'], [
                         'e.recheck_shop_id',
                         'e.note',
                         'e.note',
                         'error_name' => 'r.name'
                     ])
        ->joinLeft(['r' => DATABASE_TRADE.'.error_image'], 'e.error_image_id = r.id', [])
        ->where('e.recheck_shop_id = ?', $recheckId);

        $result = $db->fetchAll($select);

        return $result;
    }
}    