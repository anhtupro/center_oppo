<?php
class Application_Model_StaffLogDetail extends Zend_Db_Table_Abstract
{
    protected $_name = 'staff_log_detail';

    /**
     * Lấy danh sách các thay đổi của một staff theo loại thông tin và
     *     khoảng thời gian
     * @param  int $staff_id - id của staff cần check
     * @param  int/StaffInfoType enum $type     - loại thông tin
     * @param  int/UNIX timestamp $from     - ngày bắt đầu check
     * @param  int/UNIX timestamp $to       - ngày kết thúc check
     * @return query result/cursor           - con trỏ trỏ về kết quả query/
     *                                      danh sách các thay đổi của staff
     */
    function get_history($staff_id, $type, $from, $to)
    {
        $where = array();
        $where[] = $this->getAdapter()->quoteInto('object = ?', $staff_id);
        $where[] = " ( " .
                $this->getAdapter()->quoteInto('to_date IS NULL AND ? >= from_date', $to) .
            " ) OR ( " .
                $this->getAdapter()->quoteInto('? >= from_date AND ? <= to_date', $from) .
            " ) OR ( " .
                $this->getAdapter()->quoteInto('? >= from_date AND ? <= to_date', $to) .
            " ) OR ( " .
                $this->getAdapter()->quoteInto('? <= from_date', $from) 
                . " AND ". $this->getAdapter()->quoteInto('? >= to_date', $to) .
            " )"
            ;

        $where[] = $this->getAdapter()->quoteInto('info_type = ?', $type);

        return $this->fetchAll($where);
    }
}