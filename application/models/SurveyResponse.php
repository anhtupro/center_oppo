<?php
class Application_Model_SurveyResponse extends Zend_Db_Table_Abstract
{
    protected $_name = 'survey_response';

    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.response', 'p.created_at'))
            ->join(array('s' => 'staff'), 's.id=p.staff_id', array('s.firstname', 's.lastname', 's.email', 's.title'));

        $select->order('p.created_at DESC');

        if ($limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);

        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
}
