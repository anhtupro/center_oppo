<?php

class Application_Model_InsertDifferentTgdd extends Zend_Db_Table_Abstract
{
    protected $_name = 'insert_different_tgdd';

    public function getInvalidImei()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['u' => 'insert_different_tgdd'], [
                'u.imei'
            ])
            ->joinLeft(['i' => WAREHOUSE_DB . '.imei'], 'u.imei = i.imei_sn', [])
            ->where('i.imei_sn IS NULL');

        $result = $db->fetchCol($select);

        return $result;

    }

    public function getInvalidPartner()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['u' => 'insert_different_tgdd'], [
                'u.partner_id'
            ])
            ->joinLeft(['d' => WAREHOUSE_DB . '.distributor'], 'u.partner_id = d.partner_id AND (d.del = 0 OR d.del IS NULL)', [])
            ->where('u.partner_id IS NOT NULL')
            ->where('d.partner_id IS NULL');

        $result = $db->fetchCol($select);

        return $result;
    }

    public function getInvalidDistributor()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['u' => 'insert_different_tgdd'], [
                'u.partner_id'
            ])
            ->joinLeft(['d' => WAREHOUSE_DB . '.distributor'], 'u.partner_id = d.partner_id AND (d.del = 0 OR d.del IS NULL)', [])
            ->joinLeft(['s' => 'store'], 'd.id = s.d_id AND (s.del IS NULL OR s.del = 0)', [])
            ->where('u.partner_id IS NOT NULL')
            ->where('s.id IS NULL')
            ->group('u.partner_id');

        $result = $db->fetchCol($select);

        return $result;
    }

    public function updateStoreId()
    {
        $db = Zend_Registry::get('db');
        $sql = "
			UPDATE insert_different_tgdd u 
            JOIN warehouse.distributor d ON u.partner_id = d.partner_id AND (d.del = 0 OR d.del IS NULL)
            JOIN store s ON d.id = s.d_id AND (s.del IS NULL OR s.del = 0)
            SET u.store_id = s.id ";

        $db->query($sql);
        $db = null;
    }

    public function updateGood()
    {
        $db = Zend_Registry::get('db');
        $sql = "
			UPDATE insert_different_tgdd u 
            JOIN warehouse.imei i ON u.imei = i.imei_sn
            SET u.good_id = i.good_id, u.color_id = i.good_color ";

        $db->query($sql);
        $db = null;
    }

    public function getAll()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['i' => 'insert_different_tgdd'], [
                'i.*'
            ])
        ->where('i.imei is NOT NULL')
        ->where('i.store_id is NOT NULL')
        ->where('i.good_id is NOT NULL')
        ->where('i.timing_date is NOT NULL');

        $result = $db->fetchAll($select);

        return $result;

    }

    public function getImeiHasTiming()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['i' => 'insert_different_tgdd'], [
                'i.imei'
            ])
            ->join(['ts' => 'timing_sale'], 'i.imei = ts.imei', []);

         $result = $db->fetchCol($select);

         return $result;
    }
    
}