<?php

class Application_Model_CheckShopEntireDisplayProductPromotion extends Zend_Db_Table_Abstract
{
    protected $_name = 'check_shop_entire_display_product_promotion';
    protected $_schema = DATABASE_TRADE;

    public function get($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['p' => DATABASE_TRADE.'.check_shop_entire_display_product'], [
                'r.product_detail_id',
                'promotion_name' => 'c.name',
                'r.promotion_value'
            ])
            ->joinLeft(['d' => DATABASE_TRADE.'.check_shop_entire_display_product_detail'], 'p.id = d.display_product_id', [])
            ->joinLeft(['r' => DATABASE_TRADE.'.check_shop_entire_display_product_promotion'], 'r.product_detail_id = d.id', [])
            ->joinLeft(['c' => DATABASE_TRADE.'.promotion'], 'r.promotion_id = c.id', [])
            ->where('p.check_shop_id = ?', $params['check_shop_id']);
        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['product_detail_id']] [] = $element;
        }

        return $list;
    }
}