<?php
class Application_Model_HrCourseSurveyDetails extends Zend_Db_Table_Abstract
{
    protected $_name = 'hr_course_survey_details';

    public function getAnswerData($params, $type){
        $db = Zend_Registry::get("db");
        $select = $db->select();
        $select->from(array('p' => $this->_name), array('p.*'));
        $select->where('p.staff_id = ?', $params['staff_id']);
        $select->where('p.course_id = ?', $params['course_id']);
        if($type == 1){
            $select->where('p.answer_check IS NOT NULL');
        }else{
            $select->where('p.answer_content IS NOT NULL');
        }
        $result = $db->fetchAll($select);
        return $result;
    }
}