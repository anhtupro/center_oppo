<?php

class Application_Model_AirQuotation extends Zend_Db_Table_Abstract
{
    protected $_name = 'air_quotation';
    protected $_schema = DATABASE_TRADE;

    public function getAirDetailsQuotation($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $select->from(array('r' => DATABASE_TRADE.'.app_air'), ['rd.id', 'rq.*'])
            ->joinLeft(array('rd' => DATABASE_TRADE.'.air_details'), 'r.id = rd.air_id', [])
            ->joinLeft(array('rq' => DATABASE_TRADE.'.air_quotation'), 'rd.id = rq.air_details_id', [])
            ->where('r.id = ?', $params['air_id']);

        if (isset($params['price_type']) && $params['price_type'] == 0) {
            $select->where('rq.status = ?', $params['price_type']);
        }
        if (isset($params['price_type']) && $params['price_type'] != 0) {
            $select->where('rq.status = ?', $params['price_type']);
        }

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $air_quotation [$element ['air_details_id'] ] [] = $element;
            $sum_total_price_quotation [$element ['air_details_id'] ] += $element['total_price'] + $element['total_price'] * VAT;
        }

        $result = [
            'air_quotation' => $air_quotation,
            'sum_total_price_quotation' =>  $sum_total_price_quotation
        ];

        return $result;
    }

}