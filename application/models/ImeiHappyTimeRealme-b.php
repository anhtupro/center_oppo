<?php 
    class Application_Model_ImeiHappyTimeRealme extends Zend_Db_Table_Abstract
    {
        public function insertValue($value)
        {
            $db = Zend_Registry::get('db');

            $sql = "INSERT INTO `imei_happy_time` (`imei_sn`, `status`)
                VALUES " . $value
                . " ON DUPLICATE KEY UPDATE `status` = VALUES(`status`), `created_at` = NOW() ";
            $db->query($sql);
            $db = null;
        }

        public function checkImei($imei)
        {
            $db = Zend_Registry::get('db');
            $stmt = $db->prepare('CALL `check_imei_happy_time_realme`(:imei)');

            $stmt->bindParam('imei', $imei, PDO::PARAM_STR);
            $stmt->execute();

            $data = $stmt->fetch();
            
            $stmt->closeCursor();
            $stmt = $db = null;
            return $data['result'];
        }

        public function updateByImei($params = array())
        {
            $db = Zend_Registry::get('db');

            $sql = "UPDATE `imei_happy_time` 
                    SET `created_at` = NOW(), `status` = " . $params['status']
                    ." WHERE `imei_sn` = " . $params['imei_sn'];
            $db->query($sql);
            $db = null;
        }

        public function updateValue($params = array())
        {
            $db = Zend_Registry::get('db');

            $sql = "UPDATE `imei_happy_time` 
                    SET `created_at` = NOW(), `status` = " . $params['status']
                    ." WHERE `id` = " . $params['id'];
            $db->query($sql);
            $db = null;
        }

        public function fetchPagination($params = array())
        {
            $db = Zend_Registry::get('db');

            $params['imei_sn'] = empty($params['imei_sn'])?null:$params['imei_sn'];
            $params['created_at'] = empty($params['created_at'])?null:$params['created_at'];

            $stmt = $db->prepare('call `get_imei_happy_time`(:imei_sn , :created_at, :limit, :offset) ');
            $stmt->bindParam('imei_sn', $params['imei_sn'], PDO::PARAM_STR);
            $stmt->bindParam('created_at', $params['created_at'], PDO::PARAM_STR);
            $stmt->bindParam('limit', $params['limit'], PDO::PARAM_INT);
            $stmt->bindParam('offset', $params['offset'], PDO::PARAM_INT);
            $stmt->execute();

            $data = array();
            $data['data'] = $stmt->fetchAll();
            $stmt->closeCursor();
            $data['total'] = $db->query("SELECT FOUND_ROWS()")->fetchColumn();

            $stmt = $db = null;
            return $data;
        }
    }
