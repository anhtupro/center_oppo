<?php
class Application_Model_AsmProposal extends Zend_Db_Table_Abstract
{
	protected $_name = 'asm_proposal';

	public function get_all(){
		$db = Zend_Registry::get('db');
		$select = $db->select()
				->from(array('p'=>$this->_name),array('p.*'));
		$result = $db->fetchPairs($select);
		return $result;
	}
}