<?php
class Application_Model_DestructionDetails extends Zend_Db_Table_Abstract
{
    protected $_name = 'destruction_details';
    protected $_schema = DATABASE_TRADE;
    
    public function getDestructionDetails($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.destruction_id", 
            "p.category_id", 
            "p.quantity", 
            "category_name" => "c.name",
            "p.note",
            'c.has_type',
            'c.has_width',
            'p.destruction_type',
            'a.height',
            'a.width',
            'material_name' => 'm.name',
            'app_checkshop_detail_child_id' => 'a.id',
            'category_type_name' => 'w.name',
            'category_type_child_name' => 'y.name'
        );

        $select->from(array('p' => DATABASE_TRADE.'.destruction_details'), $arrCols);
        $select->joinLeft(array('c' => DATABASE_TRADE.'.category'), 'c.id = p.category_id', array());
        $select->joinLeft(array('a' => DATABASE_TRADE.'.app_checkshop_detail_child'), 'p.app_checkshop_detail_child_id = a.id', array());
        $select->joinLeft(array('m' => DATABASE_TRADE.'.category_material'), 'a.material_id = m.id', array());
        $select->joinLeft(['w' => DATABASE_TRADE.'.category_inventory_type'], 'a.category_type = w.id', []);
        $select->joinLeft(['y' => DATABASE_TRADE.'.category_inventory_type'], 'a.category_type_child = y.id', []);

        $select->where('p.destruction_id = ?', $params['id']);
        $select->group('p.id');

        if ($params['version'] && $params['version'] == 2) {
            if ($params['area_id']) {
                $select->joinLeft(['i' => DATABASE_TRADE.'.inventory_area_imei'], 'p.inventory_area_detail_id = i.id', ['i.height', 'i.width']);
                $select->joinLeft(['t' => DATABASE_TRADE.'.category_inventory_type'], 'i.category_type = t.id', [
                    'category_type_name' => 't.name',
                    'category_type_id' => 't.id'
                ]);
            }

            if ($params['store_id']) {
                $select->joinLeft(['o' => DATABASE_TRADE.'.app_checkshop_detail_child'], 'p.app_checkshop_detail_child_id = o.id', ['o.height', 'o.width']);
                $select->joinLeft(['ct' => DATABASE_TRADE.'.category_inventory_type'], 'o.category_type = ct.id', [
                    'category_type_name' => 'ct.name',
                    'category_type_id' => 'ct.id'
                ]);
            }
        }
        
        $result = $db->fetchAll($select);

        return $result;
    }


    public function getDestructionDetailsTp($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "t.tp_id"
        );

        $select->from(array('p' => DATABASE_TRADE.'.destruction_details'), $arrCols);
        $select->joinLeft(array('t' => DATABASE_TRADE.'.destruction_details_tp'), 't.destruction_details_id = p.id', array());
        $select->where('p.destruction_id = ?', $params['id']);
        $result = $db->fetchAll($select);
        return $result;
    }
    

    public function getContract($params){
         $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "store"    => "s.name",
            "category"      => "c.name",
            "p.id", 
            "p.destruction_id", 
            "p.category_id", 
            "final_price"      => "p.price_final",
            "p.quantity",
            "p.contract_id"
        );

        $select->from(array('p' => DATABASE_TRADE.'.destruction_details'), $arrCols);
         $select->joinLeft(array('a' => DATABASE_TRADE.'.destruction'), 'a.id = p.destruction_id', array());
        $select->joinLeft(array('c' => DATABASE_TRADE.'.category'), 'c.id = p.category_id', array());
         $select->joinLeft(array('s' => 'store'), 's.id = a.store_id', array());
        
        $select->where('p.contructors_id = ?', $params['contractor_id']);
        $select->where('a.id IS NOT NULL');
        $result = $db->fetchAll($select);
        return $result;
    }
}