<?php 
class Application_Model_SalaryLog extends Zend_Db_Table_Abstract
{
	protected $_name = 'salary_log';

	public function getSalaryLogTitle($title,$from_date,$province_id){
		//tạm thời sử dụng, sau này tạo bảng map
		$type = 1;
		if($title == PB_SALES_TITLE){
			$type = 3;
		}

		$db = Zend_Registry::get('db');
		$select_salary = $db->select()
            ->from(array('a'=>'salary_log'),array('a.*'))
            ->where('province_id = ?',$province_id)
            ->where('from_date <= ?',$from_date)
            ->where('type = ?',$type)
            ->where('to_date IS NULL OR to_date >= ?',$from_date);
        $salary = $db->fetchRow($select_salary);
        return $salary;
	}
}
?>