<?php
class Application_Model_AppContractorOut extends Zend_Db_Table_Abstract
{
    protected $_name = 'app_contractor_out';
    protected $_schema = DATABASE_TRADE;
    
    function getTranferOut($params)
    {
    	if( empty($params['contractor_out_id']) AND empty($params['contractor_out_id']) ){
            return false;
        }

        $db = Zend_Registry::get('db');
        
        $arrCols = array(
            'contractor_quantity_id'  => 'p.contractor_quantity_id',
            'area_id'      		  	  => "p.area_id",
            'quantity'      		  => "p.quantity",
            'category_id'   		  => "c.category_id",
        );

        $select = $db->select()
                ->from(array('p'=> DATABASE_TRADE.'.app_contractor_out'), $arrCols);
        $select->joinLeft(array('c'=> DATABASE_TRADE.'.app_contractor_quantity'), 'c.id = p.contractor_quantity_id',array());
        
        $select->where('p.id IN (?)', $params['contractor_out_id']);

        $result = $db->fetchAll($select);

        return $result;
    }

    function getOrderDetailsTranfer($params)
    {
    	if( empty($params['area']) AND empty($params['area']) ){
            return false;
        }

        $db = Zend_Registry::get('db');
        
        $arrCols = array(
            'id'  			  => 'p.id',
            'area_id'      	  => "p.area_id",
            'category_id'     => "p.category_id",
            'store_id'   	  => "p.store_id",
            'status'   		  => "p.status",
            'status'   		  => "p.status",
            'area_id'		  => 'IFNULL(c.area_id, r.area_id)'
        );

        $select = $db->select()
                ->from(array('p'=> DATABASE_TRADE.'.app_order_details'), $arrCols);
        $select->joinLeft(array('s'=> 'store'), 's.id = p.store_id',array());
        $select->joinLeft(array('r'=> 'regional_market'), 'r.id = s.regional_market',array());
        $select->joinLeft(array('c'=> 'store_area_chanel'), 'c.store_id = s.store_id',array());	
        
        $select->where('IFNULL(c.area_id, r.area_id) IN (?)', $params['area']);

        $select->where('p.category_id IN (?)', $params['cat']);

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getContractorOut($params){
        $db     = Zend_Registry::get("db");
        $select = $db->select();
            
        $arrCols = array(
            'id'                => 'p.id',
            'campaign_id'       => 'c.campaign_id', 
            'category_id'       => 'c.category_id', 
            'contractor_id'     => 'c.contractor_id',
            'short_name'        => 't.short_name',
            'area_name'         => 'a.name',
            'category_name'     => 'cat.name',
            'quantity'          => 'c.quantity',
            'quantity_out'      => 'COUNT(i.out_date)',
            'quantity_in'       => 'COUNT(i.in_area_id)',
            'transporter_name'  => 'tran.name',
            'transporter_price'  => 'p.price'
        );

        $select->from(array('p'=> DATABASE_TRADE.'.app_contractor_out'), $arrCols);
        $select->joinLeft(array('c' => DATABASE_TRADE.'.app_contractor_quantity'), 'c.id = p.contractor_quantity_id', array());
        $select->joinLeft(array('i' => DATABASE_TRADE.'.app_contractor_in'), 'i.contractor_out_id = p.id', array());
        $select->joinLeft(array('t' => DATABASE_TRADE.'.contructors'), 't.id = c.contractor_id', array());
        $select->joinLeft(array('tran' => DATABASE_TRADE.'.app_transporter'), 'tran.id = p.transporter_id', array());
        $select->joinLeft(array('a' => 'area'), 'a.id = p.area_id', array());
        $select->joinLeft(array('cat' => DATABASE_TRADE.'.category'), 'cat.id = c.category_id ', array());

        if(!empty($params['id'])){
            $select->where('p.id = ?', $params['id']);
        }

        $select->group('p.id');

        $result  = $db->fetchAll($select);

        return $result;
    }

}