<?php

class Application_Model_RewardWarningType extends Zend_Db_Table_Abstract
{
    protected $_name = 'reward_warning_type';

    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if ($result === false) {
            $where = [];
            $where[] = $this->getAdapter()->quoteInto('type = ?', 1);
			$where[] = $this->getAdapter()->quoteInto('del IS NULL OR del = ?', 0);
            $data = $this->fetchAll($where, 'title');

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->title;
                }
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }

    function get_cache_reward(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_reward_cache');

        if ($result === false) {
            $where = [];
            $where[] = $this->getAdapter()->quoteInto('type = ?', 2);
            $where[] = $this->getAdapter()->quoteInto('del IS NULL OR del = ?', 0);
            $data = $this->fetchAll($where, 'title');

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->title;
                }
            }
            $cache->save($result, $this->_name.'_reward_cache', array(), null);
        }
        return $result;
    }
}