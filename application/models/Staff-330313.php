<?php
class Application_Model_Staff extends Zend_Db_Table_Abstract
{
    protected $_name = 'staff';

    /**
     * Một số config cục bộ
     * asm : các GROUP ID được xem dashboard vùng, ví dụ gồm ASM hay là gồm ASM và Sales Admin...3
     * -- viết tiếp nếu có config thêm
     * @var array
     */
    private $config = array(
        'asm' => array(
            ASM_ID,
            ASMSTANDBY_ID,
            SALES_ADMIN_ID,
            ),
        'ttl' => 60
        );
        
    function fetchPaginationScores($page, $limit, &$total, $params){
        $db         = Zend_Registry::get('db');
        $sql_params = array(
            isset($params['pass']) ? $params['pass'] : null,
            isset($params['fail']) ? $params['fail'] : null,
            isset($params['result']) ? $params['result'] : null,
            null,
            isset($params['area_id']) ? $params['area_id'] : null,
            null,
            isset($params['name']) ? $params['name'] : null,
            isset($params['code']) ? $params['code'] : null,
            null,
            $limit,
            $page
        );

        $sql = 'CALL sp_assessment(?,?,?,?,?,?,?,?,?,?,?,@total)';
        $stmt = $db->query($sql,$sql_params);
        $list = $stmt->fetchAll();
        $stmt->closeCursor();
        $total = $db->fetchOne('select @total');
        return $list;

    }

    function fetchPaginationScoresById($id){
        $db         = Zend_Registry::get('db');
        $sql_params = array(
            null,
            null,
            null,
            $id,
            null,
            null,
            null,
            null,
            null,
            null,
            null
        );

        $sql = 'CALL sp_assessment(?,?,?,?,?,?,?,?,?,?,?,@total)';
        $stmt = $db->query($sql,$sql_params);
        $list = $stmt->fetchAll();
        $stmt->closeCursor();
        $total = $db->fetchOne('select @total');
        return $list;

    }
        
    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        if ($limit) {
            $select = $db->select()
                ->from(array('p' => $this->_name),
                    array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'), 'p.*'));
        } else {
            $select = $db->select()
                ->from(array('p' => $this->_name),
                    array(new Zend_Db_Expr('DISTINCT p.id'), 'p.*'));
        }

        $select->joinLeft(array('e'=>'staff_education'),'p.id = e.staff_id AND e.default_level = 1',array('d_level'=>'e.level','d_certificate'=>'field_of_study'));

        $select->joinLeft(array('province'),'province.id = id_place_province',array('id_place_province_name'=>'province.name'));

        if (isset($params['name']) and $params['name']) {
            $select->where('( CONCAT(p.firstname, " ",p.lastname) LIKE ?', '%'.$params['name'].'%');
            $select->orwhere('p.code LIKE ? ', '%'.$params['name'].'%');
            $select->orwhere('p.email LIKE ? )', '%'.$params['name'].'%');

        }

        if (isset($params['is_print']) and $params['is_print']){
            $select->where('p.is_print = ?', 1);
        }

        if (isset($params['company_id']) and $params['company_id']) {
            $select->where('p.company_id = ?', $params['company_id']);
        }

        if (isset($params['team']) and $params['team']) {
            if (is_array($params['team']) && count($params['team']) > 0) {
                $select->where('p.team IN (?)', $params['team']);
            } else {
                $select->where('1=0', 1);
            }
        }


        if (isset($params['contract_term']) and $params['contract_term']) {
            if (is_array($params['contract_term']) && count($params['contract_term']) > 0) {
                $select->where('p.contract_term IN (?)', $params['contract_term']);
            } else {
                $select->where('1=0', 1);
            }
        }

        if (isset($params['department']) and $params['department']) {
            if (is_array($params['department']) && count($params['department']) > 0) {
                $select->where('p.department IN (?)', $params['department']);
            } else {
                $select->where('1=0', 1);
            }
        }

        if(isset($params['salary_sales']) and $params['salary_sales'])
        {
            $select->where('p.title IN (?)', array(PGPB_TITLE, PGPB_2_TITLE,SALES_TITLE,SALES_ACCESSORIES_TITLE,SALES_LEADER_TITLE ,SALES_ACCESSORIES_LEADER_TITLE));
        }

        if (isset($params['is_officer']) and intval($params['is_officer']) > 0){
            if ($params['is_officer'] == 1)
                $select->where('p.is_officer = ? or p.team = 11 or p.title in (274,179,181)', 1);
            elseif ($params['is_officer'] == 2)
                $select->where('p.is_officer ? or p.team = 11 or p.title in (274,179,181)', 0);
                //$select->orWhere('p.team = ?', WARRANTY_CENTER);
        }

        if (isset($params['email']) and $params['email']) {
            $params['email'] = preg_replace('/'.EMAIL_SUFFIX.'/', '', $params['email']);
            $select->where('p.email LIKE ?', $params['email'].EMAIL_SUFFIX);
        }

        if (isset($params['date']) and $params['date'])
        {
            $date = explode(',', $params['date']);
            $select->where("DAY(STR_TO_DATE(p.dob, '%d/%m/%Y')) IN (?)", $date);
        }

        if (isset($params['month']) and $params['month'])
        {
            $date = explode(',', $params['month']);
            $select->where("MONTH(STR_TO_DATE(p.dob, '%d/%m/%Y')) IN (?)", $date);
        }

        if(isset($params['month_off']) and $params['month_off'])
        {
             $select
            ->joinLeft(array('mo' => 'off_date_add'), 'p.id = mo.staff_id ' , array('staff_id' => 'mo.staff_id'));
            $select->where('MONTH(mo.date) <> ?' , $params['month_off'] )->orWhere('MONTH(mo.date) is null' , null);
        }

        if (isset($params['year']) and $params['year'])
        {
            $date = explode(',', $params['year']);
            $select->where("YEAR(STR_TO_DATE(p.dob, '%d/%m/%Y')) IN (?)", $date);
        }

        if(isset($params['ready_print']) and $params['ready_print'])
        {

            if($params['ready_print'] == 1)
           {
             $select->where("p.address <> '' and p.birth_place <> ''  and p.ID_number <> '' and p.ID_place <> '' and p.ID_date <> '' and p.title <> '' and p.company_id <> 0");
           }
            elseif($params['ready_print'] == 2)
            {
                 $select->where("address = '' or birth_place = ''  or ID_number = '' or ID_place = '' or ID_date = '' or title = '' or company_id = 0");
            }

            elseif($params['ready_print'] == 3)
            {
                 $select->where("p.ID_number <> '' and p.ID_place <> '' and p.ID_date <> '' and p.title <> '' and p.company_id <> 0 and EXISTS (select * from staff_address sa2 where p.id = sa2.staff_id and sa2.address_type = 4 and sa2.address is not null)");
            }

            elseif($params['ready_print'] == 4)
            {
                 $select->where("p.ID_number = '' or p.ID_place = '' or p.ID_date = '' or p.title = '' or p.company_id = 0 or  and EXISTS (select * from staff_address sa2 where p.id = sa2.staff_id and sa2.address_type = 4 and sa2.address is not null)");
            }

        }

        if (isset($params['title']) and $params['title']) {
            
            if (is_array($params['title']) && count($params['title']) > 0) {
                $select->where('p.title IN (?)', $params['title']);
            } else {
                $select->where('1=0', 1);
            }
        }
        
        $select->joinLeft(array('st' => 'staff_temp'),
            '
                p.id = st.staff_id
            ',
            array('staff_temp_id' => 'st.id', 'staff_temp_is_approved' => 'st.is_approved'));

        if (isset($params['need_approve']) and $params['need_approve']){
            $select->where('st.is_approved = ?', 0);
        }

        if ( ( isset($params['off']) and intval($params['off']) > 0 ) || ( isset($params['sname']) and $params['sname'] == 1 ) )
            $select->where('p.off_date is '
                    . ( $params['off'] == 2 ? 'not' : '')
                    .' null');

        // list
        if (isset($params['exp']) and $params['exp'])
            $select->where('p.contract_expired_at > NOW()');

        //joined at
        if(isset($params['joined_at']) and $params['joined_at'])
        {
            $arrFrom = explode('/',$params['joined_at']);
            $params['joined_at'] = $arrFrom[2].'-'.$arrFrom[1].'-'.$arrFrom[0];
            $select->where('p.joined_at = ?' , $params['joined_at']);
        }
        // id_number
        if(isset($params['indentity']) and $params['indentity'])
        {
            $select->where('p.ID_number = ?', $params['indentity']);
        }

        if(isset($params['off_type']) and $params['off_type'])
        {
            $select->where('p.off_type = ?', $params['off_type']);
        }

        //off_date
        if(isset($params['off_date']) and $params['off_date'])
        {
            $arrFrom = explode('/',$params['off_date']);
            $params['off_date'] = $arrFrom[2].'-'.$arrFrom[1].'-'.$arrFrom[0];
            $select->where('p.off_date = ?' , $params['off_date']);
        }

        if (isset($params['sales_team_id']) and $params['sales_team_id'])
            $select->where('p.sales_team_id = ?', $params['sales_team_id']);

        if (isset($params['is_leader']) and $params['is_leader'])
            $select->where('p.is_leader = ?', $params['is_leader']);

        if (isset($params['distribution_id']) and $params['distribution_id'])
            $select->where('p.distribution_id = ?', $params['distribution_id']);

        if (isset($params['note']) and $params['note'])
            $select->where('p.note LIKE ?', '%'.$params['note'].'%');

        if (isset($params['code']) and $params['code'])
            $select->where('p.code LIKE ?', '%'.$params['code'].'%');

        if (isset($params['has_photo']) and $params['has_photo'])
            $select->where('p.photo IS NOT NULL AND p.photo <> \'\'', 1);


        if (isset($params['ood']) and $params['ood']){
            $select->where(' DATEDIFF(p.contract_expired_at,NOW()) <= ?', 30);
            /*$select->where(' DATEDIFF(NOW(),p.contract_expired_at) >= ?', 0);*/
        }

        if (isset($params['no_print']) and $params['no_print']){
            $select->where('p.print_time = 0 OR p.contract_signed_at is NULL and p.contract_expired_at is NULL', '');
        }

        if (isset($params['tags']) and $params['tags']){
            $select->join(array('ta_ob' => 'tag_object'),
                '
                    p.id = ta_ob.object_id
                    AND ta_ob.type = '.TAG_STAFF.'
                ',
                array());
            $select->join(array('ta' => 'tag'),
                '
                    ta.id = ta_ob.tag_id
                ',
                array());

            $select->where('ta.name IN (?)', $params['tags']);
        }

        if (isset($params['area_id']) and $params['area_id'] && !(isset($params['regional_market']) and $params['regional_market'])){
            $QRegionalMarket = new Application_Model_RegionalMarket();

            if (is_array($params['area_id']) && count($params['area_id']) > 0) {
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id IN (?)', $params['area_id']);
            } else {
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $params['area_id']);
            }

            $regional_markets = $QRegionalMarket->fetchAll($where);
            $tem = array();

            foreach ($regional_markets as $regional_market)
                $tem[] = $regional_market->id;

            if (is_array($tem) && count($tem) > 0)
                $select->where('p.regional_market IN (?)', $tem);
            else
                $select->where('1=0', 1);

        } elseif (isset($params['regional_market']) and $params['regional_market']) {
            if (is_array($params['regional_market']) && count($params['regional_market']) > 0) {
                $select->where('p.regional_market IN (?)', $params['regional_market']);
            } else {
                $select->where('1=0', 1);
            }
        }

        // dành để lọc staff cho các asm, admin... xem; chỉ hiện thị staff thuộc khu vực được gán
        if (isset($params['list_province_ids']) && $params['list_province_ids']) {
            if (is_array($params['list_province_ids']) && count($params['list_province_ids'])) {
                $select->where('p.regional_market IN (?)', $params['list_province_ids']);
            } elseif (is_numeric($params['list_province_ids']) && intval($params['list_province_ids']) > 0) {
                $select->where('p.regional_market = ?', intval($params['list_province_ids']));
            }
        }

        if(isset($params['area_trainer_right']) and $params['area_trainer_right'])
        {
            $QRegionalMarket = new Application_Model_RegionalMarket();

            if (is_array($params['area_trainer_right']) && count($params['area_trainer_right']) > 0) {
                $whereRegionalMarketRight = $QRegionalMarket->getAdapter()->quoteInto('area_id IN (?)', $params['area_trainer_right']);
            } else {
                $whereRegionalMarketRight  = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $params['area_trainer_right']);
            }

            $regional_markets_rights = $QRegionalMarket->fetchAll($whereRegionalMarketRight);
            $tem = array();

            foreach ($regional_markets_rights as $regional_markets_right)
                $tem[] = $regional_markets_right->id;

            if (is_array($tem) && count($tem) > 0)
                $select->where('p.regional_market IN (?)', $tem);
            else
                $select->where('1=0', 1);
        }

        if (isset($params['s_assign']) && $params['s_assign']) {
            $QDeparment = new Application_Model_Department();
            $where = $QDeparment->getAdapter()->quoteInto('name LIKE ?', 'KINH DOANH');
            $department_id = $QDeparment->fetchAll($where)->current()->id;
            $select->where('p.department = ?', $department_id);
        }

        $order_str = '';

        if (isset($params['sort']) and $params['sort']) {
            $collate = ' ';
            $desc = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';

            if ($params['sort'] == 'name'){
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= ' CONCAT(p.firstname, " ",p.lastname) '.$collate . $desc;
            } elseif ( /*$params['sort'] == 'title' || */$params['sort'] == 'department' || $params['sort'] == 'team' ) {
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= $collate . $desc;
            } else {
                $order_str = 'p.`'.$params['sort'] . '` ' . $collate . $desc;
            }

            $select->order(new Zend_Db_Expr($order_str));
        }

        

        $select->group('p.id');

        if ($limit)
            $select->limitPage($page, $limit);

        

        if (isset($params['export']) && $params['export'])
            return $select->__toString();

        $result = $db->fetchAll($select);

        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    function fetchForSearch($provinces)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('s' => $this->_name), array('s.id', 's.code', 's.firstname', 's.lastname', 's.email'))
            ->where('s.regional_market IN (?)', $provinces)
            ->where('s.title IN (?)', array(PGPB_TITLE, PGPB_2_TITLE, SALES_TITLE, LEADER_TITLE, SALES_TRAINEE_TITLE))
            ->order('s.lastname');
        return $db->query($select->__toString());
    }

    function fetchLeaderPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        if ($limit) {
            $select = $db->select()
                ->from(array('p' => $this->_name),
                    array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'), 'p.firstname', 'p.lastname', 'p.email', 'p.phone_number', 'p.regional_market'));
        } else {
            $select = $db->select()
                ->from(array('p' => $this->_name),
                    array(new Zend_Db_Expr('DISTINCT p.id'), 'p.firstname', 'p.lastname', 'p.email', 'p.phone_number', 'p.regional_market'));
        }

        $select
            ->distinct()
            ->joinLeft(array('sl' => 'store_leader'), 'sl.staff_id=p.id AND sl.status=1', array('current_store' => 'COUNT(DISTINCT sl.store_id)'))
            ->joinLeft(array('sll' => 'store_leader'), 'sll.staff_id=p.id AND sll.status=0', array('pending_store' =>'COUNT(DISTINCT sll.store_id)'))
            ->group('p.id');

        $select->where('p.group_id = ? OR p.id = 7192', LEADER_ID);

        if (isset($params['name']) and $params['name']) {
            $select->where('CONCAT(p.firstname, " ",p.lastname) LIKE ?', '%'.$params['name'].'%');
        }

        if (isset($params['email']) and $params['email']) {
            $params['email'] = preg_replace('/'.EMAIL_SUFFIX.'/', '', $params['email']);
            $select->where('p.email LIKE ?', $params['email'].EMAIL_SUFFIX);
        }

        if (isset($params['area_id']) and $params['area_id'] && !(isset($params['province']) and $params['province'])){
            $QRegionalMarket = new Application_Model_RegionalMarket();
            $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $params['area_id']);

            $regional_markets = $QRegionalMarket->fetchAll($where);
            $tem = array();

            foreach ($regional_markets as $regional_market)
                $tem[] = $regional_market->id;

            if (is_array($tem) && count($tem) > 0)
                $select->where('p.regional_market IN (?)', $tem);
            else
                $select->where('1=0', 1);
        } elseif (isset($params['province']) and $params['province']) {
            $select->where('p.regional_market = ?', $params['province']);
        }

        if (isset($params['store']) && $params['store']) {
            $QStore = new Application_Model_Store();
            $where = $QStore->getAdapter()->quoteInto('name LIKE ?', '%'.$params['store'].'%');
            $result = $QStore->fetchAll($where);

            $store_ids = array();
            foreach ($result as $key => $value) {
                $store_ids[] = $value['id'];
            }

            $select->where('sl.store_id IN (?) OR sll.store_id IN (?)', $store_ids);
        }

        // Filter for ASM and Leader
        if (isset($params['asm']) and $params['asm']) {
            $QAsm = new Application_Model_Asm();
            $list_regions = $QAsm->get_cache($params['asm']);
            $list_regions = isset($list_regions['province']) && is_array($list_regions['province']) ? $list_regions['province'] : array();

            if (count($list_regions))
                $select->where('p.regional_market IN (?)', $list_regions);
            else
                $select->where('1=0', 1);

        } elseif (isset($params['leader']) and $params['leader']) {
            $select->where('p.id = ?', $params['leader']);
        }

        $order_str = '';

        if (isset($params['sort']) and $params['sort']) {
            $collate = ' ';

            if ($params['sort'] == 'area_id') {
                $select
                    ->join(array('r' => 'regional_market'), 'r.id=p.regional_market', array())
                    ->join(array('a' => 'area'), 'a.id=r.area_id', array());
            } elseif ($params['sort'] == 'province') {
                $select
                    ->join(array('r' => 'regional_market'), 'r.id=p.regional_market', array());
            }

            $desc = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';

            if (in_array($params['sort'], array('area_id', 'province', 'name'))) {
                $collate = ' COLLATE utf8_unicode_ci ';
            }

            if ($params['sort'] == 'name'){
                $order_str .= ' CONCAT(p.firstname, " ",p.lastname) '.$collate . $desc;

            } elseif ($params['sort'] == 'area_id') {
                $order_str .= ' a.name '.$collate . $desc;
            } elseif ($params['sort'] == 'province') {
                $order_str .= ' r.name '.$collate . $desc;
            }  elseif (in_array($params['sort'], array('current_store', 'pending_store'))) {
                $order_str .= $params['sort']. ' ' . $desc;
            } else {
                $order_str = 'p.`'.$params['sort'] . '` ' . $collate . $desc;
            }

            $select->order(new Zend_Db_Expr($order_str));
        }

        if ($limit)
            $select->limitPage($page, $limit);
        
        $result = $db->fetchAll($select);

        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    function fetchSalesPgPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'), 'p.firstname', 'p.lastname', 'p.email', 'p.phone_number', 'p.regional_market', 'p.group_id', 'p.status'));

        $select
            ->distinct()
            ->joinLeft(array('cw' => 'casual_worker'), 'cw.staff_id=p.id', array('casual_status' => 'cw.status'))
            ->joinLeft(array('sl' => 'store_staff'), 'sl.staff_id=p.id', array('current_store' => 'COUNT(DISTINCT sl.store_id)', 'sl.is_leader'))
            ->group('p.id');

        $select
            ->where('(p.group_id = ?', PGPB_ID)
            ->orWhere('p.group_id = ?)', SALES_ID);

        if (isset($params['name']) and $params['name']) {
            $select->where('CONCAT(p.firstname, " ",p.lastname) LIKE ?', '%'.$params['name'].'%');
        }

        if (isset($params['email']) and $params['email']) {
            $params['email'] = preg_replace('/'.EMAIL_SUFFIX.'/', '', $params['email']);
            $select->where('p.email LIKE ?', $params['email'].EMAIL_SUFFIX);
        }

        if ( isset($params['province']) and $params['province'] ) {
            $select->where('p.regional_market = ?', $params['province']);

        } elseif ( isset($params['area_id']) and $params['area_id'] ){
            $QRegionalMarket = new Application_Model_RegionalMarket();
            $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $params['area_id']);

            $regional_markets = $QRegionalMarket->fetchAll($where);
            $tem = array();

            foreach ($regional_markets as $regional_market)
                $tem[] = $regional_market->id;

            if (is_array($tem) && count($tem) > 0)
                $select->where('p.regional_market IN (?)', $tem);
            else
                $select->where('1=0', 1);
        }

        if (isset($params['store']) && $params['store']) {
            $QStore = new Application_Model_Store();
            $where = $QStore->getAdapter()->quoteInto('name LIKE ?', '%'.$params['store'].'%');
            $result = $QStore->fetchAll($where);

            $store_ids = array();
            foreach ($result as $key => $value) {
                $store_ids[] = $value['id'];
            }

            $select->where('sl.store_id IN (?) OR sl.store_id IN (?)', $store_ids);
        }

        // Filter for ASM
        if (isset($params['asm']) and $params['asm']) {
            $QAsm = new Application_Model_Asm();
            $list_regions = $QAsm->get_cache($params['asm']);
            $list_regions = isset($list_regions['province']) && is_array($list_regions['province']) ? $list_regions['province'] : array();

            if (count($list_regions))
                $select->where('p.regional_market IN (?)', $list_regions);
            else
                $select->where('1=0', 1);
        }

        if (isset($params['staff']) and $params['staff']) {
            $select->where('p.id = ?', intval($params['staff']));
        }

        $order_str = '';

        if (isset($params['sort']) and $params['sort']) {
            $collate = ' ';

            if ($params['sort'] == 'area_id') {
                $select
                    ->join(array('r' => 'regional_market'), 'r.id=p.regional_market', array())
                    ->join(array('a' => 'area'), 'a.id=r.area_id', array());
            } elseif ($params['sort'] == 'province') {
                $select
                    ->join(array('r' => 'regional_market'), 'r.id=p.regional_market', array());
            }

            $desc = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';

            if (in_array($params['sort'], array('area_id', 'province', 'name'))) {
                $collate = ' COLLATE utf8_unicode_ci ';
            }

            if ($params['sort'] == 'name'){
                $order_str .= ' CONCAT(p.firstname, " ",p.lastname) '.$collate . $desc;

            } elseif ($params['sort'] == 'area_id') {
                $order_str .= ' a.name '.$collate . $desc;
            } elseif ($params['sort'] == 'province') {
                $order_str .= ' r.name '.$collate . $desc;
            }  elseif (in_array($params['sort'], array('current_store', 'pending_store'))) {
                $order_str .= $params['sort']. ' ' . $desc;
            } else {
                $order_str = 'p.`'.$params['sort'] . '` ' . $collate . $desc;
            }

            $select->order(new Zend_Db_Expr($order_str));
        }

        if ($limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);

        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    /**
     * Thống kê số nhân viên hiện tại, chia theo vùng kinh doanh
     * @author phamquocbuu
     * @version 2013-11-05 13:50
     */
    function analytics() {
        $db = Zend_Registry::get('db');

        $subselect = $db->select()->from('staff')
        //            ->where('contract_expired_at > NOW()')
            ->where('off_date IS NULL');

        $select = $db->select()->from($subselect, array(
                'qty'=>'COUNT(*)', 'regional_market'
                ))
            ->group('regional_market');

        $stmt = $db->query($select);
        $data = $stmt->fetchAll();

        return $data;
    }

    function analytics_by_area() {
        $db = Zend_Registry::get('db');

        $subselect = $db->select()->from('staff')
        //            ->where('contract_expired_at > NOW()')
            ->where('off_date IS NULL');

        $select = $db
            ->select()
            ->from(array( 's' => $subselect ), array('qty'=>'COUNT(*)') )
            ->joinleft(array('r' => 'regional_market'), 'r.id = s.regional_market', array())
            ->joinleft(array('a' => 'area'), 'a.id = r.area_id', array('id' => 'a.id'))
            ->group('a.id');

        $stmt = $db->query($select);
        $data = $stmt->fetchAll();

        return $data;
    }

    /**
     * Chi tiết nhân viên hiện tại theo từng vùng kinh doanh
     * @author phamquocbuu
     * @version 2013-11-06 14:40
     */
    function analytics_by_region($regional_market) {
        $db = Zend_Registry::get('db');

        $where = $db->quoteInto('regional_market LIKE ?', $regional_market);

        $subselect = $db->select()->from('staff', array('id', 'department'))
        //            ->where('contract_expired_at > NOW()')
            ->where('off_date IS NULL OR off_date = 0 OR off_date =\'\'')
            ->where($where);
        $select = $db->select()->from($subselect, array(
                'qty'=>'COUNT(*)', 'title'
                ))
            ->group('title');

        $stmt = $db->query($select);
        $data = $stmt->fetchAll();
        return $data;
    }

    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if ($result === false) {

            $data = $this->fetchAll();

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->firstname . ' ' . $item->lastname;
                }
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }

    function get_all_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_all_cache');

        if ($result === false) {

            $data = $this->fetchAll();

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = array(
                        'name' => $item->firstname . ' ' . $item->lastname,
                        'email' => $item->email,
                        'code' => $item->code,
                        'ID_number' => $item->ID_number
                        );
                }
            }
            $cache->save($result, $this->_name.'_all_cache', array(), null);
        }
        return $result;
    }

    function get_all_sale_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_all_sale_cache');

        if ($result === false) {
            $where = $this->getAdapter()->quoteInto('title IN (?)', My_Staff_Title::getSalesman());
            $data = $this->fetchAll($where);

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = array(
                        'name'            => $item->firstname . ' ' . $item->lastname,
                        'email'           => $item->email,
                        'code'            => $item->code,
                        'regional_market' => $item->regional_market,
                        'title'           => $item->title,
                        'phone_number'    => $item->phone_number,
                        );
                }
            }
            $cache->save($result, $this->_name.'_all_cache', array(), 3600*24);
        }
        return $result;
    }

    /**
     * Lấy các cột trong bảng này
     */
    function get_cols()
    {
        $cols = $this->info(Zend_Db_Table_Abstract::COLS);
        return $cols;
    }

    /**
     * Function Build Query
     * Dựng câu query để select các staff theo những điều kiện nhất định
     *      để hiển thị ở các ô trong dashboard
     *
     * @param array $cols_existed   - các cột yêu cầu có sẵn giá trị
     * @param array $cols_needed    - các cột chưa có giá trị,
     *                              yêu cầu nhập giá trị mới vào
     * @return string - câu SQL lấy danh sách các staff theo yêu cầu
     *                      trường hợp dữ liệu vào rỗng, trống hoặc không có thực
     *                      thì không chọn staff nào cả
     */
    function build_query($cols_existed = array(), $cols_needed = array(), $for = 'staff')
    {
        // bắt buộc 2 mảng này phải có phần tử
        // không điền thì check thế quái nào
        if (count($cols_needed) == 0 || count($cols_existed) == 0 || trim($cols_needed[0]) == '' || trim($cols_existed[0]) == '') {
            return "SELECT * FROM " . $this->_name . " WHERE 0";
        }

        // danh sách các cột của table
        $cols = $this->get_cols();
        // $other = array_diff($cols, $exclude);

        // giá trị mặc định
        // chỉ check các cột này
        $default = array(
            'code'                    => 'NULL',
            'contract_type'           => '0',
            'contract_signed_at'      => 'NULL',
            'contract_term'           => '0',
            'contract_expired_at'     => 'NULL',
            'department'              => '0',
            'team'                    => '0',
            'firstname'               => 'NULL',
            'lastname'                => 'NULL',
            'title'                   => '',
            'joined_at'               => 'NULL',
            'dob'                     => '',
            'certificate'             => '',
            'level'                   => '',
            'temporary_address'       => 'NULL',
            'address'                 => '',
            'regional_market'         => '0',
            'permanent_address'       => '',
            'birth_place'             => '',
            'native_place'            => '',
            'ID_number'               => '',
            'ID_place'                => '',
            'ID_date'                 => 'NULL',
            'nationality'             => '0',
            'religion'                => '0',
            'phone_number'            => '',
            'email'                   => array('', 'NULL'),
            'group_id'                => '0',
            'social_insurance_time'   => '',
            'social_insurance_number' => '',
            'photo'                   => '',
            'off_date'                => array('', 'NULL'),
            );

        /**
         * @var bool
         * Dùng để check xem từng cụm ( ) đã được thêm phép so sánh nào chưa
         * Nếu có, thêm toán tử OR
         * ngược lại thì thôi
         */
        $flag = false;

        // Mẫu where hợp lệ: ( a OR b OR c ) AND ( d AND e AND f ) AND ( off_date IS NULL )
        $where = " ( ";

        // mớ này duyệt các điều kiện cần điền vào
        // yêu cầu điền đủ tất cả các cột mới cho qua,
        //      thiếu 1 cột cũng liệt kê staff đó ra
        // vì vậy dùng toán tử OR để check
        foreach($cols as $name) {
            // không có trong danh sách giá trị default thì bỏ qua
            //      (ví dụ created_at hay created_by thì check làm quái gì)
            if ( ! isset($default[$name]) ) continue;

            if ( in_array( $name, $cols_needed ) ) {

                if (is_array($default[ $name ])){

                    foreach ($default[ $name ] as $k=>$item){
                        $or = $k>0 ? 'OR' : '';
                        $where .= ' '.$or.' `' .$name. '`' .$this->get_type( $item );
                    }

                } else {

                    if( ! $flag )
                        $where .= "`".$name . "` ";
                    else
                        $where .= " OR `" . $name . "` ";

                    $flag = true;


                    $where .= $this->get_type( $default[ $name ] );
                }
            }
        }

        // kiểm tra biến where, phòng trường hợp các mảng trên không hợp lý,
        //      dẫn tới where chả có gì
        // đã vào tới build query thì phải xét đc 1 số cột nào đó đã có,
        //      và 1 số chưa có giá trị (nhận giá trị mặc định)
        // còn không thì không select staff nào cả
        $where .= (trim($where) != '(' ? ' ) AND (' : ' 0 ) AND (');

        /**
         * @var bool
         * Dùng để check xem từng cụm ( ) đã được thêm phép so sánh nào chưa
         * Nếu có, thêm toán tử AND
         * ngược lại thì thôi
         */
        $flag = false;

        // mớ này duyệt các điều kiện phải có sẵn
        // yêu cầu có đủ tất cả các cột
        // vì vậy dùng toán tử AND để check
        foreach($cols as $name) {
            // đã comment ở trên
            if ( ! isset($default[$name]) ) continue;

            if( in_array( $name, $cols_existed ) ) {
                if (is_array($default[ $name ])){

                    foreach ($default[ $name ] as $k=>$item){
                        if ( ! $flag )
                            $where .= " `" . $name . "` ";
                        else
                            $where .= " AND `" . $name . "` ";

                        $flag = true;
                        $where .= $this->get_type( $item, 1 );
                    }

                } else {

                    if ( ! $flag )
                        $where .= " `" . $name . "` ";
                    else
                        $where .= " AND `" . $name . "` ";

                    $flag = true;
                    $where .= $this->get_type( $default[ $name ], 1 );
                }

            }
        }

        // comment như đoạn trên
        $where .= (trim($where) != '(' ? ' ) ' : ' ( 0 ) ');

        // staff nghỉ rồi thì khỏi liệt kê ra
        $where .= " AND ( off_date IS NULL ) AND ( group_id <> ".BOARD_ID.") ";

        // xét xem có phải cho ASM/Sales Admin xem không
        // nếu phải thì lọc staff theo area
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        if ( $for == 'asm' && in_array( $userStorage->group_id, $this->config['asm'] ) ) {
            // lấy theo khu vực mà thằng đó lead
            $QArea            = new Application_Model_Area();
            $QRegionalMarket  = new Application_Model_RegionalMarket();

            $user_id = $userStorage->id;

            if ($userStorage->group_id == SALES_ADMIN_ID) {
                $staff = $this->find($user_id);
                $staff = $staff->current();

                if ($staff) {
                    $region = $staff['regional_market'];
                    $region = $QRegionalMarket->find($region);
                    $region = $region->current();
                }
            }

            $where_2 = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $region['area_id']);
            $regional_markets = $QRegionalMarket->fetchAll($where_2);
            

            $regional_markets_arr = array();

            foreach ($regional_markets as $regional_market) {
                $regional_markets_arr[] = $regional_market->id;
            }

            $result_asm = My_Asm::get_cache();
            
            // echo '<pre>';
            // var_dump($result_asm[$userStorage->id]['province']);
            // exit;
            if(count($result_asm[$userStorage->id]['province'])){
                $regional_markets_arr = array_merge($regional_markets_arr,$result_asm[$userStorage->id]['province']);    
            }
            
            $where .= (count($regional_markets_arr) ? (" AND regional_market IN (".implode(',', $regional_markets_arr).") ") : '')." AND created_at >= '" . date_sub(date_create(), new DateInterval('P'.$this->config['ttl'].'D'))->format('Y-m-d H:i:s') . "' ";
        }

        $order = " ORDER BY id DESC";

        return "SELECT * FROM " . $this->_name . " WHERE " . $where . $order;
    }

    /**
     * Có câu query rồi thì chạy nó thôi
     */
    function get_by_step($cols_existed = array(), $cols_needed = array(), $for = 'staff')
    {
        $db     = Zend_Registry::get('db');
        $sql    = $this->build_query($cols_existed, $cols_needed, $for);    
        return $db->query($sql);
    }

    /**
     * Dùng để build đoạn so sánh với giá trị mặc định
     * Ví dụ:   * mặc định : 0 và negative = 0 -> trả về ' = 0 '
     *          * mặc định : 0 và negative = 1 -> trả về ' <> 0 '
     */
    function get_type($value = '', $negative = 0)
    {
        switch ($value) {
            case 'NULL':
                return ' IS '. ( $negative == 1 ? ' NOT ' : '' ) . ' NULL ';
                break;
            case '0':
                return  ( $negative == 1 ? ' <> ' : '=' ) . ' 0 ';
                break;
            case '':
                return  ( $negative == 1 ? ' <> ' : '=' ) . ' \'\' ';
                break;

            default:
                return '';
                break;
        }
    }

    public function fetchPaginationTransfer($page, $limit, &$total, $params){
        if( isset($params['from']) AND $params['from']){
            $arrFrom = explode('/',$params['from']);
            $params['from'] = $arrFrom[2].'-'.$arrFrom[1].'-'.$arrFrom[0];
        }

        if( isset($params['to']) AND $params['to']){
            $arrFrom = explode('/',$params['to']);
            $params['to'] = $arrFrom[2].'-'.$arrFrom[1].'-'.$arrFrom[0];
        }

        $db = Zend_Registry::get('db');
        $arrCols = array(
            'staff_code'     => 's.code',
            'fullname'       => new Zend_Db_Expr('CONCAT(s.firstname," ",s.lastname)'),
            'transfer_time'  => 'p.from_date',
            'transfer_id'    => 'p.id',
            'info_types'     => new Zend_Db_Expr('GROUP_CONCAT(sl.info_type)'),
            'current_values' => new Zend_Db_Expr('GROUP_CONCAT(sl.current_value)'),
            'old_values'     => new Zend_Db_Expr('GROUP_CONCAT(sl.old_value)'),
        );

        $select = $db->select()
            ->from(array('p'=>'staff_transfer'),$arrCols)
            ->join(array('sl'=>'staff_log_detail'),'p.id = sl.transfer_id',array())
            ->join(array('s'=>'staff'),'s.id = p.staff_id',array())
            ->where('p.note IS NULL')
            ->group('p.id')

        ;

        if( isset($params['transfer_id']) AND $params['transfer_id']){
            $select->where('p.id = ?',$params['transfer_id']);
        }

        if( isset($params['from']) AND $params['from']){
            $select->where('p.from_date >= ?',$params['from']);
        }

        if( isset($params['to']) AND $params['to']){
            $select->where('p.from_date <= ?',$params['to']);
        }

        $order_str = '';

        if (isset($params['sort']) and $params['sort']) {
            $collate = ' ';
            $desc    = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';

            if ($params['sort'] == 'name'){
                $collate   = ' COLLATE utf8_unicode_ci ';
                $order_str .= ' CONCAT(s.firstname, " ",s.lastname) '.$collate . $desc;
            }else {
                $order_str = 'p.`'.$params['sort'] . '` ' . $collate . $desc;
            }

            $select->order(new Zend_Db_Expr($order_str));
        }else{
            $select->order('p.created_at DESC');
        }

        if ($limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);
        return $result;

    }

    function genStaffCode($joined_at=null){
        try {
            $joined_at = $joined_at ? $joined_at : date('Y-m-d');
            $firstDay = date('Y-m-01', strtotime($joined_at));

            $db = Zend_Registry::get('db');

            $select = $db->select()
                ->from(array('p'=>'staff'),array('count(id)'))
                /*->where('p.joined_at <= ?', $joined_at)*/
                ->where('p.joined_at >= ?', $firstDay)
            ;

            $total = $db->fetchOne($select);

            $filledUp = str_pad($total + 1, 4, '0', STR_PAD_LEFT);
            return date('ym', strtotime($joined_at . ' 00:00:00')).$filledUp;

        } catch (Exception $e){
            return null;
        }
    }

    function checkIsNotHeadOffice($department_id, $team_id, $title_id){
        $arrNotHeadOffice = unserialize(CONFIG_NOT_HEAD_OFFICE);
        if (isset($arrNotHeadOffice[$department_id]) and !is_array($arrNotHeadOffice[$department_id]) and $arrNotHeadOffice[$department_id] == $department_id)
            return true;

        if (isset($arrNotHeadOffice[$department_id][$team_id][$title_id]))
            return true;

        return false;
    }

    /**
     * @return array
     * Trả về danh sách được config cho phép xem report trong trainer
     */
    function staffTrainerAllowReport(){
        // 241: tienhung.nguyen
        // 3028: thienphan
        // 553: thin.nguyen
        // 644: thong.duong
        // 7278: ngocduyen.le
		// 3026: luyt.van
        return array(241,553,644,5899,3028,7278,3026,3028);
    }

    function offDatePurposeFetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        if ($limit) {
            $select = $db->select()
                ->from(array('p' => $this->_name),
                    array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'), 'p.*'));
        } else {
            $select = $db->select()
                ->from(array('p' => $this->_name),
                    array(new Zend_Db_Expr('DISTINCT p.id'), 'p.*'));
        }

        $select->joinLeft(array('e'=>'staff_education'),'p.id = e.staff_id AND e.default_level = 1',array('d_level'=>'e.level','d_certificate'=>'field_of_study'));

        $select->joinLeft(array('province'),'province.id = id_place_province',array('id_place_province_name'=>'province.name'));

        $select->where('(p.date_off_purpose is not null OR st.date_off_purpose is not null)');

        if (isset($params['off_date_from']) and $params['off_date_from']) {
            $arrFrom = explode('/',$params['off_date_from']);
            $params['off_date_from'] = $arrFrom[2].'-'.$arrFrom[1].'-'.$arrFrom[0];
            $select->where('(p.off_date >="' . $params['off_date_from'] . '" OR st.date_off_purpose >="' . $params['off_date_from'] . '")');
        }

        if (isset($params['off_date_to']) and $params['off_date_to']) {
            $arrFrom = explode('/',$params['off_date_to']);
            $params['off_date_to'] = $arrFrom[2].'-'.$arrFrom[1].'-'.$arrFrom[0];
            $select->where('(p.off_date <="' . $params['off_date_to'] . '" OR st.date_off_purpose <="' . $params['off_date_to'] . '")');
        }

        if (isset($params['name']) and $params['name']) {
            $select->where('CONCAT(p.firstname, " ",p.lastname) LIKE ?', '%'.$params['name'].'%');
            //            $select->orwhere('p.email LIKE ?', '%'.$params['name'].'%');

        }

        if (isset($params['company_id']) and $params['company_id']) {
            $select->where('p.company_id = ?', $params['company_id']);
        }

        if (isset($params['team']) and $params['team']) {
            if (is_array($params['team']) && count($params['team']) > 0) {
                $select->where('p.team IN (?)', $params['team']);
            } else {
                $select->where('1=0', 1);
            }
        }


        if (isset($params['contract_term']) and $params['contract_term']) {
            if (is_array($params['contract_term']) && count($params['contract_term']) > 0) {
                $select->where('p.contract_term IN (?)', $params['contract_term']);
            } else {
                $select->where('1=0', 1);
            }
        }

        if (isset($params['department']) and $params['department']) {
            if (is_array($params['department']) && count($params['department']) > 0) {
                $select->where('p.department IN (?)', $params['department']);
            } else {
                $select->where('1=0', 1);
            }
        }

        if(isset($params['salary_sales']) and $params['salary_sales'])
        {
            $select->where('p.title IN (?)', array(PGPB_TITLE, PGPB_2_TITLE,SALES_TITLE,SALES_ACCESSORIES_TITLE,SALES_LEADER_TITLE ,SALES_ACCESSORIES_LEADER_TITLE));
        }

        if (isset($params['is_officer']) and intval($params['is_officer']) > 0){
            if ($params['is_officer'] == 1)
                $select->where('p.is_officer = ? or p.team = 11 or p.title in (274,179,181)', 1);
            elseif ($params['is_officer'] == 2)
                $select->where('p.is_officer ? or p.team = 11 or p.title in (274,179,181)', 0);
            //$select->orWhere('p.team = ?', WARRANTY_CENTER);
        }

        if (isset($params['email']) and $params['email']) {
            $params['email'] = preg_replace('/'.EMAIL_SUFFIX.'/', '', $params['email']);
            $select->where('p.email LIKE ?', $params['email'].EMAIL_SUFFIX);
        }

        if (isset($params['date']) and $params['date'])
        {
            $date = explode(',', $params['date']);
            $select->where("DAY(STR_TO_DATE(p.dob, '%d/%m/%Y')) IN (?)", $date);
        }

        if (isset($params['month']) and $params['month'])
        {
            $date = explode(',', $params['month']);
            $select->where("MONTH(STR_TO_DATE(p.dob, '%d/%m/%Y')) IN (?)", $date);
        }

        if(isset($params['month_off']) and $params['month_off'])
        {
            $select
                ->joinLeft(array('mo' => 'off_date_add'), 'p.id = mo.staff_id ' , array('staff_id' => 'mo.staff_id'));
            $select->where('MONTH(mo.date) <> ?' , $params['month_off'] )->orWhere('MONTH(mo.date) is null' , null);
        }

        if (isset($params['year']) and $params['year'])
        {
            $date = explode(',', $params['year']);
            $select->where("YEAR(STR_TO_DATE(p.dob, '%d/%m/%Y')) IN (?)", $date);
        }

        if(isset($params['ready_print']) and $params['ready_print'])
        {

            if($params['ready_print'] == 1)
            {
                $select->where("p.address <> '' and p.birth_place <> ''  and p.ID_number <> '' and p.ID_place <> '' and p.ID_date <> '' and p.title <> '' and p.company_id <> 0");
            }
            elseif($params['ready_print'] == 2)
            {
                $select->where("address = '' or birth_place = ''  or ID_number = '' or ID_place = '' or ID_date = '' or title = '' or company_id = 0");
            }

            elseif($params['ready_print'] == 3)
            {
                $select->where("p.ID_number <> '' and p.ID_place <> '' and p.ID_date <> '' and p.title <> '' and p.company_id <> 0 and EXISTS (select * from staff_address sa2 where p.id = sa2.staff_id and sa2.address_type = 4 and sa2.address is not null)");
            }

            elseif($params['ready_print'] == 4)
            {
                $select->where("p.ID_number = '' or p.ID_place = '' or p.ID_date = '' or p.title = '' or p.company_id = 0 or  and EXISTS (select * from staff_address sa2 where p.id = sa2.staff_id and sa2.address_type = 4 and sa2.address is not null)");
            }

        }

        if (isset($params['title']) and $params['title']) {
            if (is_array($params['title']) && count($params['title']) > 0) {
                $select->where('p.title IN (?)', $params['title']);
            } else {
                $select->where('1=0', 1);
            }
        }

        $select->joinLeft(array('st' => 'staff_temp'),
            '
                p.id = st.staff_id
            ',
            array(
                'staff_temp_id' => 'st.id',
                'staff_temp_is_approved' => 'st.is_approved',
                'staff_temp_date_off_purpose' => 'st.date_off_purpose',
                'staff_temp_date_off_purpose_reason' => 'st.date_off_purpose_reason',
                'staff_temp_date_off_purpose_detail' => 'st.date_off_purpose_detail',
                'staff_temp_off_type' => 'st.off_type',
                'staff_temp_created_at' => 'st.created_at',
                'staff_temp_created_by' => 'st.created_by',
            ));

        if (isset($params['need_approve']) and $params['need_approve']){
            $select->where('st.is_approved = ?', 0);
        }


        if (isset($params['is_approved']) and $params['is_approved'] != '') {
            if ($params['is_approved'] == 1)
                $select->where('st.is_approved IS NULL');
            elseif ($params['is_approved'] == 2)
                $select->where('st.is_approved = ?', 0);
            else
                $select->where('st.is_approved = ?', $params['is_approved']);
        }


        // list
        if (isset($params['exp']) and $params['exp'])
            $select->where('p.contract_expired_at > NOW()');

        //joined at
        if(isset($params['joined_at']) and $params['joined_at'])
        {
            $arrFrom = explode('/',$params['joined_at']);
            $params['joined_at'] = $arrFrom[2].'-'.$arrFrom[1].'-'.$arrFrom[0];
            $select->where('p.joined_at = ?' , $params['joined_at']);
        }
        // id_number
        if(isset($params['indentity']) and $params['indentity'])
        {
            $select->where('p.ID_number = ?', $params['indentity']);
        }

        if(isset($params['off_type']) and $params['off_type'])
        {
            $select->where('(p.off_type = "' . $params['off_type'] . '" OR st.off_type = "' . $params['off_type'] . '")');
        }

        //off_date
        if(isset($params['off_date']) and $params['off_date'])
        {
            $arrFrom = explode('/',$params['off_date']);
            $params['off_date'] = $arrFrom[2].'-'.$arrFrom[1].'-'.$arrFrom[0];
            $select->where('p.off_date = ?' , $params['off_date']);
        }

        if (isset($params['sales_team_id']) and $params['sales_team_id'])
            $select->where('p.sales_team_id = ?', $params['sales_team_id']);

        if (isset($params['is_leader']) and $params['is_leader'])
            $select->where('p.is_leader = ?', $params['is_leader']);

        if (isset($params['distribution_id']) and $params['distribution_id'])
            $select->where('p.distribution_id = ?', $params['distribution_id']);

        if (isset($params['note']) and $params['note'])
            $select->where('p.note LIKE ?', '%'.$params['note'].'%');

        if (isset($params['code']) and $params['code'])
            $select->where('p.code LIKE ?', '%'.$params['code'].'%');

        if (isset($params['has_photo']) and $params['has_photo'])
            $select->where('p.photo IS NOT NULL AND p.photo <> \'\'', 1);

        if (isset($params['ood']) and $params['ood']){
            $select->where(' DATEDIFF(p.contract_expired_at,NOW()) <= ?', 30);
            /*$select->where(' DATEDIFF(NOW(),p.contract_expired_at) >= ?', 0);*/
        }

        if (isset($params['no_print']) and $params['no_print']){
            $select->where('p.print_time = 0 OR p.contract_signed_at is NULL and p.contract_expired_at is NULL', '');
        }

        if (isset($params['tags']) and $params['tags']){
            $select->join(array('ta_ob' => 'tag_object'),
                '
                    p.id = ta_ob.object_id
                    AND ta_ob.type = '.TAG_STAFF.'
                ',
                array());
            $select->join(array('ta' => 'tag'),
                '
                    ta.id = ta_ob.tag_id
                ',
                array());

            $select->where('ta.name IN (?)', $params['tags']);
        }

        if (isset($params['area_id']) and $params['area_id'] && !(isset($params['regional_market']) and $params['regional_market'])){
            $QRegionalMarket = new Application_Model_RegionalMarket();

            if (is_array($params['area_id']) && count($params['area_id']) > 0) {
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id IN (?)', $params['area_id']);
            } else {
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $params['area_id']);
            }

            $regional_markets = $QRegionalMarket->fetchAll($where);
            $tem = array();

            foreach ($regional_markets as $regional_market)
                $tem[] = $regional_market->id;

            if (is_array($tem) && count($tem) > 0)
                $select->where('p.regional_market IN (?)', $tem);
            else
                $select->where('1=0', 1);

        } elseif (isset($params['regional_market']) and $params['regional_market']) {
            if (is_array($params['regional_market']) && count($params['regional_market']) > 0) {
                $select->where('p.regional_market IN (?)', $params['regional_market']);
            } else {
                $select->where('1=0', 1);
            }
        }

        // dành để lọc staff cho các asm, admin... xem; chỉ hiện thị staff thuộc khu vực được gán
        if (isset($params['list_province_ids']) && $params['list_province_ids']) {
            if (is_array($params['list_province_ids']) && count($params['list_province_ids'])) {
                $select->where('p.regional_market IN (?)', $params['list_province_ids']);
            } elseif (is_numeric($params['list_province_ids']) && intval($params['list_province_ids']) > 0) {
                $select->where('p.regional_market = ?', intval($params['list_province_ids']));
            }
        }

        if(isset($params['area_trainer_right']) and $params['area_trainer_right'])
        {
            $QRegionalMarket = new Application_Model_RegionalMarket();

            if (is_array($params['area_trainer_right']) && count($params['area_trainer_right']) > 0) {
                $whereRegionalMarketRight = $QRegionalMarket->getAdapter()->quoteInto('area_id IN (?)', $params['area_trainer_right']);
            } else {
                $whereRegionalMarketRight  = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $params['area_trainer_right']);
            }

            $regional_markets_rights = $QRegionalMarket->fetchAll($whereRegionalMarketRight);
            $tem = array();

            foreach ($regional_markets_rights as $regional_markets_right)
                $tem[] = $regional_markets_right->id;

            if (is_array($tem) && count($tem) > 0)
                $select->where('p.regional_market IN (?)', $tem);
            else
                $select->where('1=0', 1);
        }

        if (isset($params['s_assign']) && $params['s_assign']) {
            $QDeparment = new Application_Model_Department();
            $where = $QDeparment->getAdapter()->quoteInto('name LIKE ?', 'KINH DOANH');
            $department_id = $QDeparment->fetchAll($where)->current()->id;
            $select->where('p.department = ?', $department_id);
        }

        if (isset($params['date_off_purpose_update_by'])) {
            $select->where('(p.date_off_purpose_update_by = ' . $params['date_off_purpose_update_by'] . ' OR st.created_by = ' . $params['date_off_purpose_update_by'] . ' OR st.updated_by = ' . $params['date_off_purpose_update_by'] . ')');
        }

        $order_str = '';

        if (isset($params['sort']) and $params['sort']) {
            $collate = ' ';
            $desc = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';

            if ($params['sort'] == 'name'){
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= ' CONCAT(p.firstname, " ",p.lastname) '.$collate . $desc;
            } elseif ( /*$params['sort'] == 'title' || */$params['sort'] == 'department' || $params['sort'] == 'team' ) {
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= $collate . $desc;
            } elseif ($params['sort'] == 'date_off_purpose') {
                $order_str .= 'st.id DESC, p.date_off_purpose_update_at DESC';
            } else {
                $order_str = 'p.`'.$params['sort'] . '` ' . $collate . $desc;
            }

            $select->order(new Zend_Db_Expr($order_str));
        }

        $select->group('p.id');

        if ($limit)
            $select->limitPage($page, $limit);

        if (isset($params['export']) && $params['export'])
            return $select->__toString();

        $result = $db->fetchAll($select);


        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public function getStaffByStore($params){
        $db = Zend_Registry::get('db');
        $store_id = $params['store_id'];
        if(!$store_id){
            throw new Exception('please select store');
        }

        $cols = array(
            'staff_id' => 'c.id',
            'staff_name'=>'CONCAT(c.firstname," ",c.lastname)',
            'title' => 'c.title',
        );
        $select = $db->select()
                ->from(array('a'=>'store'),$cols)
                ->join(array('b'=>'store_staff'),'a.id = b.store_id',array())
                ->join(array('c'=>'staff'),'c.id = b.staff_id',array())
                ->where('c.off_date IS NULL',0)
                ->where('a.id = ?',$store_id);
        $store_staff = $db->fetchAll($select);

        $select_leader = $db->select()
                ->from(array('a'=>'store'),$cols)
                ->join(array('b'=>'store_leader'),'a.id = b.store_id',array())
                ->join(array('c'=>'staff'),'c.id = b.staff_id',array())
                ->where('c.off_date IS NULL',0)
                ->where('a.id = ?',$store_id);
        $leader = $db->fetchRow($select_leader);

        $result = array();

        if(count($store_staff)){
            foreach($store_staff as $item){
                if( in_array($item['title'],My_Staff_Title::getPg()) ){
                    $result['pg'][] = $item['staff_name'];
                }elseif(in_array($item['title'],My_Staff_Title::getSale())){
                    $result['sale'][] = $item['staff_name'];
                }
            }
        }

        if($leader){
            $result['leader'][] = $leader['staff_name'];
        }

        return $result;
    }


    function getScoresCourse($staff_id){
        $db = Zend_Registry::get('db'); 
        $select = $db->select();
        $select = "SELECT staff_id, course_name, from_date, to_date, Result
                    FROM
                    (
                    	SELECT c.id AS staff_id, d.name course_name, d.from_date, d.to_date, CASE WHEN b.result = 1 THEN 'Ðậu' ELSE 'Rớt' END Result
                    	FROM `staff_training` a
                    	INNER JOIN `trainer_course_detail` b ON a.id = b.`new_staff_id` AND (b.result IS NOT NULL AND b.result > 0)
                    	INNER JOIN staff c ON a.cmnd = c.id_number 
                    	INNER JOIN trainer_course d ON b.course_id = d.id
                    	UNION ALL
                    	SELECT staff_id, b.name course_name, b.from_date, b.to_date, CASE WHEN a.result = 1 THEN 'Ðậu' ELSE 'Rớt' END Result
                    	FROM trainer_course_detail a
                    	INNER JOIN trainer_course b ON a.course_id = b.id
                    	WHERE staff_id IS NOT NULL AND staff_id > 0 AND (result IS NOT NULL AND result > 0)
                    ) course
                    
                    WHERE staff_id = ".$staff_id;

        $result = $db->fetchAll($select);

        return $result;
    }
    
    
    function getScoresWarning($staff_id){
        $db = Zend_Registry::get('db'); 
        $select = $db->select();
        
        $select = "SELECT *
                FROM `staff_reward_warning` 
                WHERE `type` = 2 AND staff_id = ".$staff_id;

        $result = $db->fetchAll($select);

        return $result;
    }
    
    function getAttendanceDetail($staff_id){
        $db = Zend_Registry::get('db'); 
        $select = $db->select();

        // $select = '
        //     SELECT NULL yearno, NULL monthno, WDate total_attendance, 0 total_ot
        //     FROM staff_miss_time
        //     WHERE staff_id = '.$staff_id.'
        //     UNION
        //     SELECT
        //         wo.`yearno`,
        //         wo.`monthno`,
        //         SUM(
        //             CASE
        //             WHEN ti.total_attendance > wo.`workingdate` THEN
        //                 wo.`workingdate`
        //             ELSE
        //                 ti.total_attendance
        //             END
        //         ) AS total_attendance,
        //         (
        //             SUM(
        //                 CASE
        //                 WHEN (wo.`yearno` < 2016 OR (wo.`yearno` = 2016 AND wo.`monthno` < 10)) then 0
        //                 WHEN  ti.total_attendance > wo.`workingdate` THEN
        //                     ti.total_attendance - wo.`workingdate`
        //                 ELSE
        //                     0
        //                 END
        //             ) * 3
        //         ) AS total_ot
        //     FROM
        //         (
        //             SELECT
        //                 staff_id,
        //                 YEAR (created_at) yearno,
        //                 MONTH (created_at) monthno,
        //                 COUNT(id) total_attendance
        //             FROM
        //                 (
        //                     SELECT
        //                         staff_id,
        //                         DATE(created_at) created_at,
        //                         MAX(id) id
        //                     FROM
        //                         `time`
        //                     WHERE
        //                         staff_id = '.$staff_id.' and off = 0 AND NOT (DAYOFWEEK(`created_at`) = 1 AND `created_at` < "2016-10-01 00:00")
        //                     GROUP BY
        //                         staff_id,
        //                         DATE(created_at)
        //                 ) tmp
        //             GROUP BY
        //                 staff_id,
        //                 YEAR (created_at),
        //                 MONTH (created_at)
        //         ) ti
        //     INNER JOIN workingday wo ON ti.yearno = wo.`yearno`
        //     AND ti.monthno = wo.`monthno`
        //     GROUP BY
        //         ti.staff_id,
        //         wo.`yearno`,
        //         wo.`monthno`
        // ';

        $select = '
            SELECT NULL yearno, NULL monthno, WDate total_attendance, 0 total_ot
            FROM staff_miss_time
            WHERE staff_id = '.$staff_id.'
            UNION
            SELECT
                wo.`yearno`,
                wo.`monthno`,
                SUM(
                    CASE
                    WHEN ti.total_attendance > wo.`workingdate` THEN
                        wo.`workingdate`
                    ELSE
                        ti.total_attendance
                    END
                ) AS total_attendance,
                (
                    SUM(
                        CASE
                        WHEN  ti.total_attendance > wo.`workingdate` THEN
                            ti.total_attendance - wo.`workingdate`
                        ELSE
                            0
                        END
                    ) * 3
                ) AS total_ot
            FROM
                (
                    SELECT
                        staff_id,
                        YEAR (created_at) yearno,
                        MONTH (created_at) monthno,
                        COUNT(id) total_attendance
                    FROM
                        (
                            SELECT
                                staff_id,
                                DATE(created_at) created_at,
                                MAX(id) id
                            FROM
                                `time`
                            WHERE
                                staff_id = '.$staff_id.' and off = 0 AND `created_at` >= "2016-10-01 00:00"
                            GROUP BY
                                staff_id,
                                DATE(created_at)
                        ) tmp
                    GROUP BY
                        staff_id,
                        YEAR (created_at),
                        MONTH (created_at)
                ) ti
            INNER JOIN workingday wo ON ti.yearno = wo.`yearno`
            AND ti.monthno = wo.`monthno`
            GROUP BY
                ti.staff_id,
                wo.`yearno`,
                wo.`monthno`
        ';

        $result = $db->fetchAll($select);

        return $result;
    }
    
        
    function getKPIdetail($staff_id){
        $db = Zend_Registry::get('db'); 
        $select = $db->select();
        
        $select = "SELECT b. NAME good, c. NAME color, a.price, a.quantity, a.kpi_score
                    FROM (
                    		SELECT 	good_id, 	color_id, MAX(`value`) price, 	COUNT(imei_sn) quantity,
                    			SUM(
                    				CASE
                    				WHEN `value` BETWEEN 1 	AND 3999999 THEN 1
                    				WHEN `value` BETWEEN 4000000 AND 8000000 THEN 	2 ELSE 	3 END
                    			) kpi_score
                    		FROM  imei_kpi
                    		WHERE 	pg_id = ".$staff_id." AND timing_date >= '2013-11-01 00:00'
                    		GROUP BY 	good_id, 	color_id
                    	) a
                    INNER JOIN warehouse.good b ON a.good_id = b.id
                    LEFT JOIN warehouse.good_color c ON a.color_id = c.id";

        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getStaffBi($params)
    {
        $db = Zend_Registry::get('db'); 
        //1
        $select = $db->select();
        $select->from(array('p' => $this->_name),
            array('num'=>'count(p.id)','p.title', 'staff_id'=>'p.id')
        );
        $select->joinLeft(array('t' => 'team'), 'p.title = t.id', array('name'=>'t.name'));
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());
        $select->where('p.title IN (308,179,190,175,183,182,312,293,274)',null);
        $select->where('off_date IS NULL',null);
        
        if(isset($params['area_list']) and $params['area_list']){
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        if(isset($params['province']) and $params['province']){
            $select->where('r.id IN (?)', $params['province']);
        }
        
        $select->group('p.title');
        $select->order('field(p.title, 308, 179, 190, 175, 183, 274, 312, 293, 182)');
        $data = $db->fetchAll($select);
        //
        
        //2
        $select2 = $db->select();
        $select2->from(array('p' => $this->_name),
                    array('num'=>'count(p.id)')
                );
        $select2->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());
        $select2->where('off_date IS NULL',null);
        
        if(isset($params['area_list']) and $params['area_list']){
            $select2->where('r.area_id IN (?)', $params['area_list']);
        }

        if(isset($params['province']) and $params['province']){
            $select2->where('r.id IN (?)', $params['province']);
        }
        
        $data2 = $db->fetchRow($select2);
        //
        
        $result = array();
        foreach($data as $key => $value){
            $result[$key] = array(
                'title' => $value['title'],
                'name'  => $value['name'],
                'num'   => $value['num'],
                'total' => $data2['num'],
                'staff_id' => $value['staff_id']
            );
        }
        return $result;
    }
    
    
    public function getStaffBiOff($params)
    {
        $db = Zend_Registry::get('db');
        $cache      = Zend_Registry::get('cache');
            
        //1
        $select = $db->select();
        $select->from(array('p' => $this->_name),
                    array('num'=>'count(p.id)','p.title','month_off_date'=>'MONTH(off_date)','year_off_date'=>'YEAR(off_date)')
                )
                ->where('p.title IN (183,182)',null)
                ->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());
                
                if(isset($params['area_list']) and $params['area_list']){
                    $select->where('r.area_id IN (?)', $params['area_list']);
                }
                
                $select->group(array('p.title','MONTH(off_date)','YEAR(off_date)'));
        $data = $db->fetchAll($select);
        //
        
        //2
        $select2 = $db->select();
        $select2->from(array('p' => $this->_name),
                    array('num'=>'count(p.id)')
                )
                ->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());
                
                if(isset($params['area_list']) and $params['area_list']){
                    $select->where('r.area_id IN (?)', $params['area_list']);
                }
                
                $select2->where('off_date IS NULL',null);
        $data2 = $db->fetchRow($select2);
        //
        
        $result = array();
        foreach($data as $key => $value){
            $result[$value['title']][$value['month_off_date']][$value['year_off_date']] = $value['num'];
        }
            
        return $result;
    }
    
    
    public function getStaffBiJoined($params)
    {
        $db = Zend_Registry::get('db');
            
        //1
        $select = $db->select();
        $select->from(array('p' => $this->_name),
                    array('num'=>'count(p.id)','p.title','month_joined_date'=>'MONTH(joined_at)','year_joined_date'=>'YEAR(joined_at)')
                )
                ->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());
                
                if(isset($params['area_list']) and $params['area_list']){
                    $select->where('r.area_id IN (?)', $params['area_list']);
                }
                
                $select->where('p.title IN (183,182)',null);
                $select->group(array('p.title','MONTH(joined_at)','YEAR(joined_at)'));
        $data = $db->fetchAll($select);
        //
        
        //2
        $select2 = $db->select();
        $select2->from(array('p' => $this->_name),
                    array('num'=>'count(p.id)')
                )
                ->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());
                
                if(isset($params['area_list']) and $params['area_list']){
                    $select2->where('r.area_id IN (?)', $params['area_list']);
                }
                
                $select2->where('joined_at IS NULL',null);
        $data2 = $db->fetchRow($select2);
        //
        
        $result = array();
        foreach($data as $key => $value){
            $result[$value['title']][$value['month_joined_date']][$value['year_joined_date']] = $value['num'];
        }
            
        return $result;
    }

    public function updatePhoto($params = array())
    {
        if(isset($params['new_name']) && isset($params['staff_id']))
        {
            $db = Zend_Registry::get('db');
            
            $sql_update_new_photo = "update staff 
                set photo = '" . $params['new_name'] ."'
                where id = " . $params['staff_id'] . "
                limit 1";
            $stmt = $db->prepare($sql_update_new_photo);
            $res = $stmt->execute();
            if($res == true && !empty($params['old_name']))
            {
                $sql_insert_old_photo = "insert into photo_back 
                (staff_id, photo) values 
                (" . $params['staff_id'] . " , '" . $params['old_name'] . "')";
                $db->query($sql_insert_old_photo);
            }
            
            $stmt->closeCursor();
            $db = $stmt = null;
            return $res;
        }
    }
    
    public function updateIdPhoto($params = array())
    {
        if(isset($params['new_name']) && isset($params['staff_id']))
        {
            $db = Zend_Registry::get('db');
            
            $sql_update_new_photo = "update staff 
                set id_photo = '" . $params['new_name'] ."'
                where id = " . $params['staff_id'] . "
                limit 1";
            $stmt = $db->prepare($sql_update_new_photo);
            $res = $stmt->execute();
            if($res == true && !empty($params['old_name']))
            {
                $sql_insert_old_photo = "insert into photo_back 
                (staff_id, id_photo) values 
                (" . $params['staff_id'] . " , '" . $params['old_name'] . "')";
                $db->query($sql_insert_old_photo);
            }
            
            $stmt->closeCursor();
            $db = $stmt = null;
            return $res;
        }
    }

    public function updateIdPhotoBack($params = array())
    {
        if(isset($params['new_name']) && isset($params['staff_id']))
        {
            $db = Zend_Registry::get('db');
            
            $sql_update_new_photo = "update staff 
                set id_photo_back = '" . $params['new_name'] ."'
                where id = " . $params['staff_id'] . "
                limit 1";
            $stmt = $db->prepare($sql_update_new_photo);
            $res = $stmt->execute();
            if($res == true && !empty($params['old_name']))
            {
                $sql_insert_old_photo = "insert into photo_back 
                (staff_id, id_photo_back) values 
                (" . $params['staff_id'] . " , '" . $params['old_name'] . "')";
                $db->query($sql_insert_old_photo);
            }
            
            $stmt->closeCursor();
            $db = $stmt = null;
            return $res;
        }
    }

    function fetchPaginationPhoto($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        if ($limit) {
            $select = $db->select()
                ->from(array('p' => $this->_name),
                    array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'), 'p.*'));
        } else {
            $select = $db->select()
                ->from(array('p' => $this->_name),
                    array(new Zend_Db_Expr('DISTINCT p.id'), 'p.*'));
        }

        $select->joinLeft(array('e'=>'staff_education'),'p.id = e.staff_id AND e.default_level = 1',array('d_level'=>'e.level','d_certificate'=>'field_of_study'));

        $select->joinLeft(array('province'),'province.id = id_place_province',array('id_place_province_name'=>'province.name'));

        $select->joinLeft(array('rm' => 'regional_market'), 'p.regional_market=rm.id', array());
        
        $select->joinLeft(array('ar' => 'area'), 'ar.id=rm.area_id', array('area_name' => 'ar.name'));

        if (isset($params['is_print']) and $params['is_print']){
            $select->where('p.is_print = ?', 1);
        }

        if (isset($params['is_not_print']) and $params['is_not_print']){
            $select->where('p.is_print = ?', 0);
        }

        if (isset($params['status']) and $params['status'] != ''){
            $select->where('p.status = ?', $params['status']);
        }

        if (isset($params['is_official']) and $params['is_official']){
            $select->where('p.contract_term in (1, 6, 7)');
        }

        if(isset($params['only_have_photo']) and $params['only_have_photo'])
        {
            $select->where('p.photo is not null');
        }

        if (isset($params['name']) and $params['name']) {
            $select->where('( CONCAT(p.firstname, " ",p.lastname) LIKE ?', '%'.$params['name'].'%');
            $select->orwhere('p.code LIKE ? ', '%'.$params['name'].'%');
            $select->orwhere('p.email LIKE ? )', '%'.$params['name'].'%');

        }

        if (isset($params['company_id']) and $params['company_id']) {
            $select->where('p.company_id = ?', $params['company_id']);
        }

        if (isset($params['team']) and $params['team']) {
            if (is_array($params['team']) && count($params['team']) > 0) {
                $select->where('p.team IN (?)', $params['team']);
            } else {
                $select->where('1=0', 1);
            }
        }


        if (isset($params['contract_term']) and $params['contract_term']) {
            if (is_array($params['contract_term']) && count($params['contract_term']) > 0) {
                $select->where('p.contract_term IN (?)', $params['contract_term']);
            } else {
                $select->where('1=0', 1);
            }
        }

        if (isset($params['department']) and $params['department']) {
            if (is_array($params['department']) && count($params['department']) > 0) {
                $select->where('p.department IN (?)', $params['department']);
            } else {
                $select->where('1=0', 1);
            }
        }

        if(isset($params['salary_sales']) and $params['salary_sales'])
        {
            $select->where('p.title IN (?)', array(PGPB_TITLE, PGPB_2_TITLE,SALES_TITLE,SALES_ACCESSORIES_TITLE,SALES_LEADER_TITLE ,SALES_ACCESSORIES_LEADER_TITLE));
        }

        if (isset($params['is_officer']) and intval($params['is_officer']) > 0){
            if ($params['is_officer'] == 1)
                $select->where('p.is_officer = ? or p.team = 11 or p.title in (274,179,181)', 1);
            elseif ($params['is_officer'] == 2)
                $select->where('p.is_officer ? or p.team = 11 or p.title in (274,179,181)', 0);
                //$select->orWhere('p.team = ?', WARRANTY_CENTER);
        }

        if (isset($params['email']) and $params['email']) {
            $params['email'] = preg_replace('/'.EMAIL_SUFFIX.'/', '', $params['email']);
            $select->where('p.email LIKE ?', $params['email'].EMAIL_SUFFIX);
        }

        if (isset($params['date']) and $params['date'])
        {
            $date = explode(',', $params['date']);
            $select->where("DAY(STR_TO_DATE(p.dob, '%d/%m/%Y')) IN (?)", $date);
        }

        if (isset($params['month']) and $params['month'])
        {
            $date = explode(',', $params['month']);
            $select->where("MONTH(STR_TO_DATE(p.dob, '%d/%m/%Y')) IN (?)", $date);
        }

        if(isset($params['month_off']) and $params['month_off'])
        {
             $select
            ->joinLeft(array('mo' => 'off_date_add'), 'p.id = mo.staff_id ' , array('staff_id' => 'mo.staff_id'));
            $select->where('MONTH(mo.date) <> ?' , $params['month_off'] )->orWhere('MONTH(mo.date) is null' , null);
        }

        if (isset($params['year']) and $params['year'])
        {
            $date = explode(',', $params['year']);
            $select->where("YEAR(STR_TO_DATE(p.dob, '%d/%m/%Y')) IN (?)", $date);
        }

        if(isset($params['ready_print']) and $params['ready_print'])
        {

            if($params['ready_print'] == 1)
           {
             $select->where("p.address <> '' and p.birth_place <> ''  and p.ID_number <> '' and p.ID_place <> '' and p.ID_date <> '' and p.title <> '' and p.company_id <> 0");
           }
            elseif($params['ready_print'] == 2)
            {
                 $select->where("address = '' or birth_place = ''  or ID_number = '' or ID_place = '' or ID_date = '' or title = '' or company_id = 0");
            }

            elseif($params['ready_print'] == 3)
            {
                 $select->where("p.ID_number <> '' and p.ID_place <> '' and p.ID_date <> '' and p.title <> '' and p.company_id <> 0 and EXISTS (select * from staff_address sa2 where p.id = sa2.staff_id and sa2.address_type = 4 and sa2.address is not null)");
            }

            elseif($params['ready_print'] == 4)
            {
                 $select->where("p.ID_number = '' or p.ID_place = '' or p.ID_date = '' or p.title = '' or p.company_id = 0 or  and EXISTS (select * from staff_address sa2 where p.id = sa2.staff_id and sa2.address_type = 4 and sa2.address is not null)");
            }

        }

        if (isset($params['title']) and $params['title']) {
            if (is_array($params['title']) && count($params['title']) > 0) {
                $select->where('p.title IN (?)', $params['title']);
            } else {
                $select->where('1=0', 1);
            }
        }

        $select->joinLeft(array('st' => 'staff_temp'),
            '
                p.id = st.staff_id
            ',
            array('staff_temp_id' => 'st.id', 'staff_temp_is_approved' => 'st.is_approved'));

        if (isset($params['need_approve']) and $params['need_approve']){
            $select->where('st.is_approved = ?', 0);
        }

        if ( ( isset($params['off']) and intval($params['off']) > 0 ) || ( isset($params['sname']) and $params['sname'] == 1 ) )
            $select->where('p.off_date is '
                    . ( $params['off'] == 2 ? 'not' : '')
                    .' null');

        // list
        if (isset($params['exp']) and $params['exp'])
            $select->where('p.contract_expired_at > NOW()');

        //joined at
        if(isset($params['joined_at']) and $params['joined_at'])
        {
            $arrFrom = explode('/',$params['joined_at']);
            $params['joined_at'] = $arrFrom[2].'-'.$arrFrom[1].'-'.$arrFrom[0];
            $select->where('p.joined_at = ?' , $params['joined_at']);
        }
        // id_number
        if(isset($params['indentity']) and $params['indentity'])
        {
            $select->where('p.ID_number = ?', $params['indentity']);
        }

        if(isset($params['off_type']) and $params['off_type'])
        {
            $select->where('p.off_type = ?', $params['off_type']);
        }

        //off_date
        if(isset($params['off_date']) and $params['off_date'])
        {
            $arrFrom = explode('/',$params['off_date']);
            $params['off_date'] = $arrFrom[2].'-'.$arrFrom[1].'-'.$arrFrom[0];
            $select->where('p.off_date = ?' , $params['off_date']);
        }

        if (isset($params['sales_team_id']) and $params['sales_team_id'])
            $select->where('p.sales_team_id = ?', $params['sales_team_id']);

        if (isset($params['is_leader']) and $params['is_leader'])
            $select->where('p.is_leader = ?', $params['is_leader']);

        if (isset($params['distribution_id']) and $params['distribution_id'])
            $select->where('p.distribution_id = ?', $params['distribution_id']);

        if (isset($params['note']) and $params['note'])
            $select->where('p.note LIKE ?', '%'.$params['note'].'%');

        if (isset($params['code']) and $params['code'])
            $select->where('p.code LIKE ?', '%'.$params['code'].'%');
		
		if (isset($params['list_staff_code']) and $params['list_staff_code'])
            $select->where('p.code IN (?)',$params['list_staff_code']);

        if (isset($params['has_photo']) and $params['has_photo'])
            $select->where('p.photo IS NOT NULL AND p.photo <> \'\'', 1);

        if (isset($params['ood']) and $params['ood']){
            $select->where(' DATEDIFF(p.contract_expired_at,NOW()) <= ?', 30);
            /*$select->where(' DATEDIFF(NOW(),p.contract_expired_at) >= ?', 0);*/
        }

        if (isset($params['no_print']) and $params['no_print']){
            $select->where('p.print_time = 0 OR p.contract_signed_at is NULL and p.contract_expired_at is NULL', '');
        }

        if (isset($params['tags']) and $params['tags']){
            $select->join(array('ta_ob' => 'tag_object'),
                '
                    p.id = ta_ob.object_id
                    AND ta_ob.type = '.TAG_STAFF.'
                ',
                array());
            $select->join(array('ta' => 'tag'),
                '
                    ta.id = ta_ob.tag_id
                ',
                array());

            $select->where('ta.name IN (?)', $params['tags']);
        }
        if (isset($params['area_id']) and ($params['area_id'] || $params['area_id'] == 0) && !(isset($params['regional_market']) and $params['regional_market'])){
            $QRegionalMarket = new Application_Model_RegionalMarket();
            
            if (is_array($params['area_id']) && count($params['area_id']) > 0) {
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id IN (?)', $params['area_id']);
            } else {
                $where = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $params['area_id']);
            }

            $regional_markets = $QRegionalMarket->fetchAll($where);
            $tem = array();

            foreach ($regional_markets as $regional_market)
                $tem[] = $regional_market->id;

            if (is_array($tem) && count($tem) > 0)
                $select->where('p.regional_market IN (?)', $tem);
            else
                $select->where('1=0', 1);

        } elseif (isset($params['regional_market']) and $params['regional_market']) {
            if (is_array($params['regional_market']) && count($params['regional_market']) > 0) {
                $select->where('p.regional_market IN (?)', $params['regional_market']);
            } else {
                $select->where('1=0', 1);
            }
        }

        // dành để lọc staff cho các asm, admin... xem; chỉ hiện thị staff thuộc khu vực được gán
        if (isset($params['list_province_ids']) && $params['list_province_ids']) {
            if (is_array($params['list_province_ids']) && count($params['list_province_ids'])) {
                $select->where('p.regional_market IN (?)', $params['list_province_ids']);
            } elseif (is_numeric($params['list_province_ids']) && intval($params['list_province_ids']) > 0) {
                $select->where('p.regional_market = ?', intval($params['list_province_ids']));
            }
            else
            {
                // neu chua duoc gan khu vuc thi khong duoc xem
                 $select->where('p.regional_market = -1');
            }
        }

        if(isset($params['area_trainer_right']) and $params['area_trainer_right'])
        {
            $QRegionalMarket = new Application_Model_RegionalMarket();

            if (is_array($params['area_trainer_right']) && count($params['area_trainer_right']) > 0) {
                $whereRegionalMarketRight = $QRegionalMarket->getAdapter()->quoteInto('area_id IN (?)', $params['area_trainer_right']);
            } else {
                $whereRegionalMarketRight  = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $params['area_trainer_right']);
            }

            $regional_markets_rights = $QRegionalMarket->fetchAll($whereRegionalMarketRight);
            $tem = array();

            foreach ($regional_markets_rights as $regional_markets_right)
                $tem[] = $regional_markets_right->id;

            if (is_array($tem) && count($tem) > 0)
                $select->where('p.regional_market IN (?)', $tem);
            else
                $select->where('1=0', 1);
        }

        if (isset($params['s_assign']) && $params['s_assign']) {
            $QDeparment = new Application_Model_Department();
            $where = $QDeparment->getAdapter()->quoteInto('name LIKE ?', 'KINH DOANH');
            $department_id = $QDeparment->fetchAll($where)->current()->id;
            $select->where('p.department = ?', $department_id);
        }

        $order_str = '';

        if (isset($params['sort']) and $params['sort']) {
            $collate = ' ';
            $desc = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';

            if ($params['sort'] == 'name'){
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= ' CONCAT(p.firstname, " ",p.lastname) '.$collate . $desc;
            } elseif ( /*$params['sort'] == 'title' || */$params['sort'] == 'department' || $params['sort'] == 'team' ) {
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= $collate . $desc;
            } else {
                $order_str = 'p.`'.$params['sort'] . '` ' . $collate . $desc;
            }

            $select->order(new Zend_Db_Expr($order_str));
        }
        $select->where('p.title <> 375');
        $select->group('p.id');

        if ($limit)
            $select->limitPage($page, $limit);

        if (isset($params['export']) && $params['export'])
            return $select->__toString();

        $result = $db->fetchAll($select);

        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public function UpdatePrintCardStatus($staff_code){
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare('UPDATE staff SET is_print = 1 WHERE FIND_IN_SET(`code`, :staff_code)');
        $stmt->bindParam('staff_code', $staff_code, PDO::PARAM_INT);
        $res = $stmt->execute();
        $stmt->closeCursor();
        return $res;
    }
    public function findStaffid($id){
      return $this->fetchRow($this->select()->where('id = ?',$id));
    }

    public function getTotalSalePgs($params){

        $list_sale_psg = unserialize(LIST_SALE_PGS);

        $db = Zend_Registry::get('db');
        $select  = $db->select();

        $arrCols = array(
            'sum'        => "count(p.id)"
        );

        $select->from(array('p'=> 'staff'), $arrCols);
        $select->joinLeft(array('r' => 'regional_market'),'r.id = p.regional_market',array());

        if(isset($params['area_list']) and $params['area_list']){
            $select->where('r.area_id IN (?)', $params['area_list']);
        }
        elseif(isset($params['area']) and $params['area']){
            $select->where('r.area_id = ?', $params['area']);
        }

        if(isset($params['province']) and $params['province']){
            $select->where('r.id = ?', $params['province']);
        }

        $select->where('p.off_date IS NULL', null);
        $select->where('p.title IN (?)', $list_sale_psg);
        $select->where('p.status = ?', 1);
   
        $resu = $db->fetchRow($select);
        return $resu;
    }

} // end class model
