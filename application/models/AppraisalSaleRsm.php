<?php

class Application_Model_AppraisalSaleRsm extends Zend_Db_Table_Abstract
{
    protected $_name = 'appraisal_sale_rsm';
    protected $_primary = 'id';

    /**
     * @param $staffId
     * @return bool|string
     */
    public function getTitle($staffId)
    {
        $data = $this->fetchAll($this->getAdapter()->quoteInto('staff_id = ?', $staffId))->toArray();
        switch (count($data)) {
            case 0:
                return false;
            case 1:
                return $data[0]['title'];
            default:
                return 'RSM';
        }
    }

    /**
     * @param $staffId
     * @return bool|string
     */
    public function getRegion($staffId)
    {
        $data = $this->fetchAll($this->getAdapter()->quoteInto('staff_id = ?', $staffId))->toArray();
        switch (count($data)) {
            case 0:
                return false;
            default:
                return $data[0]['region'];
        }
    }

    /**
     * @param $staffId
     * @return bool|string
     */
    public function getArea($staffId)
    {
        $data = $this->fetchAll($this->getAdapter()->quoteInto('staff_id = ?', $staffId))->toArray();
        switch (count($data)) {
            case 0:
                return false;
            default:
                return $data[0]['area'];
        }
    }
}