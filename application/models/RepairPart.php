<?php
class Application_Model_RepairPart extends Zend_Db_Table_Abstract
{
    protected $_name = 'repair_part';
	protected $_schema = DATABASE_TRADE;

    public function getAll()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['p' => DATABASE_TRADE.'.repair_part'], [
                         'p.id',
                         'p.category_id',
                         'category_name' => 'c.name'
                     ])
                    ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'p.category_id = c.id', [])
                    ->where('p.status = ?', 1);
        
        $result = $db->fetchAll($select);

        return $result;
	}
	
}