<?php
class Application_Model_Costing extends Zend_Db_Table_Abstract
{
    //protected $_schema = DATABASE_COST;
    protected $_name = 'costing';
    public function fetchPagination($page, $limit, &$total, $params){
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
            'type_approve'              => 't.id',
            'waiting'                   => "GROUP_CONCAT(d.id)",
            'approved'                 => "GROUP_CONCAT(a.type_approve_details_id)",
            'staff_name'                => "GROUP_CONCAT(DISTINCT(CONCAT('',' ',s.lastname)))",
            'company_id'                => 'p.company_id', 
            'category_id'               => 'p.category_id' , 
            'chanel_id'                 => 'p.chanel_id',
            'content'                   => 'p.content',
            'cost_temp'                 => 'p.cost_temp',
            'cost_real'                 => 'p.cost_real',
            'number_change'             => 'p.number_change',
            'created_at'                => 'p.created_at',
            'status'                    => 'p.status',
            'type'                      => 'p.type_approve',
            'company'                   => 'c.name',
            'channel'                   =>  'z.name',
            'category'                  => 'g.name',
            'type_name'                 => 't.name',
            'total_pay'                 => "SUM(m.number_pay)",
            //'total_pay'                 => "SUM(m.number_pay)*COUNT(DISTINCT m.id)/COUNT(m.id) + SUM(m.cantru_xuathang)*COUNT(DISTINCT m.id)/COUNT(m.id)",
            //'cantru_xuathang'           => "SUM(m.cantru_xuathang)*COUNT(DISTINCT m.id)/COUNT(m.id)",
            'lastdate_pay'              => "MAX(m.date_pay)",
            'lastdate_ct'               =>  "MAX(m.date_ct)",
            'number_ct'                 => 'm.number_ct',
            'cantru_xuathang'           => "SUM(m.cantru_xuathang)",
           // 'cantru_xuathang'           => "SUM(m.cantru_xuathang)*COUNT(DISTINCT m.id)/COUNT(m.id)",
            'staff_waiting'            => "GROUP_CONCAT(DISTINCT d.staff_id)",
             'staff_approved'            => "GROUP_CONCAT(DISTINCT i.staff_id)"
        );
         
        $select->from(array('p'=> 'costing'), $arrCols);
        $select->joinLeft(array('m' => 'pay_costing'), 'm.cost_id = p.id', array());
        $select->joinLeft(array('c' => 'company_costing'), 'c.id = p.company_id', array());
        $select->joinLeft(array('z' => 'channel_costing'), 'z.id = p.chanel_id', array());
        $select->joinLeft(array('g' => 'category_costing'), 'g.id = p.category_id', array());
        $select->joinLeft(array('t' => 'type_approve_costing'), 't.id = p.type_approve', array());
        $select->joinLeft(array('d' => 'type_approve_details_costing'), 'd.type_approve_id = t.id', array());
        $select->joinLeft(array('a' => 'approve_costing'), 'a.cost_id = p.id AND a.type_approve_details_id = d.id', array());
        $select->joinLeft(array('i' => 'type_approve_details_costing'), 'i.id = a.type_approve_details_id AND i.type_approve_id = p.type_approve', array());
        $select->joinLeft(array('s' =>DATABASE_CENTER.'.staff'), 's.id = d.staff_id', array());
        $select->where('p.del = ?',0);

        if(!empty($params['company_id'])){
            $select->where('p.company_id = ?',$params['company_id']);
        }

        if(!empty($params['channel_id'])){
            $select->where('p.chanel_id = ?', $params['channel_id']);
        }
        
        if(!empty($params['category_id'])){
            $select->where('p.category_id = ?', $params['category_id']);
        }

        if(!empty($params['type_id'])){
            $select->where('p.type_approve = ?',$params['type_id']);
        }

        if(!empty($params['content'])){
            $select->where('p.content LIKE ?', '%'.$params['content'].'%');
        }
        
        if(!empty($params['number_temp'])){
            $select->where('p.cost_temp = ?', $params['number_temp']);
        }

        if(!empty($params['number_ct'])){
            $select->where('m.number_ct = ?', $params['number_ct']);
        }
        if(!empty($params['number_real'])){
            $select->where('p.cost_real = ?', $params['number_real']);
        }
        
        if(!empty($params['date_ct'])){
            $date_ct = date_create_from_format("d/m/Y", $params['date_ct'])->format("Y-m-d");
            $select->where('m.date_ct = ?', $date_ct);
        }
        if(!empty($params['date_tt'])){
            $date_tt = date_create_from_format("d/m/Y", $params['date_tt'])->format("Y-m-d");
            $select->where('m.date_pay = ?', $date_tt);
        }
        if(!empty($params['ct_xh'])){
            $select->where('m.cantru_xuathang = ?', $params['ct_xh']);
        }


        if(!empty($params['from_date'])){
            $from_date = date_create_from_format("d/m/Y", $params['from_date'])->format("Y-m-d");
            $select->where('p.created_at >= ?', $from_date);
        }

        if(!empty($params['to_date'])){
            $to_date = date_create_from_format("d/m/Y", $params['to_date'])->format("Y-m-d");
            $select->where('p.created_at <= ?', $to_date);
        }

        $select->group('p.id');
        $select->order('p.id DESC');
        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
       
        $total = $db->fetchOne("select FOUND_ROWS()");
         return $result;
    }


    public function exportDetailsCosting($params){
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id',
            'DMonth'                    => "MONTH(p.created_at)",
            'DYear'                     => "YEAR(p.created_at)",
            'company_id'                => 'p.company_id', 
            'category_id'               => 'p.category_id' , 
            'chanel_id'                 => 'p.chanel_id',
            'content'                   => 'p.content',
            'cost_temp'                 => 'p.cost_temp',
            'cost_real'                 => 'p.cost_real',
            'number_change'             => 'p.number_change',
            'created_at'                => 'p.created_at',
            'company'                   => 'c.name',
            'channel'                   =>  'z.name',
            'category'                  => 'g.name',
            'total_pay'                 => "SUM(m.number_pay)*COUNT(DISTINCT m.id)/COUNT(m.id) + SUM(m.cantru_xuathang)*COUNT(DISTINCT m.id)/COUNT(m.id)",
            //'cantru_xuathang'           => "SUM(m.cantru_xuathang)*COUNT(DISTINCT m.id)/COUNT(m.id)",
            'lastdate_pay'              => "MAX(m.date_pay)",
            'lastdate_ct'               =>  "MAX(m.date_ct)",
            'number_ct'                 => 'm.number_ct',
            'cantru_xuathang'           => "SUM(m.cantru_xuathang)*COUNT(DISTINCT m.id)/COUNT(m.id)"
        );
         
        $select->from(array('p'=> 'costing'), $arrCols);
        $select->joinLeft(array('m' => 'pay_costing'), 'm.cost_id = p.id', array());
        $select->joinLeft(array('c' => 'company_costing'), 'c.id = p.company_id', array());
        $select->joinLeft(array('z' => 'channel_costing'), 'z.id = p.chanel_id', array());
        $select->joinLeft(array('g' => 'category_costing'), 'g.id = p.category_id', array());
        $select->where('p.del = ?',0);

        // if(!empty($params['company_id'])){
        //     $select->where('p.company_id = ?',$params['company_id']);
        // }

        // if(!empty($params['channel_id'])){
        //     $select->where('p.chanel_id = ?', $params['channel_id']);
        // }
        
        // if(!empty($params['category_id'])){
        //     $select->where('p.category_id = ?', $params['category_id']);
        // }

        // if(!empty($params['type_id'])){
        //     $select->where('p.type_approve = ?',$params['type_id']);
        // }

        // if(!empty($params['content'])){
        //     $select->where('p.content LIKE ?', '%'.$params['content'].'%');
        // }
        
        // if(!empty($params['number_temp'])){
        //     $select->where('p.cost_temp = ?', $params['number_temp']);
        // }

        // if(!empty($params['number_ct'])){
        //     $select->where('m.number_ct = ?', $params['number_ct']);
        // }
        // if(!empty($params['number_real'])){
        //     $select->where('p.cost_real = ?', $params['number_real']);
        // }
        
        // if(!empty($params['date_ct'])){
        //     $date_ct = date_create_from_format("d/m/Y", $params['date_ct'])->format("Y-m-d");
        //     $select->where('m.date_ct = ?', $date_ct);
        // }
        // if(!empty($params['date_tt'])){
        //     $date_tt = date_create_from_format("d/m/Y", $params['date_tt'])->format("Y-m-d");
        //     $select->where('m.date_pay = ?', $date_tt);
        // }
        // if(!empty($params['ct_xh'])){
        //     $select->where('m.cantru_xuathang = ?', $params['ct_xh']);
        // }

        $select->group(array('DMonth','DYear','p.id'));
        $result = $db->fetchAll($select);
         return $result;
    }
    public function getData($params)
    {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id',
            'company_id'        => 'p.company_id', 
            'date_offer'        => 'p.date_offer',
            'category_id'       => 'p.category_id' , 
            'chanel_id'         => 'p.chanel_id',
            'content'           => 'p.content',
            'cost_temp'         => 'p.cost_temp',
            'cost_real'         => 'p.cost_real',
            'number_change'     => 'p.number_change',
            'created_at'        => 'p.created_at',
            'status'            => 'p.status',
            'type'              => 'p.type_approve',
            'note'              => 'p.note',
            'staff_approved'    => "GROUP_CONCAT(DISTINCT i.staff_id)",
            'date_thanhtoan'    => "MAX(m.date_pay)",
            'date_chungtu'      =>  "MAX(m.date_ct)",
            'number_ct'         => 'm.number_ct',
            'cantru_xuathang'      => 'm.cantru_xuathang',
             'total_pay'                 => "SUM(m.number_pay)*COUNT(DISTINCT m.id)/COUNT(m.id)",
             'not_pay'          => "(SUM(m.number_pay)*COUNT(DISTINCT m.id)/COUNT(m.id) - p.cost_real)",
              'staff_approved' => "GROUP_CONCAT(DISTINCT i.staff_id)",
            'staff_waiting'     => "GROUP_CONCAT(DISTINCT d.staff_id)"

        );
         
        $select->from(array('p'=> 'costing'), $arrCols);
        $select->joinLeft(array('m' => 'pay_costing'), 'm.cost_id = p.id', array());

        $select->joinLeft(array('t' => 'type_approve_costing'), 't.id = p.type_approve', array());
        $select->joinLeft(array('d' => 'type_approve_details_costing'), 'd.type_approve_id = t.id', array());

        $select->joinLeft(array('a' => 'approve_costing'), 'a.cost_id = p.id', array());
        $select->joinLeft(array('i' => 'type_approve_details_costing'), 'i.id = a.type_approve_details_id and i.type_approve_id = p.type_approve', array());
        $select->where('p.id = ?',$params['id']);
        $result = $db->fetchAll($select);
        
        return $result;
    }


     public function getTotalCosting($params)
    {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.category_id',
            'cate_name'        => 'c.name', 
            'DYear'       => "YEAR(p.created_at)" , 
            'DMonth'         => "MONTH(p.created_at)",
            'total'             => "SUM(m.number_pay)"
        );
         
        $select->from(array('p'=> 'costing'), $arrCols);
        $select->joinLeft(array('c' => 'category_costing'), 'c.id = p.category_id', array());
        $select->joinLeft(array('m' => 'pay_costing'), 'm.cost_id = p.id', array());
        if(!empty($params['channel'])){
            $select->where('p.chanel_id = ?',$params['channel']);
        }

        if(!empty($params['to_date']) && !empty($params['from_date'])){
            $select->where('p.created_at >="'.$params['from_date'].'" AND p.created_at <= "'.$params['to_date'].'"');
        }
        if(!empty($params['from_date']) && empty($params['to_date'])){
            $select->where('p.created_at >= ?',$params['from_date']);
        }
        if(!empty($params['to_date']) && empty($params['from_date'])){
           
            $select->where('p.created_at <= ?',$params['to_date']);
        }


        $select->group(array('p.category_id','DYear','DMonth'));
        $result = $db->fetchAll($select);
        return $result;
    }

     public function getDetailsCosting($params)
    {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.chanel_id',
            'chanel_name'        => 'c.name', 
            'DYear'       => "YEAR(p.created_at)" , 
            'DMonth'         => "MONTH(p.created_at)",
            'total'             => "SUM(m.number_pay)"
        );
         
        $select->from(array('p'=> 'costing'), $arrCols);
        $select->joinLeft(array('c' => 'channel_costing'), 'c.id = p.chanel_id', array());
        $select->joinLeft(array('m' => 'pay_costing'), 'm.cost_id = p.id', array());
        // if(!empty($params['channel'])){
        //     $select->where('p.chanel_id = ?',$params['channel']);
        // }

        if(!empty($params['to_date']) && !empty($params['from_date'])){
            $select->where('p.created_at >="'.$params['from_date'].'" AND p.created_at <= "'.$params['to_date'].'"');
        }
        if(!empty($params['from_date']) && empty($params['to_date'])){
           
            $select->where('p.created_at >= ?',$params['from_date']);
        }
        if(!empty($params['to_date']) && empty($params['from_date'])){
           
            
            $select->where('p.created_at <= ?',$params['to_date']);
        }


        $select->group(array('p.chanel_id','DYear','DMonth'));
        $result = $db->fetchAll($select);
       //echo $select; exit;
        return $result;
    }

     public function getDetailsCostingChart($params)
    {
        $db     = Zend_Registry::get("db");
        $select = $db->select();
        $prev_month = date("Y-m-1", strtotime("-1 month") ) ;
        $arrCols = array(
            'p.chanel_id',
            'chanel_name'        => 'c.name', 
            'DYear'       => "YEAR(p.created_at)" , 
            'DMonth'         => "MONTH(p.created_at)",
            'total'             => "SUM(m.number_pay)"
        );
            
        $select->from(array('p'=> 'costing'), $arrCols);
        $select->joinLeft(array('c' => 'channel_costing'), 'c.id = p.chanel_id', array());
        $select->joinLeft(array('m' => 'pay_costing'), 'm.cost_id = p.id', array());
        $select->where('p.created_at >= ?',$prev_month);

        $select->group(array('p.chanel_id','DYear','DMonth'));
        $result = $db->fetchAll($select);
        return $result;
    }
    public function getSellout($params)
    {

        $db     = Zend_Registry::get("db");
        $select = $db->select();
        $arrCols = array(
            'DYear'       => "YEAR(i.timing_date)" , 
            'DMonth'         => "MONTH(i.timing_date)",
            'totalsellout'             => "SUM(i.value)"
        );
            
        $select->from(array('i'=> 'imei_kpi'), $arrCols);
        $select->joinLeft(array('d' => 'channel_dealer'), 'd.d_id = i.distributor_id', array());
        $select->joinLeft(array('c' => 'channel_costing'), 'c.id = d.channel_id', array());
         $select->where(' i.timing_date >= "'.$params['from_date'].'" AND i.timing_date <= "'.$params['to_date'].'"');
        if(!empty($params['channel'])){
            $select->where('c.id = ?',$params['channel']);
        }
        $select->group(array('DYear','DMonth'));
        $result = $db->fetchAll($select);
         //echo  $select; exit;
        return $result;
    }

    public function getSellin($params)
    {

        $db     = Zend_Registry::get("db");
        $select = $db->select();
        $arrCols = array(

            'DYear'       => "YEAR(p.invoice_time)" , 
            'DMonth'         => "MONTH(p.invoice_time)",
            'totalsellin'             => "SUM(p.total)"
        );
        $select->from(array('p'=> WAREHOUSE_DB.'.market'), $arrCols);
        $select->joinLeft(array('d' => 'channel_dealer'), 'd.d_id = p.d_id', array());
        $select->joinLeft(array('c' => 'channel_costing'), 'c.id = d.channel_id', array());
        $select->where('p.cat_id = 11 AND p.invoice_time >= "'.$params['from_date'].'" AND p.invoice_time <= "'.$params['to_date'].'"');
         if(!empty($params['channel'])){
            $select->where('c.id = ?',$params['channel']);
        }
        $select->group(array('DYear','DMonth'));
        $result = $db->fetchAll($select);
        //echo $select; exit;
        return $result;
    }
    public function getCost($params)
    {
        
        $db     = Zend_Registry::get("db");
        $select = $db->select();
        $arrCols = array(

            'DYear'       => "YEAR(p.created_at)" , 
            'DMonth'         => "MONTH(p.created_at)",
            'totalcost'             => "SUM(d.number_pay)"
        );
            
        $select->from(array('p'=> 'costing'), $arrCols);
        $select->joinLeft(array('c' => 'channel_costing'), 'c.id = p.chanel_id', array());
        $select->joinLeft(array('d' => 'pay_costing'), 'd.cost_id = p.id', array());
        $select->where(' p.created_at >= "'.$params['from_date'].'" AND p.created_at <= "'.$params['to_date'].'"');
         if(!empty($params['channel'])){
            $select->where('c.id = ?',$params['channel']);
        }
        $select->group(array('DYear','DMonth'));
        $result = $db->fetchAll($select);
        //echo $select; exit;
        return $result;
    }

    public function getStaff()
    {
        
        $db     = Zend_Registry::get("db");
        $select = $db->select();
        $arrCols = array(
            'p.id',
            'name'         => "CONCAT(p.firstname,' ',p.lastname)"
        );
            
        $select->from(array('p'=> DATABASE_CENTER. '.staff'), $arrCols);
        $select->where('p.id IN (?)',array(103,765,15));
        $result = $db->fetchAll($select);
        //echo $select; exit;
        return $result;
    }


}
