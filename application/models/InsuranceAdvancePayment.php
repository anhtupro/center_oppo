<?php
class Application_Model_InsuranceAdvancePayment extends Zend_Db_Table_Abstract
{
    protected $_name = 'insurance_advance_payment';

    public function fetchPagination($page, $limit, &$total, $params)
    {

        $db = Zend_Registry::get('db');
        $select = $db->select()
        			->from(['i' => 'insurance_advance_payment'], [
        				new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT i.stage'),
        				'stage' => 'i.stage',
        				'i.stage_date',
                        'company_name' => 'c.name',
                        'company_id' => 'i.company_id'
        			])
                    ->joinLeft(['c' => 'company'], 'i.company_id = c.id', [])
        			->group(['stage', 'stage_date', 'company_id'])
        			->order(['i.stage_date DESC', 'i.stage DESC']);
        if ($params['stage']) {
            $select->where('i.stage = ?', $params['stage']);
        }
        if ($params['stage_date']) {
            $select->where('i.stage_date = ?', $params['stage_date']);
        }
        if ($params['company_id']) {
            $select->where('i.company_id = ?', $params['company_id']);
        }
        if ($params['staff_code']) {
            $select->where('i.staff_code = ?', $params['staff_code']);
        }
        $select->limitPage($page, $limit);		
        $list = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $list;			
    }
    public function getAll($params)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
                    ->from(['i' => 'insurance_advance_payment'])
                    ->joinLeft(['c' => 'company'], 'i.company_id = c.id', ['company' => 'c.name'])
                    ->joinLeft(['t' => 'insurance_advance_payment_type'], 'i.insurance_type = t.id', ['insurance_type_name' => 't.name'])
                    ->joinLeft(['st' => 'staff'], 'i.staff_code = st.code', ['staff_name' => 'CONCAT(st.firstname," ", st.lastname)']);

        if ($params['staff_code']) {
            $select->where('i.staff_code = ?', $params['staff_code']);
        }            
        if ($params['stage']) {
            $select->where('i.stage= ?', $params['stage']);
        } 
        if ($params['stage_date']) {
            $select->where('i.stage_date = ?', $params['stage_date']);
        }
        if ($params['company_id']) {
            $select->where('i.company_id = ?', $params['company_id']);
        }

        $result = $db->fetchAll($select);

        return $result;
    }
    public function isExistStage($stage, $stage_date, $company_id)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
                    ->from(['i' => 'insurance_advance_payment'])
                    ->where('i.stage = ?', $stage)
                    ->where('i.stage_date = ?', $stage_date)
                    ->where('i.company_id = ?', $company_id);
        $result = $db->fetchAll($select);

        return $result ? true : false;            
    }
    
    
    
}