<?php
class Application_Model_DuplicatedImeiOppo extends Zend_Db_Table_Abstract
{
	protected $_name = 'duplicated_imei_oppo';
    
	
	public function checkPgRealmeByImei($imei, $params) {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['d' => 'duplicated_imei_oppo'], [
                 'd.*'
            ])
            ->where('d.imei_realme = ?', $imei)
            ->where('d.date >= ?', $params['from_date'])
            ->where('d.date <= ?', $params['to_date']);

        $result = $db->fetchRow($select);

        return $result;
    }

    public function getDuplicatedImeiRealme($imei) {
        $db = Zend_Registry::get('db');
        $sql = "SELECT * FROM duplicated_imei_oppo d WHERE d.imei_realme =  :imei";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('imei', $imei, PDO::PARAM_INT);
        $stmt->execute();
        $getData = $stmt->fetch();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $getData;
    }
    
    public function getStoreRmFromOppo($id_store_op){
        $db = Zend_Registry::get('db');
        $sql = "SELECT s.id_rm FROM realme_hr.store_mapping s WHERE s.id_oppo = :id_oppo";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('id_oppo', $id_store_op, PDO::PARAM_INT);
        $stmt->execute();
        $getData = $stmt->fetch();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $getData['id_rm'];
    }

}                                                      
