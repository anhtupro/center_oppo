<?php

class Application_Model_CheckShopEntireDetail extends Zend_Db_Table_Abstract
{
    protected $_name = 'check_shop_entire_detail';
    protected $_schema = DATABASE_TRADE;

    public function getCheckShopDetail($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['a' => DATABASE_TRADE.'.check_shop_entire'], [
                         'category_name' => 'c.name',
                         'd.quantity',
                         'd.position'
                     ])
        ->joinLeft(['d' => DATABASE_TRADE.'.check_shop_entire_detail'], 'a.id = d.check_shop_id', [])
        ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'd.category_id = c.id', []);

        if ($params['check_shop_id']) {
            $select->where('a.id = ?', $params['check_shop_id']);
        }

        $result = $db->fetchAll($select);

        return $result;
    }
}