<?php
class Application_Model_Shift extends Zend_Db_Table_Abstract
{
	protected $_name = 'shift';

    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        if (isset($params['name']) and $params['name'])
            $select->where('p.name LIKE ?', '%'.$params['name'].'%');

        $select->order('p.name', 'COLLATE utf8_unicode_ci ASC');

        if ($limit)
        	$select->limitPage($page, $limit);

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if ($result === false) {

            $data = $this->fetchAll();

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = array(
						'name'   => $item->name,
						'start'  => $item->start,
						'end'    => $item->end,
						'status' => $item->status,
                    	);
                }
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }

    /**
     * @author [hac.tran] <[<email address>]>
     * [Lấy ca làm việc theo chức danh]
     * @param  [type] $title [description]
     * @return [type]        [description]
     */
    function getShiftByTitle($title){
        $db           = Zend_Registry::get('db');
        $select       = $db->select()
            ->from(array('a'=>'shift_object'),array('a.*'))
            ->where('object = ?',$title)
        ;
        
        $shift_object = $db->fetchRow($select);


        if($shift_object AND $shift_object['shifts']){
            $shift_ids = explode(',',$shift_object['shifts']);

            if(is_array($shift_ids) and count($shift_ids) > 0){
                $select_shift = $db->select()
                    ->from(array('a'=>$this->_name),array('a.*'))
                    ->where('id IN (?)',$shift_ids);

                $data = $db->fetchAll($select_shift);
         
                $result = array();
                if($data){
                    foreach ($data as $item){
                        $result[$item['id']] = $item;
                    }
                    return $result;
                }


            }
        }

        return $this->get_cache();
    }
}                                                      

