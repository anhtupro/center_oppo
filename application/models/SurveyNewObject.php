<?php
class Application_Model_SurveyNewObject extends Zend_Db_Table_Abstract
{
    protected $_name = 'survey_new_object';

    public function getTitle($survey_id)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->distinct()
            ->from(array('so' => $this->_name), array('so.object_id'))
            ->where('so.survey_id = ?', $survey_id);

        $result = $db->fetchCol($select);

        return $result;
    }
}
