<?php
class Application_Model_DestructionDetails extends Zend_Db_Table_Abstract
{
    protected $_name = 'destruction_details';
    protected $_schema = DATABASE_TRADE;
    
    public function getDestructionDetails($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.destruction_id", 
            "p.category_id", 
            "p.quantity", 
            "type_title" => "GROUP_CONCAT(DISTINCT type.title)", 
            "tp_id" => "GROUP_CONCAT(DISTINCT tp.tp_id)", 
            "category_name" => "c.name", 
            "p.imei_sn",
            "file_url"  => "GROUP_CONCAT(DISTINCT file.url)",
            "file_contructor"  => "GROUP_CONCAT(DISTINCT file_contructor.url)",
            "file_accept"  => "GROUP_CONCAT(DISTINCT file_accept.url)",
            "file_document"  => "GROUP_CONCAT(DISTINCT file_document.url)",
            "p.note",
            "p.contructors_id",
            "p.price",
             "p.price_final",
            "p.destruction_date",
        );

        $select->from(array('p' => DATABASE_TRADE.'.destruction_details'), $arrCols);
        $select->joinLeft(array('c' => DATABASE_TRADE.'.category'), 'c.id = p.category_id', array());
        $select->joinLeft(array('t' => DATABASE_TRADE.'.destruction_details_type'), 't.destruction_details_id = p.id', array());
        $select->joinLeft(array('type' => DATABASE_TRADE.'.destruction_type'), 'type.id = t.destruction_type_id', array());
        $select->joinLeft(array('tp' => DATABASE_TRADE.'.destruction_details_tp'), 'tp.destruction_details_id = p.id', array());
        $select->joinLeft(array('file' => DATABASE_TRADE.'.destruction_details_file'), 'file.destruction_details_id = p.id AND file.type = 1', array());
        $select->joinLeft(array('file_contructor' => DATABASE_TRADE.'.destruction_details_file'), 'file_contructor.destruction_details_id = p.id AND file_contructor.type = 2', array());
        $select->joinLeft(array('file_accept' => DATABASE_TRADE.'.destruction_details_file'), 'file_accept.destruction_details_id = p.id AND file_accept.type = 3', array());
        $select->joinLeft(array('file_document' => DATABASE_TRADE.'.destruction_details_file'), 'file_document.destruction_details_id = p.id AND file_document.type = 4', array());
        
        $select->where('p.destruction_id = ?', $params['id']);
        $select->group('p.id');
        
        $result = $db->fetchAll($select);

        return $result;
    }


    public function getDestructionDetailsTp($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "t.tp_id"
        );

        $select->from(array('p' => DATABASE_TRADE.'.destruction_details'), $arrCols);
        $select->joinLeft(array('t' => DATABASE_TRADE.'.destruction_details_tp'), 't.destruction_details_id = p.id', array());
        $select->where('p.destruction_id = ?', $params['id']);
        $result = $db->fetchAll($select);
        return $result;
    }
    

    public function getContract($params){
         $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "store"    => "s.name",
            "category"      => "c.name",
            "p.id", 
            "p.destruction_id", 
            "p.category_id", 
            "final_price"      => "p.price_final",
            "p.quantity",
            "p.contract_id"
        );

        $select->from(array('p' => DATABASE_TRADE.'.destruction_details'), $arrCols);
         $select->joinLeft(array('a' => DATABASE_TRADE.'.destruction'), 'a.id = p.destruction_id', array());
        $select->joinLeft(array('c' => DATABASE_TRADE.'.category'), 'c.id = p.category_id', array());
         $select->joinLeft(array('s' => 'store'), 's.id = a.store_id', array());
        
        $select->where('p.contructors_id = ?', $params['contractor_id']);
        $select->where('a.id IS NOT NULL');
        $result = $db->fetchAll($select);
        return $result;
    }
}