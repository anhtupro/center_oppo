<?php
class Application_Model_ProductInventoryArea extends Zend_Db_Table_Abstract{
  protected $_name = 'product_inventory_by_area';

  public function fetchPagination($page, $limit, &$total, $params){
    // public function fetchPagination(){
          $db = Zend_Registry::get('db');
          $select = $db->select();
          $select
  	        ->from(
  	        	array('p' =>  'product_inventory_by_area'),
                 	array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*')
             );
          $select->joinLeft(
            array('a'=>'area'),
            'p.area_id = a.id',
            array('area'=>'a.name')
          );
          $select->joinLeft(
            array('s'=>'staff'),
            'p.created_by = s.id',
            array('staff_title'=>'s.title', 'department'=>'s.department', 'staff_create' => 'CONCAT(s.firstname, \' \', s.lastname)')
          );
          $select->joinLeft(
            array('t'=>'team'),
            'p.department = t.id',
            array('staff_department'=>'t.name')
          );
  	    // ======= FILTER PARAMS
  		if( isset($params["ma_thiet_bi"]) AND $params["ma_thiet_bi"]){
              $select->where('p.ma_thiet_bi = ?', $params['ma_thiet_bi']);
  		}
  		if( isset($params["ten_thiet_bi"]) AND $params["ten_thiet_bi"]){
              $select->where('p.ten_thiet_bi LIKE ?', '%'. $params['ten_thiet_bi'].'%');
  		}
  		if( isset($params["area_id"]) AND $params["area_id"]){
              $select->where('p.area_id = ?', $params['area_id']);
  		}
  		if( isset($params["nhan_vien_su_dung_nhan_ban_giao"]) AND $params["nhan_vien_su_dung_nhan_ban_giao"]){
              $select->where('p.nhan_vien_su_dung_nhan_ban_giao = ?', $params['nhan_vien_su_dung_nhan_ban_giao']);
  		}
  		if( isset($params["so_bien_ban_ban_giao"]) AND $params["so_bien_ban_ban_giao"]){
              $select->where('p.so_bien_ban_ban_giao = ?', $params['so_bien_ban_ban_giao']);
  		}
  		if( isset($params["tinh_trang_thiet_bi"]) AND $params["tinh_trang_thiet_bi"]){
              $select->where('p.tinh_trang_thiet_bi = ?', $params['tinh_trang_thiet_bi']);
  		}
  		if( isset($params["ngay_mua_from"]) AND $params["ngay_mua_from"]){
              $select->where('p.ngay_mua_from = ?', $params['ngay_mua_from']);
  		}
  		if( isset($params["ngay_mua_to"]) AND $params["ngay_mua_to"]){
              $select->where('p.ngay_mua_to = ?', $params['ngay_mua_to']);
  		}
  		if( isset($params["ngay_nhan_ban_giao_from"]) AND $params["ngay_nhan_ban_giao_from"]){
              $select->where('p.ngay_nhan_ban_giao_from = ?', $params['ngay_nhan_ban_giao_from']);
  		}
  		if( isset($params["ngay_nhan_ban_giao_to"]) AND $params["ngay_nhan_ban_giao_to"]){
              $select->where('p.ngay_nhan_ban_giao_to = ?', $params['ngay_nhan_ban_giao_to']);
  		}
                if( isset($params["area"]) AND $params["area"]){
              $select->where('p.area_id = ?', $params['area']);
  		}
                if( isset($params["list_regions"]) AND $params["list_regions"]){
                    $list_regions = $params['list_regions'];
                   
                    $str_list_regions = implode(',',$list_regions);
                    $select->where("p.area_id IN ($str_list_regions) ",'');
  		}
      if (isset($params["staff_code"]) AND $params["staff_code"]) {
        $select->where('s.code =?', $params["staff_code"]);
      }
  	    // ======= PAGINATION
  	    $select->where('p.del != ?',1);
      $select->where('p.lock != ?',1);
            
            if( isset($params["department"]) AND $params["department"] AND ! in_array($params["department"], array(153, 154))  ){
                 $select->where('p.department = ?',$params["department"]);
            }
		
            $select->order('p.id DESC');
        	// $select->order('p.created_at DESC');
             if( !isset($params["export"])){
                 $select->limitPage($page, $limit);
             }
            
  	    $data = $db->fetchAll($select);
        	$total = $db->fetchOne("select FOUND_ROWS()");
  	    return $data;
  }
}