<?php

class Application_Model_AppraisalOfficeMemberRoot extends Zend_Db_Table_Abstract
{
    protected $_name = 'appraisal_office_member_root';

    
    function fetchPagination($page, $limit, &$total, $params){
        
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT s.id'),
            'fullname_from' => "CONCAT(s.firstname, ' ', s.lastname)",
            'title_from' => "t.name",
            'joined_at_from' => "s.joined_at",
            'fullname_to' => "CONCAT(s2.firstname, ' ', s2.lastname)",
            'to_id' => 's2.id',
            'joined_at_to' => "s2.joined_at",
            'title_to_id' => "s2.title",
            'title_to' => "t2.name",
            'p.is_prd'
        );

        $select->from(array('p' => 'appraisal_office_to_do_root'), $arrCols);
        $select->joinLeft(array('s' => 'staff'), 's.id = p.from_staff', array());
        $select->joinLeft(array('t' => 'team'), 't.id = s.title', array());
        $select->joinLeft(array('s2' => 'staff'), 's2.id = p.to_staff', array());
        $select->joinLeft(array('t2' => 'team'), 't2.id = s2.title', array());
        
        if(!empty($params['from_name'])){
            $select->where("CONCAT(s.firstname, ' ', s.lastname) LIKE ?", "%".$params['from_name']."%");
        }
        
        if(!empty($params['from_code'])){
            $select->where("s.code LIKE ?", "%".$params['from_code']."%");
        }
        
        if(!empty($params['to_name'])){
            $select->where("CONCAT(s2.firstname, ' ', s2.lastname) LIKE ?", "%".$params['to_name']."%");
        }
        
        if(!empty($params['to_code'])){
            $select->where("s2.code LIKE ?", "%".$params['to_code']."%");
        }
        
        if(!empty($params['department'])){
            $select->where("s2.department IN (?)", $params['department']);
        }
        
        if(!empty($params['team'])){
            $select->where("s2.team IN (?)", $params['team']);
        }
        
        if(!empty($params['title'])){
            $select->where("s2.title IN (?)", $params['title']);
        }
        
        if(!empty($params['staff_head_id']) AND $params['is_admin'] != 1){
            $select->where('p.from_staff = ?', $params['staff_head_id']);
        }
        
        $select->where('p.is_del IS NULL OR p.is_del = 0', NULL);
        $select->where('s2.off_date IS NULL', NULL);
        $select->where('p.from_staff <> p.to_staff', NULL);
        $select->order(['s2.department ASC','s.id ASC']);
        
        
        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
    
    public function updateInitRatioByField($fieldId)
    {
        $QField = new Application_Model_AppraisalOfficeFieldRoot();
        $field = $QField->fetchRow($QField->getAdapter()->quoteInto('aof_id = ? and aof_is_deleted = 0', $fieldId));
        if(!$field) {
            return;
        }
        $memberId = $field['aom_id'];
        $fields = $QField->fetchAll($QField->getAdapter()->quoteInto('aom_id = ? and aof_is_deleted = 0', $memberId))->toArray();
        $fieldIds = [];
        foreach ($fields as $field) {
            $fieldIds[] = $field['aof_id'];
        }
        $QTask = new Application_Model_AppraisalOfficeTaskRoot();
        $tasks = $QTask->fetchAll($QTask->getAdapter()->quoteInto('aof_id IN (?) and aot_is_deleted = 0', $fieldIds))->toArray();
        $ratio = 0;
        foreach ($tasks as $task) {
            $ratio += intval($task['aot_ratio']);
        }
        $userId = Zend_Auth::getInstance()->getStorage()->read()->id;
        $this->update([
            'aom_updated_at' => date('Y-m-d H:i:s'),
            'aom_updated_by' => $userId,
            'aom_init_ratio' => $ratio
        ], $this->getAdapter()->quoteInto('aom_id = ? and aom_is_deleted = 0', $memberId));
    }
    
    public function getAllMember()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.aom_id', 
            'fullname' => "CONCAT(s.firstname, ' ',s.lastname)", 
            's.code',  
            'p.aom_is_deleted',
        );

        $select->from(array('p' => 'appraisal_office_member_root'), $arrCols);
        $select->joinLeft(array('s' => 'staff'), 's.id = p.aom_staff_id', array());

        $select->where('p.aom_is_deleted = 0 AND s.off_date IS NULL', NULL);
        
        $result = $db->fetchAll($select);
        
        $data = [];
        foreach($result as $key=>$value){
            $data[$value['code']] = $value['aom_id'];
        }

        return $data;
    }
    
}