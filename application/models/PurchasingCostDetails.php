<?php
class Application_Model_PurchasingCostDetails extends Zend_Db_Table_Abstract
{
    protected $_name = 'purchasing_cost_details';


    
    function getListCostPrint($params){

        $db = Zend_Registry::get('db');

        $col = array(
                'p.*',  
                'product_name' => 'pc.name',
                'supplier_name' => 'sup.title',
            
        );

        $select = $db->select()->from(array('p' => $this->_name), $col);
        $select->join(array('pcost'=>'purchasing_cost'), 'p.pc_id = pcost.id', array());   
        $select->joinleft(array('pc'=>'pmodel_code'), 'p.product_id = pc.id', array());      
        $select->joinleft(array('sup'=>'supplier'), 'p.supplier_id = sup.id', array());     
        
        if(!empty($params['sn'])){
            $select->where('pcost.sn = ?', $params['sn']);
        }
 
        $result = $db->fetchAll($select);

        return $result;
    }
    
}