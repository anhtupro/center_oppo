<?php
class Application_Model_NotificationPgs extends Zend_Db_Table_Abstract
{
	protected $_name = 'notification_pgs';

    public function getNotificationPgs_bk($params){

    	$db     = Zend_Registry::get("db");
        
        //Child Child
        $select_child = $db->select();
        $col = array(
    	    'p.staff_id',
    	    'p.date',
    	    'result'  => 'MAX(p.result)',
            'p.area'
    	);
        $select_child->from(array('p'=> 'sp_assessment_temp_date'), $col);
        $select_child->where('p.date <> CURRENT_DATE ()', NULL);
        $select_child->group('p.staff_id');
        
    	//Child
    	$select = $db->select();
    	    
    	$arrCols = array(
    	    'p.staff_id',
    	    'p.date',
    	    'p.firstname',
    	    'p.lastname',
    	    'p.photo',
    	    'p.result',
    	    'up_star' => 'CASE WHEN (p.result > s.result) THEN 1 ELSE 0 END',
			'p.area'
    	);
    	
    	$select->from(array('p'=> 'sp_assessment_temp_date'), $arrCols);
        $select->joinLeft(array('s'=> $select_child), 's.staff_id = p.staff_id', NULL);
    	$select->where('p.date = CURRENT_DATE()', NULL);
    	
    	$select->group('p.staff_id');

    	//END Child

    	//Main
    	$db     = Zend_Registry::get("db");
    	$select_main = $db->select();
    	    
    	$arrCols = array(
    	    'p.staff_id',
    	    'p.date',
    	    'p.firstname',
    	    'p.lastname',
    	    'p.photo',
    	    'p.result',
    	    'p.up_star',
    	    'p.area',
    	    'n.status'
    	);
    	
    	$select_main->from(array('p'=> $select), $arrCols);
    	$select_main->joinLeft(array('n' => 'notification_pgs'), 'n.staff_id = p.staff_id AND n.date = p.date AND n.star = p.result AND n.staff_id_read ='.$params['staff_id_read'], array());
    	$select_main->where('p.up_star = ?', 1);
    	$select_main->where('p.area = ?', $params['area']);
    	$select_main->where('n.status IS NULL', NULL);
    	
    	$result  = $db->fetchAll($select_main);

        if($_GET['dev'] == 5){
            echo $select_main;exit;
        }
    	//Main
    	
    	return $result;
    }
    
    public function getNotificationPgs($params){

    	$db     = Zend_Registry::get("db");
    	$select_main = $db->select();
    	    
    	$arrCols = array(
    	    'p.staff_id',
    	    'date' => 'DATE(p.created_at)',
    	    's.firstname',
    	    's.lastname',
    	    's.photo',
    	    'p.result',
    	    'up_star' => 'p.status',
    	    'area' => 'a.name',
    	    'n.status'
    	);
    	
    	$select_main->from(array('p'=> 'sp_assessment_approve'), $arrCols);
    	$select_main->joinLeft(array('s' => 'staff'), 'p.staff_id = s.id', array());
        $select_main->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());
        $select_main->joinLeft(array('a' => 'area'), 'a.id = r.area_id', array());
        $select_main->joinLeft(array('n' => 'notification_pgs'), 'n.staff_id = p.staff_id AND n.date = p.confirmed_at AND n.star = p.result AND n.staff_id_read ='.$params['staff_id_read'], array());
        
        $select_main->where('p.confirmed_at = DATE(NOW())', NULL);
    	$select_main->where('a.name = ?', $params['area']);
    	$select_main->where('n.status IS NULL', NULL);
    	
    	$result  = $db->fetchAll($select_main);

        if($_GET['dev'] == 5){
            echo $select_main;exit;
        }
    	//Main
    	
    	return $result;
    }

    public function getNotificationMonth($params){
        //Main
        $db     = Zend_Registry::get("db");
        $select = $db->select();
            
        $arrCols = array(
            'p.staff_id',
            'p.point',
            'p.star',
            'p.month',
            'p.year',
            'p.status'
        );
        
        $select->from(array('p'=> 'sp_assessment_month'), $arrCols);
        
        $select->where('p.staff_id = ?', $params['staff_id']);
        //$select->where('p.month = ?', date('m'));
        $select->where('p.year = ?', date('Y'));
        $select->where('p.status = ?', 0);
        $result  = $db->fetchRow($select);
        //Main
        
        return $result;
    }
}