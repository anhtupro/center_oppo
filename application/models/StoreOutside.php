<?php

class Application_Model_StoreOutside extends Zend_Db_Table_Abstract
{
    protected $_name = 'store_outside';
    protected $_schema = DATABASE_TRADE;

    public function fetchPagination($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['s' => DATABASE_TRADE . '.store_outside'], [
                new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT s.id'),
                'store_name' => 's.name',
                'store_id' => 's.id',
                'last_date_check_shop' => 'c.created_at'
            ])

            ->joinLeft(['p' => 'regional_market'], 's.regional_market = p.id', [])
            ->joinLeft(['d' => 'regional_market'], 's.district = d.id', [])
            ->joinLeft(['c' => DATABASE_TRADE . '.check_shop_outside'], 's.id = c.store_id AND is_deleted = 0 AND c.created_at = (SELECT max(created_at) FROM trade_marketing.check_shop_outside WHERE store_id = s.id)', [])
            ->where('s.del = 0');

        if ($params['store_name']) {
            $select->where('s.name LIKE (?)', '%' . $params['store_name'] . '%');
        }

        if ($params['store_id']) {
            $select->where('s.id = ?', $params['store_id']);
        }

        if ($params['regional_market']) {
            $select->where('p.id = ?', $params['regional_market']);
        }

        if ($params['area_id']) {
            $select->where('p.area_id = ?', $params['area_id']);
        }

        if ($params['district']) {
            $select->where('d.id = ?', $params['district']);
        }
        if ($params['area_list']) {
            $select->where('p.area_id IN (?)', $params['area_list']);
        }

        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    public function getStoreInfo($id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['s' => DATABASE_TRADE . '.store_outside'], [
                'store_name' => 's.name',
                'district_name' => 'd.name',
                'area_name' => 'a.name'
            ])
            ->joinLeft(['d' => 'regional_market'], 's.district = d.id', [])
            ->joinLeft(['r' => 'regional_market'], 's.regional_market= r.id', [])
            ->joinLeft(['a' => 'area'], 'r.area_id = a.id', [])
            ->where('s.id = ?', $id);

        $result = $db->fetchRow($select);

        return $result;
    }

    public function getListStoreOutsideMap($params)
    {
        $categoryIdBrand = [
          '43' => 956,
          '2' => 957,
            '1' => 958,
            '803' => 959
        ];


        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['s' => DATABASE_TRADE.'.store_outside'], [
                'store_id' => 's.id',
                'latitude' => 'a.latitude',
                'longitude' => 'a.longitude',
                'checkshop_id' => 'a.id',
                'store_type' => 's.store_type'

            ])
            ->joinLeft(['a' => DATABASE_TRADE.'.check_shop_outside'], "s.id = a.store_id AND a.created_at = (SELECT MAX(created_at) FROM trade_marketing.check_shop_outside WHERE store_id = a.store_id)", [])
            ->joinLeft(['r' => 'regional_market'], 's.regional_market = r.id', [])
            ->where('a.latitude IS NOT NULL')
            ->where('a.longitude IS NOT NULL')
            ->where("a.latitude <> '' ")
            ->where("a.longitude <> '' ")
            ->group('s.id');

//        if ($params['category_id']) {
//            $select->join(['d' => DATABASE_TRADE.'.check_shop_outside_detail_brand'], 'a.id = d.checkshop_id AND d.category_id = '.$params['category_id'], []);
//            $select->where('d.category_id = ?', $params['category_id']);
//            $select->where('d.quantity > 0 ');
//        }

        if ($params['category_id']) {
            if ($categoryIdBrand[$params['category_id']]) {
                $select->joinLeft(['detail' => DATABASE_TRADE.'.check_shop_outside_detail_brand'], 'a.id = detail.checkshop_id AND detail.quantity > 0 AND detail.category_id = ' . $categoryIdBrand[$params['category_id']], ['brands' => "GROUP_CONCAT(detail.brand_id)"]);
            }
        }

        if ($params['area_list']) {
            if ($params['area_id']) {
                $select->where('r.area_id = ?', $params['area_id']);
            } else {
                $select->where('r.area_id IN (?)', $params['area_list']);
            }
        }

        if(($params['regional_market'])){
            $select->where('r.id = ?', $params['regional_market']);
        }

        if(($params['district'])){
            $select->where('s.district = ?', $params['district']);
        }

//        if ($params['staff_id']) {
//            $select->where('v.staff_id = ?', $params['staff_id']);
//        }

        if(($params['store_name'])){
            $select->where('s.name LIKE ?', '%'.$params['store_name'].'%');
        }

//        $select->limit(2000);

        $result = $db->fetchAll($select);

        for ($i = 0; $i < count($result); ++$i) {
            $result[$i] ['brands'] = explode(',', $result[$i]['brands']);
        }

        return $result;
    }


    public function getListStoreMap($params)
    {

        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['s' => 'store'], [
                'store_id' => 's.id',
                'latitude' => 'a.latitude',
                'longitude' => 'a.longitude',
                'checkshop_id' => 'a.id',
                'store_type' => 's.store_type'
            ])
//            ->joinLeft(['a' => DATABASE_TRADE.'.app_checkshop'], "s.id = a.store_id AND a.updated_at = (SELECT MAX(updated_at) FROM trade_marketing.app_checkshop WHERE store_id = a.store_id)", [])
            ->joinLeft(['a' => DATABASE_TRADE.'.app_checkshop'], 'a.store_id = s.id AND a.is_last = 1', [])
            ->joinLeft(['r' => 'regional_market'], 's.regional_market = r.id', [])
            ->joinLeft(['area' => 'area'], 'r.area_id = area.id', [])
            ->joinLeft(['di' => WAREHOUSE_DB.'.distributor'], 's.d_id = di.id', [])
            ->joinLeft(['l' => 'dealer_loyalty'], 'l.dealer_id = IF((di.parent IS NULL OR di.parent = 0), di.id, di.parent) AND l.is_last = 1', [])
            ->where('a.latitude IS NOT NULL')
            ->where('a.longitude IS NOT NULL')
            ->where("a.latitude <> '' ")
            ->where("a.longitude <> '' ")
            ->where('s.del IS NULL OR s.del = 0');

//        if ($params['category_id']) {
//            $select->join(['d' => DATABASE_TRADE.'.app_checkshop_detail'], 'a.id = d.checkshop_id AND d.category_id = '.$params['category_id'], []);
//            $select->where('d.category_id = ?', $params['category_id']);
//            $select->where('d.quantity > 0 ');
//        }
        if ($params['category_id']) {
            $select->joinLeft(['detail' => DATABASE_TRADE.'.app_checkshop_detail'], 'a.id = detail.checkshop_id AND detail.category_id = ' . $params['category_id'] , ['oppo_brand' => 'IF(detail.quantity > 0, 1, NULL)'  ]); // kiểm tra shop có biển hiệu alu không

            if ($params['brand_id']) {
                $stringBrand = implode(',', $params['brand_id']);

                $select->joinLeft(['db' => DATABASE_TRADE.'.app_checkshop_detail_brand'], 'a.id = db.checkshop_id AND db.category_id = ' . $params['category_id'] . ' AND db.quantity > 0  AND db.brand_id IN (' . $stringBrand . ')' , ['other_brand' => "GROUP_CONCAT(db.brand_id)"]);

//                $select->joinLeft(['c' => DATABASE_TRADE.'.category'], 'c.parent_id = ' . $params['category_id'], []);
//                $select->joinLeft(['db' => DATABASE_TRADE.'.app_checkshop_detail_brand'], 'a.id = db.checkshop_id AND db.quantity > 0 AND db.category_id = c.id AND db.brand_id IN (' . $stringBrand . ')' , ['other_brand' => "GROUP_CONCAT(db.brand_id)"]);
                $select->group('s.id');
            }
        }



        if ($params['area_list']) {
            if ($params['area_id']) {
                $select->where('area.id = ?', $params['area_id']);
            } else {
                $select->where('area.id IN (?)', $params['area_list']);
            }
        }

        if(($params['regional_market'])){
            $select->where('r.id = ?', $params['regional_market']);
        }

        if(($params['district'])){
            $select->where('s.district = ?', $params['district']);
        }

        if ($params['staff_id']) {
            $select->joinLeft(['v' => 'v_store_staff_leader_log'], 's.id = v.store_id AND v.released_at IS NULL v.is_leader = 1', []);
            $select->where('v.staff_id = ?', $params['staff_id']);
        }

        if(($params['store_name'])){
            $select->where('s.name LIKE ?', '%'.$params['store_name'].'%');
        }

        if ((isset($params['is_ka']) && $params['is_ka'] === "0") || !empty($params['is_ka'])){


            if(isset($params['is_ka']) && $params['is_ka'] === "0"){
                $select->where('l.loyalty_plan_id IS NOT NULL', NULL);
                $select->where('di.is_ka = ?', 0);
            }

            if(isset($params['is_ka']) && $params['is_ka'] === "1"){
                $select->where('di.is_ka = ?', 1);
            }

            if(isset($params['is_ka']) && $params['is_ka'] === "2"){
                $select->where('l.loyalty_plan_id IS NULL', NULL);
                $select->where('di.is_ka = ?', 0);
            }
        }

        if(!empty($params['is_ka_details'])){

            if(isset($params['is_ka']) && $params['is_ka'] === "0"){
                $select->where('l.loyalty_plan_id = ?', $params['is_ka_details']);
            }

            if(isset($params['is_ka']) && $params['is_ka'] === "1"){
                $select->where('IF(di.parent = 0, di.id, di.parent) = ?', $params['is_ka_details']);
            }

        }

//        $select->limit(500);

        $result = $db->fetchAll($select);

        for ($i = 0; $i < count($result); ++$i) {
            $stringBrand = $result[$i]['oppo_brand'] . ','. $result[$i]['other_brand'];
            $stringBrand =  trim($stringBrand, ',');
            $result[$i] ['brands'] = explode(',', $stringBrand);
        }

        return $result;
    }

}    