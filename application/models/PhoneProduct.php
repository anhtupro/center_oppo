<?php

class Application_Model_PhoneProduct extends Zend_Db_Table_Abstract
{
    protected $_name = 'phone_product';
    protected $_schema = DATABASE_TRADE;

    public function getProductChangePicture($stage_id)
    {

        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['a' => DATABASE_TRADE.'.change_picture_assign_phone'], [
                         'p.*'
                     ])
                ->joinLeft(['p' => DATABASE_TRADE.'.phone_product'], 'p.id = a.product_id', []);

        if ($stage_id) {
            $select->where('a.stage_id = ?', $stage_id);
        }
     
        $result = $db->fetchAll($select);
        
        return $result;

    }
}