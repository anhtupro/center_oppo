<?php
class Application_Model_TrainerCourseDetail extends Zend_Db_Table_Abstract
{
    protected $_name = 'trainer_course_detail';

    function fetchPagination($page, $limit, &$total, $params){

        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total  = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    function getStaffAssign($course_id)
    {
        $db = Zend_Registry::get('db');

        $select = $db->select()->from(array('p' => $this->_name),array('*'));

        $select->where('p.del = ? OR p.del IS NULL', 0);

        $select->where('p.course_id = ? ', $course_id);

        $result = $db->fetchAll($select);

        $arrayNewStaff  = array();

        $arrayOldStaff = array();

        $arrayIDs      = array();

        foreach ($result as $key => $value) {
            
            if(isset($value['staff_id']) and $value['staff_id'])
            {
                $arrayOldStaff[] = $value['staff_id'];
            }

            if(isset($value['new_staff_id']) and $value['new_staff_id'])
            {
                $arrayNewStaff[] = $value['new_staff_id'];
            }

            if(isset($value['id']) and $value['id'])
            {
                $arrayIDs[] = $value['id'];
            }

        }

        return array('new'=>$arrayNewStaff , 'old'=>$arrayOldStaff,'ids'=>$arrayIDs);;
    }

    function getInfoStaffAssign($arrayIDsCourseDetail)
    {
        $cache = Zend_Registry::get('cache');
        $cache->remove('staff_cache');
        $cache->remove('staff_all_cache');
        $cache->remove('staff_training_all_cache');
        $cache->remove('staff_training_cache');

        $QStaff              = new Application_Model_Staff();
        $QStaffTraining      = new Application_Model_StaffTraining();
        $allCachedStaff      = $QStaff->get_all_cache();
        $cachedStaffTraining = $QStaffTraining->get_cache();

        $db = Zend_Registry::get('db');

        $arrayResult         = array();

        foreach ($arrayIDsCourseDetail as $key => $value) {

            $arrayResult['id'][] = $value;

            $select = $db->select()->from(array('p' => $this->_name),array('*'));

            $select->where('p.id = ?', $value);

            $select->where('p.del = ? OR p.del IS NULL',0);
 
            $rowFetch   = $db->fetchRow($select);
            if($rowFetch){
                $arrayResult['name'][] = (isset($rowFetch['new_staff_id']) and  $rowFetch['new_staff_id']) ? $cachedStaffTraining[$rowFetch['new_staff_id']] : $allCachedStaff[$rowFetch['staff_id']]['name'] ;

                $arrayResult['code'][] = (isset($rowFetch['new_staff_id']) and  $rowFetch['new_staff_id']) ? "NHÂN VIÊN MỚI" : $allCachedStaff[$rowFetch['staff_id']]['code'] ;

                $arrayResult['new_staff'][] = (isset($rowFetch['new_staff_id']) and  $rowFetch['new_staff_id']) ? 1 : 0 ;
            }
        }

        return $arrayResult;
    }

    function getStaffNew($params){
        $db = Zend_Registry::get('db');
        
        $select = $db->select()->from(array('p' => $this->_name),array(''));
        $select->join(array('s' => 'staff_training'), 's.id = p.new_staff_id', array('s.*'));
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array('r.area_id'));
        $select->joinLeft(array('staff' => 'staff'), 'staff.ID_number = s.cmnd', array(''));
		
		$select->where('s.del <> 1 OR s.del IS NULL', NULL);

        if(isset($params['course_id']) and $params['course_id']){
            $select->where('p.course_id = ?', $params['course_id']);
        }

        $result = $db->fetchAll($select);
        return $result;

    }

    function getStaffOld($params){
        $db = Zend_Registry::get('db');
        
        $select = $db->select()->from(array('p' => $this->_name),array(''));
        $select->join(array('s' => 'staff'), 's.id = p.staff_id', array('s.*'));
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array('r.area_id'));

        if(isset($params['course_id']) and $params['course_id']){
            $select->where('p.course_id = ?', $params['course_id']);
        }

        $result = $db->fetchAll($select);
        return $result;

    }

}