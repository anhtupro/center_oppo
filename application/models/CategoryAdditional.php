<?php

class Application_Model_CategoryAdditional extends Zend_Db_Table_Abstract
{
    protected $_name = 'category_additional';
    protected $_schema = DATABASE_TRADE;

    public function getAll()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['c' => DATABASE_TRADE.'.category_additional'], [
                         'c.id',
                         'c.name'
                     ])
                    ->where('c.is_deleted = 0');

        $result = $db->fetchAll($select);

        return $result;
    }
}    