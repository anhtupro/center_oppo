<?php
class Application_Model_RequestCompany extends Zend_Db_Table_Abstract
{
    protected $_name = 'request_company';
    public function getCompany()
    {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id',
            'name'        => 'p.name'
        );
        $select->from(array('p'=>'request_company'), $arrCols);
        $result = $db->fetchAll($select);
        return $result;
    }
}