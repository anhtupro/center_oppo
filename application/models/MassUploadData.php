<?php
class Application_Model_MassUploadData extends Zend_Db_Table_Abstract
{
    protected $_name = 'massupload_data';
    
    function getData($params){
        $db = Zend_Registry::get('db');
        $col = array(
            'json' => 'p.json'
        );
        $select = $db-> select()->from(array('p' => $this->_name), $col);
        $select->where('p.area IN (?)', $params['area']);
        $select->where('p.massupload_id = ?', $params['id']);
        $result = $db->fetchAll($select);
        return $result;
    }
    function getDataReview($params){
        $db = Zend_Registry::get('db');
        $select = $db-> select()->from(array('m' => 'massupload'), ['m.created_at']);
        $select->join(['p'=>'massupload_data'],'p.massupload_id = m.id',['p.json']);
        $select->join(['st'=>'staff'],'st.id = m.created_by',['code'=>'st.code','full_name'=>"CONCAT(st.`firstname`,' ',st.`lastname`)"]);
        $select->where('m.del <> ?', 1);
        $select->where('m.parent_id = ?', $params['id_parent']);
        $result = $db->fetchAll($select);
        return $result;
    }
}