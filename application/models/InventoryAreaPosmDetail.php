<?php

class Application_Model_InventoryAreaPosmDetail extends Zend_Db_Table_Abstract
{
    protected $_name = 'inventory_area_posm_detail';
    protected $_schema = DATABASE_TRADE;

    public function getDetail($params)
    {

        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['d' => DATABASE_TRADE.'.inventory_area_posm_detail'], [
                         'd.category_id',
                         'd.quantity',
                         'd.type',
                         'd.date',
                         'd.created_by',
                         'category_name' => 'c.name',
                         'd.note',
                         'area' => 'a.name'
                     ])
                   ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'd.category_id = c.id', [])
            ->joinLeft(['a' => 'area'], 'd.area_id = a.id', [])
            ->order('d.date DESC');

            if ($params['area_id']) {
                $select->where('d.area_id = ?', $params['area_id']);
            }

            if ($params['area_list']) {
                $select->where('d.area_id IN (?)', $params['area_list']);
            }

            if ($params['from_date']) {
                $select->where('d.date >= ?', $params['from_date']);
            }

            if ($params['to_date']) {
                $select->where('d.date <= ?', $params['to_date']);
            }

        $result = $db->fetchAll($select);

        return $result;

    }

    public function exportHistory($params)
    {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $data = $this->getDetail($params);


        // export
        $heads = array(
            'Khu vực',
            'Ngày',
            'Hạng mục',
            'Số lượng',
            'Loại',
            'Ghi chú'
        );


        $PHPExcel->setActiveSheetIndex(0);
        $sheet    = $PHPExcel->getActiveSheet();

        $alpha    = 'A';
        $index    = 1;

        foreach($heads as $key)
        {
            $sheet->setCellValue($alpha.$index, $key);
            $alpha++;
        }
        $index    = 2;

        $i = 1;

        foreach($data as $item){
            $alpha    = 'A';

            $sheet->setCellValue($alpha++.$index, $item['area']);
            $sheet->setCellValue($alpha++.$index, $item['date'] ? date('Y-m-d H:i:s', strtotime($item['date'])) : '');
            $sheet->setCellValue($alpha++.$index, $item['category_name']);
            $sheet->setCellValue($alpha++.$index, $item['quantity']);
            $sheet->setCellValue($alpha++.$index, $item['type'] == 1 ? 'Nhập kho' : ($item['type'] == 2 ? 'Xuất kho' : '') );
            $sheet->setCellValue($alpha++.$index, $item['note']);


            $index++;
        }

        $filename = 'Lịch sử xuất nhập kho';
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }

    public function checkEnoughQuantityHeadOffice($area_id, $category_id, $quantity_order)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['i' => DATABASE_TRADE.'.inventory_area_posm'], [
                         'quantity' => 'IFNULL(i.quantity , 0)',
                         'category_name' => 'c.name'
                     ])
        ->where('i.category_id = ?', $category_id)
        ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'i.category_id = c.id', [])
        ->where('i.area_id = ?', $area_id); // kho HO

        $result = $db->fetchRow($select);

        if (! $result || $result['quantity'] < $quantity_order) {
            return false;
        }

        return true;

    }
}