<?php

class Application_Model_Request extends Zend_Db_Table_Abstract
{
    protected $_name = 'request';
    public function fetchPagination($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();


        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.*'));
        $select->from(array('p' => 'request'), $arrCols);
        $select->joinLeft(array('c' => 'request_category'), 'p.category_id = c.id', array('name_category' => 'name'));
        $select->joinLeft(array('comp' => 'company'), 'p.company_id = comp.id', array('name_company' => 'name'));
        $select->joinLeft(array('request_approve' => 'request_approve'), 'p.id = request_approve.request_id', array('staff_id'));
        if (!empty($params["title_name"]) and $params["title_name"] == "CEO") {
            $select->where('created_by='. $params['staff_id']. ' OR request_approve.staff_id='.$params['staff_id'].' OR status='."'cho ceo duyet'");
        }
        if (!empty($params["title_name"]) and $params["title_name"] == "SALES MANAGER") {
            $select->where('created_by='. $params['staff_id']. ' OR request_approve.staff_id='.$params['staff_id'].' OR status= '."'cho sales manager duyet'");
        } elseif (!empty($params["title_name"]) and $params["title_name"] == "KEY ACCOUNT MANAGER") {
            $select->where('created_by='. $params['staff_id']. ' OR request_approve.staff_id='.$params['staff_id'].' OR status= '."'cho key account manager duyet'");
        }
        if (!empty($params["title_name"]) and $params["title_name"] == "BANK ACCOUNTANT" and $params["staff_id"] == 17) {
            $select->where('created_by='. $params['staff_id']. ' OR request_approve.staff_id='.$params['staff_id'].' OR status= '."'cho bank accountant duyet'");
        }
        if (!empty($params["content"])) {
            $select->where('content LIKE ?', "%".$params["content"]."%");
        }
        if (!empty($params["channel_id"])) {
            $select->where('channel_id = ?', $params["channel_id"]);
        }
        if (!empty($params["category"])) {
            $select->where('category_id = ?', $params["category"]);
        }
        if (!empty($params["type"])) {
            $select->where('type = ?', $params["type"]);
        }
        if (!empty($params["company_id"])) {
            $select->where('company_id = ?', $params["company_id"]);
        }
        if (!empty($params["status"])) {
            $select->where('status LIKE ?', $params["status"]);
        }

        $select->group('p.id');
        $select->order('p.id DESC');
        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
    public
    function findRequestByID($id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('p.*'));
        $select->from(array('p' => 'request'), $arrCols);
        $select->joinLeft(array('c' => 'request_category'), 'p.category_id = c.id', array('name_category' => 'name'));
        $select->joinLeft(array('comp' => 'company'), 'p.company_id = comp.id', array('name_company' => 'name'));
        $select->joinLeft(array('channel' => 'request_channel'), 'p.channel_id = channel.id', array('name_channel' => 'name'));
        $select->where("p.id=?", $id);
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public
    function checkStaffApproving($idRequest, $idStaff)
    {

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('p.*'));
        $select->from(array('p' => 'request'), $arrCols);
        $select->joinLeft(array('request_approve' => 'request_approve'), 'p.id = request_approve.request_id', array('staff_id'));
        $select->where("p.id=?", $idRequest);
        $select->where("request_approve.staff_id=?", $idStaff);
        $result = $db->fetchAll($select);

        return $result;
    }

}