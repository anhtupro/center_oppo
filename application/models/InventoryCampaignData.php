<?php

class Application_Model_InventoryCampaignData extends Zend_Db_Table_Abstract
{
    protected $_name = 'inventory_campaign_data';

    public function getData($campaign_id, $staff_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['d' => 'inventory_campaign_data'], [
                'd.sellout',
                'd.inventory',
                'd.store_id',
                'd.brand_id'
            ])
            ->where('d.campaign_id = ?', $campaign_id)
            ->where('d.created_by = ?', $staff_id);

        $result = $db->fetchAll($select);

        foreach ($result as $item) {
            $list [$item['store_id']] [$item['brand_id']] = $item;
        }

        return $list;
    }

}