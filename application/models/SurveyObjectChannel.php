<?php
class Application_Model_SurveyObjectChannel extends Zend_Db_Table_Abstract
{
	protected $_name = 'survey_object_channel';

	public function getChannelSelectBySurvey($surveyId){
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('so' => 'survey_object'), array('so.object_id'));
            $select->joinLeft(array('soc'=>'survey_object_channel'),'so.survey_id=soc.survey_id AND so.object_id=soc.object_id',array('list_channel' =>'GROUP_CONCAT(soc.channel_id)'));
            $select->where('so.survey_id =? ',$surveyId);
        $select->group('so.object_id');
        $result = $db->fetchAll($select);
        $arrTemp=array();
        foreach($result as $key => $value){
            if(!empty($value['list_channel'])){
                $arrTemp[$value['object_id']]=explode(',', $value['list_channel']);
            }else{
                $arrTemp[$value['object_id']]=null;
            }
        }
        if($_GET['dev']){
            echo $select->__toString();
        }
        return $arrTemp;
	}
}                                                      
