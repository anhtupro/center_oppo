<?php

class Application_Model_RemoveShop extends Zend_Db_Table_Abstract
{
    protected $_name = 'remove_shop';

    public function getRemoveShop($id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'id' => "p.id",
            'store_id' => 'p.store_id',
            'reason' => 'p.reason',
            'created_at' => 'p.created_at',
            'created_by' => 'p.created_by',
            'store_name' => 's.name',
            'reason_name' => 'r.name',
            'note' => 'p.note',
            "created_name" => "CONCAT(staff.firstname, ' ',staff.lastname)",
            "status" => "p.status",
            "status_name" => "status.name",
            "p.reject",
            "p.latitude",
            "p.longitude",
            'rejected_by' => "CONCAT(staff_reject.firstname, ' ',staff_reject.lastname)",
            'reject_note'
        );

        $select->from(array('p' => 'remove_shop'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('r' => 'remove_shop_reason'), 'r.id = p.reason', array());
        $select->joinLeft(array('staff' => 'staff'), 'staff.id = p.created_by', array());
        $select->joinLeft(array('staff_reject' => 'staff'), 'staff_reject.id = p.rejected_by', array());
        $select->joinLeft(array('status' => DATABASE_TRADE . '.app_status'), 'status.status = p.status AND status.type = 11', array());

        $select->where('p.id = ?', $id);

        $result = $db->fetchRow($select);

        return $result;
    }


    public function fetchPagination($page, $limit, &$total, $params)
    {

        $db = Zend_Registry::get("db");
        $select = $db->select();

        if ($params['approve_all']) {
            $arrCol = [
                new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
                'p.status',
                'store_id' => "s.id"
            ];
        } else {
            $arrCol = [
                new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
                'p.status',
                'p.reject',
                'p.reject_note',
                'p.created_at',
                'status_name' => 'a2.name',
                'store_name' => "s.name",
                'store_code' => "s.store_code",
                'store_id' => "s.id",
                'province_name' => 'r.name',
                'district_name' => 'r2.name',
                'area_name' => 'a.name'
            ];
        }

        $select->from(array('p' => 'remove_shop'), $arrCol);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());
        $select->joinLeft(array('r2' => 'regional_market'), 'r2.id = s.district', array());
        $select->joinLeft(array('a' => 'area'), 'a.id = r.area_id', array());
        $select->joinLeft(array('a2' => DATABASE_TRADE . '.app_status'), 'a2.status = p.status AND a2.type = 11', array());
        $select->joinLeft(array('t' => DATABASE_TRADE . '.app_status_title'), 't.status_id = p.status AND t.type = 11', array());

        if (!empty($params['area_id'])) {
            $select->where('r.area_id IN (?)', $params['area_id']);
        }

        if (!empty($params['area'])) {
            $select->where('r.area_id IN (?)', $params['area']);
        }

        if (!empty($params['access_shop_id'])) {
            $select->where('s.id IN (?)', $params['access_shop_id']);
        }

        if (!empty($params['store_code'])) {
            $select->where('s.store_code = ?', $params['store_code']);
        }
        if (!empty($params['store_name'])) {
            $select->where('s.name LIKE ?', '%' . $params['store_name'] . '%');
        }

        if (!empty($params['reason'])) {
            $select->where('p.reason = ?', $params['reason']);
        }
        if (!empty($params['status_id'])) {
            $select->where('p.status IN (?)', $params['status_id']);
            $select->where('p.reject IS NULL OR p.reject = 0');
        }

        if (!empty($params['created_from'])) {
            $select->where('DATE(p.created_at) >= ?', $params['created_from']);
        }
        if (!empty($params['created_to'])) {
            $select->where('DATE(p.created_at) <= ?', $params['created_to']);
        }

        if (!empty($params['title'])) {
            $select->where('t.title = ? AND (p.reject IS NULL OR p.reject = 0)', $params['title']);
        }

        if (!$params['export'] && !$params['approve_all']) {
            $select->limitPage($page, $limit);
        }

        $select->group('p.id');

        $select->order('p.id DESC');

        $result = $db->fetchAll($select);

        if ($_GET['dev'] == 2) {
            echo "<pre>";
            var_dump($params);
            exit;
        }

        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    public function export($removeShopList)
    {
        include 'PHPExcel.php';

        $objExcel = new PHPExcel();
        $objExcel->setActiveSheetIndex(0);

        $sheet = $objExcel->getActiveSheet()->setTitle('List remove shop');

        $rowCount = 2;
        $index = 1;
        $sheet->setCellValue('A1', 'STT');
        $sheet->setCellValue('B1', 'Tên shop');
        $sheet->setCellValue('C1', 'ID shop');
        $sheet->setCellValue('D1', 'Area');
        $sheet->setCellValue('E1', 'Province');
        $sheet->setCellValue('F1', 'District');
        $sheet->setCellValue('G1', 'Trạng thái');
        $sheet->setCellValue('H1', 'Ngày đề xuất');

        foreach ($removeShopList as $shop) {
            $sheet->setCellValue('A' . $rowCount, $index);
            $sheet->setCellValue('B' . $rowCount, $shop['store_name']);
            $sheet->setCellValue('C' . $rowCount, $shop['store_id']);
            $sheet->setCellValue('D' . $rowCount, $shop['area_name']);
            $sheet->setCellValue('E' . $rowCount, $shop['province_name']);
            $sheet->setCellValue('F' . $rowCount, $shop['district_name']);
            $sheet->setCellValue('G' . $rowCount, $shop['reject'] == 1 ? 'Đề xuất bị từ chối' : $shop['status_name']);
            $sheet->setCellValue('H' . $rowCount, date('d-m-Y', strtotime($shop['created_at'])));

            ++$index;
            ++$rowCount;
        }

        //style sheet
        $style_border = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $sheet->getStyle('A0:H' . $rowCount)->applyFromArray($style_border);

        for ($i = 'A'; $i < 'I'; ++$i) {
            $sheet->getStyle($i . 1)->getFont()->setBold(true);
        }


        $objWriter = new PHPExcel_Writer_Excel2007($objExcel);
        $filename = 'Danh sách đóng shop';
        $objWriter->save($filename);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Set-Cookie: fileLoading=true');
        readfile($filename);
        exit;
    }


}