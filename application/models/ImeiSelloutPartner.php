<?php

class Application_Model_ImeiSelloutPartner extends Zend_Db_Table_Abstract {

    protected $_name = 'imei_kpi_sellout_partner';

    function fetchPagination($page, $limit, &$total, $params) {
        $db             = Zend_Registry::get('db');
        $params['from'] = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
        $params['to']   = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");

        $params['from'] = $params['from'] . ' 00:00:00';
        $params['to']   = $params['to'] . ' 23:59:59';

//        $subsql  = $db->select()
//                ->from(array('tmp' => 'imei_installment'), array('tmp.*'))
//                ->group('imei_sn');
        $subimei = $db->select()
                ->from(array('i' => 'imei_kpi_sellout_partner'), array('i.*'));
        if (isset($params['staff_id']) and $params['staff_id']) {
            $subimei->join(array('sl' => 'store_staff_log'), ' sl.store_id = i.store_id and i.timing_date >= FROM_UNIXTIME(sl.joined_at,"%Y-%m-%d") AND (sl.released_at IS NULL OR DATE(i.timing_date) < FROM_UNIXTIME(sl.released_at,"%Y-%m-%d"))
	AND sl.is_leader = 0 ', array());

            $subimei->where('sl.staff_id = ?', $params['staff_id']);
        }
        $subimei->where('i.timing_date >= ?', $params['from']);
        $subimei->where('i.timing_date <= ?', $params['to']);
        if (isset($params['area_id']) and $params['area_id']) {
            $subimei->where('i.area_id IN(?)', $params['area_id']);
        }
        if (isset($params['imei']) and $params['imei']) {
             $subimei->where('i.imei_sn = ?', $params['imei']);
        }
        if (isset($params['store_id']) and $params['store_id']){
            $subimei->where('i.store_id = ?', $params['store_id']);
        }

        $select = $db->select()
                ->from(array('p' => $subimei), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.imei_sn'), 'p.*'))
                ->joinLeft(array('a' => 'area'), 'p.area_id = a.id', array('area' => 'a.name'))
                ->joinLeft(array('rm' => 'regional_market'), 'p.province_id = rm.id', array('province' => 'rm.name'))
                ->joinLeft(array('rm1' => 'regional_market'), 'p.district_id = rm1.id', array('district' => 'rm1.name'))
                ->joinLeft(array('s' => 'store'), 'p.store_id = s.id', array('store' => 's.name'))
                ->joinLeft(array('s1' => 'staff'), 'p.pg_id = s1.id', array('firstname' => 's1.firstname', 'lastname' => 's1.lastname', 'code' => 's1.code', 'email' => 's1.email'))
                ->joinLeft(array('g' => WAREHOUSE_DB . '.good'), 'p.good_id = g.id', array('model' => 'g.desc'))
                ->joinLeft(array('c' => WAREHOUSE_DB . '.good_color'), 'p.color_id = c.id', array('color' => 'c.name'));
//                ->joinLeft(array('app' => 'apps_installing_imei'), 'app.imei_sn = p.imei_sn', array("app_installed" => 'app.app_id', "app_installed_pg" => 'app.staff_id'));
        $select->joinLeft(array('ir' => 'imei_installment'), 'ir.imei_sn = p.imei_sn and ir.staff_id=p.pg_id', array('ir.installment_company', 'installment_contract' => 'ir.contract_number'));



//        if (isset($params['staff_id']) and $params['staff_id'])
//            $select->where('p.pg_allow LIKE ?', '%,' . $params['staff_id'] . ',%');

//        if (isset($params['imei']) and $params['imei'])
//            $select->where('p.imei_sn = ?', $params['imei']);

        if (isset($params['product']) and $params['product'])
            $select->where('p.good_id = ?', $params['product']);

        if (isset($params['color']) and $params['color'])
            $select->where('p.color_id = ?', $params['color']);

//        if (isset($params['area_id']) and $params['area_id'])
//            $select->where('p.area_id IN(?)', $params['area_id']);

        if (isset($params['regional_market']) and $params['regional_market'])
            $select->where('p.province_id = ?', $params['regional_market']);

        if (isset($params['district']) and $params['district'])
            $select->where('p.district_id IN ?', $params['district']);

        if (isset($params['status']) and $params['status'] and $params['status'] == 1)
            $select->where('p.pg_id is not null and p.pg_id <> 0');

        if (isset($params['status']) and $params['status'] and $params['status'] == 2)
            $select->where('p.pg_id is null or p.pg_id = 0 ');

        if (isset($params['shop_type']) and $params['shop_type'] and $params['shop_type'] == 1)
            $select->where('p.pg_total > 0');

        if (isset($params['shop_type']) and $params['shop_type'] and $params['shop_type'] == 2)
            $select->where('p.pg_total = 0');

        if (isset($params['kpi']) and $params['kpi'] and $params['kpi'] == 1)
            $select->where('p.kpi_pg = 0');

//        if (isset($params['store_id']) and $params['store_id'])
//            $select->where('p.store_id = ?', $params['store_id']);

//        if (isset($params['from']) && $params['from'])
//            $select->where('p.timing_date >= ?', $params['from']);
//
//        if (isset($params['to']) && $params['to'])
//            $select->where('p.timing_date <= ?', $params['to']);
		$select->group('p.imei_sn');
        $select->order(array('p.pg_id', 'p.store_id', 'p.timing_date'));




        if ($limit)
            $select->limitPage($page, $limit);


        if (!empty($_GET['dev'])) {
            echo "<pre>";
            print_r($select->__toString());
            exit;
        }

        $result = $db->fetchAll($select);
        $total  = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    function fetchImeiFailPagination($page, $limit, &$total_fail, $params) {
        $db             = Zend_Registry::get('db');
        $params['from'] = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
        $params['to']   = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");

        $params['from'] = $params['from'] . ' 00:00:00';
        $params['to']   = $params['to'] . ' 23:59:59';
        $select         = $db->select()
                ->from(array('p' => 'imei_kpi_sellout_partner_fail'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.imei_sn'), 'p.*'))
                ->joinLeft(array('a' => 'area'), 'p.area_id = a.id', array('area' => 'a.name'))
                ->joinLeft(array('rm' => 'regional_market'), 'p.province_id = rm.id', array('province' => 'rm.name'))
                ->joinLeft(array('rm1' => 'regional_market'), 'p.district_id = rm1.id', array('district' => 'rm1.name'))
                ->joinLeft(array('s' => 'store'), 'p.store_id = s.id', array('store' => 's.name'))
                ->joinLeft(array('s1' => 'staff'), 'p.pg_id = s1.id', array('firstname' => 's1.firstname', 'lastname' => 's1.lastname', 'code' => 's1.code', 'email' => 's1.email'))
                ->joinLeft(array('g' => WAREHOUSE_DB . '.good'), 'p.good_id = g.id', array('model' => 'g.desc'))
                ->joinLeft(array('c' => WAREHOUSE_DB . '.good_color'), 'p.color_id = c.id', array('color' => 'c.name'));

        if (isset($params['staff_id']) and $params['staff_id'])
            $select->where('p.pg_id = ?', $params['staff_id']);

        if (isset($params['imei']) and $params['imei'])
            $select->where('p.imei_sn = ?', $params['imei']);

        if (isset($params['product']) and $params['product'])
            $select->where('p.good_id = ?', $params['product']);

        if (isset($params['color']) and $params['color'])
            $select->where('p.color_id = ?', $params['color']);

        if (isset($params['area_id']) and $params['area_id'])
            $select->where('p.area_id IN(?)', $params['area_id']);

        if (isset($params['regional_market']) and $params['regional_market'])
            $select->where('p.province_id = ?', $params['regional_market']);

        if (isset($params['district']) and $params['district'])
            $select->where('p.district_id IN ?', $params['district']);

        if (isset($params['status']) and $params['status'] and $params['status'] == 1)
            $select->where('p.pg_id is not null');

        if (isset($params['status']) and $params['status'] and $params['status'] == 2)
            $select->where('p.pg_id is null');

        if (isset($params['shop_type']) and $params['shop_type'] and $params['shop_type'] == 1)
            $select->where('p.pg_total > 0');

        if (isset($params['shop_type']) and $params['shop_type'] and $params['shop_type'] == 2)
            $select->where('p.pg_total = 0');

        if (isset($params['kpi']) and $params['kpi'] and $params['kpi'] == 1)
            $select->where('p.kpi_pg = 0');

        if (isset($params['store_id']) and $params['store_id'])
            $select->where('p.store_id = ?', $params['store_id']);

        if (isset($params['from']) && $params['from'])
            $select->where('DATE(p.timing_date) >= ?', $params['from']);

        if (isset($params['to']) && $params['to'])
            $select->where('DATE(p.timing_date) <= ?', $params['to']);

        $select->order(array('p.pg_id DESC', 'p.store_id', 'p.timing_date'));




        if ($limit)
            $select->limitPage($page, $limit);


        if (!empty($_GET['dev'])) {
            echo "<pre>";
            print_r($select->__toString());
            exit;
        }

        $result     = $db->fetchAll($select);
        $total_fail = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    function export($data) {
        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $heads = array(
            'A' => 'Imei',
            'B' => 'Model',
            'C' => 'Color',
            'D' => 'Store',
            'E' => 'Timing',
            'F' => 'Area',
            'G' => 'Province',
            // 'H' => 'KPI',
            'I' => 'Staff on Store',
            'J' => 'Staff KPI',
            'K' => 'Customer Name',
            'L' => 'Customer Phone',
            'M' => 'Customer Address',
            'N' => 'Partner ID',
            'O' => 'Activated Date',
                // 'O' => 'Total',
        );
        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        foreach ($heads as $key => $value)
            $sheet->setCellValue($key . '1', $value);
        $sheet->getStyle('A1:O1')->applyFromArray(array('font' => array('bold' => true)));

        $sheet->getColumnDimension('A')->setWidth(10);
        $sheet->getColumnDimension('B')->setWidth(30);
        $sheet->getColumnDimension('C')->setWidth(15);
        $sheet->getColumnDimension('D')->setWidth(20);
        $sheet->getColumnDimension('E')->setWidth(25);
        $sheet->getColumnDimension('F')->setWidth(25);
        $sheet->getColumnDimension('G')->setWidth(15);
       //  $sheet->getColumnDimension('H')->setWidth(10);
        $sheet->getColumnDimension('I')->setWidth(30);
        $sheet->getColumnDimension('J')->setWidth(15);
        $sheet->getColumnDimension('K')->setWidth(15);
        $sheet->getColumnDimension('L')->setWidth(30);
        $sheet->getColumnDimension('M')->setWidth(30);
        $sheet->getColumnDimension('N')->setWidth(15);
        $sheet->getColumnDimension('O')->setWidth(15);
        // $sheet->getColumnDimension('O')->setWidth(20);


        foreach ($data as $key => $value) {
            $sheet->setCellValue('A' . ($key + 2), $value['imei_sn']);
            $sheet->setCellValue('B' . ($key + 2), $value['model']);
            $sheet->setCellValue('C' . ($key + 2), $value['color']);
            $sheet->setCellValue('D' . ($key + 2), $value['store']);
            $sheet->setCellValue('E' . ($key + 2), $value['timing_date']);
            $sheet->setCellValue('F' . ($key + 2), $value['area']);
            $sheet->setCellValue('G' . ($key + 2), $value['province']);
           // $sheet->setCellValue('H' . ($key + 2), $value['kpi_pg']);
            $sheet->setCellValue('I' . ($key + 2), $value['pg_export']);
            $sheet->setCellValue('J' . ($key + 2), empty($value['pg_id']) ? 'No Staff' : $value['email']);
            $sheet->setCellValue('K' . ($key + 2), $value['customer_name']);
            $sheet->setCellValue('L' . ($key + 2), $value['customer_phone']);
            $sheet->setCellValue('M' . ($key + 2), $value['customer_address']);
            $sheet->setCellValue('N' . ($key + 2), $value['partner_id']);
            $sheet->setCellValue('O' . ($key + 2), $value['activation_date']);
            // $sheet->setCellValue('O'.($key + 2), $value['total']);
        }
// empty($value['kpi_pg']) : $value['pg_string'] ? 'No Staff'


        $filename  = 'Imeis Sellout Partner -' . date('d-m-Y H:i:s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');

        exit;
    }

}

// array('g' => WAREHOUSE_DB.'.good_price_log')