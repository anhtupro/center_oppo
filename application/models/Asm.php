<?php

class Application_Model_Asm extends Zend_Db_Table_Abstract {

    protected $_name = 'asm';

    function fetchPagination($page, $limit, &$total, $params) {
        $db = Zend_Registry::get('db');

        $select = $db->select()
                ->from(array('s' => 'staff'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS s.id'), 's.firstname', 's.lastname', 's.email', 'group_id' => 't.access_group', 'title_name' => 't.name', 'title' => 's.title'))
                ->joinLeft(array('p' => $this->_name), 'p.staff_id = s.id', array('p.area_id'))
                ->join(array('t' => 'team'), 't.id = s.title', array())
                ->where('s.status = 1 AND s.off_date IS NULL', 1);

        if (isset($params['name']) and $params['name'])
            $select->where('CONCAT(TRIM(s.firstname), " ", TRIM(s.lastname)) LIKE ?', '%' . $params['name'] . '%');

        if (isset($params['email']) and $params['email'])
            $select->where('s.email LIKE ?', str_replace(EMAIL_SUFFIX, '', $params['email']) . EMAIL_SUFFIX);

        if (isset($params['staff_id']) AND $params['staff_id']) {
            $select->where('p.staff_id = ?', $params['staff_id']);
        }

        if (isset($params['group_id'])) {
            if (is_array($params['group_id']) and count($params['group_id'])) {
                //$select->where('t.access_group IN (?)', $params['group_id']);
            }
        } else {
//             $select->where('s.group_id IN (?)', My_Staff_Group::$allow_in_area_view);
            //$select->where('t.access_group IN (?)', My_Staff_Group::$allow_in_area_view);
        }


        if (isset($params['title'])) {
            if (is_array($params['title']) and count($params['title']) > 0) {
                $select->where('s.title IN (?)', $params['title']);
            } elseif ($params['title']) {
                $select->where('s.title = ?', $params['title']);
            }
        }

        if (isset($params['team']) and $params['team']) {
            $select->where('s.team = ?', $params['team']);
        }

        $select->order('CONCAT(s.firstname, " ", s.lastname)', 'COLLATE utf8_unicode_ci ASC');

        if (!empty($_GET['dev'])) {
            echo "<pre>";
            print_r($select->__toString());
            exit;
        }

        // if ($limit)
        // $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);
        $total  = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    /**
     * Lấy danh sách các area/province/district theo từng ASM
     * @param  int $staff_id    Staff ID (Default null)
     * @return array            Format array( asm ID => array(list region) )
     */
    public function get_cache($staff_id = null) {
        $cache  = Zend_Registry::get('cache');
        $result = $cache->load($this->_name . '_cache');

        //$result = false;
        if ($result === false || (!is_null($staff_id) && !isset($result[$staff_id]) )) {
            $db = Zend_Registry::get('db');

            $select_area = $db->select()
                    ->from(array('p' => $this->_name, array('p.staff_id')))
                    ->join(array('s' => 'staff'), 's.id = p.staff_id', array())
                    ->join(array('t' => 'team'), 't.id = s.title', array())
                    ->joinLeft(array('r' => 'v_regional_market'), 'r.area_id=p.area_id', array('province' => 'r.id'))
                    ->joinLeft(array('d' => 'v_regional_market'), 'd.parent=r.id', array('district' => 'd.id'));

            $select_province = $db->select()
                    ->from(array('p' => $this->_name, array('p.staff_id')))
                    ->join(array('s' => 'staff'), 's.id = p.staff_id', array())
                    ->join(array('t' => 'team'), 't.id = s.title', array())
                    ->joinLeft(array('r' => 'v_regional_market'), 'r.id=p.area_id', array('province' => 'r.id'))
                    ->joinLeft(array('d' => 'v_regional_market'), 'd.parent=r.id', array('district' => 'd.id'));
            $select          = $db->select()
                    ->union(array($select_area, $select_province));

            $data = $db->fetchAll($select);

            $result = array();

            if ($data) {
                foreach ($data as $item) {
                    if (!isset($result[$item['staff_id']]))
                        $result[$item['staff_id']] = array('area' => array(), 'province' => array(), 'district' => array());

                    if ($item['type'] == My_Region::Area)
                        $result[$item['staff_id']]['area'][] = $item['area_id'];

                    $result[$item['staff_id']]['province'][] = $item['province'];
                    $result[$item['staff_id']]['district'][] = $item['district'];
                }

                foreach ($result as $_staff_id => $value) {
                    $result[$_staff_id]['area']     = array_filter(array_unique($value['area']));
                    $result[$_staff_id]['province'] = array_filter(array_unique($value['province']));
                    $result[$_staff_id]['district'] = array_filter(array_unique($value['district']));
                }

            }

            $cache->save($result, $this->_name . '_cache', array(), null);
        }

        return is_null($staff_id) ? $result : ( isset($result[$staff_id]) ? $result[$staff_id] : false );
    }

    /**  Lấy danh sách các region theo từng ASM
     * @param  int $staff_id    Staff ID (Default null)
     * @return array            Format array( asm ID => array(list region) )
     */
    public function get_region_cache($staff_id = null) {
        $cache  = Zend_Registry::get('cache');
        $result = $cache->load($this->_name . '_region_new_cache');

        if ($result === false || (!is_null($staff_id) && !isset($result[$staff_id]) )) {
            $data   = $this->fetchAll();
            $result = array();

            if ($data) {
                $QRegion = new Application_Model_RegionalMarket();

                foreach ($data as $item) {
                    if (!isset($result[$item['staff_id']]))
                        $result[$item['staff_id']] = array();

                    $where   = $QRegion->getAdapter()->quoteInto('area_id = ?', $item['area_id']);
                    $regions = $QRegion->fetchAll($where);

                    if ($regions)
                        foreach ($regions as $reg)
                            $result[$item['staff_id']][] = $reg['id'];
                }
            }

            $cache->save($result, $this->_name . '_region_new_cache', array(), null);
        }

        return is_null($staff_id) ? $result : ( isset($result[$staff_id]) ? $result[$staff_id] : false );
    }

    public function is_asm($staff_id, $store_id) {
        $QStore = new Application_Model_Store();
        $store  = $QStore->find($store_id);
        $store  = $store->current();

        if (!$store)
            return false;

        $regions = $this->get_cache($staff_id);

        if ($regions && is_array($regions))
            return in_array($store['district'], $regions['district']);

        return false;
    }

    public function fetchTrainerLeader($page, $limit, &$total, $params) {
        
    }

    public function get_area_trade() {
        $db = Zend_Registry::get('db');

        $select = $db->select()
                ->from(array('p' => DATABASE_TRADE . '.area_staff', array('p.staff_id', 'p.area_id')));

        $result = $db->fetchAll($select);

        $data = array();

        foreach ($result as $key => $value) {
            $data[$value['staff_id']][] = $value['area_id'];
        }

        return $data;
    }

    /**
     * @param $rsm_staff_id
     * @return array
     */
    public function getRSMSubordinate($rsm_staff_id)
    {
        $where = 'rsm.staff_id = %s and rsm.area_id = asm.area_id and asm.staff_id != %s';
        $where = sprintf($where, $rsm_staff_id, $rsm_staff_id);
        $data = $this->fetchAll(
            $this->select()
                ->from(['rsm' => $this], [])
                ->join($this, $where, ['asm.staff_id'])
        )->toArray();
        $result = [];
        foreach ($data as $datum) {
            $result[] = [
                'staff_id' => $datum['staff_id'],
                'title' => 'ASM'
            ];
        }
        return $result;
    }

    /**
     * @param $asm_staff_id
     * @return array
     */
    public function getASMSubordinate($asm_staff_id)
    {
        $dataSaleLeader = $this->fetchAll(
            $this->select()
                ->distinct()
                ->from($this, [])
                ->join(['rm' => 'regional_market'], "asm.area_id = rm.area_id and asm.staff_id = $asm_staff_id", [])
                ->join(['rm1' => 'regional_market'], "rm.id = rm1.parent", [])
                ->join(['st' => 'store'], "st.regional_market = rm.id or st.regional_market = rm1.id", [])
                ->join(['sl' => 'store_leader'], "sl.store_id = st.id", ['sl.staff_id'])
        )->toArray();
        $dataSale = $this->fetchAll(
            $this->select()
                ->distinct()
                ->from($this, [])
                ->join(['rm' => 'regional_market'], "asm.area_id = rm.area_id and asm.staff_id = $asm_staff_id", [])
                ->join(['rm1' => 'regional_market'], "rm.id = rm1.parent", [])
                ->join(['st' => 'store'], "(st.regional_market = rm.id or st.regional_market = rm1.id) and st.del = 0 and st.id not in (select store_id from store_leader)", [])
                ->join(['s' => 'store_staff_log'], "s.store_id = st.id and s.is_leader = 1 and s.released_at is null", ['s.staff_id'])
        )->toArray();

        $result = [];
        foreach ($dataSaleLeader as $datum) {
            $result[] = [
                'staff_id' => $datum['staff_id'],
                'title' => 'SaleLeader'
            ];
        }
        foreach ($dataSale as $datum) {
            $result[] = [
                'staff_id' => $datum['staff_id'],
                'title' => 'Sale'
            ];
        }
        return $result;
    }

    /**
     * @param $asm_staff_id
     * @return array
     */
    public function getASMSuperior($asm_staff_id)
    {
        $data = $this->fetchAll(
            $this->select()
                ->from(['rsm' => $this], ['rsm.staff_id'])
                ->join(['s' => 'staff'], "rsm.staff_id = s.id and s.title = 308", [])
                ->join($this, "rsm.area_id = asm.area_id and rsm.staff_id != asm.staff_id and asm.staff_id = $asm_staff_id", [])
        )->toArray();
        $result = [];
        foreach ($data as $datum) {
            $result[] = [
                'staff_id' => $datum['staff_id'],
                'title' => 'RSM'
            ];
        }
        return $result;
    }

    public function getArea($staffId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => 'asm'], [
                'a.area_id'
            ])
            ->where('a.staff_id = ?', $staffId);

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $listArea [] = $element['area_id'];
        }

        return $listArea;


    }
    
    function get_cache_staff($staff_group) {
        $db = Zend_Registry::get('db');

        $select = $db->select()
                ->from(array('s' => 'staff'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS s.id')))
                ->joinLeft(array('p' => $this->_name), 'p.staff_id = s.id', array())
                ->join(array('t' => 'team'), 't.id = s.title', array())
                ->where('s.status = 1 AND s.off_date IS NULL', 1)
                ->where('t.access_group IN (?)', $staff_group);      
                    
        $result = $db->fetchAll($select);
        
        return $result;
    }
    
    
}
