<?php

class Application_Model_StaffPermissions extends Zend_Db_Table_Abstract {

    protected $_name = 'staff_permissions';

    public function getListData($option, $param = [], $page, $limit, &$total) { //option 1: staff 2 title
        $sql_params = [
            $option,
            $param['tool_name'],
            $param['staff_code'],
            $param['department'],
            $param['team'],
            $param['title'],
            $param['area_approve'],
            $param['office_approve'],
            $param['department_approve'],
            $param['team_approve'],
            $param['title_approve'],
            $param['staff_code_approve'],
            $page,
            $limit
        ];

        $db = Zend_Registry::get('db');
        $sql = 'CALL get_list_staff_permission(?,?,?,?,?,?,?,?,?,?,?,?,?,?,@total)';
        $stmt = $db->query($sql, $sql_params);
        $list = $stmt->fetchAll();
        $stmt->closeCursor();
        $total = $db->fetchOne('select @total');
        return $list;
    }

    public function coppyData($db,$group_permission_id , $group_permission_new , $id_created_by) {
        $stmt = $db->prepare("CALL p_coppy_data_permission(:group_permission_id, :group_permission_new , :id_created_by)");
        $stmt->bindParam("group_permission_id", $group_permission_id, PDO::PARAM_INT);
        $stmt->bindParam("group_permission_new", $group_permission_new, PDO::PARAM_INT);
        $stmt->bindParam("id_created_by", $id_created_by, PDO::PARAM_INT);
        $stmt->execute();
        $stmt->closeCursor();
        $stmt = $db = null;
    }

}
