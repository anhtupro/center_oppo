<?php

class Application_Model_RequestOffice extends Zend_Db_Table_Abstract
{
    protected $_name = 'request_office';

    protected $_schema = DATABASE_SALARY;
    
    public function fetchPaginationNew($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();
        
        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            "p.company_id", 
            "p.category_id", 
            "p.department_id", 
            "p.area_id", 
            "p.team_id", 
            "p.has_payment", 
            "p.payment_type", 
            "p.has_purchasing_request",
            "p.note",
            "p.reject",
            "p.reject_by",
            "p.reject_at",
            "p.content", 
            "p.cost_temp",
            "currency" => "IF(p.currency = 2, 'USD', IF(p.currency = 3, 'CYN', 'đồng'))",
            "currency_name" => "IF(p.currency = 2, 'USD', IF(p.currency = 3, 'CYN', 'VNĐ'))",
            "p.cost_advanced",
            "p.created_by", 
            "p.date_offer", 
            "p.created_at", 
            "p.is_approve", 
            "p.del", 
            "p.payee", 
            "p.type", 
            "p.step",
            "p.month_fee",
            "p.year_fee",
            "p.status",
            "p.pr_sn", 
            "p.date_success", 
            "p.cost_before", 
            "p.request_type", 
            "p.request_type_group",
            "name_category" => "c.name", 
            "name_category_eng" => "c.name_eng",
            "name_company" => "comp.name",
            "status_pr" => "purchasing_request.status",
            "confirm_name" => "purchasing_request.confirm_name",
            "approve_name" => "purchasing_request.approve_name",
            "total_cost" => "p.cost_temp",
            "total_cost2" => "SUM(r.payment_cost)",
            "staff_id_confirm" => "confirm.staff_id",
            "confirm.title_confirm",
            "list_staff_confirm" => "GROUP_CONCAT(
		DISTINCT CONCAT(CONCAT(s.firstname, ' ', s.lastname),',',confirm2.is_confirm) 
		ORDER BY confirm2.id
		SEPARATOR ';'
            )",
            "list_payment" => "GROUP_CONCAT(
            DISTINCT CONCAT( r.id, ',', IFNULL(CAST(r.payment_date AS CHAR CHARACTER SET utf8), CAST('Chưa có ngày thanh toán' AS CHAR CHARACTER SET utf8) ), ',', r.payment_cost, ',', IFNULL(r.bill_number,' ') ) 
            ORDER BY
		r.id SEPARATOR ';' 
                )",
            "project_name" => "project.project_name",
            "type_group_title" => "type_group.title",
            "request_type_title" => "request_type.title",
            "list_staff_note" => "GROUP_CONCAT(
		DISTINCT CONCAT(CONCAT(staff_note.firstname, ' ', staff_note.lastname),'_',note.note) 
		ORDER BY note.id
		SEPARATOR ';'
            )",
            "p.estimasted_liquidation_date",
            "p.invoice_number",
            "p.invoice_date",
            "p.finance_payment_date",
            "warning_status" => "GROUP_CONCAT(w.status)"
        );

        $select->from(array('p' => DATABASE_SALARY.'.request_office'), $arrCols);
        $select->joinLeft(array('c' => 'request_category_office'), 'p.category_id = c.id', array());
        $select->joinLeft(array('comp' => 'company'), 'p.company_id = comp.id', array());
        $select->joinLeft(array('purchasing_request' => new Zend_Db_Expr("
                (Select p.id, p.status, p.sn, CONCAT(s.firstname, ' ', s.lastname) confirm_name, CONCAT(s2.firstname, ' ', s2.lastname) approve_name
                FROM purchasing_request p
                LEFT JOIN staff s ON s.id = p.confirm_by
                LEFT JOIN staff s2 ON s2.id = p.approved_by)
                ")), 'p.pr_sn = purchasing_request.sn', array());
        $select->joinLeft(array('r' => 'request_payment_office'), 'r.request_id = p.id ', array());
        $select->joinLeft(array('confirm' => 'request_confirm'), 'confirm.request_id = p.id', array());
        $select->joinLeft(array('confirm2' => 'request_confirm'), 'confirm2.request_id = p.id', array());
        $select->joinLeft(array('s' => 'staff'), 's.id = confirm2.staff_id', array());
        $select->joinLeft(array('project' => 'request_project'), 'project.id = p.project', array());
        $select->joinLeft(array('request_type' => 'request_type'), 'request_type.id = p.request_type', array());
        $select->joinLeft(array('type_group' => 'request_type_group'), 'type_group.id = p.request_type_group', array());
        $select->joinLeft(array('note' => 'request_office_note'), 'note.request_id = p.id', array());
        $select->joinLeft(array('staff_note' => 'staff'), 'staff_note.id = note.created_by', array());
        $select->joinLeft(array('w' => 'request_office_warning'), 'w.request_id = p.id AND w.del = 0', array());
        
        $select->where("p.del = 0");
        
        //Chị Vi finance xem đc tất cả đề xuất đã hoàn thành
        if($params["staff_id"] == 1707){
            $select->where("(confirm.staff_id = 1707 AND confirm.is_confirm = 1) OR (p.status = 'Hoàn thành' AND has_payment = 1)");
        }
        else{
            //Kiểm tra click view all thì check trong request_view ko thì check bt
            if(!empty($params['has_request_view']) AND !empty($params['view_all'])){

                $view_all = 0;
                $view_team = [];
                $view_department = [];
                $view_staff = [];
                foreach($params['has_request_view'] as $key=>$value){
                    if($value['all'] == 1){
                        $view_all = 1;
                    }

                    if($value['team_id']){
                        $view_team[] =  $value['team_id'];
                    }

                    if($value['department_id']){
                        $view_department[] =  $value['department_id'];
                    }

                    if($value['view_staff_id']){
                        $view_staff[] =  $value['view_staff_id'];
                    }
                }

                if($view_team){
                    $select->where("p.team_id IN (?)", $view_team);
                }

                if($view_department){
                    $select->where("p.department_id IN (?)", $view_department);
                }

                if($view_staff){
                    $select->where("p.created_by IN (?)", $view_staff);
                }

                if(!$view_all && !$view_team && !$view_department && !$view_staff){
                    return false;
                }
            }
            else{
                if(!empty($params['request_own'])){
                    $select->where('(confirm.staff_id = ? AND confirm.is_confirm = 1) OR p.created_by = '.$params["staff_id"], $params["staff_id"]);
                }
                else{
                    $select->where('p.reject IS NULL');
                    $select->where('confirm.staff_id = ?', $params["staff_id"]);
                    $select->where('confirm.is_confirm = 0');
                    $select->where('confirm.step = p.step');
                }
            }
        }
        
        
        if (!empty($params["content"])) {
            $select->where('p.content LIKE ?', "%" . trim($params["content"]) . "%");
        }
        if (!empty($params["category_id"])) {
            $select->where('p.category_id = ?', $params["category_id"]);
        }
        if (!empty($params["company_id"])) {
            $select->where('p.company_id = ?', $params["company_id"]);
        }

        if (!empty($params["from_date"])) {
            $select->where('p.created_at >= ? ', $params["from_date"]);
        }
        if (!empty($params["to_date"])) {
            $select->where('p.created_at <= ? ', $params["to_date"]);
        }
        
        if (!empty($params["month_fee"])) {
            $select->where('p.month_fee = ? ', $params["month_fee"]);
        }
        if (!empty($params["year_fee"])) {
            $select->where('p.year_fee = ? ', $params["year_fee"]);
        }
        
        if (!empty($params["department_id"])) {
            $select->where('p.department_id IN (?)', $params["department_id"]);
        }
        
        if (!empty($params["team_id"])) {
            $select->where('p.team_id IN (?)', $params["team_id"]);
        }
        
        if (!empty($params["project"])) {
            $select->where('p.project = ? ', $params["project"]);
        }
        
        if (!empty($params["request_type_group"])) {
            $select->where('p.request_type_group = ?', $params["request_type_group"]);
        }
        
        if (!empty($params["request_type"])) {
            $select->where('p.request_type = ?', $params["request_type"]);
        }
        
        if (!empty($params["cost_temp"])) {
            $cost_temp=str_replace(",","",$params["cost_temp"]);
            $select->where('p.cost_temp LIKE ?', '%'.$cost_temp.'%');
        }
        
        if (!empty($params["payee"])) {
            $select->where('p.payee LIKE ?', '%'.$params["payee"].'%');
        }
        
        if (!empty($params["request_id"])) {
            $select->where('p.id = ?', $params["request_id"]);
        }
        
        if (!empty($params["step"])) {
            $select->where('p.step IN (?)', $params["step"]);
        }
        
        if (!empty($params["is_year_contract"])) {
            $select->where('p.is_year_contract = ?', $params["is_year_contract"]);
        }
        
        $select->group('p.id');
        
        
        if (isset($params['sort']) and $params['sort']) {
            $collate = ' ';
            $desc = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';
            $order_str = 'p.`'.$params['sort'] . '` ' . $collate . $desc;

            $select->order(new Zend_Db_Expr($order_str));
        }
        else{
            $select->order('p.id DESC');
        }
        
        if(empty($params['export'])){
            $select->limitPage($page, $limit);
        }
        else{
            return $select->__toString();
        }
        
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
    
    public function fetchPagination($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $arrDeparment=array(159,160,150,151,153,400,$userStorage->department);//Thuy.to

        $staffOfLeader = $this->getStaffOfStaffLeader($userStorage->code);
        $staffPermission = $this->isLeaderOrManager($userStorage->code);
        $_isManager = $staffPermission->is_manager;
        $_isLeader = $staffPermission->is_leader;

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.*'));
        $select->from(array('p' => DATABASE_SALARY.'.request_office'), $arrCols);
        $select->joinLeft(array('c' => 'request_category_office'), 'p.category_id = c.id', array('name_category' => 'name', 'name_category_eng' => 'name_eng'));
        $select->joinLeft(array('comp' => 'company'), 'p.company_id = comp.id', array('name_company' => 'name'));
        $select->joinLeft(array('request_approve_office' => 'request_approve_office'), 'p.id = request_approve_office.request_id', array('staff_id', "staff_approved" => "group_concat(request_approve_office.staff_id separator ',')"));
        $select->joinLeft(array('request_office_flow_approve' => 'request_office_flow_approve'), 'p.type = request_office_flow_approve.type AND p.step=request_office_flow_approve.step', array('status_step' => 'note', 'status_step_eng' => 'note_eng'));
        // $select->joinLeft(array('request_office_pr' => 'request_office_pr'), 'p.id = request_office_pr.request_id', array());
        $select->joinLeft(array('purchasing_request' => 'purchasing_request'), 'p.pr_sn = purchasing_request.sn', array("status_pr" => "status"));
        // $select->joinLeft(array('request_pr_payment_office' => 'request_pr_payment_office'), 'request_pr_payment_office.request_id = p.id', array("detail_payment" => "group_concat(request_pr_payment_office.department_id separator ',')"));

        $select->joinLeft(array('request_payment_office' => 'request_payment_office'), 'p.id = request_payment_office.request_id', array("invoice_number","total_cost2"=>"SUM(request_payment_office.payment_cost)","total_cost"=>"p.cost_temp"));
        if ($userStorage->department == 149) {
            $select->join(array('request_permission_detail' => 'request_permission_detail'), 'p.type = request_permission_detail.request_type AND p.step=request_permission_detail.step AND p.payment_type=request_permission_detail.payment_type AND p.company_id=request_permission_detail.company_id AND p.has_purchasing_request=request_permission_detail.has_purchasing_request', array('finish_request',"test"=>"request_permission_detail.id"));
            $select->where("request_permission_detail.staff_id = ? ", $userStorage->id);
            $select->where('request_permission_detail.del= 0');

        } else {
            $select->join(array('request_permission_detail' => 'request_permission_detail'), 'p.type = request_permission_detail.request_type AND p.step=request_permission_detail.step AND p.department_id=request_permission_detail.department_id', array('finish_request'));
            $select->where('request_permission_detail.del= 0');
//            $_IsStaffLeader=$this->getLeaderOfStaff($userStorage->id);
            $select->where("request_permission_detail.staff_id = ? ", $userStorage->id);
            if (!empty($staffOfLeader) AND !empty($_isManager) AND $userStorage->id !=765) {
                if($userStorage->id ==7){ // Thuy.to can xem cac de xuat cua phong ban khac
                    $select->where("p.created_by IN (".  implode(",",$staffOfLeader).") OR p.department_id IN (". implode(",",$arrDeparment).")");
                }else{
                    // $select->where("p.created_by IN (?) ", $staffOfLeader);
                    $select->where("p.created_by IN (".  implode(",",$staffOfLeader).") OR p.department_id =". $userStorage->department);
                    // $select->where(" p.department_id =". $userStorage->department);
                }
            }elseif (!empty($staffOfLeader) AND !empty($_isLeader) AND $userStorage->id !=765) {

                $select->where("p.created_by IN (?) ", $staffOfLeader);
                // $select->where("p.type= 6");
            }
            elseif(empty($_isLeader) AND empty($_isManager)){
                //$select->where("p.created_by IN (?) ", $userStorage->id);
            }
        }


        if (empty($params["step"])) {

        } else {
            $select->where("p.step = ?", $params["step"]);
        }

        if (!empty($params["status_filter"]) AND $params["status_filter"] != -1) {
            $select->where("request_office_flow_approve.status_filter = ?", $params["status_filter"]);
        } elseif ($params["status_filter"] == -1) {
            $select->where("p.is_approve = 1");
        }
        $select->where('p.del= 0');



        if (!empty($params["content"])) {
            $select->where('p.content LIKE ?', "%" . trim($params["content"]) . "%");
        }
        if (!empty($params["category"])) {
            $select->where('p.category_id = ?', $params["category"]);
        }
        if (!empty($params["company_id"])) {
            $select->where('p.company_id = ?', $params["company_id"]);
        }

        if (!empty($params["from_date"])) {
            $select->where('p.created_at >= ? ', $params["from_date"]);
        }
        if (!empty($params["to_date"])) {
            $select->where('p.created_at <= ? ', $params["to_date"]);
        }

        $select->group('p.id');
        $select->order('p.id DESC');
        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        
        if($_GET['dev'] == 1){
            echo $select;
            print_r($_isManager);
            print_r($_isLeader);
            print_r($staffOfLeader);
        }
        
        return $result;


    }

    public function getStaffOfStaffLeader($leader_staff_code)
    {
        $db = Zend_Registry::get('db');
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $stmt = $db->prepare("CALL `PR_get_permission_by_staffcode_filter_version1`(:p_staff_code)");

        $stmt->bindParam('p_staff_code', $leader_staff_code, PDO::PARAM_STR);
        $stmt->execute();
        $list_of_subordinates_tmp = $stmt->fetchAll();
        $stmt->closeCursor();
        foreach ($list_of_subordinates_tmp as $key => $value) {
            $arr_temp[$key] = $value["id"];
        }
        $arr_temp[$key+1]=$userStorage->id;
        return $arr_temp;
    }

    public function isLeaderOrManager($staff_code)
    {
        $QPermission = new Application_Model_StaffPermission();
        $where = array();

        $where = $QPermission->getAdapter()->quoteInto("staff_code = ? ", $staff_code);

        $result = $QPermission->fetchRow($where);
        return $result;
    }


    public function fetchPaginationOldRequest($page, $limit, &$total, $params)
    {


        $db = Zend_Registry::get("db");
        $select = $db->select();

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.*'));
        $select->from(array('p' => DATABASE_SALARY.'.request_office'), $arrCols);
        $select->joinLeft(array('c' => 'request_category_office'), 'p.category_id = c.id', array('name_category' => 'name', 'name_category_eng' => 'name_eng'));
        $select->joinLeft(array('comp' => 'company'), 'p.company_id = comp.id', array('name_company' => 'name'));
        $select->joinLeft(array('request_approve_office' => 'request_approve_office'), 'p.id = request_approve_office.request_id', array('staff_id', "staff_approved" => "group_concat(request_approve_office.staff_id separator ',')"));
        $select->joinLeft(array('request_office_flow_approve' => 'request_office_flow_approve'), 'p.type = request_office_flow_approve.type AND p.step=request_office_flow_approve.step', array('status_step' => 'note', 'status_step_eng' => 'note_eng'));
        $select->joinLeft(array('purchasing_request' => 'purchasing_request'), 'p.pr_sn = purchasing_request.sn', array("status_pr" => "status"));
        $select->joinLeft(array('request_payment_office' => 'request_payment_office'), 'p.id = request_payment_office.request_id', array("invoice_number","total_cost2"=>"SUM(request_payment_office.payment_cost)","total_cost"=>"p.cost_temp"));


        if (empty($params["step"])) {

        } else {
            $select->where("p.step = ?", $params["step"]);
        }

        if (!empty($params["status_filter"]) AND $params["status_filter"] != -1) {
            $select->where("request_office_flow_approve.status_filter = ?", $params["status_filter"]);
        } elseif ($params["status_filter"] == -1) {
            $select->where("p.is_approve = 1");
        }
        $select->where('p.del= 0');
        if (!empty($params["content"])) {
            $select->where('p.content LIKE ?', "%" . trim($params["content"]) . "%");
        }
        if (!empty($params["category"])) {
            $select->where('p.category_id = ?', $params["category"]);
        }
        if (!empty($params["company_id"])) {
            $select->where('p.company_id = ?', $params["company_id"]);
        }

        if (!empty($params["from_date"])) {
            $select->where('p.created_at >= ? ', $params["from_date"]);
        }
        if (!empty($params["to_date"])) {
            $select->where('p.created_at <= ? ', $params["to_date"]);
        }
        $select->where('request_approve_office.staff_id = ?', $userStorage->id);
        $select->orWhere('p.created_by = ? AND p.del=0', $userStorage->id);

        $select->group('p.id');

        $select->order('p.id DESC');
        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        
        if($_GET['dev'] == 1){
            echo $select->__toString();
        }

        return $result;


    }

    public function findRequestByID($id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('p.*'));
        $select->from(array('p' => DATABASE_SALARY.'.request_office'), $arrCols);
        $select->joinLeft(array('c' => 'request_category_office'), 'p.category_id = c.id', array('name_category' => 'name'));
        $select->joinLeft(array('com' => 'company'), 'p.company_id = com.id', array('name_company' => 'name'));
        $select->joinLeft(array('request_office_pr' => 'request_office_pr'), 'p.id = request_office_pr.request_id', array('sn' => 'request_office_pr.sn'));

        $select->where("p.id=?", $id);
        $result = $db->fetchRow($select);
        return $result;
    }

    public function getListRequestForApprove($userId) //idRequest = null to get list request that user current can approve
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $arrCols = array(
            new Zend_Db_Expr('p.*'));
        $select->from(array('p' => DATABASE_SALARY.'.request_office'), $arrCols);
        if ($userStorage->department == 149) {
            $select->join(array('request_permission_detail' => 'request_permission_detail'), 'p.type = request_permission_detail.request_type AND p.step=request_permission_detail.step AND p.payment_type=request_permission_detail.payment_type AND p.company_id=request_permission_detail.company_id AND p.has_purchasing_request=request_permission_detail.has_purchasing_request', array('finish_request','update_payment',"test"=>"request_permission_detail.id"));
            $select->where("request_permission_detail.staff_id = ? ", $userStorage->id);
        } else {
            $select->join(array('request_permission_detail' => 'request_permission_detail'), 'p.type = request_permission_detail.request_type AND p.step=request_permission_detail.step AND p.department_id=request_permission_detail.department_id', array('finish_request'));
        }

//        $select->join(array('request_permission_detail' => 'request_permission_detail'), 'p.type = request_permission_detail.request_type AND p.step=request_permission_detail.step AND p.department_id= request_permission_detail.department_id', array('finish_request'));
        if (!empty($idRequest)) {
            $select->where("p.id = ? ", $idRequest);
        }

        $select->where("request_permission_detail.staff_id=?", $userId);


        $result = $db->fetchAll($select);

        $list = array();
        foreach ($result as $key => $value) {
            if (empty($value["finish_request"]) == 1) {
                $list[$value["id"]] = 1;
            }
        }
        return $list;
    }

    public function getListRequestForFinanceApprove($userId) //idRequest = null to get list request that user current can approve
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('p.*'));
        $select->from(array('p' => DATABASE_SALARY.'.request_office'), $arrCols);
        $select->join(array('request_permission_detail' => 'request_permission_detail'), 'p.type = request_permission_detail.request_type AND p.step=request_permission_detail.step AND p.department_id= request_permission_detail.department_id
        AND p.payment_type=request_permission_detail.payment_type AND p.has_purchasing_request=request_permission_detail.has_purchasing_request', array('finish_request'));
        if (!empty($idRequest)) {
            $select->where("p.id = ? ", $idRequest);
        }

        $select->where("request_permission_detail.staff_id=?", $userId);


        $result = $db->fetchAll($select);

        $list = array();
        foreach ($result as $key => $value) {
            if (empty($value["finish_request"]) == 1) {
                $list[$value["id"]] = 1;
            }
        }

        return $list;
    }

    public function getStaffApproved($idRequest)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();
        $select->from(array('p' => 'request_approve_office'), array("staff_approved" => "group_concat(concat(staff.firstname,' ',staff.lastname) separator ',')"));
        $select->joinLeft(array("staff" => "staff"), "p.staff_id=staff.id", array());
        $select->where("p.request_id =?", $idRequest);
        $select->group("p.request_id");
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getAdvancedPayment($user_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $select->from(array('p' => DATABASE_SALARY.'.request_office'), array("p.content", "p.id", "p.cost_temp"));

        if (!empty($user_id)) {
            $select->where("p.created_by=?", $user_id);
        }
        $select->where("p.category_id=?", 2);
        $select->where("p.advance_request_id IS NULL");

        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getManyPayment($user_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $select->from(array('p' => DATABASE_SALARY.'.request_office'), array("p.content", "p.id", "cost_many_total" => "p.cost_before", "cost_many" => "(p.cost_temp + IFNULL(SUM(r.cost_before),0))"));
        $select->joinLeft(array("r" => DATABASE_SALARY.".request_office"), "r.many_request_id = p.id", array());
        
        $select->where("p.created_by=?", $user_id);
        $select->where("p.category_id=?", 6);
        $select->where("p.del=?", 0);
        $select->where("p.many_request_id IS NULL");
        
        $select->group("p.id");
        $select->order("p.id DESC");

        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getManyPaymentEdit($user_id, $id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $select->from(array('p' => DATABASE_SALARY.'.request_office'), array("p.content", "p.id", "cost_many_total" => "p.cost_before", "cost_many" => "(p.cost_temp + IFNULL(SUM(r.cost_before),0))"));
        $select->joinLeft(array("r" => DATABASE_SALARY.".request_office"), "r.many_request_id = p.id AND r.id <> ". My_Util::escape_string($id), array());
        
        $select->where("p.created_by=?", $user_id);
        $select->where("p.category_id=?", 1);
        $select->where("p.del=?", 0);
        $select->where("p.many_request_id IS NULL");
        
        $select->group("p.id");
        
        

        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getBudgetPayment($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $select->from(array('p' => DATABASE_SALARY.'.request_office'), array(
            "p.content",
            "p.note",
            "p.payee",
            "p.contract_number",
            "p.contract_from",
            "p.contract_to",
            "p.id", 
            "total_budget" => "p.cost_temp",
            "total_use" => "IFNULL(SUM(r.cost_before),0)",
            "total_remaining" => "(p.cost_temp - IFNULL(SUM(r.cost_temp),0))"
        ));
        $select->joinLeft(array("r" => DATABASE_SALARY.".request_office"), "r.budget_id = p.id AND (r.del = 0 OR r.del IS NULL)", array());
        $select->where("p.del = 0 OR p.del IS NULL");

        if (!empty($params['department_id'])) {
            $select->where("p.department_id = ?", $params['department_id']);
        }
        
        if (!empty($params['project'])) {
            $select->where("p.project = ?", $params['project']);
        }
        
        if (!empty($params['request_type_group'])) {
            $select->where("p.request_type_group = ?", $params['request_type_group']);
        }
        
        if (!empty($params['request_type'])) {
            $select->where("p.request_type = ?", $params['request_type']);
        }
        
        if (!empty($params['company'])) {
            $select->where("p.company_id = ?", $params['company']);
        }
        
        $select->where("p.category_id = ?", 5);
        $select->group("p.id");
        
        if($_GET['dev'] == 1){
            echo $select;exit;
        }

        $result = $db->fetchAll($select);
        return $result;
    }
    
    public function getBudgetById($id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $select->from(array('p' => DATABASE_SALARY.'.request_office'), array(
            "p.content",
            "p.note",
            "p.payee",
            "p.contract_number",
            "p.contract_from",
            "p.contract_to",
            "p.id", 
            "total_budget" => "p.cost_temp",
            "total_use" => "IFNULL(SUM(r.cost_before),0)",
            "total_remaining" => "(p.cost_temp - IFNULL(SUM(r.cost_temp),0))"
        ));
        $select->joinLeft(array("r" => DATABASE_SALARY.".request_office"), "r.budget_id = p.id", array());
        
        $select->where("p.id = ?", $id);
        $select->where("(r.del = 0 OR r.del IS NULL)", NULL);
        $select->group("p.id");
        
        $result = $db->fetchRow($select);
        return $result;
    }

    public function checkTypeRequest($data)
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        if($userStorage->department == 154){//phong ban co director=manager
            if ($data["has_payment"] == null  ) {
                return 1;
            } elseif (!empty($data["has_payment"]) AND !empty($data["has_purchasing_request"])) {
                return 2;
            } elseif (!empty($data["has_payment"]) AND empty($data["has_purchasing_request"])) {
                return 3;
            }
        }elseif ($userStorage->department == 156 ){//phong ban co manager != director
            if ($data["has_payment"] == null  ) {
                return 4;
            } elseif (!empty($data["has_payment"]) AND !empty($data["has_purchasing_request"])) {
                return 5;
            } elseif (!empty($data["has_payment"]) AND empty($data["has_purchasing_request"])) {
                return 5;
            }
        }else{
            if ($data["has_payment"] == null  ) {//phong ban chi co manager
                return 7;
            } elseif (!empty($data["has_payment"])) {
                return 6;
            }

        }


    }
    
    
    public function checkStaffApproving($idRequest, $userId) //idRequest = null to get list request that user current can approve
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.request_id",
            "p.staff_id",
            "p.step",
            "p.update_payment",
            "p.finish_request",
        );

        $select->from(array('p' => 'request_confirm'), $arrCols);

        $select->where('p.is_confirm = 0');
        $select->where('p.request_id = ?', $idRequest);
        $select->where('p.staff_id = ?', $userId);
        $result = $db->fetchRow($select);

        return $result;
    }
    
    
    public function getNextFlow($type_request, $step_request)
    {
        $QRequestOfficeFlowApprove = new Application_Model_RequestOfficeFlowApprove();
        $where = array();
        $where[] = $QRequestOfficeFlowApprove->getAdapter()->quoteInto("type =? ", $type_request);
        $where[] = $QRequestOfficeFlowApprove->getAdapter()->quoteInto(" step =? AND del=0", $step_request);
        $result = $QRequestOfficeFlowApprove->fetchRow($where);

        if (!empty($result)) {
            $where = array();
            $where[] = $QRequestOfficeFlowApprove->getAdapter()->quoteInto("id =? ", $result->parent_flow);
            $res = $QRequestOfficeFlowApprove->fetchRow($where);

            return $res->step;
        }
        return -1;
    }
    public function checkCreatePurchasing($request_id, $step){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.request_id", 
            "max_step" => "MAX(p.step)", 
            "r.step",
        );

        $select->from(array('p' => 'request_confirm'), $arrCols);
        $select->joinLeft(array('r' => DATABASE_SALARY.'.request_office'), 'r.id = p.request_id', array());

        $select->where('p.request_id = ?', $request_id);
        $select->where('p.type = 1', NULL);
        $select->where('r.has_purchasing_request = 1', NULL);
        
        $result = $db->fetchRow($select);
        
        if($result['max_step'] == $step){
            return true;
        }
        else{
            return false;
        }
        
    }
    
    public function checkIsLastConfirm($request_id, $step){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.request_id", 
            "max_step" => "MAX(p.step)", 
        );

        $select->from(array('p' => 'request_confirm'), $arrCols);

        $select->where('p.request_id = ?', $request_id);
        $select->where('p.type = 1', NULL);
        
        $result = $db->fetchRow($select);
        
        if($result['max_step'] == $step){
            return true;
        }
        else{
            return false;
        }
        
    }
    
    
    public function checkSuccessInternalConfirm($request_id, $step){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.request_id", 
            "max_step" => "MAX(p.step)", 
        );

        $select->from(array('p' => 'request_confirm'), $arrCols);

        $select->where('p.request_id = ?', $request_id);
        $select->where('p.type = 1', NULL);
        
        $result = $db->fetchRow($select);
        
        if($result['max_step'] <= $step){
            return true;
        }
        else{
            return false;
        }
        
    }

    public function getNextStaffApproving($idRequest, $step, $type)
    {
        
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.staff_id",
            "p.title_confirm",
        );

        $select->from(array('p' => 'request_confirm'), $arrCols);
        
        $select->where('p.type = ?', $type);
        $select->where('p.step = ?', $step);
        $select->where('p.request_id = ?', $idRequest);
        $result = $db->fetchRow($select);

        return $result;

    }

    public function getLeaderOfStaff($staff_id)
    {
        $db = Zend_Registry::get('db');
        $sql = " SELECT  *  from  (
                SELECT
                st.`id`,
                st.`code`,
                st2.`id`  as  staff_id_manager,
                CONCAT(st.firstname,  '  ',  st.lastname  )  AS  'staff_name',
                CONCAT(  st2.firstname,  '  ',  st2.lastname  )  AS  'staff_name_leader',
                IFNULL(b.staff_code,b1.staff_code)  staff_code,
                IFNULL(b.is_leader,0)  is_leader,
                IF(IFNULL(b.is_leader,0)  =  0,  1,  0)  is_manager  ,
                IFNULL(b.staff_code,b1.staff_code)  AS  code_manager
                  FROM
                  staff  st
                  LEFT  JOIN  (  
                SELECT  *  FROM  staff_permission  WHERE  `is_leader`  =  1  and  staff_code  not  in  (13080027,  18110144)
                  )  b  ON  b.department_id  =  st.`department`  AND  (IF(st.title  =  341  ,  b.staff_code  =  '14080121',  1  =  1    )  )  AND  (IFNULL(b.team_id,  0)  =  0  OR  st.team  =  b.team_id)  AND  (IF(b.staff_code  =  '1304KH41',  st.title  =  b.title_id,  (IFNULL(b.title_id,  0)  =  0  OR  st.title  =  b.title_id)    )    )  AND  (  b.is_all_office  =  1  OR  st.office_id  =  b.office_id)  and  st.`code`  <>  b.`staff_code`
                  LEFT  JOIN  (  
                SELECT  *  FROM  staff_permission  WHERE  `is_manager`  =  1
                  )  b1  ON  b1.department_id  =  st.`department`  AND  (IF(st.title  =  341  ,  b1.staff_code  =  '14080121',  1  =  1    )  )  AND  (IFNULL(b1.team_id,  0)  =  0  OR  st.team  =  b1.team_id)  AND  (IF(b1.staff_code  =  '1304KH41',  st.title  =  b1.title_id,  (IFNULL(b1.title_id,  0)  =  0  OR  st.title  =  b1.title_id)    )    )  AND  (  b1.is_all_office  =  1  OR  st.office_id  =  b1.office_id)
                
                LEFT  JOIN  staff  st2  ON  IFNULL(b.staff_code,b1.staff_code)  =  st2.`code`
                  WHERE
                
                st.id  =  " . $staff_id . "
                  AND  st.`code`  <>  st2.`code`
                  AND  st2.`code`  NOT  IN  ('15010021',  '13080027',  '18110144',  '14080121',  '19080429')
                
                
                GROUP  BY  st.`code`  ,    st2.`code`
                      ORDER  BY  st.`code`,  st2.title  ASC
                )	b
                GROUP  BY  b.`code`";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();


        return $result[0];
    }

    public function getPermission($staff_id)
    {
        $QRequestPermissionDetail = new Application_Model_RequestPermissionDetail();
        $where = array();
        $where = $QRequestPermissionDetail->getAdapter()->quoteInto("staff_id =? ", $staff_id);
        $result = $QRequestPermissionDetail->fetchAll($where)->toArray();
        $arr_temp = array();
        foreach ($result as $key => $value) {
            foreach ($value as $k => $v) {
                if (!empty($v)) {
                    $arr_temp[$k][] = $v;

                }
            }
        }
        return $arr_temp;

    }
    
    public function getListFileFromPr($sn) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.url", 
            "p.name",
        );

        $select->from(array('p' => 'request_file_office'), $arrCols);
        $select->joinLeft(array('r' => DATABASE_SALARY.'.request_office'), 'r.id = p.request_id', array());
        
        $select->where('p.is_del = 0');
        $select->where('r.del = 0');
        $select->where('r.pr_sn = ?', $sn);
        $result = $db->fetchAll($select);

        return $result;
    }
    
    
    public function checkIsView($request_id, $staff_id){
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        $request_office = $this->fetchRow(['id = ?' => $request_id]);
        
        //1.Kiểm tra khớp người tạo
        if($userStorage->id == $request_office['created_by']){
            return true;
        }
        
        $db = Zend_Registry::get("db");
        
        //2.Kiểm tra khớp người confirm
        $select = $db->select();
        $arrCols = array(
            "p.id"
        );
        $select->from(array('p' => 'request_confirm'), $arrCols);
        $select->where('p.request_id = ?', $request_id);
        $select->where('p.staff_id = ?', $staff_id);
        $result_confirm = $db->fetchRow($select);
        
        if($result_confirm){
            return true;
        }
        
        //3.Kiểm tra người đc setup xem hêt, xem theo team, xem theo phòng ban, theo staff
        $select = $db->select();
        $arrCols = array(
            "p.id",
            "p.staff_id",
            "p.all",
            "p.team_id",
            "p.department_id",
            "p.view_staff_id",
        );
        $select->from(array('p' => 'request_view'), $arrCols);
        $select->where('p.staff_id = ?', $staff_id);
        $result_view = $db->fetchAll($select);
        
        foreach($result_view as $key=>$value){
            //all = 1: xem hết
            if($value['all'] == 1){
                return true;
            }
            
            //team_id: xem theo team
            if($value['team_id'] == $request_office['team_id']){
                return true;
            }
            
            //department_id: xem theo department
            if($value['department_id'] == $request_office['department_id']){
                return true;
            }
            
            //department_id: xem theo department
            if($value['view_staff_id'] == $request_office['created_by']){
                return true;
            }
        }
        return false;
    }

    public function getListCurrency() {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'id' => "id",
            'name' => "name",
        );

        $select->from(array('p' => 'currency'), $arrCols);

        $result = $db->fetchAll($select);

        return $result;
    }
    
}