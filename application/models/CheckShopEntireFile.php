<?php

class Application_Model_CheckShopEntireFile extends Zend_Db_Table_Abstract
{
    protected $_name = 'check_shop_entire_file';
    protected $_schema = DATABASE_TRADE;

    public function get( $check_shop_id, $image_type)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['f' => DATABASE_TRADE . '.check_shop_entire_file'], [
                'url' => 'f.url',
                'type' => 'f.type',
                'f.image_type'
            ])
            ->where('f.check_shop_id = ?', $check_shop_id)
            ->where('f.image_type = ?', $image_type);

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getImgDifferent($check_shop_id, $image_type)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.url',
            'p.type'
        );
        $select->from(array('p' => DATABASE_TRADE . '.check_shop_entire_file'), $arrCols);
        $select->where('p.check_shop_id = ?', $check_shop_id);
        $select->where('p.image_type = ?', $image_type);
        $select->where('p.type NOT IN (?)', array(0, 1, 2));
        $select->order('p.type ASC');
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getImageShop( $check_shop_id, $image_type)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['f' => DATABASE_TRADE . '.check_shop_entire_file'], [
                'url' => 'f.url',
                'type' => 'f.type',
                'f.image_type'
            ])
            ->where('f.check_shop_id = ?', $check_shop_id)
            ->where('f.image_type = ?', $image_type)
            ->where('f.type IN (?)', [0, 1, 2]);


        $result = $db->fetchAll($select);

        return $result;
    }

}