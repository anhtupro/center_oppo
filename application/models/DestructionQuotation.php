<?php

class Application_Model_DestructionQuotation extends Zend_Db_Table_Abstract
{
    protected $_name = 'destruction_quotation';
    protected $_schema = DATABASE_TRADE;

    public function getDestructionDetailsQuotation($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $select->from(array('r' => DATABASE_TRADE.'.destruction'), ['rd.id', 'rq.*'])
            ->joinLeft(array('rd' => DATABASE_TRADE.'.destruction_details'), 'r.id = rd.destruction_id', [])
            ->join(array('rq' => DATABASE_TRADE.'.destruction_quotation'), 'rd.id = rq.destruction_details_id AND rq.status = 0', [])
            ->where('r.id = ?', $params['destruction_id']);

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $destruction_quotation [$element ['destruction_details_id'] ] [] = $element;
            $sum_total_price_quotation [$element ['destruction_details_id'] ] += $element['total_price'] + $element['total_price'] * VAT;
        }

        $result = [
            'destruction_quotation' => $destruction_quotation,
            'sum_total_price_quotation' =>  $sum_total_price_quotation
        ];

        return $result;
    }

    public function getDestructionDetailsQuotationLast($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $select->from(array('r' => DATABASE_TRADE.'.destruction'), ['rd.id', 'rq.*'])
            ->joinLeft(array('rd' => DATABASE_TRADE.'.destruction_details'), 'r.id = rd.destruction_id', [])
            ->joinLeft(array('rq' => DATABASE_TRADE.'.destruction_quotation'), 'rd.id = rq.destruction_details_id', [])
            ->where('r.id = ?', $params['destruction_id']);

        $select->where('rq.status = 2');

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $destruction_quotation [$element ['destruction_details_id'] ] [] = $element;
            $sum_total_price_quotation [$element ['destruction_details_id'] ] += $element['total_price'] + $element['total_price'] * VAT;
        }

        $result = [
            'destruction_quotation' => $destruction_quotation,
            'sum_total_price_quotation' =>  $sum_total_price_quotation
        ];

        return $result;
    }
}