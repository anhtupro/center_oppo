<?php
class Application_Model_Contructors extends Zend_Db_Table_Abstract
{
	protected $_name = 'contructors';
    protected $_schema = DATABASE_TRADE;
	public function GetAll(){
     
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare('
            SELECT id,name,short_name,code FROM `'.DATABASE_TRADE.'`.'.$this->_name);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $db = $stmt = null;
        return $data;
    }

    public function getCode(){

        $db     = Zend_Registry::get("db");
        $select = $db->select();
            
        $arrCols = array(
            'id'     => "p.id",
            'code'   => "p.code"
        );
        
        $select->from(array('p'=> DATABASE_TRADE.'.contructors'), $arrCols);
        
        $result  = $db->fetchAll($select);

        $data = [];

        foreach($result as $key=>$value){
            $data[$value['code']] = $value['id'];
        }
        
        return $data;
    }

    // public function GetList($params){
    //     $db = Zend_Registry::get('db');
    //     $stmt = $db->prepare('
    //         SELECT id FROM Contructos where id = 
    //     ');
    //     $stmt->execute();
    //     $data = $stmt->fetchAll();
    //     $db = $stmt = null;
    //     return $data;
    // }

}