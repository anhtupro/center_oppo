<?php

class Application_Model_TimingCheckRealme extends Zend_Db_Table_Abstract
{
    public function checkImeiDealer($imei, $store)
    {
        $db = Zend_Registry::get('db');

        $stmt = $db->prepare('CALL `SP_imei_map_CheckImei_realme`(:imei, :store)');
        $stmt->bindParam('imei', $imei);
        $stmt->bindParam('store', $store, PDO::PARAM_INT);

        $stmt->execute();
        $res = $stmt->fetch();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $res;
    }
    public function checkImeiDuAn($imei){
        $db = Zend_Registry::get('db');

        $sql = 'SELECT * FROM realme_hr.imei_du_an WHERE imei_sn = :imei';

        $stmt = $db->prepare($sql);
        $stmt->bindParam('imei', $imei);

        $stmt->execute();
        $res = $stmt->fetch();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $res;
    }

    public function checkImeiTranAnh($imei,$store){
        $db = Zend_Registry::get('db');

        $sql = 'SELECT imei_sn,
                CASE WHEN d.title  LIKE "%viettel%" THEN 1 ELSE 2 END as dealer,
                CASE  WHEN s.`name` LIKE "%trần anh%" THEN 1 ELSE 2 END as store, 
                d.title,s.`name`
                from realme_warehouse.imei i 
                JOIN realme_warehouse.distributor d 
                    ON i.distributor_id = d.id
                JOIN store s 
                    ON s.id = :store
                WHERE imei_sn = :imei';

        $stmt = $db->prepare($sql);
        $stmt->bindParam('imei', $imei);
        $stmt->bindParam('store', $store, PDO::PARAM_INT);
        $stmt->execute();
        $res = $stmt->fetch();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $res;
    }
    public function insertImeiDuAnLog($staff_id,$imei,$store)
    {     

            $timing = date("Y-m-d H:i:s");

            $db = Zend_Registry::get('db');
            $sql = "INSERT INTO imei_du_an_log_realme (staff_id,imei,store_id,timing) VALUES (:staff_id,:imei,:store_id,:timing)";
            $stmt = $db->prepare($sql);
            $stmt->bindParam("staff_id",  $staff_id, PDO::PARAM_STR);
            $stmt->bindParam("imei",  $imei, PDO::PARAM_STR);
            $stmt->bindParam("store_id",  $store, PDO::PARAM_STR);
            $stmt->bindParam("timing",  $timing);
            $stmt->execute();
            $stmt->closeCursor();
            $db = $stmt = null; 
           
    }

    public function checkImeiBrandshop($imei,$store){
        $db = Zend_Registry::get('db');

        $sql = '
                SELECT i.imei_sn,IF(d.title LIKE "%OPPO Brandshop%",1,2) check_sell_in,d.is_price_retail_demo check_brs,s.is_brand_shop,brand.type
                FROM realme_warehouse.imei i
                JOIN realme_warehouse.distributor d
                    ON i.distributor_id = d.id
                LEFT JOIN store s
                    ON s.id = :store
                LEFT JOIN imei_daily_brandshop brand
                    ON i.imei_sn = brand.imei_sn
                WHERE i.imei_sn = :imei';

        $stmt = $db->prepare($sql);
        $stmt->bindParam('imei', $imei);
        $stmt->bindParam('store', $store, PDO::PARAM_INT);
        $stmt->execute();
        $res = $stmt->fetch();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $res;
    }

}
