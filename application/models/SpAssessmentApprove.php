<?php
class Application_Model_SpAssessmentApprove extends Zend_Db_Table_Abstract
{
    protected $_name = 'sp_assessment_approve';
    
    public function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            "p.staff_id",
            "fullname" => "CONCAT(s.firstname, ' ', s.lastname)",
            "code" => "s.code",
            "team_name" => "team.name",
            "title_name" => "title.name",
            "area_name" => "a.name",
            "province_name" => "r.name",
            "p.result",
            "p.created_at",
            "p.confirmed_at",
        );

        $select->from(array('p' => 'sp_assessment_approve'), $arrCols);
        $select->joinLeft(array('s' => 'staff'), 's.id = p.staff_id', array());
        $select->joinLeft(array('team' => 'team'), 'team.id = s.team', array());
        $select->joinLeft(array('title' => 'team'), 'title.id = s.title', array());
        $select->joinLeft(array('r' => 'regional_market'), 's.regional_market = r.id', array());
        $select->joinLeft(array('a' => 'area'), 'a.id = r.area_id', array());
        
        if(!empty($params['area_id'])){
            $select->where('a.id IN (?)',$params['area_id']);
        }elseif(!empty($params['area_list'])){
            $select->where('a.id IN (?)',$params['area_list']);
        }
        if(!empty($params['is_approved']) AND $params['is_approved'] == 2){
            $select->where('p.confirmed_at IS NOT NULL');
        }elseif(!empty($params['is_approved']) AND $params['is_approved'] ==1){
            $select->where('p.confirmed_at IS NULL');
        }
        if(!empty($params['has_image']) AND $params['has_image'] == 2){
            $select->where('s.photo IS NOT NULL');
        }elseif(!empty($params['has_image']) AND $params['has_image'] ==1){
            $select->where('s.photo IS NULL');
        }

        

        
        if(!empty($params['name'])){
            $select->where('CONCAT(s.firstname, " ",s.lastname) LIKE ?', '%'.$params['name'].'%');
        }
        
        if(!empty($params['code'])){
            $select->where('s.code = ?',$params['code']);
        }
        
        $select->order('id DESC');
        
        if ($limit)
            $select->limitPage($page, $limit);
        
        $result = $db->fetchAll($select);
        
        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
    
    public function getStaffInfo($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.staff_id",
            "fullname" => "CONCAT(s.firstname, ' ', s.lastname)",
            "code" => "s.code",
            "team_name" => "team.name",
            "title_name" => "title.name",
            "area_name" => "a.name",
            "province_name" => "r.name",
            "p.result",
            "p.created_at",
            "p.confirmed_at",
            "p.note",
            "s.photo",
            "d.total_attendance",
            "d.total_ot",
            "d.total_kpi",
            "d.total_warning",
            "d.total_reward",
            "d.total_pass",
            "d.total_fail",
            "d.total",
            "d.thamnien"
        );

        $select->from(array('p' => 'sp_assessment_approve'), $arrCols);
        $select->joinLeft(array('s' => 'staff'), 's.id = p.staff_id', array());
        $select->joinLeft(array('team' => 'team'), 'team.id = s.team', array());
        $select->joinLeft(array('title' => 'team'), 'title.id = s.title', array());
        $select->joinLeft(array('r' => 'regional_market'), 's.regional_market = r.id', array());
        $select->joinLeft(array('a' => 'area'), 'a.id = r.area_id', array());
        $select->joinLeft(array('d' => 'sp_assessment_temp_date_approve'), 'd.staff_id = p.staff_id AND d.date = DATE(p.created_at)', array());
        
        $select->where('p.id = ?', $params['id']);


        // echo $select->__toString();
        $result = $db->fetchRow($select);


       if($_GET['dev']){
            $select2 = $db->select();
            $select2->from(['v'=>'v_exclude_score_pg_up_star'],['*']);
            $select2->where('v.id =? ',$params['id']);
            $result2=$db->fetchRow($select2);

            echo "<pre>";
            print_r($result);
            print_r($result2);
            foreach($result2 as $key => $value){
                if(!empty($result[$key])) {
                    if($result[$key] > $value){
                        $result[$key]=$result[$key]+($value-$result[$key]);
                    }elseif($result[$key] < $value){
                        $result[$key]=$result[$key]+($result[$key]-$value);
                    }
                }
            }
        }

        return $result;
    }
    
    
    public function getStoreImage($store_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "f.url",
            "f.checkshop_id",
        );

        $select->from(array('p' => DATABASE_TRADE.'.app_checkshop'), $arrCols);
        $select->joinLeft(array('f' => DATABASE_TRADE.'.app_checkshop_file'), 'f.checkshop_id = p.id', array());
        
        $select->where("p.store_id = ?", $store_id);
        $select->where("f.type IN (0,1,2)");
        $select->where("p.id = (SELECT MAX(id) FROM trade_marketing.app_checkshop WHERE store_id = ?)", $store_id);

        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function checkIsApprove($id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
        );

        $select->from(array('p' => 'staff'), $arrCols);
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());
        
        $select->where("p.id = ?", $id);
        $select->where("r.area_id IN (?)", [1,24,25,26,34,10,32,33,51,7,13,4]);

        $result = $db->fetchRow($select);

        return $result;
    }
}