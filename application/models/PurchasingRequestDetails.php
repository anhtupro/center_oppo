<?php
class Application_Model_PurchasingRequestDetails extends Zend_Db_Table_Abstract
{
    protected $_name = 'purchasing_request_details';

    
    function getListProduct($params){
        
        if(empty($params['sn'])){
            return false;
        }
        
        $db = Zend_Registry::get('db');

        $col = array(
                "p.id",
                "p.*",            
                "c.unit", 
            
        );

        $select = $db->select()->from(array('p' => 'purchasing_request_details'), $col);
        $select->joinleft(array('d'=>'purchasing_request'), 'd.id = p.pr_id', array('ids' => 'd.id',"d.confirm_by","d.approved_by"));
        $select->joinleft(array('c'=>'pmodel_code'), 'c.id = p.pmodel_code_id', array());

        if(!empty($params['sn'])){
            $select->where('d.sn = ?', $params['sn']);
        }
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
    function getListProductInfo($params){
        
        if(empty($params['sn'])){
            return false;
        }
        
        $db = Zend_Registry::get('db');

        $col = array(
            "p.id",
            "p.pmodel_code_id",
            "product_name" => "c.name",
            "product_sub_name" => "c.sub_name",
            "p.quantity",
            "c.unit",
            "p.installation_at",
            "p.installation_time",
            "p.note"
        );

        $select = $db->select()->from(array('p' => 'purchasing_request_details'), $col);
        $select->joinleft(array('d'=>'purchasing_request'), 'd.id = p.pr_id', array('ids' => 'd.id',"d.confirm_by","d.approved_by"));
        $select->joinleft(array('c'=>'pmodel_code'), 'c.id = p.pmodel_code_id', array());

        if(!empty($params['sn'])){
            $select->where('d.sn = ?', $params['sn']);
        }
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
    function getData($params){

        $db = Zend_Registry::get('db');

        $col = array(
                "p.id",
                "c.name", 
                "code_name" => "CONCAT(IFNULL(c.`code`,''),' - ', IFNULL(c.`name`,''),' - ', IFNULL(c.`sub_name`,''))",
                "d.quantity", 
                "d.note",
                "c.unit",  
                "d.code_supplier_id",
                "d.pmodel_code_id",
            
        );

        $select = $db->select()->from(array('p' => 'purchasing_request'), $col);

        $select->joinleft(array('d'=>'purchasing_request_details'), 'd.pr_id = p.id', array('ids' => 'd.id'));
        $select->joinleft(array('c'=>'pmodel_code'), 'c.id = d.pmodel_code_id', array());

        if(!empty($params['sn'])){
            $select->where('p.sn = ?', $params['sn']);
        }

        $result = $db->fetchAll($select);

        return $result;
    }

    function getDataPo($params){

        $db = Zend_Registry::get('db');

        $col = array(
                'p.id', 
                'p.pr_id', 
                'p.pmodel_code_id',
                'c.unit',
                'quantity' => '(p.quantity-p.po_quantity)', 
                'p.status', 
                'r.sn',
                'r.area_id',
                'r.department_id',
                'created_by' => "CONCAT(staff.firstname, ' ', staff.lastname)", 
                'r.urgent_date',
                'pmodel_name' => 'c.name', 
                'pmodel_code' => 'c.code',
                'cs.supplier_id', 
                'cs.price', 
                'supplier_id' => 'cs.supplier_id',
                'code_supplier_id' => 'cs.id',
        );

        $select = $db->select()->from(array('p' => 'purchasing_request_details'), $col);

        $select->joinleft(array('r'=>'purchasing_request'), 'r.id = p.purchasing_request_id', array());
        $select->joinleft(array('c'=>'pmodel_code'), 'c.id = p.pmodel_code_id', array());
        $select->joinleft(array('cs'=>'code_supplier'), 'cs.code_id = p.pmodel_code_id', array());
        $select->joinleft(array('staff'=>'staff'), 'staff.id = r.created_by', array());

        $select->where('p.`status` IN (?)', array('Confirm','Delivery'));
        $select->where('cs.`default` = ?', 1);
        $select->where('(p.quantity-p.po_quantity) > ?', 0);

        if(!empty($params['supplier_id'])){
            $select->where('cs.supplier_id = ?', $params['supplier_id']);
        }

        if(!empty($params['area_id'])){
            $select->where('r.area_id IN (?)', $params['area_id']);
        }
        if(!empty($params['department_id'])){
            $select->where('r.department_id IN (?)', $params['department_id']);
        }
        
        if(!empty($params['created_at_from'])){
            $select->where('DATE(r.urgent_date) >= ?', $params['created_at_from']);
        }

        if(!empty($params['created_at_to'])){
            $select->where('DATE(r.urgent_date) <= ?', $params['created_at_to']);
        }

        $select->group('p.id');
        $select->order(array('cs.supplier_id ASC','p.id'));

        $result = $db->fetchAll($select);

        return $result;
    }

    function getDataEditPo($params){

        $db = Zend_Registry::get('db');

        $col = array(
                'p.id', 
                'p.purchasing_request_id', 
                'p.pmodel_code_id',
                'quantity_pending' => '(p.quantity- p.po_quantity) + IFNULL(d.quantity,0)', 
                'quantity' => 'IFNULL(d.quantity, (p.quantity-p.po_quantity))', 
                'p.status', 
                'c.unit',
                'r.sn', 
                'r.area_id', 
                'r.department_id', 
                'created_by' => "CONCAT(staff.firstname, ' ', staff.lastname)", 
                'r.urgent_date',
                'pmodel_name' => 'c.name', 
                'pmodel_code' => 'c.code',
                'cs.supplier_id', 
                'cs.price', 
                'supplier_id' => 'cs.supplier_id',
                'code_supplier_id' => 'cs.id',
                'd.po_id'
        );

        $select = $db->select()->from(array('p' => 'purchasing_request_details'), $col);

        $select->joinleft(array('r'=>'purchasing_request'), 'r.id = p.purchasing_request_id', array());
        $select->joinleft(array('d'=>'purchasing_order_details'), 'd.pr_details_id = p.id', array());
        $select->joinleft(array('c'=>'pmodel_code'), 'c.id = p.pmodel_code_id', array());
        $select->joinleft(array('cs'=>'code_supplier'), 'cs.code_id = p.pmodel_code_id', array());
        $select->joinleft(array('staff'=>'staff'), 'staff.id = r.created_by', array());

        $select->where('p.`status` IN (?)', array('Confirm','Delivery'));
        $select->where('cs.`default` = ?', 1);
        $select->where('(p.quantity-p.po_quantity+IFNULL(d.quantity,0)) > ?', 0);
        $select->where('d.po_id IS NULL OR d.po_id = ?', $params['po_id']);

        if(!empty($params['supplier_id'])){
            $select->where('cs.supplier_id = ?', $params['supplier_id']);
        }

        if(!empty($params['area_id'])){// search nhưng vẫn giữ những PR đã chọn
            $select->where('d.po_id IS NOT NULL OR (d.po_id IS NULL AND r.area_id IN(?))', $params['area_id']);
        }

        if(!empty($params['created_at_from'])){ 
            $select->where('d.po_id IS NOT NULL OR (d.po_id IS NULL AND DATE(r.urgent_date) >= ?)', $params['created_at_from']);
        }

        if(!empty($params['created_at_to'])){
            $select->where('d.po_id IS NOT NULL OR (d.po_id IS NULL AND DATE(r.urgent_date) <= ?)', $params['created_at_to']);
        }

        $select->group('p.id');
        $select->order(array('d.id DESC','p.id ASC'));
        $result = $db->fetchAll($select);

        return $result;
    }

    function getDetails($sn){

        if(empty($sn)){
            return false;
        }

        $db = Zend_Registry::get('db');

        $col = array(
            'p.id',
            'p.pr_id',
            'p.pmodel_code_id',
            'quantity_start'  => 'p.quantity',
            'quantity'        => 'IFNULL(p.quantity_real, p.quantity)',
            'quantity_real'   => 'p.quantity_real',
            'r.status',
            'r.sn',
            'r.urgent_date',
            'pmodel_name'     => 'c.name',
            'pmodel_code'     => 'c.code',
            'code_name'       => "CONCAT(c.code,' - ',c.name)",
            'price_before_dp' => "IFNULL(prs.price_before_dp,0)",
            'price'           => "IFNULL(prs.price,0)",
            'ck'           => "IFNULL(prs.ck,0)",
            'fee'           => "IFNULL(prs.fee,0)",
            'total'           => "IFNULL(prs.price,0)*IFNULL(p.quantity,0)",
            'supplier_name'   => 'sup.title',
            'prs.currency'
                //'cs.supplier_id', 
                //'cs.price', 
                //'supplier_id' => 'cs.supplier_id',
                //'code_supplier_id' => 'cs.id',
        );

        $select = $db->select()->from(array('p' => 'purchasing_request_details'), $col);

        $select->joinleft(array('r'=>'purchasing_request'), 'r.id = p.pr_id', array());
        $select->joinleft(array('c'=>'pmodel_code'), 'c.id = p.pmodel_code_id', array());
        $select->joinleft(array('prs'=> DATABASE_SALARY.'.purchasing_request_supplier'), 'p.id = prs.pr_detail_id AND prs.select_supplier = 1', array());        
        $select->joinleft(array('sup'=>'supplier'), 'prs.supplier_id = sup.id', array());
        //$select->joinleft(array('cs'=>'code_supplier'), 'cs.code_id = p.pmodel_code_id', array());

        $select->where('r.sn =?', $sn);
        
        $select->group('p.id');
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
    function getDetailsToVnd($sn){

        if(empty($sn)){
            return false;
        }

        $db = Zend_Registry::get('db');

        $col = array(
            'p.id',
            'p.pr_id',
            'p.pmodel_code_id',
            'quantity_start'  => 'p.quantity',
            'quantity'        => 'IFNULL(p.quantity_real, p.quantity)',
            'quantity_real'   => 'p.quantity_real',
            'r.status',
            'r.sn',
            'r.urgent_date',
            'pmodel_name'     => 'c.name',
            'pmodel_code'     => 'c.code',
            'code_name'       => "CONCAT(c.code,' - ',c.name)",
            'price_before_dp' => "(IFNULL(prs.price_before_dp,0)*currency.to_vnd)",
            'price'           => "(IFNULL(prs.price,0)*currency.to_vnd)",
            'ck'           => "(IFNULL(prs.ck,0)*currency.to_vnd)",
            'fee'           => "(IFNULL(prs.fee,0)*currency.to_vnd)",
            'total'           => "(IFNULL(prs.price,0)*IFNULL(p.quantity,0)*currency.to_vnd)",
            'supplier_name'   => 'sup.title',
            'prs.currency'
                //'cs.supplier_id', 
                //'cs.price', 
                //'supplier_id' => 'cs.supplier_id',
                //'code_supplier_id' => 'cs.id',
        );

        $select = $db->select()->from(array('p' => 'purchasing_request_details'), $col);

        $select->joinleft(array('r'=>'purchasing_request'), 'r.id = p.pr_id', array());
        $select->joinleft(array('c'=>'pmodel_code'), 'c.id = p.pmodel_code_id', array());
        $select->joinleft(array('prs'=> DATABASE_SALARY.'.purchasing_request_supplier'), 'p.id = prs.pr_detail_id AND prs.select_supplier = 1', array());        
        $select->joinleft(array('sup'=>'supplier'), 'prs.supplier_id = sup.id', array());
        $select->joinleft(array('currency'=>'currency'), 'currency.id = prs.currency', array());
        //$select->joinleft(array('cs'=>'code_supplier'), 'cs.code_id = p.pmodel_code_id', array());

        $select->where('r.sn =?', $sn);
        
        $select->group('p.id');
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
    
    function reportPR($params){
        
        $db = Zend_Registry::get('db');
        $select_table_temp = "(            
                            SELECT b.sn, b.urgent_date, pp.`name` as project, b.remark, b.`status`,
                            ar.`name` as area, dep.`name` as department,
                            pc.`code` as pmodel_code, pc.`name` as product, a.quantity as quantity_order, a.po_quantity as quantity_created_po, a.price,
                            (a.price*1.1) as after_vat, (a.price*a.quantity*1.1) as price_total_order,
                            table_total.total_pr_order, table_total.total_created_po,
                             a.`note`,
                            CONCAT(s.firstname,' ',s.lastname) AS created_by_name, b.created_at, 
                            CONCAT(s2.firstname,' ',s2.lastname) AS confirm_by_name, b.confirm_at,
                            b.created_by, b.project_id
                            FROM `purchasing_request_details` a
                            JOIN purchasing_request b ON a.purchasing_request_id = b.id
                            LEFT JOIN purchasing_project pp ON b.project_id = pp.id
                            LEFT JOIN staff s ON b.created_by = s.id
                            LEFT JOIN staff s2 ON b.confirm_by = s2.id
                            LEFT JOIN area ar ON ar.id = b.area_id
                            LEFT JOIN department dep ON b.department_id = dep.id
                            LEFT JOIN pmodel_code pc ON a.pmodel_code_id = pc.`id`
                            LEFT JOIN ( SELECT a.id, SUM(a.price*a.quantity*1.1) AS total_pr_order, SUM(a.price*a.po_quantity*1.1) AS total_created_po
                                    FROM purchasing_request_details a
                                    GROUP BY a.purchasing_request_id ) table_total ON a.id = table_total.id
                            )";
        
        $select = $db->select()
        ->from(array('p' => new Zend_Db_Expr($select_table_temp)),
            array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.sn'), 'p.*'));
        
        if(!empty($params['sn'])){
            $select->where('p.sn = ?', $params['sn']);
        }
        if(!empty($params['pmodel_code'])){
            $select->where('p.pmodel_code LIKE ?', '%'.$params['pmodel_code'].'%');
        }
        if(!empty($params['project_id'])){
            $select->where('p.project_id = ?', '%'.$params['project_id'].'%');
        }
        if(!empty($params['urgent_from'])){
            $select->where('DATE(p.urgent_date) >= ?', $params['urgent_from']);
        }
        if(!empty($params['urgent_to'])){
            $select->where('DATE(p.urgent_date) <= ?', $params['urgent_to']);
        }
        if(!empty($params['list_staff'])){
            $select->where('p.created_by IN (?)', $params['list_staff']);
        }
        
        $select->order('p.created_at DESC');
        $result = $db->fetchAll($select);
        return $result;
    }
    
    
    
        public function getCheckSelectSupplier($sn)
        {
            $db = Zend_Registry::get('db');
            $data = array();
            
            if(!empty($sn)){
                
                $sql = "SELECT p.*, prs.supplier_id FROM `purchasing_request_details` p
                        LEFT JOIN `purchasing_request` pr ON p.pr_id = pr.id
                        LEFT JOIN ".DATABASE_SALARY.".purchasing_request_supplier prs ON p.id = prs.pr_detail_id AND prs.select_supplier = 1
                        WHERE pr.sn = '".$sn."' 
                        AND ( prs.supplier_id is NULL OR prs.price = 0 )";

                $stmt = $db->prepare($sql);
                $stmt->execute();
                $data = $stmt->fetchAll();
                $stmt->closeCursor();
                $stmt = $db = null;
            }

            return $data;
        }
        
        
    
        public function getCreateSubmissionSupplier($params)
        {
            $db = Zend_Registry::get('db');
            $data = array();             

            $string_where = "";
            
            if(!empty($params['sn'])){
                $string_where.= " AND pr.sn = '".$params['sn']."'";
            }
            if(!empty($params['name'])){
                $string_where.= " AND pr.`name` LIKE '%".$params['name']."%'";
            }
            if(!empty($params['supplier_id'])){
                $string_where.= " AND prs_1.supplier_id = ".$params['supplier_id'];
            }
            if(!empty($params['urgent_from'])){
                $string_where.= " AND pr.urgent_date >= '".$params['urgent_from']."'";
            }
            if(!empty($params['urgent_to'])){
                $string_where.= " AND pr.urgent_date <= '".$params['urgent_to']."'";
            }
            if(!empty($params['project_id'])){
                $string_where.= " AND pr.project_id = ".$params['project_id'];
            }
            
            if(!empty($params['list_detail_id'])){
                $string_where.= " AND prd.id IN (".$params['list_detail_id'].")";
            }
            
            $sql = "SELECT pr.sn, pr.type, pr.department_id, prd.id as prd_id, prd.installation_at, prd.installation_time,
		prd.*, pm.`name` product, pm.unit, pr.`name` pr_name,

		prs_1.supplier_id AS supplier_1, 
		prs_1.price AS price_1, 
		prs_1.ck AS ck_1, 
		prs_1.fee AS fee_1, 
		(IFNULL(prd.quantity,0)*(IFNULL(prs_1.price,0) - IFNULL(prs_1.price,0)*IFNULL(prs_1.ck,0)/100)+IFNULL(prs_1.fee,0) ) AS total_1,
		prs_1.article AS article_1,  
		prs_1.warranty AS warranty_1,
		prs_1.discount AS discount_1,
		prs_1.article_other AS article_other_1,

		prs_2.supplier_id AS supplier_2, 
		prs_2.price AS price_2, 
		prs_2.ck AS ck_2, 
		prs_2.fee AS fee_2, 
		(IFNULL(prd.quantity,0)*(IFNULL(prs_2.price,0) - IFNULL(prs_2.price,0)*IFNULL(prs_2.ck,0)/100)+IFNULL(prs_2.fee,0) ) AS total_2,
		prs_2.article AS article_2, 
		prs_2.warranty AS warranty_2,
		prs_2.discount AS discount_2,
		prs_2.article_other AS article_other_2,
                
		prs_3.supplier_id AS supplier_3, 
		prs_3.price AS price_3, 
		prs_3.ck AS ck_3, 
		prs_3.fee AS fee_3, 
		(IFNULL(prd.quantity,0)*(IFNULL(prs_3.price,0) - IFNULL(prs_3.price,0)*IFNULL(prs_3.ck,0)/100)+IFNULL(prs_3.fee,0) ) AS total_3,
		prs_3.article AS article_3, 
		prs_3.warranty AS warranty_3,
		prs_3.discount AS discount_3,
		prs_3.article_other AS article_other_3,
                
		prs_h.supplier_id AS supplier_h, 
		prs_h.price AS price_h, 
		prs_h.ck AS ck_h, 
		prs_h.fee AS fee_h,
		prs_h.article AS article_h

                FROM `purchasing_request_details` prd
                LEFT JOIN purchasing_request pr ON prd.pr_id = pr.id
                LEFT JOIN ".DATABASE_SALARY.".purchasing_request_supplier prs_1 ON prd.id = prs_1.pr_detail_id AND prs_1.select_supplier = 1
                LEFT JOIN ".DATABASE_SALARY.".purchasing_request_supplier prs_2 ON prd.id = prs_2.pr_detail_id AND prs_2.supplier_id <> prs_1.supplier_id
                LEFT JOIN ".DATABASE_SALARY.".purchasing_request_supplier prs_3 ON prd.id = prs_3.pr_detail_id AND prs_3.supplier_id <> prs_1.supplier_id AND prs_3.supplier_id <> prs_2.supplier_id
                LEFT JOIN (
                                SELECT MAX(pr.id), prd.pmodel_code_id, prs.* FROM `purchasing_request_details` prd
                                LEFT JOIN `purchasing_request` pr ON prd.pr_id = pr.id AND pr.`status` = 3
                                LEFT JOIN ".DATABASE_SALARY.".purchasing_request_supplier prs ON prd.id = prs.pr_detail_id AND prs.select_supplier = 1
                                WHERE pr.`status` = 3
                            ) prs_h ON prd.pmodel_code_id = prs_h.pmodel_code_id
                LEFT JOIN pmodel_code pm ON prd.pmodel_code_id = pm.id
                WHERE pr.`status` = 2 AND pr.del = 0 
                ".$string_where."
                GROUP BY prd.id
                ORDER BY pr.id, prd.id";

            $stmt = $db->prepare($sql);
            $stmt->execute();
            $data = $stmt->fetchAll();
            $stmt->closeCursor();
            $stmt = $db = null;


            return $data;
        }
        
        function getDetailsRequestBySn($sn, $request_id) {
            
            if(empty($sn)){
                return;
            }
            
            $db = Zend_Registry::get("db");
            $select = $db->select();

            $arrCols = array(
                'p.id',
                'p.content',
                'p.cost_temp', 
                'p.pr_sn', 
                'p.currency',
                'p.finance_payment_date'
            );

            $select->from(array('p' => DATABASE_SALARY.'.request_office'), $arrCols);

            $select->where('p.del = 0');
            $select->where('p.category_id <> 5');
            $select->where("p.pr_sn = ?", $sn);
            
            if(!empty($request_id)){
                $select->where('p.id <> ?', $request_id);
            }
            
            $result = $db->fetchAll($select);

            return $result;
        }

}



