<?php
class Application_Model_AppContractorIn extends Zend_Db_Table_Abstract
{
    protected $_name = 'app_contractor_in';
    protected $_schema = DATABASE_TRADE;

    public function getInfoIn($params){

    	$db     = Zend_Registry::get("db");
    	$select = $db->select();
    	    
    	$arrCols = array(
    		'p.id',
    	    'p.contractor_id', 
    	    'c.name', 
    	    'campaign_id' 	=> 'campaign.id', 
    	    'campaign_name' => 'campaign.name',
    	    'category_name' => 'cat.name',
    	    'p.category_id', 
    	    'p.quantity', 
    	    'p.created_by', 
    	    'p.created_at'
    	);
    	
    	$select->from(array('p'=> DATABASE_TRADE.'.app_contractor_quantity'), $arrCols);
    	$select->joinLeft(array('campaign' => DATABASE_TRADE.'.campaign_demo'), 'campaign.id = p.campaign_id', array());
    	$select->joinLeft(array('c' => DATABASE_TRADE.'.contructors'), 'c.id = p.contractor_id', array());
    	$select->joinLeft(array('cat' => DATABASE_TRADE.'.category'), 'cat.id = p.category_id', array());
    	$select->where('p.id = ?', $params['contractor_quantity_id']);
    	
    	$result  = $db->fetchRow($select);
    	
    	return $result;
    }

    public function getListImei($params){

    	$db     = Zend_Registry::get("db");
    	$select = $db->select();
    	    
    	$arrCols = array(
    	    'p.contractor_id', 
    	    'p.campaign_id', 
    	    'p.category_id', 
    	    'p.imei_sn', 
    	    'created_at'
    	);
    	
    	$select->from(array('p'=> DATABASE_TRADE.'.app_contractor_in'), $arrCols);

    	$select->where('p.contractor_id = ?', $params['contractor_id']);
    	$select->where('p.campaign_id = ?', $params['campaign_id']);
    	$select->where('p.category_id = ?', $params['category_id']);
    	
    	$result  = $db->fetchAll($select);
    	
    	return $result;
    }
    
}