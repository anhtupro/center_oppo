<?php
class Application_Model_CapacityDictionary extends Zend_Db_Table_Abstract
{
    protected $_name = 'capacity_dictionary';
    
    function fetchPagination($page, $limit, &$total, $params){
        
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            'title' => "p.title",
            'type' => 'p.type'
        );

        $select->from(array('p' => 'capacity_dictionary'), $arrCols);
        
        if(!empty($params['title'])){
            $select->where('p.title LIKE ?', '%'.$params['title'].'%');
        }
        
        if(!empty($params['type'])){
            $select->where('p.type = ?', $params['type']);
        }
        
        $select->where('p.is_deleted = 0 OR p.is_deleted IS NULL', NULL);
        $select->order(['p.type', 'p.title']);

        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
    
    function getDictionaryLevel(){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "l.level", 
            "level_title" => "l.title",
        );

        $select->from(array('p' => 'capacity_dictionary'), $arrCols);
        $select->joinLeft(array('l' => 'capacity_dictionary_level'), 'p.id = l.dictionary_id', array());

        $select->order(['p.id', 'l.level DESC']);
        $result = $db->fetchAll($select);

        return $result;
    }
    
}