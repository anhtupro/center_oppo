<?php
class Application_Model_Time2 extends Zend_Db_Table_Abstract
{
    protected $_name = 'time';

    function fetchPagination($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get('db');
        if (isset($params['month']) and $params['month']) {
            $select = $db->select();
            $select_fields = array(
                new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
                'total_date' => 'COUNT(p.staff_id)',
                'staff_id',
                'status');

            if (isset($params['get_fields']) and is_array($params['get_fields']))
                foreach ($params['get_fields'] as $get_field)
                    array_push($select_fields, $get_field);
                else
                    array_push($select_fields, 'p.*');

            $select->from(array('p' => $this->_name), $select_fields)->group('p.staff_id');
        } else {
            if ($limit) {
                $select = $db->select()->from(array('p' => $this->_name), array(new Zend_Db_Expr ('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));
            } else {
                $select = $db->select()->from(array('p' => $this->_name), array('p.*'));
            }
        }

        $select->distinct();
        
        $select->join(array('s' => 'staff'), 'p.staff_id = s.id', array(
            's.email',
            's.firstname',
            's.lastname',
            's.department',
            's.team',
            's.title',
            's.regional_market',
            's.phone_number'));

        //join bang ngay phep
        $select->joinLeft(array('o' => 'off_date'), 'p.staff_id = o.id', array('o.date'));
        $select->joinLeft(array('s2'=>'staff'),'s2.id = p.approved_by',array('approved_name'=>'CONCAT(s2.firstname," ", s2.lastname)'));

        if (isset($params['email']) and $params['email']) {
            $params['email'] = preg_replace('/' . EMAIL_SUFFIX . '/', '', $params['email']);
            $select->where('s.email LIKE ?', $params['email'] . EMAIL_SUFFIX);
        }

        if (isset($params['month']) and $params['month']) {
            $select->where('MONTH(p.created_at) = ?', $params['month']);
        }

        if (isset($params['team']) and $params['team']) {
            $select->where('s.team in (?)', $params['team']);
        }

        if (isset($params['title']) and $params['title']) {
            $select->where('s.title in (?)', $params['title']);
        }

        if (isset($params['department']) and $params['department']) {
            $select->where('s.department in (?)', $params['department']);
        }

        if (isset($params['code']) and $params['code']){
            $select->where('s.code LIKE ?', '%' . $params['code'] . '%');
        }

        if (isset($params['off']) and $params['off']) {
            if ($params['off'] == 1)
                $select->where('p.off = ?', $params['off']);
            else
                $select->where('p.off = ?', '0');
        }

        if (isset($params['name']) and $params['name']){
            $select->where('CONCAT(s.firstname, " ",s.lastname) LIKE ?', '%' . $params['name'] .'%');
        }

        if (isset($params['staff_id']) and $params['staff_id']) {
            $select->where('s.id = ?', $params['staff_id']);

        } elseif (isset($params['sale']) and $params['sale']) {

            $QStore = new Application_Model_Store();
            $array_staff_check   = $QStore->get_pg_store_sale($params['sale']);
            $array_staff_check[] = $params['sale'];

            if(is_array($array_staff_check) and $array_staff_check){
                $select->where('s.id in (?)', $array_staff_check);
            }
                
        } elseif (isset($params['leader']) and $params['leader']) {
            $select->where('s.id in (?)', $this->getStaffForTiming($params['leader']));
        } elseif (isset($params['asm']) and $params['asm']) {
            $QAsm = new Application_Model_Asm();
            $list_regions = $QAsm->get_cache($params['asm']);
            if ($list_regions['province'] && is_array($list_regions['province']) && count($list_regions['province']) > 0){
                $select->where('p.regional_market IN (?)', $list_regions['province']); // l?c staff thu?c regional_market tr�n
            }
               
            else
                $select->where('1=0', 1);
            $select->where('s.team in (75,119,294)', '');

        } elseif (isset($params['other']) and $params['other']) {
            $QTimePermission = new Application_Model_TimePermission();
            $list_regions = $QTimePermission->get_region_cache($params['other']);

            if ($list_regions && is_array($list_regions) && count($list_regions) > 0)
                $select->where('p.regional_market IN (?)', $list_regions); // l?c staff thu?c regional_market tr�n
            else
                $select->where('1=0', 1);

            ///////////////////////////////////////////////////////////////
            $list_teams = $QTimePermission->get_team_cache($params['other']);
            if ($list_teams && is_array($list_teams) && count($list_teams) > 0)
            {
                $select->where('s.team IN (?)', $list_teams); // l?c staff thu?c regional_market tr�n
            }
            else
            {
                $select->where('1=0', 1);
            }

        }


        if (isset($params['from_date']) and $params['from_date']) {
            $date = My_Date::normal_to_mysql($params['from_date']);
            $select->where('date(p.created_at) >= ?', $date);
        }

        if (isset($params['to_date']) and $params['to_date']) {
            $date = My_Date::normal_to_mysql($params['to_date']);
            $select->where('date(p.created_at) <= ?', $date);
        }

        if (isset($params['regional_market']) and $params['regional_market'])
            $select->where('s.regional_market = ?', $params['regional_market']);

        if (isset($params['district']) and $params['district'])
            $select->where('st.district = ?', $params['district']);


        if (isset($params['area_id']) and $params['area_id']) {
            $QRegionalMarket = new Application_Model_RegionalMarket();
            $where = $QRegionalMarket->getAdapter()->quoteInto('area_id in (?)', $params['area_id']);

            $regional_markets = $QRegionalMarket->fetchAll($where);
            $tem = array();
            foreach ($regional_markets as $regional_market)
                $tem[] = $regional_market->id;

            $select->where('s.regional_market IN (?)', $tem);
        }

        if (isset($params['sort']) and $params['sort']) {
            $order_str = '';

            switch ($params['sort']) {
                case 'st.id':
                    $order_str = 'st.`name`';
                    break;
                case 'st.regional_market':
                    $select->join(array('rm' => 'regional_market'), 'st.regional_market = rm.id',
                        array());
                    $order_str = 'rm.`name`';
                    break;
                default:
                    break;
            }

            $collate = ' ';
            $desc = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';

            if ($params['sort'] == 'name') {
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= ' CONCAT(s.firstname, " ",s.lastname) ' . $collate . $desc;
            } elseif (in_array($params['sort'], array('st.regional_market', 'st.id'))) {
                $collate = ' COLLATE utf8_unicode_ci ';
                $order_str .= $collate . $desc;
            } else {
                $order_str = $params['sort'] . ' ' . $collate . $desc;
            }

            $select->order(new Zend_Db_Expr($order_str));
            
        }

        if ($limit){
            $select->limitPage($page, $limit);
        }
        //PC::debug($select->__toString());
        $result = $db->fetchAll($select);

        if ($limit){
            $total = $db->fetchOne("select FOUND_ROWS()");
        }

        return $result;
    }

    public function getPublicHolyday($params = array())
    {
        $db = Zend_Registry::get('db');

        $sql = "SELECT
                `date`
            FROM `all_date_advanced` 
            WHERE MONTH(`date`) = :month
                AND YEAR(`date`) = :year
                AND public = 1
            GROUP BY `date`";
        
        $stmt = $db->prepare($sql);
        $stmt->bindParam('month', $params['month'], PDO::PARAM_STR);
        $stmt->bindParam('year', $params['year'], PDO::PARAM_STR);

        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $stmt = $db = null;

        $data_map = array();
        foreach ($data as $key => $value) 
        {
            $data_map[] = intval(date('d', strtotime($data['date'])));
        }

        return $data_map;
    }

    function getDay($staff_id, $month)
    {
        $db = Zend_Registry::get('db');
        if (isset($staff_id) and isset($month)) {
            $select = $db->select()->from(array('p' => $this->_name), array('p.*'));
            $select->where('staff_id = ? ', $staff_id);
            $select->where('MONTH(created_at) = ?', $month);
            $select->where('YEAR(created_at) = ?', date('Y'));
            $select->where('status = ?', '1');
            $result = count($db->fetchAll($select));
            return $result;
        } else
            return - 1;
    }

    function checkInStatus($staff_id)
    {
        $QStaff = new Application_Model_Staff();
        $staff_rowset = $QStaff->find($staff_id);
        $staff  = $staff_rowset->current();

        $db = Zend_Registry::get('db');
        $select = $db->select()->from(array('p' => $this->_name), array('p.*'));
        $select->where('staff_id = ? ', $staff_id);
        $select->where('DATE(created_at) = DATE(CURDATE())', null);
        $result = $db->fetchRow($select);

        if(empty($result))
        {
            return 0;
        }
        else
            return 1;

    }
    
    function getDayDashboard($params)
    {
        $db       = Zend_Registry::get('db');
        $staff_id = isset($params['user_id']) ? $params['user_id'] : '';
        $month    = isset($params['month']) ? $params['month'] : '';
        $year     = isset($params['year']) ? $params['year'] : '';
        $QStaff   = new Application_Model_Staff();
        
        if (isset($staff_id) and isset($month)) {
            $select = $db->select()
                   ->from(array('p' => $this->_name), array('p.*'));
            $select->where('staff_id = ? ', $staff_id);
            $select->where('MONTH(created_at) = ?', $month);

            if(isset($year) and $year){
              $select->where('YEAR(created_at) = ?', $year);
            }

            $select->group('DATE(created_at)');
            
            $result = $db->fetchAll($select);
            $day_approve = $day_not_approve = $cagay = $capg =  0;

            $staffRowset = $QStaff->find($staff_id);
            $staff       = $staffRowset->current();
            
            foreach ($result as $k => $v)
            {
                if(isset($v['status']) AND $v['status'] == 0){ 
                    $day_not_approve++;
                }else{
                    $day_approve++;
                }

                //kiểm tra là pg thì kiểm tra công ca gãy
                if(My_Staff_Title::isPg($staff['title']) and $v['shift'] == 2){
                    $cagay++;
                }elseif(My_Staff_Title::isPg($staff['title']) and $v['shift'] != 2){
                    $capg++;
                }

            }

            $result = array(
                    'day_approve'     => $day_approve ? $day_approve : 0,
                    'day_not_approve' => $day_not_approve ? $day_not_approve : 0,
                    'total'           => count($result),
                    'cagay'           => $cagay ? $cagay : 0,
                    'cathuong'        => $capg ? $cagay : 0
            );
            
            return $result;
        } else
            return - 1;
    }

    function fetchChildbearing($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('p'=>'off_childbearing'),array(
                new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
                'p.*',
                'staff_name'=>'CONCAT(s.firstname," ",s.lastname)',
                'title'=>'t1.name',
                'team'=>'t2.name',
                's.code',
                'p.off_type',
                ))
            ->join(array('s'=>'staff'),'p.staff_id = s.id',array())
            ->join(array('t1'=>'team'),'t1.id = s.title',array())
            ->join(array('t2'=>'team'),'t2.id = t1.parent_id',array())
        ;

        if(isset($params['from_date']) AND $params['from_date']){
            $from = explode('/',$params['from_date']);
            $from = date('Y-m-d',strtotime($from[2].'-'.$from[1].'-'.$from[0]));
            $select->where('p.from_date >= ?',$from);
        }

        if(isset($params['to_date']) AND $params['to_date']){
            $to = explode('/',$params['to_date']);
            $to = date('Y-m-d',strtotime($to[2].'-'.$to[1].'-'.$to[0]));
            $select->where('p.from_date <= ?',$to);
        }

        if(isset($params['code']) AND $params['code']){
            $select->where('s.code = ?',$params['code']);
        }

        if(isset($params['email']) AND $params['email']){
            $select->where('s.email LIKE ?','%'.$params['email'].'%');
        }

        if(isset($params['name']) AND $params['name']){
            $select->where('CONCAT(s.firstname," ",s.lastname) LIKE ?','%'.$params['name'].'%');
        }

        if(isset($params['off_type']) AND $params['off_type']){
            $select->where('p.off_type = ?',$params['off_type']);
        }

        $select->order('p.created_at DESC');
        if ($limit)
            $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);

        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public function getTimeNotCheckIn($staff_id)
    {

        $month = date('m');
        $last_day_of_month = date('d');
        $QStaff = new Application_Model_Staff();
        $QTime  = new Application_Model_Time();
        $staff_current_set = $QStaff->find($staff_id);
        $staff = $staff_current_set->current();
        $QDateSpecial = new Application_Model_DateSpecial();
        $list_date_special = $QDateSpecial->get_date($month);
        $time_not_check_in = array();
        for($i= 1 ; $i <= $last_day_of_month ; $i++)
        {
            $date_special = 0;
            if(isset($list_date_special) and $list_date_special)
            {
                foreach($list_date_special as $k => $v) {
                    if ($v['date'] == $i and $v['team'] == $staff['id'])
                    {
                        $date_special = 1;
                    }
                }

                if(isset($date_special) and $date_special)
                {
                    continue;
                }
            }


            $date_time =  $i <= 9 ? '0'.$i :  $i;

            $check = date('Y-m-'.$date_time);

            if(isset($check) and $check)
            {
                $weekday = date("l", strtotime($check));

                if(isset($weekday) and $weekday == "Sunday")
                {
                    continue;
                }

                if(isset($weekday) and $weekday == "Saturday")
                {
                    continue;
                }
            }

            $where = array();


            $where[] = $QTime->getAdapter()->quoteInto('DATE(created_at) = ?' , date('Y-m-d' , strtotime($check)));
            $where[] = $QTime->getAdapter()->quoteInto('staff_id = ?' , $staff_id);
            $result  = $QTime->fetchRow($where);

            if(empty($result))
            {
                $time_not_check_in[] = date('d' , strtotime($check));
            }


        }

        if(!empty($time_not_check_in))
        {
            return implode("," , $time_not_check_in);
        }
        else
        {
            return null;
        }


    }

    public function approveApi($params = array())
    {
        $id          = isset($params['id']) ? $params['id'] : null;
        $yesterday   = isset($params['yesterday']) ? $params['yesterday'] : null;
        $shift       = isset($params['shift']) ? $params['shift'] : null;
        $back_url    = isset($params['back_url']) ? $params['back_url'] : null;

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $off         = isset($params['off']) ? $params['off'] : null;
        $user_id     = isset($params['user_id']) ? $params['user_id'] : null;


        $timing_at   = isset($params['timing_at']) ? $params['timing_at'] : null;
        $approved_at = isset($params['approved_at']) ? $params['approved_at'] : null;
        $approved_by = isset($params['approved_by']) ? $params['approved_by'] : null;
        $status      = isset($params['status']) ? $params['status'] : null;
    }

    /**
     * Function saveAPI
     * để thuận tiện việc nhập và trả dữ liệu
     * @param  array  $params - mảng tất cả các tham số cần thiết
     * @return array
     */
    
    public function saveApi($params = array())
    {
      
        $id          = isset($params['id']) ? $params['id'] : null;
        $yesterday   = isset($params['yesterday']) ? $params['yesterday'] : null;
        $shift       = isset($params['shift']) ? $params['shift'] : null;
        $back_url    = isset($params['back_url']) ? $params['back_url'] : null;
        //cham cong training
        $training    = isset($params['training']) ? $params['training'] : null;
        $type        = isset($params['type']) ? $params['type'] : null;
        $work        = isset($params['work']) ? $params['work'] : null;
        $locale      = isset($params['locale']) ? $params['locale'] : null;
        $number      = isset($params['number']) ? $params['number'] : null;
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();  
        $off         = isset($params['off']) ? $params['off'] : null;
        $user_id     = isset($params['user_id']) ? $params['user_id'] : null;
        $check_approve = isset($params['check_approve']) ? $params['check_approve'] : null;

        $timing_at   = isset($params['timing_at']) ? $params['timing_at'] : null;
        $approved_at = isset($params['approved_at']) ? $params['approved_at'] : null;
        $approved_by = isset($params['approved_by']) ? $params['approved_by'] : null;
        $status      = isset($params['status']) ? $params['status'] : null;

        $QTime       = new Application_Model_Time();
        $QStaff      = new Application_Model_Staff();
        $staffRowSet = $QStaff->find($id);
        $staff       = $staffRowSet->current();

        if(empty($id)){
             return array(
                'code' => -1,
                'message' => 'Please input staff!',
                );
        }

        if(empty($shift))
        {
             return array(
                'code' => -1,
                'message' => 'Please input shift!',
                );
        }

        if(empty($staff))
        {
             return array(
                'code' => -1,
                'message' => 'Invalid staff for checkin!',
                );
        }

        $date = date('Y-m-d H:i:s');
        $QLog = new Application_Model_Log();
        $QLockTiming = new Application_Model_HrTimingLock();
       
        try{
             // kiem tra ngay cham cong
            if ($timing_at)
            {
                $where = array();
                $datetime = new DateTime($timing_at);
                $day      = $datetime->format('d');
                $month    = $datetime->format('m');
                $year     = $datetime->format('Y');

                //kiem tra xem tháng đó có chốt công chưa
                $whereLocktiming   = array();
                $whereLocktiming[] = $QLockTiming->getAdapter()->quoteInto('month = ?' , intval($month));
                $whereLocktiming[] = $QLockTiming->getAdapter()->quoteInto('year = ?' , intval($year));
                $lockTiming        = $QLockTiming->fetchRow($whereLocktiming);

                if($lockTiming)
                {
                    return array(
                        'code' => -1,
                        'message' => 'This month can\'t not approve!',
                    );
                }


                $where[] = $QTime->getAdapter()->quoteInto('staff_id = ?', $id);
                $where[] = $QTime->getAdapter()->quoteInto('DAY(created_at) = ?', $day);
                $where[] = $QTime->getAdapter()->quoteInto('MONTH(created_at) = ?', $month);
                $where[] = $QTime->getAdapter()->quoteInto('YEAR(created_at) = ?', $year);
                $result  = $QTime->fetchRow($where);
                
                if(isset($check_approve) and $check_approve)
                {
                    if(isset($staff['title']) and $staff['title'] and My_Staff::isPgTitle($staff['title']))
                    {
                        $data = array(
                            'regional_market' => $staff['regional_market'],
                            'approved_at'     => $approved_at,
                            'approved_by'     => $approved_by,
                            'status'          => $status,
                        );

                        $QTime->update($data, $where);
                    }
                }
                else
                {
                        if(isset($result) and $result)
                        {

                            if($result['shift'] != 2)
                            {
                                $data               = array();
                                $data['shift']      = $shift;
                                $data['updated_at'] = $date;
                                $data['updated_by'] = $userStorage->id;
                                $QTime->update($where , $data);
                            }
                        }
                        else if(empty($result))
                        {
                            $data = array(
                                'staff_id'        => $id,
                                'created_at'      => $timing_at,
                                'regional_market' => $staff['regional_market'],
                                'approved_at'     => $approved_at,
                                'approved_by'     => $approved_by,
                                'status'          => $status,
                                'shift'           => $shift
                            );

                            if (isset($yesterday) and $yesterday)
                            {
                                $data['yesterday'] = 1;
                                $date = date("Y-m-d H:i:s", strtotime($timing_at . "- 1 days"));
                            }

                            if (isset($staff) and $staff['team'] == TRAINING_TEAM and $training)
                            {
                                if(empty($note))
                                    return array(
                                        'code' => -1,
                                        'message' => 'please input note for training!',
                                    );
                                $data['work']   = $work;
                                $data['locale'] = $locale;
                                $data['number'] = $number;
                                $data['note']   = htmlspecialchars(trim($note));
                            }

                            $QTime->insert($data);
                        }

                    }
                }

            //commit
            $result = array(
                'code' => 1,
                'message' => 'Done!',
                'id' => $id,
            );



            return $result;

        }
        catch (exception $e)
            {
                
                return array(
                    'code' => -3,
                    'message' => 'Cannot save, please try again!' . $e->getMessage(),
                );
            }
    }


    public function getLimitedTime($staff_id)
    {
        $date = date('d');
        $QStaff = new Application_Model_Staff();
        $staff_current_set = $QStaff->find($staff_id);
        $staff = $staff_current_set->current();

        $team_of_staff = $staff['team'];
        $joined_at = $staff['joined_at'];

        $QDateSpecial = new Application_Model_DateSpecial();
        $list_date_special = $QDateSpecial->get_date(date('m'));
        $total_timing = 0;


        for($i=1; $i<= $date; $i++)
        {
            $date_special = 0;
            foreach($list_date_special as $k => $v) {
                if ($v['date'] == $i and $v['team'] == $team_of_staff)
                {
                    $date_special = 1;
                }
            }

            if(isset($date_special) and $date_special)
            {
                continue;
            }

            if($i <= 9)
            {
                $date_time = '0'.$i;
            }
            else{
                $date_time=$i;
            }

            $check = date('Y-m-'.$date_time);

            if(isset($check) and $check)
            {
                $weekday = date("l", strtotime($check));

                if(isset($weekday) and $weekday == "Sunday")
                {
                    continue;
                }

                if(isset($weekday) and $weekday == "Saturday")
                {
                    continue;
                }
            }

            //set xem nhan vien do co phai nhan vien moi di lam trong thang k
            if($joined_at <= $date_time)
            {
                continue;
            }


            $total_timing ++;
        }
        return $total_timing;

    }

    public function getStaffForTiming($staff_id){

        if(!$staff_id){
            return false;
        }

        $db  = Zend_Registry::get('db');
        $cols = array('b.staff_id');
        $str = "( b.released_at IS NULL OR 
                    ( FROM_UNIXTIME(b.released_at,'%Y-%m-%d') > FROM_UNIXTIME(a.released_at,'%Y-%m-%d') 
                        AND FROM_UNIXTIME(b.released_at,'%Y-%m-%d') < IFNULL(FROM_UNIXTIME(a.released_at,'%Y-%m-%d'),'9999-01-01') 
                    ) 
                )";
        $select = $db->select()
            ->from(array('a'=>'store_leader_log'),$cols)
            ->join(array('b'=>'store_staff_log'),'a.store_id = b.store_id',array())
            ->where('a.staff_id = ?',$staff_id)
            ->where('a.released_at IS NULL')
            ->where($str,1)
            ->where('b.is_leader = 0 OR b.staff_id = ?',$staff_id)
        ;
        $list = $db->fetchAll($select);
       
        $result = array();
        $result[] = $staff_id;
        if($list){
            foreach ($list as $item) {
                if(!isset($result['staff_id'])){
                    $result[] = $item['staff_id'];
                }
            }
        }

        return $result;
    }


    // old code get list
    public function listStaff($limit = 10, $page = 1, $params = array())
    {
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array("st" => "staff"),array(new Zend_Db_Expr("
                            SQL_CALC_FOUND_ROWS st.id, st.firstname, st.lastname, st.email, st.code, 
                            SUM(IF(t.shift = 2, 1, 0)) AS 'g',
                            SUM(IF(t.shift = 1 OR t.shift = 3 OR t.shift = 0 OR t.shift = 6, 1, 0)) AS 'x',
                            SUM(IF(t.shift = 7 OR t.shift = 8, 1, 0)) AS 't'")));
        
        if(empty($params['month']))
        {
            $params['month'] = date('m');
        }

        if(!empty($params['only_training']))
        {
            $select->join(array('t' => 'time'), 
                                    "t.staff_id = st.id 
                                    AND month(t.created_at) = " . $params['month'] . "
                                    AND year(t.created_at) = " . $params['year'] . "
                                    AND (t.shift = 7 OR t.shift = 8)
                                    ", 
                                    array());
        }
        else
        {
            $select->joinLeft(array('t' => 'time'), 
                                    "t.staff_id = st.id 
                                    AND month(t.created_at) = " . $params['month'] . " 
                                    AND year(t.created_at) = " . $params['year'] , 
                                    array());
        }
        
        
        $select->where("st.email is not null");
        $select->where("st.email <> ''");
        if(isset($params['name']) && !empty($params['name']))
        {
            $select->where("concat(st.firstname, ' ', st.lastname) like ?", '%' . $params['name'] . '%');
        }

        if(isset($params['email']) && !empty($params['email']))
        {
            $select->where("st.email = ?", $params['email'] . '@oppomobile.vn');
        }

        if(isset($params['code']) && !empty($params['code']))
        {
            $select->where("st.code = ?", $params['code']);
        }
        $auth = Zend_Auth::getInstance()->getStorage()->read();

        if($auth->title == SALES_TITLE) // phan quyen cho sale
        {
            $select->where("st.title = ?", PGPB_TITLE);

            $QStoreStaffLog = new Application_Model_StoreStaffLog();
            
            $where_store_staff_log = array();
            $where_store_staff_log[] = $this->getAdapter()->quoteInto('staff_id = ?', $auth->id);
            $where_store_staff_log[] = $this->getAdapter()->quoteInto('released_at is NULL');
            
            $array_store = array();
            foreach($QStoreStaffLog->fetchAll($where_store_staff_log) as $value)
            {
                $array_store[] = $value['store_id'];
            }
            $store_id = implode(",", $array_store);
            $select->join(array("stl" => "store_staff_log"), "stl.staff_id = st.id and stl.store_id in (" . $store_id .") ", array());
            $select->where('st.title = '. CHUYEN_VIEN_BAN_HANG_TITLE . ' or st.title = ' . PGPB_TITLE . ' or st.title = ' . PB_SALES_TITLE);
        }

        if($auth->title == LEADER_TITLE) // phan quyen cho leader
        {
            $QRegionalMarket = new Application_Model_RegionalMarket();
            $where_market = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $auth->regional_market_id);
            $regional_markets = $QRegionalMarket->fetchAll($where_market);

            $array_regional_markets = array();
            foreach($regional_markets as $value)
            {
                $array_regional_markets[] = $value['id'];
            }
            $regional_markets_id = implode(",", $array_regional_markets);

            $select->where('st.regional_market IN (?)', $regional_markets_id);
            $select->where('st.title = '. SALES_TITLE . ' or st.title = ' . PGPB_TITLE . ' or st.title = ' . PB_SALES_TITLE);
        }

        if($auth->title == SALES_ADMIN_TITLE)
        {
            $QRegionalMarket = new Application_Model_RegionalMarket();
            $where_market = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $auth->regional_market_id);
            $regional_markets = $QRegionalMarket->fetchAll($where_market);

            $array_regional_markets = array();
            foreach($regional_markets as $value)
            {
                $array_regional_markets[] = $value['id'];
            }
            $regional_markets_id = implode(",", $array_regional_markets);

            $select->where('st.regional_market IN (?)', $regional_markets_id);
            $select->where('st.title = '. SALES_TITLE . ' or st.title = ' . PGPB_TITLE . ' or st.title = ' . PB_SALES_TITLE);
        }

        if($auth->title == SALE_SALE_ASM)
        {
            $QRegionalMarket = new Application_Model_RegionalMarket();
            $where_market = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $auth->regional_market_id);
            $regional_markets = $QRegionalMarket->fetchAll($where_market);

            $array_regional_markets = array();
            foreach($regional_markets as $value)
            {
                $array_regional_markets[] = $value['id'];
            }
            $regional_markets_id = implode(",", $array_regional_markets);

            $select->where('st.regional_market IN (?)', $regional_markets_id);
            $select->where('st.team = '. SALES_TEAM);
        }

        if ($limit){
            $select->limitPage($page, $limit);
        }

        $select->group("st.id");
        $select->order("st.id desc");
        $data['data'] = $db->fetchAll($select);
        $data['total'] = $db->fetchOne("SELECT FOUND_ROWS()");
        return $data;

    }

    public function get_staff_check_in($staff_id, $month, $year )
    {
        $db = Zend_Registry::get('db');

        $data = $db->query("call staff_check_in(" . $staff_id . "," . $month . "," . $year . ")")->fetchAll();
        $array_date = array();
        for($i = 1; $i <= 31; $i++)
        {
            $array_date[$i] = array(
                'day' => $i,
                'date' => date('Y-' . $month . '-' . $i),
                'type' => '',
                'status' => '',
            );
        }

        $array_have_day = array();

        foreach($data as $value)
        {
            if(in_array($value['day'], $array_have_day))
            {
                continue;
            }
            else
            {
                array_push($value['day']);
                $array_date[$value['day']]['type'] = $value['type'];
                if($value['status'] == 0)
                {
                    $array_date[$value['day']]['class'] = '';
                }
                else
                {
                    $array_date[$value['day']]['class'] = 'complete';
                }
                $array_date[$value['day']]['status'] = $value['status'];
                $array_date[$value['day']]['id'] = $value['id'];
            }
        }

        return $array_date;
    }

    public function check_in_detail_list($limit = 10, $page = 1, $params = array())
    {
        $db = Zend_Registry::get('db');

        $select_sql = '';
        $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($params['month']), $params['year']);
        for($i = 1; $i <= $number_day_of_month; $i++ )
        {
            $select_sql .= "MAX( IF( day(date(t.created_at)) = " . $i . ", 
            CONCAT(CASE WHEN t.status = 1 THEN '<span class=\"approved\">' WHEN t.status <> 1 THEN '<span class=\"non-approved\">' END,
            CASE
                WHEN t.shift = 4 THEN 'P'
                WHEN ada.id IS NOT NULL AND dsa.id IS NULL THEN IF(t.shift = 9, cd.code_half, cd.code)
                WHEN t.shift = 9 THEN 'H' 
                WHEN t.shift = 2 THEN 'G' 
                WHEN t.shift = 1 OR t.shift = 3 OR t.shift = 0 OR t.shift = 6 THEN 'X' 
                WHEN t.shift = 7 OR t.shift = 8 THEN 'T' END, '</span>' ), '' ) ) AS `" . $i . "`,";
             
        }


        $select = $db->select()
            ->from(array("st" => "staff"),array(new Zend_Db_Expr("
                            SQL_CALC_FOUND_ROWS st.id, st.firstname, st.lastname, st.email, 
                            " . $select_sql . "
                            st.code,
                            ar.name as `area`,
                            st.department,
                            st.title,
                            st.team,
                            
                            SUM(IF(t.status = 1 AND ada.id = 2 AND ada.id IS NOT NULL AND dsa.id IS NULL AND t.shift <> 4, IF(t.shift = 9, 0.5, 1), 0)) AS `sun_days`,
                            SUM(IF(t.status = 1 AND ada.id <> 2 AND ada.id IS NOT NULL AND dsa.id IS NULL AND t.shift <> 4, IF(t.shift = 9, 0.5, 1), 0)) AS `special_days`,
                            SUM(IF(t.status = 1 AND t.shift = 2 AND dsa.id IS NULL AND t.shift <> 4, 1, 0)) AS `gay_days`,
                            SUM(IF(t.status = 1 AND (t.shift = 1 OR t.shift = 3 OR t.shift = 0 OR t.shift = 6 AND t.shift <> 4 AND ada.id IS NOT NULL ) AND (ada.id IS NULL OR dsa.id IS NOT NULL), IF(t.shift = 9, 0.5,1), 0)) AS `work_days`
                            ")));
        
        if(empty($params['month']))
        {
            $params['month'] = date('m');
        }

        if(!empty($params['only_training']))
        {
            $select->join(array('t' => 'time'), 
                                    "t.staff_id = st.id 
                                    AND month(t.created_at) = " . $params['month'] . "
                                    AND year(t.created_at) = " . $params['year'] . "
                                    AND (t.shift = 7 OR t.shift = 8)
                                    ", 
                                    array());
        }
        else
        {
            $select->joinLeft(array('t' => 'time'), 
                                    "t.staff_id = st.id 
                                    AND month(t.created_at) = " . $params['month'] . " 
                                    AND year(t.created_at) = " . $params['year'] , 
                                    array());
        }

        $select->join(array('rm' => 'regional_market'),
                        "st.regional_market = rm.id"
                        ,
                        array());

        
        $select->join(array('ar' => 'area'),
                        "ar.id = rm.area_id"
                        ,
                        array());

        $select->joinLeft(array('ada' => 'all_date_advanced'),
                            "date(t.created_at) = ada.date
                                AND ada.is_off = 1
                                AND (
                                    (st.title = ada.type_id AND ada.type = 3)
                                )
                                AND ada.category_date IS NOT NULL",
                            array()
                        );
        

        $select->joinLeft(array('cd' => 'date_category'),
                            "cd.id = ada.category_date
                            AND ada.category_date IS NOT NULL",
                            array()
                        );

        $select->joinLeft(array('dsa' => 'date_staff_advanced'),
                            "date(t.created_at) = dsa.date
                            AND dsa.staff_id = st.id
                            AND dsa.is_del <> 1",
                            array()
                        );

        
        
        $select->where("st.email is not null");
        $select->where("st.email <> ''");

        if(isset($params['area']) && !empty($params['area']))
        {
            $select->where("ar.id = ?", $params['area']);
        }

        if(isset($params['name']) && !empty($params['name']))
        {
            $select->where("concat(st.firstname, ' ', st.lastname) like ?", '%' . $params['name'] . '%');
        }

        if(isset($params['email']) && !empty($params['email']))
        {
            $select->where("st.email = ?", $params['email'] . '@oppomobile.vn');
        }

        if(isset($params['code']) && !empty($params['code']))
        {
            $select->where("st.code = ?", $params['code']);
        }
        $auth = Zend_Auth::getInstance()->getStorage()->read();

        if($auth->title == SALES_TITLE) // phan quyen cho sale
        {

            $QStoreStaffLog = new Application_Model_StoreStaffLog();
            
            $where_store_staff_log = array();
            $where_store_staff_log[] = $this->getAdapter()->quoteInto('staff_id = ?', $auth->id);
            $where_store_staff_log[] = $this->getAdapter()->quoteInto('released_at is NULL');
            
            $array_store = array();
            foreach($QStoreStaffLog->fetchAll($where_store_staff_log) as $value)
            {
                $array_store[] = $value['store_id'];
            }
            $store_id = implode(",", $array_store);
            $select->join(array("stl" => "store_staff_log"), "stl.staff_id = st.id and stl.store_id in (" . $store_id .") ", array());
            $select->where('st.title = '. CHUYEN_VIEN_BAN_HANG_TITLE . ' or st.title = ' . PGPB_TITLE . ' or st.title = ' . PB_SALES_TITLE);
        }

        if($auth->title == LEADER_TITLE) // phan quyen cho leader
        {
            $QRegionalMarket = new Application_Model_RegionalMarket();
            $where_market = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $auth->regional_market_id);
            $regional_markets = $QRegionalMarket->fetchAll($where_market);

            $array_regional_markets = array();
            foreach($regional_markets as $value)
            {
                $array_regional_markets[] = $value['id'];
            }
            $regional_markets_id = implode(",", $array_regional_markets);

            $select->where('st.regional_market IN (?)', $regional_markets_id);
            $select->where('st.title = '. SALES_TITLE . ' or st.title = ' . PGPB_TITLE . ' or st.title = ' . PB_SALES_TITLE);
        }

        if($auth->title == SALES_ADMIN_TITLE)
        {
            $QRegionalMarket = new Application_Model_RegionalMarket();
            $where_market = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $auth->regional_market_id);
            $regional_markets = $QRegionalMarket->fetchAll($where_market);

            $array_regional_markets = array();
            foreach($regional_markets as $value)
            {
                $array_regional_markets[] = $value['id'];
            }
            $regional_markets_id = implode(",", $array_regional_markets);

            $select->where('st.regional_market IN (?)', $regional_markets_id);
            $select->where('st.title = '. SALES_TITLE . ' or st.title = ' . PGPB_TITLE . ' or st.title = ' . PB_SALES_TITLE);
        }

        if($auth->title == SALE_SALE_ASM)
        {
            $QRegionalMarket = new Application_Model_RegionalMarket();
            $where_market = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $auth->regional_market_id);
            $regional_markets = $QRegionalMarket->fetchAll($where_market);

            $array_regional_markets = array();
            foreach($regional_markets as $value)
            {
                $array_regional_markets[] = $value['id'];
            }
            $regional_markets_id = implode(",", $array_regional_markets);

            $select->where('st.regional_market IN (?)', $regional_markets_id);
            $select->where('st.team = '. SALES_TEAM);
        }

        if ($limit){
            $select->limitPage($page, $limit);
        }
        
        $select->order("st.id desc");
        $select->group("st.id");
        $data['data'] = $db->fetchAll($select);
        $data['total'] = $db->fetchOne("SELECT FOUND_ROWS()");
        return $data;
    }   

    public function get_staff_new_time($limit = 10, $page = 1, $sum_offset = 0, $params = array())
    {
        $db = Zend_Registry::get('db');
        $db->query('SET SESSION group_concat_max_len = 1000000;');
        $select_sql = '';
        $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($params['month']), $params['year']);
        
        $select_staff_id = $db->select()
            ->from(array("st" => "staff_training"),
                                            array(new Zend_Db_Expr("SQL_CALC_FOUND_ROWS st.*
                                                , ar.name as `area`
                                                , ar.id as `area_id`
                                                , 152 as `department`
                                                , CONCAT('[', GROUP_CONCAT(day(tct.date) SEPARATOR ','), ']') as `list_day`
                                                ")));

        if(empty($params['month']))
        {
            $params['month'] = date('m');
        }

        $from_date = $params['year'] . '-' . $params['month'] . '-01';
        $to_date = $params['year'] . '-' . $params['month'] . '-' . $number_day_of_month;

        $select_staff_id->join(array('rm' => 'regional_market'),
                        "st.regional_market = rm.id"
                        ,
                        array());

        
        $select_staff_id->join(array('ar' => 'area'),
                        "ar.id = rm.area_id"
                        ,
                        array());
        
        $select_staff_id->join(array('tcd' => 'trainer_course_detail'),
                                'st.id = tcd.new_staff_id '
                                ,
                                array());
        
        $select_staff_id->join(array('tct' => 'trainer_course_timing'),
                                'tct.course_detail_id = tcd.id AND tcd.result = 1'
                                ,
                                array());

        $from_date = date($params['year'] . '-' . $params['month'] . '-1');
        $to_date = date($params['year'] . '-' . $params['month'] . '-' . $number_day_of_month);
        
        // $select_staff_id->where("st.joined_at <= ?", $from_date);
        // $select_staff_id->where("st.off_date IS NULL OR st.off_date >= ?", $from_date);
        
        if(isset($params['area']) && !empty($params['area']))
        {
            $select_staff_id->where("ar.id = ?", $params['area']);
        }
        
        if(isset($params['name']) && !empty($params['name']))
        {
            $select_staff_id->where("CONCAT(st.firstname, ' ', st.lastname) like ?", '%' . $params['name'] . '%');
        }

        if(isset($params['email']) && !empty($params['email']))
        {
            $select_staff_id->where("st.id = -1");
        }

        if(isset($params['code']) && !empty($params['code']))
        {
            $select_staff_id->where("st.cmnd = ?", $params['code']);
        }

        if(isset($params['department']) && !empty($params['department']) && $params['department'] != 75)
        {
            $select_staff_id->where("st.id = -1");
        }

        if(isset($params['team']) && !empty($params['team']))
        {
            $select_staff_id->where("st.team = ?", $params['team']);
        }

        if(isset($params['title']) && !empty($params['title']))
        {
            $select_staff_id->where("st.title = ?", $params['title']);
        }

        $select_staff_id->where("tct.date BETWEEN '" . $from_date . "' AND '" . $to_date . "'");

        $auth = Zend_Auth::getInstance()->getStorage()->read();

        if($auth->group_id != HR_ID && $auth->id != 644)
        {
            if($auth->is_officer == 1)
            {
                $QCheckin = new Application_Model_CheckIn();
                $list_code = $QCheckin->getListCodePermission($auth->code);
                if(!empty($list_code))
                {
                    $select_staff_id->where("st.code IN (" . $list_code . ")");
                }
                else
                {
                    $select_staff_id->where("1 = -1");
                }
            }
            else
            {
                if($auth->title == SALES_TITLE) // phan quyen cho sale
                {
                    $select_staff_id->where("st.title = ?", PGPB_TITLE);

                    $QStoreStaffLog = new Application_Model_StoreStaffLog();
                    
                    $where_store_staff_log = array();
                    $where_store_staff_log[] = $this->getAdapter()->quoteInto('staff_id = ?', $auth->id);
                    $where_store_staff_log[] = $this->getAdapter()->quoteInto('released_at is NULL');
                    
                    $array_store = array();
                    foreach($QStoreStaffLog->fetchAll($where_store_staff_log) as $value)
                    {
                        $array_store[] = $value['store_id'];
                    }
                    $store_id = implode(",", $array_store);
                    $select_staff_id->join(array("stl" => "store_staff_log"), "stl.staff_id = st.id and stl.store_id in (" . $store_id .") ", array());
                    $select_staff_id->where('st.title = '. CHUYEN_VIEN_BAN_HANG_TITLE . ' or st.title = ' . PGPB_TITLE . ' or st.title = ' . PB_SALES_TITLE);
                }

                if($auth->title == LEADER_TITLE) // phan quyen cho leader
                {
                    $QStoreLeaderLog = new Application_Model_StoreLeaderLog();
                    
                    $where_store_leader_log = array();
                    $where_store_leader_log[] = $this->getAdapter()->quoteInto('staff_id = ?', $auth->id);
                    $where_store_leader_log[] = $this->getAdapter()->quoteInto('released_at is NULL');
                    
                    $array_store = array();
                    foreach($QStoreLeaderLog->fetchAll($where_store_leader_log) as $value)
                    {
                        $array_store[] = $value['store_id'];
                    }
                    $store_id = implode(",", $array_store);
                    $select_staff_id->join(array("stl" => "store_staff_log"), "stl.staff_id = st.id and stl.store_id in (" . $store_id .") ", array());
                    $select_staff_id->where('st.title = '. SALES_TITLE);
                }

                if($auth->title == SALES_ADMIN_TITLE)
                {
                    $QRegionalMarket = new Application_Model_RegionalMarket();
                    $where_market = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $auth->regional_market_id);
                    $regional_markets = $QRegionalMarket->fetchAll($where_market);

                    $array_regional_markets = array();
                    foreach($regional_markets as $value)
                    {
                        $array_regional_markets[] = $value['id'];
                    }
                    $regional_markets_id = implode(",", $array_regional_markets);

                    $select_staff_id->where('st.regional_market IN (?)', $regional_markets_id);
                    $select_staff_id->where('st.title = '. SALES_TITLE . ' or st.title = ' . PGPB_TITLE . ' or st.title = ' . PB_SALES_TITLE);
                }

                if($auth->title == SALE_SALE_ASM)
                {
                    $QAsm = new Application_Model_Asm();
                    $cachedASM = $QAsm->get_cache($auth->id);
                    $tem = $cachedASM['province'];
                    if ($tem)
                        $list_province_ids = $tem;
                    else
                        $list_province_ids = -1;
                        

                    if (isset($list_province_ids) && $list_province_ids) {
                        if (is_array($list_province_ids) && count($list_province_ids)) {
                            $select_staff_id->where('st.regional_market IN (?)', $list_province_ids);
                        } elseif (is_numeric($list_province_ids) && intval($list_province_ids) > 0) {
                            $select_staff_id->where('st.regional_market = ?', intval(list_province_ids));
                        }
                    }

                    $select_staff_id->where('st.title = ?', LEADER_TITLE);
                }
            }
        }

        if($page > 0)
        {
            $offset = ($page - 1) * $limit + $sum_offset;
        }
        elseif($page == 0)
        {
            $offset = 0;
        }
        else
        {
            $limit = 0;
        }

        if ($limit != null)
        {
            $select_staff_id->limit($limit, $offset);
        }

        $select_staff_id->order("st.id asc");
        $select_staff_id->group('st.id');
        
        $data_staff_id = $db->fetchAll($select_staff_id);
        

       
        $data['total'] = $db->fetchOne("SELECT FOUND_ROWS()");
        $data['data'] = $data_staff_id;
        
        return $data;
    }

    public function get_new_staff_time($params = array())
    {
        $db = Zend_Registry::get('db');

        $stmt = $db->prepare('call `PR_get_time_new_staff`(:from_date, :to_date, :name, :area)');

        $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($params['month']), $params['year']);
        $from_date = $params['year'] . '-' . $params['month'] . '-01';
        $to_date = $params['year'] . '-' . $params['month'] . '-' . $number_day_of_month;

        $params['area'] = empty($params['area'])?null:$params['area'];

        $stmt->bindParam('from_date', $from_date, PDO::PARAM_STR);
        $stmt->bindParam('to_date', $to_date, PDO::PARAM_STR);
        $stmt->bindParam('name', $params['name'], PDO::PARAM_STR);
        $stmt->bindParam('area', $params['area'], PDO::PARAM_INT);

        $stmt->execute();
        $data = $stmt->fetchAll();

        $stmt->closeCursor();
        $stmt = $db = null;
        return $data;
    }

    public function get_time_list($limit = 10, $page = 1, $params = array())
    {
        $db = Zend_Registry::get('db');
        $db->query('SET SESSION group_concat_max_len = 1000000;');
        $select_sql = '';
        $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($params['month']), $params['year']);
        if(!empty($params['approve_all']))
        {
            $select = "GROUP_CONCAT( CONCAT(\"'\", t.id, \"'\" )) as `list_time_id`";
        }
        else
        {
            $select = "
                SQL_CALC_FOUND_ROWS st.id, st.firstname, st.lastname, st.email, 
                " . $select_sql . "
                st.code,
                ar.name as `area`,
                cp.name as `company_name`,
                st.department,
                st.title,
                st.team,
                st.ID_number as `cmnd`,
                st.is_officer,
                st.joined_at as `joined_at`,
                st.off_date as `off_date`,
                std.number as `number`,
                SEC_TO_TIME( SUM( TIME_TO_SEC( cid.overtime ) ) ) AS `overtime`,
                CONCAT('{',
                GROUP_CONCAT(
                    CASE
                        WHEN t.real_time > 0 OR t.approve_time > 0 OR t.hr_approved > 0
                            THEN CONCAT('\"', day(t.created_at), '\":{\"type\":\"'
                                    ,
                                    CASE
                                        WHEN IF(t.hr_approved > 0, t.hr_time , t.approve_time) = 0 THEN '-'
                                        WHEN t.hr_time = 0.5 THEN
                                            IF(ada.id IS NOT NULL AND dsa.id IS NULL, dc.code_half, 'H')
                                        WHEN IF(t.hr_approved > 0, t.hr_time , t.approve_time) > 0.5 OR t.real_time > 0.5 THEN
                                            IF(ada.id IS NOT NULL AND dsa.id IS NULL, dc.code, 'X')
                                    END
                                    ,IF(ada.id IS NOT NULL AND dsa.id IS NULL, CONCAT('\",\"multiple\":\"',ada.multiple), '')
                                    ,'\",\"status\":\"',t.status
                                    ,'\",\"real\":\"',IF(t.real_time IS NULL, '', t.real_time)
                                    ,'\",\"approve\":\"',IF(t.approve_time IS NULL, '', t.approve_time)
                                    ,'\",\"hr\":\"',IF(t.hr_time IS NULL, '', t.hr_time)
                                    ,'\",\"hr_approved\":\"',IF(t.hr_approved IS NULL, '', t.hr_approved)
                                    , '\",\"mc\":\"1\"}')
                        WHEN t.shift = 9 
                            THEN CONCAT('\"', day(t.created_at), '\":{\"type\":\"H\",\"status\":\"',t.status,'\"}')
                        WHEN ada.id IS NOT NULL AND dsa.id IS NULL 
                            THEN CONCAT('\"', day(t.created_at), '\":{\"type\":\"',IF(t.shift = 9, dc.code_half, dc.code),'\",\"status\":\"',t.status,'\",\"multiple\":\"',ada.multiple,'\"}')
                        WHEN t.shift = 2 
                            THEN CONCAT('\"', day(t.created_at), '\":{\"type\":\"G\",\"status\":\"',t.status,'\"}')
                        WHEN t.shift = 1 OR t.shift = 3 OR t.shift = 0 OR t.shift = 6 
                            THEN CONCAT('\"', day(t.created_at), '\":{\"type\":\"X\",\"status\":\"',t.status,'\"}')
                    END
                )
                ,'}') as `list`
                ";
        }
        $select = $db->select()
            ->from(array("st" => "staff"),array(new Zend_Db_Expr($select)));
        $select_staff_id = $db->select()
            ->from(array("st" => "staff"),array(new Zend_Db_Expr("SQL_CALC_FOUND_ROWS st.id as `id`")));
        if(empty($params['month']))
        {
            $params['month'] = date('m');
        }

        if(!empty($params['only_training']))
        {
            $select->join(array('t' => 'time'), 
                                    "t.staff_id = st.id 
                                    AND month(t.created_at) = " . $params['month'] . "
                                    AND year(t.created_at) = " . $params['year'] . "
                                    AND (t.shift = 7 OR t.shift = 8)
                                    ", 
                                    array());
        }
        else
        {
            $select->joinLeft(array('t' => 'time'), 
                                    "t.staff_id = st.id 
                                    AND month(t.created_at) = " . $params['month'] . " 
                                    AND year(t.created_at) = " . $params['year'] , 
                                    array());
        }

        $select->join(array('cp' => 'company'),
                        "st.company_id = cp.id"
                        ,
                        array());

        $select->join(array('rm' => 'regional_market'),
                        "st.regional_market = rm.id"
                        ,
                        array());

        
        $select->join(array('ar' => 'area'),
                        "ar.id = rm.area_id"
                        ,
                        array());

        $select_staff_id->join(array('rm' => 'regional_market'),
                        "st.regional_market = rm.id"
                        ,
                        array());

        
        $select_staff_id->join(array('ar' => 'area'),
                        "ar.id = rm.area_id"
                        ,
                        array());
                        
        $select->joinLeft(array('cgm' => 'company_group_map'),
                            "st.title = cgm.title",
                            array()
                        );

        $select->joinLeft(array('ada' => 'all_date_advanced'),
                            "date(t.created_at) = ada.date
                                AND ada.is_off = 1
                                AND (
                                    (cgm.company_group = ada.type_id AND ada.type = 4)
                                )
                                AND ada.category_date IS NOT NULL",
                            array()
                        );
        

        $select->joinLeft(array('dc' => 'date_category'),
                            "dc.id = ada.category_date
                            AND ada.category_date IS NOT NULL",
                            array()
                        );
        
        $select->joinLeft(array('cid' => 'check_in_detail'),
                            "date(t.created_at) = cid.check_in_day
                            AND st.code = cid.staff_code",
                            array()
                        );

        $select->joinLeft(array('dsa' => 'date_staff_advanced'),
                            "date(t.created_at) = dsa.date
                            AND dsa.staff_id = st.id
                            AND dsa.is_del <> 1",
                            array()
                        );
        
        $select->joinLeft(array('std' => 'saturday'),
                            "std.month = " . $params['month'] . " 
                            AND std.year = " . $params['year'] . "
                            AND std.type = 4 AND std.type_id = cgm.company_group",
                            array()
                        );

        $from_date = date($params['year'] . '-' . $params['month'] . '-1');
        $to_date = date($params['year'] . '-' . $params['month'] . '-' . $number_day_of_month);
        
        // $select_staff_id->where("st.joined_at <= ?", $from_date);
        // $select_staff_id->where("st.off_date IS NULL OR st.off_date >= ?", $from_date);
        
        if(isset($params['area']) && !empty($params['area']))
        {
            $select_staff_id->where("ar.id = ?", $params['area']);
        }
        
        if(isset($params['name']) && !empty($params['name']))
        {
            $select_staff_id->where("concat(st.firstname, ' ', st.lastname) like ?", '%' . $params['name'] . '%');
        }

        if(isset($params['email']) && !empty($params['email']))
        {
            $select_staff_id->where("st.email = ?", $params['email'] . '@oppomobile.vn');
        }

        if(isset($params['code']) && !empty($params['code']))
        {
            $select_staff_id->where("st.code = ?", $params['code']);
        }

        if(isset($params['department']) && !empty($params['department']))
        {
            $select_staff_id->where("st.department = ?", $params['department']);
        }

        if(isset($params['team']) && !empty($params['team']))
        {
            $select_staff_id->where("st.team = ?", $params['team']);
        }

        if(isset($params['title']) && !empty($params['title']))
        {
            $select_staff_id->where("st.title = ?", $params['title']);
        }

        
        $auth = Zend_Auth::getInstance()->getStorage()->read();

        if($auth->group_id != HR_ID)
        {
            if($auth->is_officer == 1)
            {
                $QCheckin = new Application_Model_CheckIn();
                $list_code = $QCheckin->getListCodePermission($auth->code);
                if(!empty($list_code))
                {
                    $select_staff_id->where("st.code IN (" . $list_code . ")");
                }
                else
                {
                    $select_staff_id->where("1 = -1");
                }
            }
            else
            {
                if($auth->title == SALES_TITLE) // phan quyen cho sale
                {
                    $select_staff_id->where("st.title = ?", PGPB_TITLE);

                    $QStoreStaffLog = new Application_Model_StoreStaffLog();
                    
                    $where_store_staff_log = array();
                    $where_store_staff_log[] = $this->getAdapter()->quoteInto('staff_id = ?', $auth->id);
                    $where_store_staff_log[] = $this->getAdapter()->quoteInto('released_at is NULL');
                    
                    $array_store = array();
                    foreach($QStoreStaffLog->fetchAll($where_store_staff_log) as $value)
                    {
                        $array_store[] = $value['store_id'];
                    }
                    $store_id = implode(",", $array_store);
                    $select_staff_id->join(array("stl" => "store_staff_log"), "stl.staff_id = st.id and stl.store_id in (" . $store_id .") ", array());
                    $select_staff_id->where('st.title = '. CHUYEN_VIEN_BAN_HANG_TITLE . ' or st.title = ' . PGPB_TITLE . ' or st.title = ' . PB_SALES_TITLE);
                }

                if($auth->title == LEADER_TITLE) // phan quyen cho leader
                {
                    $QStoreLeaderLog = new Application_Model_StoreLeaderLog();
                    
                    $where_store_leader_log = array();
                    $where_store_leader_log[] = $this->getAdapter()->quoteInto('staff_id = ?', $auth->id);
                    $where_store_leader_log[] = $this->getAdapter()->quoteInto('released_at is NULL');
                    
                    $array_store = array();
                    foreach($QStoreLeaderLog->fetchAll($where_store_leader_log) as $value)
                    {
                        $array_store[] = $value['store_id'];
                    }
                    $store_id = implode(",", $array_store);
                    $select_staff_id->join(array("stl" => "store_staff_log"), "stl.staff_id = st.id and stl.store_id in (" . $store_id .") ", array());
                    $select_staff_id->where('st.title = '. SALES_TITLE);
                }

                if($auth->title == SALES_ADMIN_TITLE)
                {
                    $QRegionalMarket = new Application_Model_RegionalMarket();
                    $where_market = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $auth->regional_market_id);
                    $regional_markets = $QRegionalMarket->fetchAll($where_market);

                    $array_regional_markets = array();
                    foreach($regional_markets as $value)
                    {
                        $array_regional_markets[] = $value['id'];
                    }
                    $regional_markets_id = implode(",", $array_regional_markets);

                    $select_staff_id->where('st.regional_market IN (?)', $regional_markets_id);
                    $select_staff_id->where('st.title = '. SALES_TITLE . ' or st.title = ' . PGPB_TITLE . ' or st.title = ' . PB_SALES_TITLE);
                }

                if($auth->title == SALE_SALE_ASM)
                {
                    $QAsm = new Application_Model_Asm();
                    $cachedASM = $QAsm->get_cache($auth->id);
                    $tem = $cachedASM['province'];
                    if ($tem)
                        $list_province_ids = $tem;
                    else
                        $list_province_ids = -1;
                        

                    if (isset($list_province_ids) && $list_province_ids) {
                        if (is_array($list_province_ids) && count($list_province_ids)) {
                            $select_staff_id->where('st.regional_market IN (?)', $list_province_ids);
                        } elseif (is_numeric($list_province_ids) && intval($list_province_ids) > 0) {
                            $select_staff_id->where('st.regional_market = ?', intval(list_province_ids));
                        }
                    }

                    $select_staff_id->where('st.title = ?', LEADER_TITLE);
                }
            }
        }
        if ($limit && empty($params['approve_all']))
        {
            $select_staff_id->limitPage($page, $limit);
        }
        
        $select_staff_id->order("st.id asc");
        $select_staff_id->group('st.id');
        $data_staff_id = $db->fetchAll($select_staff_id);

        $data['total'] = $db->fetchOne("SELECT FOUND_ROWS()");

        $array_staff_id = array();
        foreach ($data_staff_id as $key_staff_id => $value_staff_id) 
        {
            $array_staff_id[] = $value_staff_id['id'];
        }
        
        if(!empty($array_staff_id))
        {
            $select->where('st.id IN (?)', $array_staff_id);
        }
        else
        {
            $select->where('st.id IN (?)', array(-1));
        }
        $select->group("st.id");
        if(empty($params['approve_all']))
        {
            $data['data'] = $db->fetchAll($select);
        }
        else
        {
            $data = $db->fetchOne($select);
        }
        return $data;
    }

    public function get_time_list_export($limit = 10, $page = 1, $params = array())
    {
        $db = Zend_Registry::get('db');
        $db->query('SET SESSION group_concat_max_len = 1000000;');
        $select_sql = '';
        $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($params['month']), $params['year']);
        if(!empty($params['approve_all']))
        {
            $select = "GROUP_CONCAT( CONCAT(\"'\", t.id, \"'\" )) as `list_time_id`";
        }
        else
        {
            $select = "
                SQL_CALC_FOUND_ROWS st.id, st.firstname, st.lastname, st.email, 
                " . $select_sql . "
                st.code,
                ar.name as `area`,
                cp.name as `company_name`,
                st.department,
                st.title,
                st.team,
                st.ID_number as `cmnd`,
                st.is_officer,
                st.joined_at as `joined_at`,
                st.off_date as `off_date`,
                std.number as `number`,
                SEC_TO_TIME( SUM( TIME_TO_SEC( cid.overtime ) ) ) AS `overtime`,
                CONCAT('{',
                GROUP_CONCAT(
                    CASE
                        WHEN t.real_time > 0 OR t.approve_time > 0 OR t.hr_approved > 0
                            THEN CONCAT('\"', day(t.created_at), '\":{\"type\":\"'
                                    ,
                                    CASE
                                        WHEN IF(t.hr_approved > 0, t.hr_time , t.approve_time) = 0 THEN '-'
                                        WHEN t.hr_time = 0.5 THEN
                                            IF(ada.id IS NOT NULL AND dsa.id IS NULL, dc.code_half, 'H')
                                        WHEN IF(t.hr_approved > 0, t.hr_time , t.approve_time) > 0.5 OR t.real_time > 0.5 THEN
                                            IF(ada.id IS NOT NULL AND dsa.id IS NULL, dc.code, 'X')
                                    END
                                    ,IF(ada.id IS NOT NULL AND dsa.id IS NULL, CONCAT('\",\"multiple\":\"',ada.multiple), '')
                                    ,'\",\"status\":\"',t.status
                                    ,'\",\"real\":\"',IF(t.real_time IS NULL, '', t.real_time)
                                    ,'\",\"approve\":\"',IF(t.approve_time IS NULL, '', t.approve_time)
                                    ,'\",\"hr\":\"',IF(t.hr_time IS NULL, '', t.hr_time)
                                    ,'\",\"hr_approved\":\"',IF(t.hr_approved IS NULL, '', t.hr_approved)
                                    , '\",\"mc\":\"1\"}')
                        WHEN t.shift = 9 
                            THEN CONCAT('\"', day(t.created_at), '\":{\"type\":\"H\",\"status\":\"',t.status,'\"}')
                        WHEN ada.id IS NOT NULL AND dsa.id IS NULL 
                            THEN CONCAT('\"', day(t.created_at), '\":{\"type\":\"',IF(t.shift = 9, dc.code_half, dc.code),'\",\"status\":\"',t.status,'\",\"multiple\":\"',ada.multiple,'\"}')
                        WHEN t.shift = 2 
                            THEN CONCAT('\"', day(t.created_at), '\":{\"type\":\"G\",\"status\":\"',t.status,'\"}')
                        WHEN t.shift = 1 OR t.shift = 3 OR t.shift = 0 OR t.shift = 6 
                            THEN CONCAT('\"', day(t.created_at), '\":{\"type\":\"X\",\"status\":\"',t.status,'\"}')
                    END
                )
                ,'}') as `list`
                ";
        }
        $select = $db->select()
            ->from(array("st" => "staff"),array(new Zend_Db_Expr($select)));
        $select_staff_id = $db->select()
            ->from(array("st" => "staff"),array(new Zend_Db_Expr("SQL_CALC_FOUND_ROWS st.id as `id`")));
        if(empty($params['month']))
        {
            $params['month'] = date('m');
        }

        if(!empty($params['only_training']))
        {
            $select->join(array('t' => 'time'), 
                                    "t.staff_id = st.id 
                                    AND month(t.created_at) = " . $params['month'] . "
                                    AND year(t.created_at) = " . $params['year'] . "
                                    AND (t.shift = 7 OR t.shift = 8)
                                    ", 
                                    array());
        }
        else
        {
            $select->joinLeft(array('t' => 'time'), 
                                    "t.staff_id = st.id 
                                    AND month(t.created_at) = " . $params['month'] . " 
                                    AND year(t.created_at) = " . $params['year'] , 
                                    array());
        }

        $select->join(array('cp' => 'company'),
                        "st.company_id = cp.id"
                        ,
                        array());

        $select->join(array('rm' => 'regional_market'),
                        "st.regional_market = rm.id"
                        ,
                        array());

        
        $select->join(array('ar' => 'area'),
                        "ar.id = rm.area_id"
                        ,
                        array());
                        
        $select->joinLeft(array('cgm' => 'company_group_map'),
                            "st.title = cgm.title",
                            array()
                        );

        $select->joinLeft(array('ada' => 'all_date_advanced'),
                            "date(t.created_at) = ada.date
                                AND ada.is_off = 1
                                AND (
                                    (cgm.company_group = ada.type_id AND ada.type = 4)
                                )
                                AND ada.category_date IS NOT NULL",
                            array()
                        );
        

        $select->joinLeft(array('dc' => 'date_category'),
                            "dc.id = ada.category_date
                            AND ada.category_date IS NOT NULL",
                            array()
                        );
        
        $select->joinLeft(array('cid' => 'check_in_detail'),
                            "date(t.created_at) = cid.check_in_day
                            AND st.code = cid.staff_code",
                            array()
                        );

        $select->joinLeft(array('dsa' => 'date_staff_advanced'),
                            "date(t.created_at) = dsa.date
                            AND dsa.staff_id = st.id
                            AND dsa.is_del <> 1",
                            array()
                        );
        
        $select->joinLeft(array('std' => 'saturday'),
                            "std.month = " . $params['month'] . " 
                            AND std.year = " . $params['year'] . "
                            AND std.type = 4 AND std.type_id = cgm.company_group",
                            array()
                        );

        $from_date = date($params['year'] . '-' . $params['month'] . '-1');
        $to_date = date($params['year'] . '-' . $params['month'] . '-' . $number_day_of_month);
        
        $select->where("st.joined_at <= ?", $from_date);
        $select->where("st.off_date IS NULL OR st.off_date >= ?", $from_date);
        
        if(isset($params['area']) && !empty($params['area']))
        {
            $select->where("ar.id = ?", $params['area']);
        }
        
        if(isset($params['name']) && !empty($params['name']))
        {
            $select->where("concat(st.firstname, ' ', st.lastname) like ?", '%' . $params['name'] . '%');
        }

        if(isset($params['email']) && !empty($params['email']))
        {
            $select->where("st.email = ?", $params['email'] . '@oppomobile.vn');
        }

        if(isset($params['code']) && !empty($params['code']))
        {
            $select->where("st.code = ?", $params['code']);
        }
        $auth = Zend_Auth::getInstance()->getStorage()->read();

        if($auth->group_id != HR_ID)
        {
            if($auth->is_officer == 1)
            {
                $QCheckin = new Application_Model_CheckIn();
                $list_code = $QCheckin->getListCodePermission($auth->code);
                if(!empty($list_code))
                {
                    $select->where("st.code IN (" . $list_code . ")");
                }
                else
                {
                    $select->where("1 = -1");
                }
            }
            else
            {
                if($auth->title == SALES_TITLE) // phan quyen cho sale
                {
                    $select->where("st.title = ?", PGPB_TITLE);

                    $QStoreStaffLog = new Application_Model_StoreStaffLog();
                    
                    $where_store_staff_log = array();
                    $where_store_staff_log[] = $this->getAdapter()->quoteInto('staff_id = ?', $auth->id);
                    $where_store_staff_log[] = $this->getAdapter()->quoteInto('released_at is NULL');
                    
                    $array_store = array();
                    foreach($QStoreStaffLog->fetchAll($where_store_staff_log) as $value)
                    {
                        $array_store[] = $value['store_id'];
                    }
                    $store_id = implode(",", $array_store);
                    $select->join(array("stl" => "store_staff_log"), "stl.staff_id = st.id and stl.store_id in (" . $store_id .") ", array());
                    $select->where('st.title = '. CHUYEN_VIEN_BAN_HANG_TITLE . ' or st.title = ' . PGPB_TITLE . ' or st.title = ' . PB_SALES_TITLE);
                }

                if($auth->title == LEADER_TITLE) // phan quyen cho leader
                {
                    $QStoreLeaderLog = new Application_Model_StoreLeaderLog();
                    
                    $where_store_leader_log = array();
                    $where_store_leader_log[] = $this->getAdapter()->quoteInto('staff_id = ?', $auth->id);
                    $where_store_leader_log[] = $this->getAdapter()->quoteInto('released_at is NULL');
                    
                    $array_store = array();
                    foreach($QStoreLeaderLog->fetchAll($where_store_leader_log) as $value)
                    {
                        $array_store[] = $value['store_id'];
                    }
                    $store_id = implode(",", $array_store);
                    $select->join(array("stl" => "store_staff_log"), "stl.staff_id = st.id and stl.store_id in (" . $store_id .") ", array());
                    $select->where('st.title = '. SALES_TITLE);
                }

                if($auth->title == SALES_ADMIN_TITLE)
                {
                    $QRegionalMarket = new Application_Model_RegionalMarket();
                    $where_market = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $auth->regional_market_id);
                    $regional_markets = $QRegionalMarket->fetchAll($where_market);

                    $array_regional_markets = array();
                    foreach($regional_markets as $value)
                    {
                        $array_regional_markets[] = $value['id'];
                    }
                    $regional_markets_id = implode(",", $array_regional_markets);

                    $select->where('st.regional_market IN (?)', $regional_markets_id);
                    $select->where('st.title = '. SALES_TITLE . ' or st.title = ' . PGPB_TITLE . ' or st.title = ' . PB_SALES_TITLE);
                }

                if($auth->title == SALE_SALE_ASM)
                {
                    $QAsm = new Application_Model_Asm();
                    $cachedASM = $QAsm->get_cache($auth->id);
                    $tem = $cachedASM['province'];
                    if ($tem)
                        $list_province_ids = $tem;
                    else
                        $list_province_ids = -1;
                        

                    if (isset($list_province_ids) && $list_province_ids) {
                        if (is_array($list_province_ids) && count($list_province_ids)) {
                            $select->where('st.regional_market IN (?)', $list_province_ids);
                        } elseif (is_numeric($list_province_ids) && intval($list_province_ids) > 0) {
                            $select->where('st.regional_market = ?', intval(list_province_ids));
                        }
                    }

                    $select->where('st.title = ?', LEADER_TITLE);
                }
            }
        }
        if ($limit && empty($params['approve_all']))
        {
            $select->limitPage($page, $limit);
        }

        $array_staff_id = array();

        $select->order("st.id asc");
        $select->group("st.id");
        if(empty($params['approve_all']))
        {
            $data['data'] = $db->fetchAll($select);
            $data['total'] = $db->fetchOne("SELECT FOUND_ROWS()");
        }
        else
        {
            $data = $db->fetchOne($select);
        }
        return $data;
    }

    public function get_time_list_total_export($limit = 10, $page = 1, $params = array())
    {
        $db = Zend_Registry::get('db');
        $db->query('SET SESSION group_concat_max_len = 1000000;');
        $select_sql = '';
        $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($params['month']), $params['year']);
        if(!empty($params['approve_all']))
        {
            $select = "GROUP_CONCAT( CONCAT(\"'\", t.id, \"'\" )) as `list_time_id`";
        }
        else
        {
            $select = "
                SQL_CALC_FOUND_ROWS st.id, st.firstname, st.lastname, st.email, 
                " . $select_sql . "
                st.code,
                st.id as `staff_id`,
                ar.name as `area`,
                cp.name as `company_name`,
                st.department,
                st.title,
                st.team,
                ct.name as `contract_term`,
                ac.id as `contract_id`,
                ac.from_date as `contract_from_date`,
                ac.title as `contract_title`,
                st.is_officer,
                st.joined_at as `joined_at`,
                st.off_date as `off_date`,
                std.number as `number`,
                SEC_TO_TIME( SUM( TIME_TO_SEC( cid.overtime ) ) ) AS `overtime`,
                CONCAT('{',
                GROUP_CONCAT(
                    CASE
                        WHEN t.real_time > 0 OR t.approve_time > 0 OR t.hr_approved > 0
                            THEN CONCAT('\"', day(t.created_at), '\":{\"type\":\"'
                                    ,
                                    CASE
                                        WHEN IF(t.hr_approved > 0, t.hr_time , t.approve_time) = 0 THEN '-'
                                        WHEN t.hr_time = 0.5 THEN
                                            IF(ada.id IS NOT NULL AND dsa.id IS NULL, dc.code_half, 'H')
                                        WHEN IF(t.hr_approved > 0, t.hr_time , t.approve_time) = 1 THEN
                                            IF(ada.id IS NOT NULL AND dsa.id IS NULL, dc.code, 'X')
                                    END
                                    ,IF(ada.id IS NOT NULL AND dsa.id IS NULL, CONCAT('\",\"multiple\":\"',ada.multiple), '')
                                    ,'\",\"status\":\"',t.status
                                    ,'\",\"real\":\"',IF(t.real_time IS NULL, '', t.real_time)
                                    ,'\",\"approve\":\"',IF(t.approve_time IS NULL, '', t.approve_time)
                                    ,'\",\"hr\":\"',IF(t.hr_time IS NULL, '', t.hr_time)
                                    ,'\",\"hr_approved\":\"',IF(t.hr_approved IS NULL, '', t.hr_approved)
                                    , '\",\"mc\":\"1\"}')
                        WHEN t.shift = 9 
                            THEN CONCAT('\"', day(t.created_at), '\":{\"type\":\"H\",\"status\":\"',t.status,'\"}')
                        WHEN ada.id IS NOT NULL AND dsa.id IS NULL 
                            THEN CONCAT('\"', day(t.created_at), '\":{\"type\":\"',IF(t.shift = 9, dc.code_half, dc.code),'\",\"status\":\"',t.status,'\",\"multiple\":\"',ada.multiple,'\"}')
                        WHEN t.shift = 2 
                            THEN CONCAT('\"', day(t.created_at), '\":{\"type\":\"G\",\"status\":\"',t.status,'\"}')
                        WHEN t.shift = 1 OR t.shift = 3 OR t.shift = 0 OR t.shift = 6 
                            THEN CONCAT('\"', day(t.created_at), '\":{\"type\":\"X\",\"status\":\"',t.status,'\"}')
                    END
                )
                ,'}') as `list`
                ";
        }
        $select = $db->select()
            ->from(array("st" => "staff"),array(new Zend_Db_Expr($select)));
        $select_staff_id = $db->select()
            ->from(array("st" => "staff"),array(new Zend_Db_Expr("SQL_CALC_FOUND_ROWS st.id as `id`, ac.id as `contract_id`")));

        if(empty($params['month']))
        {
            $params['month'] = date('m');
        }

        if(!empty($params['only_training']))
        {
            $select->join(array('t' => 'time'), 
                                    "t.staff_id = st.id 
                                    AND month(t.created_at) = " . $params['month'] . "
                                    AND year(t.created_at) = " . $params['year'] . "
                                    AND (t.shift = 7 OR t.shift = 8)
                                    ", 
                                    array());
        }
        else
        {
            $select->joinLeft(array('t' => 'time'), 
                                    "t.staff_id = st.id 
                                    AND month(t.created_at) = " . $params['month'] . " 
                                    AND year(t.created_at) = " . $params['year'] , 
                                    array());
        }

        $select->join(array('cp' => 'company'),
                        "st.company_id = cp.id"
                        ,
                        array());

        $select->join(array('rm' => 'regional_market'),
                        "st.regional_market = rm.id"
                        ,
                        array());

        
        $select->join(array('ar' => 'area'),
                        "ar.id = rm.area_id"
                        ,
                        array());

        // $select_staff_id->join(array('rm' => 'regional_market'),
        //                 "st.regional_market = rm.id"
        //                 ,
        //                 array());

        
        // $select_staff_id->join(array('ar' => 'area'),
        //                 "ar.id = rm.area_id"
        //                 ,
        //                 array());
                        
        $select->joinLeft(array('cgm' => 'company_group_map'),
                            "st.title = cgm.title",
                            array()
                        );

        $select->joinLeft(array('ada' => 'all_date_advanced'),
                            "date(t.created_at) = ada.date
                                AND ada.is_off = 1
                                AND (
                                    (cgm.company_group = ada.type_id AND ada.type = 4)
                                )
                                AND ada.category_date IS NOT NULL",
                            array()
                        );
        

        $select->joinLeft(array('dc' => 'date_category'),
                            "dc.id = ada.category_date
                            AND ada.category_date IS NOT NULL",
                            array()
                        );
        
        $select->joinLeft(array('cid' => 'check_in_detail'),
                            "date(t.created_at) = cid.check_in_day
                            AND st.code = cid.staff_code",
                            array()
                        );

        $select->joinLeft(array('dsa' => 'date_staff_advanced'),
                            "date(t.created_at) = dsa.date
                            AND dsa.staff_id = st.id
                            AND dsa.is_del <> 1",
                            array()
                        );
        
        $select->joinLeft(array('std' => 'saturday'),
                            "std.month = " . $params['month'] . " 
                            AND std.year = " . $params['year'] . "
                            AND std.type = 4 AND std.type_id = cgm.company_group",
                            array()
                        );

        $from_date = date($params['year'] . '-' . $params['month'] . '-1');
        $to_date = date($params['year'] . '-' . $params['month'] . '-' . $number_day_of_month);
        
        $select->where("st.joined_at <= ?", $from_date);
        $select->where("st.off_date IS NULL OR st.off_date >= ?", $from_date);
        
        if(isset($params['area']) && !empty($params['area']))
        {
            $select->where("ar.id = ?", $params['area']);
        }
        
        if(isset($params['name']) && !empty($params['name']))
        {
            $select->where("concat(st.firstname, ' ', st.lastname) like ?", '%' . $params['name'] . '%');
        }

        if(isset($params['email']) && !empty($params['email']))
        {
            $select->where("st.email = ?", $params['email'] . '@oppomobile.vn');
        }

        if(isset($params['code']) && !empty($params['code']))
        {
            $select->where("st.code = ?", $params['code']);
        }

        if(isset($params['department']) && !empty($params['department']))
        {
            $select->where("st.department = ?", $params['department']);
        }

        if(isset($params['team']) && !empty($params['team']))
        {
            $select->where("st.team = ?", $params['team']);
        }

        if(isset($params['title']) && !empty($params['title']))
        {
            $select->where("st.title = ?", $params['title']);
        }
        
        $auth = Zend_Auth::getInstance()->getStorage()->read();

        $select->joinLeft(array('ac' => 'asm_contract'),
                                "ac.staff_id = st.id AND ac.status = 2",
                                array());
        
        $first_day = $params['year'] . '-' . $params['month'] . '-1';

        $select->joinLeft(array('acb' => new Zend_Db_Expr(
                    "(SELECT staff_id, MAX(from_date) as `from_date`
            FROM `asm_contract`
            WHERE from_date < '" . $first_day . "'
                AND status = 2
            GROUP BY staff_id)"
        )),
        "ac.from_date = acb.from_date AND acb.staff_id = st.id",
        array());

        $select->joinLeft(array('acc' => 'asm_contract'),
                                "acc.staff_id = st.id AND acc.from_date = '" . $first_day ."' AND acc.status = 2",
                                array());

        $select->where("
        ( (acb.staff_id IS NOT NULL AND acc.staff_id IS NULL) 
            OR 
        MONTH(ac.from_date) = " . $params['month'] . " AND YEAR(ac.from_date) = " . $params['year'] . ")"
        );

        $select->group('st.id');
        $select->group('ac.id');

        if ($limit && empty($params['approve_all']))
        {
            $select->limitPage($page, $limit);
        }
        
        // $select->order("st.id asc");
        // $select->order("ac.id asc");
        
        // $data = $db->fetchAll($select);
        
        $array_staff_id = array();

        // foreach ($data_staff_id as $key_staff_id => $value_staff_id) 
        // {
        //     $array_staff_id[] = $value_staff_id['id'];
        //     $array_contract_staff_id[] = "st.id = " . $value_staff_id['id']
        //                     . " AND ac.id = " . $value_staff_id['contract_id'];
        // }

        // if(!empty($array_contract_staff_id))
        // {
        //     $select->join(array('ac' => 'asm_contract'),
        //                         implode(" OR ", $array_contract_staff_id),
        //                         array()
        //                     );
        // }
        // else
        // {
        //     $select->join(array('ac' => 'asm_contract'),
        //                         'ac.id = -1',
        //                         array()
        //                     );
        // }

        $select->joinLeft(array('ct' => 'contract_term'),
                            "ac.new_contract = ct.id",
                            array()
                        );

        $select->group("st.id");
        $select->group("ac.id");
        if(empty($params['approve_all']))
        {
            $data_total = $db->fetchAll($select);
            $data['total'] = $db->fetchOne("SELECT FOUND_ROWS()");
            foreach ($data_total as $key_total => &$value_total) 
            {
                if(!empty($data_total[$key_total+1]) && $data_total[$key_total+1]['staff_id'] == $value_total['staff_id'])
                {
                    $value_total['contract_to_date'] = $data_total[$key_total+1]['contract_from_date'];
                    $value_total['transfer'] = 1;
                    $data_total[$key_total+1]['transfer'] = 1;
                }
                elseif($value_total['transfer'] != 1)
                {
                    $value_total['transfer'] = 0;
                }
            }
            $data['data'] = $data_total;
        }
        else
        {
            $data = $db->fetchOne($select);
        }

        return $data;
    }

    public function get_time_list_total($limit = 10, $page = 1, $params = array())
    {
        $db = Zend_Registry::get('db');
        $db->query('SET SESSION group_concat_max_len = 1000000;');
        $select_sql = '';
        $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($params['month']), $params['year']);
        if(!empty($params['approve_all']))
        {
            $select = "GROUP_CONCAT( CONCAT(\"'\", t.id, \"'\" )) as `list_time_id`";
        }
        else
        {
            $select = "
                SQL_CALC_FOUND_ROWS st.id, st.firstname, st.lastname, st.email, 
                " . $select_sql . "
                st.code,
                st.id as `staff_id`,
                ar.name as `area`,
                cp.name as `company_name`,
                st.department,
                st.title,
                st.team,
                ct.name as `contract_term`,
                ac.id as `contract_id`,
                ac.from_date as `contract_from_date`,
                ac.title as `contract_title`,
                st.is_officer,
                st.joined_at as `joined_at`,
                st.off_date as `off_date`,
                std.number as `number`,
                SEC_TO_TIME( SUM( TIME_TO_SEC( cid.overtime ) ) ) AS `overtime`,
                CONCAT('{',
                GROUP_CONCAT(
                    CASE
                        WHEN t.real_time > 0 OR t.approve_time > 0 OR t.hr_approved > 0
                            THEN CONCAT('\"', day(t.created_at), '\":{\"type\":\"'
                                    ,
                                    CASE
                                        WHEN IF(t.hr_approved > 0, t.hr_time , t.approve_time) = 0 THEN '-'
                                        WHEN t.hr_time = 0.5 THEN
                                            IF(ada.id IS NOT NULL AND dsa.id IS NULL, dc.code_half, 'H')
                                        WHEN IF(t.hr_approved > 0, t.hr_time , t.approve_time) = 1 THEN
                                            IF(ada.id IS NOT NULL AND dsa.id IS NULL, dc.code, 'X')
                                    END
                                    ,IF(ada.id IS NOT NULL AND dsa.id IS NULL, CONCAT('\",\"multiple\":\"',ada.multiple), '')
                                    ,'\",\"status\":\"',t.status
                                    ,'\",\"real\":\"',IF(t.real_time IS NULL, '', t.real_time)
                                    ,'\",\"approve\":\"',IF(t.approve_time IS NULL, '', t.approve_time)
                                    ,'\",\"hr\":\"',IF(t.hr_time IS NULL, '', t.hr_time)
                                    ,'\",\"hr_approved\":\"',IF(t.hr_approved IS NULL, '', t.hr_approved)
                                    , '\",\"mc\":\"1\"}')
                        WHEN t.shift = 9 
                            THEN CONCAT('\"', day(t.created_at), '\":{\"type\":\"H\",\"status\":\"',t.status,'\"}')
                        WHEN ada.id IS NOT NULL AND dsa.id IS NULL 
                            THEN CONCAT('\"', day(t.created_at), '\":{\"type\":\"',IF(t.shift = 9, dc.code_half, dc.code),'\",\"status\":\"',t.status,'\",\"multiple\":\"',ada.multiple,'\"}')
                        WHEN t.shift = 2 
                            THEN CONCAT('\"', day(t.created_at), '\":{\"type\":\"G\",\"status\":\"',t.status,'\"}')
                        WHEN t.shift = 1 OR t.shift = 3 OR t.shift = 0 OR t.shift = 6 
                            THEN CONCAT('\"', day(t.created_at), '\":{\"type\":\"X\",\"status\":\"',t.status,'\"}')
                    END
                )
                ,'}') as `list`
                ";
        }
        $select = $db->select()
            ->from(array("st" => "staff"),array(new Zend_Db_Expr($select)));
        $select_staff_id = $db->select()
            ->from(array("st" => "staff"),array(new Zend_Db_Expr("SQL_CALC_FOUND_ROWS st.id as `id`, ac.id as `contract_id`")));

        if(empty($params['month']))
        {
            $params['month'] = date('m');
        }

        if(!empty($params['only_training']))
        {
            $select->join(array('t' => 'time'), 
                                    "t.staff_id = st.id 
                                    AND month(t.created_at) = " . $params['month'] . "
                                    AND year(t.created_at) = " . $params['year'] . "
                                    AND (t.shift = 7 OR t.shift = 8)
                                    ", 
                                    array());
        }
        else
        {
            $select->joinLeft(array('t' => 'time'), 
                                    "t.staff_id = st.id 
                                    AND month(t.created_at) = " . $params['month'] . " 
                                    AND year(t.created_at) = " . $params['year'] , 
                                    array());
        }

        $select->join(array('cp' => 'company'),
                        "st.company_id = cp.id"
                        ,
                        array());

        $select->join(array('rm' => 'regional_market'),
                        "st.regional_market = rm.id"
                        ,
                        array());

        
        $select->join(array('ar' => 'area'),
                        "ar.id = rm.area_id"
                        ,
                        array());

        $select_staff_id->join(array('rm' => 'regional_market'),
                        "st.regional_market = rm.id"
                        ,
                        array());

        
        $select_staff_id->join(array('ar' => 'area'),
                        "ar.id = rm.area_id"
                        ,
                        array());
                        
        $select->joinLeft(array('cgm' => 'company_group_map'),
                            "st.title = cgm.title",
                            array()
                        );

        $select->joinLeft(array('ada' => 'all_date_advanced'),
                            "date(t.created_at) = ada.date
                                AND ada.is_off = 1
                                AND (
                                    (cgm.company_group = ada.type_id AND ada.type = 4)
                                )
                                AND ada.category_date IS NOT NULL",
                            array()
                        );
        

        $select->joinLeft(array('dc' => 'date_category'),
                            "dc.id = ada.category_date
                            AND ada.category_date IS NOT NULL",
                            array()
                        );
        
        $select->joinLeft(array('cid' => 'check_in_detail'),
                            "date(t.created_at) = cid.check_in_day
                            AND st.code = cid.staff_code",
                            array()
                        );

        $select->joinLeft(array('dsa' => 'date_staff_advanced'),
                            "date(t.created_at) = dsa.date
                            AND dsa.staff_id = st.id
                            AND dsa.is_del <> 1",
                            array()
                        );
        
        $select->joinLeft(array('std' => 'saturday'),
                            "std.month = " . $params['month'] . " 
                            AND std.year = " . $params['year'] . "
                            AND std.type = 4 AND std.type_id = cgm.company_group",
                            array()
                        );

        $from_date = date($params['year'] . '-' . $params['month'] . '-1');
        $to_date = date($params['year'] . '-' . $params['month'] . '-' . $number_day_of_month);
        
        $select_staff_id->where("st.joined_at <= ?", $from_date);
        $select_staff_id->where("st.off_date IS NULL OR st.off_date >= ?", $from_date);
        
        if(isset($params['area']) && !empty($params['area']))
        {
            $select_staff_id->where("ar.id = ?", $params['area']);
        }
        
        if(isset($params['name']) && !empty($params['name']))
        {
            $select_staff_id->where("concat(st.firstname, ' ', st.lastname) like ?", '%' . $params['name'] . '%');
        }

        if(isset($params['email']) && !empty($params['email']))
        {
            $select_staff_id->where("st.email = ?", $params['email'] . '@oppomobile.vn');
        }

        if(isset($params['code']) && !empty($params['code']))
        {
            $select_staff_id->where("st.code = ?", $params['code']);
        }

        if(isset($params['department']) && !empty($params['department']))
        {
            $select_staff_id->where("st.department = ?", $params['department']);
        }

        if(isset($params['team']) && !empty($params['team']))
        {
            $select_staff_id->where("st.team = ?", $params['team']);
        }

        if(isset($params['title']) && !empty($params['title']))
        {
            $select_staff_id->where("st.title = ?", $params['title']);
        }

        $auth = Zend_Auth::getInstance()->getStorage()->read();

        $select_staff_id->joinLeft(array('ac' => 'asm_contract'),
                                "ac.staff_id = st.id AND ac.status = 2",
                                array());
        
        $first_day = $params['year'] . '-' . $params['month'] . '-1';

        $select_staff_id->joinLeft(array('acb' => new Zend_Db_Expr(
                    "(SELECT staff_id, MAX(from_date) as `from_date`
            FROM `asm_contract`
            WHERE from_date < '" . $first_day . "'
            AND status = 2
            GROUP BY staff_id)"
        )),
        "ac.from_date = acb.from_date AND acb.staff_id = st.id",
        array());

        $select_staff_id->joinLeft(array('acc' => 'asm_contract'),
                                "acc.staff_id = st.id AND acc.from_date = '" . $first_day ."' AND acc.status = 2",
                                array());

        $select_staff_id->where("
        ( (acb.staff_id IS NOT NULL AND acc.staff_id IS NULL) 
            OR 
        MONTH(ac.from_date) = " . $params['month'] . " AND YEAR(ac.from_date) = " . $params['year'] . ")"
        );

        $select_staff_id->group('st.id');
        $select_staff_id->group('ac.id');

        if ($limit && empty($params['approve_all']))
        {
            $select_staff_id->limitPage($page, $limit);
        }
        
        $select_staff_id->order("st.id asc");
        $select_staff_id->order("ac.id asc");
        
        $data_staff_id = $db->fetchAll($select_staff_id);
        $data['total'] = $db->fetchOne("SELECT FOUND_ROWS()");
        $array_staff_id = array();

        foreach ($data_staff_id as $key_staff_id => $value_staff_id) 
        {
            $array_staff_id[] = $value_staff_id['id'];
            $array_contract_staff_id[] = "st.id = " . $value_staff_id['id']
                            . " AND ac.id = " . $value_staff_id['contract_id'];
        }

        if(!empty($array_contract_staff_id))
        {
            $select->join(array('ac' => 'asm_contract'),
                                implode(" OR ", $array_contract_staff_id),
                                array()
                            );
        }
        else
        {
            $select->join(array('ac' => 'asm_contract'),
                                'ac.id = -1',
                                array()
                            );
        }

        $select->joinLeft(array('ct' => 'contract_term'),
                            "ac.new_contract = ct.id",
                            array()
                        );

        if(!empty($array_staff_id))
        {
            $select->where('st.id IN (?)', $array_staff_id);
        }
        else
        {
            $select->where('st.id IN (?)', array(-1));
        }

        $select->group("st.id");
        $select->group("ac.id");
        if(empty($params['approve_all']))
        {
            $data_total = $db->fetchAll($select);

            foreach ($data_total as $key_total => &$value_total) 
            {
                if(!empty($data_total[$key_total+1]) && $data_total[$key_total+1]['staff_id'] == $value_total['staff_id'])
                {
                    $value_total['contract_to_date'] = $data_total[$key_total+1]['contract_from_date'];
                }
            }
            
            $data['data'] = $data_total;
        }
        else
        {
            $data = $db->fetchOne($select);
        }

        return $data;
    }

    public function approvedMultiTime($params = array())
    {
        $array_date_approved = array();

        if(!empty($params['day']) && !empty($params['month']) && !empty($params['year']))
        {
            foreach($params['day'] as $key => $value)
            {
                $array_date_approved[] = "'" .date('Y-m-d', strtotime(date($params['year'] . '-' . $params['month'] . '-' .$value))) . "'";
            }
        }

        $sql = "UPDATE `time`
                SET status = 1,
                    approved_at = '" . date("y-m-d H:i:s") . "',
                    approved_by = " . $params['user_approved'] . ",
                    updated_by = " . $params['user_approved'] . ",
                    updated_at = '" . date("y-m-d H:i:s") . "'
                WHERE
                    staff_id = " . $params['staff_id'] . " AND 
                    date(created_at) IN (" . implode(",", $array_date_approved) . ")";
        $db = Zend_Registry::get('db');
        $db->query($sql);
        $db = null;
    }

    public function getListTrainingCheckIn($params = array())
    {
        $db = Zend_Registry::get('db');

        $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($params['month']), $params['year']);
        $from_date = date($params['year'] . "-" . $params['month'] . "-1");
        $to_date = date($params['year'] . "-" . $params['month'] . "-" . $number_day_of_month);

        $stmt = $db->prepare("call `get_training_check_in`(:from_date, :to_date)");

        $stmt->bindParam("from_date", $from_date, PDO::PARAM_STR);
        $stmt->bindParam("to_date", $to_date, PDO::PARAM_STR);
        
        $stmt->execute();
        $data = $stmt->fetchAll();

        $stmt->closeCursor();
        $stmt = $db = null;
        return $data;
    }

    public function getListStaffTraining($params = array())
    {
        $db = Zend_Registry::get('db');
        $db->query('SET SESSION group_concat_max_len = 1000000;');
        $select = $db->select()
            ->from(array("tct" => "trainer_course_timing"),array(new Zend_Db_Expr("
                            str.firstname as `firstname`,
                            str.lastname as `lastname`,
                            ar.name as `area`,
                            152 as `department`,
                            str.team as `team`,
                            str.title as `title`,
                            CONCAT('[', GROUP_CONCAT(day(tct.date) SEPARATOR ','), ']') as `list_day`")));

        $select->join(array('tcd' => 'trainer_course_detail'), 
                'tct.course_detail_id = tcd.id AND tcd.result = 1', 
                array());

        $select->join(array('str' => 'staff_training'), 
                'str.id = tcd.new_staff_id', 
                array());
        
        $select->join(array('rm' => 'regional_market'), 
                'str.regional_market = rm.id', 
                array());
    
        $select->join(array('ar' => 'area'), 
                'ar.id = rm.area_id', 
                array());

        if(isset($params['area']) && !empty($params['area']))
        {
            $select->where("ar.id = ?", $params['area']);
        }
        
        if(isset($params['name']) && !empty($params['name']))
        {
            $select->where("concat(str.firstname, ' ', str.lastname) like ?", '%' . $params['name'] . '%');
        }

        $select->where('MONTH(tct.date) = ?', $params['month']);
        $select->where('YEAR(tct.date) = ?', $params['year']);

        $select->group('str.id');
        echo $select->__toString(); die;
    }

    public function getListTraining($params = array())
    {
        $db = Zend_Registry::get('db');

        $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($params['month']), $params['year']);
        $from_date = date($params['year'] . "-" . $params['month'] . "-1");
        $to_date = date($params['year'] . "-" . $params['month'] . "-" . $number_day_of_month);

        $stmt = $db->prepare("call `get_list_staff_training`(:from_date, :to_date, :limit, :offset, :name, :code)");

        $stmt->bindParam("from_date", $from_date, PDO::PARAM_STR);
        $stmt->bindParam("to_date", $to_date, PDO::PARAM_STR);
        $stmt->bindParam("limit", $params['limit'], PDO::PARAM_INT);
        $stmt->bindParam("offset", $params['offset'], PDO::PARAM_INT);
        $stmt->bindParam("name", $params['name'], PDO::PARAM_STR);
        $stmt->bindParam("code", $params['code'], PDO::PARAM_STR);
        
        $stmt->execute();
        $data = array();
        $data['data'] = $stmt->fetchAll();

        $stmt->closeCursor();
        $data['total'] = $db->fetchOne("SELECT FOUND_ROWS()");

        $stmt = $db = null;
        return $data;
    }

    public function getListTrainingByStaffId($params = array())
    {
        $db = Zend_Registry::get('db');

        $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($params['month']), $params['year']);
        $from_date = date($params['year'] . "-" . $params['month'] . "-1");
        $to_date = date($params['year'] . "-" . $params['month'] . "-" . $number_day_of_month);

        $stmt = $db->prepare("call `get_training_day_by_staff_id`(:from_date, :to_date, :limit, :offset, :list_staff_id, :list_cmnd)");
        
        $stmt->bindParam("from_date", $from_date, PDO::PARAM_STR);
        $stmt->bindParam("to_date", $to_date, PDO::PARAM_STR);
        $stmt->bindParam("limit", $params['limit'], PDO::PARAM_INT);
        $stmt->bindParam("offset", $params['offset'], PDO::PARAM_INT);
        $stmt->bindParam("list_staff_id", $params['list_staff_id'], PDO::PARAM_STR);
        $stmt->bindParam("list_cmnd", $params['list_cmnd'], PDO::PARAM_STR);

        $stmt->execute();
        $data = $stmt->fetchAll();
        
        $stmt = $db = null;

        $data_map = array();

        foreach ($data as $key => $value) 
        {
            if(!empty($value['staff_id']))
            {
                $data_map[$value['staff_id']] = json_decode($value['list_day']);
            }
            else
            {
                $data_map[$value['cmnd']] = json_decode($value['list_day']);
            }
        }
        return $data_map;
    }

    public function getListLeaveByStaffId($params = array())
    {
        $db = Zend_Registry::get('db');

        $number_day_of_month = cal_days_in_month(CAL_GREGORIAN, intval($params['month']), $params['year']);
        $from_date = date($params['year'] . "-" . $params['month'] . "-1");
        $to_date = date($params['year'] . "-" . $params['month'] . "-" . $number_day_of_month);

        $stmt = $db->prepare("call `get_leave_day_by_staff_id`(:from_date, :to_date, :list_staff_id)");
        
        $stmt->bindParam("from_date", $from_date, PDO::PARAM_STR);
        $stmt->bindParam("to_date", $to_date, PDO::PARAM_STR);
        $stmt->bindParam("list_staff_id", $params['list_staff_id'], PDO::PARAM_STR);
        
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt = $db = null;
        $data_map = array();
        
        foreach ($data as $key => $value) 
        {
            $data_map[$value['staff_id']][$value['day']] = $value;
        }
        
        return $data_map;
    }

    public function approveOffice($staff_id, $date)
    {
        $db = Zend_Registry::get('db');

        // $sql = "UPDATE `time`
        //         SET `approve_time` = 1, `status` = 1
        //         WHERE date(created_at) = '$date' AND staff_id = $staff_id AND `lock` <> 1 ";

        $stmt = $db->prepare("call `update_time_check_in` (:staff_id, :date, null)");

        $stmt->bindParam("staff_id", $staff_id, PDO::PARAM_INT);
        $stmt->bindParam("date", $date, PDO::PARAM_STR);

        $stmt->execute();
        $stmt->closeCursor();
        $db = $stmt = null;
    }

    public function approveOfficeHR($staff_id, $date, $value)
    {
        $db = Zend_Registry::get('db');

        $stmt = $db->prepare("call `update_time_check_in` (:staff_id, :date, :value)");

        $stmt->bindParam("staff_id", $staff_id, PDO::PARAM_INT);
        $stmt->bindParam("date", $date, PDO::PARAM_STR);
        $stmt->bindParam("value", $value, PDO::PARAM_STR);

        $stmt->execute();
        $stmt->closeCursor();
        $db = $stmt = null;
    }

    public function approveAll($params)
    {
        $params['approve_all'] = 1;
        $code = $this->get_time_list(null, 0, $params);
        if(!empty($code))
        {
            $sql = "UPDATE `time`
                    SET 
                        `status` = 1,
                        `approve_time` = IF(note = 'Import from machine', 1, `approve_time`)
                    WHERE `id` IN ($code) AND `lock` <> 1";
            
            $db = Zend_Registry::get('db');
            $db->query($sql);
            $db = null;
        }
    }

    public function totalTime($params = array())
    {
        $db = Zend_Registry::get('db');

        $stmt = $db->prepare("CALL `PR_total_time` (:month, :year, :code, :department, :team, :title, :area, :limit, :offset)");

        $params['department'] = empty($params['department'])?null:$params['department'];
        $params['team'] = empty($params['team'])?null:$params['team'];
        $params['title'] = empty($params['title'])?null:$params['title'];
        $params['area'] = empty($params['area'])?null:$params['area'];
        $params['limit'] = empty($params['limit'])?null:$params['limit'];

        $stmt->bindParam('month', $params['month'], PDO::PARAM_INT);
        $stmt->bindParam('code', $params['code'], PDO::PARAM_STR);
        $stmt->bindParam('year', $params['year'], PDO::PARAM_INT);
        $stmt->bindParam('department', $params['department'], PDO::PARAM_INT);
        $stmt->bindParam('team', $params['team'], PDO::PARAM_INT);
        $stmt->bindParam('title', $params['title'], PDO::PARAM_INT);
        $stmt->bindParam('area', $params['area'], PDO::PARAM_INT);
        $stmt->bindParam('limit', $params['limit'], PDO::PARAM_INT);
        $stmt->bindParam('offset', $params['offset'], PDO::PARAM_INT);
        
        $stmt->execute();
        $data = array();
        $data['data'] = $stmt->fetchAll();

        $stmt->closeCursor();
        $data['total'] = $db->fetchOne("SELECT FOUND_ROWS()");

        $stmt = $db = null;
        return $data;
    }

    public function getDataTraining($params = array())
    {
        $db = Zend_Registry::get('db');

        $stmt = $db->prepare('CALL `PR_get_training_transfer` (:month, :year, :list_staff_id)');

        $stmt->bindParam('month', $params['month'], PDO::PARAM_INT);
        $stmt->bindParam('year', $params['year'], PDO::PARAM_INT);
        $stmt->bindParam('list_staff_id', $params['list_staff_id'], PDO::PARAM_STR);

        $stmt->execute();
        $data = $stmt->fetchAll();

        $stmt->closeCursor();
        $stmt = $db = null;

        $array_training_title = array();

        foreach($data as $key => $value)
        {
            if(empty($array_training_title[$value['staff_id']][$value['title']]))
            {
                $array_training_title[$value['staff_id']][$value['title']] = 1;
            }
            else
            {
                $array_training_title[$value['staff_id']][$value['title']]++;
            }
        }

        return $array_training_title;
    }

    public function getStaffTime($params = array())
    {
        $db = Zend_Registry::get('db');

        $stmt = $db->prepare('CALL `PR_get_staff_time`(:month, :year, :staff_id)');
        $stmt->bindParam('month', $params['month'], PDO::PARAM_INT);
        $stmt->bindParam('year', $params['year'], PDO::PARAM_INT);
        $stmt->bindParam('staff_id', $params['staff_id'], PDO::PARAM_INT);
        $stmt->execute();

        $data = $stmt->fetchAll();

        $stmt->closeCursor();
        $stmt = $db = null;
        return $data;
    }

    public function getAllowance()
    {
        $db = Zend_Registry::get('db');

        $stmt = $db->prepare("SELECT * FROM `allowance`");
        $stmt->execute();

        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $stmt = $db = null;
        return $data;
    }

    public function setStaffAllowance($params = array())
    {
        if(!empty($params['staff_id']) && !empty($params['year']) && !empty($params['month']) && !empty($params['allowance_id']))
        {
            $sql = "INSERT INTO `allowance_detail` (`year`, `month`, `staff_id`, `allowance_id`, `quantity`)
                        VALUES(:year, :month, :staff_id, :allowance_id, :quantity)
                    ON DUPLICATE KEY UPDATE `quantity` = VALUES(`quantity`)";
            
            $db = Zend_Registry::get('db');
            $stmt = $db->prepare($sql);

            $stmt->bindParam('year', $params['year'], PDO::PARAM_INT);
            $stmt->bindParam('month', $params['month'], PDO::PARAM_INT);
            $stmt->bindParam('staff_id', $params['staff_id'], PDO::PARAM_INT);
            $stmt->bindParam('allowance_id', $params['allowance_id'], PDO::PARAM_INT);
            $stmt->bindParam('quantity', $params['quantity'], PDO::PARAM_INT);

            $stmt->execute();
            $stmt->closeCursor();
            
            $db = $stmt = null;
        }
    }

    public function getStaffAllowanceById($id)
    {
        if(!empty($id))
        {
            $db = Zend_Registry::get('db');
            
            $sql = "SELECT 
                        ad.*,
                        st.code as `staff_code` 
                    FROM `allowance_detail` ad
                    JOIN `staff` st ON ad.staff_id = st.id
                    WHERE ad.id = :id";
            $stmt = $db->prepare($sql);
            $stmt->bindParam("id", $id, PDO::PARAM_STR);
            $stmt->execute();

            $data = $stmt->fetch();
            $stmt->closeCursor();
            $stmt = $db = null;
            return $data;
        }
    }

    public function getListAllowanceByStaffId($params = array())
    {
        $db = Zend_Registry::get('db');

        $stmt = $db->prepare('CALL `PR_get_staff_allowance_by_list_id`(:list_staff_id, :month, :year)');

        $params['month'] = empty($params['month'])?null:$params['month'];
        $params['year'] = empty($params['year'])?null:$params['year'];

        $stmt->bindParam('list_staff_id', $params['list_staff_id'], PDO::PARAM_STR);

        $stmt->bindParam('month', $params['month'], PDO::PARAM_INT);
        $stmt->bindParam('year', $params['year'], PDO::PARAM_INT);

        $stmt->execute();
        $data = array();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $stmt = $db = null;
        
        $data_map = array();
        foreach($data as $key => $value)
        {
            $data_map[$value['staff_id']][$value['allowance_id']] = $value['quantity'];
        }
        
        return $data_map;
    }

    public function fetchPaginationAllowance($params = array())
    {
        $db = Zend_Registry::get('db');

        $stmt = $db->prepare('CALL `PR_get_staff_allowance`(:code, :name, :month, :year, :limit, :offset)');

        $params['month'] = empty($params['month'])?null:$params['month'];
        $params['year'] = empty($params['year'])?null:$params['year'];
        $params['limit'] = empty($params['limit'])?null:$params['limit'];

        $stmt->bindParam('code', $params['code'], PDO::PARAM_STR);
        $stmt->bindParam('name', $params['name'], PDO::PARAM_STR);

        $stmt->bindParam('month', $params['month'], PDO::PARAM_INT);
        $stmt->bindParam('year', $params['year'], PDO::PARAM_INT);
        $stmt->bindParam('limit', $params['limit'], PDO::PARAM_INT);
        $stmt->bindParam('offset', $params['offset'], PDO::PARAM_INT);

        $stmt->execute();
        $data = array();
        $data['data'] = $stmt->fetchAll();
        $stmt->closeCursor();
        $data['total'] = $db->fetchOne("SELECT FOUND_ROWS()");
        $stmt = $db = null;
        return $data;
    }
}
