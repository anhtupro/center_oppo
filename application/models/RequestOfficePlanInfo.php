<?php
class Application_Model_RequestOfficePlanInfo extends Zend_Db_Table_Abstract
{
    protected $_name = 'request_office_plan_info';
    
    protected $_schema = DATABASE_SALARY;

    public function getInfo($id) {
        $db     = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['p' => DATABASE_SALARY.'.request_office_plan_info'], [
                'p.*'
            ])
            ->where('p.rq_plan_id = ?', $id)
            ->where('p.del = ?', 0);

        $result = $db->fetchAll($select);
        return $result;
    }

    function getAll($params){
        $db     = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['p' => DATABASE_SALARY.'.request_office_plan_info'], [
                'p.*',
                'project_name' => 'pp.name',
            ])
            ->joinLeft(array('rp'=> DATABASE_SALARY.'.request_office_plan'), 'rp.id = p.rq_plan_id', array())
            ->joinLeft(array('pp'=>'purchasing_project'), 'pp.id = p.project_id', array());
        
        if(!empty($params['request_type'])){
            $select->where('rp.request_type = ?', $params['request_type']);
        }
        
        $select->where('p.del = ?', 0);
        
        $data = $db->fetchAll($select);
        $result = array();
        if ($data){
            foreach ($data as $val){
                $result[$val['rq_plan_id']][$val['id']] = array(
                    'from_date'     => $val['from_date'],
                    'to_date'       => $val['to_date'],
                    'month'         => $val['month'],
                    'year'          => $val['year'],
                    'price'         => $val['price'],
                    'currency'      => $val['currency'],
                    'project_name'  => $val['project_name']
                );
            }
        }
        return $result;
    }
}