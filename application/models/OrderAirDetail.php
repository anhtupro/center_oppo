<?php

class Application_Model_OrderAirDetail extends Zend_Db_Table_Abstract
{
    protected $_name = 'order_air_detail';
    protected $_schema = DATABASE_TRADE;

    public function getCategory($stageId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['o' => DATABASE_TRADE.'.order_air_detail'], [
                         'order_air_detail_id' => 'o.id',
                         'category_id' => 'o.category_id',
                         'category_name' => 'c.name'
                     ])
                      ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'o.category_id = c.id', [])
                    ->where('o.order_air_stage_id = ?', $stageId)
                     ->order('o.id');

        $result = $db->fetchAll($select);

        return $result;
    }
}