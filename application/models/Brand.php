<?php
class Application_Model_Brand extends Zend_Db_Table_Abstract
{
	protected $_name = 'brand';
	public function getAll()
	{
	    $QBrand = new Application_Model_Brand();
	    $result = $QBrand->fetchAll()->toArray();
        foreach ($result as $brand) {
            $listBrand [] = $brand['name']; 
        }  
        return $listBrand;
	}
	
}