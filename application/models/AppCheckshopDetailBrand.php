<?php

class Application_Model_AppCheckshopDetailBrand extends Zend_Db_Table_Abstract
{
    protected $_name = 'app_checkshop_detail_brand';
    protected $_schema = DATABASE_TRADE;

    public function getDetail($store_id)
    {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'category_id' => 'd.category_id',
            'd.quantity',
            'category_name' => 'c.name',
            'brand_name' => 'b.name',
            'brand_id' => 'b.id'

        );

        $select->from(array('p'=> DATABASE_TRADE.'.app_checkshop'), $arrCols);
        $select->joinLeft(array('d' => DATABASE_TRADE.'.app_checkshop_detail_brand'), 'd.checkshop_id = p.id', array());
        $select->joinLeft(array('c' => DATABASE_TRADE.'.category'), 'd.category_id = c.id', array());
        $select->joinLeft(array('b' => DATABASE_TRADE.'.brand'), 'd.brand_id = b.id', array());
        $select->where('p.store_id = ?', $store_id);
        $select->where('d.category_id IS NOT NULL');
//        $select->where('p.is_lock = 1');
        $select->where("p.is_last = 1", NULL);
        $select->order('b.id');
        $result = $db->fetchAll($select);

        return $result;
    }

    public function getDataOutside($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['s' => 'store'], [
                'store_name' => 's.name',
                'last_checkshop_date' => 're.created_at',
                'store_id' => 's.id',
                'channel' => "(CASE WHEN (l.loyalty_plan_id IS NOT NULL AND d.is_ka = 0) THEN plan.name
                                    WHEN (d.is_ka = 1) THEN 'KA'
                                    WHEN (l.loyalty_plan_id IS NULL AND d.is_ka = 0) THEN 'SHOP THƯỜNG'
                                    END)",
                "s.partner_id",
                "area_name" => "a.name",
                "dealer_id" => "d.id",
                'created_by' => "CONCAT(st.firstname, ' ', st.lastname)",
                'category_name' => 'c.name',
                'quantity' => 'detail.quantity',
                'brand_name' => 'b.name'

            ])
//            ->joinLeft(['p' => 'v_store_staff_leader_log'], 's.id = p.store_id AND p.is_leader = 1', [])
            ->joinLeft(['d' => WAREHOUSE_DB . '.distributor'], 'd.id = s.d_id', [])
            ->joinLeft(['l' => 'dealer_loyalty'], 'l.dealer_id = (IF((d.parent = 0 OR d.parent IS NULL), d.id, d.parent)) AND l.is_last = 1', [])
            ->joinLeft(['plan' => 'loyalty_plan'], 'plan.id = l.loyalty_plan_id', [])
            ->join(['re' => DATABASE_TRADE . '.app_checkshop'], "s.id = re.store_id AND re.is_last = 1", [])
            ->joinLeft(['detail' => DATABASE_TRADE.'.app_checkshop_detail_brand'], 're.id = detail.checkshop_id', [])
            ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'detail.category_id = c.id', [])
            ->joinLeft(['b' =>DATABASE_TRADE .'.brand'], 'detail.brand_id = b.id', [])
            ->joinLeft(['r' => 'regional_market'], 's.regional_market = r.id', [])
            ->joinLeft(['a' => 'area'], 'a.id = r.area_id', [])
            ->joinLeft(['st' => 'staff'], 'st.id = re.created_by', [])
            ->where('s.del IS NULL OR s.del = 0');

        if ($params['list_area']) {
            $select->where('r.area_id IN (?)', $params['list_area']);
        }

        if(!empty($params['store_name'])){
            $select->where('s.name LIKE ?', '%'.$params['store_name'].'%');
        }

        if(!empty($params['staff_id'])){
            $select->where('p.staff_id = ?', $params['staff_id']);
        }

        if(!empty($params['area_id'])){
            $select->where('r.area_id = ?', $params['area_id']);
        }

        if(!empty($params['list_assign_area_id'])){
            $select->where('r.area_id IN (?)', $params['list_assign_area_id']);
        }

//        if(!empty($params['from_date'])){
//            $from_date = str_replace('/', '-', $params['from_date']);
//            $from_date = date('Y-m-d', strtotime($from_date));
//
//            $select->where('DATE(re.created_at) >= ?', $from_date);
//        }
//
//        if(!empty($params['to_date'])){
//            $to_date = str_replace('/', '-', $params['to_date']);
//            $to_date = date('Y-m-d', strtotime($to_date));
//            $select->where('DATE(re.created_at) <= ?', $to_date);
//        }

        $result = $db->fetchAll($select);

        return $result;
    }

    public function exportOutside($params)
    {
        ini_set("memory_limit", -1);
        ini_set("display_error", 1);
        error_reporting(~E_ALL);
        require_once 'PHPExcel.php';

        $data = $this->getDataOutside($params);

        $objExcel = new PHPExcel();
        $objExcel->setActiveSheetIndex(0);

        $sheet = $objExcel->getActiveSheet()->setTitle('Check shop hạng mục đối thủ');

        $rowCount = 2;
        $index = 1;
        $sheet->setCellValue('A1', 'STT');
        $sheet->setCellValue('B1', 'Kênh');
        $sheet->setCellValue('C1', 'Shop ID');
        $sheet->setCellValue('D1', 'Dealer ID');
        $sheet->setCellValue('E1', 'Partner ID');
        $sheet->setCellValue('F1', 'Tên shop');
        $sheet->setCellValue('G1', 'Khu vực');
        $sheet->setCellValue('H1', 'Ngày checkshop gần nhất');
        $sheet->setCellValue('I1', 'Người checkshop');
        $sheet->setCellValue('J1', 'Hạng mục');
        $sheet->setCellValue('K1', 'Hãng');
        $sheet->setCellValue('L1', 'Số lượng');

        foreach ($data as $element) {
            $sheet->setCellValue('A' . $rowCount, $index);
            $sheet->setCellValue('B' . $rowCount, $element['channel']);
            $sheet->setCellValue('C' . $rowCount, $element['store_id']);
            $sheet->setCellValue('D' . $rowCount, $element['dealer_id']);
            $sheet->setCellValue('E' . $rowCount, $element['partner_id']);
            $sheet->setCellValue('F' . $rowCount, $element['store_name']);
            $sheet->setCellValue('G' . $rowCount, $element['area_name']);
            $sheet->setCellValue('H' . $rowCount, date('d-m-Y', strtotime($element['last_checkshop_date'] )) );
            $sheet->setCellValue('I' . $rowCount, $element['created_by'] );
            $sheet->setCellValue('J' . $rowCount, $element['category_name'] );
            $sheet->setCellValue('K' . $rowCount, $element['brand_name'] );
            $sheet->setCellValue('L' . $rowCount, $element['quantity'] );

            ++$index;
            ++$rowCount;
        }

        //style sheet
        $style_border = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $sheet->getStyle('A0:L' . $rowCount)->applyFromArray($style_border);

        for ($i = 'A'; $i < 'L' ; ++$i) {
            $sheet->getStyle($i. 1)->getFont()->setBold(true);
        }


        $objWriter = new PHPExcel_Writer_Excel2007($objExcel);
        $filename = 'Check shop hạng mục đối thủ';
        $objWriter->save($filename);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Set-Cookie: fileLoading=true');
        readfile($filename);
        exit;
    }



}    