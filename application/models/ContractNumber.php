<?php

class Application_Model_ContractNumber extends Zend_Db_Table_Abstract
{
    public function fetchPagination($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get("db");

        $select_air = $db->select()
            ->from(['a' => DATABASE_TRADE . '.air_details'], ['a.contract_number'])
            ->join(['b' => DATABASE_TRADE . '.app_air'], 'b.id = a.air_id', [])
            ->where("a.contract_number <> ''");

        $select_repair = $db->select()
            ->from(['c' => DATABASE_TRADE . '.repair_details'], ['c.contract_number'])
            ->join(['d' => DATABASE_TRADE . '.repair'], 'd.id = c.repair_id', [])
            ->where("c.contract_number <> ''");

        $select_transfer = $db->select()
            ->from(['e' => DATABASE_TRADE . '.transfer'], ['e.contract_number'])
            ->where("e.contract_number <> ''");

        $select_destruction = $db->select()
            ->from(['f' => DATABASE_TRADE . '.destruction'], ['f.contract_number'])
            ->where("f.contract_number <> ''");

        if ($params['contract_number']) {
            $select_air->where('a.contract_number = ?', $params['contract_number']);
            $select_repair->where('c.contract_number = ?', $params['contract_number']);
            $select_transfer->where('e.contract_number = ?', $params['contract_number']);
            $select_destruction->where('f.contract_number = ?', $params['contract_number']);
        }

        $select_union = $db->select()->union(array($select_air, $select_repair, $select_transfer, $select_destruction));

        $select = $db->select()
            ->from(['g' => $select_union], [
                "contract_number" => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT TRIM(g.contract_number)')
            ]);

        $select->limitPage($page, $limit);
        $result = $db->fetchCol($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    public function getAirContractTotal($params = null)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE . '.air_details'], [
                'store_name' => 'd.name',
                'cost' => "SUM(c.total_price) * 1.1",  // VAT
                'contract_number' => "TRIM(a.contract_number)",
                'id' => 'b.id'
            ])
            ->join(['b' => DATABASE_TRADE . '.app_air'], 'a.air_id = b.id AND b.remove = 0', [])
            ->joinLeft(['c' => DATABASE_TRADE . '.air_quotation'], 'a.id = c.air_details_id AND c.status = 4', [])
            ->join(['d' => 'store'], 'd.id = b.store_id', [])
            ->where("a.contract_number <> ''")
            ->where("a.quantity > 0")
            ->group('a.id');

        if ($params['contract_number']) {
            $select->where('a.contract_number = ?', $params['contract_number']);
        }

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['contract_number']] [] = $element;
        }

        return $list;
    }
    
    public function getRepairContractTotal($params = null)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE . '.repair_details'], [
                'store_name' => 'd.name',
                'cost' => "SUM(c.total_price) * 1.1", // VAT
                'contract_number' => "TRIM(a.contract_number)",
                'id' => 'b.id'
            ])
            ->join(['b' => DATABASE_TRADE . '.repair'], 'a.repair_id = b.id AND b.remove = 0', [])
            ->joinLeft(['c' => DATABASE_TRADE . '.repair_quotation'], 'a.id = c.repair_details_id AND c.status = 4', [])
            ->join(['d' => 'store'], 'd.id = b.store_id', [])
            ->where("a.contract_number <> ''")
            ->where("a.quantity > 0")
            ->group('a.id');

        if ($params['contract_number']) {
            $select->where('a.contract_number = ?', $params['contract_number']);
        }

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['contract_number']] [] = $element;
        }

        return $list;
    }

    public function getTransferContractTotal($params = null)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE . '.transfer'], [
                'store_name' => 'c.name',
                'cost' => "SUM(b.total_price) * 1.1", // VAT
                'contract_number' => "TRIM(a.contract_number)",
                'id' => 'a.id'
            ])
            ->joinLeft(['b' => DATABASE_TRADE . '.transfer_price'], 'a.id = b.transfer_id AND b.status = 4', [])
            ->join(['c' => 'store'], 'a.store_from = c.id', [])
            ->where("a.contract_number <> ''")
            ->where("a.remove = 0")
            ->group('a.id');

        if ($params['contract_number']) {
            $select->where('a.contract_number = ?', $params['contract_number']);
        }

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['contract_number']] [] = $element;
        }
        
        return $list;
    }
    
    public function getDestructionContractTotal($params = null)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE . '.destruction'], [
                'store_name' => 'c.name',
                'cost' => "SUM(b.total_price) * 1.1", // VAT
                'contract_number' => "TRIM(a.contract_number)",
                'id' => 'a.id'
            ])
            ->joinLeft(['b' => DATABASE_TRADE . '.destruction_price'], 'a.id = b.destruction_id AND b.status = 4', [])
            ->join(['c' => 'store'], 'a.store_id = c.id', [])
            ->where("a.contract_number <> ''")
            ->where("a.remove = 0")
            ->group('a.id');

        if ($params['contract_number']) {
            $select->where('a.contract_number = ?', $params['contract_number']);
        }

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['contract_number']] [] = $element;
        }

        return $list;
    }


    public function getAirContractDetail($contract_number)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE . '.air_details'], [
                'b.store_id',
                'category_id' => 'a.cate_id',
                'store_name' => 'd.name',
                'contract_number' => "TRIM(a.contract_number)",
                'id' => 'b.id',
                'category_name' => 'c.name',
                'detail_id' => 'a.id',
                'contractor_name' => 'e.name',
                'contractor_id' => 'e.id'
            ])
            ->join(['b' => DATABASE_TRADE . '.app_air'], 'a.air_id = b.id AND b.remove = 0', [])
            ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'a.cate_id = c.id', [])
            ->join(['d' => 'store'], 'd.id = b.store_id', [])
            ->joinLeft(['e' => DATABASE_TRADE.'.contructors'], 'e.id = a.contractor_id', [])
            ->where('a.contract_number = ?', $contract_number)
            ->where('a.quantity > 0');

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getRepairContractDetail($contract_number)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE . '.repair_details'], [
                'b.store_id',
                'store_name' => 'd.name',
                'category_id' => 'a.category_id',
                'contract_number' => "TRIM(a.contract_number)",
                'id' => 'b.id',
                'category_name' => 'c.name',
                'detail_id' => 'a.id',
                'contractor_name' => 'e.name',
                'contractor_id' => 'e.id'
            ])
            ->join(['b' => DATABASE_TRADE . '.repair'], 'a.repair_id = b.id AND b.remove = 0', [])
            ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'a.category_id = c.id', [])
            ->join(['d' => 'store'], 'd.id = b.store_id', [])
            ->joinLeft(['e' => DATABASE_TRADE.'.contructors'], 'e.id = a.contructors_id', [])
            ->where('a.contract_number = ?', $contract_number)
            ->where('a.quantity > 0');
           
        $result = $db->fetchAll($select);

        return $result;
    }

    public function getTransferContractDetail($contract_number)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE . '.transfer'], [
                'store_id' => 'c.id',
                'store_name' => 'c.name',
                'contract_number' => "TRIM(a.contract_number)",
                'id' => 'a.id',
                'category_id' => 'd.id',
                'category_name' => 'GROUP_CONCAT(d.name)',
                'contractor_name' => 'e.name',
                'contractor_id' => 'e.id'
            ])
            ->joinLeft(['b' => DATABASE_TRADE . '.transfer_details'], 'a.id = b.transfer_id AND b.quantity > 0', [])
            ->join(['c' => 'store'], 'a.store_from = c.id', [])
            ->joinLeft(['d' => DATABASE_TRADE.'.category'], 'd.id = b.category_id', [])
            ->joinLeft(['e' => DATABASE_TRADE.'.contructors'], 'e.id = a.contractor_id', [])

            ->where("a.contract_number = ?", $contract_number)
            ->where("a.remove = 0")
            ->group('a.id');

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getDestructionContractDetail($contract_number)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE . '.destruction'], [
                'store_id' => 'c.id',
                'store_name' => 'c.name',
                'contract_number' => "TRIM(a.contract_number)",
                'id' => 'a.id',
                'category_name' => 'GROUP_CONCAT(d.name)',
                'contractor_name' => 'e.name',
                'contractor_id' => 'e.id'
            ])
            ->joinLeft(['b' => DATABASE_TRADE . '.destruction_details'], 'a.id = b.destruction_id AND b.quantity > 0', [])
            ->join(['c' => 'store'], 'a.store_id = c.id', [])
            ->joinLeft(['d' => DATABASE_TRADE.'.category'], 'd.id = b.category_id', [])
            ->joinLeft(['e' => DATABASE_TRADE.'.contructors'], 'e.id = a.contractor_id', [])

            ->where("a.contract_number = ?", $contract_number)
            ->where("a.remove = 0")
            ->group('a.id');

        $result = $db->fetchAll($select);

        return $result;
    }


    public function getAirContractCost($contract_number)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE . '.air_details'], [
                'contract_number' => "TRIM(a.contract_number)",
                'detail_id' => 'a.id',
                'title' => 'd.title',
                'total_price' => 'd.total_price',
                'quantity' => 'd.quantity'
            ])
            ->join(['b' => DATABASE_TRADE . '.app_air'], 'a.air_id = b.id AND b.remove = 0', [])
            ->join(['d' => DATABASE_TRADE . '.air_quotation'], 'a.id = d.air_details_id AND d.status = 4', [])
            
            ->where('a.contract_number = ?', $contract_number)
            ->where('a.quantity > 0');

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['detail_id']] [] = $element;
        }

        return $list;
    }

    public function getRepairContractCost($contract_number)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE . '.repair_details'], [
                'contract_number' => "TRIM(a.contract_number)",
                'detail_id' => 'a.id',
                'title' => 'd.title',
                'total_price' => 'd.total_price',
                'quantity' => 'd.quantity'
            ])
            ->join(['b' => DATABASE_TRADE . '.repair'], 'a.repair_id = b.id AND b.remove = 0', [])
            ->join(['d' => DATABASE_TRADE . '.repair_quotation'], 'a.id = d.repair_details_id AND d.status = 4', [])

            ->where('a.contract_number = ?', $contract_number)
            ->where('a.quantity > 0');

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['detail_id']] [] = $element;
        }

        return $list;
    }

    public function getTransferContractCost($contract_number)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE . '.transfer'], [
                'id' => 'a.id',
                'title' => 'd.title',
                'total_price' => 'd.total_price',
                'quantity' => 'd.quantity'
            ])
            ->join(['d' => DATABASE_TRADE . '.transfer_price'], 'a.id = d.transfer_id AND d.status = 4', [])

            ->where("a.contract_number = ?", $contract_number)
            ->where("a.remove = 0");

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['id']] [] = $element;
        }

        return $list;
    }

    public function getDestructionContractCost($contract_number)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE . '.destruction'], [
                'id' => 'a.id',
                'title' => 'd.title',
                'total_price' => 'd.total_price',
                'quantity' => 'd.quantity'
            ])
            ->join(['d' => DATABASE_TRADE . '.destruction_price'], 'a.id = d.destruction_id AND d.status = 4', [])

            ->where("a.contract_number = ?", $contract_number)
            ->where("a.remove = 0");

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['id']] [] = $element;
        }

        return $list;
    }


}