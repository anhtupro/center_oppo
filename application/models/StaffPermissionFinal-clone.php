<?php

class Application_Model_StaffPermissionFinalClone extends Zend_Db_Table_Abstract
{
    protected $_name = 'staff_permission_final_clone';

    public function getDepartment()
    {
        $db = Zend_Registry::get('db');

        $sql = "SELECT t.*
                    FROM `team` t
                    WHERE t.parent_id = 0 and t.del <>1";

        $data = $db->fetchAll($sql);
        $db = null;
        return $data;
    }

    public function checkPermission($code)
    {
        if(empty($code))
        {
            return false;
        }

        $db = Zend_Registry::get('db');
        $sql = "SELECT *
                    FROM `staff_permission` sp
                    WHERE sp.staff_code = :code
                        AND (sp.is_leader > 0 OR sp.is_manager > 0)";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("code", $code, PDO::PARAM_STR);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $stmt = $db = null;
        if(!empty($data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function fetchPagination($params = array())
    {
        $db = Zend_Registry::get('db');

        $stmt = $db->prepare("CALL `get_staff_permission_final_clone`(:group, :type_permission, :value_permission, :office, :department, :permission, :limit, :offset)");

        $params['permission'] = empty($params['permission'])?null:$params['permission'];
        $params['department'] = empty($params['department'])?null:$params['department'];
        $params['limit'] = empty($params['limit'])?null:$params['limit'];
        $params['office'] = empty($params['office'])?null:$params['office'];
        $params['group']=null;
        $stmt->bindParam("group", $params['group'], PDO::PARAM_INT);
        $stmt->bindParam("type_permission",$params['group'], PDO::PARAM_INT);
        $stmt->bindParam("value_permission",$params['group'], PDO::PARAM_INT);
        $stmt->bindParam("office",$params['office'], PDO::PARAM_INT);
        $stmt->bindParam("permission",$params['permission'], PDO::PARAM_INT);
        $stmt->bindParam("department",$params['department'], PDO::PARAM_INT);
        $stmt->bindParam("limit",$params['limit'], PDO::PARAM_INT);
        $stmt->bindParam("offset",$params['offset'], PDO::PARAM_INT);


        $stmt->execute();
        $data['data'] = $stmt->fetchAll();
        $stmt->closeCursor();
        $data['total'] = $db->query("SELECT FOUND_ROWS()")->fetchColumn();
        return $data;
    }

    public function getStaffPermissionLogin($staff_code)
    {
        $db = Zend_Registry::get('db');

        $stmt = $db->prepare("CALL `get_staff_permission_login`(:code)");

        $stmt->bindParam("code",$staff_code, PDO::PARAM_STR);

        $stmt->execute();

        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $data;
    }

    public function updateStaffPermission($params = array())
    {
        $db = Zend_Registry::get('db');

        $stmt = $db->prepare("UPDATE `staff_permission`
                                    SET is_leader = :is_leader, is_manager = :is_manager, is_global = :is_global
                                    WHERE id = :id ");

        $stmt->bindParam("is_leader", $params['is_leader'], PDO::PARAM_INT);
        $stmt->bindParam("is_manager", $params['is_manager'], PDO::PARAM_INT);
        $stmt->bindParam("is_global", $params['is_global'], PDO::PARAM_INT);
        $stmt->bindParam("id", $params['id'], PDO::PARAM_INT);

        $stmt->execute();
        $stmt->closeCursor();
        $db = $stmt = null;
    }

//        public function inserValues($values)
//        {
//            $db = Zend_Registry::get('db');
//            $sql = "INSERT INTO
//                        `staff_permission` (`staff_code`, `office_id`,`is_all_office`, `department_id`, `team_id`, `title_id`, `is_manager`, `is_leader`, `is_global`,`is_approve`)
//                    VALUES " . $values . "
//                    ON duplicate key UPDATE `is_manager` = VALUES(`is_manager`) , `is_leader` =  VALUES(`is_leader`),
//                                `department_id` = VALUES(`department_id`),`team_id` = `team_id`,
//                                `title_id` = `title_id`,
//                                `office_id` = VALUES(`office_id`), `is_all_office` = VALUES(`is_all_office`),
//                                `is_global` = VALUES(`is_global`), `is_approve` = VALUES(`is_approve`)
//                    ";
//            $db->query($sql);
//            $db = null;
//        }

    public function save($data, $id){
        $db = Zend_Registry::get('db');

        $db->beginTransaction();
        try {
            if ($id) {

                $where = $this->getAdapter()->quoteInto('id = ?', $id);

//                $this->update($data, $where);
//                debug( $this->update($data, $where)); exit;

            } else {
                    $this->insert($data);
            }

            $arr = array('code' => 1, 'message' => 'Done');
            $db->commit();
            return $arr;

        } catch (Exception $e) {
            $db->rollBack();
            $arr = array('code' => -1, 'message' => $e->getMessage());
        }

    }

    public function getOffice()
    {
        $db = Zend_Registry::get('db');

        $sql = "SELECT o.*
                    FROM `office` o
                    WHERE o.del = 0";

        $data = $db->fetchAll($sql);

        $db = null;

        return $data;
    }

    public function getStaff($input = '')
    {
        $db = Zend_Registry::get('db');

        $sql = "SELECT st.id as `id`, st.code as `code`, CONCAT(st.firstname, ' ', st.lastname, ' | ', st.code) as `staff_info`
                    FROM staff st
                    WHERE st.code = :input_code
                        OR st.email = :input_email 
                        OR CONCAT(st.firstname, ' ', st.lastname) LIKE :input_name";

        $input_code = $input;
        $input_email = $input . "@oppomobile.vn";
        $input_name = "%" . $input . "%";

        $stmt = $db->prepare($sql);

        $stmt->bindParam("input_code", $input_code, PDO::PARAM_STR);
        $stmt->bindParam("input_email", $input_email, PDO::PARAM_STR);
        $stmt->bindParam("input_name", $input_name, PDO::PARAM_STR);

        $stmt->execute();

        $data = $stmt->fetchAll();

        $stmt->closeCursor();
        $stmt = $db = null;

        return $data;
    }

    public function getListCode($code = '')
    {

    }


    public function getPermissionPurchasingRequest($staff_id)
    {
        $db = Zend_Registry::get('db');

        $data = array();

        if(!empty($staff_id)){

            $sql = "SELECT st.id AS  'staff_id', st.`code` , sp.`is_manager` , sp.`is_leader` , sp.`department_id` 
                        FROM staff st
                        INNER JOIN staff_permission sp ON st.code = sp.`staff_code` 
                        WHERE st.id = ".$staff_id."
                        AND sp.department_id is not NULL
                        AND (sp.is_leader = 1 OR sp.is_manager = 1)
                        GROUP BY  sp.`staff_code` ,  sp.`department_id` ";

            $stmt = $db->prepare($sql);
            $stmt->execute();
            $data = $stmt->fetchAll();
            $stmt->closeCursor();
            $stmt = $db = null;
        }

        return $data;
    }
    public function getOfficeId($staff_code)
    {
        $db     = Zend_Registry::get("db");
        $select = $db->select();
        $select->from(array('o'=> 'staff_permission'), array('office_id' => 'o.office_id'))->where('staff_code =?', $staff_code);

        $result = $db->fetchAll($select);
        foreach ($result as $value) {
            $officeId[] = $value['office_id'];
        }

        return $officeId;
    }

    public function getBySn($sn){

        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('sp'=>'staff_permission_final_clone'),array("*"))
            ->where('sp.del = ?',0)
            ->where('sp.sn = ?',$sn)
        ;
        $result = $db->fetchAll($select);
        return $result;
    }	

    public function updateValueApp($sn, $id, $valueApp){
        $db = Zend_Registry::get('db');
        $sql = "UPDATE `staff_permission_final_clone` SET `value_app`= " . $valueApp . " WHERE `sn` = " . $sn . " AND `value_app` = " . $id . "";
        $db->query($sql);
    }
}