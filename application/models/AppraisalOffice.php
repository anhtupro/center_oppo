<?php
class Application_Model_AppraisalOffice extends Zend_Db_Table_Abstract
{
    protected $_name = 'appraisal_office';

    /**
     * @param $staffId
     * @param $year
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getStaffQuarter($staffId, $year)
    {
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $sql = "select p.aop_id,
       p.aop_name,
       ifnull(round(sum(aot_head_department_appraisal) / count(aot_head_department_appraisal), 1), 0) point
from appraisal_office_plan p
       join appraisal_office_to_do td on p.aop_id = td.fk_plan and p.aop_type = 1 and p.aop_status = 2 and td.aotd_staff_id = :staffId and year(p.aop_from) = :year
       left join appraisal_office_member m
            on p.aop_id = m.aop_id
              and m.aom_staff_id = td.aotd_staff_id and p.aop_is_deleted = 0 and m.aom_is_deleted = 0
       left join appraisal_office_field f on m.aom_id = f.aom_id and f.aof_is_deleted = 0
       left join appraisal_office_task t on f.aof_id = t.aof_id and aot_is_deleted = 0
group by p.aop_id
order by p.aop_from";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('staffId', $staffId, PDO::PARAM_INT);
        $stmt->bindParam('year', $year, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }

    /**
     * @param $year
     * @param $staffId
     * @param $staffTitle
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getManagerQuarter($year, $staffId, $staffTitle)
    {
        $headId = 0;
        if($staffTitle != 523) {
            $headId = $staffId;
        }
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $sql = "select p.aop_id, p.aop_name, ifnull(aot_head_department_appraisal, 0) point, count(aot_head_department_appraisal) count
from appraisal_office_plan p
       join appraisal_office_to_do td on p.aop_id = td.fk_plan and p.aop_type = 1 and p.aop_status = 2 and p.aop_is_deleted and year(p.aop_from) = :year and (:headId = 0 or td.aotd_head_id = :headId)
       left join appraisal_office_member m
                 on p.aop_id = m.aop_id
                   and m.aom_staff_id = td.aotd_staff_id and p.aop_is_deleted = 0 and m.aom_is_deleted = 0
       left join appraisal_office_field f on m.aom_id = f.aom_id and f.aof_is_deleted = 0
       left join appraisal_office_task t on f.aof_id = t.aof_id and aot_is_deleted = 0
group by p.aop_id, t.aot_head_department_appraisal
order by p.aop_from";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('year', $year, PDO::PARAM_INT);
        $stmt->bindParam('headId', $headId, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }

    /**
     * @param $staffId
     * @param $year
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getStaffCapacityByYear($staffId, $year)
    {
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $sql = "select s.aos_id, s.aos_name, sum(rd.fk_dsa) / count(rd.fk_dsa) point
from appraisal_office_plan p
       join appraisal_office_to_do td
            on p.aop_id = td.fk_plan and p.aop_type = 2 and p.aop_status = 2 and p.aop_is_deleted = 0 and
               year(p.aop_from) = :year and td.aotd_staff_id = :staffId
       left join appraisal_office_capacity c
                 on c.fk_aop = td.fk_plan and c.fk_staff = td.aotd_staff_id and c.aoc_is_deleted = 0
       left join dynamic_survey_result r on c.fk_dsr = r.dsr_id
       left join dynamic_survey_result_detail rd on rd.fk_dsr = r.dsr_id
       left join dynamic_survey_question q on q.dsq_id = rd.fk_dsq
       left join appraisal_office_skill s on q.dsq_ref = s.aos_id
where rd.fk_dsa is not null
group by s.aos_id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('staffId', $staffId, PDO::PARAM_INT);
        $stmt->bindParam('year', $year, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }

    /**
     * @param $staffId
     * @param $year
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getStaffCapacityCompare($staffId, $year)
    {
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $sql = "select p.aop_id, p.aop_name, s.aos_id, s.aos_name, sum(rd.fk_dsa) / count(rd.fk_dsa) point
from appraisal_office_plan p
       join appraisal_office_to_do td
            on p.aop_id = td.fk_plan and p.aop_type = 2 and p.aop_status = 2 and year(aop_from) <= :year and p.aop_is_deleted = 0 and td.aotd_staff_id = :staffId
       left join appraisal_office_capacity c
                 on c.fk_aop = td.fk_plan and c.fk_staff = td.aotd_staff_id and c.aoc_is_deleted = 0
       left join dynamic_survey_result r on c.fk_dsr = r.dsr_id
       left join dynamic_survey_result_detail rd on rd.fk_dsr = r.dsr_id
       left join dynamic_survey_question q on q.dsq_id = rd.fk_dsq
       left join appraisal_office_skill s on q.dsq_ref = s.aos_id
where rd.fk_dsa is not null
group by p.aop_id, s.aos_id
order by p.aop_from";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('staffId', $staffId, PDO::PARAM_INT);
        $stmt->bindParam('year', $year, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }

    /**
     * @param $year
     * @param $staffId
     * @param $staffTitle
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getManagerCapacity($year, $staffId, $staffTitle)
    {

        $headId = 0;
        if($staffTitle != 523) {
            $headId = $staffId;
        }
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $sql = "select s.aos_id, s.aos_name, count(rd.fk_dsa) count, rd.fk_dsa
from appraisal_office_plan p
       join appraisal_office_to_do td
            on p.aop_id = td.fk_plan and p.aop_type = 2 and p.aop_status = 2 and year(aop_from) <= :year and p.aop_is_deleted = 0 and (:headId = 0 or td.aotd_head_id = :headId)
       left join appraisal_office_capacity c
                 on c.fk_aop = td.fk_plan and c.fk_staff = td.aotd_staff_id and c.aoc_is_deleted = 0
       left join dynamic_survey_result r on c.fk_dsr = r.dsr_id
       left join dynamic_survey_result_detail rd on rd.fk_dsr = r.dsr_id
       left join dynamic_survey_question q on q.dsq_id = rd.fk_dsq
       left join appraisal_office_skill s on q.dsq_ref = s.aos_id
where rd.fk_dsa is not null
group by s.aos_id, rd.fk_dsa
order by p.aop_from";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('year', $year, PDO::PARAM_INT);
        $stmt->bindParam('headId', $headId, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }

    /**
     * @param $planId
     * @param $staff
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function checkQuarterPlanComplete($planId, $staff)
    {
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $sql = "select t.*
from appraisal_office_to_do td
    join appraisal_office_member m on td.aotd_staff_id = m.aom_staff_id and td.fk_plan = m.aop_id and m.aom_is_deleted = 0 and td.fk_plan = :planId and td.aotd_staff_id = :staffId
    join appraisal_office_field f on m.aom_id = f.aom_id and f.aof_is_deleted = 0
    join appraisal_office_task t on f.aof_id = t.aof_id and t.aot_is_deleted = 0 and aot_self_appraisal = 0";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('planId', $planId, PDO::PARAM_INT);
        $stmt->bindParam('staffId', $staff, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }

    /**
     * @param $planId
     * @param $staff
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function checkQuarterManagerPlanComplete($planId, $staff)
    {
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $sql = "select t.*
from appraisal_office_to_do td
    join appraisal_office_member m on td.aotd_staff_id = m.aom_staff_id and td.fk_plan = m.aop_id and m.aom_is_deleted = 0 and td.fk_plan = :planId and td.aotd_staff_id = :staffId
    join appraisal_office_field f on m.aom_id = f.aom_id and f.aof_is_deleted = 0
    join appraisal_office_task t on f.aof_id = t.aof_id and t.aot_is_deleted = 0 and aot_head_department_appraisal = 0";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('planId', $planId, PDO::PARAM_INT);
        $stmt->bindParam('staffId', $staff, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }

    /**
     * @param $planId
     * @param $staff
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function checkCapacityPlanComplete($planId, $staff)
    {
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        
        /*
        $sql = "select *
from appraisal_office_to_do td
join appraisal_office_capacity c on td.aotd_staff_id = c.fk_staff and td.fk_plan = c.fk_aop and td.fk_plan = :planId and td.aotd_staff_id = :staffId";
         */ 
         
        
        $sql = "select *
from appraisal_office_to_do td
WHERE td.fk_plan = :planId and td.from_staff = :staffId";
        
        $stmt = $db->prepare($sql);
        $stmt->bindParam('planId', $planId, PDO::PARAM_INT);
        $stmt->bindParam('staffId', $staff, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }
}