<?php

class Application_Model_TradeCheckInImage extends Zend_Db_Table_Abstract
{
    protected $_name = 'check_in_image';
    protected $_schema = DATABASE_TRADE;

    public function get($checkInId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['i' => DATABASE_TRADE.'.check_in_image'], [
                         'i.*'
                     ])
        ->where('i.check_in_id = ?', $checkInId);

        $result = $db->fetchAll($select);

        return $result;
    }
}