<?php
class Application_Model_StaffRewardWarning extends Zend_Db_Table_Abstract
{
    protected $_name = 'staff_reward_warning';

    function fetchPagination($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get('db');

        $select = $db->select();

        $select->from(array('p' => $this->_name),array('p.id', 'p.staff_id', 'p.type', 'p.warning_type', 'p.content', 'p.date', 'p.file', 'warning_name' => 't.title'));
        $select->joinLeft(array('t'=> 'reward_warning_type'), 't.id = p.warning_type',array());

        if(isset($params['staff_id']) and $params['staff_id'])
        {
            $select
                ->where('p.staff_id = ?', $params['staff_id']);
        }

        $select->order(array('p.date DESC'));
        $select->where('p.del = ? OR p.del IS NULL',0);

        if ($limit)
            $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);


        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    function getInfoStaff($staff_id)
    {
        $db     = Zend_Registry::get('db');

        $select = $db->select();

        $select->from(array('p' => $this->_name),array('p.type','p.content','p.month','p.year'));

        $select ->where('p.staff_id = ?', $staff_id);

        $select->order(array('p.month DESC'));

        $select->order(array('p.year DESC'));

        $select->where('p.del = ? OR p.del IS NULL',0);

        $result = $db->fetchAll($select);

        return $result;

    }

}