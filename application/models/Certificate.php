<?php

class Application_Model_Certificate extends Zend_Db_Table_Abstract
{
    protected $_name = 'certificate';
    
    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(
                    array('p'=>$this->_name),
                    array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
                            'p.*',                           
                        ))              
            ->order('p.id DESC');      
        if ($limit) {
            $select->limitPage($page, $limit);
        }
        $result = $db->fetchAll($select);
        if ($limit) {
            $total = $db->fetchOne("select FOUND_ROWS()");
        }
        return $result;
    }
    
    function getData($params){
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(
                    array('p'=>$this->_name),
                    array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
                            'p.*',                           
                        ));
        if (isset($params['id']) and $params['id']){
            $select->where('p.id = ?', $params['id']);
        }else{        
            $select->where('p.del = ?', 0);
        }        
        $result = $db->fetchRow($select);
        return $result;
    }
    
}    