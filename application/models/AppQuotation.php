<?php
class Application_Model_AppQuotation extends Zend_Db_Table_Abstract
{
	protected $_name = 'app_quotation';
	protected $_schema = DATABASE_TRADE;
	 // function getQuotation($id){
  //        $db = Zend_Registry::get('db');
  //        $select = $db->select();
  //        $select->from(array('a' => DATABASE_TRADE.'.app_quotation'), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.id'), 'a.quotation_cat_id','a.horizontal','a.height','a.deep','a.total_dt','a.quantity','a.price','a.total_price','a.contractor_id','a.edit_id'));
  //           $select->joinleft(array('c'=>DATABASE_TRADE.'.app_quotation_cat'),'c.id = a.quotation_cat_id', array ('name' => 'c.name'));
  //           $select->where('air_id = ?',$id); 
  //           $quotation = $db->fetchAll($select);
  //           return $quotation;
  //   }
    public function getExcel($params,$id_ca,$id_con)
    {
       $db = Zend_Registry::get('db');
        $select = $db->select();

        $col = array(new Zend_Db_Expr('p.id'),'p.quotation_cat_id','p.horizontal','p.height','p.deep','p.total_dt','p.quantity','p.price','p.total_price','p.contractor_id','p.air_id','p.edit_id','p.category_id');
        $select->from(array('p' => DATABASE_TRADE.'.app_quotation'), $col)
       ->joinleft(array('q'=>DATABASE_TRADE.'.app_quotation'),'q.edit_id = p.id', array ('id1'=>'q.id','quotation_cat_id1'=>'q.quotation_cat_id','horizontal1'=>'q.horizontal','height1'=>'q.height','deep1'=>'q.deep','total_dt1'=>'q.total_dt','quantity1'=>'q.quantity','price1'=>'q.price','total_price1'=>'q.total_price','contractor_id1'=>'q.contractor_id','air_id1'=>'q.air_id','edit_id1'=>'q.edit_id','q.category_id'))
        ->joinleft(array('c'=>DATABASE_TRADE.'.app_quotation_cat'),'c.id = p.quotation_cat_id', array ('quotation_cate_name'=>'c.name'))
        ->joinleft(array('t'=>DATABASE_TRADE.'.app_type_cate_quotation'),'t.id = c.type_id', array ('type'=>'t.name_quotation'));

       

        $select->where('p.air_id = ?', $params['id']);
        $select->where('p.edit_id IS NULL');
        $select->where('p.category_id = ?',$id_ca);
        $select->where('p.contractor_id = ?',$id_con);
        
        // echo $select; exit;
        $list=$db->fetchAll($select);
        return $list;
    }
    function getQuotation($params){
       
        $db = Zend_Registry::get('db');
        $select = $db->select();

        $col = array(new Zend_Db_Expr('p.id'),'p.quotation_cat_id','p.horizontal','p.height','p.deep','p.total_dt','p.quantity','p.price','p.total_price','p.contractor_id','p.edit_id','p.category_id');
        $select->from(array('p' => DATABASE_TRADE.'.app_quotation'), $col)
        ->joinleft(array('q'=>DATABASE_TRADE.'.app_quotation'),'q.edit_id = p.id', array ('remove' => 'q.edit_id'))
        ->joinleft(array('e'=>DATABASE_TRADE.'.app_quotation_cat'),'e.id = p.quotation_cat_id', array ('name' => 'e.name','unit'=>'e.unit'))
        ->joinleft(array('x'=>DATABASE_TRADE.'.category'),'x.id = p.category_id', array ('cate_name' => 'x.name'))
        ->joinleft(array('c'=>DATABASE_TRADE.'.contructors'),'c.id = p.contractor_id', array ('contructor_name'=>'c.short_name'))
        ->joinleft(array('t'=>DATABASE_TRADE.'.app_type_cate_quotation'),'t.id = e.type_id', array ('type_name'=>'t.name_quotation'));

        if (isset($params['title']) and $params['title']==199){
            $select->where('c.user = ?', $params['id_user']);
        }

        $select->where('p.air_id = ?', $params['id']);

        $select->order('p.quotation_cat_id');

        $list=$db->fetchAll($select);
       // var_dump($list); exit;
        //echo $select; exit;
        return $list;
    }

    //insert excel
     public static function insertAllrow($params,$db){
     $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
     $config = $config->toArray();
     $db_log = new Zend_Db_Adapter_Pdo_Mysql(array(
       'host'     => $config['resources']['db']['params']['host'],
       'username' => $config['resources']['db']['params']['username'],
       'password' => My_HiddenDB::decryptDb($config['resources']['db']['params']['password']),
       'dbname'   => 'trade_test'
     ));
      
     $temp= array();
      foreach ($params as $key => $value) {
        if(!empty($value))
        {
         $temp =$params[$key];
         break;
        }
      }
      //print_r($temp); exit;
    
     $arrkey = array_keys($temp);
     $str_insert = '';
     foreach ($params as $k => $param){
      $str_insert .= "('".implode("', '", $param)."')" . ',';
     }
      
     $str_rows = rtrim($str_insert,',');
      
     $sql  = "INSERT INTO $db ";
    
     $sql .= " (`".implode("`, `", array_keys($temp))."`)";
      
     $sql .= " VALUES $str_rows ";
    //echo $sql; exit;
      $db_log->query($sql);
    }
}