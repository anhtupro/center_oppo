<?php

class Application_Model_RecheckShop extends Zend_Db_Table_Abstract
{
    protected $_name = 'recheck_shop';
    protected $_schema = DATABASE_TRADE;

    public function fetchPagination($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(['s' => 'store'], [
                'store_id' => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT s.id'),
                'store_name' => 's.name',
                'recheck_last' => 'MAX(re.created_at)'
            ])
            ->joinLeft(['r' => 'regional_market'], 's.regional_market = r.id', [])
            ->joinLeft(['v' => 'v_store_staff_leader_log'], 's.id = v.store_id AND v.is_leader = 1', [])
            ->joinLeft(['re' => DATABASE_TRADE.'.recheck_shop'], 're.store_id = s.id AND re.recheck_shop_round_id ='.$params['round_id'], [])
            ->where('s.del = 0 OR s.del IS NULL');

        if ($params['recheck_type'] == 1) {
            if ($params['limit_store'] == 1) {
                $select->join(['assign_store' => DATABASE_TRADE.'.assign_store_recheck_shop'], 's.id = assign_store.store_id AND assign_store.round_id = '. $params['round_id'], []);
            }

            if(!empty($params['list_assign_area_id'])){
                $select->where('r.area_id IN (?)', $params['list_assign_area_id']);
            }
        }

        if ($params['recheck_type'] == 2) {

            if ($params['limit_store'] == 1) {
                $select->join(['assign_store' => DATABASE_TRADE.'.assign_store_recheck_shop'], 's.id = assign_store.store_id AND assign_store.round_id = '. $params['round_id'], []);
            }
            
            $select->where('r.area_id IN (?)', $params['list_area']);
        }

        if ($params['error_image'] == 1) {
            $select->where('re.error_image = ?', 1);
        }

        if ($params['error_image'] == 2) {
            $select->where('re.error_image = ?', 0);
        }

        if ($params['status'] == 1) {
            $select->where('re.id IS NOT NULL');
        }

        if ($params['status'] == 2) {
            $select->where('re.id IS NULL');
        }


        if ($params['area_id']) {
            $select->where('r.area_id = (?)', $params['area_id']);
        }

        if ($params['store_id']) {
            $select->where('s.id = (?)', $params['store_id']);
        }

        if ($params['store_name']) {
            $select->where('s.name LIKE (?)', '%' . $params['store_name'] . '%');
        }

        if (!empty($params['regional_market'])){
            $select->where('r.id = ?', $params['regional_market']);
        }

        if (!empty($params['district'])){
            $select->where('s.district = ?', $params['district']);
        }

        if ($params['staff_id']) {
            $select->where('v.staff_id = ?', $params['staff_id']);
        }
      
        $select->group('s.id');
        $select->order("MAX(re.created_at) DESC");

        $select->limitPage($page, $limit);
        
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    public function getRecheckShop($recheckShopId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['r' => DATABASE_TRADE . '.recheck_shop'], [
                'r.id',
                'r.created_at',
                'r.app_check_shop_id',
                'r.store_id',
                'r.note',
                'r.error_image',
                'round_id' => 'r.recheck_shop_round_id',
                'staff_check_shop' => "CONCAT(st.firstname, ' ', st.lastname)",
                'shipping_address' => 's.shipping_address',
                'district_name' => 'd.name',
                'area_name' => 'a.name',
                'store_name' => 's.name'
            ])
            ->joinLeft(['st' => 'staff'], 'r.created_by = st.id', [])
            ->joinLeft(['s' => 'store'], 'r.store_id = s.id', [])
            ->joinLeft(['d' => 'regional_market'], 's.district = d.id', [])
            ->joinLeft(['e' => 'regional_market'], 's.regional_market = e.id', [])
            ->joinLeft(['a' => 'area'], 'e.area_id = a.id', [])
            ->where('r.id = ?', $recheckShopId);

        $result = $db->fetchRow($select);

        return $result;
    }

    public function getDataExportTime($params)
    {
        $db = Zend_Registry::get("db");

        $sub_child_select = $db->select()
            ->from(['r' => DATABASE_TRADE . '.recheck_shop'], [
                'id' => "MAX(r.id)"
            ])
            ->where('r.is_deleted = ?', 0)
            ->where('r.recheck_shop_round_id = ?', $params['round_id'])
            ->group('r.store_id');

        $sub_select =  $db->select()
            ->from(['m' => $sub_child_select], [
                'r.*'
            ])
            ->join(['r' => DATABASE_TRADE . '.recheck_shop'], 'r.id = m.id', []);

        $select = $db->select()
            ->from(['s' => 'store'], [
                'store_name' => 's.name',
                're.created_at',
                're.note',
                'store_id' => 'p.store_id',
                'channel' => "(CASE WHEN (l.loyalty_plan_id IS NOT NULL AND d.is_ka = 0) THEN plan.name
                                    WHEN (d.is_ka = 1) THEN 'KA'
                                    WHEN (l.loyalty_plan_id IS NULL AND d.is_ka = 0) THEN 'SHOP THƯỜNG'
                                    END)",
                "s.partner_id",
                "area_name" => "a.name",
                "dealer_id" => "d.id",
                'sale_name' => "CONCAT(sale.firstname, ' ', sale.lastname)",
                'created_by' => "CONCAT(st.firstname, ' ', st.lastname)",
                'error_image' =>"CASE WHEN (re.error_image = 1)  THEN 'Có'
                                      WHEN (re.error_image = 0) THEN 'Không'
                                       WHEN (re.error_image IS NULL) THEN '' 
                                END",
                'error_name' => 'err.name',
                'error_note' => 'img.note'

            ])
            ->joinLeft(['p' => 'v_store_staff_leader_log'], 's.id = p.store_id AND p.is_leader = 1', [])
            ->joinLeft(['d' => WAREHOUSE_DB . '.distributor'], 'd.id = s.d_id', [])
            ->joinLeft(['l' => 'dealer_loyalty'], 'l.dealer_id = (IF((d.parent = 0 OR d.parent IS NULL), d.id, d.parent)) AND l.is_last = 1', [])
            ->joinLeft(['plan' => 'loyalty_plan'], 'plan.id = l.loyalty_plan_id', [])

//            ->joinLeft(['re' => DATABASE_TRADE . '.recheck_shop'], "s.id = re.store_id AND re.is_deleted = 0 AND re.recheck_shop_round_id =" . $params['round_id'], [])
            ->joinLeft(['re' => $sub_select], 's.id = re.store_id', [])
            ->joinLeft(['img' => DATABASE_TRADE . '.recheck_shop_error_image'], "re.id = img.recheck_shop_id", [])
            ->joinLeft(['err' => DATABASE_TRADE . '.error_image'], "img.error_image_id = err.id", [])


            ->joinLeft(['r' => 'regional_market'], 's.regional_market = r.id', [])
            ->joinLeft(['a' => 'area'], 'a.id = r.area_id', [])
            ->joinLeft(['sale' => 'staff'], 'sale.id = p.staff_id', [])
            ->joinLeft(['st' => 'staff'], 'st.id = re.created_by', [])
            ->where('s.del IS NULL OR s.del = 0');

        if ($params['recheck_type'] == 1) {
            if ($params['limit_store'] == 1) {
                $select->join(['assign_store' => DATABASE_TRADE.'.assign_store_recheck_shop'], 's.id = assign_store.store_id AND assign_store.round_id = '. $params['round_id'], []);
            }

            if(!empty($params['list_assign_area_id'])){
                $select->where('r.area_id IN (?)', $params['list_assign_area_id']);
            }
        }

        if ($params['recheck_type'] == 2) {
            if ($params['limit_store'] == 1) {
                $select->join(['assign_store' => DATABASE_TRADE.'.assign_store_recheck_shop'], 's.id = assign_store.store_id AND assign_store.round_id = '. $params['round_id'], []);
            }
            $select->where('r.area_id IN (?)', $params['list_area']);
        }

        if ($params['error_image'] == 1) {
            $select->where('re.error_image = ?', 1);
        }

        if ($params['error_image'] == 2) {
            $select->where('re.error_image = ?', 0);
        }



        if ($params['status'] == 1) {
            $select->where('re.id IS NOT NULL');
        }

        if ($params['status'] == 2) {
            $select->where('re.id IS NULL');
        }

        if(!empty($params['store_name'])){
            $select->where('s.name LIKE ?', '%'.$params['store_name'].'%');
        }

        if(!empty($params['staff_id'])){
            $select->where('p.staff_id = ?', $params['staff_id']);
        }

        if(!empty($params['area_id'])){
            $select->where('r.area_id IN (?)', $params['area_id']);
        }


        if(!empty($params['from_date'])){
            $from_date = str_replace('/', '-', $params['from_date']);
            $from_date = date('Y-m-d', strtotime($from_date));

            $select->where('DATE(re.created_at) >= ?', $from_date);
        }

        if(!empty($params['to_date'])){
            $to_date = str_replace('/', '-', $params['to_date']);
            $to_date = date('Y-m-d', strtotime($to_date));
            $select->where('DATE(re.created_at) <= ?', $to_date);
        }

        $result = $db->fetchAll($select);

        return $result;
    }

    public function exportTime($params)
    {
        include 'PHPExcel.php';

        $statistics = $this->getDataExportTime($params);
        $objExcel = new PHPExcel();

        $objExcel->setActiveSheetIndex(0);

        $sheet = $objExcel->getActiveSheet()->setTitle('Recheck Shop');

        $rowCount = 2;
        $index = 1;

        $sheet->setCellValue('A1', 'STT');
        $sheet->setCellValue('B1', 'KÊNH');
        $sheet->setCellValue('C1', 'ID SHOP');
        $sheet->setCellValue('D1', 'DEALER ID');
        $sheet->setCellValue('E1', 'PARTNER ID');
        $sheet->setCellValue('F1', 'TÊN SHOP');
        $sheet->setCellValue('G1', 'KHU VỰC');
        $sheet->setCellValue('H1', 'LỖI HÌNH ẢNH');
        $sheet->setCellValue('I1', 'LOẠI LÕI');
        $sheet->setCellValue('J1', 'NOTE');
        $sheet->setCellValue('K1', 'NGÀY RECHECK SHOP');
        $sheet->setCellValue('L1', 'Giờ RECHECK SHOP');
        $sheet->setCellValue('M1', 'SALE QUẢN LÝ SHOP');
        $sheet->setCellValue('N1', 'NHÂN VIÊN RECHECK SHOP');


        foreach ($statistics as $statistic) {
            $sheet->setCellValue('A' . $rowCount, $index);
            $sheet->setCellValue('B' . $rowCount, $statistic['channel']);
            $sheet->setCellValue('C' . $rowCount, $statistic['store_id']);
            $sheet->setCellValue('D' . $rowCount, $statistic['dealer_id']);
            $sheet->setCellValue('E' . $rowCount, $statistic['partner_id']);
            $sheet->setCellValue('F' . $rowCount, $statistic['store_name']);
            $sheet->setCellValue('G' . $rowCount, $statistic['area_name']);
            $sheet->setCellValue('H' . $rowCount, $statistic['error_image']);
            $sheet->setCellValue('I' . $rowCount, $statistic['error_name']);
            $sheet->setCellValue('J' . $rowCount, $statistic['error_note'] ? $statistic['error_note'] : '');
            $sheet->setCellValue('K' . $rowCount, $statistic['created_at'] ? date('d/m/Y', strtotime($statistic['created_at'])) : '' );
            $sheet->setCellValue('l' . $rowCount, $statistic['created_at'] ? date('H:i:s', strtotime($statistic['created_at'])) : '' );
            $sheet->setCellValue('M' . $rowCount, $statistic['sale_name'] );
            $sheet->setCellValue('N' . $rowCount, $statistic['created_by'] );

            ++$index;
            ++$rowCount;
        }

        //style sheet
        $style_border = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $sheet->getStyle('A0:M' . $rowCount)->applyFromArray($style_border);

        for ($i = 'A'; $i < 'N' ; ++$i) {
            $sheet->getStyle($i. 1)->getFont()->setBold(true);
        }


        $objWriter = new PHPExcel_Writer_Excel2007($objExcel);
        $filename = 'Recheck shop';
        $objWriter->save($filename);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Set-Cookie: fileLoading=true');
        readfile($filename);
        exit;
    }
}    