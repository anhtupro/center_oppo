<?php
class Application_Model_Event extends Zend_Db_Table_Abstract
{
    public function check_staff($staff_id){
    	$db = Zend_Registry::get('db');
        $sql = 'SELECT *
				FROM team_building 
				WHERE staff_id = :staff_id';
        $stmt = $db->prepare($sql);
        $stmt->bindParam("staff_id",  $staff_id, PDO::PARAM_STR);
        $stmt->execute();

        $data = $stmt->fetch();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $data;
    }

    public function update_status($staff_id){
    	$db = Zend_Registry::get('db');
        $sql = 'UPDATE team_building SET status = 1 WHERE staff_id = :staff_id';
        $stmt = $db->prepare($sql);
        $stmt->bindParam("staff_id",  $staff_id, PDO::PARAM_STR);
        

        $stmt->execute();
        $stmt->closeCursor();
        $db = $stmt = null;
        
    }
    public function get_data_team($team){
    	$db = Zend_Registry::get('db');
        $sql = 'SELECT t.full_name,t.staff_id,
				s.`code`,t.team color,department.`name` department_name,team.`name` team_name,title.`name` title_name,
				s.gender,s.photo,t.`status`,s.phone_number
				FROM team_building t
				LEFT JOIN staff s
				ON t.staff_id = s.id
				LEFT JOIN team team ON s.team = team.id
				LEFT JOIN team title ON s.title = title.id
				LEFT JOIN team department ON s.department = department.id AND department.parent_id = 0 
				WHERE t.team = :team AND is_mentor = 0
				ORDER BY t.`status` DESC,staff_id ASC
				';
        $stmt = $db->prepare($sql);
        $stmt->bindParam("team",  $team, PDO::PARAM_STR);
        $stmt->execute();

        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $data;
    }
}
