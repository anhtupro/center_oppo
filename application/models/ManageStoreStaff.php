<?php
class Application_Model_ManageStoreStaff extends Zend_Db_Table_Abstract
{
	public function saveStoreLeader($string){
		$db     = Zend_Registry::get('db');
		$sql = 'INSERT INTO store_leader_log (staff_id,store_id,joined_at) VALUES' .$string;
		// echo $sql;die;
		$stmt = $db->prepare($sql);
	    $stmt->execute();
	    $res =  $db->lastInsertId();
	    $stmt->closeCursor();
	    $db = $stmt = null;

	    return $res;
	}
	public function saveStoreStaff($string){
		$db     = Zend_Registry::get('db');
		$sql = 'INSERT INTO store_staff_log (staff_id,store_id,is_leader,joined_at) VALUES' .$string;
		$stmt = $db->prepare($sql);
	    $stmt->execute();
	    $res =  $db->lastInsertId();
	    $stmt->closeCursor();
	    $db = $stmt = null;

	    return $res;
	}
	public function logStaff($params = array()){
		
	    $db = Zend_Registry::get('db');
            $params = array_merge(
                  array(
						's_id'		=> null,
						'type'		=> null,
						'is_level'	=> null,
						'staff_id'	=> null,
						'store_id'	=> null,
						'date'		=> null
                  ),
                  $params
            );
            // echo "<pre>";print_r($params);die;
            $sql = "INSERT INTO assign_store_log (s_id,action,type,is_level,staff_id,store_id,date) VALUES (:s_id,1,:type,:is_level,:staff_id,:store_id,:date)";
            $stmt = $db->prepare($sql);
            $stmt->bindParam("s_id",  $params['s_id'], PDO::PARAM_STR);
            $stmt->bindParam("type",  $params['type'], PDO::PARAM_STR);
            $stmt->bindParam("is_level",  $params['is_level'], PDO::PARAM_STR);
            $stmt->bindParam("staff_id",  $params['staff_id'], PDO::PARAM_STR);
            $stmt->bindParam("store_id",  implode("",$params['store_id']), PDO::PARAM_STR);
            $stmt->bindParam("date",  $params['date'], PDO::PARAM_STR);
            $stmt->execute();
            $stmt->closeCursor();
            $db = $stmt = null;
	}
	public function logLeader($params = array()){
		
	    $db = Zend_Registry::get('db');
            $params = array_merge(
                  array(
						's_id'		=> null,
						'type'		=> null,
						'staff_id'	=> null,
						'store_id'	=> null,
						'date'		=> null
                  ),
                  $params
            );
            // echo "<pre>";print_r($params);die;
            $sql = "INSERT INTO assign_store_log (s_id,action,type,staff_id,store_id,date) VALUES (:s_id,1,:type,:staff_id,:store_id,:date)";
            $stmt = $db->prepare($sql);
            $stmt->bindParam("s_id",  $params['s_id'], PDO::PARAM_STR);
            $stmt->bindParam("type",  $params['type'], PDO::PARAM_STR);
            $stmt->bindParam("staff_id",  $params['staff_id'], PDO::PARAM_STR);
            $stmt->bindParam("store_id",  implode("",$params['store_id']), PDO::PARAM_STR);
            $stmt->bindParam("date",  $params['date'], PDO::PARAM_STR);
            $stmt->execute();
            $stmt->closeCursor();
            $db = $stmt = null;
	}
	public function check_store($table,$staff_id,$store_id){
		$db     = Zend_Registry::get('db');
		$sql = 'SELECT s.`name` 
				FROM '.$table.' stl
				JOIN store s
				ON stl.store_id = s.id
				WHERE stl.staff_id = '.$staff_id.' AND stl.released_at is NULL AND stl.store_id IN ('.$store_id.')';
		$stmt = $db->prepare($sql);
	    $stmt->execute();
	    $res =  $stmt->fetchAll();
	    $stmt->closeCursor();
	    $db = $stmt = null;

	    return $res;
	}
	public function check_province($staff_id,$store_id){
		$db     = Zend_Registry::get('db');
		$province_staff = $db->query('SELECT regional_market FROM staff where id = '.$staff_id.'')->fetch();
		$province_store = $db->query('SELECT regional_market FROM store where id IN ('.$store_id.')')->fetch();

		$staff = implode("",$province_staff);
		$store = implode("",$province_store);
		// echo $staff;echo"<br>";echo $store;die;
		$res;

		if($staff == $store)
			$res = 1;
		else
			$res = 0;

	    $db = null;
	    return $res;
	}
	public function check_pgpb($store_id){
		$db  = Zend_Registry::get('db');
		$sql = 'SELECT * FROM store_staff_log WHERE store_id = '.$store_id.' AND released_at IS NULL AND is_leader = 0';
		$stmt = $db->prepare($sql);
	    $stmt->execute();
	    $res =  $stmt->fetch();
	    $stmt->closeCursor();
	    $db = $stmt = null;

	    return $res;
	}
	public function check_region_share($staff_id){
		$db  = Zend_Registry::get('db');
		$sql = 'SELECT s.staff_id,s1.shared
				FROM sales_region_staff_log s
				JOIN sales_region s1 
				ON s.sales_region_id = s1.id
				WHERE s.staff_id ='.$staff_id;

		$stmt = $db->prepare($sql);
	    $stmt->execute();
	    $res =  $stmt->fetchAll();
	    $stmt->closeCursor();
	    $db = $stmt = null;

	    return $res;
	} 
	public function check_leader_is_sales($staff_id)
	{
		$db  = Zend_Registry::get('db');
		$sql = 'SELECT * FROM store_staff_log WHERE staff_id = '.$staff_id.' AND released_at IS NULL';
		$stmt = $db->prepare($sql);
	    $stmt->execute();
	    $res =  $stmt->fetchAll();
	    $stmt->closeCursor();
	    $db = $stmt = null;

	    return $res;
	}
	public function get_day_transfer($staff_id){
		//echo $staff_id;die;
		$db     = Zend_Registry::get('db');
		$sql = 'SELECT MAX(from_date) as from_date FROM staff_transfer WHERE staff_id ='.$staff_id;
		$stmt = $db->prepare($sql);
		
	    $stmt->execute();

	    $res =  $stmt->fetch(); 
	    $stmt->closeCursor();
	    $db = $stmt = null;

	    return $res;
	}

	public function get_joined_at($staff_id){
		//echo $staff_id;die;
		$db     = Zend_Registry::get('db');
		$sql = 'SELECT joined_at FROM staff WHERE id ='.$staff_id;
		$stmt = $db->prepare($sql);
		
	    $stmt->execute();

	    $res =  $stmt->fetch(); 
	    $stmt->closeCursor();
	    $db = $stmt = null;

	    return $res;
	}
	public function get_title($staff_id){
		$db     = Zend_Registry::get('db');
		$sql = 'SELECT title 
				FROM  staff
				WHERE id ='.$staff_id;
		$stmt = $db->prepare($sql);
		
	    $stmt->execute();

	    $res =  $stmt->fetch(); 
	    $stmt->closeCursor();
	    $db = $stmt = null;

	    return $res;
	}
	public function get_email($staff_id){
		$db     = Zend_Registry::get('db');
		$sql = 'SELECT `code`
				FROM  staff
				WHERE id ='.$staff_id;
		$stmt = $db->prepare($sql);
		
	    $stmt->execute();

	    $res =  $stmt->fetch(); 
	    $stmt->closeCursor();
	    $db = $stmt = null;

	    return $res;
	}
}
