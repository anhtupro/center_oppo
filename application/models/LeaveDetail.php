<?php

class Application_Model_LeaveDetail extends Zend_Db_Table_Abstract{

    protected $_name = 'leave_detail';

	private function dbconnect(){
		return Zend_Registry::get('db');
	}

    public function editImage($params = array())
    {
        $db = Zend_Registry::get('db');

        $sql_update = "UPDATE `leave_detail`
                        SET image = :image
                        WHERE id = :id";
        $stmt = $db->prepare($sql_update);
        $stmt->bindParam('image', $params['image'], PDO::PARAM_STR);
        $stmt->bindParam('id', $params['id'], PDO::PARAM_INT);

        $stmt->execute();
        
        $stmt->closeCursor();
        $stmt = $db = null;

    }

    public function getLeaveById($id, $staff_id)
    {
        $db = $this->dbconnect();

        $sql = "SELECT
                    ld.*,
                    lt.note,
                    CONCAT(st.firstname, ' ', st.lastname) AS `staff_name`,
                    st.joined_at,
                    CONCAT(st2.firstname, ' ', st2.lastname) AS `staff_name_approved`,
                    CONCAT(st3.firstname, ' ', st3.lastname) AS `staff_name_hr_approved`
                FROM `leave_detail` ld
                JOIN `leave_type` lt ON ld.leave_type = lt.id
                JOIN `staff` st ON ld.staff_id = st.id
                LEFT JOIN `staff` st2 ON ld.approved_by = st2.id
                LEFT JOIN `staff` st3 ON ld.hr_approved_by = st3.id
                WHERE ld.staff_id = :staff_id and ld.`status` <> 2";

        if(isset($id) && $id){
            $sql .=' AND ld.id = :id';
                    }


        $stmt = $db->prepare($sql);
        if(isset($id) && $id){
            $stmt->bindParam('id', $id, PDO::PARAM_INT);
        }
        $stmt->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
        $stmt->execute();
        if(isset($id) && $id){
            $data = $stmt->fetch();
        }
        else {
            $data = $stmt->fetchAll();
        }

        $stmt->closeCursor();
        $stmt = $db = null;

        return $data;
    }

    public function getDetailLeaveById($id)
    {
        $db = $this->dbconnect();

        $sql = "SELECT
                    ld.*,
                    lt.note,
                    CONCAT(st.firstname, ' ', st.lastname) AS `staff_name`,
                    CONCAT(st2.firstname, ' ', st2.lastname) AS `staff_name_approved`,
                    CONCAT(st3.firstname, ' ', st3.lastname) AS `staff_name_hr_approved`
                FROM `leave_detail` ld
                JOIN `leave_type` lt ON ld.leave_type = lt.id
                JOIN `staff` st ON ld.staff_id = st.id
                LEFT JOIN `staff` st2 ON ld.approved_by = st2.id
                LEFT JOIN `staff` st3 ON ld.hr_approved_by = st3.id
                WHERE ld.id = :id";

        $stmt = $db->prepare($sql);
        $stmt->bindParam('id', $id, PDO::PARAM_INT);
        $stmt->execute();
        $data = $stmt->fetch();

        $stmt->closeCursor();
        $stmt = $db = null;

        return $data;
    }

    public function checkHalfLeave()
    {
        $db = Zend_Registry::get('db');

        $sql = "select lhc.id
                from `company_group` cg 
                join `leave_half_config` lhc
                    on lhc.group = cg.id
                join `company_group_map` cgm
                    on cgm.company_group = cg.id
                where cgm.title = :title and lhc.status = 1";

        $auth = Zend_Auth::getInstance()->getStorage()->read();
        $stmt = $db->prepare($sql);
    
        $stmt->bindParam('title', $auth->title, PDO::PARAM_INT);
        $stmt->execute();

        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $stmt = $db = null;

        if(empty($data))
        {
            return false;
        }
        return true;
    }

    public function insertLeave($params = array())
    {
        $db = $this->dbconnect();
        $outp_message = "messages";
        $stmt = $db->prepare("CALL PR_create_leave(:staff_id, :from, :to, :is_officer, :reason, :leave_type, :image, @outp_message, @outp_status)");
        $stmt->bindParam("staff_id", $params['staff_id'], PDO::PARAM_INT);
        $stmt->bindParam("from", $params['from'], PDO::PARAM_STR);
        $stmt->bindParam("to", $params['to'], PDO::PARAM_STR);
        $stmt->bindParam("is_officer", $params['is_officer'], PDO::PARAM_INT);
        $stmt->bindParam("reason", $params['reason'], PDO::PARAM_STR);
        $stmt->bindParam("leave_type", $params['leave_type'], PDO::PARAM_INT);
        $stmt->bindParam("image", $params['image'], PDO::PARAM_STR);
        $stmt->execute();
        $outp_message = $db->query("select @outp_message as `message`")->fetch();
        $outp_status = $db->query("select @outp_status as `status`")->fetch();

        $data = array();
        $data['message'] = $outp_message['message'];
        $data['status'] = $outp_status['status'];
		$stmt->closeCursor();
		$db = $stmt = null;
        return $data;
    }

    public function getImageById($id)
    {
        $db = Zend_Registry::get('db');
        $sql = "SELECT `image`, `staff_id` FROM `leave_detail` WHERE id = :id";

        $stmt = $db->prepare($sql);
        $stmt->bindParam('id', $id, PDO::PARAM_INT);

        $stmt->execute();
        $data = $stmt->fetch();
        $stmt->closeCursor();
        $stmt = $db = null;

        return $data;
    }

    public function insertLeaveHalf($params = array())
    {
        $db = $this->dbconnect();
        $outp_message = "messages";
        $stmt = $db->prepare("CALL PR_create_leave_half(:staff_id, :date, :is_officer, :reason, :leave_type, :image, @outp_message, @outp_status)");
        $stmt->bindParam("staff_id", $params['staff_id'], PDO::PARAM_INT);
        $stmt->bindParam("date", $params['date'], PDO::PARAM_STR);
        $stmt->bindParam("is_officer", $params['is_officer'], PDO::PARAM_INT);
        $stmt->bindParam("reason", $params['reason'], PDO::PARAM_STR);
        $stmt->bindParam("leave_type", $params['leave_type'], PDO::PARAM_INT);
        $stmt->bindParam("image", $params['image'], PDO::PARAM_STR);

        $res = $stmt->execute();
        $outp_message = $db->query("select @outp_message as `message`")->fetch();
        $outp_status = $db->query("select @outp_status as `status`")->fetch();

        $data = array();
        $data['message'] = $outp_message['message'];
        $data['status'] = $outp_status['status'];
		$stmt->closeCursor();
		$db = $stmt = null;
        return $data;
    }

    public function approvalLeave($id)
    {
        if(isset($id))
        {
            $data = array(
                'status' => 1,
            );
            $where = $this->getAdapter()->quoteInto('id = ?',$id);
            $this->update($data, $where);
        }
    }

    public function deleteLeave($id)
    {
        if(isset($id))
        {
            $where = $this->getAdapter()->quoteInto("id = ?", $id);
            $this->delete($where);
        }
    }

    public function _select($limit = 10, $page = 1, $params = array())
    {
        $db = $this->dbconnect();

        $select = $db->select()
            ->from(array("ld" => "leave_detail"),array(
                    new Zend_Db_Expr('SQL_CALC_FOUND_ROWS ld.id, ld.*
                                    , ld.total as `total`
                                    , lt.need_images as `need_images`
                                    , IF( (ld.image = "" OR ld.image = "[]") AND lt.need_images > 0, 1, 0 ) as `need_upload_image`
                                    , IF( (ld.image = "" OR ld.image = "[]") AND lt.need_images, 1, 0 ) as `sort_image`
                                    ,SUM(
                                	CASE WHEN e.date IS NOT NULL THEN 0 
                                	WHEN IFNULL(f.time_number, 0) + IFNULL(h.office_time,0) + CASE WHEN g.check_in_day IS NOT NULL AND g.status = 1 THEN IF(g.is_half_day = 1, 0.5, 1) ELSE 0 END >= 1 THEN 0
                                	WHEN IFNULL(f.time_number, 0) + IFNULL(h.office_time,0) + CASE WHEN g.check_in_day IS NOT NULL AND g.status = 1 THEN IF(g.is_half_day = 1, 0.5, 1) ELSE 0 END  + IF(IFNULL(ld.is_half_day,0) <> 0, 0.5, 1) > 1 THEN (IFNULL(f.time_number, 0) + IFNULL(h.office_time,0) + CASE WHEN g.check_in_day IS NOT NULL AND g.status = 1 THEN IF(g.is_half_day = 1, 0.5, 1) ELSE 0 END  + IF(IFNULL(ld.is_half_day,0) <> 0, 0.5, 1)) - 1
                                	ELSE IF(IFNULL(ld.is_half_day,0) <> 0, 0.5, 1) END) AS Tong
                                    '
                )))
            ->join(array("st" => "staff"), "st.id = ld.staff_id", array("staff_name" => "concat(st.firstname,' ',st.lastname)"))
            ->join(array("lt" => "leave_type"), "lt.id = ld.leave_type", array("leave_type_note" => "lt.note"))
            ->join(array("ltp" => "leave_type"), "lt.parent = ltp.id", array("leave_type_parent_note" => "ltp.note", "leave_type_parent_id" => "ltp.id"))
            //->joinLeft(array("cgm" => "company_group_map"), "cgm.title = st.title", array())
            ->join(array("ad" => "all_date"), "(ad.date BETWEEN ld.from_date AND ld.to_date) ", array())
           // ->joinLeft(array("ada" => "all_date_advanced"), "ada.date = ad.date AND ada.type_id = cgm.company_group AND ada.type = 4", array())
            ->joinLeft(array("f" => "time_machine"), "ad.date = f.check_in_day AND ld.staff_id = f.staff_id", array())
            ->joinLeft(array("g" => "time_gps"), "ad.date = g.check_in_day AND ld.staff_id = g.staff_id", array())
            ->joinLeft(array("h" => "temp_time"), "ad.date = h.date AND ld.staff_id = h.staff_id", array())
            //  ->joinLeft(array("ada" => "all_date_advanced"), "ada.date = ad.date AND ada.type_id = cgm.company_group AND ada.type = 4", array());
            ->joinLeft(array( 'e' => new Zend_Db_Expr (
                "(SELECT DISTINCT a.`date`, b.title FROM `all_date_advanced` a INNER JOIN company_group_map b ON a.`type_id` = b.company_group)" )
            ), 'st.title = e.title AND ad.date = e.date', array());
        if(isset($params['staff_id']))
        {
            $select->where("ld.staff_id = ?", $params['staff_id']);
        }

        if(isset($params['name']) && !empty($params['name']))
        {
            $select->where("concat(st.firstname, ' ', st.lastname) like ?", '%' . $params['name'] . '%');
        }

        if(isset($params['email']) && !empty($params['email']))
        {
            $select->where("st.email = ?", $params['email'] . '@oppomobile.vn');
        }

        if(isset($params['status']) && $params['status'] != '')
        {
            $select->where("ld.status = ?", $params['status']);
        }

        if(!empty($params['from']) && !empty($params['to']))
        {
            $select->where("ld.from_date >= ?", $params['from']);
            $select->where("ld.from_date <= ?", $params['to']);
        }


        $select->order("sort_image desc");
        $select->group("ld.id");
		
        $data['data'] = $db->fetchAll($select);
        $data['total'] = $db->fetchOne("SELECT FOUND_ROWS()");
//		 echo $select->__toString();
        return $data;
    }


    public function updateStatus($params = array())
    {
        $db = Zend_Registry::get('db');

        $stmt = $db->prepare('CALL `PR_update_status_leave` (:id, :status, :admin, :note)');
        $userStorage = Zend_Auth::getInstance ()->getStorage ()->read ();
        $params['status'] = empty($params['status'])?null:$params['status'];
        $stmt->bindParam('id', $params['id'], PDO::PARAM_INT);
    $stmt->bindParam('status', $params['status'], PDO::PARAM_INT);
        $stmt->bindParam('admin', $userStorage->id , PDO::PARAM_INT);
        $stmt->bindParam('note', $params['note'], PDO::PARAM_STR);
        $stmt->execute();
        $stmt->closeCursor();
        $stmt = $db = null;
    }

    public function updateHrStatus($params = array())
    {
        $db = Zend_Registry::get('db');

        $stmt = $db->prepare('CALL `PR_hr_approved` (:id, :status,:hr_approved_by)');

        $params['status'] = empty($params['status'])?null:$params['status'];
        $stmt->bindParam('id', $params['id'], PDO::PARAM_INT);
        $stmt->bindParam('status', $params['status'], PDO::PARAM_INT);
        $stmt->bindParam('hr_approved_by', $params['hr_approved_by'], PDO::PARAM_INT);
        $stmt->execute();
        $stmt->closeCursor();
        $stmt = $db = null;
    }
    
    public function updateHrChooseDateStatus($params = array()){
        $db = Zend_Registry::get('db');

        $stmt = $db->prepare('CALL `PR_hr_approved_choose_date` (:id, :status,:hr_approved_by,:hr_approved_at)');

        $params['status'] = empty($params['status'])?null:$params['status'];
        $stmt->bindParam('id', $params['id'], PDO::PARAM_INT);
        $stmt->bindParam('status', $params['status'], PDO::PARAM_INT);
        $stmt->bindParam('hr_approved_by', $params['hr_approved_by'], PDO::PARAM_INT);
        $stmt->bindParam('hr_approved_at', $params['hr_approved_at'], PDO::PARAM_STR);
        $stmt->execute();
        $stmt->closeCursor();
        $stmt = $db = null;
    }

    public function _selectAdmin($limit = 10, $page = 1, $params = array(),$list_export_staff_id = '')
    {
        $db = $this->dbconnect();
		
        $select = $db->select()
            ->from(array("ld" => "leave_detail"),array(
                    new Zend_Db_Expr('SQL_CALC_FOUND_ROWS ld.id, ld.*
                                    , st.code as code 
                                    , t1.name as `department_name`
                                    , t2.name as `team_name`
                                    , IF(ld.total >= 1, DATEDIFF(ld.to_date, ld.from_date) + 1,ld.total) as `total`
                                    , SUM(IF(ad2.id IS NOT NULL, 1, 0 )) as `day_leave`
                                    , IF( (ld.image = "" OR ld.image = "[]") AND lt.need_images > 0, 1, 0 ) as `sort_image`'
                )))
            ->join(array("st" => "staff"), "st.id = ld.staff_id", array("staff_name" => "concat(st.firstname,' ',st.lastname)"))
            ->join(array("lt" => "leave_type"), "lt.id = ld.leave_type", array("leave_type_note" => "lt.note"))
            ->join(array("lt2" => "leave_type"), "lt.parent = lt2.id", array("id_leave_group" => "lt2.id","leave_group" => "lt2.note"))
            ->joinLeft(array("cgm" => "company_group_map"), "cgm.title = st.title", array())
            ->join(array("t1" => "team"), "t1.id = st.department", array())
            ->join(array("t2" => "team"), "t2.id = st.team", array())
            ->join(array("t3" => "team"), "t3.id = st.title", array("title_name" => "t3.name"))
            ->join(array("rm" => "regional_market"), "st.regional_market = rm.id" ,array())
            ->join(array("ar" => "area"), "rm.area_id = ar.id" ,array('area_name' => 'ar.name'))
            ->joinLeft(array("ad" => "all_date"), "(ad.date BETWEEN ld.from_date AND ld.to_date) AND YEAR(ad.date) = YEAR(CURRENT_DATE())", array())
            ->joinLeft(array("ad2" => "all_date"), "ad2.date < NOW() AND (ad2.date BETWEEN ld.from_date AND ld.to_date) AND YEAR(ad2.date) = YEAR(CURRENT_DATE())", array())
            ->joinLeft(array("ada" => "all_date_advanced"), "ada.date = ad.date AND ada.type_id = cgm.company_group AND ada.type = 4", array());

        // $select->where("ada.id IS NULL");
        if(isset($params['staff_id']))
        {
            $select->where("ld.staff_id = ?", $params['staff_id']);
        }

        if(isset($params['name']) && !empty($params['name']))
        {
            $select->where("concat(st.firstname, ' ', st.lastname) like ?", '%' . $params['name'] . '%');
        }

        if(isset($params['code']) && !empty($params['code']))
        {
            $select->where("st.code = ?", $params['code']);
        }

        if(isset($params['department']) && !empty($params['department']))
        {
            $select->where("st.department = ?", $params['department']);
        }

        if(isset($params['team']) && !empty($params['team']))
        {
            $select->where("st.team = ?", $params['team']);
        }

        if(isset($params['title']) && !empty($params['title']))
        {
            $select->where("st.title = ?", $params['title']);
        }

        if($params['status'] == '0')
        {
            $select->where("ld.status = 0");
        }
        elseif($params['status'] == '1')
        {
            $select->where("ld.status = 1");
        }

        if( $params['hr_approved'] == '0')
        {
            $select->where("ld.hr_approved = 0");
        }
        elseif($params['hr_approved'] == '1')
        {
            $select->where("ld.hr_approved = 1");
        }

        // print_r($params); die;

        if(isset($params['parent_leave_type']) && !empty($params['parent_leave_type']))
        {
            $select->where("lt2.id = ?", $params['parent_leave_type'] );
        }

        if(isset($params['id']) && $params['id'] != '')
        {
            $select->where("ld.id = ?", $params['id']);
        }

        if(!empty($params['from']) && !empty($params['to']))
        {
            $select->where("ld.from_date >= ?", $params['from']);
            $select->where("ld.from_date <= ?", $params['to']);
        }

        if(!empty($params['from_date']) && !empty($params['to_date']))
        {
            $select->where("ld.from_date >= ?", $params['from_date']);
            $select->where("ld.from_date <= ?", $params['to_date']);
        }
		
        if($list_export_staff_id){
                $select->where("st.id IN ($list_export_staff_id)", '' );	
        }
        if(isset($params['list_staff']) && !empty($params['list_staff'])){
            $list_staff = $params['list_staff'];
            if(!empty($list_staff)){
                    $select->where("st.id in ($list_staff)" );
            }
        }
        if ($limit){
            $select->limitPage($page, $limit);
        }
        $select->group("ld.id");
        $select->order("ld.from_date desc");
        $select->order("st.code asc");
            if ($_SERVER['REMOTE_ADDR'] == '112.109.92.6') {
//                echo "<pre>";
//                print_r($select->__toString());
//                echo "</pre>";
            }
        $data['data'] = $db->fetchAll($select);
        $data['total'] = $db->fetchOne("SELECT FOUND_ROWS()");
        return $data;

    }

    public function _selectAdminLimit($limit = 10, $page = 1, $params = array())
    {
        $db = $this->dbconnect();

        $select = $db->select()
            ->from(array("ld" => "leave_detail"),array(
                    new Zend_Db_Expr('SQL_CALC_FOUND_ROWS ld.id, ld.*
                                    , st.code as code 
                                    , t1.name as `department_name`
                                    , t2.name as `team_name`
                                    , ld.total as `total`
                                    , SUM(IF(ad2.id IS NOT NULL, 1, 0 )) as `day_leave`
                                    , IF( (ld.image = "" OR ld.image = "[]") AND lt.need_images > 0, 1, 0 ) as `sort_image`'
                )))
            ->join(array("st" => "staff"), "st.id = ld.staff_id", array("staff_name" => "concat(st.firstname,' ',st.lastname)"))
            ->join(array("lt" => "leave_type"), "lt.id = ld.leave_type", array("leave_type_note" => "lt.note"))
            ->join(array("lt2" => "leave_type"), "lt.parent = lt2.id", array("leave_group" => "lt2.note"))
            ->joinLeft(array("cgm" => "company_group_map"), "cgm.title = st.title", array())
            ->join(array("t1" => "team"), "t1.id = st.department", array())
            ->join(array("t2" => "team"), "t2.id = st.team", array())
            ->join(array("t3" => "team"), "t3.id = st.title", array("title_name" => "t3.name"))
            ->join(array("rm" => "regional_market"), "st.regional_market = rm.id" ,array())
            ->join(array("ar" => "area"), "rm.area_id = ar.id" ,array('area_name' => 'ar.name'))
            ->joinLeft(array("ti" => "time"), "ld.to_date < ti.created_at AND ti.staff_id = ld.staff_id", array())
            ->joinLeft(array("ad" => "all_date"), "(ad.date BETWEEN ld.from_date AND ld.to_date) AND YEAR(ad.date) = YEAR(CURRENT_DATE())", array())
            ->joinLeft(array("ad2" => "all_date"), "ad2.date < NOW() AND (ad2.date BETWEEN ld.from_date AND ld.to_date) AND YEAR(ad2.date) = YEAR(CURRENT_DATE())", array())
            ->joinLeft(array("ada" => "all_date_advanced"), "ada.date = ad.date AND ada.type_id = cgm.company_group AND ada.type = 4", array());

        // $select->where("ada.id IS NULL");
        if(isset($params['staff_id']))
        {
            $select->where("ld.staff_id = ?", $params['staff_id']);
        }

        if(isset($params['name']) && !empty($params['name']))
        {
            $select->where("concat(st.firstname, ' ', st.lastname) like ?", '%' . $params['name'] . '%');
        }

        if(isset($params['code']) && !empty($params['code']))
        {
            $select->where("st.code = ?", $params['code']);
        }

        $select->where("ld.hr_approved = 1");

        if(isset($params['parent_leave_type']) && !empty($params['parent_leave_type']))
        {
            $select->where("lt2.id = ?", $params['parent_leave_type'] );
        }

        if(isset($params['id']) && $params['id'] != '')
        {
            $select->where("ld.id = ?", $params['id']);
        }

        $select->where("DATEDIFF(CURDATE(), ld.to_date) > 3");
        $select->where("ti.id IS NULL");
        $select->where("st.status != 0");


        $auth = Zend_Auth::getInstance()->getStorage()->read();
        // if($auth->group_id == HR_ID)
        // {
        //     $select->where('ld.status = 1');
        // }

        if(!in_array($auth->group_id, array(HR_ID, 1)))
        {
            if($auth->is_officer == 1)
            {
                $QCheckin = new Application_Model_CheckIn();
                $list_info = $QCheckin->getListCodePermission($auth->code);

                $where_permission_array = array();
                foreach($list_info as $key => $val)
                {
                    $where_team = !empty($val['team_id'])?(" AND st.team = " . $val['team_id']):"";
                    $where_permission_array[] = "(st.office_id = " . $val['office_id'] 
                                            . " AND st.department = " . $val['department_id']
                                            . $where_team
                                            . " )";
                }

                $select->where(implode(" OR ", $where_permission_array));

                // if(!empty($list_code))
                // {
                //     $select->where("st.code IN (" . $list_code . ")");
                // }
                // else
                // {
                //     $select->where("1 = -1");
                // }
            }
            else
            {
                if(in_array($auth->title, array(SALES_TITLE, PB_SALES_TITLE)))
                {
                    $select->where("st.title = ?", PGPB_TITLE);

                    $QStoreStaffLog = new Application_Model_StoreStaffLog();
                    
                    $where_store_staff_log = array();
                    $where_store_staff_log[] = $this->getAdapter()->quoteInto('staff_id = ?', $auth->id);
                    $where_store_staff_log[] = $this->getAdapter()->quoteInto('released_at is NULL');
                    
                    $array_store = array();
                    foreach($QStoreStaffLog->fetchAll($where_store_staff_log) as $value)
                    {
                        $array_store[] = $value['store_id'];
                    }
                    $store_id = implode(",", $array_store);
                    $select->join(array("stl" => "store_staff_log"), "stl.staff_id = ld.staff_id and stl.store_id in (" . $store_id .")", array());
                    $select->where('st.title = '. CHUYEN_VIEN_BAN_HANG_TITLE . ' or st.title = ' . PGPB_TITLE . ' or st.title = ' . PB_SALES_TITLE);
                }

                if($auth->title == SALES_ADMIN_TITLE)
                {
                    $QRegionalMarket = new Application_Model_RegionalMarket();
                    $where_market = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $auth->regional_market_id);
                    $regional_markets = $QRegionalMarket->fetchAll($where_market);

                    $array_regional_markets = array();
                    foreach($regional_markets as $value)
                    {
                        $array_regional_markets[] = $value['id'];
                    }
                    $regional_markets_id = implode(",", $array_regional_markets);

                    $select->where('st.regional_market IN ('. $regional_markets_id . ')');
                    $select->where('st.title = '. SALES_TITLE . ' or st.title = ' . PGPB_TITLE . ' or st.title = ' . PB_SALES_TITLE);
                }

                if($auth->title == LEADER_TITLE) // phan quyen cho leader
                {
                    // $QStoreLeaderLog = new Application_Model_StoreLeaderLog();
                    
                    // $where_store_leader_log = array();
                    // $where_store_leader_log[] = $this->getAdapter()->quoteInto('staff_id = ?', $auth->id);
                    // $where_store_leader_log[] = $this->getAdapter()->quoteInto('released_at is NULL');
                    
                    // $array_store = array();
                    // foreach($QStoreLeaderLog->fetchAll($where_store_leader_log) as $value)
                    // {
                    //     $array_store[] = $value['store_id'];
                    // }
                    // $store_id = implode(",", $array_store);
                    // $select->join(array("stl" => "store_staff_log"), "stl.staff_id = st.id and stl.store_id in (" . $store_id .") ", array());
                    // $select->where('st.title = '. SALES_TITLE);
                    $select->where('st.id in (?)', $this->getStaffForTiming($auth->id));
                    
                }

                if(in_array($auth->group_id, array(ASM_ID, 16)))
                {
                    $QAsm = new Application_Model_Asm();
                    $list_regions = $QAsm->get_cache($auth->id);
                    if ($list_regions['province'] && is_array($list_regions['province']) && count($list_regions['province']) > 0){
                        // $select->where('t.regional_market IN (?)', $list_regions['province']); // l?c staff thu?c regional_market tr�n
                        $select->where('st.regional_market IN (?)', $list_regions['province']);
                    }
                    else
                        $select->where('1=0', 1);
                    $select->where('st.team in (75,119,294)', '');
                }
            }
        }

        if ($limit){
            $select->limitPage($page, $limit);
        }

        $select->group("ld.id");
        $select->order("ld.status desc");

        if(!empty($_GET['dev']))
        {   
            echo '<pre>';
            echo $select->__toString(); die;
        }
        

        $data['data'] = $db->fetchAll($select);
        $data['total'] = $db->fetchOne("SELECT FOUND_ROWS()");
        return $data;

    }
    
    
     public function _selectAdminById($limit = 10, $page = 1, $params = array())
    {
        $db = $this->dbconnect();

        $select = $db->select()
            ->from(array("ld" => "leave_detail"),array(
                    new Zend_Db_Expr('SQL_CALC_FOUND_ROWS ld.id, ld.*
                                    , st.code as code 
                                    , t1.name as `department_name`
                                    , t2.name as `team_name`
                                    , ld.total as `total`
                                    , IF( (ld.image = "" OR ld.image = "[]") AND lt.need_images > 0, 1, 0 ) as `sort_image`'
                )))
            ->join(array("st" => "staff"), "st.id = ld.staff_id", array("staff_name" => "concat(st.firstname,' ',st.lastname)"))
            ->join(array("lt" => "leave_type"), "lt.id = ld.leave_type", array("leave_type_note" => "lt.note"))
            ->join(array("lt2" => "leave_type"), "lt.parent = lt2.id", array("leave_group" => "lt2.note"))
            ->joinLeft(array("cgm" => "company_group_map"), "cgm.title = st.title", array())
            ->join(array("t1" => "team"), "t1.id = st.department", array())
            ->join(array("t2" => "team"), "t2.id = st.team", array())
            ->joinLeft(array("ad" => "all_date"), "(ad.date BETWEEN ld.from_date AND ld.to_date) AND YEAR(ad.date) = YEAR(CURRENT_DATE())", array())
            ->joinLeft(array("ada" => "all_date_advanced"), "ada.date = ad.date AND ada.type_id = cgm.company_group AND ada.type = 4", array());

        // $select->where("ada.id IS NULL");
        if(isset($params['staff_id']))
        {
            $select->where("ld.staff_id = ?", $params['staff_id']);
        }

        if(isset($params['name']) && !empty($params['name']))
        {
            $select->where("concat(st.firstname, ' ', st.lastname) like ?", '%' . $params['name'] . '%');
        }

        if(isset($params['code']) && !empty($params['code']))
        {
            $select->where("st.code = ?", $params['code']);
        }

        if(isset($params['status']) && $params['status'] != '')
        {
            $select->where("ld.status = ?", $params['status']);
        }

        if(isset($params['id']) && $params['id'] != '')
        {
            $select->where("ld.id = ?", $params['id']);
        }
        else
        {
            // $select->where("1 = -1");
        }

        // if(!empty($params['from']) && !empty($params['to']))
        // {
        //     $select->where("ld.from_date >= ?", $params['from']);
        //     $select->where("ld.from_date <= ?", $params['to']);
        // }

        $auth = Zend_Auth::getInstance()->getStorage()->read();
        // if($auth->group_id == HR_ID)
        // {
        //     $select->where('ld.status = 1');
        // }

        if(!in_array($auth->group_id, array(HR_ID, 1)))
        {
            if($auth->is_officer == 1)
            {
                $QCheckin = new Application_Model_CheckIn();
                $list_info = $QCheckin->getListCodePermission($auth->code);

                $where_permission_array = array();
                foreach($list_info as $key => $val)
                {
                    $where_team = !empty($val['team_id'])?(" AND st.team = " . $val['team_id']):"";
                    $where_permission_array[] = "(st.office_id = " . $val['office_id'] 
                                            . " AND st.department = " . $val['department_id']
                                            . $where_team
                                            . " )";
                }

                $select->where(implode(" OR ", $where_permission_array));

                if(!empty($list_code))
                {
                    $select->where("st.code IN (" . $list_code . ")");
                }
                else
                {
                    // $select->where("1 = -1");
                }
            }
            else
            {
                if($auth->title == SALES_TITLE)
                {
                    $select->where("st.title = ?", PGPB_TITLE);

                    $QStoreStaffLog = new Application_Model_StoreStaffLog();
                    
                    $where_store_staff_log = array();
                    $where_store_staff_log[] = $this->getAdapter()->quoteInto('staff_id = ?', $auth->id);
                    $where_store_staff_log[] = $this->getAdapter()->quoteInto('released_at is NULL');
                    
                    $array_store = array();
                    foreach($QStoreStaffLog->fetchAll($where_store_staff_log) as $value)
                    {
                        $array_store[] = $value['store_id'];
                    }
                    $store_id = implode(",", $array_store);
                    $select->join(array("stl" => "store_staff_log"), "stl.staff_id = ld.staff_id and stl.store_id in (" . $store_id .")", array());
                    $select->where('st.title = '. CHUYEN_VIEN_BAN_HANG_TITLE . ' or st.title = ' . PGPB_TITLE . ' or st.title = ' . PB_SALES_TITLE);
                }

                if($auth->title == SALES_ADMIN_TITLE)
                {
                    $QRegionalMarket = new Application_Model_RegionalMarket();
                    $where_market = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $auth->regional_market_id);
                    $regional_markets = $QRegionalMarket->fetchAll($where_market);

                    $array_regional_markets = array();
                    foreach($regional_markets as $value)
                    {
                        $array_regional_markets[] = $value['id'];
                    }
                    $regional_markets_id = implode(",", $array_regional_markets);

                    $select->where('st.regional_market IN ('. $regional_markets_id . ')');
                    $select->where('st.title = '. SALES_TITLE . ' or st.title = ' . PGPB_TITLE . ' or st.title = ' . PB_SALES_TITLE);
                }

                if($auth->title == LEADER_TITLE) // phan quyen cho leader
                {
                    $QStoreLeaderLog = new Application_Model_StoreLeaderLog();
                    
                    $where_store_leader_log = array();
                    $where_store_leader_log[] = $this->getAdapter()->quoteInto('staff_id = ?', $auth->id);
                    $where_store_leader_log[] = $this->getAdapter()->quoteInto('released_at is NULL');
                    
                    $array_store = array();
                    foreach($QStoreLeaderLog->fetchAll($where_store_leader_log) as $value)
                    {
                        $array_store[] = $value['store_id'];
                    }
                    $store_id = implode(",", $array_store);
                    $select->join(array("stl" => "store_staff_log"), "stl.staff_id = st.id and stl.store_id in (" . $store_id .") ", array());
                    $select->where('st.title = '. SALES_TITLE);
                    
                }

                if(in_array($auth->group_id, array(ASM_ID, 16)))
                {
                    $QRegionalMarket = new Application_Model_RegionalMarket();
                    $where_market = $QRegionalMarket->getAdapter()->quoteInto('area_id = ?', $auth->regional_market_id);
                    $regional_markets = $QRegionalMarket->fetchAll($where_market);

                    $array_regional_markets = array();
                    foreach($regional_markets as $value)
                    {
                        $array_regional_markets[] = $value['id'];
                    }
                    $regional_markets_id = implode(",", $array_regional_markets);

                    $select->where('st.regional_market IN ('. $regional_markets_id . ')');
                    $select->where('st.team = '. SALES_TEAM);
                }
            }
        }

        if ($limit){
            $select->limitPage($page, $limit);
        }

        $select->group("ld.id");
        $select->order("ld.status desc");

        if(!empty($_GET['dev']))
        {   
            echo '<pre>';
            echo $select->__toString(); die;
        }
        

        $data['data'] = $db->fetchAll($select);
        $data['total'] = $db->fetchOne("SELECT FOUND_ROWS()");
        return $data;

    }

    public function _selectHR($limit = 10, $page = 1, $params = array())
    {
        $db = $this->dbconnect();

        $select = $db->select()
            ->from(array("ld" => "leave_detail"),array(
                    new Zend_Db_Expr('SQL_CALC_FOUND_ROWS ld.id, ld.*
                                    , st.code as code 
                                    , ld.total as `total`
                                    , IF( (ld.image = "" OR ld.image = "[]") AND lt.need_images > 0, 1, 0 ) as `sort_image`'
                )))
            ->join(array("st" => "staff"), "st.id = ld.staff_id", array("name" => "concat(st.firstname,' ',st.lastname)"))
            ->join(array("lt" => "leave_type"), "lt.id = ld.leave_type", array("leave_type_name" => "lt.note"))
            ->join(array("lt2" => "leave_type"), "lt.parent = lt2.id", array("leave_group" => "lt2.note"))
            ->join(array("cgm" => "company_group_map"), "cgm.title = st.title", array())
            ->joinLeft(array("ad" => "all_date"), "(ad.date BETWEEN ld.from_date AND ld.to_date) AND YEAR(ad.date) = YEAR(CURRENT_DATE())", array())
            ->joinLeft(array("ada" => "all_date_advanced"), "ada.date = ad.date AND ada.type_id = cgm.company_group AND ada.type = 4", array());

        // $select->where("ada.id IS NULL");
        if(isset($params['staff_id']))
        {
            $select->where("ld.staff_id = ?", $params['staff_id']);
        }

        if(isset($params['name']) && !empty($params['name']))
        {
            $select->where("concat(st.firstname, ' ', st.lastname) like ?", '%' . $params['name'] . '%');
        }

        if(isset($params['email']) && !empty($params['email']))
        {
            $select->where("st.email = ?", $params['email'] . '@oppomobile.vn');
        }

        if(isset($params['status']) && $params['status'] != '')
        {
            $select->where("ld.status = ?", $params['status']);
        }

        if(!empty($params['from']) && !empty($params['to']))
        {
            $select->where("ld.from_date >= ?", $params['from']);
            $select->where("ld.from_date <= ?", $params['to']);
        }

        if(!empty($params['department']))
        {
            $select->where("st.department = ?", $params['department']);
        }

        if(!empty($params['team']))
        {
            $select->where("st.team = ?", $params['team']);
        }

        if(!empty($params['title']))
        {
            $select->where("st.title = ?", $params['title']);
        }

        $auth = Zend_Auth::getInstance()->getStorage()->read();
        // if($auth->group_id == HR_ID)
        {
            $select->where('ld.status = 1');
        }

        // echo '<pre>';
        // echo $select->__toString(); die;

        // if ($limit){
        //     $select->limitPage($page, $limit);
        // }

        $select->group("ld.id");
        $select->order("ld.hr_approved desc");

        $data['data'] = $db->fetchAll($select);
        $data['total'] = $db->fetchOne("SELECT FOUND_ROWS()");
        return $data;

    }

    public function updateTotal($date_update = '')
    {
        if(!isset($date_update) || empty($date_update))
        {
            return false;
        }
        $db = $this->dbconnect();

        $sql = "UPDATE leave_detail ld
                SET 
                ld.total = (SELECT count(ad.id) 
                            FROM all_date ad 
                            WHERE ad.`date` >= ld.from_date  AND ad.`date` <= ld.to_date AND ad.is_off = 0)
                WHERE ld.from_date <= :date_update AND ld.to_date >= :date_update";
        $stmt = $db->prepare($sql);

        $stmt->bindParam("date_update", $date_update, PDO::PARAM_STR);

        $res = $stmt->execute();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $res;
    }

    public function getStaffForTiming($staff_id){

        if(!$staff_id){
            return false;
        }

        $db  = Zend_Registry::get('db');
        $cols = array('b.staff_id');
        $str = "( b.released_at IS NULL OR 
                    ( FROM_UNIXTIME(b.released_at,'%Y-%m-%d') > FROM_UNIXTIME(a.released_at,'%Y-%m-%d') 
                        AND FROM_UNIXTIME(b.released_at,'%Y-%m-%d') < IFNULL(FROM_UNIXTIME(a.released_at,'%Y-%m-%d'),'9999-01-01') 
                    ) 
                )";
        $select = $db->select()
            ->from(array('a'=>'store_leader_log'),$cols)
            ->join(array('b'=>'store_staff_log'),'a.store_id = b.store_id',array())
            ->where('a.staff_id = ?',$staff_id)
            ->where('a.released_at IS NULL')
            ->where($str,1)
            // ->where('b.is_leader = 0 OR b.staff_id = ?',$staff_id)
        ;

        $list = $db->fetchAll($select);
       
        $result = array();
        $result[] = $staff_id;
        if($list){
            foreach ($list as $item) {
                if(!isset($result['staff_id'])){
                    $result[] = $item['staff_id'];
                }
            }
        }

        return $result;
    }
	
    public function insertLeave2($params = array()) {
        $db = Zend_Registry::get('db');
        //	$db->beginTransaction ();
        try {

            $check_leav_time = 0;
            $staff_id = $params ['staff_id'];
            $from = $params ['from'];
            $to = $params ['to'];
            $leave_type = $params ['leave_type'];
            $reason = $params ['reason'];
            $is_officer = $params ['is_officer'];

            $sql_check_leav_time = "SELECT
	           COUNT(ld.id) AS `check_leav_time`
            FROM
            	`leave_detail` AS `ld`
            WHERE
            ((
            	(ld.from_date <= '$from')
            AND (ld.to_date >= '$from')
            OR (
            	ld.from_date <= '$to'
            	AND ld.to_date >= '$to'
            )
            OR (
            	ld.from_date >= '$from'
            	AND ld.to_date <= '$to'
            )
            )
            )
            AND (ld.staff_id = $staff_id)
            AND  (ld.hr_approved <> 2 AND ld.status <> 2)";
            $stmt = $db->prepare($sql_check_leav_time);
            $stmt->execute();
            $result = $stmt->fetchAll();

            $stmt->closeCursor();
            $stmt = null;
            if ($result [0] ['check_leav_time'] > 0) {
                // Thời gian nghỉ phép không đúng
                return array(
                    'message' => 'Thời gian nghỉ phép không đúng',
                    'status' => 0
                );
            }

            $select_total = $db->select()->from(array('ad' => 'all_date'), array('total' => 'COUNT(ad.id)'))
                    ->join(array('st' => 'staff'), "st.id = $staff_id")
                    ->join(array('t' => 'team'), 't.id = st.title')
                    ->joinLeft(array('ada' => 'all_date_advanced'), 'ada.date = ad.date AND ada.type_id = t.policy_group AND ada.type = 4')
                    ->where("ad.date BETWEEN '$from' AND '$to'")
                    ->where('ada.id IS NULL');
            $result_total = $db->fetchAll($select_total);
            $total = $result_total[0]['total'];

            $stmt = $db->prepare("SELECT PR_get_stock(:staff_id) as total_stock ");
            $stmt->bindParam("staff_id", $params ['staff_id'], PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetch();

            $number_stock = $data ['total_stock'];

            $select_max = $db->select()->from(array('lt' => 'leave_type'), array('max_day_per_year','max_day_per_time','sub_stock','need_insurance','sub_time'))
                    ->where('lt.id = ?', $params ['leave_type']);
            $data_max = $db->fetchAll($select_max);

            $year_leave = 0;

            $sub_stock = $data_max [0] ['sub_stock'];
            $max_day_per_time = $data_max [0] ['max_day_per_time'];
            $max_day_per_year = $data_max [0] ['max_day_per_year'];
            if ($sub_stock >= 1) {
                if ($number_stock <= $total) {
                    $year_leave = $number_stock;
                } else {
                    $year_leave = $total;
                }
            }

            if ($max_day_per_time > 0) {
                if ($total > $max_day_per_time) {
                    return array(
                        'message' => 'Số phép đăng ký vượt quá cho phép',
                        'status' => 0
                    );
                }
            }

            // $year = YEAR($from);
            if ($max_day_per_year > 0) {
                $select_stock_year = $db->select()->from(array('ld' => 'leave_detail'), array('total_stock_year' => 'COUNT(*)'))
                        ->join(array('ldd' => 'leave_detail_day'), "ld.id = ldd.leave_detail_id")
                        ->join(array('st' => 'staff'), "st.id = ld.staff_id")
                        ->join(array('t' => 'team'), 't.id = st.title')
                        ->joinLeft(array('ada' => 'all_date_advanced'), 'ada.date = ldd.date AND ada.type_id = t.policy_group AND ada.type = 4')
                        ->where('st.id = ?', $staff_id)
                        ->where('ld.leave_type = ?', $leave_type)
                        ->where('ld.hr_approved = 1')
                        ->where('ldd.is_special_day = 0')
                        ->where('YEAR(ldd.date) = ?', 2017)
                        ->where('ada.id IS NULL');
                $stock_year = $db->fetchAll($select_stock_year);
                $stock_year = $stock_year [0] ['total_stock_year'];
                if ($total + $stock_year > $max_day_per_year) {
                    
                }
            }

            $date = date('Y-m-d H:i:s');
            $arr_leave_detail = array(
                'staff_id' => $staff_id,
                'from_date' => $from,
                'to_date' => $to,
                'total' => $total,
                'reason' => $reason,
                'is_office' => $is_officer,
                'status' => 0,
                'created_at' => $date,
                'leave_type' => $leave_type,
            );
            if (!empty($params['image'])) {
                $arr_leave_detail['image'] = $params['image'];
            }

            if (!empty($params['due_date'])) {
                $arr_leave_detail['due_date'] = $params['due_date'];
            }

            if (!empty($params['status']) && isset($params['status'])) {//trang edit
                $arr_leave_detail['status'] = $params['status'];
            }

            if (!empty($params['created_by']) && isset($params['created_by'])) {//trang edit
                $arr_leave_detail['created_by'] = $params['created_by'];
            }

            $QLeaveDetail = new Application_Model_LeaveDetail ();
            $id_leave_detail = $QLeaveDetail->insert($arr_leave_detail);

            $sql = "INSERT INTO `leave_detail_day` (`leave_detail_id`, `date`, `is_special_day`, `is_year_leave`)
    		SELECT
    		$id_leave_detail AS `leave_detail_id`,
    		ad.date AS `date`,
    		IF(ada.id IS NULL, 0, 1) AS `is_special_day`,
    		0 AS `is_year_leave`
    		FROM `all_date` ad
    		JOIN `staff` st
    		ON st.id = $staff_id
    		JOIN `team` t
    		ON t.id = st.title
    		LEFT JOIN `all_date_advanced` ada
    		ON ada.date = ad.date
    		AND ada.type_id = t.policy_group
    		AND ada.type = 4
    		WHERE
    		(ad.date BETWEEN '$from' AND '$to')";
            $stmt = $db->query($sql);
            return array(
                'message' => 'Success',
                'status' => 1
            );

            // commit
            //	$db->commit ();
        } catch (exception $e) {
            //	$db->rollback ();
            echo "<pre>";
            var_dump($e->getMessage());
            die;
        }
    }
    
    public function insertLeaveHalf2($params = array()) {
        $db = Zend_Registry::get('db');
        //	$db->beginTransaction ();
        try {

            $check_leav_time = 0;
            $staff_id = $params ['staff_id'];
            $date = $params ['date'];
            $leave_type = $params ['leave_type'];
            $reason = $params ['reason'];
            $is_officer = $params ['is_officer'];

            $select = $db->select()->from(array('ld' => 'leave_detail'), array('check_leav_time' => 'COUNT(ld.id)'))
                    ->where('ld.from_date <= ?', $date)
                    ->where('ld.to_date >= ?', $date)
                    ->where('ld.hr_approved <> 2')
                    ->where('ld.status <> 2')
                    ->where('ld.staff_id = ?', $params ['staff_id']);
            $result = $db->fetchAll($select);

            if ($result [0] ['check_leav_time'] > 0) {
                return array(
                    'message' => 'Thời gian nghỉ phép không dúng ',
                    'status' => 0
                );
            }

            $select_total = $db->select()->from(array('ad' => 'all_date'), array('total' => 'COUNT(ad.id)'))
                        ->join(array('st' => 'staff'), "st.id = $staff_id")
                        ->join(array('t' => 'team'), 't.id = st.title')
                        ->joinLeft(array('ada' => 'all_date_advanced'), 'ada.date = ad.date AND ada.type_id = t.policy_group AND ada.type = 4')
                        ->where("ad.date = '$date'")
                        ->where('ada.id IS NULL');
            $result_total = $db->fetchAll($select_total);
            if ($result_total [0] ['total'] > 0) {
                $total = 0.5;
            }

            $stmt = $db->prepare("SELECT PR_get_stock(:staff_id) as total_stock ");
            $stmt->bindParam("staff_id", $params ['staff_id'], PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetch();
            $number_stock = $data ['total_stock'];

            $select_max = $db->select()->from(array('lt' => 'leave_type'), array('max_day_per_year','max_day_per_time','sub_stock','need_insurance','sub_time'))
                    ->where('lt.id = ?', $params ['leave_type']);
            $data_max = $db->fetchAll($select_max);

            $year_leave = 0;
            $sub_stock = $data_max [0] ['sub_stock'];
            $max_day_per_time = $data_max [0] ['max_day_per_time'];
            $max_day_per_year = $data_max [0] ['max_day_per_year'];

            if ($sub_stock >= 1) {
                if ($number_stock <= $total) {
                    $year_leave = $number_stock;
                } else {
                    $year_leave = $total;
                }
            }

            if ($max_day_per_time > 0) {
                if ($total > $max_day_per_time) {
                    return array(
                        'message' => 'Số ngày dang ký vuợt quá cho phép trong một lần',
                        'status' => 0
                    );
                }
            }

            if ($max_day_per_year > 0) {
                $select_stock_year = $db->select()->from(array('ld' => 'leave_detail'), array('total_stock_year' => 'COUNT(*)'))
                        ->join(array('ldd' => 'leave_detail_day'), "ld.id = ldd.leave_detail_id")
                        ->join(array('st' => 'staff'), "st.id = ld.staff_id")
                        ->join(array('t' => 'team'), 't.id = st.title')
                        ->joinLeft(array('ada' => 'all_date_advanced'), 'ada.date = ldd.date AND ada.type_id = t.policy_group AND ada.type = 4')
                        ->where('st.id = ?', $staff_id)
                        ->where('ld.leave_type = ?', $leave_type)
                        ->where('ld.hr_approved = 1')
                        ->where('ldd.is_special_day = 0')
                        ->where("YEAR(ldd.date) = YEAR($date)")
                        ->where('ada.id IS NULL');

                $stock_year = $db->fetchAll($select_stock_year);
                $stock_year = $stock_year [0] ['total_stock_year'];
                if ($total + $stock_year > $max_day_per_year) {
                    return array(
                        'message' => 'Số phép đăng ký vượt quá số ngày trong một năm',
                        'status' => 0
                    );
                }
            }

            $created = date('Y-m-d H:i:s');
            $arr_leave_detail = array(
                'staff_id' => $staff_id,
                'from_date' => $date,
                'to_date' => $date,
                'total' => $total,
                'reason' => $reason,
                'is_office' => $is_officer,
                'status' => 0,
                'created_at' => $created,
                'leave_type' => $leave_type,
                'is_half_day' => 1
            );
            if (!empty($params['image'])) {
                $arr_leave_detail['image'] = $params['image'];
            }

            if (!empty($params['due_date'])) {
                $arr_leave_detail['due_date'] = $params['due_date'];
            }

            if (!empty($params['status']) && isset($params['status'])) {//trang edit
                $arr_leave_detail['status'] = $params['status'];
            }

            if (!empty($params['created_by']) && isset($params['created_by'])) {//trang edit
                $arr_leave_detail['created_by'] = $params['created_by'];
            }

            $QLeaveDetail = new Application_Model_LeaveDetail ();
            $id_leave_detail = $QLeaveDetail->insert($arr_leave_detail);

            $sql = "INSERT INTO `leave_detail_day` (`leave_detail_id`, `date`, `is_special_day`, `is_year_leave`)
    		SELECT
    		$id_leave_detail AS `leave_detail_id`,
    		ad.date AS `date`,
    		IF(ada.id IS NULL, 0, 1) AS `is_special_day`,
    		0 AS `is_year_leave`
    		FROM `all_date` ad
    		JOIN `staff` st
    		ON st.id = $staff_id
    		JOIN `team` t
    		ON t.id = st.title
    		LEFT JOIN `all_date_advanced` ada
    		ON ada.date = ad.date
    		AND ada.type_id = t.policy_group
    		AND ada.type = 4
    		WHERE
    		(ad.date = '$date' )";
            $stmt = $db->query($sql);
            return array(
                'message' => 'Success',
                'status' => 1
            );

            // commit
            // $db->commit ();
        } catch (exception $e) {
            // $db->rollback ();
        }
    }
	
	    public function getListLeave($params) {

        $db = $this->dbconnect();
        
        $select = $db->select()
        ->from(array("ld" => "leave_detail"),array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS ld.id, ld.*
                                    , st.`code` as `code`
                                    , t1.`name` as `department_name`
                                    , t2.`name` as `team_name`
                                    , ld.total as `total`
                                    ,SUM(
                                	CASE WHEN e.date IS NOT NULL THEN 0 
                                	WHEN IFNULL(f.time_number, 0) + IFNULL(h.office_time,0) + CASE WHEN g.check_in_day IS NOT NULL AND g.status = 1 THEN IF(g.is_half_day = 1, 0.5, 1) ELSE 0 END >= 1 THEN 0
                                	WHEN IFNULL(f.time_number, 0) + IFNULL(h.office_time,0) + CASE WHEN g.check_in_day IS NOT NULL AND g.status = 1 THEN IF(g.is_half_day = 1, 0.5, 1) ELSE 0 END  + IF(IFNULL(ld.is_half_day,0) <> 0, 0.5, 1) > 1 THEN (IFNULL(f.time_number, 0) + IFNULL(h.office_time,0) + CASE WHEN g.check_in_day IS NOT NULL AND g.status = 1 THEN IF(g.is_half_day = 1, 0.5, 1) ELSE 0 END  + IF(IFNULL(ld.is_half_day,0) <> 0, 0.5, 1)) - 1
                                	ELSE IF(IFNULL(ld.is_half_day,0) <> 0, 0.5, 1) END) AS Tong    
                                    , SUM(IF(ad2.id IS NOT NULL, 1, 0 )) as `day_leave`
                                    , IF( (ld.image = "" OR ld.image = "[]") AND lt.need_images > 0, 1, 0 ) as `sort_image`'
                )))
                ->join(array("st" => "staff"), "st.id = ld.staff_id", array("staff_name" => "concat(st.firstname,' ',st.lastname)"))
                ->join(array("lt" => "leave_type"), "lt.id = ld.leave_type", array("leave_type_note" => "lt.note"))
                ->join(array("lt2" => "leave_type"), "lt.parent = lt2.id", array("leave_group" => "lt2.note"))
               // ->joinLeft(array("cgm" => "company_group_map"), "cgm.title = st.title", array())
                ->join(array("t1" => "team"), "t1.id = st.department", array())
                ->join(array("t2" => "team"), "t2.id = st.team", array())
                ->join(array("t3" => "team"), "t3.id = st.title", array("title_name" => "t3.name"))
                ->join(array("rm" => "regional_market"), "st.regional_market = rm.id" ,array())
                ->join(array("ar" => "area"), "rm.area_id = ar.id" ,array('area_name' => 'ar.name'))
			
               // ->joinLeft(array("ad" => "all_date"), "(ad.date BETWEEN ld.from_date AND ld.to_date) AND YEAR(ad.date) = YEAR(CURRENT_DATE())", array())
                ->joinLeft(array("ad2" => "all_date"), "(ad2.date BETWEEN ld.from_date AND ld.to_date) ", array())
                //->joinLeft(array("ldd" => "leave_detail_day"), "ldd.leave_detail_id = ld.id", array())
                ->joinLeft(array("f" => "time_machine"), "ad2.date = f.check_in_day AND ld.staff_id = f.staff_id", array())
                ->joinLeft(array("g" => "time_gps"), "ad2.date = g.check_in_day AND ld.staff_id = g.staff_id", array())
                ->joinLeft(array("h" => "temp_time"), "ad2.date = h.date AND ld.staff_id = h.staff_id", array())
              //  ->joinLeft(array("ada" => "all_date_advanced"), "ada.date = ad.date AND ada.type_id = cgm.company_group AND ada.type = 4", array());
                ->joinLeft(array( 'e' => new Zend_Db_Expr (
            "(SELECT DISTINCT a.`date`, b.title FROM `all_date_advanced` a INNER JOIN company_group_map b ON a.`type_id` = b.company_group)" )
        ), 'st.title = e.title AND ad2.date = e.date', array());
					;
              
                // $select->where("ada.id IS NULL");
                if(isset($params['staff_id']))
                {
                    $select->where("ld.staff_id = ?", $params['staff_id']);
                }
        
                if(isset($params['name']) && !empty($params['name']))
                {
                    $select->where("concat(st.firstname, ' ', st.lastname) like ?", '%' . $params['name'] . '%');
                }
        
                if(isset($params['code']) && !empty($params['code']))
                {
                    $select->where("st.code = ?", $params['code']);
                }
        
                if(isset($params['department']) && !empty($params['department']))
                {
                    $select->where("st.department = ?", $params['department']);
                }
        
                if(isset($params['team']) && !empty($params['team']))
                {
                    $select->where("st.team = ?", $params['team']);
                }
        
                if(isset($params['title']) && !empty($params['title']))
                {
                    $select->where("st.title = ?", $params['title']);
                }
        
                if($params['status'] == '0')
                {
                    $select->where("ld.status = 0");
                }
                elseif($params['status'] == '1')
                {
                    $select->where("ld.status = 1");
                }
        
                if( $params['hr_approved'] == '0')
                {
                    $select->where("ld.hr_approved = 0");
                }
                elseif($params['hr_approved'] == '1')
                {
                    $select->where("ld.hr_approved = 1");
                }
        
        
                if(isset($params['parent_leave_type']) && !empty($params['parent_leave_type']))
                {
                    $select->where("lt2.id = ?", $params['parent_leave_type'] );
                }
        
                if(isset($params['id']) && $params['id'] != '')
                {
                    $select->where("ld.id = ?", $params['id']);
                }
        
                if(!empty($params['from']) && !empty($params['to']))
                {
                    $select->where("ld.from_date >= ?", $params['from']);
                    $select->where("ld.from_date <= ?", $params['to']);
                }
        
                if(!empty($params['from_date']) && !empty($params['to_date']))
                {
                    $select->where("ld.from_date >= ?", $params['from_date']);
                    $select->where("ld.from_date <= ?", $params['to_date']);
                }
                
                
                $select->group("ld.id");
                $select->order("ld.status desc");
       
                if(!empty($params['dev'])){
                    echo '111111';
                    echo "<pre>";
                    print_r($params);
                    echo "</pre>";
                    echo $select->__toString();
                    exit();
                }
                    
                $data['data'] = $db->fetchAll($select);
                $data['total'] = $db->fetchOne("SELECT FOUND_ROWS()");
                return $data;
    }
	public function GetReportLeave($params)
    {
        $from_date_str = $to_date_str = $code_str = $name_str = "";
                
        if(isset($params['from_date']) && !empty($params['from_date']))
        {
            $from_date_str = "AND a.date >= '".$params['from_date']."'";
        }
        
        if(isset($params['to_date']) && !empty($params['to_date']))
        {
            $to_date_str = "AND a.date <= '".$params['to_date']."'";
        }
        
        if(isset($params['code']) && !empty($params['code']))
        {
            $code_str = "AND d.code = ".$params['code'];
        }
        if(isset($params['name']) && !empty($params['name']))
        {
            $name_str = "AND CONCAT(d.firstname, ' ', d.lastname)  LIKE '%".$params['name']."%'";
        }

        
        $db = $this->dbconnect();

        $sql = "SELECT c.from_date, c.to_date, e.note leavetype, d.code, CONCAT(d.firstname, ' ', d.lastname) staffname,
                SUM(IF(IFNULL(c.is_half_day,0) <> 0, 0.5, 1)) phep,
                SUM(CASE WHEN IFNULL(f.time_number, 0) + IFNULL(h.office_time,0) + CASE WHEN g.check_in_day IS NOT NULL AND g.status = 1 THEN IF(g.is_half_day = 1, 0.5, 1) ELSE 0 END > 1 THEN 1 ELSE 
                IFNULL(f.time_number, 0) + IFNULL(h.office_time,0) + CASE WHEN g.check_in_day IS NOT NULL AND g.status = 1 THEN IF(g.is_half_day = 1, 0.5, 1) ELSE 0 END END) cong
                FROM `all_date` a
                INNER JOIN leave_detail c ON a.date BETWEEN c.from_date AND c.to_date
                ".$from_date_str."
                ".$to_date_str."
                INNER JOIN leave_type e ON c.leave_type = e.id
                LEFT JOIN
                (SELECT DISTINCT a.`date`, c.id
                 FROM `all_date_advanced` a INNER JOIN company_group_map b ON 
                a.`type_id` = b.company_group
                INNER JOIN staff c ON b.title = c.title 
                ) b ON a.date = b.date AND c.staff_id = b.id
                INNER JOIN staff d ON c.staff_id = d.id
                LEFT JOIN time_machine f ON a.date = f.check_in_day AND c.staff_id = f.staff_id
                LEFT JOIN time_gps g ON a.date = g.check_in_day AND c.staff_id = g.staff_id
                LEFT JOIN temp_time h ON a.date = h.date AND c.staff_id = h.staff_id
                WHERE b.date IS NULL
                ".$name_str."
                ".$code_str."
                GROUP BY c.from_date, c.to_date, e.note, d.code, CONCAT(d.firstname, ' ', d.lastname)";
         
            $stmt = $db->prepare($sql);
            $stmt->execute();
           
            $Data_report_leave = $stmt->fetchAll();
         $stmt->closeCursor();
            $db = $stmt = null;
        return $Data_report_leave;
    }
	
	public function reportOverTime($list_staff)
    {
        $db = $this->dbconnect();

        $sql = "SELECT d.`name` areaname, e.`name` depname, f.`name` teamname, g.`name` titlename, b.code , CONCAT(b.`firstname`, ' ', b.`lastname`) staffname, MAX(`date`) `date`
                FROM
                (
                SELECT staff_id, MAX(`check_in_day`) `date` FROM time_gps GROUP BY staff_id
                UNION 
                SELECT staff_id, MAX(`check_in_day`) `date` FROM time_machine GROUP BY staff_id
                UNION
                SELECT staff_id, MAX(`date`) `date` FROM temp_time GROUP BY staff_id
                UNION
                SELECT staff_id, MAX(`to_date`) `date` FROM leave_detail GROUP BY staff_id
                UNION
                SELECT `staff_id`,MAX(`tct`.`date`) AS `date`
                FROM `trainer_course_timing` `tct` INNER JOIN `trainer_course_detail` `tcd` 
                ON `tct`.`course_detail_id` = `tcd`.`id` AND staff_id IS NOT NULL
                
                
                GROUP BY staff_id
                    ) a INNER JOIN staff b ON a.staff_id = b.id	and b.`status` = 1
                    LEFT JOIN company_group_map gm on b.title = gm.title
                    INNER JOIN regional_market c ON b.`regional_market` =c.`id`
                    INNER JOIN `area` d ON c.`area_id` = d.`id`
                    LEFT JOIN team e ON b.`department` = e.`id`
                    LEFT JOIN team f ON b.`team` = f.`id`
                    LEFT JOIN team g ON b.`title` = g.`id`
                    WHERE IFNULL(gm.company_group, 0) <> 6
                    AND b.id IN ($list_staff)
                    GROUP BY d.`name`, e.`name`, f.`name`, g.`name`, b.code, CONCAT(b.`firstname`, ' ', b.`lastname`) 
                    HAVING DATEDIFF(NOW(), MAX(`date`)) >= 3
                ";
//        echo "<pre>";
//        print_r($sql);
//        die;
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $data_export_over = $stmt->fetchAll();
            $stmt->closeCursor();
            $db = $stmt = null;
        
        return $data_export_over;
    }
	
	public function getCycleByYear($year){
        if(empty($year)){
            return 0;
        }
        $db = $this->dbconnect();
        $sql= "SELECT lbm.* FROM `leave_life_time` llt
                LEFT JOIN leave_by_month lbm ON lbm.`from_date` BETWEEN llt.from_date AND llt.to_date 
                WHERE llt.`year` = :p_year
                order by lbm.`from_date`";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('p_year',$year , PDO::PARAM_INT);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $db = $stmt = null;
        
        return $data;
        
    }

    public function getByInsurancePassBy(){
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('l' => 'leave_detail'),array('l.staff_id','l.from_date','l.to_date','l.created_at'))
            ->joinLeft(array('lt'=>'leave_type'),'l.leave_type = lt.id',array('leave_type' => 'note'))
            ->joinLeft(array('s'=>'staff'),'s.id = l.staff_id',array('staff_code' => 'code','firstname' => 'firstname','lastname' => 'lastname'))
            ->joinLeft(array('t'=>'team'),'t.id = s.title',array('job_title' => 'name'))
            ->joinLeft(array('e'=>'unit_code'),'(e.company_id = s.company_id AND ((s.`id_place_province` = 64 AND e.`is_foreigner` = 1) OR (s.`id_place_province` <> 64 AND e.`is_foreigner` = 0)))',array('unit_code_name' => 'e.unit_code',))
            ->where('l.insurance_passby = ?', 1)
        ;
        $select->order('l.created_at DESC');
        $select->group ( array ('staff_id') );
        return  $db->fetchAll($select);
    }

    public function checkDupLeaveChangeToDate($leave_id, $staff_id, $from_date_leave, $to_date_new) {
        $db = Zend_Registry::get('db');
        $db = $this->dbconnect();

        $select = $db->select()
        ->from(array("ld" => "leave_detail"), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS ld.id, ld.*')));
        $select->where('ld.id <> ?',$leave_id);
        $select->where('ld.staff_id = ?',$staff_id);
        $select->where('ld.from_date > ?',$from_date_leave);
        $select->where('(? BETWEEN ld.from_date AND ld.to_date) OR (? > ld.to_date)',$to_date_new);
        $select->where('ld.hr_approved <> ?',2);
        $select->where('ld.status <> ?',2);
        $data = $db->fetchAll($select);
        return $data;
    }
	
}