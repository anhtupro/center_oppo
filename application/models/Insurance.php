<?php
class Application_Model_Insurance extends Zend_Db_Table_Abstract
{
    protected $_name = 'pre_status';
    public function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');
        $cols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.id'),
            'staff_id'                                        => 'a.id',
            'a.dob',
            'a.ID_number',
            'staff_code'                                      => 'a.code',
            'staff_name'                                      => 'CONCAT(a.firstname," ",a.lastname)',
            'unit_code_name'                                  => 'e.unit_code',
            'company_name'                                    => 'd.name',
            'regional_market'                                 => 'b.name',
            'department_name'                                 => 'h.name',
            'team_name'                                       => 'g.name',
            'title_name'                                      => 'CONCAT(f.name_vn," - ",l.name," - ",m.name)',
            'a.insurance_number',
            'a.have_book',
            'a.close_book',
            'a.return_ins_card_time',
            'a.return_ins_book_time',
            'a.medical_card_number',
            'a.take_card_from_ins',
            'a.take_card_from_staff',
            'a.delivery_card_ins',
            'increase'                                        => 'CASE WHEN DAY(i.time_alter) > 15 THEN DATE_ADD(i.time_alter, INTERVAL 1 MONTH) ELSE i.time_alter END',
            'decrease'                                        => 'CASE WHEN DAY(j.time_alter) > 15 THEN DATE_ADD(j.time_alter, INTERVAL 1 MONTH) ELSE j.time_alter END',
            'status'                                          => 'CASE WHEN DATE(NOW()) >= k.from_date AND DATE(NOW()) <= k.to_date THEN 2
                                                                        WHEN j.time_alter IS NOT NULL OR j.time_alter != "0000-00-00" THEN 3
                                                                        ELSE 1 END',
            'bh'                                              => new Zend_Db_Expr('CASE WHEN i.staff_id IS NOT NULL THEN 1 ELSE 0 END'),
            'kv'                                              => 'm.name',
            'time_alter_increase'                             => 'i.time_alter',
            'salary'                                          => 'n.salary',
            'note' => 'a.note_ins'
        );
        $select = $db->select()
            ->from(array('a'=>'staff'),$cols)
            ->join(array('b'=>'regional_market'),'b.id = a.regional_market',array())
            ->joinLeft(array('c'=>'insurance_info'),'c.staff_id = a.id',array())
            ->joinLeft(array('d'=>'company'),'d.id = a.company_id',array())
            ->joinLeft(array('e'=>'unit_code'),'e.id = a.unit_code_id',array())
            ->joinLeft(array('f'=>'team'),'f.id = a.title',array())
            ->joinLeft(array('g'=>'team'),'g.id = a.team',array())
            ->joinLeft(array('h'=>'team'),'h.id = a.department',array())
            ->joinLeft(array('i'=>'pre_status'),'a.id = i.staff_id AND i.type = 1 AND i.`option` = 1 AND i.del = 0 AND i.locked = 1',array())
            ->joinLeft(array('j'=>'pre_status'),'a.id = j.staff_id AND j.type = 2 AND j.`option` = 3 AND j.del = 0 AND j.locked = 1',array())
            ->joinLeft(array('k'=>'off_childbearing'),'k.staff_id = a.id',array())
            ->joinLeft(array('l'=>'province'),'b.province_id = l.id',array())
            ->joinLeft(array('m'=>'area_nationality'),'m.id = l.area_national_id',array())
            ->joinLeft(array('n'=>'salary'),'n.staff_id = a.id',array())
            ->joinLeft(array('o'=>new Zend_Db_Expr('
                        (SELECT staff_id, MAX(time_effective) as time_effective
                        FROM salary GROUP BY staff_id)
                    ')
                ),'o.staff_id = n.staff_id AND o.time_effective = n.time_effective',array())
            ->joinLeft(array('p'=> new Zend_Db_Expr('(
                    SELECT MAX(id) id, staff_id FROM pre_status
                    WHERE `option` = 1 AND del = 0 AND `locked` = 1
                    GROUP BY staff_id
                )')),'i.staff_id = p.staff_id AND p.id = i.id',array())
            ->joinLeft(array('q'=> new Zend_Db_Expr('(
                    SELECT MAX(id) id, staff_id FROM pre_status
                    WHERE `option` = 3 AND del = 0 AND `locked` = 1
                    GROUP BY staff_id
                )')),'j.staff_id = q.staff_id AND q.id = j.id',array())
            ->group('a.id')
            ->order(array('bh DESC','status ASC','i.time_alter ASC','j.time_alter DESC','a.id ASC','b.name ASC'))
        ;

        if(isset($params['name']) AND $params['name']){
            $select->where('CONCAT(a.firstname," ",a.lastname) LIKE ?','%'.$params['name'].'%');
        }

        if(isset($params['code']) AND $params['code']){
            $select->where('a.code LIKE ?','%'.$params['code'].'%');
        }

        if(isset($params['insurance_number']) AND $params['insurance_number']){
            $select->where('a.insurance_number LIKE ?','%'.$params['insurance_number'].'%');
        }

        if(isset($params['company_id']) AND $params['company_id']){
            $select->where('a.company_id = ?',$params['company_id']);
        }

        if(isset($params['unit_code_id']) AND $params['unit_code_id']){
            $select->where('a.unit_code_id = ?',$params['unit_code_id']);
        }

        if(isset($params['regional_market']) AND $params['regional_market']){
            $select->where('a.regional_market = ?',$params['regional_market']);
        }

        if(isset($params['have_book']) AND $params['have_book'] AND $params['have_book'] >= 0){
            if($params['have_book'] == 1){
                $select->where('a.have_book IS NOT NULL AND a.have_book != "0000-00-00"');
            }else{
                $select->where('a.have_book IS NULL OR a.have_book = "0000-00-00"');
            }
        }

        if(isset($params['close_book']) AND $params['close_book'] AND $params['close_book'] >= 0){
            if($params['close_book'] == 1){
                $select->where('a.close_book IS NOT NULL AND a.close_book != "0000-00-00"');
            }else{
                $select->where('a.close_book IS NULL  OR a.close_book = "0000-00-00"');
            }
        }

        if(isset($params['return_ins_book_time']) AND $params['return_ins_book_time'] AND $params['have_book'] >= 0){
            if($params['return_ins_book_time'] == 1){
                $select->where('a.return_ins_book_time IS NOT NULL AND a.return_ins_book_time != "0000-00-00"');
            }else{
                $select->where('a.return_ins_book_time IS NULL OR a.return_ins_book_time = "0000-00-00"');
            }
        }

        if(isset($params['return_ins_card_time']) AND $params['return_ins_card_time'] AND $params['have_book'] >= 0){
            if($params['return_ins_card_time'] == 1){
                $select->where('a.return_ins_card_time IS NOT NULL AND a.return_ins_card_time != "0000-00-00"');
            }else{
                $select->where('a.return_ins_card_time IS NULL OR a.return_ins_card_time = "0000-00-00"');
            }
        }

        if(isset($params['take_card_from_ins']) AND $params['take_card_from_ins'] AND $params['have_book'] >= 0){
            if($params['take_card_from_ins'] == 1){
                $select->where('a.take_card_from_ins IS NOT NULL AND a.take_card_from_ins != "0000-00-00"');
            }else{
                $select->where('a.take_card_from_ins IS NOT NULL OR a.take_card_from_ins = "0000-00-00"');
            }
        }

        if(isset($params['take_card_from_staff']) AND $params['take_card_from_staff'] AND $params['have_book'] >= 0){
            if($params['take_card_from_staff'] == 1){
                $select->where('a.take_card_from_staff IS NOT NULL AND a.take_card_from_staff != "0000-00-00"');
            }else{
                $select->where('a.take_card_from_staff IS NOT NULL OR a.take_card_from_staff = "0000-00-00"');
            }
        }

        if(isset($params['delivery_card_ins']) AND $params['delivery_card_ins'] AND $params['have_book'] >= 0){
            if($params['delivery_card_ins']){
                $select->where('a.delivery_card_ins IS NOT NULL AND a.delivery_card_ins != "0000-00-00"');
            }else{
                $select->where('a.delivery_card_ins IS NOT NULL OR a.delivery_card_ins = "0000-00-00"');
            }
        }

        if(isset($params['status']) AND $params['status'] >0){
            $select->having('status = ?',$params['status']);
        }

        if(isset($params['c45']) AND $params['c45'] == 1){
            $select->where('i.id IS NOT NULL');
        }

        $select->where("((n.time_effective IS NOT NULL AND o.time_effective IS NOT NULL) OR n.time_effective IS NULL)");

        if(isset($limit) AND $limit AND !$params['export']){
            $select->limitPage($page,$limit);
        }
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public function fetchPaginationReport($page, $limit, &$total, $params){
        $start = '2015-07-01';
        $from = '';
        $to = '';
        if(isset($params['month']) AND $params['month'] ){
            $month  = My_Date::normal_to_mysql($params['month']);
            $to = date('Y-m-15',strtotime($month));
            $limitDate = date('Y-m-20',strtotime($month));
            $interval = new DateInterval('P1M');
            $date = new DateTime($month);
            $date->sub($interval);
            $from = $date->format('Y-m-16');
        }

        $db = Zend_Registry::get('db');
        $cols = array(
                //new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
                'p.*',
                'staff_name'      => 'CONCAT(s.firstname," ",s.lastname)',
                'unit_code_name'  => 'u.unit_code',
                'company_name'    => 'c.name',
                'option_name'     => 'o.name',
                'staff_code'      => 's.code',
                'team_name'       => 't.name',
                'title_name'      => 'CONCAT(tt.name_vn," - ",pr.name," - ",ai.name)',
                'department_name' => 'd.name',
                'kv'              => 'ai.name',
                'locked_by_name'  => '(CONCAT(st.firstname," ",st.lastname))',
                'current_month'   => new Zend_Db_Expr('1')
            );
        $select = $db->select()
            ->from(array('p'=>'pre_status'),$cols)
            ->join(array('s'=>'staff'),'s.id = p.staff_id',array())
            ->join(array('c'=>'company'),'c.id = p.company_id',array())
            ->joinLeft(array('tt'=>'team'),'tt.id = p.title',array())
            ->joinLeft(array('t'=>'team'),'t.id = tt.parent_id',array())
            ->joinLeft(array('d'=>'team'),'d.id = t.parent_id',array())
            ->joinLeft(array('u'=>'unit_code'),'u.id = p.unit_code_id',array())
            ->joinLeft(array('o'=>'status_option'),'o.id = p.option',array())
            ->joinLeft(array('r'=>'regional_market'),'r.id = s.regional_market',array())
            ->joinLeft(array('pr'=>'province'),'r.province_id = pr.id',array())
            ->joinLeft(array('ai'=>'area_nationality'),'ai.id = pr.area_national_id',array())
            ->joinLeft(array('st'=>'staff'),'st.id = p.locked_by',array())
            //->order(array('type ASC','option ASC','time_alter ASC'))
            ->where('p.del = ?',0)
            ;

        $cols['current_month'] = new Zend_Db_Expr('0');
        $select2 = $db->select()
            ->from(array('p'=>'pre_status'),$cols)
            ->join(array('s'=>'staff'),'s.id = p.staff_id',array())
            ->join(array('c'=>'company'),'c.id = p.company_id',array())
            ->joinLeft(array('tt'=>'team'),'tt.id = p.title',array())
            ->joinLeft(array('t'=>'team'),'t.id = tt.parent_id',array())
            ->joinLeft(array('d'=>'team'),'d.id = t.parent_id',array())
            ->joinLeft(array('u'=>'unit_code'),'u.id = p.unit_code_id',array())
            ->joinLeft(array('o'=>'status_option'),'o.id = p.option',array())
            ->joinLeft(array('r'=>'regional_market'),'r.id = s.regional_market',array())
            ->joinLeft(array('pr'=>'province'),'r.province_id = pr.id',array())
            ->joinLeft(array('ai'=>'area_nationality'),'ai.id = pr.area_national_id',array())
            ->joinLeft(array('st'=>'staff'),'st.id = p.locked_by',array())
            ->where('p.del = ?',0)
            ->where('p.time_alter >= ?',$start)
            ->where('p.time_alter < ?',$from)
            ->where('p.locked IN (0,2)',0)
        ;

        if(isset($params['code']) AND $params['code']){
            $select->where('s.code = ?',$params['code']);
            $select2->where('s.code = ?',$params['code']);
        }  

        if(isset($params['ins_number']) AND $params['ins_number']){
            $select->where('s.insurance_number = ?',$params['ins_number']);
            $select2->where('s.insurance_number = ?',$params['ins_number']);
        }

        if(isset($params['s_qdnv']) AND $params['s_qdnv']){
            $select->where('p.qdnv = ?',$params['s_qdnv']);
            $select2->where('p.qdnv = ?',$params['s_qdnv']);
        }


        if(isset($params['unit_code_id']) AND $params['unit_code_id']){
            $select->where('p.unit_code_id = ?',$params['unit_code_id']);
            $select2->where('p.unit_code_id = ?',$params['unit_code_id']);
        }

        if(isset($params['name']) AND $params['name']){
            $select->where('CONCAT(s.firstname," ",s.lastname) LIKE ?','%'.$params['name'].'%');
            $select2->where('CONCAT(s.firstname," ",s.lastname) LIKE ?','%'.$params['name'].'%');
        }

        if(isset($params['date_ins']) AND $params['date_ins']){
            $select->where('p.time_alter = ?',trim(My_Date::normal_to_mysql($params['date_ins'])));
        }

        if(isset($params['date_approved']) AND $params['date_approved']){
            $select->where('DATE(p.locked_at) = ?',trim(My_Date::normal_to_mysql($params['date_approved'])));
        }

        if(isset($params['type']) AND $params['type']){
            if($params['type'] == 1){
                $select->where('`option` IN (1,2)');
                $select2->where('`option` IN (1,2)');
            }elseif($params['type'] == 2){
                $select->where('`option` IN (3,4,5,6,9,10)');
                $select2->where('`option` IN (3,4,5,6,9,10)');
            }elseif($params['type'] == 3){
                $select->where('`option` IN (7,8)');
                $select2->where('`option` IN (7,8)');
            }
        }

        if(isset($params['locked']) AND $params['locked'] >=0 ){
            $locked = $params['locked'];
            if($locked == 1){
               $select->where('p.locked = ?',$params['locked']);
            }else{
                $select->where('p.locked IN (0,2)',$params['locked']);
            }

            //$select->where('p.locked = ?',$params['locked']);
        }

        if(isset($params['month']) AND $params['month'] ){
            $select->where("p.time_alter BETWEEN '$from' AND '$to'");
            // $select->where('p.time_created_at IS NULL OR DATE(p.time_created_at) <= ?',$limitDate);
        }

        $selectUnion = $db->select()
                ->union(array($select,$select2));
        $colsAll = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.*'),
        );

        $selectAll = $db->select()
                    ->from(array('a'=>new Zend_Db_Expr("($selectUnion)")),$colsAll)
                    ->order(array('current_month ASC','type ASC','option ASC','time_alter ASC','qdnv'))
        ;

        if($limit){
            $selectAll->limitPage($page,$limit);
        }

        if(isset($params['before']) AND $params['before'] == 1 AND isset($params['month']) AND $params['month']){

        }else{
            $selectAll->where('current_month = ?',1);
        }
// echo $selectAll->__toString();
        //$result = $db->fetchAll($select);
        $result = $db->fetchAll($selectAll);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public function getDateIns($month){
        $result = array();
        if($month){
            $tmp    = explode('/', $month);
            $month  = $tmp[1].'-'.$tmp[0].'-01';
            $db     = Zend_Registry::get('db');
            $stmt   = $db->query('CALL getDate(?)',$month);
            $list   = $stmt->fetchAll();
            if(count($list) > 0){
                foreach($list as $key => $value):
                    $result[$value['time_ins']] = $value['time_ins'];
                endforeach;
            }
        }
        return $result;
    }

    public function getDateInsApproved($month){
        $result = array();
        if($month){
            $month  = My_Date::normal_to_mysql($month);
            $date = new DateTime($month);
            $interval = new DateInterval('P1M');
            $date->sub($interval);
            $from = $date->format('Y-m-15');
            $to = date('Y-m-15',strtotime($month));

            $db     = Zend_Registry::get('db');
            $select = $db->select()
                ->from(array('a'=>'pre_status'),array('locked_at'=>'DATE_FORMAT(DATE(locked_at),"%d/%m/%Y")'))
                ->where('time_ins >= ?',$from)
                ->where('time_ins <= ?',$to )
                ->where('del = ?',0)
                ->where('locked_at IS NOT NULL')
                ->where('locked = ?',1)
                ->group('locked_at')
                ->order('locked_at ASC')

            ;
            $list = $db->fetchAll($select);
            if(count($list) > 0){
                foreach($list as $key => $value):
                    $result[$value['locked_at']] = $value['locked_at'];
                endforeach;
            }
        }
        return $result;
    }

    public function showHistory($staff_id){
        $db = Zend_Registry::get('db');
        $cols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
            'p.*',
            'staff_name'      => 'CONCAT(s.firstname," ",s.lastname)',
            'unit_code_name'  => 'u.unit_code',
            'company_name'    => 'c.name',
            'option_name'     => 'o.name',
            'staff_code'      => 's.code',
            'team_name'       => 't.name',
            'title_name'      => 'CONCAT(tt.name_vn," - ",pr.name," - ",ai.name)',
            'department_name' => 'd.name',
            'kv'              => 'ai.name',
            'type_name'       => '(CASE WHEN p.type = 1 THEN "TĂNG" WHEN p.type = 2 THEN "GIẢM" ELSE "ĐIỀU CHỈNH" END )',
            'month'           => '(CASE WHEN DAY(p.time_alter) <= 15 THEN p.time_alter ELSE DATE_ADD(DATE_FORMAT(p.time_alter,"%Y-%m-01"),INTERVAL 1 MONTH) END )'
        );

        $select = $db->select()
            ->from(array('p'=>'pre_status'),$cols)
            ->join(array('s'=>'staff'),'s.id = p.staff_id',array())
            ->join(array('c'=>'company'),'c.id = p.company_id',array())
            ->join(array('tt'=>'team'),'tt.id = p.title',array())
            ->join(array('t'=>'team'),'t.id = tt.parent_id',array())
            ->join(array('d'=>'team'),'d.id = t.parent_id',array())
            ->joinLeft(array('u'=>'unit_code'),'u.id = p.unit_code_id',array())
            ->joinLeft(array('o'=>'status_option'),'o.id = p.option',array())
            ->join(array('r'=>'regional_market'),'r.id = s.regional_market',array())
            ->joinLeft(array('pr'=>'province'),'r.province_id = pr.id',array())
            ->joinLeft(array('ai'=>'area_nationality'),'ai.id = pr.area_national_id',array())
            ->order(array('time_effective ASC','type ASC','option ASC','time_alter ASC'))
            ->where('p.staff_id = ?',$staff_id)
            ->where('p.locked = ?',1)
            ->where('p.del = ?',0)
        ;
        // echo $select;
        // exit;
        $result = $db->fetchAll($select);
        return $result;
    }

    public function recycleBin($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');
        $cols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
            'p.*',
            'staff_name'      => 'CONCAT(s.firstname," ",s.lastname)',
            'unit_code_name'  => 'u.unit_code',
            'company_name'    => 'c.name',
            'option_name'     => 'o.name',
            'staff_code'      => 's.code',
            'team_name'       => 't.name',
            'title_name'      => 'CONCAT(tt.name_vn," - ",pr.name," - ",ai.name)',
            'department_name' => 'd.name',
            'kv'              => 'ai.name'
        );
        $select = $db->select()
            ->from(array('p'=>'pre_status'),$cols)
            ->join(array('s'=>'staff'),'s.id = p.staff_id',array())
            ->join(array('c'=>'company'),'c.id = p.company_id',array())
            ->join(array('d'=>'team'),'d.id = p.department',array())
            ->join(array('t'=>'team'),'t.id = p.team',array())
            ->join(array('tt'=>'team'),'tt.id = p.title',array())
            ->joinLeft(array('u'=>'unit_code'),'u.id = p.unit_code_id',array())
            ->joinLeft(array('o'=>'status_option'),'o.id = p.option',array())
            ->join(array('r'=>'regional_market'),'r.id = s.regional_market',array())
            ->joinLeft(array('pr'=>'province'),'r.province_id = pr.id',array())
            ->joinLeft(array('ai'=>'area_nationality'),'ai.id = pr.area_national_id',array())
            ->order(array('del_at DESC','type ASC','option ASC','time_alter DESC'))
            ->where("p.del = ?",1)
        ;

        if(isset($params['code']) AND $params['code']){
            $select->where('s.code = ?',$params['code']);
        }


        if(isset($params['ins_number']) AND $params['ins_number']){
            $select->where('s.insurance_number = ?',$params['ins_number']);
        }

        if(isset($params['company_id']) AND $params['company_id']){
            $select->where('s.company_id = ?',$params['company_id']);
        }

        if(isset($params['name']) AND $params['name']){
            $select->where('CONCAT(s.firstname," ",s.lastname) LIKE ?','%'.$params['name'].'%');
        }

        if(isset($params['date_bh']) AND $params['date_bh']){
            $select->where('p.time_alter = ?',trim(My_Date::normal_to_mysql($params['date_bh'])));
        }

        if(isset($params['type']) AND $params['type']){
            if($params['type'] == 1){
                $select->where('`option` IN (1,2)');
            }elseif($params['type'] == 2){
                $select->where('`option` IN (3,4,5,6,9,10)');
            }elseif($params['type'] == 3){
                $select->where('`option` IN (7,8)');
            }
        }

        if($limit){
            $select->limitPage($page,$limit);
        }
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public function fetchActiveBeforeTerm($page,$limit,&$total, $params){
        $db = Zend_Registry::get('db');

        $stmt = $db->query('Call p_insurance_staff_active()');
        $stmt->execute();

        $cols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.id'),
            'a.*',
            'staff_name' => 'CONCAT(b.firstname, " " ,b.lastname)',
            'staff_code' => 'b.code',
        );

        $select = $db->select()
                ->from(array('a'=>'insurance_staff_active'),$cols)
                ->join(array('b'=>'staff'),'a.staff_id = b.id',array())
                ->order(array('status ASC','a.active_date DESC'));
        ;

        if($limit){
            $select->limitPage($page,$limit);
        }

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public function addInsuranceInfo($ids = array()){
        if(count($ids)){
            $db = Zend_Registry::get('db');
            $arr = array();
            foreach($ids as $id){
                $arr[] = intval($id);
            }
            
            $id_str = implode(',', $arr);
            $sql = '
                SELECT b.id, b.firstname, b.lastname, b.ID_number, b.ID_date, b.gender 
                FROM pre_status a
                INNER JOIN staff b ON a.staff_id = b.id
                LEFT JOIN insurance_info c ON c.staff_id = a.staff_id
                WHERE c.id IS NULL AND a.id IN ('.$id_str.')
                GROUP BY b.id;  
            ';
            $stmt = $db->query($sql);
            $stmt->execute();
        }
    }

    public function c45($params){
        $db = Zend_Registry::get('db');

        $params['regional_market'] = !empty($params['regional_market'])? $params['regional_market']:null;
        $params['status'] = !empty($params['status'])? $params['status']:null;

        $sql_params = array(
            $params['code'],
            $params['name'],
            $params['insurance_number'],
            $params['status'],
            $params['regional_market'],
            $params['unit_code_id']
        );
        $stmt = $db->query('CALL p_insurance_c45(?,?,?,?,?,?)',$sql_params);
        $result = $stmt->fetchAll();
        return $result;

    }

    public function hintQdnv($month,$company_id,$option = 3){
        $db          = Zend_Registry::get('db');
        $date        = My_Date::normal_to_mysql($month);
        $from_create = new DateTime($date);
        $from_create = $from_create->sub(new DateInterval('P1M'));
        $from        = $from_create->format('Y-m-16');

        $to_create  = new DateTime($date);
        $to         = $to_create->format('Y-m-16');
        $month_year = $to_create->format('my');

        $cols = array(
            'a.qdnv',
            'stt' => 'CASE WHEN a.qdnv IS NOT NULL AND a.qdnv != ""  THEN  SUBSTRING_INDEX(a.qdnv,"/",1)  ELSE 0 END',
        );

        $select = $db->select()
            ->from(array('a'=>'pre_status'),$cols)
            ->where('a.`option` = ?',$option)
            ->where('time_alter >= ?',$from)
            ->where('time_alter <= ?',$to)
            ->where('qdnv IS NOT NULL AND qdnv != ""')
            ->where('company_id = ?',$company_id)
            ->where('del IS NULL OR del = ?',0)
            ->order('CASE WHEN a.qdnv IS NOT NULL AND a.qdnv != "" THEN  SUBSTRING_INDEX(a.qdnv,"/",1) ELSE 0 END DESC')
            ->limit(1)
        ;
        
        $row = $db->fetchRow($select);
        $stt = 1;
        if($row){
            $stt = intval($row['stt']) + 1;
        }
        
        $qd = '/QĐNV';
        if($option == 5){
            $qd = '/QĐNKL';
        }

        $qdnv = str_pad($stt,3,'0',STR_PAD_LEFT).'/'.$month_year.$qd;
        return $qdnv;
    }

    public function get_cols_name_book(){
        return array(
            'code'                 => 'code',
            'have_book'            => 'have_book',
            'close_book'           => 'close_book',
            'return_ins_book_time' => 'return_book',
            'insurance_number'     => 'insurance_number'
        );
    }

}