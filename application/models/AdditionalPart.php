<?php

class Application_Model_AdditionalPart extends Zend_Db_Table_Abstract
{
    protected $_name = 'additional_part';
    protected $_schema = DATABASE_TRADE;

    public function getAll($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['p' => DATABASE_TRADE.'.additional_part'], [
                         'p.*'
                     ])
        ->where('p.is_deleted = 0');

        if ($params['additional_part_type']) {
            $select->where('p.type IN (?)', $params['additional_part_type']);
        }

        $result = $db->fetchAll($select);

        return $result;
    }

    public function fetchPagination($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE . '.additional_part'], [
                'id' => new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT a.id'),
                'name' => 'a.name',
                'type' => 'a.type'
            ])
        ->where('a.is_deleted = 0');

        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
}