<?php
class Application_Model_Distributor extends Zend_Db_Table_Abstract
{
    protected $_name = 'distributor';
    protected $_schema = WAREHOUSE_DB;

    function getListDistributorUpdateEmail($list_area = array()) {
        $db = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('d' => WAREHOUSE_DB . '.' . $this->_name), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS d.id'), 'store_id' => 'd.id', 'store_name' => 'd.title', 'store_code' => 'd.store_code','email_main' => 'd.email', 'area_name' => 'a.name'))
                ->joinLeft(array('muh' => WAREHOUSE_DB . '.mass_upload_email_dealer'), 'muh.store_id = d.`id`', array('email' => 'muh.email'))
                ->join(array('rm' => DATABASE_CENTER . '.regional_market'), 'rm.id = d.`district`', array())
                ->join(array('rm1' => DATABASE_CENTER . '.regional_market'), 'rm1.id = rm.`parent`', array())
                ->join(array('a' => DATABASE_CENTER . '.area'), 'a.id = rm1.`area_id`', array())
                ->where('(d.`del` = 0 OR d.del IS NULL)')
                ->where('d.`is_ka` <> 1');
        if (!empty($list_area)) {
            $select->where('a.id IN(?)', $list_area);
        }
        $result = $db->fetchAll($select);
        return $result;
    }
    
    function getListDistributorUpdateActive($list_area = array()) {
        $db = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('d' => WAREHOUSE_DB . '.mass_upload_active_dealer'), array('d.*'))
                ->join(array('a' => DATABASE_CENTER . '.area'), 'a.id = d.`area`', array('area_name'=>'a.name'));
        if (!empty($list_area)) {
            $select->where('a.id IN(?)', $list_area);
        }
        $result = $db->fetchAll($select);
        return $result;
    }

    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => WAREHOUSE_DB.'.'.$this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'))
            ->joinLeft(array('s' => 'store'), 'p.id=s.d_id', array('total' => new Zend_Db_Expr('SUM(
                                                                        CASE
                                                                        WHEN s.id IS NULL THEN 0
                                                                        WHEN s.del = 0 OR s.del IS NULL THEN 1 ELSE 0 
                                                                        END
                                                                    )')))
            ->join(array('dt' => 'regional_market'), 'p.district=dt.id', array('district_name' => 'dt.name'))
            ->join(array('pr' => 'regional_market'), 'pr.id=dt.parent', array('province_name' => 'pr.name'))
            ->join(array('a' => 'area'), 'a.id=pr.area_id', array('area_name' => 'a.name'));

        if (isset($params['name']) and $params['name']){
            $select->where('p.title LIKE ?', '%'.$params['name'].'%');
        }

        if (isset($params['district_id']) and $params['district_id']){
            $select->where('dt.id = ?', $params['district_id']);
        }elseif (isset($params['region_id']) and $params['region_id']){
            $select->where('pr.id = ?', $params['region_id']);
        }elseif (isset($params['area_id']) and $params['area_id']){
            $select->where('a.id = ?', $params['area_id']);
        }

        if (isset($params['no_parent']) and $params['no_parent']){
            $select->where('p.parent = ?', 0);
        }

        if(isset($params['keyword']) and $params['keyword']){
            $keyword = $this->getAdapter()->quote('%'.$params['keyword'].'%');
            $select->where("p.title LIKE ".$keyword." OR p.store_code LIKE ".$keyword." ");   
        }

        if(isset($params['keyword']) and $params['keyword']){
            $keyword = $this->getAdapter()->quote('%'.$params['keyword'].'%');
            $select->where("p.title LIKE ".$keyword." OR p.store_code LIKE ".$keyword." ");   
        }

        if(isset($params['list_id']) and is_array($params['list_id'])){
            $select->where('p.id IN (?)',$params['list_id']);
        }

        if(isset($params['view_all']) and $params['view_all']){

        }else{
            $select->where('p.del IS NULL OR p.del = ?', 0);    
        }
        

        $order_str = $collate = '';

        if (isset($params['sort']) and $params['sort']) {
            $collate = '';

            if (in_array($params['sort'], array('name', 'title', 'unames', 'district', 'province', 'area')))
                $collate = ' COLLATE utf8_unicode_ci ';

            $desc = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';

            switch ( $params['sort'] ) {
                case 'district':
                    $order_str = 'dt.`name`';
                    break;
                case 'province':
                    $order_str = 'pr.`name`';
                    break;
                case 'area':
                    $order_str = 'a.`name`';
                    break;
                case 'total':
                    $order_str = 'total';
                    break;

                default:
                    $order_str = 'p.`'.$params['sort'] . '` ';
                    break;
            }

            $order_str .=  $collate . $desc;

            $select->order(new Zend_Db_Expr($order_str));
        }

        $select ->group('p.id')->order('p.title', 'COLLATE utf8_unicode_ci ASC');
           
        
        if ($limit){
            $select->limitPage($page, $limit);
        }

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        
        return $result;
    }

    public function count_store($id)
    {
        if (!$id) return false;

        $QStore = new Application_Model_Store();

        if ($id != 'null')
            $where = $QStore->getAdapter()->quoteInto('(del=0 OR del IS NULL) AND d_id = ?', $id);
        else
            $where = $QStore->getAdapter()->quoteInto('(del=0 OR del IS NULL) AND (d_id IS NULL OR d_id = 0)', 1);

        $stores = $QStore->fetchAll($where);

        if ($stores) {
            return $stores->count();
        }

        return false;
    }

    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if ($result === false) {
            $db = Zend_Registry::get('db');

            $select = $db->select()
                ->from(array('p' => WAREHOUSE_DB.'.'.$this->_name),
                    array('p.*'))
                ->joinLeft(array('r' => 'regional_market'), 'r.id=p.district', array('district_name' => 'r.name'))
                ->joinLeft(array('r2' => 'regional_market'), 'r2.id=r.parent', array('region_name' => 'r2.name'))
                ->joinLeft(array('a' => 'area'), 'a.id=r2.area_id', array('area_name' => 'a.name'))
                ->joinLeft(array('b' => 'bigarea'), 'b.id=a.bigarea_id', array('bigarea_name' => 'b.name'))
                ->order('title');

            $data = $db->fetchAll($select);

            $result = array();
            foreach ($data as $item){
                $result[$item['id']] = array(
                    'title'         => $item['title'],
                    'district'      => $item['district'],
                    'district_name' => $item['district_name'],
                    'region_name'   => $item['region_name'],
                    'area_name'     => $item['area_name'],
                    'add'           => $item['add'],
                    'partner_id'    => $item['partner_id'],
                    'bigarea_name'    => $item['bigarea_name'],
                );
            }

            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }

    function get_distributor_mapping(){

        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => WAREHOUSE_DB.'.distributor_mapping'),
                array('p.*'));

        $data = $db->fetchAll($select);

        $result = [];
        foreach ($data as $key => $value) {
            $result[$value['distributor_id']] = $value['code'];
        }
  
        return $result;
    }

    public function getFilter($search){
        $db = Zend_Registry::get('db');
        $sql = '
            SELECT id, title FROM warehouse.distributor
            WHERE title LIKE "%'.$search.'%"
            GROUP BY id LIMIT 20
        ';
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $data;
    }
    
    public function get_channel_create($search){
        $db = Zend_Registry::get('db');
        $sql = "SELECT id, title FROM ".WAREHOUSE_DB.".distributor
                WHERE channel_cost = 1";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $data;
    }
    
    public function getChannelKa($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'channel' => "IF(p.parent = 0, p.id, p.parent)",
            'p.title'
        );

        $select->from(array('p' => WAREHOUSE_DB.'.distributor'), $arrCols);
        $select->where('p.is_ka = 1', NULL);
        $select->where('p.is_ka_parent = 1', NULL);

        $select->group('IF(p.parent = 0, p.id, p.parent)');

        $result = $db->fetchAll($select);

        return $result;
    }
}                                                      
