<?php
class Application_Model_RepairQuotation extends Zend_Db_Table_Abstract
{
    protected $_name = 'repair_quotation';
     protected $_schema = DATABASE_TRADE;

    public function getRepairDetailsQuotation($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $select->from(array('r' => DATABASE_TRADE.'.repair'), ['rd.id', 'rq.*'])
                ->joinLeft(array('rd' => DATABASE_TRADE.'.repair_details'), 'r.id = rd.repair_id', [])
                ->joinLeft(array('rq' => DATABASE_TRADE.'.repair_quotation'), 'rd.id = rq.repair_details_id', [])
                ->where('r.id = ?', $params['repair_id']);

        if (isset($params['price_type']) && $params['price_type'] == 0) {
            $select->where('rq.status = ?', $params['price_type']);
        }
        if (isset($params['price_type']) && $params['price_type'] != 0) {
            $select->where('rq.status = ?', $params['price_type']);
        }
        
        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $repair_quotation [$element ['repair_details_id'] ] [] = $element;
            $sum_total_price_quotation [$element ['repair_details_id'] ] += $element['total_price'] + $element['total_price'] * VAT;
        }

        $result = [
          'repair_quotation' => $repair_quotation,
           'sum_total_price_quotation' =>  $sum_total_price_quotation
        ];

        return $result;
    }
    
    public function getRepairDetailsQuotationLast($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $select->from(array('r' => DATABASE_TRADE.'.repair'), ['rd.id', 'rq.*'])
                ->joinLeft(array('rd' => DATABASE_TRADE.'.repair_details'), 'r.id = rd.repair_id', [])
                ->joinLeft(array('rq' => DATABASE_TRADE.'.repair_quotation'), 'rd.id = rq.repair_details_id', [])
                ->where('r.id = ?', $params['repair_id']);
        
        $select->where('rq.status = 2');
        
        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $repair_quotation [$element ['repair_details_id'] ] [] = $element;
            $sum_total_price_quotation [$element ['repair_details_id'] ] += $element['total_price'] + $element['total_price'] * VAT;
        }

        $result = [
          'repair_quotation' => $repair_quotation,
           'sum_total_price_quotation' =>  $sum_total_price_quotation
        ];

        return $result;
    }
    
}