<?php
class Application_Model_RequestPayment extends Zend_Db_Table_Abstract
{
    protected $_name = 'request_payment';
    public function getRealCost($idRequest){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SUM(p.payment_cost) as cost_real_sum'));
        $select->from(array('p' => 'request_payment'), $arrCols);
        $select->joinLeft(array('request' => 'request'), 'p.request_id = request.id');
        $select->where('p.request_id=?', $idRequest);
        $select->group('p.request_id');
        $select->order('p.id DESC');
        $result = $db->fetchRow($select);
        return $result;
    }
}