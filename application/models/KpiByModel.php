<?php
class Application_Model_KpiByModel extends Zend_Db_Table_Abstract
{
	protected $_name = 'kpi_by_model';
	
	function fetchPagination($page, $limit, &$total, $params) {
//           if (!in_array($_SERVER['REMOTE_ADDR'], array('103.37.32.126', '112.109.92.6', '14.161.22.196', '115.78.166.171','171.244.18.76','171.244.18.86'))) {  
//               echo "<pre>";
//            print_r("Hệ thống đang tạm khóa .Vui lòng quay lại sau ít phút nữa");
//            die;
//        }


      

        $db             = Zend_Registry::get('db');
        $params['from'] = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
        $params['to']   = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");

        $params['from'] = $params['from'] . ' 00:00:00';
        $params['to']   = $params['to'] . ' 23:59:59';

        $subimei = $db->select()
                ->from(array('i' => 'kpi_by_model'), array('i.id', 'i.good_id', 'i.timing_date', 'i.color_id', 'i.store_id' , 'i.status', 'i.qty', 'i.area_id', 'i.province_id', 'i.district_id', 'i.pg_allow', 'i.pg_total', 'i.status_locked'));
        if (isset($params['staff_id']) and $params['staff_id']) {
            $subimei->join(array('sl' => 'store_staff_log'), ' sl.store_id = i.store_id and i.timing_date >= FROM_UNIXTIME(sl.joined_at,"%Y-%m-%d") AND (sl.released_at IS NULL OR DATE(i.timing_date) < FROM_UNIXTIME(sl.released_at,"%Y-%m-%d"))
	AND sl.is_leader = 0 ', array());

            $subimei->where('sl.staff_id = ?', $params['staff_id']);
        }
        $subimei->where('i.timing_date >= ?', $params['from']);
        $subimei->where('i.timing_date <= ?', $params['to']);
        if (isset($params['area_id_search']) and $params['area_id_search']) {
            $subimei->where('i.area_id IN(?)', $params['area_id_search']);
        }
        if (isset($params['store_id']) and $params['store_id']){
            $subimei->where('i.store_id = ?', $params['store_id']);
        }

        $select = $db->select()
            ->from(array('p' => $subimei), array(
                new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
                'p.id',
                'p.good_id',
                'p.timing_date',
                'p.color_id',
                'p.store_id' ,
                'p.status',
                'p.area_id',
                'p.province_id',
                'p.district_id',
                'p.pg_allow',
                'p.pg_total',
                'quantity'  => 'p.qty',
                'p.status_locked'
            ))

            ->joinLeft(array('kbs' => 'kpi_by_staff'), 'kbs.kpi_by_model_id = p.id', array('staff_qty' => 'kbs.qty', 'pg_id' => 'kbs.pg_id','staff_qty' => 'kbs.qty',
                    'info_qty' => new Zend_Db_Expr('GROUP_CONCAT(kbs.qty)') ,
                    'kbs.qty',
                    'info_pg_id' => new Zend_Db_Expr('GROUP_CONCAT(kbs.pg_id)'),
                    'total_quantity_pg' => "SUM(IF(kbs.qty IS NULL, 0, kbs.qty))"
                ) )
                ->joinLeft(array('a' => 'area'), 'p.area_id = a.id', array('area' => 'a.name'))
                ->joinLeft(array('rm' => 'regional_market'), 'p.province_id = rm.id', array('province' => 'rm.name'))
                ->joinLeft(array('rm1' => 'regional_market'), 'p.district_id = rm1.id', array('district' => 'rm1.name'))
                ->joinLeft(array('s' => 'store'), 'p.store_id = s.id', array('store' => 's.name'))
                ->joinLeft(array('s1' => 'staff'), 'kbs.pg_id = s1.id', array('firstname' => 's1.firstname', 'lastname' => 's1.lastname', 'code' => 's1.code', 'email' => 's1.email'))
                ->joinLeft(array('g' => WAREHOUSE_DB . '.good'), 'p.good_id = g.id', array('model' => 'g.desc'))
                ->joinLeft(array('c' => WAREHOUSE_DB . '.good_color'), 'p.color_id = c.id', array('color' => 'c.name'));
//                ->joinLeft(array('app' => 'apps_installing_imei'), 'app.imei_sn = p.imei_sn', array("app_installed" => 'app.app_id', "app_installed_pg" => 'app.staff_id'));
//        $select->joinLeft(array('ir' => 'imei_installment'), 'ir.imei_sn = p.imei_sn and ir.staff_id=p.pg_id', array('ir.installment_company', 'installment_contract' => 'ir.contract_number'));



        if (isset($params['staff_id']) and $params['staff_id'])
            $select->where('p.pg_allow LIKE ?', '%,' . $params['staff_id'] . ',%');

//        if (isset($params['imei']) and $params['imei'])
//            $select->where('p.imei_sn = ?', $params['imei']);

        if (isset($params['product']) and $params['product'])
            $select->where('p.good_id = ?', $params['product']);

        if (isset($params['color']) and $params['color'])
            $select->where('p.color_id = ?', $params['color']);

        if (isset($params['area_id']) and $params['area_id'])
            $select->where('p.area_id IN(?)', $params['area_id']);

        if (isset($params['regional_market']) and $params['regional_market'])
            $select->where('p.province_id = ?', $params['regional_market']);

        if (isset($params['district']) and $params['district'])
            $select->where('p.district_id IN ?', $params['district']);

        if (isset($params['status']) and $params['status'] and $params['status'] == 1) {
            $select->having('quantity = total_quantity_pg');
        }

        if (isset($params['status']) and $params['status'] and $params['status'] == 2) {
            $select->having('quantity > total_quantity_pg');
        }


        if (isset($params['shop_type']) and $params['shop_type'] and $params['shop_type'] == 1)
            $select->where('p.pg_total > 0');

        if (isset($params['shop_type']) and $params['shop_type'] and $params['shop_type'] == 2)
            $select->where('p.pg_total = 0');

        if (isset($params['kpi']) and $params['kpi'] and $params['kpi'] == 1)
            $select->where('p.kpi_pg = 0');

        if (isset($params['store_id']) and $params['store_id'])
            $select->where('p.store_id = ?', $params['store_id']);

//        if (isset($params['from']) && $params['from'])
//            $select->where('p.timing_date >= ?', $params['from']);
//
//        if (isset($params['to']) && $params['to'])
//            $select->where('p.timing_date <= ?', $params['to']);
        
//        if (isset($params['sum']) and $params['sum']){
//            
//        }
//        else{
//            
//        }
        $select->where('p.`status` = 1');   // status = 1 =>  TGDD 
        
        $select->group('p.id');
        
//        // lấy số tổng
//        $select_total_quantity = $db->select()
//            ->from(['t' => $select], [
//               'total_area' => 'SUM(t.quantity)'
//           ]);
//
//        $total_area = $db->fetchOne($select_total_quantity);

	
        $select->order(array('p.store_id', 'p.timing_date'));

        

        if ($limit)
            $select->limitPage($page, $limit);


        if (!empty($_GET['dev'])) {
             // echo "<pre>";
              print_r($select->__toString());
              exit;
        }
            
        $result = $db->fetchAll($select);
        $total  = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

	function getTotalQuantity($params) {


        $db             = Zend_Registry::get('db');
        $params['from'] = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
        $params['to']   = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");

        $params['from'] = $params['from'] . ' 00:00:00';
        $params['to']   = $params['to'] . ' 23:59:59';

        $subimei = $db->select()
                ->from(array('i' => 'kpi_by_model'), array('i.id', 'i.good_id', 'i.timing_date', 'i.color_id', 'i.store_id' , 'i.status', 'i.qty', 'i.area_id', 'i.province_id', 'i.district_id', 'i.pg_allow', 'i.pg_total'));
        if (isset($params['staff_id']) and $params['staff_id']) {
            $subimei->join(array('sl' => 'store_staff_log'), ' sl.store_id = i.store_id and i.timing_date >= FROM_UNIXTIME(sl.joined_at,"%Y-%m-%d") AND (sl.released_at IS NULL OR DATE(i.timing_date) < FROM_UNIXTIME(sl.released_at,"%Y-%m-%d"))
	AND sl.is_leader = 0 ', array());

            $subimei->where('sl.staff_id = ?', $params['staff_id']);
        }
        $subimei->where('i.timing_date >= ?', $params['from']);
        $subimei->where('i.timing_date <= ?', $params['to']);
        if (isset($params['area_id_search']) and $params['area_id_search']) {
            $subimei->where('i.area_id IN(?)', $params['area_id_search']);
        }
        if (isset($params['store_id']) and $params['store_id']){
            $subimei->where('i.store_id = ?', $params['store_id']);
        }

        $select = $db->select()
            ->from(array('p' => $subimei), array(
                'quantity'  => 'p.qty',
            ))

            ->joinLeft(array('kbs' => 'kpi_by_staff'), 'kbs.kpi_by_model_id = p.id', array(
                    'total_quantity_pg' => "SUM(IF(kbs.qty IS NULL, 0, kbs.qty))"
                ) )
                ->joinLeft(array('a' => 'area'), 'p.area_id = a.id', array())
                ->joinLeft(array('rm' => 'regional_market'), 'p.province_id = rm.id', array())
                ->joinLeft(array('rm1' => 'regional_market'), 'p.district_id = rm1.id', array())
                ->joinLeft(array('s' => 'store'), 'p.store_id = s.id', array())
                ->joinLeft(array('s1' => 'staff'), 'kbs.pg_id = s1.id', array())
                ->joinLeft(array('g' => WAREHOUSE_DB . '.good'), 'p.good_id = g.id', array())
                ->joinLeft(array('c' => WAREHOUSE_DB . '.good_color'), 'p.color_id = c.id', array());

        if (isset($params['staff_id']) and $params['staff_id'])
            $select->where('p.pg_allow LIKE ?', '%,' . $params['staff_id'] . ',%');

        if (isset($params['product']) and $params['product'])
            $select->where('p.good_id = ?', $params['product']);

        if (isset($params['color']) and $params['color'])
            $select->where('p.color_id = ?', $params['color']);

        if (isset($params['area_id']) and $params['area_id'])
            $select->where('p.area_id IN(?)', $params['area_id']);

        if (isset($params['regional_market']) and $params['regional_market'])
            $select->where('p.province_id = ?', $params['regional_market']);

        if (isset($params['district']) and $params['district'])
            $select->where('p.district_id IN ?', $params['district']);

//        if (isset($params['status']) and $params['status'] and $params['status'] == 1) {
//            $select->having('quantity = total_quantity_pg');
//        }
//
//        if (isset($params['status']) and $params['status'] and $params['status'] == 2) {
//            $select->having('quantity <> total_quantity_pg');
//        }


        if (isset($params['shop_type']) and $params['shop_type'] and $params['shop_type'] == 1)
            $select->where('p.pg_total > 0');

        if (isset($params['shop_type']) and $params['shop_type'] and $params['shop_type'] == 2)
            $select->where('p.pg_total = 0');

        if (isset($params['kpi']) and $params['kpi'] and $params['kpi'] == 1)
            $select->where('p.kpi_pg = 0');

        if (isset($params['store_id']) and $params['store_id'])
            $select->where('p.store_id = ?', $params['store_id']);

        $select->where('p.`status` = 1');   // status = 1 =>  TGDD

        $select->group('p.id');

        // lấy số tổng
        $select_total_quantity = $db->select()
            ->from(['t' => $select], [
               'total_quantity' => 'SUM(t.quantity)',
               'quantity_selected' => 'SUM(t.total_quantity_pg)',
           ]);

        $result = $db->fetchRow($select_total_quantity);

        if (!empty($_GET['devv'])) {
             // echo "<pre>";
              print_r($select_total_quantity->__toString());
              exit;
        }


        return $result;
    }





        function fetchPaginationCount($page, $limit, &$total, $params) {


        $db             = Zend_Registry::get('db');
        $params['from'] = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
        $params['to']   = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");

        $params['from'] = $params['from'] . ' 00:00:00';
        $params['to']   = $params['to'] . ' 23:59:59';

        $subimei = $db->select()
                ->from(array('i' => 'kpi_by_model'), array('i.*'));
        if (isset($params['staff_id']) and $params['staff_id']) {
            $subimei->join(array('sl' => 'store_staff_log'), ' sl.store_id = i.store_id and i.timing_date >= FROM_UNIXTIME(sl.joined_at,"%Y-%m-%d") AND (sl.released_at IS NULL OR DATE(i.timing_date) < FROM_UNIXTIME(sl.released_at,"%Y-%m-%d"))
	AND sl.is_leader = 0 ', array());

            $subimei->where('sl.staff_id = ?', $params['staff_id']);
        }
        $subimei->where('i.timing_date >= ?', $params['from']);
        $subimei->where('i.timing_date <= ?', $params['to']);
        if (isset($params['area_id_search']) and $params['area_id_search']) {
            $subimei->where('i.area_id IN(?)', $params['area_id_search']);
        }
        if (isset($params['store_id']) and $params['store_id']){
            $subimei->where('i.store_id = ?', $params['store_id']);
        }

        $select = $db->select()
                ->from(array('p' => $subimei), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'), 	
				'total_area'  => new Zend_Db_Expr('SUM(p.qty)')	
				))
              /*  ->joinLeft(array('kbs' => 'kpi_by_staff'), 'kbs.kpi_by_model_id = p.id', array('staff_qty' => 'kbs.qty', 'pg_id' => 'kbs.pg_id','staff_qty' => 'kbs.qty', 
                    // 'quantity'  => new Zend_Db_Expr('MAX(p.qty)'),
                   // 'total_area'  => new Zend_Db_Expr('SUM(p.qty)'),
                   // 'info_qty'     => new Zend_Db_Expr('GROUP_CONCAT(kbs.qty)') , 'kbs.qty', 'info_pg_id'     => new Zend_Db_Expr('GROUP_CONCAT(kbs.pg_id)')) 
					)
					*/
                ->joinLeft(array('a' => 'area'), 'p.area_id = a.id', array('area' => 'a.name'))
                ->joinLeft(array('rm' => 'regional_market'), 'p.province_id = rm.id', array('province' => 'rm.name'))
                ->joinLeft(array('rm1' => 'regional_market'), 'p.district_id = rm1.id', array('district' => 'rm1.name'))
                ->joinLeft(array('s' => 'store'), 'p.store_id = s.id', array('store' => 's.name'))
                // ->joinLeft(array('s1' => 'staff'), 'kbs.pg_id = s1.id', array('firstname' => 's1.firstname', 'lastname' => 's1.lastname', 'code' => 's1.code', 'email' => 's1.email'))
                ->joinLeft(array('g' => WAREHOUSE_DB . '.good'), 'p.good_id = g.id', array('model' => 'g.desc'))
                ->joinLeft(array('c' => WAREHOUSE_DB . '.good_color'), 'p.color_id = c.id', array('color' => 'c.name'));
//                ->joinLeft(array('app' => 'apps_installing_imei'), 'app.imei_sn = p.imei_sn', array("app_installed" => 'app.app_id', "app_installed_pg" => 'app.staff_id'));
//        $select->joinLeft(array('ir' => 'imei_installment'), 'ir.imei_sn = p.imei_sn and ir.staff_id=p.pg_id', array('ir.installment_company', 'installment_contract' => 'ir.contract_number'));



        if (isset($params['staff_id']) and $params['staff_id'])
            $select->where('p.pg_allow LIKE ?', '%,' . $params['staff_id'] . ',%');

//        if (isset($params['imei']) and $params['imei'])
//            $select->where('p.imei_sn = ?', $params['imei']);

        if (isset($params['product']) and $params['product'])
            $select->where('p.good_id = ?', $params['product']);

        if (isset($params['color']) and $params['color'])
            $select->where('p.color_id = ?', $params['color']);

        if (isset($params['area_id']) and $params['area_id'])
            $select->where('p.area_id IN(?)', $params['area_id']);

        if (isset($params['regional_market']) and $params['regional_market'])
            $select->where('p.province_id = ?', $params['regional_market']);

        if (isset($params['district']) and $params['district'])
            $select->where('p.district_id IN ?', $params['district']);
/*
        if (isset($params['status']) and $params['status'] and $params['status'] == 1)
            $select->where('kbs.pg_id is not null');

        if (isset($params['status']) and $params['status'] and $params['status'] == 2)
            $select->where('kbs.pg_id is null');
*/
        if (isset($params['shop_type']) and $params['shop_type'] and $params['shop_type'] == 1)
            $select->where('p.pg_total > 0');

        if (isset($params['shop_type']) and $params['shop_type'] and $params['shop_type'] == 2)
            $select->where('p.pg_total = 0');

        if (isset($params['kpi']) and $params['kpi'] and $params['kpi'] == 1)
            $select->where('p.kpi_pg = 0');

        if (isset($params['store_id']) and $params['store_id'])
            $select->where('p.store_id = ?', $params['store_id']);

//        if (isset($params['from']) && $params['from'])
//            $select->where('p.timing_date >= ?', $params['from']);
//
//        if (isset($params['to']) && $params['to'])
//            $select->where('p.timing_date <= ?', $params['to']);
        
//        if (isset($params['sum']) and $params['sum']){
//            
//        }
//        else{
//            
//        }
            
//        $select->group('p.id');
	
//        $select->order(array('p.store_id', 'p.timing_date'));

        

        if ($limit)
            $select->limitPage($page, $limit);


        if (!empty($_GET['dev'])) {
           echo $select; die;
        }
            
        $result = $db->fetchAll($select);
        $total  = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }
    	function fetchPaginationDetail($page, $limit, &$total, $params) {


        $db             = Zend_Registry::get('db');
        $params['from'] = date_create_from_format("d/m/Y", $params['from'])->format("Y-m-d");
        $params['to']   = date_create_from_format("d/m/Y", $params['to'])->format("Y-m-d");

        $params['from'] = $params['from'] . ' 00:00:00';
        $params['to']   = $params['to'] . ' 23:59:59';

        $subimei = $db->select()
                ->from(array('i' => 'kpi_by_model'), array('i.*'));
        if (isset($params['staff_id']) and $params['staff_id']) {
            $subimei->join(array('sl' => 'store_staff_log'), ' sl.store_id = i.store_id and i.timing_date >= FROM_UNIXTIME(sl.joined_at,"%Y-%m-%d") AND (sl.released_at IS NULL OR DATE(i.timing_date) < FROM_UNIXTIME(sl.released_at,"%Y-%m-%d"))
	AND sl.is_leader = 0 ', array());

            $subimei->where('sl.staff_id = ?', $params['staff_id']);
        }
        $subimei->where('i.timing_date >= ?', $params['from']);
        $subimei->where('i.timing_date <= ?', $params['to']);
       
        if (isset($params['area_id_search']) and $params['area_id_search']) {
            $subimei->where('i.area_id IN(?)', $params['area_id_search']);
        }
        if (isset($params['store_id']) and $params['store_id']){
            $subimei->where('i.store_id = ?', $params['store_id']);
        }

        $select = $db->select()
                ->from(array('p' => $subimei), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
				'p.*'
				
				))
                ->joinLeft(array('kbs' => 'kpi_by_staff'), 'kbs.kpi_by_model_id = p.id', array('staff_qty' => 'kbs.qty', 'pg_id' => 'kbs.pg_id',
                    'staff_qty' => 'kbs.qty', 
                    'quantity'  => 'p.qty'
                    ) 
                )
                ->joinLeft(array('a' => 'area'), 'p.area_id = a.id', array('area' => 'a.name'))
                ->joinLeft(array('rm' => 'regional_market'), 'p.province_id = rm.id', array('province' => 'rm.name'))
                ->joinLeft(array('rm1' => 'regional_market'), 'p.district_id = rm1.id', array('district' => 'rm1.name'))
                ->joinLeft(array('s' => 'store'), 'p.store_id = s.id', array('store' => 's.name'))
                ->joinLeft(array('s1' => 'staff'), 'kbs.pg_id = s1.id', array('firstname' => 's1.firstname', 'lastname' => 's1.lastname', 'code' => 's1.code', 'email' => 's1.email'))
                ->joinLeft(array('g' => WAREHOUSE_DB . '.good'), 'p.good_id = g.id', array('model' => 'g.desc'))
                ->joinLeft(array('c' => WAREHOUSE_DB . '.good_color'), 'p.color_id = c.id', array('color' => 'c.name'));

        if (isset($params['staff_id']) and $params['staff_id'])
            $select->where('p.pg_allow LIKE ?', '%,' . $params['staff_id'] . ',%');
        
        if (isset($params['product']) and $params['product'])
            $select->where('p.good_id = ?', $params['product']);

        if (isset($params['color']) and $params['color'])
            $select->where('p.color_id = ?', $params['color']);
		
		 if (isset($params['area_id']) and $params['area_id'])
            $select->where('p.area_id IN(?)', $params['area_id']);

        if (isset($params['regional_market']) and $params['regional_market'])
            $select->where('p.province_id = ?', $params['regional_market']);

        if (isset($params['district']) and $params['district'])
            $select->where('p.district_id IN ?', $params['district']);

//        if (isset($params['status']) and $params['status'] and $params['status'] == 1)
//            $select->where('kbs.pg_id is not null');
//
//        if (isset($params['status']) and $params['status'] and $params['status'] == 2)
//            $select->where('kbs.pg_id is null');

        if (isset($params['shop_type']) and $params['shop_type'] and $params['shop_type'] == 1)
            $select->where('p.pg_total > 0');

        if (isset($params['shop_type']) and $params['shop_type'] and $params['shop_type'] == 2)
            $select->where('p.pg_total = 0');
		
		if (isset($params['kpi']) and $params['kpi'] and $params['kpi'] == 1)
            $select->where('p.kpi_pg = 0');

        if (isset($params['store_id']) and $params['store_id'])
            $select->where('p.store_id = ?', $params['store_id']);

        $select->where('p.`status` = 1');   // status = 1 =>  TGDD 
        
        $select->order(array('p.store_id', 'p.timing_date', 'p.id'));

        // if ($limit)
        //     $select->limitPage($page, $limit);


        if (!empty($_GET['dev'])) {
            echo "<pre>";
             print_r($select->__toString());
             exit;
        }
//echo "<pre>";
//            print_r($select->__toString());
            
        $result = $db->fetchAll($select);
        $total  = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public function getListChannel($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['km' => 'kpi_by_model'], [
                'channel_id' => "IF(d.parent IS NULL OR d.parent = 0, d.id, d.parent)",
                'channel_name' => "IFNULL(dp.title, d.title)"
            ])
            ->joinLeft(['s' => 'store'], 'km.store_id = s.id', [])
            ->joinLeft(['d' => WAREHOUSE_DB . '.distributor'], 's.d_id = d.id', [])
            ->joinLeft(['dp' => WAREHOUSE_DB . '.distributor'], 'd.parent = dp.id', [])
            ->where('km.timing_date >= ?', $params['from_date'])
            ->where('km.timing_date <= ?', $params['to_date'])
            ->where('d.is_ka = ?', 1)
            ->group('channel_id');

        $result = $db->fetchAll($select);
        foreach ($result as $item) {
            $list [$item['channel_id']] = $item['channel_name'];
        }
        return $list;

    }


    public function getListModel($params)
    {
        $db = Zend_Registry::get("db");
        $sub_select = $db->select()
            ->from(['km' => 'kpi_by_model'], [
                'good_id' => "km.good_id",
                'good_price_log_id' => "IF(p.id IS NOT NULL , p.id, pk.id)"
            ])
            ->joinLeft(['p' => WAREHOUSE_DB . '.good_price_log'], 'km.good_id = p.good_id  
                                                                    AND km.color_id = p.color_id
                                                                    AND km.timing_date >= p.from_date 
                                                                    AND (p.to_date IS NULL OR km.timing_date <= p.to_date) ', [])

            ->joinLeft(['pk' => WAREHOUSE_DB . '.good_price_log'], 'km.good_id = pk.good_id  
                                                                    AND pk.color_id = 0 
                                                                    AND km.timing_date >= pk.from_date 
                                                                    AND (pk.to_date IS NULL OR km.timing_date <= pk.to_date) ', [])
            ->where('km.timing_date >= ?', $params['from_date'])
            ->where('km.timing_date <= ?', $params['to_date'])
        ->group('good_price_log_id');

        if ($params['group_by_type'] == 2) { // report leader
            $sub_select->where('km.leader_id > ?', 0);
        }

        // main select
        $select = $db->select()
            ->from(['p' => WAREHOUSE_DB . '.good_price_log'], [
                'good_price_log_id' => 'p.id',
                'color' => 'gc.name',
                'p.price',
                'model_name' => "CONCAT(
                                m.`desc`,
                                IF(gc.name IS NOT NULL , CONCAT(' / ', gc.name, ' / '), ' / ') ,
                                CONCAT(
                                IF
                                    ( p.from_date IS NOT NULL, DATE_FORMAT( p.from_date, '%d-%m-%Y' ), '?' ),
                                    ' -> ',
                                IF
                                    ( p.to_date IS NOT NULL, DATE_FORMAT( p.to_date, '%d-%m-%Y' ), '?' ) 
                                ) 
                              )"

            ])
            ->join(array('g' => new Zend_Db_Expr('(' . $sub_select . ')')), 'p.id  = g.good_price_log_id', array())
            ->joinLeft(['m' => WAREHOUSE_DB . '.good'], 'p.good_id = m.id', [])
            ->joinLeft(['gpl' => WAREHOUSE_DB . '.good_price_log'], 'gpl.id = g.good_price_log_id', [])
            ->joinLeft(['gc' => WAREHOUSE_DB . '.good_color'], 'gc.id = gpl.color_id', [])
            ->order('p.good_id');
        $result = $db->fetchAll($select);

        if ($_GET['dev'] == 1) {
            echo $select;die;
        }

        $list = [];

        foreach ($result as $item) {
            $list [$item ['good_price_log_id']] = $item;
        }

        return $list;
    }

    public function convertDataQuantityDetail($data, $type)
    {
        // type = 1: area
        // type = 2: leader
        // type = 3: store
        // type = 4: channel
        if ($type == 1) {
            $type_name = 'area_id';
        }
        if ($type == 2) {
            $type_name = 'leader_id';
        }
        if ($type == 3) {
            $type_name = 'store_id';
        }
        if ($type == 4) {
            $type_name = 'channel_id';
        }


        $list = [];
        foreach ($data as $item) {
            $list[$item[$type_name]] [$item['good_price_log_id']] = $item;
        }

        return $list;

    }
    
     public function getListSaleLeader($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['n' => 'sales_region_staff_log'], [
                'staff_name' => "CONCAT(st.firstname, ' ', st.lastname)",
                'st.code',
                'title' => 't.name',
                'st.email',
                'region_name' => 'o.name' ,
                'leader_id' => 'st.id',
                'area_name' => 'a.name'
            ])
            ->joinLeft(['o' => 'sales_region'], 'n.sales_region_id = o.id', [])
            ->joinLeft(['a' => 'area'], 'o.parent = a.id', [])
            ->joinLeft(['st' => 'staff'], 'n.staff_id = st.id', [])
            ->joinLeft(['t' => 'team'], 'st.title = t.id', [])
            ->where('n.from_date <= ?', $params['to_date'])
            ->where('n.to_date IS NULL OR n.to_date > ?', $params['from_date'])
            ->where('n.type <> 1')
             ->group('n.staff_id');

        $result = $db->fetchAll($select);

        foreach ($result as $item) {
            $list [$item['leader_id']] = $item;
        }

        return $list;

    }

    public function getDataStore($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['s' => 'store'], [
                'store_id' => 's.id',
                'store_name' => 's.name',
                'district_name' => 'di.name',
                'province_name' => 'p.name',
                'area_name' => 'a.name',
                'sellout' => 'SUM(km.qty)'
            ])
            ->joinLeft(['km' => 'kpi_by_model'], 's.id = km.store_id', [])
            ->joinLeft(['di' => 'regional_market'], 'di.id = s.district', [])
            ->joinLeft(['p' => 'regional_market'], 'p.id = di.parent', [])
            ->joinLeft(['a' => 'area'], 'p.area_id = a.id', [])
            ->where('s.name LIKE ?', '%' . $params['store_name'] . '%')
            ->where('km.timing_date >= ?', $params['from_date'])
            ->where('km.timing_date <= ?', $params['to_date'])

        ->group('s.id');

        $result = $db->fetchAll($select);

        return $result;

    }

    public function updatePgAllow($params)
    {
        $db = Zend_Registry::get('db');

        $sql = "
       UPDATE 
        (
        SELECT 
        km.id,
        
        IFNULL( CONCAT( ',', GROUP_CONCAT(l.staff_id), ',' ), 0 ) pg_allow,
        IFNULL(GROUP_CONCAT(CONCAT('Name: ', CONCAT(st.firstname, ' ', st.lastname), CONCAT('<br>Email: ', SUBSTRING_INDEX(IFNULL(st.email, 'noemail@'), '@', 1), CONCAT('<br>Code: ', st.`code`))), '<hr>'), 'No PG') pg_string,
        COUNT( l.staff_id ) pg_total,
        IFNULL( GROUP_CONCAT( CONCAT( st.firstname, ' ', st.lastname, ' ', st.`code` ) ), 'No PG' ) pg_export
        
        FROM kpi_by_model km 
        LEFT JOIN store_staff_log l 
            ON l.store_id = km.store_id 
            AND l.is_leader = 0
            AND km.timing_date >= FROM_UNIXTIME(l.joined_at,'%Y-%m-%d')
            AND (l.released_at IS NULL OR km.timing_date < FROM_UNIXTIME(l.released_at,'%Y-%m-%d'))
            
        LEFT JOIN staff st ON st.id = l.staff_id 
        
        WHERE (km.timing_date BETWEEN :from_date AND :to_date) 
        AND km.store_id = :store_id
        
        GROUP BY km.id
        ) km_new 
        
        JOIN kpi_by_model km_old ON km_new.id = km_old.id
        
        SET km_old.pg_allow = km_new.pg_allow ,
                km_old.pg_string = km_new.pg_string ,
                km_old.pg_total = km_new.pg_total ,
                km_old.pg_export = km_new.pg_export
         
         ";

        $stmt = $db->prepare($sql);
        $stmt->bindParam("from_date", $params['from_date']);
        $stmt->bindParam("to_date", $params['to_date']);
        $stmt->bindParam("store_id", $params['store_id']);

        $stmt->execute();
        $stmt->closeCursor();
        $db   = $stmt = null;
    }

    public function deleteBaoSo($params)
    {
        $db = Zend_Registry::get('db');

        $sql = "
            DELETE ks
            FROM kpi_by_model km 
            JOIN kpi_by_staff ks ON km.id = ks.kpi_by_model_id 
            
            WHERE (km.timing_date BETWEEN :from_date AND :to_date) 
            AND km.store_id = :store_id
        ";

        $stmt = $db->prepare($sql);
        $stmt->bindParam("from_date", $params['from_date']);
        $stmt->bindParam("to_date", $params['to_date']);
        $stmt->bindParam("store_id", $params['store_id']);

        $stmt->execute();
        $stmt->closeCursor();
        $db   = $stmt = null;
    }
    
}