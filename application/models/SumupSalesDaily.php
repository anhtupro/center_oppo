<?php

class Application_Model_SumupSalesDaily extends Zend_Db_Table_Abstract
{
    protected $_name = 'sumup_sales_daily';

    public function fetchPagination($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(['s1' => 'sumup_sales_daily'], [
                new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT s1.store_id'),
                's1.store_id', 'total' => 'SUM(s1.quantity)',
                's1.date',
                's1.created_at'
            ])
            ->joinLeft(['s2' => 'store'], 's1.store_id = s2.id', ['store_name' => 's2.name'])
            ->joinLeft(['st' => 'staff'], 's1.created_by = st.id', ['staff_name' => "CONCAT(st.firstname,' ', st.lastname)", 'staff_code' => 'st.code'])
            ->group(['s1.store_id', 's1.date'])->order('s1.created_at DESC');

        if ($params['staff_code']) {
            $select->where('st.code = ?', $params['staff_code']);
        }
        if ($params['from_date'] && $params['to_date']) {
            $select->where("s1.date >= ?", $params['from_date']);
            $select->where("s1.date <= ?", $params['to_date']);
        }
        if ($params['store_id']) {
            $select->where('s1.store_id = ?', $params['store_id']);
        }

        if ($params['store_id_number']) {
            $select->where('s1.store_id = ?', $params['store_id_number']);
        }

        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    public function getStatistics($params)
    {
        $db = Zend_Registry::get('db');
        // get total each brand
        $select = $db->select()
            ->from(['s1' => 'sumup_sales_daily'], [
                's1.brand_id',
                'brand_name' => 'TRIM(b.name)',
                's1.store_id',
                'area' => 'r.name',
                's1.date',
                's1.created_at',
                'staff_name' => "CONCAT(st.firstname,' ',st.lastname)",
                'quantity' => 'SUM(quantity)',
                'total' => 's3.total_quantity'
            ])
            ->joinLeft(['st' => 'staff'], 's1.created_by = st.id', [])
            ->joinLeft(['s2' => 'store'], 's1.store_id = s2.id', [])
            ->joinLeft(['r' => 'regional_market'], 's2.regional_market = r.id', [])
            ->joinLeft(['b' => 'brand'], 's1.brand_id = b.id', [])
            ->joinLeft(['s3' => 'sumup_sales_daily_total'], 's1.store_id = s3.store_id AND s1.date = s3.date', [])
            ->group(['s1.date', 's1.brand_id', 's1.store_id'])
            ->order(['s1.store_id', 's1.date']);

        if ($params['staff_code']) {
            $select->where('st.code = ?', $params['staff_code']);
        }
        if ($params['from_date'] && $params['to_date']) {
            $select->where('s1.date >= ?', $params['from_date'])
                ->where('s1.date <= ?', $params['to_date']);
        }
        if ($params['store_id']) {
            $select->where('s1.store_id = ?', $params['store_id']);
        }

        if ($params['store_id_number']) {
            $select->where('s1.store_id = ?', $params['store_id_number']);
        }


        $list = $db->fetchAll($select);
        foreach ($list as $value) {
            $totalEachBrand [$value['store_id'] . ',' . $value['date']] [$value['brand_name']] = $value['quantity'];
            $totalEachBrand [$value['store_id'] . ',' . $value['date']] ['store_id'] = $value['store_id'];
            $totalEachBrand [$value['store_id'] . ',' . $value['date']] ['date'] = $value['date'];
            $totalEachBrand [$value['store_id'] . ',' . $value['date']] ['created_at'] = $value['created_at'];
            $totalEachBrand [$value['store_id'] . ',' . $value['date']] ['staff_name'] = $value['staff_name'];
            $totalEachBrand [$value['store_id'] . ',' . $value['date']] ['area'] = $value['area'];
            $totalEachBrand [$value['store_id'] . ',' . $value['date']] ['total'] = $value['total'];
        }

        // get total each model
        $select = $db->select()
            ->from(['s1' => 'sumup_sales_daily'], [
                's1.good_id',
                'model' => 'TRIM(p.name)',
                's1.quantity',
                's1.store_id',
                's1.date'
            ])
            ->joinLeft(['p' => 'phone_good'], 's1.good_id = p.id AND p.is_deleted = 0', [])
            ->joinLeft(['st' => 'staff'], 's1.created_by = st.id', [])
            ->where('s1.good_id <> ?', 0);

        if ($params['staff_code']) {
            $select->where('st.code = ?', $params['staff_code']);
        }
        if ($params['from_date'] && $params['to_date']) {
            $select->where('s1.date >= ?', $params['from_date'])
                ->where('s1.date <= ?', $params['to_date']);
        }
        if ($params['store_id']) {
            $select->where('s1.store_id = ?', $params['store_id']);
        }

        $list = $db->fetchAll($select);
        foreach ($list as $value) {
            $totalEachModel [$value['store_id'] . ',' . $value['date']] [$value['model']] = $value['quantity'];
            $totalEachModel [$value['store_id'] . ',' . $value['date']] [$value['date']] = $value['quantity'];
            $totalEachModel [$value['store_id'] . ',' . $value['date']] [$value['store_id']] = $value['quantity'];
        }

        // combine 2 array
        foreach ($totalEachBrand as $keyBrand => $valueBrand) {
            foreach ($totalEachModel[$keyBrand] as $key => $value) {
                $totalEachBrand [$keyBrand] [$key] = $value;
            }
        }

        return $totalEachBrand;
    }

    public function getHistory($staff_id, $date)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
                    ->from(['s' => 'store_staff_log'], [
                        's.store_id',
                        'store_name' => 's2.name',
                        'brands' => "GROUP_CONCAT(DISTINCT b.name)",
                        'ad.date'
                    ])
                    ->join(['s2' => 'store'], 's.store_id = s2.id AND s2.is_sumup_daily = 1', [])
                    ->joinCross(['ad' => 'all_date'])
                    ->joinLeft(['s1' => 'sumup_sales_daily'], "s.store_id = s1.store_id AND s1.date = ad.date", [])
                    ->joinLeft(['b' => 'brand'], 's1.brand_id = b.id', [])
                    ->where('s.staff_id = ?', $staff_id)
                    ->where('s.released_at IS NULL', '')
                    ->where('s.is_leader = ?', 0)
                    ->where('ad.date IN (?)', $date)
                    ->where("DATE_FORMAT(FROM_UNIXTIME(s.joined_at), '%Y-%m-%d') < ?", date('Y-m-d')) // những PG mới được gán shop không cần báo số của ngày hôm trước ngày vào làm
                    ->where('DATE_FORMAT(FROM_UNIXTIME(s.joined_at), \'%Y-%m-%d\') <= ad.date')
                    ->group(['s.store_id', 'ad.date']);

        $history = $db->fetchAll($select);

        return $history;
    }

    public function getListNeedToReport($staff_id, $date)
    {
        $QBrand = new Application_Model_Brand();
        $listBrand = $QBrand->getAll();
        $histories = $this->getHistory($staff_id, $date);

        for ($i = 0; $i < count($histories); $i++) {
            $histories[$i] ['already_do_detail_report_brands'] = explode(',', $histories[$i] ['brands']);
            $histories[$i] ['need_to_do_detail_report_brands'] = array_diff($listBrand, $histories[$i] ['already_do_detail_report_brands']);
            if ($histories[$i] ['need_to_do_detail_report_brands']) {
                $remainDetailReportBrand [] = $histories[$i];
            }
        }

        return $remainDetailReportBrand;

    }


}