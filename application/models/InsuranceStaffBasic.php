<?php
class Application_Model_InsuranceStaffBasic extends Zend_Db_Table_Abstract
{
    protected $_name = 'insurance_staff_basic';
// 1 là tab tăng
// 2 giảm
// 3 điều chỉnh
// 4 chế độ
// 5 sau chế độ
// 6 trong quá khứ
    public function get_by_option($option){
        $db = Zend_Registry::get('db');
//        $db->beginTransaction();
        $select = $db->select()
            ->from(array('i' => 'insurance_staff_basic'), 'i.*')
            ->joinLeft(array('i1'=>'insurance_option'),'i1.id = i.option',array('type' => 'name'))
            ->joinLeft(array('s'=>'staff'),'s.id = i.staff_id',array('staff_code' => 'code','firstname' => 'firstname','lastname' => 'lastname'))
            ->joinLeft(array('t'=>'team'),'t.id = s.title',array('job_title' => 'name'))
            ->joinLeft(array('e'=>'unit_code'),'(e.company_id = s.company_id AND ((s.`id_place_province` = 64 AND e.`is_foreigner` = 1) OR (s.`id_place_province` <> 64 AND e.`is_foreigner` = 0)))',array('unit_code_name' => 'e.unit_code',))
		
            ->where('i.passby = ?', 1)
        ;
        if ($option == 1) {
            $select->where('i.option IN (?)', array(1,2));
        }
        if ($option == 2) {
            $select->where('i.option IN (?)', array(3,4));
        }
        if ($option == 3) {
            $select->where('i.option IN (?)', array(5,6,7));
        }
        if ($option == 4) {
            $select->where('i.option IN (?)', array(8,9,10,11,12,13,14,15));
        }
        if ($option == 5) {
            $select->where('i.option IN (?)', array(16,17));
        }
        if ($option == 6) {
            $select->where('i.option IN (?)', array(18,19));
        }
            $select->order('i.created_at DESC');
            $select->group ( array ('staff_id','option') );
//          echo $select->__toString(); exit;
        return  $db->fetchAll($select);
    }
    
    public function removeDisable($id){
        $db = Zend_Registry::get('db');
        //$db->beginTransaction();
        //get data
        $select = $db->select()
            ->from(array('i' => 'insurance_staff_basic'), 'i.*');
        $select->where('i.id = ?',$id);
        $data = $db->fetchRow($select);
       
        $db->beginTransaction();
        try {
            //del insurance staff basic
            $QInsuranceStaffBasic = new Application_Model_InsuranceStaffBasic();
            $where = $QInsuranceStaffBasic->getAdapter()->quoteInto('id = ?', $id);
            $result = $QInsuranceStaffBasic->delete($where);

            //del insurance staff basic del
            $QInsuranceStaffBasicDel = new Application_Model_InsuranceStaffBasicDel();
            $where_del = array();
            $where_del[] = $QInsuranceStaffBasicDel->getAdapter()->quoteInto('staff_id = ?', $data['staff_id']);
            $where_del[] = $QInsuranceStaffBasicDel->getAdapter()->quoteInto('type = ?', $data['type']);
            $where_del[] = $QInsuranceStaffBasicDel->getAdapter()->quoteInto('`option` = ?', $data['option']);
            $where_del[] = $QInsuranceStaffBasicDel->getAdapter()->quoteInto('time_effective = ?', $data['time_effective']);
            $where_del[] = $QInsuranceStaffBasicDel->getAdapter()->quoteInto('time_alter = ?', $data['time_alter']);
            $where_del[] = $QInsuranceStaffBasicDel->getAdapter()->quoteInto('created_at = ?', $data['created_at']);
            $where_del[] = $QInsuranceStaffBasicDel->getAdapter()->quoteInto('created_by = ?', $data['created_by']);
            $where_del[] = $QInsuranceStaffBasicDel->getAdapter()->quoteInto('passby = ?', $data['passby']);
            $result_del = $QInsuranceStaffBasicDel->delete($where_del);
            $db->commit();
            return [
              'code' =>   1 , 
              'message' => 'Success'
            ];
        } catch (Exception $exc) {
            $db->rollback();
            return [
              'code' =>   -1 , 
              'message' => 'Restore Fail'
            ];
        }
    }
}