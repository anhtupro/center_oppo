<?php
class Application_Model_DestructionDetailsType extends Zend_Db_Table_Abstract
{
    protected $_name = 'destruction_details_type';
    protected $_schema = DATABASE_TRADE;

    public function getDestructionDetailsPart($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.destruction_part_id", 
            "p.quantity", 
            "r.title",
            'destruction_details_id' => 'd.id'
        );

        $select->from(array('p' => DATABASE_TRADE.'.destruction_details_part'), $arrCols);
        $select->joinLeft(array('r' => DATABASE_TRADE.'.destruction_part'), 'r.id = p.destruction_part_id', array());
        $select->joinLeft(array('d' => DATABASE_TRADE.'.destruction_details'), 'd.id = p.destruction_details_id', array());
        $select->where('d.destruction_id = ?', $params['destruction_id']);

        $result = $db->fetchAll($select);

        $data = [];
        foreach($result as $key=>$value){
            $data[$value['destruction_details_id']][] = $value;
        }
        
        return $data;
    }
    
}