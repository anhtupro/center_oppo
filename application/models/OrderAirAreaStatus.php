<?php

class Application_Model_OrderAirAreaStatus extends Zend_Db_Table_Abstract
{
    protected $_name = 'order_air_area_status';
    protected $_schema = DATABASE_TRADE;

    public function getStatus($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['s' => DATABASE_TRADE.'.order_air_area_status'], [
                         's.area_id',
                         's.order_air_stage_id',
                         's.status',
                         'status_name' => 'a.name' 
                     ])
                    ->joinLeft(['a' => DATABASE_TRADE.'.app_status'], 's.status = a.status', [])
                    ->where('s.order_air_stage_id = ?', $params['stage_id']);

        if ($params['type']) {
            $select->where('a.type = ?', $params['type']);
        }

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['area_id']] = $element;
        }

        return $list;
    }

    public function getStatusNameOrderAir($status, $type)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['a' => DATABASE_TRADE.'.app_status'], [
                         'a.name'
                     ])
        ->where('a.type = ?', $type)
        ->where('a.status = ?', $status);
        
        $result = $db->fetchRow($select);

        return $result['name'];
    }
}