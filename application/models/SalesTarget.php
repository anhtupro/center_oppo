<?php
class Application_Model_SalesTarget extends Zend_Db_Table_Abstract
{
    protected $_name = 'sales_target';

    /**
     * @author buu.pham
     * @param  [type] $page   [description]
     * @param  [type] $limit  [description]
     * @param  [type] &$total [description]
     * @param  [type] $params [description]
     * @return [type]         [description]
     */
    public function fetchPagination($page, $limit, &$total, $params)
    {   // nhân viên mới dưới 60 ngày nếu ko đạt target chỉ hiện lên mà ko cần bật nút protect
        // nhân viên vào làm tháng đầu tiên chưa đủ 30 ngày thì tính chỉ tiêu bình quân theo ngày đã làm. 
        // nhân viên từ bộ phận khác chuyển qua thì tính chỉ tiêu bình quân từ ngày chuyển qua đến cuối tháng.
        $db = Zend_Registry::get('db');
        $type = isset($params['type']) ? $params['type'] : null;

        if (!$type) {
            $sub_select = array();
            $target_list = array(My_Staff_Group::PG, My_Staff_Group::SALES);

            foreach ($target_list as $_group)
                $sub_select[] = $this->buildTargetQuery($_group, $params, true);

            $main_select = $db->select()->union($sub_select);
            $select = $db->select()->from(array('p' => $main_select), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.*'))); 
            
        } else {
            $select = $this->buildTargetQuery($type, $params);
        }

        if (isset($params['sort']) && $params['sort']) {
            switch ($params['sort']) {
                case 'name':
                    $sort = " CONCAT(firstname, ' ', lastname) COLLATE utf8_unicode_ci ";
                    break;
                case 'province':
                    $sort = " province COLLATE utf8_unicode_ci ";
                    break;
                default:
                    $sort = $params['sort'];
                    break;
            }

            $sort .= $params['desc'] ? " DESC " : " ASC " ;
            $select->order(new Zend_Db_Expr($sort));
        }
        
        ////
         if (isset($params['paction']) && $params['paction']) {
            if (My_Sale_Target_Action::NO_ACTION == $params['paction'])
                $select->where('p.allow_protect = 1 and p.kill_at is null');
        }

        if (isset($params['export']) && $params['export']){
            //return $select->__toString();
            $result = $db->fetchAll($select);
            return $result;
        }
        
         if(!empty($_GET['dev'])){
            echo "<pre>";
            print_r($select->__toString());
            echo "</pre>";
            exit;
        }

        if ($limit)
            $select->limitPage($page, $limit);
 // echo $select;
        $result = $db->fetchAll($select);

        if ((!isset($params['export']) || !$params['export']) && $limit)
            $total = $db->fetchOne("SELECT FOUND_ROWS()");

        return $result;
    }

    /**
     * @author buu.pham
     * Lấy target theo nhóm nv và ngày search
     * @param  [type] $from  [description]
     * @param  [type] $group [description]
     * @return [type]        [description]
     */
    public function getTarget($from, $group)
    {
        $db = Zend_Registry::get('db');
        $sql = "SELECT s.target
                FROM sales_target AS s
                INNER JOIN 
                (
                	SELECT MAX(a.from_date) AS from_date
                	FROM
                	(	
                		SELECT *
                		FROM
                			sales_target
                		HAVING
                			from_date <= ?
                	) AS a
                ) AS b 
                ON b.from_date = s.from_date
                WHERE s.object_id = ? 
                LIMIT 1";
        $result = $db->query($sql, array($from, $group));
        $row = $result->fetch();

        if ($row && isset($row['target']) && intval($row['target'])) return intval($row['target']);
        return null;
    }

    /**
     * @author buu.pham
     * Build câu query để union khi cần xem tất cả các group
     * @param  [type]  $type                   [description]
     * @param  [type]  $params                 [description]
     * @param  boolean $no_sql_calc_found_rows [description]
     * @return [type]                          [description]
     */
    public function buildTargetQuery($type, $params, $no_sql_calc_found_rows = false)
    {
        $join_cond = ' s.title IN ( ';
        $group = $type;

        switch ($type) {
            case My_Staff_Group::PG:
                $type_id = 'pg_id';
                $join_cond .= PGPB_TITLE .','.CHUYEN_VIEN_BAN_HANG_TITLE.' ) AND ';
                break;
            case My_Staff_Group::SALES:
                $type_id = 'sale_id';
                $join_cond .= SALES_TITLE . ' ) AND ';
                break;
            case My_Staff_Group::LEADER:
                $type_id = 'leader_id';
                $join_cond .= LEADER_TITLE . ') AND ';
                break;
            default:
                // throw new Exception("Invalid type", 1);
                break;
        }

        if (!isset($params['from']) && !date_create_from_format('d/m/Y', $params['from']))
            throw new Exception("Invalid From date", 3);

        $from = date_create_from_format('d/m/Y', $params['from'])->format('Y-m-d');

        if (!isset($params['to']) && !date_create_from_format('d/m/Y', $params['to']))
            throw new Exception("Invalid To date", 3);

        $to = date_create_from_format('d/m/Y', $params['to'])->format('Y-m-d');

        $target = $this->getTarget($from, $group);

        if (!$target) throw new Exception("Target not set", 2);

        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('s' => 'staff'), array(
                new Zend_Db_Expr((!$no_sql_calc_found_rows ? 'SQL_CALC_FOUND_ROWS' : '').' s.id'),
                's.regional_market',
                's.joined_at',
                'diff' => new Zend_Db_Expr(sprintf("DATEDIFF('%s', s.joined_at)", $to)),
                'target' => new Zend_Db_Expr($target),
                'target_first' => new Zend_Db_Expr(sprintf("%d/(DATEDIFF('%s', '%s'))*(DATEDIFF('%s', s.joined_at))",$target,$to,$from,$to) ) ,  
                's.firstname',
                's.lastname',
                's.code',
                's.email',
                's.title',
                'type' => new Zend_Db_Expr($type),
            ))
            ->join(array('r' => 'regional_market'), 'r.id=s.regional_market', array('province' => 'r.name'))
            ->join(array('a' => 'area'), 'a.id=r.area_id', array('area' => 'a.name'))
            ->joinLeft(array('i' => 'imei_kpi'), $join_cond.' s.id=i.'.$type_id, array('i.' . $type_id, 'total' => 'COUNT(DISTINCT i.imei_sn)'))
            ->joinLeft(array('x' => new Zend_Db_Expr('( SELECT a.id, COUNT(b.staff_id) as protect_time_target, b.staff_id
									 FROM staff a
									 LEFT JOIN sales_target_protect_log b ON a.id = b.staff_id 
                                     WHERE b.year_target = '.$params['year_target'].'
									 GROUP BY b.staff_id)')), 's.id=x.staff_id', array('protect_time_target' => 'x.protect_time_target') )
                                     
            ->joinLeft(array('g' => new Zend_Db_Expr(sprintf(" (SELECT * FROM v_staff_transfer
                                                        WHERE  from_date > '".$from."' AND from_date < '".$to." ' 
                                            			ORDER BY from_date DESC )"))), $join_cond." s.id = g.staff_id ", array( 'date_change_title' => 'g.from_date',
                                                                                                                                'target_change_title'=>new Zend_Db_Expr(sprintf("%d/(DATEDIFF('%s', '%s'))*(DATEDIFF('%s', g.from_date))",$target,$to,$from,$to) )) )
                                                        
            ->joinLeft(
                array('p' => 'sales_target_protect'),
                's.id=p.staff_id',
                array(
                    'protect_id'    => 'p.id',
                   //'protect_time'  => new Zend_Db_Expr('IFNULL(p.protect_time, 0)'),
                    'allow_protect' => new Zend_Db_Expr(sprintf(
                        "fn_allow_protect(s.id, %d, s.joined_at, '%s', '%s', COUNT(DISTINCT i.imei_sn))",
                        $type,
                        $from,
                        $to
                    )),
                    'kill_at',
                )
            );
            
        if (isset($params['area']) && $params['area']) {
            if (is_array($params['area']) && count($params['area']))
                    $select->where('r.area_id IN (?)', $params['area']);
            elseif (is_numeric($params['area']))
                    $select->where('r.area_id = ?', intval($params['area']));
            else
                $select->where('1=0', 1);
        }
        
        if (isset($params['province']) && $params['province']) {
            if (is_array($params['province']) && count($params['province']))
                    $select->where('r.id IN (?)', $params['province']);
            elseif (is_numeric($params['province']))
                    $select->where('r.id = ?', intval($params['province']));
            else
                $select->where('1=0', 1);
        }

        if (isset($params['area_list']) && $params['area_list']) {
            if (is_array($params['area_list']) && count($params['area_list']))
                    $select->where('r.area_id IN (?)', $params['area_list']);
            elseif (is_numeric($params['area_list']))
                    $select->where('r.area_id = ?', intval($params['area_list']));
            else
                $select->where('1=0', 1);
        }
            
            // search ở check_box:  protect -  kill - No action
        if (isset($params['paction']) && $params['paction']) {
            if (My_Sale_Target_Action::KILL == $params['paction'])
                $select->where('p.kill_at IS NOT NULL AND p.kill_at <> 0');

            if (My_Sale_Target_Action::PROTECT == $params['paction'])
                $select->where('x.protect_time_target IS NOT NULL AND x.protect_time_target <> 0');
            
            // đã chèn thêm ở fetchPagination
               // if (My_Sale_Target_Action::NO_ACTION == $params['paction'])
                //    $select->where(" (' p.kill_at is null')");
        }

        if (isset($params['email']) && $params['email']) {
            $select->where('s.email LIKE ?', str_replace(EMAIL_SUFFIX, '', trim($params['email'])).EMAIL_SUFFIX);
        }

        if (isset($params['name']) && $params['name']) {
            $select->where("CONCAT(TRIM(BOTH FROM s.firstname), ' ', TRIM(BOTH FROM s.lastname)) LIKE ?", '%'.trim($params['name']).'%');
        }

        $select->where("( 
                            ( `g`.`from_date` is NULL AND ( DATE(i.timing_date) >= '".$from."' ) AND ( DATE(i.timing_date) <= '".$to."' ) )	
                		OR 
          		            ( `g`.`from_date` is NOT NULL AND ( DATE(i.timing_date) >= `g`.`from_date` ) AND ( DATE(i.timing_date) <= '".$to."' )  )
                		)");
                        
        $select->where('s.off_date IS NULL OR s.off_date = 0', 1);
        $select->where('Datediff("'.$to.'", s.joined_at) >= 60');

        $select->group($type_id);
        $select->having (new Zend_Db_Expr(sprintf("	  ( date_change_title is NULL AND (( total < %d AND ( DATE(`s`.`joined_at`) <= '%s' ) ) OR ( total < target_first AND ( DATE(`s`.`joined_at`) >= '%s' ) )) )
     		                                         OR
                                              		  ( date_change_title is NOT NULL and  total < target_change_title  )",$target,$from,$from)));
        return $select;
    }
}
