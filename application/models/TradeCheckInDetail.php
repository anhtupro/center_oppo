<?php

class Application_Model_TradeCheckInDetail extends Zend_Db_Table_Abstract
{
    protected $_name = 'check_in_detail';
    protected $_schema = DATABASE_TRADE;

    public function get($checkInId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['d' => DATABASE_TRADE.'.check_in_detail'], [
                         'd.*'
                     ])
        ->where('d.check_in_id = ?', $checkInId);

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getStaff($checkInId)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['c' => DATABASE_TRADE.'.check_in'], [
                         'staff_name' => "CONCAT(st.firstname, ' ', st.lastname)",
                         'check_in_date' => 'c.created_at'
                     ])
                   ->joinLeft(['st' => 'staff'], 'c.staff_id = st.id', [])
                   ->where('c.id = ?', $checkInId);

        $result = $db->fetchRow($select);

        return $result;
    }
}