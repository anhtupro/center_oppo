<?php

class Application_Model_InsuranceStaff extends Zend_Db_Table_Abstract {

    protected $_name = 'insurance_staff';

    public function fetchPagination($page, $limit, &$total, $params) {
        $db     = Zend_Registry::get('db');
        $cols   = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.id'),
            'staff_id'        => 'a.id',
            'a.dob',
            'a.gender',
            'a.ID_number',
            'staff_code'      => 'a.code',
            'staff_name'      => 'CONCAT(a.firstname," ",a.lastname)',
            'unit_code_name'  => 'e.unit_code',
            'company_name'    => 'd.name',
            'regional_market' => 'b.name',
            'department_name' => 'h.name',
            'team_name'       => 'g.name',
            'title_name'      => 'CONCAT(f.name_vn," - ",IFNULL(rm.name,rmd.name)," - ",b.name," - ",m.name)',
            'a.insurance_number',
            'a.have_book',
            'a.close_book',
            'a.return_ins_card_time',
            'a.return_ins_book_time',
            'a.medical_card_number',
            'a.take_card_from_ins',
            'a.take_card_from_staff',
            'a.delivery_card_ins',
            'note'            => 'a.note_ins',
            'increase'        => 'CASE WHEN DAY(i.time_alter) > 15 THEN DATE_ADD(i.time_alter, INTERVAL 1 MONTH) ELSE i.time_alter END',
            'decrease'        => 'CASE WHEN DAY(j.time_alter) > 15 THEN DATE_ADD(j.time_alter, INTERVAL 1 MONTH) ELSE j.time_alter END',
            'status'          => 'a.status',
            'a.code_BHYT_ins',
            'a.have_BHYT_time',
            'a.qdnv',
            'a.hospital_id',
            'hospital_name'   => 'ho.name',
            'hospital_code'   => 'ho.code',
            'province_code'   => 'ho.province_code',
            'province_name'   => 'pr.name',
            'log_created_at'  => 'MAX(ili.created_at)'
        );
        $select = $db->select()
                ->from(array('a' => 'staff'), $cols)
                ->join(array('b' => 'regional_market'), 'b.id = a.regional_market', array())
                ->joinLeft(array('rm' => 'regional_market'), 'rm.id = a.district', array())
                ->joinLeft(array('rmd' => 'regional_market'), 'rmd.parent = b.id and rmd.default_contract_district=1', array())
                ->join(array('ist' => 'insurance_staff_TM_now'), 'ist.staff_id = a.id', array())
                ->join(array('i' => 'insurance_staff'), 'i.staff_id = a.id  AND i.`type` = 1 AND i.`option` in (1, 2) AND i.del = 0 AND i.locked = 1 AND i.time_alter = ist.time_alter', array())
                ->joinLeft(array('j' => 'insurance_staff'), 'a.id = j.staff_id AND j.`type` = 2 AND j.`option` = 3 AND j.del = 0 AND j.locked = 1', array())
                ->joinLeft(array('c' => 'insurance_info'), 'c.staff_id = a.id', array())
                ->joinLeft(array('d' => 'company'), 'd.id = a.company_id', array())
                ->joinLeft(array('e' => 'unit_code'), '(e.company_id = i.company_id AND ((a.`id_place_province` in (64, 65)  AND e.`is_foreigner` = 1) OR (a.`id_place_province` not in (64, 65) AND e.`is_foreigner` = 0)))', array())
                ->joinLeft(array('f' => 'team'), 'f.id = a.title', array())
                ->joinLeft(array('g' => 'team'), 'g.id = a.team', array())
                ->joinLeft(array('h' => 'team'), 'h.id = a.department', array())
                ->joinLeft(array('m' => 'area_nationality'), 'm.id = IFNULL(rm.area_national_id,rmd.area_national_id)', array())
                ->joinLeft(array('ho' => 'hospital'), 'ho.id = a.hospital_id', array())
                ->joinLeft(array('pr' => 'province'), 'pr.id = ho.province_id', array())
                ->joinLeft(array('ili' => 'insurance_log_info'), 'a.id = ili.staff_id', array())
                ->group('a.id')
        ;
        if (isset($params['status']) AND ( $params['status'] >= 0)) {

            $select->where('a.`status` = ?', $params['status']);
        }

        if (isset($params['name']) AND $params['name']) {
            $select->where('CONCAT(a.firstname," ",a.lastname) LIKE ?', '%' . $params['name'] . '%');
        }

        if (isset($params['code']) AND $params['code']) {
            $select->where('a.code LIKE ?', '%' . $params['code'] . '%');
        }

        if (isset($params['insurance_number']) AND $params['insurance_number']) {
            $select->where('a.insurance_number LIKE ?', '%' . $params['insurance_number'] . '%');
        }

        if (isset($params['company_id']) AND $params['company_id']) {
            $select->where('a.company_id = ?', $params['company_id']);
        }

        if (isset($params['unit_code_id']) AND $params['unit_code_id']) {
            $select->where('e.id = ?', $params['unit_code_id']);
        }

        if (isset($params['regional_market']) AND $params['regional_market']) {
            $select->where('a.regional_market = ?', $params['regional_market']);
        }

        /* if(isset($params['search_by']) AND $params['search_by'] == 8807){
          $select->where('a.team in  (75, 147, 397)',array(75, 147, 397));
          }
         */
        if (isset($limit) AND $limit AND ! $params['export']) {
            $select->limitPage($page, $limit);
        }
        if ($_GET['dev'] == 1) {
            echo $select->__toString();
            die;
        }
        $result = $db->fetchAll($select);
        $total  = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public function fetchPaginationReport($page, $limit, &$total, $params) {
        $start = '2015-07-01';
        $from  = '';
        $to    = '';
        if (isset($params['month']) AND $params['month']) {
            $month     = My_Date::normal_to_mysql($params['month']);
            $to        = date('Y-m-15', strtotime($month));
            $limitDate = date('Y-m-20', strtotime($month));
            $interval  = new DateInterval('P1M');
            $date      = new DateTime($month);
            $date->sub($interval);
            $from      = $date->format('Y-m-16');
        }

        $db     = Zend_Registry::get('db');
        $cols   = array(
            //new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
            'p.*',
            'staff_name'      => 'CONCAT(s.firstname," ",s.lastname)',
            'unit_code_name'  => 'u.unit_code',
            'company_name'    => 'c.name',
            'option_name'     => 'o.name',
            'staff_code'      => 's.code',
            'team_name'       => 't.name',
            'title_name'      => 'CONCAT(tt.name_vn," - ",rm.name_insurance," - ",pr.name, " - ",ai.name)',
            'department_name' => 'd.name',
            'kv'              => 'ai.name',
            'locked_by_name'  => '(CONCAT(st.firstname," ",st.lastname))',
            'current_month'   => new Zend_Db_Expr('1')
        );
        $select = $db->select()
                ->from(array('p' => 'insurance_staff'), $cols)
                ->join(array('s' => 'staff'), 's.id = p.staff_id', array())
                ->joinLeft(array('st_c' => 'staff_contract'), 's.id = st_c.staff_id AND st_c.`status` = 2 AND st_c.`is_expired` = 0 AND st_c.`is_disable` = 0 AND p.`time_effective` = st_c.`from_date`', array())
                ->joinLeft(array('c' => 'company'), 'c.id = p.company_id', array())
                ->joinLeft(array('tt' => 'team'), 'tt.id = p.title', array())
                ->joinLeft(array('t' => 'team'), 't.id = tt.parent_id', array())
                ->joinLeft(array('d' => 'team'), 'd.id = t.parent_id', array())
                ->joinLeft(array('u' => 'unit_code'), '(u.company_id = p.unit_code_id AND ((s.`id_place_province` = 64 AND u.`is_foreigner` = 1) OR (s.`id_place_province` <> 64 AND u.`is_foreigner` = 0)))', array())
                ->joinLeft(array('o' => 'insurance_option'), 'o.id = p.`option` ', array())
                ->joinLeft(array('r' => 'regional_market'), 'r.id = IFNULL(p.regional_market,IFNULL(st_c.regional_market,s.regional_market))', array())
                ->joinLeft(array('pr' => 'province'), 'r.province_id = pr.id', array())
                ->joinLeft(array('rm' => 'regional_market'), 'rm.id = IFNULL(p.district,IFNULL(st_c.district_id,s.district))', array())
                ->joinLeft(array('ai' => 'area_nationality'), 'ai.id = rm.area_national_id', array())
                ->joinLeft(array('st' => 'staff'), 'st.id = p.locked_by', array())
                //->order(array('type ASC','option ASC','time_alter ASC'))
                ->where('p.del = ?', 0)
        ;

        $cols['current_month'] = new Zend_Db_Expr('0');

        if (isset($params['code']) AND $params['code']) {
            $select->where('s.code = ?', $params['code']);
//             $select2->where('s.code = ?',$params['code']);
        }

        if (isset($params['ins_number']) AND $params['ins_number']) {
            $select->where('s.insurance_number = ?', $params['ins_number']);
//             $select2->where('s.insurance_number = ?',$params['ins_number']);
        }

        if (isset($params['s_qdnv']) AND $params['s_qdnv']) {
            $select->where('p.qdnv = ?', $params['s_qdnv']);
//             $select2->where('p.qdnv = ?',$params['s_qdnv']);
        }


        if (isset($params['unit_code_id']) AND $params['unit_code_id']) {
            $select->where('p.unit_code_id = ?', $params['unit_code_id']);
//             $select2->where('p.unit_code_id = ?',$params['unit_code_id']);
        }

        if (isset($params['name']) AND $params['name']) {
            $select->where('CONCAT(s.firstname," ",s.lastname) LIKE ?', '%' . $params['name'] . '%');
//             $select2->where('CONCAT(s.firstname," ",s.lastname) LIKE ?','%'.$params['name'].'%');
        }

        if (isset($params['date_ins']) AND $params['date_ins']) {
            $select->where('p.time_alter = ?', trim(My_Date::normal_to_mysql($params['date_ins'])));
        }

        if (isset($params['date_approved']) AND $params['date_approved']) {
            $select->where('DATE(p.locked_at) = ?', trim(My_Date::normal_to_mysql($params['date_approved'])));
        }

        if (isset($params['type']) AND $params['type']) {
            if ($params['type'] == 1) {
                $select->where('p.`option` IN (1,2,16,17,19)');
//                 $select2->where('`option` IN (1,2)');
            } elseif ($params['type'] == 2) {
                $select->where('p.`option` IN (3,4,8,9,10,11,12,13,14,15,18)');
//                 $select2->where('`option` IN (3,4,5,6,9,10)');
            } elseif ($params['type'] == 3) {
                $select->where('p.`option` IN (5,6,7)');
//                 $select2->where('`option` IN (7,8)');
            }
        }

        if (isset($params['option']) AND $params['option'] > 0) {

            $select->where('p.`option` = ?', $params['option']);
        }

        if (isset($params['locked']) AND $params['locked'] >= 0) {
            $locked = $params['locked'];
            if ($locked == 1) {
                $select->where('p.locked = ?', $params['locked']);
            } else {
                $select->where('p.locked IN (0,2)', $params['locked']);
            }

            //$select->where('p.locked = ?',$params['locked']);
        }

        if (isset($params['month']) AND $params['month']) {
            $select->where("p.time_alter BETWEEN '$from' AND '$to'");
            // $select->where('p.time_created_at IS NULL OR DATE(p.time_created_at) <= ?',$limitDate);
        }

//         $selectUnion = $db->select()
//         ->union(array($select,$select2));
        $colsAll = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS a.*'),
        );

        $selectAll = $db->select()
                ->from(array('a' => new Zend_Db_Expr("($select)")), $colsAll)
                ->order(array('locked_at DESC', 'type ASC', 'option ASC', 'time_alter ASC', 'qdnv'))
        ;

        if ($limit) {
            $selectAll->limitPage($page, $limit);
        }

        if (isset($params['before']) AND $params['before'] == 1 AND isset($params['month']) AND $params['month']) {
            
        } else {
            $selectAll->where('current_month = ?', 1);
        }

//        echo $selectAll->__toString();

        $result = $db->fetchAll($selectAll);
        $total  = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public function showHistory($staff_id) {
        $db   = Zend_Registry::get('db');
        $cols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
            'p.*',
            'staff_name'      => 'CONCAT(s.firstname," ",s.lastname)',
            'unit_code_name'  => 'u.unit_code',
            'company_name'    => 'c.name',
            'option_name'     => 'o.name',
            'staff_code'      => 's.code',
            'team_name'       => 't.name',
            'title_name'      => 'CONCAT(tt.name_vn," - ",pr.name," - ",ai.name)',
            'department_name' => 'd.name',
            'kv'              => 'ai.name',
//             'type_name'       => '(CASE WHEN p.type = 1 THEN "TĂNG" WHEN p.type = 2 THEN "GIẢM" ELSE "ĐIỀU CHỈNH" END )',
            'type_name'       => 'o.symbol',
            'type_desc'       => 'o.desc',
            'month'           => '(CASE WHEN DAY(p.time_alter) <= 15 THEN p.time_alter ELSE DATE_ADD(DATE_FORMAT(p.time_alter,"%Y-%m-01"),INTERVAL 1 MONTH) END )'
        );

        $select = $db->select()
                ->from(array('p' => 'insurance_staff'), $cols)
                ->join(array('s' => 'staff'), 's.id = p.staff_id', array())
                ->join(array('c' => 'company'), 'c.id = p.company_id', array())
                ->join(array('tt' => 'team'), 'tt.id = p.title', array())
                ->join(array('t' => 'team'), 't.id = tt.parent_id', array())
                ->join(array('d' => 'team'), 'd.id = t.parent_id', array())
                // ->joinLeft(array('u'=>'unit_code'),'u.id = p.unit_code_id',array())
                ->joinLeft(array('u' => 'unit_code'), '(u.company_id = p.unit_code_id AND ((s.`id_place_province` = 64 AND u.`is_foreigner` = 1) OR (s.`id_place_province` <> 64 AND u.`is_foreigner` = 0)))', array())
                ->joinLeft(array('o' => 'insurance_option'), 'o.id = p.`option`', array())
                ->join(array('r' => 'regional_market'), 'r.id = s.regional_market', array())
                ->joinLeft(array('pr' => 'province'), 'r.province_id = pr.id', array())
                ->joinLeft(array('ai' => 'area_nationality'), 'ai.id = pr.area_national_id', array())
                ->order(array('time_effective ASC', 'type ASC', 'option ASC', 'time_alter ASC'))
                ->where('p.staff_id = ?', $staff_id)
                ->where('p.locked = ?', 1)
                ->where('p.del = ?', 0)
        ;
        // echo $select;
        // exit;
        $result = $db->fetchAll($select);
        return $result;
    }

    public function showHistoryStaff($staff_id) {
        $db   = Zend_Registry::get('db');
        $cols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
            'p.*',
            'staff_name'      => 'CONCAT(s.firstname," ",s.lastname)',
            'unit_code_name'  => 'u.unit_code',
            'company_name'    => 'c.name',
            'option_name'     => 'o.name',
            'staff_code'      => 's.code',
            'team_name'       => 't.name',
            'title_name'      => 'CONCAT(tt.name_vn," - ",pr.name," - ",ai.name)',
            'department_name' => 'd.name',
            'kv'              => 'ai.name',
//             'type_name'       => '(CASE WHEN p.type = 1 THEN "TĂNG" WHEN p.type = 2 THEN "GIẢM" ELSE "ĐIỀU CHỈNH" END )',
            'type_name'       => 'o.desc',
            'month'           => '(CASE WHEN DAY(p.time_alter) <= 15 THEN p.time_alter ELSE DATE_ADD(DATE_FORMAT(p.time_alter,"%Y-%m-01"),INTERVAL 1 MONTH) END )'
        );

        $select = $db->select()
                ->from(array('p' => 'insurance_staff'), $cols)
                ->join(array('s' => 'staff'), 's.id = p.staff_id', array())
                ->join(array('c' => 'company'), 'c.id = p.company_id', array())
                ->join(array('tt' => 'team'), 'tt.id = p.title', array())
                ->join(array('t' => 'team'), 't.id = tt.parent_id', array())
                ->join(array('d' => 'team'), 'd.id = t.parent_id', array())
                ->joinLeft(array('u' => 'unit_code'), 'u.id = p.unit_code_id', array())
                ->joinLeft(array('o' => 'insurance_option'), 'o.id = p.`option`', array())
                ->join(array('r' => 'regional_market'), 'r.id = s.regional_market', array())
                ->joinLeft(array('pr' => 'province'), 'r.province_id = pr.id', array())
                ->joinLeft(array('ai' => 'area_nationality'), 'ai.id = pr.area_national_id', array())
                ->order(array('time_effective ASC', 'type ASC', 'option ASC', 'time_alter ASC'))
                ->where('p.staff_id = ?', $staff_id)
                ->where('p.locked = ?', 1)
                ->where('p.del = ?', 0)
        ;
        // echo $select;
        // exit;
        $result = $db->fetchAll($select);
        return $result;
    }

}
