<?php
class Application_Model_RequestChannel extends Zend_Db_Table_Abstract
{
    protected $_name = 'request_channel';
    public function getChannel()
    {
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id',
            'name'        => 'p.name'
        );
        $select->from(array('p'=> 'request_channel'), $arrCols);
        $result = $db->fetchAll($select);
        return $result;
    }

}