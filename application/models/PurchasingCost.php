<?php
class Application_Model_PurchasingCost extends Zend_Db_Table_Abstract
{
    protected $_name = 'purchasing_cost';


    function getinfoCOST($params){

        $db = Zend_Registry::get('db');

        $col = array(
                'p.*',  
                'created_by_name'    => "CONCAT(s.firstname, ' ', s.lastname)",
                'area_created_by' => 'ar.name',
                'department_created_by' => 't.name',
                'confirm_by_name'    => "CONCAT(st.firstname, ' ', st.lastname)",
            
        );

        $select = $db->select()->from(array('p' => $this->_name), $col);

        $select->joinleft(array('s'=>'staff'), 's.id = p.created_by', array());              
        $select->joinleft(array('st'=>'staff'), 'st.id = p.confirm_by', array());
        $select->joinleft(array('rm'=>'regional_market'), 's.regional_market = rm.id', array());
        $select->joinleft(array('ar'=>'area'), 'rm.area_id = ar.id', array());
        $select->joinleft(array('t'=>'team'), 't.id = s.department', array());    
        

        if(!empty($params['sn'])){
            $select->where('sn = ?', $params['sn']);
        }

        $result = $db->fetchRow($select);

        return $result;
    }
    
    
    function listCOST($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));
        
        $select->joinleft(array('s'=>'staff'), 's.id = p.created_by', array('fullname' => "CONCAT(s.firstname, ' ',s.lastname)"));
        $select->joinleft(array('a'=>'area'), 'a.id = p.area_id', array('area_name' => "a.name"));
        
        if(!empty($params['sn'])){
            $select->where('sn = ?', $params['sn']);
        }
        if(!empty($params['name'])){
            $select->where('cost_name LIKE ?', '%'.$params['name'].'%');
        }
        if(!empty($params['area_id'])){
            $select->where('p.area_id IN (?)', $params['area_id']);
        }
        if(!empty($params['remark'])){
            $select->where('remark LIKE ?', '%'.$params['remark'].'%');
        }
        if(!empty($params['created_at_from'])){
            $select->where('DATE(p.created_at) >= ?', $params['created_at_from']);
        }
        if(!empty($params['created_at_to'])){
            $select->where('DATE(p.created_at) <= ?', $params['created_at_to']);
        }
 
        // phân quyền
        if(!empty($params['only_me'])){
            $select->where('p.created_by = ?', $params['only_me']);
        }        
        if(!empty($params['area_access'])){
            $select->where('p.`area_id` IN (?)', $params['area_access']);
        }
        if(!empty($params['showroom_access'])){
            $select->where('p.`showroom_id` IN (?)', $params['showroom_access']);
        }        
        if(!empty($params['department_access'])){
            $select->where('p.`department_id` IN (?)', $params['department_access']);
        }
        
        
        $select->order('p.created_at DESC');
        if($limit){
            $select->limitPage($page, $limit);
        }
        // $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        // if($userStorage->id == 4617){
        //     echo $select; exit;
        // }
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
    
    
    function reportCOST($params){
        
        $db = Zend_Registry::get('db');
        $select_table_temp = "(
                                SELECT b.sn, b.cost_name, b.delivery_date, b.remark, b.`status`,
                                pc.`name` as product, sup.title as supplier, 
                                a.quantity, a.price, a.vat, a.total_price, table_total.total_cost,                                
				a.invoice_number, a.invoice_date, a.note,
                                ar.`name` as area, dep.`name` as department,
                                CONCAT(s.firstname,' ',s.lastname) AS created_by_name, b.created_at, 
                                CONCAT(s2.firstname,' ',s2.lastname) AS confirm_by_name, b.confirm_at,
                                b.created_by, b.department_id, b.area_id, b.showroom_id
                                FROM `purchasing_cost_details` a
                                JOIN purchasing_cost b ON a.pc_id = b.id 
                                LEFT JOIN staff s ON b.created_by = s.id
                                LEFT JOIN staff s2 ON b.confirm_by = s2.id
                                LEFT JOIN area ar ON ar.id = b.area_id
                                LEFT JOIN team dep ON b.department_id = dep.id
                                LEFT JOIN pmodel_code pc ON a.product_id = pc.`id`
                                LEFT JOIN supplier sup ON sup.id = a.supplier_id
                                LEFT JOIN (	SELECT a.id, SUM(total_price) AS total_cost
						FROM `purchasing_cost_details` a
						GROUP BY a.pc_id ) table_total ON a.id = table_total.id
                            )";
        
        $select = $db->select()
        ->from(array('p' => new Zend_Db_Expr($select_table_temp)),
            array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.sn'), 'p.*'));
        
        if(!empty($params['sn'])){
            $select->where('p.sn = ?', $params['sn']);
        }
        if(!empty($params['name'])){
            $select->where('p.cost_name LIKE ?', '%'.$params['name'].'%');
        }
        if(!empty($params['area_id'])){
            $select->where('p.area_id IN (?)', $params['area_id']);
        }
        if(!empty($params['remark'])){
            $select->where('p.remark LIKE ?', '%'.$params['remark'].'%');
        }
        if(!empty($params['created_at_from'])){
            $select->where('DATE(p.created_at) >= ?', $params['created_at_from']);
        }
        if(!empty($params['created_at_to'])){
            $select->where('DATE(p.created_at) <= ?', $params['created_at_to']);
        }
        
        // phân quyền
        if(!empty($params['only_me'])){
            $select->where('p.created_by = ?', $params['only_me']);
        }        
        if(!empty($params['area_access'])){
            $select->where('p.`area_id` IN (?)', $params['area_access']);
        }
        if(!empty($params['showroom_access'])){
            $select->where('p.`showroom_id` IN (?)', $params['showroom_access']);
        }        
        if(!empty($params['department_access'])){
            $select->where('p.`department_id` IN (?)', $params['department_access']);
        }        
        
        $select->order('p.created_at DESC');
        $result = $db->fetchAll($select);
        return $result;
    }
    
}