<?php
class Application_Model_Contract extends Zend_Db_Table_Abstract
{	

    protected $_name    = 'asm_contract';
    protected $_primart = 'id';
  	public function listAll(){
    	return $this->fetchAll()->toArray();
  	}
  	public function findPrimaykey($id){
    	return $this->fetchRow($this->select()->where('id = ?',$id));
  	}
    public function getParentContract($staff_id){
      $db                 = Zend_Registry::get('db');
      $sql                = 
      'SELECT ac.* , t.`name`, ct.`name` as name_contract
      FROM asm_contract AS ac 
      INNER JOIN team AS t
      ON ac.title         = t.id
      INNER JOIN contract_term as ct
      ON ac.new_contract  = ct.id
      WHERE ac.print_type = 1 
      AND ac.staff_id     = '.$staff_id;
      $stmt               = $db->query($sql);
      $list               = $stmt->fetchAll();
      $stmt->closeCursor();
      return $list;
    }
  	public function listNewContract(){
        $db   = Zend_Registry::get('db');
        $sql  = 'select * from contract_term';
        $stmt = $db->query($sql);
        $list = $stmt->fetchAll();
        $stmt->closeCursor();
        return $list;
  	}
  	public function listTeam(){
        $db   = Zend_Registry::get('db');
        $sql  = 'select * from team';
        $stmt = $db->query($sql);
        $list = $stmt->fetchAll();
        $stmt->closeCursor();
        return $list;
  	}
    public function listCompany(){
        $db   = Zend_Registry::get('db');
        $sql  = 'select * from company';
        $stmt = $db->query($sql);
        $list = $stmt->fetchAll();
        $stmt->closeCursor();
        return $list;
    }
}
