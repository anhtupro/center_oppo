<?php
class Application_Model_MiniGameReward extends Zend_Db_Table_Abstract
{
	protected $_name = 'mini_game_reward';

    public function getRemainingItems($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'r.id', 'r.reward', 'r.limit', 'total'=>'COUNT(l.id)', 'remain'=>'(r.limit - COUNT(l.id))'
        );
        $select->from(array('r' => $this->_name), $arrCols);
        $select->joinleft(array('l' => 'mini_game_reward_log'), 'r.id = l.reward', array());
        $select->where('r.round = ?', $params['round']);
        $select->group('r.id');
        $result = $db->fetchAll($select);

        return $result;
    }

    public function checkRemainingItems($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
             'r.id', 'r.reward', 'r.limit', 'total'=>'COUNT(l.id)', 'remain'=>'(r.limit - COUNT(l.id))'
        );
        $select->from(array('r' => $this->_name), $arrCols);
        $select->joinleft(array('l' => 'mini_game_reward_log'), 'r.id = l.reward', array());
        $select->where('r.round = ?', $params['round']);
        $select->where('r.id = ?', $params['id']);

        $result = $db->fetchRow($select);

        return $result;
    }
}
