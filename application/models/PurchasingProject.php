<?php
class Application_Model_PurchasingProject extends Zend_Db_Table_Abstract
{
	protected $_name = 'purchasing_project';

        public function fetchPagination($page, $limit, &$total, $params){
            $db = Zend_Registry::get('db');
            $cols = array(
                new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
                'p.id','p.name','p.remark'
            );
            $select = $db->select()
                ->from(array('p'=>$this->_name),$cols);

            $select->where('p.del =  ?',0);
            if ($limit)
                $select->limitPage($page, $limit);
            $result = $db->fetchAll($select);
            $total = $db->fetchOne("select FOUND_ROWS()");
            return $result;

        }
        function get_cache(){
            $cache      = Zend_Registry::get('cache');
            $result     = $cache->load($this->_name.'_cache');

            if ($result === false) {

                $db = Zend_Registry::get('db');
                $select = $db->select()
                        ->from(array('p' => $this->_name), array('p.*'));

                $select->where('p.del IS NULL OR p.del = ?', 0);
                $select->order(new Zend_Db_Expr('p.`name` COLLATE utf8_unicode_ci'));
                $data = $db->fetchAll($select);

                $result = array();
                if ($data){
                    foreach ($data as $item){
                        $result[$item['id']] = $item['name'];
                    }
                }

                $cache->save($result, $this->_name.'_cache', array(), null);
            }
            return $result;
        }
        

        function get_cache_all(){
            $cache      = Zend_Registry::get('cache');
            $result     = $cache->load($this->_name.'_cache_all');

            if ($result === false) {

                $db = Zend_Registry::get('db');
                $select = $db->select()
                        ->from(array('p' => $this->_name), array('p.*')); 

                $data = $db->fetchAll($select);
                $result = array(); 

                if ($data){
                    foreach ($data as $k => $item){
                        $result[$item['id']] = $item['name'];                    
                    }
                }

                $cache->save($result, $this->_name.'_cache_all', array(), null);
            }
            return $result;
        }
    
}                                                      
