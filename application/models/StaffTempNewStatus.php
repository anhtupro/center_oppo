<?php

class Application_Model_StaffTempNewStatus extends Zend_Db_Table_Abstract
{
    protected $_name = 'staff_temp_new_status';

    public function getApproveType($title)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['s' => 'staff_temp_new_status'], [
                        's.approve_type'
                     ])
        ->where('s.title = ?', $title);

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [] = $element['approve_type'];
        }
        return $list;
    }

    public function getTitle($approve_type)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['s' => 'staff_temp_new_status'], [
                         's.*'
                     ])
        ->where('s.approve_type = ?', $approve_type);

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $listTitle [] = $element['title'];
    }

        return $listTitle? $listTitle : false;
    }
}