<?php
class Application_Model_ContractorCategory extends Zend_Db_Table_Abstract
{
	protected $_name = 'contractor_category';

	protected $_schema = DATABASE_TRADE;

	public function getContructorCategory($params) {
            $db = Zend_Registry::get("db");
            $select = $db->select();

            $arrCols = array(
                "p.campaign_id", 
                "p.area_id", 
                "p.category_id", 
                "c.contractor_id", 
                "total_quantity" => "SUM(p.quantity)", 
                "c.price", 
                "total_price" => "(SUM(p.quantity)*c.price)", 
                "category_name" => "cat.name",
                "contructor_name" => "con.name"
            );

            $select->from(array('p' => DATABASE_TRADE.'.campaign_area'), $arrCols);
            $select->joinLeft(array('c' => DATABASE_TRADE.'.contractor_category'), 'c.campaign_id = p.campaign_id AND c.category_id = p.category_id AND c.area_id = p.area_id', array());
            $select->joinLeft(array('cat' => DATABASE_TRADE.'.category'), 'cat.id = p.category_id', array());
            $select->joinLeft(array('con' => DATABASE_TRADE.'.contructors'), 'con.id = c.contractor_id', array());
            
            $select->where('p.campaign_id = ?', $params['campaign_id']);
            $select->group(['c.contractor_id', 'c.category_id']);
            
            $result = $db->fetchAll($select);

            return $result;
        }


}