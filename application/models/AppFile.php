<?php
class Application_Model_AppFile extends Zend_Db_Table_Abstract
{
    protected $_name = 'app_file';
    protected $_schema = DATABASE_TRADE;
    public function GetListDestruction($params){

        $db = Zend_Registry::get('db');
        if (!empty($params['id']) ){
            $stmt = $db->prepare('
                SELECT id,file_name,parrent_id FROM '.DATABASE_TRADE.'.'.$this->_name
                .' where id = '.$params['id'].' and type = '."0"

            );
        }
        if (!empty($params['parrent_id']) ){
            $stmt = $db->prepare('
                SELECT id,file_name,parrent_id FROM '.DATABASE_TRADE.'.'.$this->_name
                .' where parrent_id = '.$params['parrent_id'].' and type =  '."0"
            );
        }
        
        $stmt->execute();
        $data = $stmt->fetchAll();
        $db = $stmt = null;
        return $data;
    }
    public function GetListRepair($params){

        $db = Zend_Registry::get('db');
        if (!empty($params['id']) ){
            $stmt = $db->prepare('
                SELECT id,file_name,parrent_id FROM '.DATABASE_TRADE.'.'.$this->_name
                .' where id = '.$params['id'].' and type = '."1"

            );
        }
        if (!empty($params['parrent_id']) ){
            $stmt = $db->prepare('
                SELECT id,file_name,parrent_id FROM '.DATABASE_TRADE.'.'.$this->_name
                .' where parrent_id = '.$params['parrent_id'].' and type =  '."1"
            );
        }
        
        $stmt->execute();
        $data = $stmt->fetchAll();
        $db = $stmt = null;
        return $data;
    }
    public function GetImgRepair($params){

        $db = Zend_Registry::get('db');
        if (!empty($params['id']) ){
            $stmt = $db->prepare('
                SELECT id,file_name,parrent_id FROM '.DATABASE_TRADE.'.'.$this->_name
                .' where id = '.$params['id'].' and type = '."6"

            );
        }
        if (!empty($params['parrent_id']) ){
            $stmt = $db->prepare('
                SELECT id,file_name,parrent_id FROM '.DATABASE_TRADE.'.'.$this->_name
                .' where parrent_id = '.$params['parrent_id'].' and type =  '."6"
            );
        }
        
        $stmt->execute();
        $data = $stmt->fetchAll();
        $db = $stmt = null;
        return $data;
    }
    public function GetDocument($params){
        $db = Zend_Registry::get('db');
        if (!empty($params['id']) ){
            $stmt = $db->prepare('
                SELECT id,file_name,parrent_id FROM '.DATABASE_TRADE.'.'.$this->_name
                .' where id = '.$params['id'].' and type = '."5"

            );
        }
        if (!empty($params['parrent_id']) ){
            $stmt = $db->prepare('
                SELECT id,file_name,parrent_id FROM '.DATABASE_TRADE.'.'.$this->_name
                .' where parrent_id = '.$params['parrent_id'].' and type =  '."5"
            );
        }
        
        $stmt->execute();
        $data = $stmt->fetchAll();
        $db = $stmt = null;
        return $data;
    }
    public function GetListTransfer($params){

        $db = Zend_Registry::get('db');
        if (!empty($params['id']) ){
            $stmt = $db->prepare('
                SELECT id,file_name,parrent_id FROM '.DATABASE_TRADE.'.'.$this->_name
                .' where id = '.$params['id'].' and type = '."2"

            );
        }
        if (!empty($params['parrent_id']) ){
            $stmt = $db->prepare('
                SELECT id,file_name,parrent_id FROM '.DATABASE_TRADE.'.'.$this->_name
                .' where parrent_id = '.$params['parrent_id'].' and type =  '."2"
            );
        }
        
        $stmt->execute();
        $data = $stmt->fetchAll();
        $db = $stmt = null;
        return $data;
    }
    public function GetListShopTo($params){

        $db = Zend_Registry::get('db');
        if (!empty($params['id']) ){
            $stmt = $db->prepare('
                SELECT id,file_name,parrent_id FROM '.DATABASE_TRADE.'.'.$this->_name
                .' where id = '.$params['id'].' and type = '."3"

            );
        }
        if (!empty($params['parrent_id']) ){
            $stmt = $db->prepare('
                SELECT id,file_name,parrent_id FROM '.DATABASE_TRADE.'.'.$this->_name
                .' where parrent_id = '.$params['parrent_id'].' and type =  '."3"
            );
        }
        
        $stmt->execute();
        $data = $stmt->fetchAll();
        $db = $stmt = null;
        return $data;
    }
    public function GetListTransferTo($params){

        $db = Zend_Registry::get('db');
        if (!empty($params['id']) ){
            $stmt = $db->prepare('
                SELECT id,file_name,parrent_id FROM '.DATABASE_TRADE.'.'.$this->_name
                .' where id = '.$params['id'].' and type = '."4"

            );
        }
        if (!empty($params['parrent_id']) ){
            $stmt = $db->prepare('
                SELECT id,file_name,parrent_id FROM '.DATABASE_TRADE.'.'.$this->_name
                .' where parrent_id = '.$params['parrent_id'].' and type =  '."4"
            );
        }
        
        $stmt->execute();
        $data = $stmt->fetchAll();
        $db = $stmt = null;
        return $data;
    }
}