<?php

class Application_Model_AreaLog extends Zend_Db_Table_Abstract{

	public function __construct(){
		$this->table = 'area_log';
	}

	private function dbconnect(){
		return Zend_Registry::get('db');
	}

	public function _insert($params = array()){
		$db = $this->dbconnect();
		$stmt = $db->prepare('CALL SP_area_log_Insert(:id, :staff_id, :action, :date)');
		$stmt->bindParam('id', $params['id'], PDO::PARAM_INT);
		$stmt->bindParam('staff_id', $params['staff_id'], PDO::PARAM_INT);
		$stmt->bindParam('action', $params['action'], PDO::PARAM_STR);
		$stmt->bindParam('date', $params['date'], PDO::PARAM_STR);
		$res = $stmt->execute();
		$stmt->closeCursor();
		$db = $stmt = null;
		return $res;
	}

}