<?php
class Application_Model_SalesTargetProtectLog extends Zend_Db_Table_Abstract
{
    protected $_name = 'sales_target_protect_log';

    public function getLastProtectTime($staff_id)
    {
        $sql = "SELECT action_at FROM sales_target_protect_log WHERE staff_id = ? AND action_type = ? ORDER BY action_at DESC LIMIT 1";
        $db = Zend_Registry::get('db');
        $result = $db->query($sql, array(intval($staff_id), My_Sale_Target_Action::PROTECT));
        $row = $result->fetch();

        if (!$row || !isset($row['action_at'])) return false;
        return $row['action_at'];
    }
}
