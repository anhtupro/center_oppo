<?php
class Application_Model_AreaStaff extends Zend_Db_Table_Abstract
{
	protected $_name = 'area_staff';
    protected $_schema = DATABASE_TRADE;

    function getAreaTrade($email){
        if( empty($email) ){
            return false;
        }

        $db = Zend_Registry::get('db');
        
        $arrCols = array(
            'area_id'            => 'a.area_id',
        );

        $select = $db->select()
                ->from(array('p'=> DATABASE_TRADE.'.staff'), $arrCols);
        $select->joinLeft(array('a'=> DATABASE_TRADE.'.area_staff'), 'a.staff_id = p.id',array());

        if(isset($email) and $email){
            $select->where('p.email = ?', $email);
        }
        $result = $db->fetchAll($select);

        $data = [];

        if($result){
            foreach($result as $key=>$value){
                $data[] = $value['area_id'];
            }
        }
        
        return $data;
    }

    public function getArea($staff_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE.'.area_staff'], [
                'a.area_id'
            ])
            ->where('a.staff_id = ?', $staff_id);

        $result = $db->fetchCol($select);

        return $result;
    }

    public function getAll()
    {
        $db = Zend_Registry::get("db");

        // select oppo
        $select_oppo = $db->select()
            ->from(['st' => 'staff'], [
                'staff_id' => 'st.id',
//                         'a.area_id',
                'string_area_id' => "GROUP_CONCAT(DISTINCT a.area_id)",
                'string_area_name' => "GROUP_CONCAT(DISTINCT e.name)",
                'staff_name' => "CONCAT(st.firstname, ' ', st.lastname)",
                'email' => 'st.email',
                'title' => 't.name'
            ])
            ->joinLeft(['a' => DATABASE_TRADE.'.area_staff'], 'a.staff_id = st.id', [])
            ->joinLeft(['e' => 'area'], 'a.area_id = e.id', [])
            ->joinLeft(['t' => 'team'], 'st.title = t.id', [])
            ->where('st.title IN (?)', [TRADE_MARKETING_EXECUTIVE,TRADE_MARKETING_LEADER,533,464,471,534,466,535,467,391,392,465,308,181,179,536,TRADE_MARKETING_MANAGER])
            ->where('st.off_date IS NULL')
            ->group('st.id');



        // select realme
        $select_realme = $db->select()
            ->from(['st' => DATABASE_TRADE . '.staff_realme'], [
                'staff_id' => 'st.id',
//                         'a.area_id',
                'string_area_id' => "GROUP_CONCAT(DISTINCT a.area_id)",
                'string_area_name' => "GROUP_CONCAT(DISTINCT e.name)",
                'staff_name' => "CONCAT(st.firstname, ' ', st.lastname)",
                'email' => 'st.email',
                'title' => 't.name'
            ])
            ->joinLeft(['a' => DATABASE_TRADE.'.area_staff'], 'a.staff_id = st.id', [])
            ->joinLeft(['e' => 'area'], 'a.area_id = e.id', [])
            ->joinLeft(['t' => 'team'], 'st.title = t.id', [])
            ->where('st.title IN (?)', [TRADE_MARKETING_EXECUTIVE,TRADE_MARKETING_LEADER,533,464,471,534,466,535,467,391,392,465,308,181,179,536,TRADE_MARKETING_MANAGER, TRADE_SUPERVISOR_REALME_TITLE])
            ->where('st.off_date IS NULL')
            ->group('st.id');


        $select_union = $db->select()->union(array($select_oppo, $select_realme));
        $select = $db->select()
            ->from(['g' => $select_union], [
                'g.*'
            ]);

        $result = $db->fetchAll($select);


        foreach ($result as $key => $item) {
            $result[$key] ['list_area_id'] = explode(',', $item['string_area_id']);
            $result[$key] ['list_area_name'] = explode(',', $item['string_area_name']);

            $listAreaId =  $result[$key] ['list_area_id'];
            $listAreaName =  $result[$key] ['list_area_name'];

            foreach ( $listAreaId as $index => $value) {
                $result[$key] ['list_area'] [] = [
                    'area_id' => $value,
                    'area_name' => $listAreaName[$index]
                ];
            }
        }

        return $result;
    }

    public function getAreaById($staff_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['a' => DATABASE_TRADE.'.area_staff'], [
                'a.area_id'
            ])
            ->where('a.staff_id = ?', $staff_id);

        $result = $db->fetchCol($select);
        return $result;
    }

    public function getTradeArea()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'staff_id' => "p.staff_id",
            "area_id"  => "p.area_id",
            "fullname"  => "CONCAT(st.firstname, ' ', st.lastname)"
        );

        $select->from(array('p' => DATABASE_TRADE .'.area_staff'), $arrCols);
        $select->joinLeft(['st' => 'staff'], 'p.staff_id = st.id', []);
        $select->where('st.title IN (?)', [TRADE_MARKETING_EXECUTIVE, TRADE_MARKETING_LEADER]);
        $select->where('st.off_date IS NULL');

        $result = $db->fetchAll($select);

        $data = [];
        foreach($result as $key=>$value){
            $data[$value['area_id']][] = ['staff_id' => $value['staff_id'], 'fullname' => $value['fullname']];
        }

        return $data;
    }


    public function getListStaff($params)
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['a' => DATABASE_TRADE.'.area_staff'], [
                        'a.staff_id',
                         'staff_name' => "CONCAT(s.firstname, ' ', s.lastname)",
                         's.title',
                         'list_area' => "GROUP_CONCAT(e.name)"
                     ])
        ->join(['s' => 'staff'], 'a.staff_id = s.id AND s.off_date IS NULL', [])
            ->joinLeft(['e' => 'area'], 'e.id = a.area_id', [])

            ->where('a.area_id IN (?)', $params['area_list'])
            ->order('s.title')
            ->group('a.staff_id');

        if ($userStorage->title == TRADE_MARKETING_LEADER) {
//            $select->where('a.staff_id = ?',$userStorage->id);
//            $select->orWhere('s.title = ?',TRADE_MARKETING_EXECUTIVE);
            $select->where("a.staff_id = " . $userStorage->id . " OR s.title = " . TRADE_MARKETING_EXECUTIVE);
        } elseif ($userStorage->title == TRADE_MARKETING_EXECUTIVE) {
            $select->where('a.staff_id = ?',$userStorage->id);
        } else {
            $select->where('s.title IN (?)',[TRADE_MARKETING_LEADER, TRADE_MARKETING_EXECUTIVE]);
        }

        $result = $db->fetchAll($select);

        return $result;
        
    }

    public function checkLegalStore($store_id, $staff_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['s' => 'store'], [
                's.id'
            ])
            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['p' => 'regional_market'], 'di.parent = p.id', [])
            ->joinLeft(['a' => 'area'], 'p.area_id = a.id', [])
            ->join(['f' => DATABASE_TRADE . '.area_staff'], 'a.id = f.area_id', [])
            ->where('f.staff_id = ?', $staff_id)
            ->where('s.id = ?', $store_id);

        $result = $db->fetchOne($select);

        return $result ? true : false;

    }
}