<?php
class Application_Model_GoodKpiLog extends Zend_Db_Table_Abstract
{
    protected $_name = 'good_kpi_log';

    public function fetchByMonth($page, $limit, &$total, $params)
    {
        // SELECT g.good_id, g.color_id, g.price,
        // CASE WHEN g.from_date < '2015-05-01' THEN '2015-05-01' ELSE g.from_date END,
        // CASE WHEN g.to_date > '2015-05-31' OR g.to_date IS NULL THEN '2015-05-31' ELSE g.to_date END
        // FROM good_price_log g
        // WHERE g.from_date <= '2015-05-31'
        // AND (g.to_date >= '2015-05-01' OR g.to_date IS NULL)
        // ORDER BY g.good_id, g.color_id, g.from_date

        $from = date_create_from_format('d/m/Y', $params['from'])->format("Y-m-d");
        $to = date_create_from_format('d/m/Y', $params['to'])->format("Y-m-d");

        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('g' => $this->_name), array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS g.id'), 'g.good_id', 'g.color_id', 'g.kpi', 'g.type',
                'from' => new Zend_Db_Expr(sprintf("CASE WHEN g.from_date < '%s' THEN '%s' ELSE g.from_date END", $from, $from)),
                'to' => new Zend_Db_Expr(sprintf("CASE WHEN g.to_date > '%s' THEN '%s' ELSE g.to_date END", $to, $to)),
            ))
            ->joinLeft(array('b' => WAREHOUSE_DB.'.good'), 'g.good_id = b.id', array('cat_id' => 'b.cat_id'))
            ->joinLeft(array('d' => WAREHOUSE_DB.'.distributor'), 'g.distributor_id = d.id', array('distributor_name' => 'd.title'))
            ->joinLeft(array('ar' => 'area'), 'g.area_id = ar.id', array('area_name' => 'ar.name'));
            
        if (isset($params['good_id']) && $params['good_id'])
            $select->where('g.good_id = ?', intval($params['good_id']));
            
        if (isset($params['cat']) && $params['cat'])
            $select->where('b.cat_id = ?', intval($params['cat']));
            
        if (isset($params['name']) && $params['name'])
            $select->where('b.desc LIKE ?', '%'.$params['name'].'%');

        if (isset($params['type']) && $params['type'])
            $select->where('g.type = ?', intval($params['type']));

        if (isset($params['to']) && date_create_from_format('d/m/Y', $params['to']))
            $select->where('g.from_date <= ?', date_create_from_format('d/m/Y', $params['to'])->format("Y-m-d"));

        if (isset($params['from']) && date_create_from_format('d/m/Y', $params['from']))
            $select->where('g.to_date IS NULL OR g.to_date >= ?', date_create_from_format('d/m/Y', $params['from'])->format("Y-m-d"));

        $select->order(array('g.good_id', 'g.color_id', 'g.from_date'));

        if ($limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);

        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    public function get_list($from, $to, $type,$area_id = false)
    {
        $db = Zend_Registry::get('db');

        $_from = sprintf("CASE WHEN p.from_date < '%s' THEN '%s' ELSE p.from_date END", $from, $from);
        $_to = sprintf("CASE WHEN p.to_date > '%s' OR p.to_date IS NULL THEN '%s' ELSE p.to_date END", $to, $to);
        $_color = sprintf("CASE WHEN p.color_id IS NULL THEN 0 ELSE p.color_id END");

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array(
                    'area_id',
                    'from_date' => new Zend_Db_Expr($_from),
                    'to_date' => new Zend_Db_Expr($_to),
                    'p.good_id',
                    'color_id' => new Zend_Db_Expr($_color),
                    'kpi',
                )
            )
            ->where('p.type = ?', intval($type))
            ->where('DATE(from_date) <= ?', $to)
            ->where('DATE(to_date) IS NULL OR DATE(to_date) >= ?', $from)
            ->order(array('p.area_id'))
            ;


        $log = $db->fetchAll($select);

        $log_list = array();

        foreach ($log as $key => $value) {
            if($area_id == false){
                $log_list[ $value['good_id'] ][$value['color_id']][] = array(
                    'from' => $value['from_date'],
                    'to'   => $value['to_date'],
                    'kpi'  => $value['kpi'],
                );
            }else{
                $log_list[ $value['area_id'] ][ $value['good_id'] ][$value['color_id']][] = array(
                    'from' => $value['from_date'],
                    'to'   => $value['to_date'],
                    'kpi'  => $value['kpi'],
                );
            }
        }

        return $log_list;
    }
    
    public function get_list_access($from, $to, $type)
    {
        $db = Zend_Registry::get('db');

        $_from = sprintf("CASE WHEN p.from_date < '%s' THEN '%s' ELSE p.from_date END", $from, $from);
        $_to = sprintf("CASE WHEN p.to_date > '%s' OR p.to_date IS NULL THEN '%s' ELSE p.to_date END", $to, $to);
        $_color = sprintf("CASE WHEN p.color_id IS NULL THEN 0 ELSE p.color_id END");

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array(
                    'from_date' => new Zend_Db_Expr($_from),
                    'to_date' => new Zend_Db_Expr($_to),
                    'p.good_id',
                    'color_id' => new Zend_Db_Expr($_color),
                    'kpi',
                )
            )
            ->join(array('b' => WAREHOUSE_DB.'.'.'good'), 'b.id=p.good_id', array('good_desc' => 'b.desc'))
            ->where('p.type = ?', intval($type))
            ->where('b.cat_id = ?', ACCESS_CAT_ID)
            ->where('DATE(from_date) <= ?', $to)
            ->where('DATE(to_date) IS NULL OR DATE(to_date) >= ?', $from);
            

        $log = $db->fetchAll($select);

        $log_list = array();

        foreach ($log as $key => $value) {
            $log_list[ $value['good_id'] ][$value['color_id']][] = array(
                'from' => $value['from_date'],
                'to'   => $value['to_date'],
                'kpi'  => $value['kpi'],
            );
        }

        return $log_list;
    }

    public function save($params,$id){

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $data = array(
            'good_id'        => intval($params['good_id']),
            'color_id'       => intval($params['color_id']),
            'from_date'      => $params['from_date'],
            'to_date'        => $params['to_date'],
            'kpi'            => $params['kpi'],
            'type'           => $params['type'],
            'area_id'        => $params['area_id'],
            'distributor_id' => $params['distributor_id'],
        );

        $date = date('Y-m-d H:i:s');
        $result = array();

        try {
            if($id){
                $where = $this->getAdapter()->quoteInto('id = ?',$id);
                $data['updated_at'] = $date;
                $data['updated_by'] = $userStorage->id;
                $this->update($data,$where);
            }else{
                $data['created_at'] = $date;
                $data['created_by'] = $userStorage->id;
                $this->insert($data);
            }
            $result = array('code' => 0,'message'=>'Done');
        } catch (Exception $e) {
            $result = array('code'=>1,'message'=>$e->getMessage());
        }
        return $result;
    }

    public function _insert($value){
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare('
            INSERT INTO good_kpi_log(good_id, color_id, kpi, type, from_date, to_date, created_at, area_id, distributor_id)
            VALUES'.$value
        );
        $res = $stmt->execute();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $res;
    }

}