<?php
class Application_Model_LessonComment extends Zend_Db_Table_Abstract
{
    protected $_name = 'lesson_comment';

    function fetchPagination($page, $limit, &$total, $params) {
        $db = Zend_Registry::get('db');

        $select->from(array('c' => $this->_name),
                array('s.id as id','content','s.firstname','s.lastname','c.created_at as created_at')
            )
            ->joinLeft(array('s' => 'staff'), 'c.staff_id = s.id', array());


        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }
    
    //L?y t�n ngu?i comment d?a v�o b?ng staff_training
    function fetchCommentStaff($id){
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('c' => $this->_name),
                    array('s.id as id','content','s.firstname','s.lastname','c.created_at as created_at','d.firstname as firstname2','d.lastname as lastname2','c.id as id_commment')
                )
                ->where('c.lesson_id = ?', $id)
                ->joinLeft(array('s' => 'staff_training'), 'c.staff_cmnd = s.cmnd', array())
                ->joinLeft(array('d' => 'staff'), 'c.staff_cmnd = d.ID_number', array())
                ->group("c.id")
                ->order("created_at DESC")
                ->limit(10,0);
        $result = $db->fetchAll($select);
        return $result;
    }
    

}