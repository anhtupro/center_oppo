<?php

class Application_Model_StoreLeaderLog extends Zend_Db_Table_Abstract {

    protected $_name = 'store_leader_log';

    public function is_leader($staff_id, $store_id, $time) {
        $where   = array();
        $where[] = $this->getAdapter()->quoteInto('store_id = ?', $store_id);
        $where[] = $this->getAdapter()->quoteInto('staff_id = ?', $staff_id);
        $where[] = $this->getAdapter()
                ->quoteInto('? >= FROM_UNIXTIME(joined_at, \'%Y-%m-%d\') AND ( ? < FROM_UNIXTIME(released_at, \'%Y-%m-%d\') OR released_at IS NULL OR released_at = 0 )', date('Y-m-d', strtotime($time)));

        return $this->fetchRow($where) ? true : false;
    }

    public function get_leader($store_id, $time) {
        $where   = array();
        $where[] = $this->getAdapter()->quoteInto('store_id = ?', $store_id);

        $where[] = $this->getAdapter()->quoteInto(
                'FROM_UNIXTIME(joined_at, \'%Y-%m-%d\') <= ?', date('Y-m-d', strtotime($time))
        );

        $where[] = $this->getAdapter()->quoteInto(
                'released_at IS NULL OR released_at = 0 OR FROM_UNIXTIME(released_at, \'%Y-%m-%d\') > ?', date('Y-m-d', strtotime($time))
        );

        $result = $this->fetchRow($where);

        return $result && intval($result['staff_id']) > 0 ? $result['staff_id'] : false;
    }

    /**
     * Lấy danh sách các store mà staff đó quản lý từ $from đến $to
     * @param  int $staff_id  -  staff id
     * @param  datetime $time - thời gian để check
     * @return array          - mảng các store id
     */
    public function get_stores_cache($staff_id, $from, $to) {
        $cache  = Zend_Registry::get('cache');
        $result = $cache->load($this->_name . '_cache');

        if ($result === false) {
            $db     = Zend_Registry::get('db');
            $select = $db->select()
                    ->distinct()
                    ->from(array('p' => $this->_name), array('p.store_id', 'p.staff_id'))
                    // ->where('staff_id = ?', $staff_id)
                    ->where('FROM_UNIXTIME(joined_at, \'%Y-%m-%d\') <= ?', $to)
                    ->where('released_at IS NULL OR released_at = 0 OR FROM_UNIXTIME(released_at, \'%Y-%m-%d\') > ?', $from);

            $data = $db->fetchAll($select);

            $result = array();

            foreach ($data as $value)
                $result[$value['staff_id']][] = $value['store_id'];

            $cache->save($result, $this->_name . '_cache', array(), 3600 * 24);
        }

        return isset($result[$staff_id]) && count($result[$staff_id]) ? $result[$staff_id] : false;
    }

    public function findPrimaykey($id) {
        return $this->fetchRow($this->select()->where('id = ?', $id));
    }

    public function findStaffId($id) {
        return $this->fetchRow($this->select()->where('staff_id = ?', $id));
    }

    public function getStoreByStaffAndTitle($staff_id, $old_title, $new_title, $old_province, $new_province) {
        $data               = [];
        $db                 = Zend_Registry::get('db');
        if (($old_title <> $new_title || $old_province <> $new_province) && $old_title== 190
                  && (!in_array($old_province,array(4212,4213,3417)) || !in_array($new_province,array(4212,4213,3417)))
                ) {
            $sql  = 'SELECT * FROM store_leader_log s 
             where s.released_at is null and s.staff_id=:staff_id';
            $stmt = $db->prepare($sql);
            $stmt->bindParam("staff_id", $staff_id, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetch();
            $stmt->closeCursor();
            $db   = $stmt = null;
        }
        return $data;
    }

}
