<?php
class Application_Model_EndYear extends Zend_Db_Table_Abstract
{
    protected $_name = 'year_end_2020';
    public function get_staff($id){
    	$db = Zend_Registry::get('db');
        $sql = 'SELECT e.*,s.photo,s.id staff_id
				FROM '. $this->_name.' e
                                left join staff s on s.code = e.code
				WHERE e.id = :id';
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id",  $id, PDO::PARAM_STR);
        $stmt->execute();

        $data = $stmt->fetch();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $data;
    }

   
}
