<?php
class Application_Model_CustomerCheckCallHistory extends Zend_Db_Table_Abstract
{
    protected $_name = 'customer_check_call_history';

    public function getSummary($from, $to)
    {
        $db = Zend_Registry::get('db');
        $sql = "SELECT
                staff_id,
                FLOOR(
                    (
                        DayOfMonth(time) - 1
                    ) / 7
                ) + 1 AS WEEK_IN_MONTH,
                DATE(time) AS DATE,
                SUM(CASE WHEN result = ? THEN 1 ELSE 0 END) AS OK,
                SUM(CASE WHEN result = ? THEN 1 ELSE 0 END) AS NOT_OK,
                SUM(CASE WHEN result = ? THEN 1 ELSE 0 END) AS NOT_CONFIRMED,
                SUM(CASE WHEN result = ? THEN 1 ELSE 0 END) AS RESURVEY
            FROM
                `customer_check_call_history`
            WHERE time >= ?
            AND time <= ?
            GROUP BY
            staff_id,
            FLOOR(
                    (
                        DayOfMonth(time) - 1
                    ) / 7
                ) + 1,
                DATE(time)"
        ;

        $result = $db->query($sql, array(
            My_Survey_Result::OK,
            My_Survey_Result::NOT_OK,
            My_Survey_Result::NOT_CONFIRMED,
            My_Survey_Result::RESURVEY,
            $from,
            $to,
        ));

        $week = array();

        foreach ($result as $_key => $_value) {
            $week[ $_value['staff_id'] ]
                [ $_value['WEEK_IN_MONTH'] ]
                [ $_value['DATE'] ] = array(
                    'ok'            => $_value['OK'],
                    'not_ok'        => $_value['NOT_OK'],
                    'not_confirmed' => $_value['NOT_CONFIRMED'],
                    'resurvey'      => $_value['RESURVEY'],
                    'total' => ( isset( $_value['OK'] ) ? $_value['OK'] : 0 ) 
                        + ( isset( $_value['NOT_OK'] ) ? $_value['NOT_OK'] : 0 ) 
                        + ( isset( $_value['NOT_CONFIRMED'] ) ? $_value['NOT_CONFIRMED'] : 0 ) 
                        + ( isset( $_value['RESURVEY'] ) ? $_value['RESURVEY'] : 0 )
            );
        } // end foreach

        return $week;
    } // end func
}
