<?php
class Application_Model_PurchasingRequest extends Zend_Db_Table_Abstract
{
    protected $_name = 'purchasing_request';

    function fetchPagination($page, $limit, &$total, $params){
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $db = Zend_Registry::get('db');

        $col = array(
                new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 
                'p.sn','p.*',
                'project_name' => 'j.name',
                "created_by" => "CONCAT(s.firstname, ' ',s.lastname)" , 
                "confirm_by" => "CONCAT(s2.firstname, ' ',s2.lastname)" ,
                "approved_by"=> "CONCAT(s3.firstname, ' ',s3.lastname)" ,
                "product_name" => "c.name", "unit" => "c.unit", "quantity"=>"prd.quantity",
                "installation_at" => "prd.installation_at","installation_time" => "prd.installation_time",
                "prs.*",
                "currency_name" => "IFNULL(currency.name, 'VNĐ')"


        );
        $col['order_status_new']=new Zend_Db_Expr("Case When p.status=2 Then 1   When p.status= 1 Then 2   When p.status=3 Then 3  Else 4 End");

        $select = $db->select()->from(array('p' => $this->_name), $col);
        
        $select->joinleft(array('prd'=>'purchasing_request_details'), 'prd.pr_id = p.id', array());
        $select->joinleft(array('prs'=> DATABASE_SALARY.'.purchasing_request_supplier'), 'prs.pr_detail_id = prd.id AND prs.select_supplier = 1', array());
        $select->joinleft(array('currency'=>'currency'), 'currency.id = prs.currency', array());
        $select->joinleft(array('c'=>'pmodel_code'), 'c.id = prd.pmodel_code_id', array());
        $select->joinleft(array('j'=>'purchasing_project'), 'j.id = p.project_id', array());
        $select->joinleft(array('s'=>'staff'), 's.id = p.created_by', array());
        $select->joinleft(array('s2'=>'staff'), 's2.id = p.confirm_by', array());
        $select->joinleft(array('s3'=>'staff'), 's3.id = p.approved_by', array());

        if(!empty($params['sn'])){
            $select->where('p.sn = ?', $params['sn']);
        }

        if(!empty($params['name'])){
            $select->where('p.`name` LIKE ?', "%".$params['name']."%");
        }
        
        if(!empty($params['type'])){
            $select->where('p.type = ?', $params['type']);
        }

        if(!empty($params['project_id'])){
            $select->where('p.project_id = ?', $params['project_id']);
        }

        if(!empty($params['pmodel_code_id'])){
            $select->where('prd.pmodel_code_id = ?', $params['pmodel_code_id']);
        }
        
        if(!empty($params['urgent_from'])){
            $select->where('DATE(p.urgent_date) >= ?', $params['urgent_from']);
        }

        if(!empty($params['urgent_to'])){
            $select->where('DATE(p.urgent_date) <= ?', $params['urgent_to']);
        }
        
        if(!empty($params['create_from'])){
            $select->where('DATE(p.created_at) >= ?', $params['create_from']);
        }
        
        if(!empty($params['create_to'])){
            $select->where('DATE(p.created_at) <= ?', $params['create_to']);
        }

        if(!empty($params['status'])){
            $select->where('p.status = ? ', $params['status']);
        }
        
        if(!empty($params['department_id'])){
            $select->where('p.department_id = ?', $params['department_id']);
        }
        
        if(!empty($params['supplier_id'])){
            $select->where('prs.supplier_id = ?', $params['supplier_id']);
        }
        
        ///// PHÂN QUYỀN        
        if(!empty($params['access_department_arr'])){
            $select->where('p.department_id IN (?)', $params['access_department_arr']);
        }
        
        if(!empty($params['create_by'])){
            $select->where('p.created_by = ?', $params['create_by']);
        }
        
        $select->where('p.del = 0',null);
  //echo $select; die();      
        if(empty($params['export'])){
            $select->group('p.id');
        }
        
        if($userStorage->email == 'hien.tran@oppo-aed.vn' ){
            $select->order(['(p.`status` = 3)', '(p.`status` = 1)', '(p.`status` = 2)', 'p.confirm_at DESC']);
            //$select->order(['(p.`status` = 2) DESC']);
            
        }
        elseif($userStorage->email == 'thuy.to@oppo-aed.vn'){
            $select->order(['(p.`status` = 2) DESC', '(SUM(prd.quantity*prs.price+prs.fee)> 500000000) DESC', 'p.created_at DESC']);
        }elseif( $userStorage->team == TEAM_PURCHASING){
            $select->order(['order_status_new ASC','p.created_at DESC']);

        }
        else{
            $select->order('p.created_at DESC');
        }
        
        if($_GET['dev'] == 1){
              echo $select;exit;
        }
        
        
        if(empty($params['export'])){
            $select->limitPage($page, $limit);
        }
       // echo $select->__toString();
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
    
    function fetchPagination2($page, $limit, &$total, $params){
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $db = Zend_Registry::get('db');

        $col = array(
                new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 
                'p.sn','p.*',
                'project_name' => 'j.name',   
                'sum_partial_amount' => 'sum(ro.cost_temp)',
                'ro_id' => 'ro.id',
                'is_done'   => 'ro.is_done',
                "created_by" => "CONCAT(s.firstname, ' ',s.lastname)" , 
                "confirm_by" => "CONCAT(s2.firstname, ' ',s2.lastname)" ,
                "approved_by"=> "CONCAT(s3.firstname, ' ',s3.lastname)" ,
                "product_name" => "c.name", "unit" => "c.unit", "quantity"=>"prd.quantity",
                "installation_at" => "prd.installation_at","installation_time" => "prd.installation_time",
                "prs.*",
        );
        $col['order_status_new']=new Zend_Db_Expr("Case When p.status=2 Then 1   When p.status= 1 Then 2   When p.status=3 Then 3  Else 4 End");

        $select = $db->select()->from(array('p' => $this->_name), $col);
        $select->joininner(array('ro'=> DATABASE_SALARY.'.request_office'), 'ro.pr_sn = p.sn', array());
        $select->joinleft(array('prd'=>'purchasing_request_details'), 'prd.pr_id = p.id', array());
        $select->joinleft(array('prs'=> DATABASE_SALARY.'.purchasing_request_supplier'), 'prs.pr_detail_id = prd.id AND prs.select_supplier = 1', array());
        $select->joinleft(array('c'=>'pmodel_code'), 'c.id = prd.pmodel_code_id', array());
        $select->joinleft(array('j'=>'purchasing_project'), 'j.id = p.project_id', array());
        $select->joinleft(array('s'=>'staff'), 's.id = p.created_by', array());
        $select->joinleft(array('s2'=>'staff'), 's2.id = p.confirm_by', array());
        $select->joinleft(array('s3'=>'staff'), 's3.id = p.approved_by', array());
        $select->where('p.status = ? ', 3);
        if(!empty($params['access_department_arr'])){
            $select->where('p.department_id IN (?)', $params['access_department_arr']);
        }
        
        if(isset($params['is_done']) and $params['is_done'])
        {
            if($params['is_done'] == 1){
                $select->where('ro.is_done = 0');
            }elseif($params['is_done'] == -1){
                $select->where('ro.is_done = 1');
            }
        }
        
        if(!empty($params['sn'])){
            $select->where('p.sn = ?', $params['sn']);
        }

        if(!empty($params['name'])){
            $select->where('p.`name` LIKE ?', "%".$params['name']."%");
        }
        
        if(!empty($params['urgent_from'])){
            $select->where('DATE(p.urgent_date) >= ?', $params['urgent_from']);
        }

        if(!empty($params['urgent_to'])){
            $select->where('DATE(p.urgent_date) <= ?', $params['urgent_to']);
        }
        
        $select->where('p.del = 0',null);   
        if(empty($params['export'])){
            $select->group('p.id');
        }       
        
        if(empty($params['export'])){
            $select->limitPage($page, $limit);
        }
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
    
    public function getRequestOffice($sn) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'r.id',
            'r.content',
            'r.cost_temp',
        );

        $select->from(array('r' => DATABASE_SALARY.'.request_office'), $arrCols);
        $select->where('r.pr_sn = ?', $sn);
        $result = $db->fetchAll($select);

        return $result;
    }
    
    function getData($params){

        $db = Zend_Registry::get('db');

        $col = array(
                'p.id',
                'p.name',
                'p.type',
                'p.sn', 
                'p.status', 
                'p.created_at', 
                'p.confirm_at', 
                'p.confirm_by',
                'p.urgent_date',
                'p.remark',
                'p.shipping_address',
                'p.project_id',
                'p.area_id',
                'p.department_id',
                'p.team_id',
                'p.have_contract',
                'p.contract_number',
                'p.is_reject',
                'p.reject_note',
                'p.reject_by',
                'p.request_type_group',
                'p.request_type',
                "created_by" => "CONCAT(s.firstname, ' ',s.lastname)" , 
                "reject_by" => "CONCAT(s2.firstname, ' ',s2.lastname)" , 
                "department_name" => 't.name' ,
        );

        $select = $db->select()->from(array('p' => $this->_name), $col);

        //$select->joinleft(array('d'=>'purchasing_request_details'), 'd.pr_id = p.id', array());
        $select->joinleft(array('s'=>'staff'), 's.id = p.created_by', array());
        $select->joinleft(array('s2'=>'staff'), 's2.id = p.reject_by', array());
        $select->joinleft(array('t'=>'team'), 's.department = t.id', array());

        if(!empty($params['sn'])){
            $select->where('p.sn = ?', $params['sn']);
        }
        $select->group('p.id');

        $select->order('p.created_at DESC');
        $result = $db->fetchRow($select);

        return $result;
    }
    
    function getDataFull($params){

        $db = Zend_Registry::get('db');

        $col = array(
            'p.id',
            'p.name',
            'p.type',
            'p.sn', 
            'p.status', 
            'p.created_at', 
            'p.confirm_at', 
            'p.confirm_by',
            'p.urgent_date',
            'p.remark',
            'p.shipping_address',
            'p.project_id',
            'p.area_id',
            'p.department_id',
            'p.team_id',
            'p.have_contract',
            'p.contract_number',
            "created_by" => "CONCAT(s.firstname, ' ',s.lastname)" , 
            "department_name" => 't.name' ,
            "team_name" => 't2.name' ,
            "type_name" => "type.name",
            "project_name" => "project.name",
            "area_name" => "area.name"
        );

        $select = $db->select()->from(array('p' => $this->_name), $col);

        $select->joinleft(array('type'=>'purchasing_type'), 'type.id = p.type', array());
        $select->joinleft(array('project'=>'purchasing_project'), 'project.id = p.project_id', array());
        $select->joinleft(array('area'=>'area'), 'area.id = p.area_id', array());
        $select->joinleft(array('s'=>'staff'), 's.id = p.created_by', array());
        $select->joinleft(array('t'=>'team'), 'p.department_id = t.id', array());
        $select->joinleft(array('t2'=>'team'), 'p.team_id = t2.id', array());

        if(!empty($params['sn'])){
            $select->where('p.sn = ?', $params['sn']);
        }
        $select->group('p.id');

        $select->order('p.created_at DESC');
        $result = $db->fetchRow($select);

        return $result;
    }
    
    function getInfoPurchasingRequest($pr_sn){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'sn' => "p.sn",
            "p.urgent_date",
            "p.name",
            "p.shipping_address",
            "p.type",
            "p.project_id",
            "type_name" => "IF(p.type = -1, 'Chưa xác định', type.name)",
            "project_name" => "project.name",
            'p.remark',
            'area_name' => 'a.name',
            'department_name' => 't.name',
            'team_name' => 't2.name',
        );

        $select->from(array('p' => 'purchasing_request'), $arrCols);
        $select->joinleft(array('type'=>'purchasing_type'), 'type.id = p.type', array());
        $select->joinleft(array('project'=>'purchasing_project'), 'project.id = p.project_id', array());
        $select->joinleft(array('a'=>'area'), 'a.id = p.area_id', array());
        $select->joinleft(array('t'=>'team'), 't.id = p.department_id', array());
        $select->joinleft(array('t2'=>'team'), 't2.id = p.team_id', array());

        $select->where('p.sn = ?', $pr_sn);
        $result = $db->fetchRow($select);
        
        return $result;
    }
    
    function getInfoPurchasingRequestDetails($pr_sn){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id', 
            'p.pr_id', 
            'p.pmodel_code_id', 
            'quantity' => 'IFNULL(p.quantity_real,p.quantity)',
            'p.note',
            'p.installation_at', 
            'p.installation_time', 
            'code.code',
            'code.unit',
            'code_name' => 'code.name',
            's.supplier_id', 
            's.price_before_dp', 
            'price_before_dp_format' => 'FORMAT(s.price_before_dp, 2)',
            's.price',
            'price_format' => 'FORMAT(s.price, 2)',
            's.ck', 
            'currency' => "IF(s.currency = 2, 'USD', 'VNĐ')", 
            's.discount', 
            's.fee', 
            's.article', 
            's.article_other', 
            's.warranty',
            'supplier_name' => 'sup.title',
            'total_price' => '(IFNULL(p.quantity_real,p.quantity)*(price - (s.price*s.ck/100))+fee)',
            'total_price_format' => 'FORMAT((IFNULL(p.quantity_real,p.quantity)*(price - (s.price*s.ck/100))+fee),2)',
        );

        $select->from(array('p' => 'purchasing_request_details'), $arrCols);
        $select->joinleft(array('r'=>'purchasing_request'), 'r.id = p.pr_id', array());
        $select->joinleft(array('code'=>'pmodel_code'), 'code.id = p.pmodel_code_id', array());
        $select->joinleft(array('s'=> DATABASE_SALARY.'.purchasing_request_supplier'), 's.pr_detail_id = p.id', array());
        $select->joinleft(array('sup'=>'supplier'), 'sup.id = s.supplier_id', array());

        $select->where('r.sn = ?', $pr_sn);
        $select->where('s.select_supplier = 1');
        
        $result = $db->fetchAll($select);
        
        return $result;
    }
    
    
    function getInfoPurchasingBySn($pr_sn){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'sn' => "p.sn",
            "p.urgent_date",
            "p.name",
            "p.shipping_address",
            "p.type",
            "p.project_id",
            "type_name" => "IF(p.type = -1, 'Chưa xác định', type.name)",
            "project_name" => "project.name",
            'p.remark'
        );

        $select->from(array('p' => 'purchasing_request'), $arrCols);
        $select->joinleft(array('type'=>'purchasing_type'), 'type.id = p.type', array());
        $select->joinleft(array('project'=>'purchasing_project'), 'project.id = p.project_id', array());

        $select->where('p.sn = ?', $pr_sn);
        $result = $db->fetchRow($select);
        
        return $result;
    }
    
    function getListPrForRo($params){
        
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $team_id = $userStorage->team;
        $staff_id = $userStorage->id;
        
        $db = Zend_Registry::get("db");
        $select = $db->select();
        
        $purchasing_select = "SELECT  p.sn, p.id, p.name, d.quantity, d.quantity_real, s.price, p.department_id, p.team_id,
                            SUM(IFNULL(d.quantity_real, d.quantity)*(s.price-s.price*s.ck/100)+s.fee) total_price, p.created_by
                            FROM purchasing_request p
                            LEFT JOIN `purchasing_request_details` AS `d` ON d.pr_id = p.id
                            LEFT JOIN ".DATABASE_SALARY.".`purchasing_request_supplier` AS `s` ON s.pr_detail_id = d.id 
                            WHERE (p.status = '3') AND s.select_supplier = 1
                            GROUP BY p.id";
        
        $request_office_select = "SELECT o.pr_sn, SUM(o.cost_temp) total_ro
                                    FROM ".DATABASE_SALARY.".request_office o
                                    WHERE o.del = 0 AND o.category_id <> 5
                                    GROUP BY o.pr_sn";

        $arrCols = array(
            "p.sn", 
            "p.name", 
            "p.department_id", 
            "p.team_id", 
            "p.total_price", 
            "o.total_ro",
            "p.created_by"
        );

        $select->from(array('p' => new Zend_Db_Expr('(' . $purchasing_select . ')')), $arrCols);
        $select->joinLeft(array('o' => new Zend_Db_Expr('(' . $request_office_select . ')')), 'o.pr_sn = p.sn', array());
        
        $select->where('IFNULL(o.total_ro,0) < p.total_price');
        
        if(!empty($params['access_department_arr'])){
            $select->where('p.department_id IN (?)', $params['access_department_arr']);
        }
        else{
            $select->where("(p.team_id = ".$team_id." OR p.created_by = ".$staff_id.")");
        }
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
        
}