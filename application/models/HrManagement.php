<?php
class Application_Model_HrManagement extends Zend_Db_Table_Abstract
{
	public function headcount_staffallcompany($year){
		$db = Zend_Registry::get('db');
		$stmt = $db->query('CALL sp_hr_management_staff_all_company('.$year.')');
        $list     = $stmt->fetchAll();
        $stmt->closeCursor();
        // echo "<pre>";print_r($list);die;
        return $list;
	}

	public function get_group($month_from,$year_from,$month_to,$year_to,$group){
		$db = Zend_Registry::get('db');
		$stmt = $db->query('CALL sp_hr_management_get_headcount_group('.$month_from.','.$year_from.','.$month_to.','.$year_to.','.$group.')');
        $list     = $stmt->fetchAll();
        $stmt->closeCursor();
        // echo "<pre>";print_r($list);die;
        return $list;
	}

	public function get_group_title($params){
		$db = Zend_Registry::get('db');

				 // echo 'CALL sp_hr_management_get_headcount_and_turnover_group_title_test('.$params['month_from'].','.$params['year_from'].','.$params['month_to'].','.$params['year_to'].','.$params['department'].',"'.$params['team'].'","'.$params['title'].'")';die;

	    $sql  = "CALL sp_hr_management_get_headcount_and_turnover_group_title_appr(:month_from, :year_from, :month_to, :year_to, :department,:team,:title,:area,:regional_market) ";
	    $stmt               = $db->prepare($sql);

        $stmt->bindParam('month_from', $params['month_from'],PDO::PARAM_INT);
        $stmt->bindParam('year_from', $params['year_from'], PDO::PARAM_INT);
        $stmt->bindParam('month_to', $params['month_to'], PDO::PARAM_INT);
        $stmt->bindParam('year_to', $params['year_to'], PDO::PARAM_INT);
        $stmt->bindParam('department', $params['department'], PDO::PARAM_STR);
        $stmt->bindParam('team', $params['team'], PDO::PARAM_STR);
        $stmt->bindParam('title', $params['title'] , PDO::PARAM_STR);
        $stmt->bindParam('area', $params['area'], PDO::PARAM_STR);
        $stmt->bindParam('regional_market', $params['regional_market'] , PDO::PARAM_STR);

        $stmt->execute();
        $list = $stmt->fetchAll();
        $stmt->closeCursor();

        
        
        return $list;
	}

	public function get_seniority($year,$month,$title){
		$db = Zend_Registry::get('db');
		// echo $month1;die;
		
		$db = Zend_Registry::get('db');
		$stmt = $db->query('CALL sp_hr_management_get_seniority_by_title('.$year.','.$month.','.$title.')');
        $list     = $stmt->fetchAll();
        $stmt->closeCursor();
        return $list;
	}

	public function get_new_staff($month,$year){
		$db = Zend_Registry::get('db');
		$stmt = $db->query('CALL sp_hr_management_new_staff('.$year.','.$month.')');
        $list     = $stmt->fetchAll();
        $stmt->closeCursor();
        return $list;
	}

	public function get_staff_turnover_all($year){
		$db = Zend_Registry::get('db');
		
		$query = 'DROP TABLE IF EXISTS hr_management_turnover_all ;
					CREATE TABLE hr_management_turnover_all
				AS
				(
				SELECT CONCAT(DATE_FORMAT(aDate, "%b")) Monthtex, MIN(aDate) Mindate, MAX(aDate) Maxdate
				FROM (
				  SELECT @maxDate - INTERVAL (a.a+(10*b.a)+(100*c.a)+(1000*d.a)) DAY aDate FROM
				  (SELECT 0 AS a UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3
				   UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL
				   SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) a, 
				  (SELECT 0 AS a UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3
				   UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL
				   SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) b, 
				  (SELECT 0 AS a UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3
				   UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL
				   SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) c, 
				  (SELECT 0 AS a UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3
				   UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL
				   SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) d, 
				  (SELECT /* truyền biến Year*/ @year :=  '.$year.',  @minDate := CONCAT(@year, "-01-01"), @maxDate := IF(CONCAT(@year, "-12-31") > LAST_DAY(NOW()), LAST_DAY(NOW()), CONCAT(@year, "-12-31"))  ) e
				) f
				WHERE aDate BETWEEN @minDate AND @maxDate
				GROUP BY CONCAT(DATE_FORMAT(aDate, "%b"))
			
				);';

				// echo $query;die;
		$db->query($query);
     

        // die;

        $sql = 'SELECT a.Monthtex, a.Mindate, a.Maxdate, a.Newstaff, a.Off, COUNT(b.staff_id) transfer
				FROM
				(
				SELECT a.Monthtex, a.Mindate, a.Maxdate, a.Newstaff, COUNT(b.id) Off
				FROM
				(
				SELECT a.Monthtex, a.Mindate, a.Maxdate
				, COUNT(b.id) Newstaff
				FROM hr_management_turnover_all a
				LEFT JOIN 
				(SELECT a.id, a.joined_at FROM staff a INNER JOIN title_mapping_report b ON a.title = b.title_id) b ON b.`joined_at` BETWEEN a.Mindate AND a.Maxdate
				GROUP BY a.Monthtex, a.Mindate, a.Maxdate
				) a
				LEFT JOIN 
				(SELECT a.id, a.off_date FROM staff a INNER JOIN title_mapping_report b ON a.title = b.title_id) b ON b.`off_date` BETWEEN a.Mindate AND a.Maxdate
				GROUP BY a.Monthtex, a.Mindate, a.Maxdate, a.Newstaff
				) a
				LEFT JOIN 
				v_staff_transfer_fix b ON b.title IS NOT NULL AND b.old_title IS NOT NULL AND b.title <> b.old_title AND b.from_date BETWEEN a.Mindate AND a.Maxdate
				GROUP BY a.Monthtex, a.Mindate, a.Maxdate, a.Newstaff, a.Off
				ORDER BY a.Maxdate
				;';


        $stmt = $db->prepare($sql);                       
        $stmt->execute();
        $list = $stmt->fetchAll();
        $stmt->closeCursor();
        $db = $stmt = null;
        // echo "<pre>";print_r($list);die;
        return $list;
	}

	public function get_transfer_report($month,$year){
		$db = Zend_Registry::get('db');
		$stmt = $db->query('CALL sp_hr_management_transfer_report('.$month.','.$year.')');
        $list     = $stmt->fetchAll();
        $stmt->closeCursor();
        return $list;
	}
	public function get_turnover_by_title($year,$title,$department,$team){
		$db = Zend_Registry::get('db');
		$sql  = "CALL sp_hr_management_turnover_all_dept_test(:year,:department,:team,:title) ";
	    $stmt               = $db->prepare($sql);

  
        $stmt->bindParam('year', $year, PDO::PARAM_INT);
        $stmt->bindParam('department', $department, PDO::PARAM_STR);
        $stmt->bindParam('team', $team, PDO::PARAM_STR);
        $stmt->bindParam('title', $title , PDO::PARAM_STR);


        $stmt->execute();
        $list = $stmt->fetchAll();
        $stmt->closeCursor();
        return $list;
	}
	public function get_turnover_by_team($year,$team){
		$db = Zend_Registry::get('db');
		$stmt = $db->query('CALL sp_hr_management_turnover_by_team('.$year.','.$team.')');
        $list     = $stmt->fetchAll();
        $stmt->closeCursor();
        return $list;
	}

	public function get_compare_title($year,$title){
		$db = Zend_Registry::get('db');
		$stmt = $db->query('CALL sp_hr_management_compare('.$year.','.$title.')');
        $list     = $stmt->fetchAll();
        $stmt->closeCursor();
        return $list;
	}

	public function get_turnover_all_dept($year){
		$db = Zend_Registry::get('db');
		$stmt = $db->query('CALL sp_hr_management_turnover_all_dept('.$year.')');
        $list     = $stmt->fetchAll();
        $stmt->closeCursor();
        return $list;
	}

	public function get_total_off_by_title($year,$title){
		$db = Zend_Registry::get('db');
		$stmt = $db->query('CALL sp_hr_management_total_off_by_title('.$year.','.$title.')');
        $list     = $stmt->fetchAll();
        $stmt->closeCursor();
        return $list;
	}
	public function get_total_off_by_team($year,$team){
		$db = Zend_Registry::get('db');
		$stmt = $db->query('CALL sp_hr_management_total_off_by_team('.$year.','.$team.')');
        $list     = $stmt->fetchAll();
        $stmt->closeCursor();
        return $list;
	}

	public function get_all_company_seniority($year){
		$db = Zend_Registry::get('db');
		$stmt = $db->query('CALL sp_hr_management_staff_all_seniority('.$year.')');
        $list     = $stmt->fetchAll();
        $stmt->closeCursor();
        return $list;
	}

	public function get_compare_team($year,$team){
		$db = Zend_Registry::get('db');
		$stmt = $db->query('CALL sp_hr_management_compare_by_team('.$year.','.$team.')');
        $list     = $stmt->fetchAll();
        $stmt->closeCursor();
        return $list;
	}

	public function get_seniority_all($params){
		$db = Zend_Registry::get('db');

				 // echo 'CALL sp_hr_management_get_seniority('.$params['year'].','.$params['month'].','.$params['department'].',"'.$params['team'].'","'.$params['title'].'")';die;

	    $sql  = "CALL sp_hr_management_get_seniority(:year, :month,:department,:team,:title) ";
	    $stmt               = $db->prepare($sql);

        $stmt->bindParam('month', $params['month'], PDO::PARAM_INT);
        $stmt->bindParam('year', $params['year'], PDO::PARAM_INT);
        $stmt->bindParam('department', $params['department'], PDO::PARAM_STR);
        $stmt->bindParam('team', $params['team'], PDO::PARAM_STR);
        $stmt->bindParam('title', $params['title'] , PDO::PARAM_STR);


        $stmt->execute();
        $list = $stmt->fetchAll();
        $stmt->closeCursor();

        
        
        return $list;
	}

	public function get_transfer_by_year($year){
		$db = Zend_Registry::get('db');
		$stmt = $db->query('CALL sp_hr_management_get_transfer_by_year('.$year.')');
        $list     = $stmt->fetchAll();
        $stmt->closeCursor();
        return $list;
	}

	public function get_staff_transfer_all($params){
		$db = Zend_Registry::get('db');

				 // echo 'CALL sp_hr_management_get_headcount_and_turnover_group_title_test('.$params['month_from'].','.$params['year_from'].','.$params['month_to'].','.$params['year_to'].','.$params['department'].',"'.$params['team'].'","'.$params['title'].'")';die;

	    $sql  = "CALL sp_hr_management_transfer_staff(:year_from,:month_from , :year_to,:month_to ,:department,:team,:title) ";
	    $stmt               = $db->prepare($sql);

        $stmt->bindParam('month_from', $params['month_from'],PDO::PARAM_INT);
        $stmt->bindParam('year_from', $params['year_from'], PDO::PARAM_INT);
        $stmt->bindParam('month_to', $params['month_to'], PDO::PARAM_INT);
        $stmt->bindParam('year_to', $params['year_to'], PDO::PARAM_INT);
        $stmt->bindParam('department', $params['department'], PDO::PARAM_STR);
        $stmt->bindParam('team', $params['team'], PDO::PARAM_STR);
        $stmt->bindParam('title', $params['title'] , PDO::PARAM_STR);


        $stmt->execute();
        $list = $stmt->fetchAll();
        $stmt->closeCursor();

        
        
        return $list;
	}

	public function getHeadcount($params){

		$nest = "SELECT 
				p.id, 
				p.joined_at joined_at_start,
				(p.off_date - INTERVAL 1 DAY) off_date_start,
				IFNULL(s.from_date, p.joined_at) joined_at,
                                (p.off_date) off_date_start2,
				IFNULL(s.to_date, (p.off_date - INTERVAL 1 DAY)) off_date,
                                IFNULL(s.to_date, (p.off_date)) off_date2,
				-- IFNULL(s.title, p.title) title,
				IFNULL(s.team, p.team) team,
				IFNULL(s.department, p.department) department,
				IFNULL(s.regional_market, p.regional_market) regional_market,
                                IFNULL(s.area, rm.area_id) `area`,
				CASE 
				WHEN s.to_date IS NULL AND s.title IS NOT NULL AND s.title <> p.title THEN p.title
				WHEN s.title IS NULL THEN p.title
				WHEN s.title IS NOT NULL THEN s.title
				ELSE 0 END title,
                                p.date_off_purpose_reason,
                                p.company_id
				FROM staff p
				LEFT JOIN v_staff_transfer_fix_ballack s ON s.staff_id = p.id
                                LEFT JOIN `regional_market` AS rm ON rm.id=p.regional_market
				";

		$db     = Zend_Registry::get("db");
		$select = $db->select();


		$arrCols = array(
		    'p.id', 
		    'p.joined_at', 
		    'p.off_date', 
		    'p.title', 
		    'a.YearNo', 
		    'a.MonthNo', 
		    'a.QuarterNo', 
			'staff_work'   => '(CASE WHEN (DATE(p.joined_at) <= a.ToDate AND (DATE(p.off_date) >= a.ToDate OR p.off_date IS NULL)) THEN 1 ELSE 0 END)',
			'staff_off'    => '(CASE WHEN (DATE(p.off_date2) >= a.FromDate AND (DATE(p.off_date2) <= a.ToDate) AND p.off_date_start2 IS NOT NULL AND p.off_date_start2 >= a.FromDate AND p.off_date_start2 <= a.ToDate) THEN 1 ELSE 0 END)',
                        'involuntary'    => '(CASE WHEN (DATE(p.off_date) >= a.FromDate AND (DATE(p.off_date) <= a.ToDate) AND p.off_date_start IS NOT NULL AND p.off_date_start >= a.FromDate AND p.off_date_start <= a.ToDate AND p.date_off_purpose_reason = 2) THEN 1 ELSE 0 END)',
			'voluntary'    => '(CASE WHEN (DATE(p.off_date) >= a.FromDate AND (DATE(p.off_date) <= a.ToDate) AND p.off_date_start IS NOT NULL AND p.off_date_start >= a.FromDate AND p.off_date_start <= a.ToDate AND p.date_off_purpose_reason <> 2) THEN 1 ELSE 0 END)',
                        'seniority' => '(TIMESTAMPDIFF(MONTH, p.joined_at, a.ToDate))'
		);

		$sub_select = "
                    select YEAR(aDate) YearNo, MONTH(aDate) MonthNo, CEIL(MONTH(aDate) / 3) QuarterNo, MIN(aDate) FromDate, MAX(aDate) ToDate from (
                      select @maxDate - interval (a.a+(10*b.a)+(100*c.a)+(1000*d.a)) day aDate from
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) a, /*10 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) b, /*100 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) c, /*1000 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) d, /*10000 day range*/
                      (select  @minDate :=  CONCAT('".$params['from']."'), @maxDate := CASE WHEN CONCAT('".$params['to']."') <= DATE(NOW()) THEN CONCAT('".$params['to']."') ELSE DATE(NOW()) END) e
                    ) f
                    where aDate between @minDate and @maxDate
                    GROUP BY YEAR(aDate), MONTH(aDate)
                    ";

        $select->from(array('p'=> new Zend_Db_Expr('(' . $nest . ')')), $arrCols);
        $select->joinLeft(array('t'=> 'team'), 't.id = p.title',array());
        $select->joinLeft(array('t2'=> 'team'), 't2.id = t.parent_id',array());
        $select->joinLeft(array('t3'=> 'team'), 't3.id = t2.parent_id',array());
        $select->joinCross(array('a' => new Zend_Db_Expr('(' . $sub_select . ')')), NULL, NULl);
        
        if(!empty($params['not_department'])){
            $select->where('t3.id NOT IN (?)', $params['not_department']);
        }
        
        if(!empty($params['not_company'])){
            $select->where('p.company_id NOT IN (?)', $params['not_company']);
        }

        if(!empty($params['area_id'])){
        	$select->where('p.area IN (?)', $params['area_id']);
        }

        if(!empty($params['title'])){
            $select->where('p.title IN (?)', $params['title']);
        }

        if(!empty($params['team'])){
            $select->where('p.team IN (?)', $params['team']);
        }

        if(!empty($params['department'])){
            $select->where('p.department IN (?)', $params['department']);
        }

        // Main SELECT
        $main_select = $db->select();

        $cols = array(
            'p.MonthNo', 
            'p.YearNo', 
            'staff_work' => 'COUNT(DISTINCT (CASE WHEN p.staff_work = 1 THEN p.id END))',
            'staff_off'  => 'COUNT(DISTINCT (CASE WHEN p.staff_off = 1 THEN p.id END))',
            'involuntary'  => 'COUNT(DISTINCT (CASE WHEN p.involuntary = 1 THEN p.id END))',
            'voluntary'  => 'COUNT(DISTINCT (CASE WHEN p.voluntary = 1 THEN p.id END))',
            'p.seniority'
        );

        $main_select->from(array('p'=> new Zend_Db_Expr('(' . $select . ')')), $cols);
        
       	$main_select->where('p.title <> ?', 375);
       	$main_select->where('p.joined_at IS NOT NULL', NULL);
        
        $main_select->group(array('p.MonthNo','p.YearNo'));

        if($_GET['zzz'] == 1){
        	echo $select;
        }
        
        $result  = $db->fetchAll($main_select);
        
		return $result;
	}

	public function getSeniority($params){

		$nest = "SELECT 
				p.id, 
                                p.company_id,
				p.joined_at joined_at_start,
				(p.off_date - INTERVAL 1 DAY) off_date_start,
				IFNULL(s.from_date, p.joined_at) joined_at,
				IFNULL(s.to_date, (p.off_date - INTERVAL 1 DAY)) off_date,
                                (p.off_date) off_date_start2,
                                IFNULL(s.to_date, (p.off_date)) off_date2,
				-- IFNULL(s.title, p.title) title,
				IFNULL(s.team, p.team) team,
				IFNULL(s.department, p.department) department,
				IFNULL(s.regional_market, p.regional_market) regional_market,
                                IFNULL(s.area, rm.area_id) `area`,
				CASE 
				WHEN s.to_date IS NULL AND s.title IS NOT NULL AND s.title <> p.title THEN p.title
				WHEN s.title IS NULL THEN p.title
				WHEN s.title IS NOT NULL THEN s.title
				ELSE 0 END title,
                                p.date_off_purpose_reason
				FROM staff p
				LEFT JOIN v_staff_transfer_fix s ON s.staff_id = p.id
                                LEFT JOIN `regional_market` AS rm ON rm.id=p.regional_market
				";

		$db     = Zend_Registry::get("db");
		$select = $db->select();

                // Tỉ lệ turnover bỏ tính những trường hợp nghĩ với lý do chuyển công tác => không tính staff_off date_off_purpose_reason = 6
		$arrCols = array(
		    'p.id', 
		    'p.joined_at', 
		    'p.off_date', 
		    'p.title', 
		    'a.YearNo', 
		    'a.MonthNo', 
		    'a.QuarterNo', 
			'staff_work'   => '(CASE WHEN (DATE(p.joined_at) <= a.ToDate AND (DATE(p.off_date) >= a.ToDate OR p.off_date IS NULL)) THEN 1 ELSE 0 END)',
			'staff_off'    => '(CASE WHEN (DATE(p.off_date2) >= a.FromDate AND (DATE(p.off_date2) <= a.ToDate) AND p.off_date_start2 IS NOT NULL AND p.off_date_start2 >= a.FromDate AND p.off_date_start2 <= a.ToDate AND p.date_off_purpose_reason <> 6) THEN 1 ELSE 0 END)',
			'seniority' => 'CASE
							WHEN TIMESTAMPDIFF(MONTH, p.joined_at_start, a.ToDate) < 0 THEN 0
							WHEN TIMESTAMPDIFF(MONTH, p.joined_at_start, a.ToDate) <= 3 THEN 1 
							WHEN TIMESTAMPDIFF(MONTH, p.joined_at_start, a.ToDate) <= 6 THEN 2
							WHEN TIMESTAMPDIFF(MONTH, p.joined_at_start, a.ToDate) <= 12 THEN 3
							WHEN TIMESTAMPDIFF(MONTH, p.joined_at_start, a.ToDate) <= 24 THEN 4
							WHEN TIMESTAMPDIFF(MONTH, p.joined_at_start, a.ToDate) <= 36 THEN 5
							WHEN TIMESTAMPDIFF(MONTH, p.joined_at_start, a.ToDate) <= 48 THEN 6
							WHEN TIMESTAMPDIFF(MONTH, p.joined_at_start, a.ToDate) > 48 THEN 7
							ELSE 0 END',
		);

		$sub_select = "
                    select YEAR(aDate) YearNo, MONTH(aDate) MonthNo, CEIL(MONTH(aDate) / 3) QuarterNo, MIN(aDate) FromDate, MAX(aDate) ToDate from (
                      select @maxDate - interval (a.a+(10*b.a)+(100*c.a)+(1000*d.a)) day aDate from
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) a, /*10 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) b, /*100 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) c, /*1000 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) d, /*10000 day range*/
                      (select  @minDate :=  CONCAT('".$params['from']."'), @maxDate := CASE WHEN CONCAT('".$params['to']."') <= DATE(NOW()) THEN CONCAT('".$params['to']."') ELSE DATE(NOW()) END) e
                    ) f
                    where aDate between @minDate and @maxDate
                    GROUP BY YEAR(aDate), MONTH(aDate)
                    ";

        $select->from(array('p'=> new Zend_Db_Expr('(' . $nest . ')')), $arrCols);
        $select->joinLeft(array('t'=> 'team'), 't.id = p.title',array());
        $select->joinLeft(array('t2'=> 'team'), 't2.id = t.parent_id',array());
        $select->joinLeft(array('t3'=> 'team'), 't3.id = t2.parent_id',array());
        $select->joinCross(array('a' => new Zend_Db_Expr('(' . $sub_select . ')')), NULL, NULl);
        
        if(!empty($params['not_department'])){
            $select->where('t3.id NOT IN (?)', $params['not_department']);
        }
        
        if(!empty($params['not_company'])){
            $select->where('p.company_id NOT IN (?)', $params['not_company']);
        }
        
        if(!empty($params['area_id'])){
        	$select->where('p.area IN (?)', $params['area_id']);
        }

        if(!empty($params['title'])){
            $select->where('p.title IN (?)', $params['title']);
        }

        if(!empty($params['team'])){
            $select->where('p.team IN (?)', $params['team']);
        }

        if(!empty($params['department'])){
            $select->where('p.department IN (?)', $params['department']);
        }
        
        // Main SELECT
        $main_select = $db->select();

        $cols = array(
            'p.MonthNo', 
            'p.YearNo', 
            'staff_work' 			=> 'COUNT(DISTINCT (CASE WHEN p.staff_work = 1 THEN p.id END))',
            'staff_off'  			=> 'COUNT(DISTINCT (CASE WHEN p.staff_off = 1 THEN p.id END))', 
            'p.seniority',
            'count_seniority' 		=> 'COUNT(DISTINCT (CASE WHEN p.staff_work = 1 THEN p.id END))',
            'count_seniority_off' 	=> 'COUNT(DISTINCT (CASE WHEN p.staff_off = 1 THEN p.id END))',
        );

        $main_select->from(array('p'=> new Zend_Db_Expr('(' . $select . ')')), $cols);
        
       	$main_select->where('p.title <> ?', 375);
        
        $main_select->group(array('p.MonthNo','p.YearNo', 'p.seniority'));
        
        $result  = $db->fetchAll($main_select);
        
        
		return $result;
	}

	public function getHeadcountTotal($params){

		$nest = "SELECT 
				p.id, 
                                p.company_id,
				p.joined_at joined_at_start,
                                (p.off_date - INTERVAL 1 DAY) off_date_start,
				s.to_date to_date_tranfer,
				IFNULL(s.from_date, p.joined_at) joined_at,
				IFNULL(s.to_date, (p.off_date - INTERVAL 1 DAY)) off_date,
                                (p.off_date) off_date_start2,
                                IFNULL(s.to_date, (p.off_date)) off_date2,
				-- IFNULL(s.title, p.title) title,
				IFNULL(s.team, p.team) team,
				IFNULL(s.department, p.department) department,
				IFNULL(s.regional_market, p.regional_market) regional_market,
                                IFNULL(s.area, rm.area_id) `area`,
				CASE 
				WHEN s.to_date IS NULL AND s.title IS NOT NULL AND s.title <> p.title THEN p.title
				WHEN s.title IS NULL THEN p.title
				WHEN s.title IS NOT NULL THEN s.title
				ELSE 0 END title
				FROM staff p
				LEFT JOIN v_staff_transfer_fix_ballack s ON s.staff_id = p.id
                                LEFT JOIN `regional_market` AS rm ON rm.id=p.regional_market
				";

		$db     = Zend_Registry::get("db");
		$select = $db->select();


		$arrCols = array(
		    'p.id', 
		    'p.joined_at', 
		    'p.off_date', 
		    'p.title', 
		    'a.YearNo', 
		    'a.MonthNo', 
		    'a.QuarterNo', 
			'staff_work'   => '(CASE WHEN (DATE(p.joined_at) <= a.ToDate AND (DATE(p.off_date) > a.ToDate OR p.off_date IS NULL)) THEN 1 ELSE 0 END)',
			'staff_off'    => '(CASE WHEN (DATE(p.off_date2) >= a.FromDate AND (DATE(p.off_date2) <= a.ToDate) AND p.off_date_start2 IS NOT NULL AND p.off_date_start2 >= a.FromDate AND p.off_date_start2 <= a.ToDate) THEN 1 ELSE 0 END)',
			'staff_new'    => '(CASE WHEN (DATE(p.joined_at_start) >= a.FromDate AND (DATE(p.joined_at_start) <= a.ToDate) AND p.to_date_tranfer IS NULL) THEN 1 ELSE 0 END)',
			'seniority' => '(TIMESTAMPDIFF(MONTH, p.joined_at, a.ToDate))'
		);

		$sub_select = "
                    select YEAR(aDate) YearNo, MONTH(aDate) MonthNo, CEIL(MONTH(aDate) / 3) QuarterNo, MIN(aDate) FromDate, MAX(aDate) ToDate from (
                      select @maxDate - interval (a.a+(10*b.a)+(100*c.a)+(1000*d.a)) day aDate from
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) a, /*10 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) b, /*100 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) c, /*1000 day range*/
                      (select 0 as a union all select 1 union all select 2 union all select 3
                       union all select 4 union all select 5 union all select 6 union all
                       select 7 union all select 8 union all select 9) d, /*10000 day range*/
                      (select  @minDate :=  CONCAT('".$params['from']."'), @maxDate := CASE WHEN CONCAT('".$params['to']."') <= DATE(NOW()) THEN CONCAT('".$params['to']."') ELSE DATE(NOW()) END) e
                    ) f
                    where aDate between @minDate and @maxDate
                    GROUP BY YEAR(aDate), MONTH(aDate)
                    ";

        $select->from(array('p'=> new Zend_Db_Expr('(' . $nest . ')')), $arrCols);
        $select->joinLeft(array('t'=> 'team'), 't.id = p.title',array());
        $select->joinLeft(array('t2'=> 'team'), 't2.id = t.parent_id',array());
        $select->joinLeft(array('t3'=> 'team'), 't3.id = t2.parent_id',array());
        $select->joinCross(array('a' => new Zend_Db_Expr('(' . $sub_select . ')')), NULL, NULl);
        
        if(!empty($params['not_department'])){
            $select->where('t3.id NOT IN (?)', $params['not_department']);
        }
        
        if(!empty($params['not_company'])){
            $select->where('p.company_id NOT IN (?)', $params['not_company']);
        }
        
        if(!empty($params['area_id'])){
        	$select->where('p.area IN (?)', $params['area_id']);
        }

        if(!empty($params['title'])){
            $select->where('p.title IN (?)', $params['title']);
        }

        if(!empty($params['team'])){
            $select->where('p.team IN (?)', $params['team']);
        }

        if(!empty($params['department'])){
            $select->where('p.department IN (?)', $params['department']);
        }

        // Main SELECT
        $main_select = $db->select();

        $cols = array(
            'p.MonthNo', 
            'p.YearNo', 
            'staff_work' => 'COUNT(DISTINCT (CASE WHEN p.staff_work = 1 THEN p.id END))',
            'staff_off'  => 'COUNT(DISTINCT (CASE WHEN p.staff_off = 1 THEN p.id END))',
            'staff_new'  => 'COUNT(DISTINCT (CASE WHEN p.staff_new = 1 THEN p.id END))',
            'p.title'
        );

        $main_select->from(array('p'=> new Zend_Db_Expr('(' . $select . ')')), $cols);
        
       	$main_select->where('p.title <> ?', 375);
        
        $main_select->group(array('p.MonthNo','p.YearNo','p.title'));
        
        $result  = $db->fetchAll($main_select);
        
		return $result;
	}

	public function getTranfer($params){

		$db     = Zend_Registry::get("db");
		$select = $db->select();
		    
		$arrCols = array(
		    'month' 	=> 'MONTH(p.from_date)', 
		    'year'		=> 'YEAR(p.from_date)', 
		    'sum' 		=> 'COUNT(p.staff_id)',
		    'p.from_date', 
		    'p.area', 
		    'p.title'
		);
		
		$select->from(array('p'=> 'v_staff_transfer_fix'), $arrCols);
		$select->where('p.old_title <> p.title', NULL);
		
		$select->group(array('MONTH(p.from_date)', 'YEAR(p.from_date)', 'p.title'));
		
		$result  = $db->fetchAll($select);
		
		return $result;
	}
	public function exportReport($from,$to,$department){
		// $db = Zend_Registry::get('db');
	    // $sql  = "CALL sp_report_hr(:_from,:_to) ";
	    if (!empty($department)) {
	    	$department_ = implode($department, ',');
	    }
	    
	    $db = Zend_Registry::get('db');
        $select = $db->select();
	    $sql = '

		SELECT
    YEAR("'.$from.'") `year`
    ,DATE_FORMAT("'.$from.'", "%b") `month`
    ,s.`code`
    ,CONCAT(s.firstname," ",s.lastname) staff_name
    ,c.`name` company_name
    ,department.id id_department
    ,department.`name` department
    ,team.`name` team
    ,title.`name` title
    ,a.`name` area  
    ,rm.`name` province
    ,district.`name` district
		,IF(s.phone_number is NULL or s.phone_number = "",NULL,CONCAT(s.phone_number,"","")) phone_number
		,s.email
		-- ,s.contract_signed_at
		-- ,ct.`name` contract_name
    ,IF(s.is_officer = 1,"X",NULL) is_officer
    ,cg.name as `company_group_name`
    ,s.joined_at
    ,s.off_date
    ,IF(s.off_date > "'.$to.'",NULL,sor.`name`) off_date_reason
    ,IF(s.off_date > "'.$to.'",NULL,s.date_off_purpose_detail) off_date_reason_detail
    ,IF(s.`status` = 1 OR s.off_date > "'.$to.'","On",IF(s.`status` = 0,"Off",IF(s.`status` = 3,"Childbearing","Temporary Off"))) `status`
    ,TIMESTAMPDIFF(MONTH, s.joined_at, DATE(DATE_ADD("'.$from.'", INTERVAL -1 DAY))) dau_thang
		,IF(s.off_date is null or s.off_date > "'.$to.'",TIMESTAMPDIFF(MONTH, s.joined_at, DATE(DATE_ADD("'.$to.'", INTERVAL -1 DAY))),TIMESTAMPDIFF(MONTH, s.joined_at, s.off_date)) cuoi_thang
    ,IF(s.joined_at BETWEEN "'.$from.'" AND "'.$to.'","X",NULL) new_staff
    ,IF(transfer.staff_id > 1,"X",NULL) transfer
    ,IF(s.title IN (182,283,419,420) AND TIMESTAMPDIFF(MONTH, s.joined_at, DATE(DATE_ADD("'.$from.'", INTERVAL -1 DAY))) < 3 ,"X,",
        IF((s.title) NOT IN (182,283,419,420) AND TIMESTAMPDIFF(MONTH, s.joined_at, DATE(DATE_ADD("'.$from.'", INTERVAL -1 DAY))) < 2,"X",NULL)) thu_viec_dau_thang
    ,IF(s.title IN (182,283,419,420) AND TIMESTAMPDIFF(MONTH, s.joined_at, DATE(DATE_ADD("'.$from.'", INTERVAL -1 DAY))) > 3 ,"X,",
        IF((s.title) NOT IN (182,283,419,420) AND TIMESTAMPDIFF(MONTH, s.joined_at, DATE(DATE_ADD("'.$from.'", INTERVAL -1 DAY))) > 2,"X",NULL)) chinh_thuc_dau_thang
    ,IF(s.joined_at < "'.$from.'","X",NULL) tong_nhan_su_dau_thang
    ,IF(s.`status` in (1,2,3) OR s.off_date <= "'.$to.'" OR s.off_date > "'.$to.'","X",NULL) tong_nhan_su_cuoi_thang
    ,IF(s.off_date is not NULL AND s.off_date <= "'.$to.'" AND TIMESTAMPDIFF(MONTH, s.joined_at,s.off_date) < 3,"X",NULL) "OFF < 3month"
    ,IF(s.off_date is not NULL AND s.off_date <= "'.$to.'" AND TIMESTAMPDIFF(MONTH, s.joined_at,s.off_date) >= 3 AND 
                                                         TIMESTAMPDIFF(MONTH, s.joined_at,s.off_date) < 6,"X",NULL) "OFF > 3month <= 6month"
    ,IF(s.off_date is not NULL AND s.off_date <= "'.$to.'" AND TIMESTAMPDIFF(MONTH, s.joined_at,s.off_date) >= 6 AND 
                                                         TIMESTAMPDIFF(MONTH, s.joined_at,s.off_date) < 12,"X",NULL) "OFF > 6month <= 12month"
    ,IF(s.off_date is not NULL AND s.off_date <= "'.$to.'" AND TIMESTAMPDIFF(MONTH, s.joined_at,s.off_date) >= 12 AND 
                                                         TIMESTAMPDIFF(MONTH, s.joined_at,s.off_date) < 24,"X",NULL) "OFF > 12month <= 24month"
    ,IF(s.off_date is not NULL AND s.off_date <= "'.$to.'" AND TIMESTAMPDIFF(MONTH, s.joined_at,s.off_date) >= 24 AND 
                                                         TIMESTAMPDIFF(MONTH, s.joined_at,s.off_date) < 48,"X",NULL) "OFF > 24month <= 48month"
    ,IF(s.off_date is not NULL AND s.off_date <= "'.$to.'" AND TIMESTAMPDIFF(MONTH, s.joined_at,s.off_date) >= 48,"X",NULL) "OFF > 48month"
    ,IF(s.date_off_purpose_reason = 2,"X",NULL) involuntary
    ,IF(s.date_off_purpose_reason <> 2,"X",NULL) voluntary
    ,IF((s.off_date is NULL OR s.off_date > "'.$to.'") AND TIMESTAMPDIFF(MONTH, s.joined_at,"'.$to.'") < 3,"X",NULL) "ON < 3month"
    ,IF((s.off_date is NULL OR s.off_date > "'.$to.'") AND TIMESTAMPDIFF(MONTH, s.joined_at,"'.$to.'") >= 3 AND 
                                                         TIMESTAMPDIFF(MONTH, s.joined_at,"'.$to.'") < 6,"X",NULL) "ON > 3month <= 6month"
    ,IF((s.off_date is NULL OR s.off_date > "'.$to.'") AND TIMESTAMPDIFF(MONTH, s.joined_at,"'.$to.'") >= 6 AND 
                                                         TIMESTAMPDIFF(MONTH, s.joined_at,"'.$to.'") < 12,"X",NULL) "ON > 6month <= 12month"
    ,IF((s.off_date is NULL OR s.off_date > "'.$to.'") AND TIMESTAMPDIFF(MONTH, s.joined_at,"'.$to.'") >= 12 AND 
                                                         TIMESTAMPDIFF(MONTH, s.joined_at,"'.$to.'") < 24,"X",NULL) "ON > 12month <= 24month"
		,IF((s.off_date is NULL OR s.off_date > "'.$to.'") AND TIMESTAMPDIFF(MONTH, s.joined_at,"'.$to.'") >= 24 AND 
                                                         TIMESTAMPDIFF(MONTH, s.joined_at,"'.$to.'") < 48,"X",NULL) "ON > 24month <= 48month"
    ,IF((s.off_date is NULL OR s.off_date > "'.$to.'") AND TIMESTAMPDIFF(MONTH, s.joined_at,"'.$to.'") >= 48,"X",NULL) "ON > 48month"
    ,v_contract.from_date contract_signed_at
    ,v_contract.contract_name contract_name
FROM (
    SELECT *
    FROM
    (
        SELECT 
        p.id, 
        p.code,
        p.firstname,
        p.lastname,
        p.phone_number,
        p.email,
        p.is_officer,
        p.date_off_purpose_detail,
        p.status,
        p.title title_now,
        -- p.company_id,
        p.contract_term,
        p.district,
        p.joined_at,
        p.off_date,
        (p.off_date - INTERVAL 1 DAY) off_date_start,
        IFNULL(s.from_date, p.joined_at) joined_at_transfer,
        (p.off_date) off_date_start2,
        IFNULL(s.to_date, (p.off_date - INTERVAL 1 DAY)) off_date_transfer,
        IFNULL(s.to_date, (p.off_date)) off_date_transfer2,
        -- IFNULL(s.title, p.title) title,
        IFNULL(s.company_id, p.company_id) company_id,
        IFNULL(s.team, p.team) team,
        IFNULL(s.department, p.department) department,
        IFNULL(s.regional_market, p.regional_market) regional_market,
        IFNULL(s.area, rm.area_id) `area`,
        CASE 
        WHEN s.to_date IS NULL AND s.title IS NOT NULL AND s.title <> p.title THEN p.title
        WHEN s.title IS NULL THEN p.title
        WHEN s.title IS NOT NULL THEN s.title
        ELSE 0 END title,
        p.date_off_purpose_reason
        FROM staff p
        LEFT JOIN v_staff_transfer_fix_ballack s ON s.staff_id = p.id
        LEFT JOIN `regional_market` AS rm ON rm.id=p.regional_market
    ) p
    WHERE ( off_date_start2 IS NULL AND p.joined_at_transfer <= "'.$to.'" AND (off_date_transfer2 >= "'.$to.'" OR off_date_transfer2 IS NULL) )
        OR ( off_date_start2 IS NOT NULL AND p.joined_at_transfer <= "'.$to.'" AND (off_date_transfer2 >= "'.$from.'") )
) s
LEFT JOIN company c
    ON c.id = s.company_id
LEFT JOIN contract_term ct
	ON ct.id = s.contract_term
LEFT JOIN v_staff_contract_insurance v_contract
	ON v_contract.staff_id = s.id AND v_contract.from_date <= "'.$to.'"
LEFT JOIN team department
    ON department.id = s.department
LEFT JOIN team team
    ON team.id = s.team
LEFT JOIN team title    
    ON title.id = s.title
left JOIN company_group cg
    ON title.company_group = cg.id
LEFT JOIN regional_market rm
    ON rm.id = s.regional_market
LEFT JOIN regional_market district
    ON district.id = s.district  
LEFT JOIN area a
    ON a.id = rm.area_id
LEFT JOIN staff_offdate_reason sor
    ON sor.id = s.date_off_purpose_reason
LEFT JOIN 
	(
		SELECT MAX(from_date) date_transfer,staff_id,v.old_department id_old_department,department.`name` old_department,team.`name` old_team,title.`name` old_title
FROM v_staff_transfer_fix v
JOIN team title 
	ON title.id = v.old_title
JOIN team team 
	ON team.id = v.old_team
JOIN team department
	ON department.id = v.old_department
WHERE v.from_date > "'.$from.'" AND v.to_date is NULL
GROUP BY v.staff_id
ORDER BY v.staff_id
	) as rs ON rs.staff_id = s.id
LEFT JOIN 
    (SELECT staff_id FROM staff_transfer WHERE from_date BETWEEN "'.$from.'" AND "'.$to.'") as transfer
    ON transfer.staff_id = s.id
WHERE ( s.off_date >= "'.$from.'" OR s.off_date is null ) AND joined_at <= "'.$to.'"
AND s.title <> 375 AND s.company_id NOT IN (7, 5) AND department.id NOT IN (525, 321, 479, 321) AND c.id NOT IN (3,4,5) AND (s.date_off_purpose_reason <> 6 OR s.date_off_purpose_reason IS NULL)
AND v_contract.print_type = 1
GROUP BY `code`
';
            
		if (!empty($department)) {
			$sql .= 'HAVING id_department IN ('.$department_.');';
		}
                
                $userStorage = Zend_Auth::getInstance()->getStorage()->read();
                if ($userStorage->email == 'tam.do@oppomobile.vn') {
                    echo $sql;
                    exit;
                }
                
	    $result = $db->fetchAll($sql);
	    
        return $result;
        
	}
}