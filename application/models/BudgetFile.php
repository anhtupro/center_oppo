<?php

class Application_Model_BudgetFile extends Zend_Db_Table_Abstract
{
    protected $_name = 'budget_hr';
    
    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('p'=>'budget_hr'),array(
                new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
                'p.*',
                'category_group' => 'rtg.title',
                'category_type' => 'rt.title',
                'supplier_name' => 'sp.title',
                'contract_title' => 'c.name',
                'project_title' => 'pr.title',
            ))
            ->joinLeft(array('rt'=>'request_type'),'rt.id = p.category',array())
            ->joinLeft(array('rtg'=>'request_type_group'),'rtg.id = p.category_group',array())
            ->joinLeft(array('c'=>'budget_contract'),'c.id = p.contract_id',array()) 
            ->joinLeft(array('pr'=>'project'),'pr.id = p.project_id',array())
            ->joinLeft(array('sp'=>'supplier'),'sp.id = p.supplier_id',array())
            ->where('p.del = ?', 0)
            ->order('p.id DESC');
            
        ;

        if(isset($params['name']) AND $params['name']){
            $select->where('p.name LIKE ?','%'.$params['name'].'%');
        }
        if(isset($params['amount_down']) AND $params['amount_down']){
            $select->where('p.amount < ?',$params['amount_down']);
        }
        if(isset($params['amount_up']) AND $params['amount_up']){
            $select->where('p.amount > ?',$params['amount_up']);
        }
        if(isset($params['category_group']) AND $params['category_group']){
            $select->where('p.category_group = ?',$params['category_group']);
        }
        if(isset($params['category']) AND $params['category']){
            $select->where('p.category = ?',$params['category']);
        }
        if(isset($params['budget_type']) AND $params['budget_type']){
            $select->where('p.budget_type = ?',$params['budget_type']);
        }
        if(isset($params['project_id']) AND $params['project_id']){
            $select->where('p.project_id = ?',$params['project_id']);
        }
        if(isset($params['supplier_id']) AND $params['supplier_id']){
            $select->where('p.supplier_id = ?',$params['supplier_id']);
        }
        if(isset($params['from_date']) AND $params['from_date']){
            $select->where('p.from_date >= ?',$params['from_date']);
        }
        if(isset($params['to_date']) AND $params['to_date']){
            $select->where('p.to_date < ?',$params['to_date']);
        }
        if ($limit) {
            $select->limitPage($page, $limit);
        }
        $result = $db->fetchAll($select);
        
        if ($limit) {
            $total = $db->fetchOne("select FOUND_ROWS()");
        }
        
        return $result;
    }  
}
