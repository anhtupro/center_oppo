<?php
class Application_Model_CompanyGroup extends Zend_Db_Table_Abstract
{
    protected $_name = 'company_group';

    public function getAll()
    {
        $db = Zend_Registry::get('db');

        $stmt = $db->prepare('SELECT * FROM company_group WHERE parent IS NOT NULL ORDER BY id');
        $stmt->execute();
        $data = $stmt->fetchAll();

        $stmt->closeCursor();
        $db = $stmt = null;
        
        return $data;
    }

    

    public function getTitle($params = array())
    {
        $db = Zend_Registry::get('db');

        $stmt = $db->prepare('CALL `PR_get_title_company_group`(:department, :team, :title, :company_group, :limit, :offset, :id)');

        $params['id'] = empty($params['id'])?null:$params['id'];
        $params['company_group'] = empty($params['company_group'])?null:$params['company_group'];
        $params['limit'] = empty($params['limit'])?null:$params['limit'];
        $params['department'] = empty($params['department'])?null:$params['department'];
        $params['team'] = empty($params['team'])?null:$params['team'];
        $params['title'] = empty($params['title'])?null:$params['title'];
        
        $stmt->bindParam("department", $params['department'], PDO::PARAM_INT);
        $stmt->bindParam("id", $params['id'], PDO::PARAM_INT);
        $stmt->bindParam("team", $params['team'], PDO::PARAM_INT);
        $stmt->bindParam("title", $params['title'], PDO::PARAM_INT);
        $stmt->bindParam("company_group", $params['company_group'], PDO::PARAM_INT);
        $stmt->bindParam("limit", $params['limit'], PDO::PARAM_INT);
        $stmt->bindParam("offset", $params['offset'], PDO::PARAM_INT);

        $stmt->execute();
        
        $data = array();
        $data['data'] = $stmt->fetchAll();
        $stmt->closeCursor();
        $data['total'] = $db->fetchOne("SELECT FOUND_ROWS()");
        $stmt = $db = null;
        return $data;
    }

    public function getStaff($params = array())
    {
        $db = Zend_Registry::get('db');

        $stmt = $db->prepare('CALL `PR_get_staff_company_group`(:name, :code, :department, :team, :title, :company_group, :limit, :offset)');

        $params['name'] = empty($params['name'])?null:$params['name'];
        $params['code'] = empty($params['code'])?null:$params['code'];
        $params['company_group'] = empty($params['company_group'])?null:$params['company_group'];
        $params['limit'] = empty($params['limit'])?null:$params['limit'];
        $params['department'] = empty($params['department'])?null:$params['department'];
        $params['team'] = empty($params['team'])?null:$params['team'];
        $params['title'] = empty($params['title'])?null:$params['title'];
        
        $stmt->bindParam("name", $params['name'], PDO::PARAM_STR);
        $stmt->bindParam("code", $params['code'], PDO::PARAM_STR);

        $stmt->bindParam("department", $params['department'], PDO::PARAM_INT);
        $stmt->bindParam("team", $params['team'], PDO::PARAM_INT);
        $stmt->bindParam("title", $params['title'], PDO::PARAM_INT);
        $stmt->bindParam("company_group", $params['company_group'], PDO::PARAM_INT);
        $stmt->bindParam("limit", $params['limit'], PDO::PARAM_INT);
        $stmt->bindParam("offset", $params['offset'], PDO::PARAM_INT);

        $stmt->execute();
        
        $data = array();
        $data['data'] = $stmt->fetchAll();
        $stmt->closeCursor();
        $data['total'] = $db->fetchOne("SELECT FOUND_ROWS()");
        $stmt = $db = null;
        return $data;
    }

    public function insertTeam($params = array())
    {
        $db = Zend_Registry::get('db');

        $stmt = $db->prepare('CALL `PR_set_company_group_for_team`(:company_group, :department, :team, :title)');

        $params['company_group'] = empty($params['company_group'])?null:$params['company_group'];
        $params['department'] = empty($params['department'])?null:$params['department'];
        $params['team'] = empty($params['team'])?null:$params['team'];
        $params['title'] = empty($params['title'])?null:$params['title'];

        $stmt->bindParam("department", $params['department'], PDO::PARAM_INT);
        $stmt->bindParam("team", $params['team'], PDO::PARAM_INT);
        $stmt->bindParam("title", $params['title'], PDO::PARAM_INT);
        $stmt->bindParam("company_group", $params['company_group'], PDO::PARAM_INT);

        $stmt->execute();
        $stmt = $db = null;
        return $data;
    }

    public function setCompanyGroup($params = array())
    {
        $db = Zend_Registry::get('db');

        $sql = "INSERT INTO `company_group_map` (`company_group`, `title`)
                    VALUES (:company_group, :title)
                    ON DUPLICATE KEY UPDATE `company_group` = VALUES(`company_group`)";

        $stmt = $db->prepare($sql);
        $stmt->bindParam('company_group', $params['company_group'], PDO::PARAM_INT);
        $stmt->bindParam('title', $params['title'], PDO::PARAM_INT);

        $stmt->execute();
        $stmt->closeCursor();
        $stmt = $db = null;
    }

    public function removeStaff($staff_id)
    {
        if(!empty($staff_id))
        {
            $db = Zend_Registry::get('db');

            $sql = "DELETE FROM `staff_company_group` WHERE staff_id = " . $staff_id . " LIMIT 1";
            $db->query($sql);
            $db = null;
        }
    }

    public function removeTitle($id)
    {
        if(!empty($id))
        {
            $db = Zend_Registry::get('db');

            $sql = "DELETE FROM `company_group_map` WHERE id = " . $id . " LIMIT 1";
            $db->query($sql);
            $db = null;
        }
    }

    public function insertStaff($params = array())
    {
        if(!empty($params['staff_id']) && !empty($params['company_group']))
        {
            $db = Zend_Registry::get('db');

            $sql = "INSERT INTO `staff_company_group` (`staff_id`, `company_group_id`) 
                        VALUES (:staff_id, :compary_group)
                    ON DUPLICATE KEY UPDATE `company_group_id` = VALUES(`company_group_id`)";
            
            $stmt = $db->prepare($sql);

            $stmt->bindParam('staff_id', $params['staff_id'], PDO::PARAM_INT);
            $stmt->bindParam('compary_group', $params['company_group'], PDO::PARAM_INT);
            $stmt->execute();

            $stmt->closeCursor();
            $stmt = $db = null;
        }
    }

    public function getGroupByTitle($title)
    {
        $db = Zend_Registry::get('db');

        if(!empty($title))
        {
            $sql = "SELECT cgm.*, cg.name as `company_group_name`
                    FROM `company_group_map` cgm
                    JOIN `company_group` cg ON cgm.company_group = cg.id
                    WHERE cgm.title = :title";
            $stmt = $db->prepare($sql);
            $stmt->bindParam("title", $title, PDO::PARAM_STR);

            $stmt->execute();
            $data = $stmt->fetch();

            $stmt->closeCursor();
            $stmt = $db = null;

            return $data['company_group_name'];        
        }
        else 
        {
            return null;
        }
    }

    public function getGroupParentByTitle($title)
    {
        $db = Zend_Registry::get('db');

        if(!empty($title))
        {
            $sql = "SELECT cgm.*, cg2.name as `company_group_name`
                    FROM `company_group_map` cgm
                    JOIN `company_group` cg ON cgm.company_group = cg.id
                    JOIN `company_group` cg2 ON cg.parent = cg2.id
                    WHERE cgm.title = :title";
            $stmt = $db->prepare($sql);
            $stmt->bindParam("title", $title, PDO::PARAM_STR);

            $stmt->execute();
            $data = $stmt->fetch();

            $stmt->closeCursor();
            $stmt = $db = null;

            return $data['company_group_name'];        
        }
        else 
        {
            return null;
        }
    }

    public function getManager($staff)
    {
        if(!empty($staff))
        {
            $db = Zend_Registry::get('db');
            $data = array();
            if($staff->is_officer != 1)
            {
                if(in_array($staff->title, array(PGPB_TITLE, CHUYEN_VIEN_BAN_HANG_TITLE, PB_SALES_TITLE, SALES_TITLE)) )
                {
                    $sql = "SELECT 
                                CONCAT(st.firstname, ' ', st.lastname) as `staff_name`,
                                st.department as `department`,
                                st.team as `team`,
                                st.title as `title`,
                                asm.*
                            FROM `regional_market` rm
                            JOIN `area` ar ON rm.area_id = ar.id
                            JOIN `asm` asm ON asm.area_id = ar.id
                            JOIN `staff` st ON st.id = asm.staff_id
                            WHERE rm.id = :regional_market AND st.group_id IN (5, 16)
                            GROUP BY st.id";

                    $stmt = $db->prepare($sql);
                    $stmt->bindParam("regional_market", $staff->regional_market, PDO::PARAM_INT);
                }

                if(!empty($stmt))
                {
                    $stmt->execute();
                    $data = $stmt->fetchAll();
                    $stmt->closeCursor();
                    $stmt = $db = null;
                }
                return $data;
            }
            elseif($staff->is_officer == 1 && !empty($staff->office_id) && !empty($staff->department))
            {
                // $sql = "SELECT 
                //         CONCAT(st.firstname, ' ', st.lastname) as `staff_name`,
                //         st.department as `department`,
                //         st.team as `team`,
                //         st.title as `title`
                //     FROM `staff_permission` sp
                //     JOIN `staff` st ON st.code = sp.staff_code
                //     WHERE sp.office_id = :office_id
                //         AND sp.department_id = :department_id
                //         AND sp.staff_code <> :staff_code
                //         AND sp.is_manager > 0";
                // $stmt = $db->prepare($sql);
                // $stmt->bindParam("staff_code", $staff->code, PDO::PARAM_STR);
                // $stmt->bindParam("department_id", $staff->department, PDO::PARAM_INT);
                // $stmt->bindParam("office_id", $staff->office_id, PDO::PARAM_INT);

                // if(!empty($stmt))
                // {
                //     $stmt->execute();
                //     $data = $stmt->fetchAll();
                //     $stmt->closeCursor();
                //     $stmt = $db = null;
                // }
                // return $data;
            }
        }
        else
        {
            return null;
        }
    }

    public function getLineManager($staff)
    {
        if(!empty($staff))
        {
            $db = Zend_Registry::get('db');
            $data = array();
            if($staff->is_officer != 1)
            {
                if(in_array($staff->title, array(PGPB_TITLE, CHUYEN_VIEN_BAN_HANG_TITLE, PB_SALES_TITLE)))
                {
                    $sql = "SELECT 
                                CONCAT(st.firstname, ' ', st.lastname) as `staff_name`,
                                st.department as `department`,
                                st.team as `team`,
                                st.title as `title`
                            FROM `store_staff_log` stl
                            JOIN `store_staff_log` stl2 ON stl2.store_id = stl.store_id AND stl2.released_at IS NULL
                            JOIN `staff` st ON stl2.staff_id = st.id AND st.off_date IS NULL
                            WHERE stl.staff_id = :staff_id
                                AND st.title = " . SALES_TITLE . " AND stl.released_at IS NULL
                            GROUP BY st.id ";
                    $stmt = $db->prepare($sql);
                    $stmt->bindParam("staff_id", $staff->id, PDO::PARAM_INT);
                }

                if(in_array($staff->title, array(SALES_TITLE)))
                {
                    $sql = "SELECT 
                                CONCAT(st.firstname, ' ', st.lastname) as `staff_name`,
                                st.department as `department`,
                                st.team as `team`,
                                st.title as `title`
                            FROM `store_staff_log` stl
                            JOIN `store_leader_log` sll ON stl.store_id = sll.store_id AND sll.released_at IS NULL
                            JOIN `staff` st ON sll.staff_id = st.id AND st.off_date IS NULL
                            WHERE stl.staff_id = :staff_id
                            GROUP BY st.id ";
                    $stmt = $db->prepare($sql);
                    $stmt->bindParam("staff_id", $staff->id, PDO::PARAM_INT);
                }
            }
            elseif($staff->is_officer == 1 && !empty($staff->office_id) && !empty($staff->department))
            {
                // $sql = "SELECT 
                //             CONCAT(st.firstname, ' ', st.lastname) as `staff_name`,
                //             st.department as `department`,
                //             st.team as `team`,
                //             st.title as `title`
                //         FROM `staff_permission` sp
                //         JOIN `staff` st ON st.code = sp.staff_code
                //         WHERE sp.office_id = :office_id
                //             AND sp.department_id = :department_id
                //             AND sp.staff_code <> :staff_code
                //             AND sp.is_leader > 0";

                // $stmt = $db->prepare($sql);
                // $stmt->bindParam("staff_code", $staff->code, PDO::PARAM_STR);
                // $stmt->bindParam("department_id", $staff->department, PDO::PARAM_INT);
                // $stmt->bindParam("office_id", $staff->office_id, PDO::PARAM_INT);
            }

            if(!empty($stmt))
            {
                $stmt->execute();
                $data = $stmt->fetchAll();
                $stmt->closeCursor();
                $stmt = $db = null;
            }
            return $data;
        }
        else
        {
            return null;
        }
    }
}