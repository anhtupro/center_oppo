<?php
class Application_Model_RequestOfficeNote extends Zend_Db_Table_Abstract
{
    protected $_name = 'request_office_note';
    
    public function getListNote($request_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.request_id", 
            "p.created_at", 
            "p.created_by", 
            "p.note", 
            "fullname" => "CONCAT(s.firstname, ' ', s.lastname)",
        );

        $select->from(array('p' => 'request_office_note'), $arrCols);
        $select->joinLeft(array('s' => 'staff'), 's.id = p.created_by', array());

        $select->where('p.request_id = ?', $request_id);
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
}