<?php
class Application_Model_CodeSupplier extends Zend_Db_Table_Abstract
{
	protected $_name = 'code_supplier';

	function fetchCodeSupplier($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        $select = $db->select()->from(array('p' => 'pmodel_code'), array(new Zend_Db_Expr
                ('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        $select->joinLeft(array('b' => $this->_name), 'p.id = b.code_id', array('code' =>
                'p.code','price' => 'b.price','default' => 'b.default'));
        $select->joinLeft(array('a' => 'supplier'), 'b.supplier_id = a.id', array('supplier' =>
                'a.title'));
        

        if (isset($params['supplier']) and $params['supplier'])
            $select->where('a.title LIKE ?', '%' . $params['supplier'] . '%');

        if (isset($params['code']) and $params['code'])
            $select->where('p.name LIKE ?', '%' . $params['code'] . '%');
        if (isset($params['code_id']) and $params['code_id'])
            $select->where('p.code LIKE ?', '%' . $params['code_id'] . '%');

        if (isset($params['price']) && $params['price'])
            $select->where('b.price = ?', $params['price']);

        // if (isset($params['default']) && $params['default'])
        //     $select->where('p.default = ?', $params['default']);

        $select->where('b.default is null or b.default = ?',1);
        $select->group('p.id');
        // echo "<pre>";print_r($select->__toString());die;
        if ($limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }
    function fetchRowCodeSupplier($id){
        $db = Zend_Registry::get('db');

        $select = $db->select()->from(array('p' => $this->_name), array(new Zend_Db_Expr
                ('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        $select->join(array('a' => 'supplier'), 'p.supplier_id = a.id', array('supplier' =>
                'a.title'));
        $select->join(array('b' => 'pmodel_code'), 'p.code_id = b.id', array('code' =>
                'b.code'));

        $select->where('p.id = ?', $id);

        $result = $db->fetchAll($select);
        return $result;
    }
    function fetchRowCode($code){
        $db = Zend_Registry::get('db');

        $select = $db->select()->from(array('p' => $this->_name), array(new Zend_Db_Expr
                ('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        $select->join(array('a' => 'supplier'), 'p.supplier_id = a.id', array('supplier' =>
                'a.title'));
        $select->join(array('b' => 'pmodel_code'), 'p.code_id = b.id', array('code' =>
                'b.code'));

        $select->where('b.code = ?', $code);

        $result = $db->fetchAll($select);
        return $result;
    }

}