<?php
class Application_Model_SurveyObject extends Zend_Db_Table_Abstract
{
    protected $_name = 'survey_object';

    public function check($staff_id)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->distinct()
            ->from(array('so' => $this->_name), array('so.survey_id'))
            ->join(array('su' => 'survey'), 'su.id=so.survey_id AND su.status=1', array('su.type'))
            ->join(array('s' => 'staff'), '(so.object_id = s.title OR so.object_id = s.team) AND s.id = ' . intval($staff_id), array())
            ->joinLeft(array('sr' => 'survey_response'), 'su.id=sr.survey_id AND sr.staff_id = s.id', array())
            ->where('sr.id IS NULL')
            ->where('su.id != ?',3)
            ->limitPage(1, 1);

        $result = $db->fetchOne($select);
        if($result){
            return $result;
        }
        // get by time
        $select_2 = $db->select()
            ->distinct()
            ->from(array('so' => $this->_name), array('so.survey_id','response_at'=>'DATE(sr.created_at)'))
            ->join(array('su' => 'survey'), 'su.id=so.survey_id AND su.status=1', array())
            ->join(array('s' => 'staff'), 'so.object_id = s.title AND s.id = ' . intval($staff_id), array())
            ->joinLeft(array('sr' => 'survey_response'), 'su.id = sr.survey_id AND sr.staff_id = s.id AND DATE(sr.created_at) = CURRENT_DATE()', array())
            ->where('sr.id IS NULL')
            ->where('su.status=1')
            //->where('su.id in  (?)',array(12,13,16))
            ->limitPage(1, 1);
        $select->order(new Zend_Db_Expr('RAND()'));
            
        $result = $db->fetchRow($select_2);
        if($result){
            if(!$result['response_at']){
                return $result['survey_id'];
            }

        }

        return null;
    }
        public function checkNew($staff_id,$channel_id )
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->distinct()
            ->from(array('so' => $this->_name), array('so.survey_id'))
            ->joinLeft(array('soc' => 'survey_object_channel'),'so.survey_id=soc.survey_id',array())
            ->join(array('su' => 'survey'), 'su.id=so.survey_id AND su.status=1', array('su.type'))
            ->join(array('s' => 'staff'), '(so.object_id = s.title OR so.object_id = s.team) AND s.id = ' . intval($staff_id), array())
            ->joinLeft(array('sr' => 'survey_response'), 'su.id=sr.survey_id AND sr.staff_id = s.id', array())
            ->where('sr.id IS NULL')
            ->where('su.id != ?',3)
            ->where('soc.channel_id IN (?) ',$channel_id)
            ->limitPage(1, 1);
        $select->order(new Zend_Db_Expr('RAND()'));
        // if($staff_id == 27682){
        //     echo $select->__toString();exit;
        // }
        $result = $db->fetchOne($select);

        if($result){

            return $result;
        }
        $select_2 = $db->select()
            ->distinct()
            ->from(array('so' => $this->_name), array('so.survey_id','response_at'=>'DATE(sr.created_at)'))
            ->joinLeft(array('soc' => 'survey_object_channel'),'so.survey_id=soc.survey_id',array())
            ->join(array('su' => 'survey'), 'su.id=so.survey_id AND su.status=1', array())
            ->join(array('s' => 'staff'), 'soc.object_id = s.title AND s.id = ' . intval($staff_id), array())
            ->joinLeft(array('sr' => 'survey_response'), 'su.id = sr.survey_id AND sr.staff_id = s.id AND DATE(sr.created_at) = CURRENT_DATE()', array())
            ->where('soc.channel_id IN (?) ',$channel_id)
            ->where('sr.id IS NULL')
            ->where('su.status=1')
            //->where('su.id in  (?)',array(12,13,16))
            ->limitPage(1, 1);

        $result = $db->fetchRow($select_2);
        if($result){
            if(!$result['response_at']){

                return $result['survey_id'];
            }

        }

        return null;
    }

    public function getTitle($survey_id)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->distinct()
            ->from(array('so' => $this->_name), array('so.object_id'))
            ->where('so.survey_id = ?', $survey_id);

        $result = $db->fetchCol($select);

        return $result;
    }

}
