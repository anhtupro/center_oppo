<?php

class Application_Model_RecheckShopRound extends Zend_Db_Table_Abstract
{
    protected $_name = 'recheck_shop_round';
    protected $_schema = DATABASE_TRADE;

    public function fetchPagination($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()
                    ->from(['r' => DATABASE_TRADE.'.recheck_shop_round'], [
                        new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT r.id'),
                        'r.name',
                        'r.from_date',
                        'r.to_date',
                        'r.limit_store',
                        'r.recheck_type'
                    ])
                    ->where('r.is_deleted = 0 OR r.is_deleted IS NULL')
        ->order('r.id DESC');
        if (isset($params['from_date'])) {
            $select->where('r.from_date >= ?', $params['from_date']);
        }

        if (isset($params['to_date'])) {
            $select->where('r.to_date <= ?', $params['to_date']);
        }

        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    public function isRunning($roundId)
    {
        $QRecheckShopRound = new Application_Model_RecheckShopRound();
        $recheckRound = $QRecheckShopRound->fetchRow(['id = ?' => $roundId])->toArray();

        $currentDate = date('Y-m-d');
        if ($recheckRound['from_date'] <= $currentDate && $recheckRound['to_date'] >= $currentDate) {
            return true;
        }
        return false;
    }
    
}    