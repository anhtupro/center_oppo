<?php
class Application_Model_AirMapProduct extends Zend_Db_Table_Abstract
{
    protected $_name = 'product_air';
    protected $_schema = DATABASE_TRADE;


    public function getAirDetailsSup($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "s.category_id", 
            "i.width", 
            "i.high", 
            "i.material_id",
            "i.location" 
            

        );

        $select->from(array('p' => DATABASE_TRADE.'.product_air'), $arrCols);
        $select->joinLeft(array('s' => DATABASE_TRADE.'.product'), 's.id = p.product_id', array());
        $select->joinLeft(array('i' => DATABASE_TRADE.'.product_image'), 'i.product_id = s.id', array());
        $select->where('p.air_id = ?', $params['id']);
        $result = $db->fetchAll($select);
        return $result;
    }
}