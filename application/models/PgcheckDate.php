<?php
class Application_Model_PgcheckDate extends Zend_Db_Table_Abstract
{
    protected $_name = 'pgcheck_date';
    
    
    public function getInfoPgs($id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "staff_id" => "p.id", 
            "staff_code" => "p.code", 
            "fullname" => "CONCAT(p.firstname, ' ', p.lastname)", 
            "team_name" =>  "t.name", 
            "title_name" => "t2.name"
        );

        $select->from(array('p' => 'staff'), $arrCols);
        $select->joinLeft(array('t' => 'team'), 't.id = p.team', array());
        $select->joinLeft(array('t2' => 'team'), 't2.id = p.title', array());
        
        $select->where("p.id = ?", $id);

        $result = $db->fetchRow($select);
        
        return $result;
    }
    
    public function getListCategoryGroup(){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.name"
        );

        $select->from(array('p' => 'pgcheck_category_group'), $arrCols);
        $select->where('is_del = 0');
        
        $result = $db->fetchAll($select);
        
        return $result;
    }
    
    public function getListCategoryGroupById($id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.name"
        );

        $select->from(array('p' => 'pgcheck_category_group'), $arrCols);
        $select->where('p.id = ?', $id);
        
        $result = $db->fetchRow($select);
        
        return $result;
    }
    
    public function getListStore($id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.staff_id", 
            "p.store_id", 
            "joined_at" => "FROM_UNIXTIME(p.joined_at)", 
            "store_name" => "s.name" ,
            'v.channel_id'
        );

        $select->from(array('p' => 'store_staff_log'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('v' => 'v_channel_store'), 'v.store_id = s.id', array());
        
        $select->where("p.staff_id = ?", $id);
        $select->where("p.released_at IS NULL");

        $result = $db->fetchAll($select);
        
        return $result;
    }
    
    public function getListCategory(){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.type", 
            "name", 
        );

        $select->from(array('p' => 'pgcheck_category'), $arrCols);
        
        $select->where("p.is_del = ?", 0);
        
        $select->order('p.stt ASC');

        $result = $db->fetchAll($select);
        
        return $result;
    }
    
    public function getListCategoryById($id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "c.id", 
            "c.name", 
            "c.type", 
            "p.status", 
        );

        $select->from(array('p' => 'pgcheck_date_details'), $arrCols);
        $select->joinLeft(array('c' => 'pgcheck_category'), 'c.id = p.pgcheck_category_id', array());
        $select->where("p.pgcheck_date_id = ?", $id);

        $result = $db->fetchAll($select);
        
        return $result;
    }
    
    function fetchPagination($page, $limit, &$total, $params)
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            "pg_name" => "CONCAT(s.firstname, ' ', s.lastname)", 
            "pg_code" => "s.code", 
            "p.created_at", 
            "area_name" => "a.name", 
            "pg_category" => "GROUP_CONCAT(IF((c.type = 1 OR c.type = 2), CONCAT(d.pgcheck_category_id,',',d.status), NULL) ORDER BY c.id SEPARATOR ';') ",
            "btn_category" => "GROUP_CONCAT(IF(c.type = 3, CONCAT(d.pgcheck_category_id,',',d.status), NULL) ORDER BY c.id SEPARATOR ';')",
            "store_name" => "store.name"
        );

        $select->from(array('p' => 'pgcheck_date'), $arrCols);
        $select->joinLeft(array('s' => 'staff'), 's.id = p.pg_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());
        $select->joinLeft(array('a' => 'area'), 'a.id = r.area_id', array());
        $select->joinLeft(array('d' => 'pgcheck_date_details'), 'd.pgcheck_date_id = p.id', array());
        $select->joinLeft(array('store' => 'store'), 'store.id = p.store_id', array());
        $select->joinLeft(array('c' => 'pgcheck_category'), 'c.id = d.pgcheck_category_id', array());
        
        if(!empty($params['pg_name'])){
            $select->where("CONCAT(s.firstname, ' ', s.lastname) LIKE ?", '%'.$params['pg_name'].'%');
        }
        
        if(!empty($params['store_name'])){
            $select->where("store.name LIKE ?", '%'.$params['store_name'].'%');
        }
        
        if(!empty($params['area_id'])){
            $select->where("a.id = ?", $params['area_id']);
        }
        
        if(!empty($params['pg_code'])){
            $select->where("s.code LIKE ?", '%'.$params['pg_code'].'%');
        }
        
        if(!empty($params['created_by'])){
            $select->where("p.created_by = ?", $params['created_by']);
        }
        
        if(!empty($params['area_list'])){
            $select->where("a.id IN (?)", $params['area_list']);
        }
        
        if($_GET['dev'] == 1){
            echo "<pre>";
            echo $select;exit;
        }
        
        $select->group('p.id');
        $select->order('p.id DESC');
        
        if(empty($export)){
            $select->limitPage($page, $limit);
        }
        
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
    
    function fetchPaginationStaff($page, $limit, &$total, $params)
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            "pg_name" => "CONCAT(s.firstname, ' ', s.lastname)", 
            "pg_code" => "s.code", 
            "p.staff_id", 
            "p.store_id", 
            "p.is_leader", 
            "from_date" => "FROM_UNIXTIME(p.joined_at)", 
            "to_date" => "FROM_UNIXTIME(p.released_at)", 
            "area_name" => "a.name", 
            "pgcheck_date_details_id" => "d.id", 
            "d.created_at", 
            "d.note",
            "d.is_off",
            "d.is_btn",
            "d.shift",
            "details.pg_category", 
            "details.flash_sale_category",
            "details.btn_category",
            "details.list_category",
            "store_name" => "store.name",
            "store_code" => "store.store_code",
            "channel_name" => "channel.channel_name",
            "image_pgs" => "details.image_pgs",
            "image_flashsale" => "details.image_flashsale",
            "image_btn" => "details.image_btn"
        );

        $select->from(array('p' => 'store_staff_log'), $arrCols);
        $select->joinLeft(array('s' => 'staff'), 's.id = p.staff_id', array());
        $select->joinLeft(array('store' => 'store'), 'store.id = p.store_id', array());
        $select->joinLeft(array('c' => 'v_channel_store'), 'c.store_id = store.id', array());
        $select->joinLeft(array('channel' => 'v_channel'), 'channel.channel_id = c.channel_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());
        $select->joinLeft(array('a' => 'area'), 'a.id = r.area_id', array());
        $select->joinLeft(array('d' => 'pgcheck_date'), "d.status = 0 AND d.pg_id = p.staff_id AND d.store_id = p.store_id AND DATE(d.created_at) = '".$params['date']."'", array());
        
        $nestedSelect = "SELECT p.pgcheck_date_id, 
                        GROUP_CONCAT(IF((c.type = 1), CONCAT(c.group_id,',',p.status), NULL) ORDER BY c.id SEPARATOR ';') AS pg_category,
                        GROUP_CONCAT(IF(c.type = 2, CONCAT(c.group_id,',',p.status), NULL) ORDER BY c.id SEPARATOR ';') AS flash_sale_category,
                        GROUP_CONCAT(IF(c.type = 3, CONCAT(c.group_id,',',p.status), NULL) ORDER BY c.id SEPARATOR ';') AS btn_category,
                        GROUP_CONCAT(IF( p.status = 1, c.group_id, NULL )  SEPARATOR ',') AS list_category,
                        f.url image_pgs,
                        f2.url image_flashsale,
                        f3.url image_btn
                        FROM pgcheck_date_details p
                        LEFT JOIN pgcheck_date d ON d.id = p.pgcheck_date_id
                        LEFT JOIN pgcheck_category c ON c.id = p.pgcheck_category_id
                        LEFT JOIN (SELECT * from pgcheck_file WHERE type = 1) f ON f.pgcheck_date_id = d.id
                        LEFT JOIN (SELECT * from pgcheck_file WHERE type = 2) f2 ON f2.pgcheck_date_id = d.id 
                        LEFT JOIN (SELECT * from pgcheck_file WHERE type = 3) f3 ON f3.pgcheck_date_id = d.id 
                        WHERE (DATE(d.created_at) = '".$params['date']."' OR d.created_at IS NULL)
                        GROUP BY p.pgcheck_date_id";
        
        $select->joinLeft(array('details' => new Zend_Db_Expr('(' . $nestedSelect . ')')), 'details.pgcheck_date_id = d.id', array());
        
        if(!empty($params['pg_name'])){
            $select->where("CONCAT(s.firstname, ' ', s.lastname) LIKE ?", '%'.$params['pg_name'].'%');
        }
        
        if(!empty($params['store_name'])){
            $select->where("store.name LIKE ?", '%'.$params['store_name'].'%');
        }
        
        if(!empty($params['area_id'])){
            $select->where("a.id = ?", $params['area_id']);
        }
        
        if(!empty($params['pg_code'])){
            $select->where("s.code LIKE ?", '%'.$params['pg_code'].'%');
        }
        
        if(!empty($params['date'])){
            $select->where("(DATE(d.created_at) = ? OR d.created_at IS NULL)", $params['date']);
        }
        
        if(!empty($params['created_by'])){
            $select->where("p.staff_id = ?", $params['created_by']);
        }
        
        if(!empty($params['area_list'])){
            $select->where("a.id IN (?)", $params['area_list']);
        }
        
        //$select->where('d.status = 0', NULL);
        $select->where('p.released_at IS NULL', NULL);
        $select->where('p.is_leader = 0', NULL);
        
        $select->where('c.channel_id IN (?)', $params['channel']);
        
        if(empty($params['export'])){
            $select->order('d.id ASC');
        }
        
        if(empty($params['export'])){
            $select->limitPage($page, $limit);
        }
        
        if($_GET['dev'] == 1){
            echo "<pre>";
            echo $select;exit;
        }
        
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
    
    public function getListChannel($params) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "channel_id",
            "channel_name",
        );

        $select->from(array('p' => 'v_channel'), $arrCols);
        
        if(!empty($params['channel'])){
            $select->where('channel_id IN (?)', $params['channel']);
        }
      
        $select->order(array("field(channel_id, ".implode(",",$params['channel']).") ASC"));
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
    function fetchChannelImage($page, $limit, &$total, $params)
    {
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.channel_id'),
            "p.channel_name", 
            "url" => "GROUP_CONCAT(f.url)", 
        );

        $select->from(array('p' => 'v_channel'), $arrCols);
        $select->joinLeft(array('f' => 'pgcheck_channel_file'), 'f.channel_id = p.channel_id', array());
        
        $select->where('f.is_del = 0 OR f.is_del IS NULL', NULL);
        $select->group('p.channel_id');
        
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
    
    public function getChannelIdFromStaff($id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.staff_id", 
            "p.store_id", 
            "s.channel_id" 
        );

        $select->from(array('p' => 'store_staff_log'), $arrCols);
        $select->joinLeft(array('s' => 'v_channel_store'), 's.store_id = p.store_id', array());
        
        $select->where("p.staff_id = ?", $id);
        $select->where("p.released_at IS NULL");

        $result = $db->fetchAll($select);
        
        $data = [];
        foreach($result as $key=>$value){
            $data[$value['store_id']] = $value['channel_id'];
        }
        
        return $data;
    }
    
    public function getChannelIdFromStaffStore($id, $store_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.staff_id", 
            "p.store_id", 
            "s.channel_id" 
        );

        $select->from(array('p' => 'store_staff_log'), $arrCols);
        $select->joinLeft(array('s' => 'v_channel_store'), 's.store_id = p.store_id', array());
        
        $select->where("p.staff_id = ?", $id);
        $select->where("p.store_id = ?", $store_id);
        $select->where("p.released_at IS NULL");
        
        $result = $db->fetchRow($select);
        
        return $result['channel_id'];
    }
    
    public function getListCategoryChannel($channel_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id',
            'p.name',
            'p.type',
            'p.stt',
            'p.channel_id',
            'p.group_id',
        );

        $select->from(array('p' => 'pgcheck_category'), $arrCols);

        $select->where('p.channel_id = ?', $channel_id);
        $select->where('p.is_del = 0');
        
        $result = $db->fetchAll($select);

        return $result;
        
    }
    
    public function getTotalStoreChannel($params){
        
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "s.id", 
            "s.regional_market", 
            "r.area_id", 
            "c.channel_id", 
            "total_store" => "COUNT(DISTINCT s.id)",
        );

        $select->from(array('p' => 'store_staff_log'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());
        $select->joinLeft(array('c' => 'v_channel_store'), 'c.store_id = s.id', array());
        
        $select->where('r.area_id IN (?)', $params['area_list']);
        
        $select->where('p.released_at IS NULL AND p.is_leader = 0 AND s.del IS NULL OR s.del = 0');
        $select->group(['c.channel_id']);
        
        $result = $db->fetchAll($select);

        return $result;
        
    }
    
    public function getTotalStoreChannelByArea($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "s.id", 
            "s.regional_market", 
            "r.area_id", 
            "c.channel_id", 
            "total_store" => "COUNT(DISTINCT s.id)",
        );

        $select->from(array('p' => 'store_staff_log'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());
        $select->joinLeft(array('c' => 'v_channel_store'), 'c.store_id = s.id', array());
        
        $select->where('r.area_id IN (?)', $params['area_list']);
        
        $select->where('p.released_at IS NULL AND p.is_leader = 0 AND s.del IS NULL OR s.del = 0');
        $select->group(['c.channel_id','r.area_id']);
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getTotalStoreChannelByRegion($params){
        
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "s.id", 
            "s.regional_market", 
            "r.area_id", 
            "c.channel_id", 
            "total_store" => "COUNT(DISTINCT s.id)",
            'a.region_id'
        );

        $select->from(array('p' => 'store_staff_log'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());
        $select->joinLeft(array('a' => 'area'), 'a.id = r.area_id', array());
        $select->joinLeft(array('c' => 'v_channel_store'), 'c.store_id = s.id', array());
        
        $select->where('r.area_id IN (?)', $params['area_list']);
        
        $select->where('p.released_at IS NULL AND p.is_leader = 0 AND s.del IS NULL OR s.del = 0');
        $select->group(['c.channel_id','a.region_id']);
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getTotalCategoryCheck($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.pg_id", 
            "p.store_id", 
            "p.channel_id", 
            "p.created_at", 
            "d.pgcheck_category_id", 
            "d.status", 
            "c.group_id", 
            "total" => "COUNT(DISTINCT p.store_id)",
        );

        $select->from(array('p' => 'pgcheck_date'), $arrCols);
        $select->joinLeft(array('d' => 'pgcheck_date_details'), 'd.pgcheck_date_id = p.id', array());
        $select->joinLeft(array('c' => 'pgcheck_category'), 'c.id = d.pgcheck_category_id', array());
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());
        
        $select->where("p.channel_id IS NOT NULL AND p.channel_id <> 0 AND d.`status` = 1");
        $select->where("p.channel_id IN (?)", $params['channel']);
        $select->where("DATE(p.created_at) = ?", $params['date']);
        
        $select->where("r.area_id IN (?)", $params['area_list']);
        
        $select->group(["c.group_id", "p.channel_id"]);
        $result = $db->fetchAll($select);
        
        
        return $result;
    }
    
    public function getTotalCategoryCheckByArea($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.pg_id", 
            "p.store_id", 
            "p.channel_id", 
            "p.created_at", 
            "d.pgcheck_category_id", 
            "d.status", 
            "c.group_id", 
            "total" => "COUNT(DISTINCT p.store_id)",
            "r.area_id"
        );

        $select->from(array('p' => 'pgcheck_date'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());
        $select->joinLeft(array('d' => 'pgcheck_date_details'), 'd.pgcheck_date_id = p.id', array());
        $select->joinLeft(array('c' => 'pgcheck_category'), 'c.id = d.pgcheck_category_id', array());
        
        $select->where("p.channel_id IS NOT NULL AND p.channel_id <> 0 AND d.`status` = 1");
        $select->where("p.channel_id IN (?)", $params['channel']);
        $select->where("DATE(p.created_at) = ?", $params['date']);
        $select->where("c.group_id = ?", $params['group_category_id']);
        
        $select->where("r.area_id IN (?)", $params['area_list']);
        
        $select->group(["r.area_id", "p.channel_id"]);
        
        $result = $db->fetchAll($select);
        
        
        return $result;
    }
    
    public function getTotalCategoryCheckByRegion($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.pg_id", 
            "p.store_id", 
            "p.channel_id", 
            "p.created_at", 
            "d.pgcheck_category_id", 
            "d.status", 
            "c.group_id", 
            "total" => "COUNT(DISTINCT p.store_id)",
            "r.area_id",
            "a.region_id",
        );

        $select->from(array('p' => 'pgcheck_date'), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());
        $select->joinLeft(array('a' => 'area'), 'a.id = r.area_id', array());
        $select->joinLeft(array('d' => 'pgcheck_date_details'), 'd.pgcheck_date_id = p.id', array());
        $select->joinLeft(array('c' => 'pgcheck_category'), 'c.id = d.pgcheck_category_id', array());
        
        $select->where("p.channel_id IS NOT NULL AND p.channel_id <> 0 AND d.`status` = 1");
        $select->where("p.channel_id IN (?)", $params['channel']);
        $select->where("DATE(p.created_at) = ?", $params['date']);
        $select->where("c.group_id = ?", $params['group_category_id']);
        
        $select->where("r.area_id IN (?)", $params['area_list']);
        
        $select->group(["a.region_id", "p.channel_id"]);
        
        $result = $db->fetchAll($select);
        
        
        return $result;
    }
    
    public function getGroupCategory($group_category_id){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.name",
        );

        $select->from(array('p' => 'pgcheck_category_group'), $arrCols);

        $select->where('p.id = ?', $group_category_id);
        $result = $db->fetchRow($select);

        return $result;
    }
    
    public function getAreaList($params) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'p.id',
            'p.name',
        );

        $select->from(array('p' => 'area'), $arrCols);

        $select->where('p.id IN (?)', $params['area_list']);
        
        if(!empty($params["region_id"])){
            $select->where("p.region_id = ?", $params["region_id"]);
        }
        
        $result = $db->fetchAll($select);
        
        $data = [];
        foreach($result as $key=>$value){
            $data[$value['id']] = $value['name'];
        }

        return $data;
    }
    
    
}