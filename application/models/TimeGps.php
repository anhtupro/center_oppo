<?php
class Application_Model_TimeGps extends Zend_Db_Table_Abstract
{
    protected $_name = 'time_gps';
    function getStaffForTestByTeam($team){
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['p' => 'time_gps'], [
                'p.latitude',
                'p.longitude',

            ])
           	->join(array('staff'=>'staff'),'staff.id=p.staff_id','')
            ->where('staff.team = ?', 611)
            ->where('p.check_in_day > ?', '2020-02-01');


        $result = $db->fetchAll($select);
        return $result;
    }



}
