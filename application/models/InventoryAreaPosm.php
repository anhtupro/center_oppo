<?php

class Application_Model_InventoryAreaPosm extends Zend_Db_Table_Abstract
{
    protected $_name = 'inventory_area_posm';
    protected $_schema = DATABASE_TRADE;

    public function getAll($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['i' => DATABASE_TRADE.'.inventory_area_posm'], [
                         'i.quantity',
                         'i.category_id',
                         'i.area_id',
                         'category_name' => 'c.name',
                         'parent_id' => 'p.id',
                         'parent_name' => 'p.name'
                     ])
        ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'i.category_id = c.id', [])
        ->joinLeft(['d' => DATABASE_TRADE.'.inventory_category_posm_child'], 'c.id = d.category_id', [])
        ->joinLeft(['p' => DATABASE_TRADE.'.inventory_category_posm_parent'], 'd.parent_id = p.id', []);

        if($params['area_id']) {
            $select->where('i.area_id = ?', $params['area_id']);
        }

        $result = $db->fetchAll($select);

        return $result;
    }

    public function getParentQuantity($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['i' => DATABASE_TRADE.'.inventory_area_posm'], [
                         'parent_id' => 'p.id',
                         'parent_name' => 'p.name',
                         'quantity' => "SUM(i.quantity)"
                     ])
            ->joinLeft(['d' => DATABASE_TRADE.'.inventory_category_posm_child'], 'i.category_id = d.category_id', [])
            ->joinLeft(['p' => DATABASE_TRADE.'.inventory_category_posm_parent'], 'd.parent_id = p.id', [])
            ->group('d.parent_id');

        if($params['area_id']) {
            $select->where('i.area_id = ?', $params['area_id']);
        }

        $result = $db->fetchAll($select);

        return $result;

    }

    public function getChildQuantity($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['i' => DATABASE_TRADE.'.inventory_area_posm'], [
                'parent_id' => 'p.id',
                'quantity' => "i.quantity",
                'category_id' => 'c.id',
                'category_name' => 'c.name'
            ])
            ->joinLeft(['c' => DATABASE_TRADE.'.category'], 'i.category_id = c.id', [])
            ->joinLeft(['d' => DATABASE_TRADE.'.inventory_category_posm_child'], 'i.category_id = d.category_id', [])
            ->joinLeft(['p' => DATABASE_TRADE.'.inventory_category_posm_parent'], 'd.parent_id = p.id', []);


        if($params['area_id']) {
            $select->where('i.area_id = ?', $params['area_id']);
        }

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $listQuanityChild [$element['parent_id']] [] = $element;
        }

        return $listQuanityChild;
    }
}    