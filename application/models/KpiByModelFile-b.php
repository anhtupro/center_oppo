<?php
class Application_Model_KpiByModelFile extends Zend_Db_Table_Abstract
{
	protected $_name = 'kpi_by_model_file';
	
	//update `store_id`,`area_id`,`province_id`,`district_id`
	public function updateBasic($id) {
        $db = Zend_Registry::get('db');
        $sql = "
			UPDATE kpi_by_model_file a
                        INNER JOIN (
				SELECT k.id FROM kpi_by_model_file k WHERE k.`random_do_so` = '$id'
			) b ON b.id = a.id
			INNER JOIN " . WAREHOUSE_DB . ".distributor d ON d.partner_id = a.partner_id COLLATE utf8_unicode_ci
			INNER JOIN store s ON d.id = s.d_id AND (s.del <> 1 OR s.del IS NULL OR s.del = '')
			INNER JOIN regional_market rm ON rm.id = s.regional_market
			INNER JOIN `area` re ON re.id = rm.area_id
			SET a.`store_id` = s.id, a.`area_id` = re.id, a.`province_id` = rm.id, a.`district_id` = s.district, a.distributor_id = d.`id`
		";
        $db->query($sql);
        $db = null;
    }
	
	//update `good_id`
	public function updateGoodId($id) {
        $db = Zend_Registry::get('db');
        $sql = "
			UPDATE kpi_by_model_file a
                        INNER JOIN (
				SELECT k.id FROM kpi_by_model_file k WHERE k.`random_do_so` = '$id'
			) b ON b.id = a.id
			INNER JOIN " . WAREHOUSE_DB . ".good g ON g.`name` = TRIM(a.model) COLLATE utf8_unicode_ci
			SET a.good_id = g.id
		";
        $db->query($sql);
        $db = null;
    }
	
	//update `good_id`
	public function updateColorId($id) {
        $db = Zend_Registry::get('db');
        $sql = "
			UPDATE kpi_by_model_file a
                        INNER JOIN (
				SELECT k.id FROM kpi_by_model_file k WHERE k.`random_do_so` = '$id'
			) b ON b.id = a.id
			LEFT JOIN warehouse.`good_color` gc ON gc.`name` = a.color_name COLLATE utf8_unicode_ci
			SET a.color_id = gc.id
		";
        $db->query($sql);
        $db = null;
    }
	
	//insert into kpi_by_model
	public function insertData($id){
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare("
			INSERT INTO `kpi_by_model`( `timing_date`, `distributor_id`, `store_id`, `qty`, `area_id`, `province_id`, `district_id`, `good_id`, `color_id`, `pg_allow`, 
                        `pg_string`, `pg_total`, `pg_export`, `partner_id`, `status`, `status_locked`)
			SELECT  i.`timing_date`, i.`distributor_id`, i.`store_id`, i.`qty`, i.`area_id`, i.`province_id`, i.`district_id`, i.`good_id`, i.`color_id`
				, IFNULL(CONCAT(',', GROUP_CONCAT(stl.`staff_id`), ','), 0) pg_allow
				, IFNULL(GROUP_CONCAT(CONCAT('Name: ', CONCAT(st.firstname, ' ', st.lastname), CONCAT('<br>Email: ', SUBSTRING_INDEX(IFNULL(st.email, 'noemail@'), '@', 1), CONCAT('<br>Code: ', st.`code`))), '<hr>'), 'No PG') pg_string	
				, COUNT(stl.staff_id) pg_total
				, IFNULL(GROUP_CONCAT(CONCAT(st.firstname, ' ', st.lastname, ' ', st.`code`)), 'No PG') pg_export
				, i.`partner_id`, i.`status`, i.`status_locked`
			FROM kpi_by_model_file i
			LEFT JOIN store s ON s.`id` = i.`store_id`
			LEFT JOIN store_staff_log stl ON stl.`store_id` = s.`id` AND DATE(i.`timing_date`) >= FROM_UNIXTIME(stl.joined_at,'%Y-%m-%d') AND (stl.released_at IS NULL OR DATE(i.`timing_date`) < FROM_UNIXTIME(stl.released_at,'%Y-%m-%d'))
AND stl.`is_leader` = 0
			LEFT JOIN staff st ON st.id = stl.staff_id
			WHERE i.random_do_so = '$id'
                        GROUP BY i.`id`
        ");
   
        $res = $stmt->execute();
        $stmt->closeCursor();
        $stmt = null;
        return $res;
    }
	
	//check color
	public function checkColor($id){
        $db = Zend_Registry::get('db');
		$sql = "SELECT g.`desc`
				FROM kpi_by_model_file kb
				LEFT JOIN " . WAREHOUSE_DB . ".`good_color` gc ON gc.`name` = kb.color_name COLLATE utf8_unicode_ci
				LEFT JOIN " . WAREHOUSE_DB . ".`good` g ON g.`id` = kb.`good_id`
				WHERE gc.`name` IS NULL
				GROUP BY kb.`good_id`
			";
        $stmt = $db->prepare($sql);                       
        $stmt->execute();
        $checkColor = $stmt->fetchAll();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $checkColor;
    }
	
	//delete table kpi_by_model_file
	public function deleteModelFile($id) {
        $db = Zend_Registry::get('db');
        $sql = "
			DELETE FROM kpi_by_model_file WHERE random_do_so = '$id'
		";
        $db->query($sql);
        $db = null;
    }
}