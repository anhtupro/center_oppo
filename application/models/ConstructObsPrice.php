<?php

class Application_Model_ConstructObsPrice extends Zend_Db_Table_Abstract
{
    protected $_name = 'construct_obs_price';
    protected $_schema = DATABASE_TRADE;

    public function getPrice($params)
    {

        $db = Zend_Registry::get("db");
        $select = $db->select();

        $select->from(array('o' => DATABASE_TRADE.'.construct_obs'), ['o.id', 'p.*'])
            ->joinLeft(array('p' => DATABASE_TRADE.'.construct_obs_price'), 'o.id = p.construct_obs_id', [])
            ->where('o.id = ?', $params['construct_obs_id']);

        if (isset($params['price_type']) && $params['price_type'] == 0) {
            $select->where('p.status = ?', $params['price_type']);
        }
        if (isset($params['price_type']) && $params['price_type'] != 0) {
            $select->where('p.status = ?', $params['price_type']);
        }


        $result = $db->fetchAll($select);

        $total_price = 0;

        foreach ($result as $element) {
            $list_price [] = $element;
            $total_price += $element['total_price'] + $element['total_price'] * VAT;
        }

        $result = [
            'list_price' => $list_price,
            'total_price' =>  $total_price
        ];

        return $result;
    }
}