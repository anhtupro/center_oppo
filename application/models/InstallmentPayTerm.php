<?php

class Application_Model_InstallmentPayTerm extends Zend_Db_Table_Abstract
{
    protected $_name = 'installment_pay_term';

    public function getAll()
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['p' => 'installment_pay_term'], [
                'p.*'
            ])
            ->where('p.del = ?', 0);
        $result = $db->fetchAll($select);

        return $result;
    }

    public function getTermByModel($channel_id, $good_price_log_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['p' => DATABASE_CENTER .'.installment_package'], [
                'pt.*',
            ])
            ->joinLeft(['pt' => DATABASE_CENTER . '.installment_pay_term'], 'p.pay_term_id = pt.id', [])
            ->where('p.channel_id = ?', $channel_id)
            ->where('p.good_price_log_id = ?', $good_price_log_id)
            ->where('p.from_date <= CURDATE()')
            ->where('p.to_date IS NULL OR p.to_date >= CURDATE()')
            ->where('pt.del = ?', 0)
            ->group('pt.id')
            ->order('pt.term ASC');

        $result = $db->fetchAll($select);

        return $result;

    }
}