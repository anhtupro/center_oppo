<?php

class Application_Model_PurchasingRatingDetailNew extends Zend_Db_Table_Abstract
{
    protected $_name = 'purchasing_rating_detail_new';
    public function getData($params){
        $QTeam                  = new Application_Model_Team();
        $department             = $QTeam->get_list_department();
        
        $db = Zend_Registry::get('db');
        $select = $db->select()
                        ->from(array('p'=>$this->_name),array('*'));
        $select->joinLeft(['prf'=>'purchasing_rating_file_new'],'prf.purchasing_type_id=p.purchasing_type_id AND p.stage=prf.stage AND p.supplier_id=prf.supplier_id',['file_name' => new Zend_Db_Expr('GROUP_CONCAT(prf.name)'),'file_url'=>new Zend_Db_Expr('GROUP_CONCAT(prf.url)')]);
        $select->joinLeft(['s'=>'supplier'],'s.id=p.supplier_id',['supplier_name'=>'s.title']);
        // Filter
        if(!empty($params['stage'])){
            $select->where('p.stage = ?',$params['stage']);
        }

        if(!empty($params['purchasing_type_id'])){
            $select->where('p.purchasing_type_id = ?',$params['purchasing_type_id']);
        }

        // 
        $select->where('p.del = ?',0);

        $select->group('p.id');


        $result = $db->fetchAll($select);


        // echo "<pre>";
        // echo $select->__toString();


        $arrTemp=[];
        foreach($result as $key => $value){
            $arrTemp[$value['supplier_id']]['inputData']=[];
        }

        foreach($arrTemp as $key => $value){
            $i=0;
            foreach($result as $k => $v){
                if(intval($key)== intval($v['supplier_id'])){
                    $arrTemp[$key]['inputData'][$i]['departmentId']=$v['department_id'];
                    $arrTemp[$key]['inputData'][$i]['departmentName']=$department[$v['department_id']];
                    $arrTemp[$key]['inputData'][$i]['note']=$v['note'];
                    $arrTemp[$key]['inputData'][$i]['ratingDetail']=!empty($v['json_criteria']) ? json_decode($v['json_criteria']):[];
                    $arrTemp[$key]['inputData'][$i]['point']=$v['point'];
                    $arrTemp[$key]['inputData']['supplierId']=$v['supplier_id'];
                    $arrTemp[$key]['inputData']['supplierName']=$v['supplier_name'];
                    $arrTemp[$key]['inputData']['file']['name']=explode(",",$v['file_name']);
                    $arrTemp[$key]['inputData']['file']['url']=explode(",",$v['file_url']);
                    $arrTemp[$key]['inputData'][$i]['id']=$v['id'];
                    $arrTemp[$key]['id']=$v['id'];
                    $i++;
                }

            }
        }

        return $arrTemp;
    }
    function fetchPagination($page, $limit, &$total, $params){
        
        $query = 'SET GLOBAL group_concat_max_len=15000;';
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
        $config = $config->toArray();
        $con=mysqli_connect($config['resources']['db']['params']['host'],$config['resources']['db']['params']['username'],My_HiddenDB::decryptDb($config['resources']['db']['params']['password']),$config['resources']['db']['params']['dbname']);
        mysqli_query($con,$query);
        
        $db = Zend_Registry::get('db');

        $selectChild = $db->select()
            ->from(array('child'=>$this->_name),array(
                'child.id'
                ,'child.purchasing_type_id'
                ,'child.stage'
                ,'child.date'
                ,'AVG(child.point) AS point'
                ,'child.department_id'
                ,'child.created_at'
                ,'GROUP_CONCAT(child.created_by) as list_created_by'
                ,'child.supplier_id'
                ,'child.active_rating',
            ));
        $selectChild->joinLeft(array('team'),'team.id = child.department_id',['name_department_rating'=> 'GROUP_CONCAT(`team`.`name`)']);

        $selectChild->group(['child.purchasing_type_id','child.stage','child.supplier_id','child.department_id']);
        $selectChild->where('child.del = ?', 0 );
        
        if(!empty($params['team_id']) AND $params['team_id'] != TEAM_PURCHASING ){ // Purchasing
            $selectChild->where('child.active_rating = ?', 1 );
        }
        
        if(isset($params['times']) AND $params['times']){
            $selectChild->where('child.stage = ?', $params['times']);
        }

        $collumns=new Zend_Db_Expr('GROUP_CONCAT(s.title) AS supplier_name');
        $select = $db->select()
            ->from(array('p'=>new Zend_Db_Expr('('.$selectChild.')')),array(
                'p.id'
                ,'p.stage'
                ,'p.date'
                ,'AVG(p.point) AS point'
                ,'p.created_at'
                ,'GROUP_CONCAT(p.list_created_by) as list_created_by'
                ,'p.active_rating'
                ,'GROUP_CONCAT(p.name_department_rating) as name_department_rating'
                ,$collumns
            ));

        $select->joinLeft(array('s'=>'supplier'),'s.id = p.supplier_id',[]);
        $select->joinLeft(array('pt'=>'purchasing_type'),'pt.id = p.purchasing_type_id',['name_purchasing_type'=>'pt.name','purchasing_type_id'=>'pt.id']);


        // Filter-------
        if(isset($params['supplier_name']) AND $params['supplier_name']){
            $select->where('s.title LIKE ?','%'.$params['supplier_name'].'%');
        }
        
        

        if(isset($params['department_name']) AND $params['department_name']){
            $select->where('team.name LIKE ?', '%'.$params['department_name'].'%' );
        }
        if(isset($params['purchasing_type_id']) AND $params['purchasing_type_id']){
            $select->where('p.purchasing_type_id = ?',$params['purchasing_type_id']);
        }
        if(isset($params['date']) AND $params['date']){
            $select->where('p.date = ?',$params['date']);
        }


        $select->group(['p.purchasing_type_id','p.stage','p.supplier_id']);
        

        $selectParent=$db->select();
        $selectParent->from(['temp' => new Zend_Db_Expr('('.$select.')')],
        [
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT temp.id'),
        'temp.*',
        'GROUP_CONCAT(temp.point) as avg_point' ,
        'GROUP_CONCAT(temp.supplier_name) as list_supplier' ,
        'GROUP_CONCAT(temp.name_department_rating) as name_department_rating' ,
        'GROUP_CONCAT(temp.list_created_by) as list_created_by' ,
        'temp.supplier_name',
        'temp.active_rating',
        ]);
        $selectParent->order('temp.created_at DESC');
        $selectParent->group(['temp.purchasing_type_id','temp.stage']);
        

        if ($limit)
            $selectParent->limitPage($page, $limit);

        $result = $db->fetchAll($selectParent);


       
        foreach($result as $key => $value){
                $result[$key]['supplier_name']=array_unique(explode(',',$value['list_supplier']));
                $result[$key]['avg_point']=explode(',',$value['avg_point']);
                $result[$key]['list_department_id']=array_unique(explode(',',$value['list_department_id']));
                $result[$key]['name_department_rating']=array_unique(explode(',',$value['name_department_rating']));
                $result[$key]['list_created_by']=array_unique(explode(',',$value['list_created_by']));
        }


        if($_GET['dev']){
        echo "<pre>";
        echo $selectParent->__toString();
        print_r($result);
        }


        if ($limit)
            $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }
    function export($params){
        $db = Zend_Registry::get('db');
        $select=$db->select();
        $select->from(['p'=>$this->_name],['p.id','p.point','p.stage','p.date']);
        $select->joinLeft(['pt'=>'purchasing_type'],'pt.id=p.purchasing_type_id',['purchasing_type_name'=>'pt.name']);
        $select->joinLeft(['s'=>'supplier'],'s.id=p.supplier_id',['supplier_name'=>'s.name']);
        $select->joinLeft(['t'=>'team'],'t.id=p.department_id',['department_name'=>'t.name']);
        $select->joinLeft(['staff'=>'staff'],'staff.id=p.created_by',['created_name'=>"CONCAT(staff.firstname, ' ', staff.lastname)"]);
        
        if(isset($params['times']) AND $params['times']){
            $select->where('p.stage = ?', $params['times']);
        }
        
        $select->where('p.del = ?', 0 );
        $select->order(['p.purchasing_type_id','p.supplier_id','p.stage','p.point']);
        $res=$db->fetchAll($select);
        return $res;

    }
}



