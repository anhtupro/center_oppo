<?php
class Application_Model_Team extends Zend_Db_Table_Abstract
{
	protected $_name = 'team';

    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        if (isset($params['name']) and $params['name'])
            $select->where('p.name LIKE ?', '%'.$params['name'].'%');

        $select->order('p.name', 'COLLATE utf8_unicode_ci ASC');

        if ($limit)
        	$select->limitPage($page, $limit);

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if ($result === false) {

            $data = $this->fetchAll(null, 'name');

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->name;
                }
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }

    function get_all_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_all_cache');

        if ($result === false) {

            $data = $this->fetchAll(null, 'name');

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = array('name' => $item->name, 'email' => $item->email);
                }
            }
            $cache->save($result, $this->_name.'_all_cache', array(), null);
        }
        return $result;
    }

    function get_recursive_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_recursive_cache');

        if ($result === false) {
            $where = $this->getAdapter()->quoteInto('del = ? ', 0);
            $data = $this->fetchAll($where, 'name');

            $result = array();
            if ($data->count()){
                $data = $data->toArray();
                $result = $this->formatTree($data, 0);
            }
            $cache->save($result, $this->_name.'_recursive_cache', array(), null);
        }
        return $result;
    }
    
    function get_cache_team(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache_team');

        if ($result === false) {
           
            $where = array();
            $where[] = $this->getAdapter()->quoteInto('parent_id = 0 ', null);
            $department = $this->fetchAll($where , 'id');
            
            $department_array = array();
            
            foreach($department as $k => $v)
            {
                $department_array[] = $v['id'];
            } 
            
            $where = array();
            $where[] = $this->getAdapter()->quoteInto('parent_id in (?) ', $department_array );
            
            $data = $this->fetchAll($where);

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->name;
                }
            }
            $cache->save($result, $this->_name.'_cache_team', array(), null);
        }
        return $result;
        
        
    }

    function formatTree($tree, $parent){
        $tree2 = array();
        foreach($tree as $i => $item){
            if($item['parent_id'] == $parent){
                $tree2[$item['id']] = $item;
                $tree2[$item['id']]['children'] = $this->formatTree($tree, $item['id']);
            }
        }

        return $tree2;
    }

    public function get_all($name = ''){
        $arrCols = array('p.id','p.name');
        if($name == 'vn'){
            $arrCols = array('p.id','name'=>'p.name_vn');
        }
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('p'=>$this->_name),$arrCols)
            ->order('p.name ASC')
        ;
        $result = $db->fetchPairs($select);
        return $result;
    }

    public function get_department(){
        $arrCols = array('p.id','p.name');
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('p'=>$this->_name),$arrCols)
            ->where('p.parent_id = 0')
            ->order('p.name ASC')
        ;
        $result = $db->fetchPairs($select);
        return $result;
    }

    public function get_team($id = NULL,$name=''){
        $arrCols = array('p.id','p.name');
        if($name == 'vn'){
            $arrCols = array('p.id','name'=>'p.name_vn');
        }
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('p'=>$this->_name),$arrCols)
            ->order('p.name ASC')
        ;
        if($id){
            $select->where('p.parent_id = ?',$id);
        }
        $result = $db->fetchPairs($select);
        return $result;
    }

    public function get_cache_title(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache_title');

        if ($result === false) {

            $teams = $this->get_cache_team();
            $arrTeamId = array_keys($teams);
            $where = array();
            $where[] = $this->getAdapter()->quoteInto('parent_id in (?) ', $arrTeamId );
            $data = $this->fetchAll($where);
            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->name;
                }
            }
            $cache->save($result, $this->_name.'_cache_title', array(), null);
        }
        return $result;
    }

    public function get_cache_salesman()
    {
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache_salesman_title');

        if ($result === false) {
            $where = array();
            $where[] = $this->getAdapter()->quoteInto('id in (?) ', My_Staff_Title::getSalesman());
            $data = $this->fetchAll($where);
            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->name;
                }
            }
            $cache->save($result, $this->_name.'_cache_salesman_title', array(), null);
        }
        return $result;
    }

    public function get_cache_salesmantuan()
    {
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache_salesmantuan_title');

        if ($result === false) {
            $where = array();
            $where[] = $this->getAdapter()->quoteInto('id in (?) ', My_Staff_Title::getSalesmanTuan());
            $data = $this->fetchAll($where);
            $result = array();
            if ($data){
                foreach ($data as $item){
                    $name = $item->name;
                    if ($item->id == PG_BRANDSHOP || $item->id == SENIOR_PROMOTER_BRANDSHOP) {
                        $name = $name.' BRANDSHOP';
                    }
                    $result[$item->id] = $name;
                }
            }
            $cache->save($result, $this->_name.'_cache_salesmantuan_title', array(), null);
        }
        return $result;
    }

    public function getTitleTeamDepartment($title){
        $db = Zend_Registry::get('db');
        $cols = array(
            'title'           => 'a.id',
            'team'            => 'b.id',
            'department'      => 'c.id',
            'title_name'      => 'a.name',
            'title_name_vn'   => 'a.name_vn',
            'team_name'       => 'b.name',
            'department_name' => 'c.name',
        );
        $select = $db->select()
            ->from(array('a'=>'team'),$cols)
            ->join(array('b'=>'team'),'a.parent_id = b.id',array())
            ->join(array('c'=>'team'),'b.parent_id = c.id',array())
            ->order(array('c.name','b.name','a.name'))
        ;

        if($title){
            $select->where('a.id = ?',$title);
        }

        $result = $db->fetchRow($select);
        return $result;
    }

    /**
     * @param $department
     * @return mixed
     * @throws Zend_Exception
     * get team or title
     */
    public function getTeamByDepartment($department){
        $db = Zend_Registry::get('db');
        $cols = array(
            'id',
            'name'
        );
        $select = $db->select()
            ->from(array('a'=>$this->_name),$cols)
            ->where('a.parent_id = ?',$department);
        $result = $db->fetchPairs($select);

        return $result;
    }


    public function getSpecialist($list){
        $db = Zend_Registry::get('db');
        $sql = '
            SELECT id, name FROM '.$this->_name.'
            WHERE FIND_IN_SET(id, "'.$list.'")
        ';
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $data;
    }

    public function getTitleTeamDepartmentList($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');
        $cols = array(
            'title'           => 'a.id',
            'team'            => 'b.id',
            'department'      => 'c.id',
            'title_name'      => 'a.name',
            'title_name_vn'   => 'a.name_vn',
            'team_name'       => 'b.name',
            'department_name' => 'c.name',
            'contract_term' => 'ct.name',
            'policy_group' => 'cp.name',
            'company_group' => 'cp2.name',
            'access_group' => 'g.name',
            'is_yearly_leave' => 'a.is_yearly_leave'
            
        );

        $select = $db->select()
            ->from(array('a'=>'team'),$cols)
            ->join(array('b'=>'team'),'a.parent_id = b.id',array())
            ->join(array('c'=>'team'),'b.parent_id = c.id',array())
            ->joinleft(array('ct'=>'contract_types'),'a.contract_term = ct.id',array())
                  ->joinleft(array('cp'=>'company_group'),'a.policy_group = cp.id',array())
                 ->joinleft(array('cp2'=>'company_group'),'a.company_group = cp2.id',array())
                 ->joinleft(array('g'=>'group'),'a.access_group = g.id',array())
                ->where('a.del=0')
            ->order(array('c.name','b.name','a.name'))
        ;

        if($params['title']){
            $select->where('a.id = ?',$params['title']);
        }

        if($params['team']){
            $select->where('b.id = ?',$params['team']);
        }

        if($params['department']){
            $select->where('c.id = ?',$params['department']);
        }

        $result = $db->fetchAll($select);
        return $result;
    }

    public function countStaffByTeam($params){
        if(empty($params['team'])){
            return false;
        }

        $db = Zend_Registry::get('db');
        $cols = array(
            'id'            => 'p.id',
            'name'          => 'p.name',
            'num'           => 'COUNT(DISTINCT s.id)',
        );

        $select = $db->select()
            ->from(array('p'=>'team'),$cols)
            ->join(array('s'=>'staff'), 's.title = p.id', array())
            ->joinLeft(array('r'=>'regional_market'), 's.regional_market = r.id',array());
        ;

        if(!empty($params['area_id'])){
            $select->where('r.area_id = ?', $params['area_id']);
        }

        $select->where('s.off_date IS NULL', NULL);
        $select->where('p.parent_id = ?', $params['team']);

        $select->group('p.id');

        $result = $db->fetchAll($select);
        return $result;
    }
    
    
    function get_list_department(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache_list_department');

        if (!$result) {

            $where = array();
            $where[] = $this->getAdapter()->quoteInto('parent_id = ?', 0);
            $where[] = $this->getAdapter()->quoteInto('del = ?', 0);
            $data = $this->fetchAll($where, 'name');

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->name;
                }
            }
            $cache->save($result, $this->_name.'_cache_list_department', array(), null);
        }
        return $result;
    }
    
}