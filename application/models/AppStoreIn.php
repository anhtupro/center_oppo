<?php
class Application_Model_AppStoreIn extends Zend_Db_Table_Abstract
{
    protected $_name = 'app_store_in';
    protected $_schema = DATABASE_TRADE;
    
    public function checkImeiStore($imei){

        $db     = Zend_Registry::get("db");
        $select = $db->select();
            
        $arrCols = array(
            'p.area_id', 
            'p.campaign_id', 
            'p.category_id', 
            'p.imei_sn', 
            'p.created_at', 
            'p.created_by', 
            'imei_in_store' => 'i.imei_sn'
        );
        
        $select->from(array('p'=> DATABASE_TRADE.'.app_area_in'), $arrCols);
        $select->joinLeft(array('i' => DATABASE_TRADE.'.app_store_in'), 'i.imei_sn = p.imei_sn', array());
        $select->where('p.imei_sn = ?', $imei);
        
        $result  = $db->fetchRow($select);
        
        return $result;
    }
}