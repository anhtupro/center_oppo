<?php

class Application_Model_LeaveInfo extends Zend_Db_Table_Abstract{

    public function __construct()
    {
        $this->table = "leave_info";
    }

    private function dbconnect(){
		return Zend_Registry::get('db');
	}

    public function selectByStaffID($staff_id = '')
    {
        $db = $this->dbconnect();
        $sql = "SELECT stock FROM leave_info WHERE staff_id = :staff_id LIMIT 1";

        $stmt = $db->prepare($sql);
        $stmt->bindParam("staff_id", $staff_id, PDO::PARAM_INT);

        $stmt->execute();
        $info = $stmt->fetch();
        $stmt->closeCursor();
	    $db = $stmt = null;
        return $info;
    }

    public function getStockById($staff_id = '')
    {
        $db = $this->dbconnect();
        $sql = "SELECT PR_get_stock(:staff_id) as `stock`";
        $stmt = $db->prepare($sql);

        $stmt->bindParam("staff_id", $staff_id, PDO::PARAM_INT);

        $stmt->execute();
        $data = $stmt->fetch();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $data['stock'];
    }
    
    public function getYearStockById($staff_id = '')
    {
        $db = $this->dbconnect();
        $sql = "SELECT PR_get_year_stock(:staff_id) as `stock`";
		// $sql = "SELECT fn_get_stock_new(:staff_id) as `stock`";
        $stmt = $db->prepare($sql);

        $stmt->bindParam("staff_id", $staff_id, PDO::PARAM_INT);

        $stmt->execute();
        $data = $stmt->fetch();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $data['stock'];
    }

    public function getHistoryLeave($staff_id = '')
    {
        $db = Zend_Registry::get('db');
        $sql = "CALL PR_year_leave_history(:staff_id)";

        $stmt = $db->prepare($sql);
        $stmt->bindParam('staff_id', $staff_id, PDO::PARAM_INT);
        $stmt->execute();

        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        $stmt = $db = null;

        $array_data = array();
        foreach($data as $key => $value)
        {
            $array_data[intval($value['month']) . '-' . intval($value['year'])] = $value['number'];
        }

        return $array_data;
    }

}
