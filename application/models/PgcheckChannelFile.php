<?php
class Application_Model_PgcheckChannelFile extends Zend_Db_Table_Abstract
{
    protected $_name = 'pgcheck_channel_file';
    
    public function getListImageChannel($channel_id_list) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.channel_id",
            "p.url",
            "p.type",
        );

        $select->from(array('p' => 'pgcheck_channel_file'), $arrCols);
        
        $select->where('type = ?', 1);
        $select->where('is_del = ?', 0);
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getListBtnChannel($channel_id_list) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.channel_id",
            "p.url",
            "p.type",
        );

        $select->from(array('p' => 'pgcheck_channel_file'), $arrCols);
        
        $select->where('type = ?', 3);
        $select->where('is_del = ?', 0);
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getListFlashSaleChannel($channel_id_list) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.channel_id",
            "p.url",
            "p.type",
        );

        $select->from(array('p' => 'pgcheck_channel_file'), $arrCols);
        
        $select->where('type = ?', 2);
        $select->where('is_del = ?', 0);
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
    public function getList($channel_id_list){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.channel_id",
            "p.url",
            "p.type",
        );

        $select->from(array('p' => 'pgcheck_channel_file'), $arrCols);
        
        $select->where('is_del = ?', 0);
        
        $select->where('channel_id IN (?)', $channel_id_list);
        
        $result = $db->fetchAll($select);

        return $result;
    }
    
}