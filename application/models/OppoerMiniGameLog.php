<?php

class Application_Model_OppoerMiniGameLog extends Zend_Db_Table_Abstract {

    protected $_name = 'oppoer_minigame_log';

    public function getLogByItem($week) {
         $db = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('p' => $this->_name), array('p.item_id','count'=>'count(p.id)','p.item_name'))
                ->where('p.week = ?',$week )
                ->group('p.item_id');
        $data = $db->fetchAll($select);
        return $data;
    }
     public function getLog($week) {
         $db = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('p' => $this->_name), array('p.item_id','count'=>'count(p.id)','p.item_name'))
                ->where('p.week = ?',$week );
        $data = $db->fetchAll($select);
        return $data;
    }

}
