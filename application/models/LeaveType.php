<?php

class Application_Model_LeaveType extends Zend_Db_Table_Abstract{

    protected $_name = 'leave_type';

	private function dbconnect(){
		return Zend_Registry::get('db');
	}

    public function insertLeaveType($params = array())
    {
        if(!empty($params['note']) && isset($params['status']))
        {
            if(empty($params['parent']))
            {
                $params['parent'] = null;
            }
            $this->insert($params);
        }
    }

    public function getParent($params = array())
    {
        $db = $this->dbconnect();
        $sql = "select * from leave_type where (parent is null or parent = 0) and status = 1";
        if(!empty($params['id']))
        {
            $sql .= " and id <> " . $params['id'];
        }
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $db = $stmt = null;
        return $data;
    }

    public function getChild($parent_id = -1)
    {
        $db = $this->dbconnect();
        $sql = "select * from leave_type where status = 1 and parent = " . $parent_id;
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $db = $stmt = null;
        return $data;
    }

    public function findById($id)
    {
        $db = $this->dbconnect();

        $stmt = $db->prepare("select * from leave_type where id = :id");
        $stmt->bindParam('id', $id, PDO::PARAM_INT);
        $stmt->execute();
        $data = $stmt->fetch();

        $stmt->closeCursor();
        $stmt = $db = null;

        return $data;
    }

    public function updateLeaveType($params = array())
    {
        if(!empty($params['note']) && isset($params['status']) && isset($params['id']))
        {
            $data = array(
                'note' => $params['note'],
                'status' => $params['status'],
                'parent' => $params['parent'],
                'sub_stock' => $params['sub_stock'],
                'sub_time' => $params['sub_time'], 
                'hr_approved' => $params['hr_approved'],
                'need_images' => $params['need_images'],
                'need_insurance' => $params['need_insurance'],
                'max_day_per_time' => $params['max_day_per_time'],
                'max_day_per_year' => $params['max_day_per_year'],
                'chedo' => $params['chedo'],
                'summary' => $params['summary'],
            );

            

            if(empty($data['parent']))
            {
                $data['parent'] = new Zend_Db_Expr('NULL');
            }
            $where = $this->getAdapter()->quoteInto('id = ?',$params['id']);
            $this->update($data, $where);
        }
    }

    public function selectActive()
    {
        $db = $this->dbconnect();

        $stmt = $db->prepare("select id, note from " . $this->_name . " where status = 1");
        $stmt->execute();
        $data = $stmt->fetchAll();

        $db = $stmt = null;
        return $data;
    }

    public function selectAll()
    {
        $db = $this->dbconnect();

        $sql = "
                SELECT 
                    lt.note AS `note`,
                    lt.id AS `id`,
                    ltpr.note AS `parent`,
                    lt.status AS `status`,
                    lt.sub_stock AS `sub_stock`
                FROM leave_type lt
                LEFT JOIN leave_type ltpr ON lt.parent = ltpr.id
                ";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();

        $db = $stmt = null;
        return $data;
    }

    public function selectById($id = -1)
    {
        $db = $this->dbconnect();

        $stmt = $db->prepare("select * from " . $this->_name . " where id = :id");
        $stmt->bindParam("id", intval($id), PDO::PARAM_INT);
        $stmt->execute();
        $data = $stmt->fetch();

        $db = $stmt = null;
        return $data;
    }
    
    
    public function get_type_name()
    {
        $cache  = Zend_Registry::get('cache');
        $result = $cache->load($this->_name . '_cache_type_name');

        if ($result === false) {
            $db = Zend_Registry::get('db');

            $select = $db->select()
                ->from(array('p' => $this->_name), array('p.id', 'p.*'));
            $data = $db->fetchAll($select);

            $result = array();

            if ($data)
                foreach ($data as $item)
                    $result[ $item['id'] ] = $item['note'];

            $cache->save($result, $this->_name.'_cache_type_name', array(), null);
        }

        return $result;
    }

}