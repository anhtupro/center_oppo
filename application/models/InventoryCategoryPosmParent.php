<?php

class Application_Model_InventoryCategoryPosmParent extends Zend_Db_Table_Abstract
{
    protected $_name = 'inventory_category_posm_parent';
    protected $_schema = DATABASE_TRADE;

    public function getAll($params = null)
    {

        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['p' => DATABASE_TRADE.'.inventory_category_posm_parent'], [
                         'p.id',
                         'p.name'
                     ]);

        if ($params['not_inventory_head_office']) {
            $select->where('p.inventory_head_office IS NULL');
        }

        $result = $db->fetchAll($select);

        return $result;
    }
}