<?php
class Application_Model_UnitCode extends Zend_Db_Table_Abstract
{
    protected $_name = 'unit_code';

    function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        $select = $db->select()
            ->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*'));

        if (isset($params['unit_code']) and $params['unit_code'])
            $select->where('p.unit_code LIKE ?', '%'.$params['unit_code'].'%');

        if (isset($params['company_id']) and $params['company_id'])
            $select->where('p.company_id = ?', $params['company_id']);

        $select->order('p.unit_code', 'COLLATE utf8_unicode_ci ASC');

        if ($limit)
            $select->limitPage($page, $limit);

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    function get_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_cache');

        if ($result === false) {

            $data = $this->fetchAll(null, 'unit_code');

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[$item->id] = $item->unit_code;
                }
            }
            $cache->save($result, $this->_name.'_cache', array(), null);
        }
        return $result;
    }

    function get_code_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_code_cache');

        if ($result === false) {

            $data = $this->fetchAll();

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[strtolower($item->unit_code)] = $item->id;
                }
            }
            $cache->save($result, $this->_name.'_code_cache', array(), null);
        }
        return $result;
    }

    function get_by_company_cache(){
        $cache      = Zend_Registry::get('cache');
        $result     = $cache->load($this->_name.'_by_company_cache');

        if ($result === false) {

            $data = $this->fetchAll();

            $result = array();
            if ($data){
                foreach ($data as $item){
                    $result[strtolower($item->company_id)] = $item->id;
                }
            }
            $cache->save($result, $this->_name.'_by_company_cache', array(), null);
        }
        return $result;
    }

    public function get_all(){
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('p'=>$this->_name),array('p.id','p.unit_code'))
            ->order('p.unit_code ASC')    
            ;
        $result = $db->fetchPairs($select);
        return $result;    
    }

    public function get_unit_company(){
        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('p'=>$this->_name),array('p.id','p.company_id'))
            ->order('p.id ASC')    
            ;
        $result = $db->fetchPairs($select);
        return $result;   
    }
}