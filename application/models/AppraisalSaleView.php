<?php

class Application_Model_AppraisalSaleView extends Zend_Db_Table_Abstract
{
    const TITLE_RSM_ID = 308;
    const TITLE_ASM_ID = 179;
    const TITLE_ASM_STANDBY_ID = 181;
    const TITLE_SALE_LEADER_ID = 190;
    const TITLE_SALE_ID = 183;
    const TITLE_PG_ID = 182;

    const TITLE_RSM = 'RSM';
    const TITLE_ASM = 'ASM';
    const TITLE_ASM_STANDBY = 'ASM';
    const TITLE_SALE_LEADER = 'LEADER';
    const TITLE_SALE = 'SALE';
    const TITLE_PG = 'PG';

    protected $_name = 'v_appraisal_sale';
    protected $_primary = 'id';

    /**
     * @param $staffId
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getToDoSurvey($staffId)
    {
        $data = [
            'superior' => [],
            'self' => [],
            'subordinate' => []
        ];
        $QPlan = new Application_Model_AppraisalSalePlan();
        $plan = $QPlan->getLastPlan();
        // Check time run plan
        if (is_null($plan) && $plan['asp_status'] == 2) {
            return $data;
        }
        // Get data
        $planId = $plan['asp_id'];
        $QToDo = new Application_Model_AppraisalSaleToDo();
        $result = $QToDo->getToDoSurvey($planId, $staffId);
        foreach ($result as $todo) {
            $tmp = [
                'id' => $todo['to_staff'],
                'title' => $QToDo->convertTitleFromId($todo['to_title'])
            ];
            switch ($todo['type']) {
                case 0:
                    $data['self'][] = $tmp;
                    break;
                case 1:
                    $data['subordinate'][] = $tmp;
                    break;
                case 2:
                    $data['superior'][] = $tmp;
                    break;
            }
        }
        return $data;
    }

    /**
     * @param $userId
     * @return bool
     * @throws Zend_Exception
     */
    public function hasSurveyToDo($userId)
    {
        $QPlan = new Application_Model_AppraisalSalePlan();
        $plan = $QPlan->getLastPlan();
        
        
        
        if ($plan === null) {
            return false;
        }
        $toDo = $this->getToDoSurvey($userId);
        
        
        
        $QMember = new Application_Model_AppraisalSaleMember();
        $members = $QMember->getByPlanAndStaff($plan['asp_id'], $userId);
        $total = array_sum(array_map('count', $toDo));
        
        
        
        return $total != count($members);
    }

    /**
     * @param $staffId string
     * @param $title
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getTotalAppraisalSale($staffId, $title)
    {
        $sql = 'select p.asp_id, p.asp_name, ifnull(round(sum(m.asm_total_point / m.asm_count_question) / count(*), 1), 0) total
from (select distinct if(td.to_title = :title, td.to_staff, if(td1.to_title = :title, td1.to_staff, if(td2.to_title = :title, td2.to_staff, td3.to_staff))) staff_id, td.asp_id
      from tmp_appraisal_to_do td
        left join tmp_appraisal_to_do td1 on td.asp_id = td1.asp_id and td.to_staff = td1.from_staff and td.to_title = td1.from_title and td.type = 1
        left join tmp_appraisal_to_do td2 on td1.asp_id = td2.asp_id and td1.to_staff = td2.from_staff and td1.to_title = td2.from_title and td1.type = 1
        left join tmp_appraisal_to_do td3 on td2.asp_id = td3.asp_id and td2.to_staff = td3.from_staff and td2.to_title = td3.from_title and td2.type = 1
      where (td.to_title = :title or td1.to_title = :title or td2.to_title = :title or td3.to_title = :title) and (:staff_id = 0 or td.from_staff = :staff_id)) l
  join tmp_appraisal_to_do td on l.asp_id = td.asp_id and l.staff_id = td.to_staff and td.from_title != 182
  join (select * from appraisal_sale_plan where asp_status != 0 and asp_is_deleted = 0 order by asp_id desc limit 4) p on l.asp_id = p.asp_id
  left join appraisal_sale_member m on td.asp_id = m.asp_id and td.to_staff = m.asm_staff_be_appraisal and td.from_staff = m.asm_staff_id and m.asm_is_deleted = 0
group by l.asp_id
order by l.asp_id desc';
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare($sql);
        $stmt->bindParam('staff_id', $staffId, PDO::PARAM_INT);
        $stmt->bindParam('title', $title, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return array_reverse($result);
    }

    /**
     * @param $title
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getTotalAppraisalByTitle($title)
    {
        $sql = "select p.asp_id, p.asp_name, ifnull(round(sum(result) / count(*), 1), 0) total
from (select td.asp_id, to_staff, ifnull(round(sum(m.asm_total_point / m.asm_count_question) / count(*), 1), 0) result
  from tmp_appraisal_to_do td
    left join appraisal_sale_member m on td.asp_id = m.asp_id and td.from_staff = m.asm_staff_id and td.to_staff = m.asm_staff_be_appraisal and m.asm_is_deleted = 0
  where to_title = :title
  group by td.asp_id, to_staff) tmp
join appraisal_sale_plan p on tmp.asp_id = p.asp_id
group by p.asp_id
order by p.asp_id desc
limit 4";
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare($sql);
        $stmt->bindParam('title', $title, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return array_reverse($result);
    }

    public function getTotalAppraisalSubMember($boss_id)
    {
        $db = Zend_Registry::get('db');

        $sql = "SELECT a.`asp_id`,
                        asp.asp_name AS asp_name,
                        ROUND(SUM(IFNULL(a.`asm_total_point`, 0) / IF(a.`asm_count_question` IS NULL OR a.`asm_count_question` = 0, 1, a.`asm_count_question`)) / COUNT(*), 1) AS total
                FROM `appraisal_sale_member` AS a
                JOIN (
                        SELECT `asm_staff_id`, `asm_staff_be_appraisal`, `asp_id`
                        FROM `appraisal_sale_member`
                        WHERE `asm_staff_be_appraisal` = $boss_id AND `asm_type` = 2
                     ) AS b ON a.`asm_staff_be_appraisal` = b.`asm_staff_id` AND a.`asp_id` = b.`asp_id` 
                LEFT JOIN appraisal_sale_plan as asp ON a.`asp_id` = asp.asp_id
                WHERE  asp.asp_status != 0 AND asp.asp_is_deleted = 0
                GROUP BY a.`asp_id` 
                LIMIT 4 ";

        $stmt = $db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();

        return $result;
    }

    public function getTotalAppraisalSaleLeaderByAsm($asm_id)
    {
        $sql = "select asp_id, asp_name, round(sum(asm_total_point / asm_count_question) / count(*), 1) total
from (select distinct s.asp_id, p.asp_name, s.asm_id, s.leader_id, m.asm_count_question, m.asm_total_point
from tmp_appraisal_sale s
join appraisal_sale_plan as p ON s.asp_id = p.asp_id AND p.asp_status != 0 AND p.asp_is_deleted = 0
  left join appraisal_sale_member m on s.leader_id = m.asm_staff_be_appraisal and s.asp_id = m.asp_id
where s.asm_id = :staff_id and s.leader_id is not null) tmp
group by asp_id desc
limit 4";
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare($sql);
        $stmt->bindParam('staff_id', $asm_id, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return array_reverse($result);
    } // Done

    public function getTotalAppraisalSaleByAsm($asm_id)
    {
        $sql = "select asp_id, asp_name, round(sum(asm_total_point / asm_count_question) / count(*), 1) total
from (select distinct s.asp_id, p.asp_name, s.asm_id, s.sale_id, m.asm_count_question, m.asm_total_point
from tmp_appraisal_sale s
join appraisal_sale_plan as p ON s.asp_id = p.asp_id AND p.asp_status != 0 AND p.asp_is_deleted = 0
  left join appraisal_sale_member m on s.sale_id = m.asm_staff_be_appraisal and s.asp_id = m.asp_id
where s.asm_id = :staff_id and s.sale_id is not null) tmp
group by asp_id desc
limit 4";
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare($sql);
        $stmt->bindParam('staff_id', $asm_id, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();

        return array_reverse($result);
    } // Done

    public function getLeaderByAsm($asmId, $planId)
    {
        $sql = "select distinct st.id sub_member_id, concat(st.firstname,' ', st.lastname) AS sub_member_name
from tmp_appraisal_sale s
left join staff AS st ON s.leader_id = st.id
where s.asm_id = :staff_id and s.asp_id = :plan_id and s.leader_id is not null";
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare($sql);
        $stmt->bindParam('staff_id', $asmId, PDO::PARAM_INT);
        $stmt->bindParam('plan_id', $planId, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();

        return array_reverse($result);
    }

    public function getSaleByAsm($asmId, $planId)
    {
        $sql = "select distinct st.id sub_member_id, concat(st.firstname,' ', st.lastname) AS sub_member_name
from tmp_appraisal_sale s
left join staff AS st ON s.sale_id = st.id
where s.asm_id = :staff_id and s.asp_id = :plan_id and s.sale_id is not null";
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare($sql);
        $stmt->bindParam('staff_id', $asmId, PDO::PARAM_INT);
        $stmt->bindParam('plan_id', $planId, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();

        return array_reverse($result);
    }

    /**
     * @param $planId
     * @param $userId
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getDetailAppraisal($planId, $userId)
    {
        $QPlan = new Application_Model_AppraisalSalePlan();
        $plan = $QPlan->getPlanById($planId);

        $surveyId = $plan['asp_survey_rsm_asm'];
        $QToDo = new Application_Model_AppraisalSaleToDo();
        if (in_array($QToDo->getTitleById($userId, $planId), [self::TITLE_SALE_LEADER_ID, self::TITLE_SALE_ID])) {
            $surveyId = $plan['asp_survey_sale_leader'];
        }
        if (in_array($QToDo->getTitleById($userId, $planId), [self::TITLE_PG_ID])) {
            $surveyId = $plan['asp_survey_pg'];
        }
        $answersSale = self::getResultAppraisalSale($planId, $surveyId, $userId);
        $QMember = new Application_Model_AppraisalSaleMember();
        $memberComment = $QMember->getCommentForStaff($planId, $userId);
        $dataComment = [];
        foreach ($memberComment as $member) {
            $tmp = [
                'name' => '---',
                'asm_promote' => $member['asm_promote'],
                'asm_improve' => $member['asm_improve'],
                'asm_staff_title' => $member['asm_staff_title'],
            ];
            if ($member['asm_type'] == 1) { // Cấp trên
                $QStaff = new Application_Model_Staff();
                $staff = $QStaff->fetchRow($QStaff->getAdapter()->quoteInto('id = ?', $member['asm_staff_id']));
                if ($staff) {
                    $tmp['name'] = $staff['firstname'] . ' ' . $staff['lastname'];
                }
            }
            $dataComment[] = $tmp;
        }
        $result = [
            'data_sale' => $answersSale,
            'data_comment' => $dataComment
        ];
        if ($QToDo->getTitleById($userId, $planId) == self::TITLE_SALE_ID) {
            $answersPg = self::getResultPGAppraisalSale($planId, $userId);
            $result['data_pg'] = $answersPg;
        }
        return $result;
    }

    /**
     * @param $planId
     * @param $surveyId
     * @param $staffId
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getResultAppraisalSale($planId, $surveyId, $staffId)
    {
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $query = "select q.dsq_id, q.dsq_title question, ifnull(round(sum(a.dsa_value) / count(*), 1), 0) result
from dynamic_survey ds
  join dynamic_survey_question q on ds.ds_id = q.fk_ds and ds.ds_id = :survey_id and q.dsq_type in ('range', 'radio', 'checkbox', 'select')
  join tmp_appraisal_to_do td on td.asp_id = :plan_id and td.to_staff = :staff_id and td.from_title != 182
  left join appraisal_sale_member m on td.asp_id = m.asp_id and td.from_staff = m.asm_staff_id and td.to_staff = m.asm_staff_be_appraisal and m.asm_is_deleted = 0
  left join dynamic_survey_result_detail rd on m.fk_dsr = rd.fk_dsr and q.dsq_id = rd.fk_dsq
  left join dynamic_survey_answer a on rd.fk_dsa = a.dsa_id and q.dsq_id = a.fk_dsq
group by q.dsq_id";
        /** @var \Zend_Db_Statement $stmt */
        $stmt = $db->prepare($query);
        $stmt->bindParam('staff_id', $staffId, PDO::PARAM_INT);
        $stmt->bindParam('plan_id', $planId, PDO::PARAM_INT);
        $stmt->bindParam('survey_id', $surveyId, PDO::PARAM_INT);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        return $data;
    }

    /**
     * @param $planId
     * @param $staffId
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getResultPGAppraisalSale($planId, $staffId)
    {
        $query = "select dsq_title question, ifnull(round(sum(dsa_value) / count(*), 1), 0) result
from dynamic_survey_result_detail rd
join dynamic_survey_question q on rd.fk_dsq = q.dsq_id
join dynamic_survey_answer a on rd. fk_dsa = a.dsa_id
join appraisal_sale_member m on rd.fk_dsr = m.fk_dsr and m.asp_id = :plan_id and m.asm_staff_be_appraisal = :staff_id and m.asm_staff_title = 182
right join tmp_appraisal_to_do td on td.from_title = 182 and m.asm_staff_id = td.from_staff and m.asm_is_deleted = 0
group by dsq_id
having dsq_id is not null";
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare($query);
        $stmt->bindParam('staff_id', $staffId, PDO::PARAM_INT);
        $stmt->bindParam('plan_id', $planId, PDO::PARAM_INT);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $stmt->closeCursor();
        return $data;
    }

    /**
     * @param $bossId
     * @param $subTitle
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getResultByLevel($bossId, $subTitle)
    {
        $query = "select l.asp_id, l.staff_id, concat(s.firstname, ' ', s.lastname) staff_name, ifnull(round(sum(m.asm_total_point / m.asm_count_question) / count(*), 1), 0) result
from (select distinct if(td.to_title = :title, td.to_staff, if(td1.to_title = :title, td1.to_staff, if(td2.to_title = :title, td2.to_staff, td3.to_staff))) staff_id, td.asp_id
      from tmp_appraisal_to_do td
        left join tmp_appraisal_to_do td1 on td.asp_id = td1.asp_id and td.to_staff = td1.from_staff and td.to_title = td1.from_title and td.type = 1
        left join tmp_appraisal_to_do td2 on td1.asp_id = td2.asp_id and td1.to_staff = td2.from_staff and td1.to_title = td2.from_title and td1.type = 1
        left join tmp_appraisal_to_do td3 on td2.asp_id = td3.asp_id and td2.to_staff = td3.from_staff and td2.to_title = td3.from_title and td2.type = 1
      where (td.to_title = :title or td1.to_title = :title or td2.to_title = :title or td3.to_title = :title) and td.from_staff = :staff_id) l
  join tmp_appraisal_to_do td on l.asp_id = td.asp_id and l.staff_id = td.to_staff and td.from_title != 182
  join (select * from appraisal_sale_plan where asp_status != 0 and asp_is_deleted = 0 order by asp_id desc limit 4) p on l.asp_id = p.asp_id
  join staff s on l.staff_id = s.id
  left join appraisal_sale_member m on td.asp_id = m.asp_id and td.to_staff = m.asm_staff_be_appraisal and td.from_staff = m.asm_staff_id and m.asm_is_deleted = 0
group by l.asp_id, l.staff_id";
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare($query);
        $stmt->bindParam('title', $subTitle, PDO::PARAM_INT);
        $stmt->bindParam('staff_id', $bossId, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();

        $data = [];
        foreach ($result as $value) {
            if ($data[$value['asp_id']] == null) {
                $data[$value['asp_id']] = [];
            }
            if ($value['staff_id'] != null) {
                if ($data[$value['asp_id']][$value['staff_id']] == null) {
                    $data[$value['asp_id']][$value['staff_id']] = [
                        'staff_name' => '',
                        'result' => 0,
                    ];
                }
                $data[$value['asp_id']][$value['staff_id']]['staff_name'] = $value['staff_name'];
                $data[$value['asp_id']][$value['staff_id']]['result'] += $value['result'];
            }
        }
        return $data;
    }

    /**
     * @param $planId
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getResultRsmRegion($planId)
    {
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');

        $sql = "select reg.id, reg.name, reg.rsm_id staff_id, round(sum(ifnull(mem.asm_total_point, 0)) / sum(if(asm_count_question != 0 and asm_count_question is not null, asm_count_question, 1)), 1) as result
from region reg
  left join appraisal_sale_member mem on reg.id = mem.asm_to_cache and mem.asm_staff_be_appraisal_title = 308 and mem.asp_id = :plan_id
where reg.del = 0 and reg.status = 1
group by reg.id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('plan_id', $planId, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();

        $data = [];
        foreach ($result as $region) {
            $data[] = [
                'region_id' => $region['id'],
                'region_name' => $region['name'],
                'staff_id' => $region['staff_id'],
                'result' => $region['result'],
            ];
        }

        return $data;
    }

    /**
     * @param $planId
     * @param $regionId
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getResultAsmArea($planId, $regionId)
    {
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');

        $sql = "select area.id, area.name, area.asm_id staff_id, round(sum(ifnull(mem.asm_total_point, 0)) / sum(if(asm_count_question != 0 and asm_count_question is not null, asm_count_question, 1)), 1) as result
from area
  left join appraisal_sale_member mem on area.id = mem.asm_to_cache and mem.asm_staff_be_appraisal_title = 179 and mem.asp_id = :plan_id
where area.name is not null and area.region_id = :region_id and area.del = 0 and area.status = 1
group by area.id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('plan_id', $planId, PDO::PARAM_INT);
        $stmt->bindParam('region_id', $regionId, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();

        $data = [];
        foreach ($result as $region) {
            $data[] = [
                'area_id' => $region['id'],
                'area_name' => $region['name'],
                'staff_id' => $region['staff_id'],
                'result' => $region['result'],
            ];
        }
        return $data;
    }

    /**
     * @param array $data
     * @param int $total
     * @param int $page
     * @param bool $export
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getResult($data, &$total, $page = 0, $export = false)
    {
        if($export == 0) {
            $query = "select sql_calc_found_rows td.asp_id, sf.code from_staff_code, td.from_staff, concat(sf.firstname, ' ', sf.lastname) from_name, td.from_title, st.code to_staff_code, td.to_staff, concat(st.firstname, ' ', st.lastname) to_name, td.to_title, m.asm_created_at, round(m.asm_total_point / m.asm_count_question, 1) point
from tmp_appraisal_to_do td
  join staff sf on td.from_staff = sf.id and td.asp_id = :plan_id
  join staff st on td.to_staff = st.id
  left join appraisal_sale_member m on td.asp_id = m.asp_id and td.from_staff = m.asm_staff_id and td.to_staff = m.asm_staff_be_appraisal and m.asm_is_deleted = 0
where concat(sf.firstname, ' ', sf.lastname) like concat(N'%', :from_name, '%') and concat(st.firstname, ' ', st.lastname) like concat(N'%', :to_name, '%')
  and (:from_title = 0 or td.from_title = :from_title) and (:to_title = 0 or td.to_title = :to_title)
limit :offset, :limit";
        } else {
            if($export == 1) {
                $fromTitle = [462, 308, 179, 190, 183];
                $toTitle = [308, 179];
            } elseif($export == 2) {
                $fromTitle = [308, 179, 190, 183];
                $toTitle = [190, 183];
            } else {
                $fromTitle = [182];
                $toTitle = [183];
            }
            $query = "select sql_calc_found_rows td.asp_id, sf.code from_staff_code, af.name from_staff_area, td.from_staff, concat(sf.firstname, ' ', sf.lastname) from_name, td.from_title, st.code to_staff_code, at.name to_staff_area, td.to_staff, concat(st.firstname, ' ', st.lastname) to_name, td.to_title, m.asm_created_at, round(m.asm_total_point / m.asm_count_question, 1) point, q.dsq_id question_id, q.dsq_title question, if(rd.fk_dsa, a.dsa_title, rd.dsrd_text) value
from tmp_appraisal_to_do td
  join staff sf on td.from_staff = sf.id and td.asp_id = :plan_id
  join regional_market rmf on sf.regional_market = rmf.id
  join area af on af.id = rmf.area_id
  join staff st on td.to_staff = st.id
  join regional_market rmt on st.regional_market = rmt.id
  join area at on at.id = rmt.area_id
  left join appraisal_sale_member m on td.asp_id = m.asp_id and td.from_staff = m.asm_staff_id and td.to_staff = m.asm_staff_be_appraisal and m.asm_is_deleted = 0
  left join dynamic_survey_result_detail rd on m.fk_dsr = rd.fk_dsr
  left join dynamic_survey_question q on rd.fk_dsq = q.dsq_id
  left join dynamic_survey_answer a on rd.fk_dsa is not null and rd.fk_dsa = a.dsa_id
where concat(sf.firstname, ' ', sf.lastname) like concat(N'%', :from_name, '%') and concat(st.firstname, ' ', st.lastname) like concat(N'%', :to_name, '%')";
            $query = $query . sprintf(' and (:from_title = 0 or td.from_title = :from_title) and (:to_title = 0 or td.to_title = :to_title) and td.from_title in (%s) and td.to_title in (%s)', implode(",", $fromTitle), implode(",", $toTitle));
        }
        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare($query);
        $fromName = $data['from_name'];
        $toName = $data['to_name'];
        $stmt->bindParam('from_name', $fromName, PDO::PARAM_STR);
        $stmt->bindParam('to_name', $toName, PDO::PARAM_STR);
        $stmt->bindParam('from_title', $data['from_title'], PDO::PARAM_INT);
        $stmt->bindParam('to_title', $data['to_title'], PDO::PARAM_INT);
        $stmt->bindParam('plan_id', $data['plan_id'], PDO::PARAM_INT);

        $limit = 20;
        if($export == 0) {
            $page --;
            $offset = $limit * $page;
            $stmt->bindParam('offset', $offset, PDO::PARAM_INT);
            $stmt->bindParam('limit', $limit, PDO::PARAM_INT);
        }
        $stmt->execute();
        $result = $stmt->fetchAll();
        $total = $db->fetchOne("select found_rows()");
        $stmt->closeCursor();
        return $result;
    }

    /**
     * @param $planId
     * @param $fromStaff
     * @param $toStaff
     * @return array
     * @throws Zend_Db_Statement_Exception
     * @throws Zend_Exception
     */
    public function getResultDetail($planId, $fromStaff, $toStaff)
    {
        $query = "select q.dsq_id question_id, q.dsq_title question, if(rd.fk_dsa, a.dsa_title, rd.dsrd_text) value
from tmp_appraisal_to_do td
  join staff sf on td.from_staff = sf.id
  join staff st on td.to_staff = st.id
  left join appraisal_sale_member m on td.asp_id = m.asp_id and td.from_staff = m.asm_staff_id and td.to_staff = m.asm_staff_be_appraisal and m.asm_is_deleted = 0 and m.asp_id = :plan_id
  left join dynamic_survey_result_detail rd on m.fk_dsr = rd.fk_dsr
  left join dynamic_survey_question q on rd.fk_dsq = q.dsq_id
  left join dynamic_survey_answer a on rd.fk_dsa is not null and rd.fk_dsa = a.dsa_id
where m.asm_staff_id = :from_staff and m.asm_staff_be_appraisal = :to_staff and m.asp_id = :plan_id";

        /** @var Zend_Db_Adapter_Abstract $db */
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare($query);
        $stmt->bindParam('plan_id', $planId, PDO::PARAM_INT);
        $stmt->bindParam('from_staff', $fromStaff, PDO::PARAM_INT);
        $stmt->bindParam('to_staff', $toStaff, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt->closeCursor();
        return $result;
    }
}