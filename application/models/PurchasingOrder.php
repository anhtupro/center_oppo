<?php
class Application_Model_PurchasingOrder extends Zend_Db_Table_Abstract
{
    protected $_name = 'purchasing_order';

    function fetchPagination($page, $limit, &$total, $params){

        $db = Zend_Registry::get('db');

        $col = array(
                new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 
                'p.sn', 
                'fullname' => "CONCAT(s.firstname, ' ',s.lastname)", 
                'p.po_name', 
                'p.remark', 
                'p.quotation', 
                'p.delivery_date', 
                'p.status', 
                'p.supplier_id',
                'p.created_at', 
                'p.confirm_at',
                'p.confirm_by',
                'supplier_name' => 'sup.title'  
        );

        $select = $db->select()->from(array('p' => $this->_name), $col);

        $select->joinleft(array('s'=>'staff'), 's.id = p.created_by', array());
        $select->joinleft(array('sup'=>'supplier'), 'sup.id = p.supplier_id', array());

        if(!empty($params['sn'])){
            $select->where('sn = ?', $params['sn']);
        }
        if(!empty($params['created_at_from'])){
            $select->where('DATE(p.created_at) >= ?', $params['created_at_from']);
        }
        if(!empty($params['created_at_to'])){
            $select->where('DATE(p.created_at) <= ?', $params['created_at_to']);
        }

        $select->group('p.id');

        $select->order('p.created_at DESC');
        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }

    function getPurchasingOrder($params){

        $db = Zend_Registry::get('db');

        $col = array(
                'p.sn', 
                's.title', 
                'p.id', 
                'p.created_at', 
                'p.po_name', 
                'p.remark', 
                'p.quotation', 
                'p.delivery_date', 
                'p.status', 
                'supplier_name' => 'sup.title',
                'supplier_contact_person' => 'sup.name',
                'p.supplier_id',
                'created_by'    => "CONCAT(s.firstname, ' ', s.lastname)",
                'p.confirm_by'
            
        );

        $select = $db->select()->from(array('p' => $this->_name), $col);

        $select->joinleft(array('s'=>'staff'), 's.id = p.created_by', array());
        $select->joinleft(array('sup'=>'supplier'), 'sup.id = p.supplier_id', array());

        if(!empty($params['sn'])){
            $select->where('sn = ?', $params['sn']);
        }

        $result = $db->fetchRow($select);

        return $result;
    }

    
    function listPO($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');

        $select_table_temp = "(
                                        SELECT 
                                                `p`.`sn`,
                                                `p`.`po_name`,
                                                `p`.`delivery_date`,
                                                `p`.`remark`,
                                                `p`.`supplier_id`,
                                                `p`.`created_at`,
                                                `p`.`created_by`,
                                                `p`.`status`,
                                                `p`.`confirm_at`,
                                                `p`.`confirm_by`,
                                                 'PO' as type
                                        FROM
                                                `purchasing_order` AS `p`

                                        GROUP BY
                                                `p`.`id`

                )";
        
        $select = $db->select()
        ->from(array('p' => new Zend_Db_Expr($select_table_temp)),
            array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.sn'), 'p.*'));
        
        $select->joinleft(array('s'=>'staff'), 's.id = p.created_by', array('fullname' => "CONCAT(s.firstname, ' ',s.lastname)"));
        $select->joinleft(array('sup'=>'supplier'), 'sup.id = p.supplier_id', array('supplier' => "sup.name"));
        
        if(!empty($params['sn'])){
            $select->where('sn = ?', $params['sn']);
        }
        if(!empty($params['supplier_id'])){
            $select->where('supplier_id = ?', $params['supplier_id']);
        }
        if(!empty($params['name'])){
            $select->where('po_name LIKE ?', '%'.$params['name'].'%');
        }
        if(!empty($params['remark'])){
            $select->where('remark LIKE ?', '%'.$params['remark'].'%');
        }
        if(!empty($params['created_at_from'])){
            $select->where('DATE(p.created_at) >= ?', $params['created_at_from']);
        }
        if(!empty($params['created_at_to'])){
            $select->where('DATE(p.created_at) <= ?', $params['created_at_to']);
        }
 
        if(!empty($params['list_staff'])){
            $select->where('p.created_by IN (?)', $params['list_staff']);
        } 

        
        $select->order('p.created_at DESC');
        if($limit){
            $select->limitPage($page, $limit);
        }
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
    
    
    
    function getinfoPO($params){

        $db = Zend_Registry::get('db');

        $col = array(
                'p.sn', 
                's.title', 
                'p.id', 
                'p.created_at', 
                'p.po_name', 
                'p.remark', 
                'p.quotation', 
                'p.delivery_date', 
                'p.status', 
                'supplier_name' => 'sup.title',
                'supplier_contact_person' => 'sup.name',
                'p.supplier_id',
                'created_by'    => "CONCAT(s.firstname, ' ', s.lastname)",
                'area_created_by' => 'ar.name',
                'department_created_by' => 't.name',
                'p.confirm_by',
                'confirm_by'    => "CONCAT(st.firstname, ' ', st.lastname)",
                'p.confirm_at'
            
        );

        $select = $db->select()->from(array('p' => $this->_name), $col);

        $select->joinleft(array('s'=>'staff'), 's.id = p.created_by', array());              
        $select->joinleft(array('st'=>'staff'), 'st.id = p.confirm_by', array());
        $select->joinleft(array('sup'=>'supplier'), 'sup.id = p.supplier_id', array());
        $select->joinleft(array('rm'=>'regional_market'), 's.regional_market = rm.id', array());
        $select->joinleft(array('ar'=>'area'), 'rm.area_id = ar.id', array());
        $select->joinleft(array('t'=>'team'), 't.id = s.department', array());     
        
        if(!empty($params['sn'])){
            $select->where('sn = ?', $params['sn']);
        }

        $result = $db->fetchRow($select);

        return $result;
    }
    
    
    function reportPO($params){
        
        $db = Zend_Registry::get('db');
        $select_table_temp = "(SELECT b.sn, b.po_name, DATE(b.delivery_date) as delivery_date, 
                                        pc.`name` as product, sup.title as supplier,
                                        a.quantity, a.price, a.price*1.1 as after_vat, a.price*a.quantity*1.1 as total,
                                        table_total.total_po*1.1 as total_po,
                                        b.`status`, b.remark, 
                                        ar.`name` as area, dep.`name` as department,
                                        CONCAT(s.firstname,' ',s.lastname) AS created_by_name, b.created_at, 
                                        CONCAT(s2.firstname,' ',s2.lastname) AS confirm_by_name, b.confirm_at,
                                        b.created_by, b.supplier_id
                                FROM `purchasing_order_details` a
                                JOIN purchasing_order b ON a.po_id = b.id
                                LEFT JOIN staff s ON b.created_by = s.id
                                LEFT JOIN staff s2 ON b.confirm_by = s2.id
                                LEFT JOIN (	SELECT a.id, SUM(price*quantity) as total_po FROM `purchasing_order_details` a
                                                 GROUP BY a.po_id ) table_total ON a.id = table_total.id
                                LEFT JOIN pmodel_code pc ON a.pmodel_code_id = pc.`id`
                                LEFT JOIN supplier sup ON sup.id = b.supplier_id
                                LEFT JOIN purchasing_request_details prd ON a.pr_details_id = prd.id
                                LEFT JOIN purchasing_request pr ON prd.purchasing_request_id = pr.id
                                LEFT JOIN area ar ON ar.id = pr.area_id
                                LEFT JOIN team dep ON pr.department_id = dep.id
                            )";
        
        $select = $db->select()
        ->from(array('p' => new Zend_Db_Expr($select_table_temp)),
            array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.sn'), 'p.*'));
        
        if(!empty($params['sn'])){
            $select->where('p.sn = ?', $params['sn']);
        }
        if(!empty($params['supplier_id'])){
            $select->where('supplier_id = ?', $params['supplier_id']);
        }
        if(!empty($params['name'])){
            $select->where('p.po_name LIKE ?', '%'.$params['name'].'%');
        }
        if(!empty($params['remark'])){
            $select->where('p.remark LIKE ?', '%'.$params['remark'].'%');
        }
        if(!empty($params['created_at_from'])){
            $select->where('DATE(p.created_at) >= ?', $params['created_at_from']);
        }
        if(!empty($params['created_at_to'])){
            $select->where('DATE(p.created_at) <= ?', $params['created_at_to']);
        }
        if(!empty($params['list_staff'])){
            $select->where('p.created_by IN (?)', $params['list_staff']);
        }        
        
        $select->order('p.created_at DESC');
        $result = $db->fetchAll($select);
        return $result;
    }
    
    
    
}