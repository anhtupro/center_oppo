<?php
class Application_Model_TradeContract extends Zend_Db_Table_Abstract
{
    protected $_name = 'trade_contract';
    protected $_schema = DATABASE_TRADE;

    public function getCodeFromCategoryId($category_id, $type) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.code_purchasing_id",
            "p.category_id",
            "p.type",
            "code.code",
            "code_id" => "code.id"
        );

        $select->from(array('p' => 'pmodel_code_category'), $arrCols);
        $select->joinLeft(array('code' => 'pmodel_code'), 'code.id = p.code_purchasing_id', array());

        $select->where('p.category_id = ?', $category_id);
        $select->where('p.type = ?', $type);
        $result = $db->fetchRow($select);

        return $result;
    }
    
    public function getCatName($category_id) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.name",
        );

        $select->from(array('p' => DATABASE_TRADE.'.category'), $arrCols);

        $select->where('p.id = ?', $category_id);
        $result = $db->fetchRow($select);

        return $result;
    }
    
    public function getContructorsId($contructors_id) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.name",
        );

        $select->from(array('p' => DATABASE_TRADE.'.contructors'), $arrCols);

        $select->where('p.id = ?', $contructors_id);
        $result = $db->fetchRow($select);

        return $result;
    }
    
    public function getSupplierFromContructors($contructors_id) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.supplier_id",
            "p.contructors_id",
        );

        $select->from(array('p' => 'supplier_contructors'), $arrCols);

        $select->where('p.contructors_id = ?', $contructors_id);
        $result = $db->fetchRow($select);

        return $result;
    }
    
}