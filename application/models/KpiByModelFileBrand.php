<?php

class Application_Model_KpiByModelFileBrand extends Zend_Db_Table_Abstract
{
    protected $_name = 'kpi_by_model_file_brand';

    public function updateBasic($id) {
        $db = Zend_Registry::get('db');
        $sql = "
			UPDATE kpi_by_model_file_brand a
                   
			INNER JOIN " . WAREHOUSE_DB . ".distributor d ON d.parent = 2316 AND d.partner_id = a.partner_id 
			INNER JOIN store s ON d.id = s.d_id AND (s.del <> 1 OR s.del IS NULL OR s.del = '')
			INNER JOIN regional_market rm ON rm.id = s.regional_market
			INNER JOIN `area` re ON re.id = rm.area_id
			SET a.`store_id` = s.id, a.`area_id` = re.id, a.`province_id` = rm.id, a.`district_id` = s.district, a.distributor_id = d.`id`
			 WHERE a.`random_do_so` = '$id'
		";
        $db->query($sql);
        $db = null;
    }

    public function updateGoodId($id) {
        $db = Zend_Registry::get('db');
        $sql = "
			UPDATE kpi_by_model_file_brand a
                        INNER JOIN (
				SELECT k.id FROM kpi_by_model_file_brand k WHERE k.`random_do_so` = '$id'
			) b ON b.id = a.id
			INNER JOIN " . WAREHOUSE_DB . ".good g ON g.`name` = TRIM(a.model)
			SET a.good_id = g.id
		";
        $db->query($sql);
        $db = null;
    }

    public function checkColor($id){
        $db = Zend_Registry::get('db');
        $sql = "SELECT g.`desc`
				FROM kpi_by_model_file_brand kb
				LEFT JOIN " . WAREHOUSE_DB . ".`good_color` gc ON gc.`name` = kb.color_name COLLATE utf8_unicode_ci
				LEFT JOIN " . WAREHOUSE_DB . ".`good` g ON g.`id` = kb.`good_id`
				LEFT JOIN " . WAREHOUSE_DB . ".`good_mapping` gm ON gm.`color_id` = gc.`id` and gm.good_id = kb.good_id
				WHERE gm.`id` IS NULL AND kb.`random_do_so` = '$id'
				GROUP BY kb.`good_id`
			";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $checkColor = $stmt->fetchAll();
        $stmt->closeCursor();
        $db = $stmt = null;
        return $checkColor;
    }

    public function updateColorId($id) {
        $db = Zend_Registry::get('db');
        $sql = "
			UPDATE
             kpi_by_model_file_brand a 
            INNER JOIN warehouse.good_color gc on gc.`name` = a.color_name 
             INNER JOIN warehouse.good_mapping gm on gm.`color_id` = gc.`id` and gm.good_id = a.good_id
            SET a.color_id = gm.`color_id`
            WHERE a.`random_do_so` = '$id'
		";
        $db->query($sql);
        $db = null;
    }

    public function deleteModelFile($id) {
        $db = Zend_Registry::get('db');
        $sql = "
			DELETE FROM kpi_by_model_file_brand WHERE random_do_so = '$id'
		";
        $db->query($sql);
        $db = null;
    }

    public function insertData($id){
        $db = Zend_Registry::get('db');
        $stmt = $db->prepare("
			INSERT INTO `kpi_by_model_brand`( `timing_date`, `distributor_id`, `store_id`, `qty`, `area_id`, `province_id`, `district_id`, `good_id`, `color_id`, `pg_allow`, 
                        `pg_string`, `pg_total`, `pg_export`, `partner_id`, `status`, `status_locked`)
			SELECT  i.`timing_date`, i.`distributor_id`, i.`store_id`, i.`qty`, i.`area_id`, i.`province_id`, i.`district_id`, i.`good_id`, i.`color_id`
				, IFNULL(CONCAT(',', GROUP_CONCAT(stl.`staff_id`), ','), 0) pg_allow
				, IFNULL(GROUP_CONCAT(CONCAT('Name: ', CONCAT(st.firstname, ' ', st.lastname), CONCAT('<br>Email: ', SUBSTRING_INDEX(IFNULL(st.email, 'noemail@'), '@', 1), CONCAT('<br>Code: ', st.`code`))), '<hr>'), 'No PG') pg_string	
				, COUNT(stl.staff_id) pg_total
				, IFNULL(GROUP_CONCAT(CONCAT(st.firstname, ' ', st.lastname, ' ', st.`code`)), 'No PG') pg_export
				, i.`partner_id`, i.`status`, i.`status_locked`
			FROM kpi_by_model_file_brand i
			LEFT JOIN store s ON s.`id` = i.`store_id`
			LEFT JOIN store_staff_log stl ON stl.`store_id` = s.`id` AND DATE(i.`timing_date`) >= FROM_UNIXTIME(stl.joined_at,'%Y-%m-%d') AND (stl.released_at IS NULL OR DATE(i.`timing_date`) < FROM_UNIXTIME(stl.released_at,'%Y-%m-%d'))
AND stl.`is_leader` = 0
			LEFT JOIN staff st ON st.id = stl.staff_id
			WHERE i.random_do_so = '$id'
                        GROUP BY i.`id`
        ");

        $res = $stmt->execute();
        $stmt->closeCursor();
        $stmt = null;
        return $res;
    }

    public function getWrongProduct($random_do_so, $brand_id)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['k' => 'kpi_by_model_file_brand'], [
                'model' => "CONCAT(g.desc, '/', g.name)"
            ])
            ->joinLeft(['g' => WAREHOUSE_DB . '.good'], 'k.good_id = g.id', [])
            ->where('k.random_do_so = ?', $random_do_so)
            ->where('g.brand_id <> ?', $brand_id)
        ->group('k.good_id');

        $result = $db->fetchCol($select);

        return $result;
    }

    public function updateValue($params)
    {
        $db = Zend_Registry::get('db');
        $sql = "
			UPDATE kpi_by_model_brand km 
            LEFT JOIN warehouse.good_price_log cm 
                ON km.good_id = cm.good_id 
                AND km.color_id = cm.color_id
                AND km.timing_date >= cm.from_date 
                AND (cm.to_date IS NULL OR km.timing_date <= cm.to_date)
                
            LEFT JOIN warehouse.good_price_log kcm 
                ON km.good_id = kcm.good_id 
                AND kcm.color_id = 0
                AND km.timing_date >= kcm.from_date 
                AND (kcm.to_date IS NULL OR km.timing_date <= kcm.to_date)
                
            SET km.value = IF(cm.price, cm.price, IF(kcm.price, kcm.price, 0))
                
            WHERE (km.timing_date BETWEEN :from_date AND :to_date)
			";

        $stmt = $db->prepare($sql);
        $stmt->bindParam('from_date', $params['from_date']);
        $stmt->bindParam('to_date', $params['to_date']);
        $stmt->execute();
        $stmt->closeCursor();
        $db = $stmt = null;
    }
}