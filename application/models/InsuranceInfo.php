<?php
/**
 * Created by PhpStorm.
 * User: hac
 * Date: 10/23/2015
 * Time: 10:22 AM
 */
class Application_Model_InsuranceInfo extends Zend_Db_Table_Abstract
{
    protected $_name = 'insurance_info';

    public function add($staff_id){

        $db = Zend_Registry::get('db');
        $select = $db->select()
                ->from(array('a'=>$this->_name),array('a.*'))
                ->where('a.staff_id = ?',$staff_id);
        $row = $db->fetchRow($select);

        if(!$row){
            $QStaff = new Application_Model_Staff();
            $staff = $QStaff->find($staff_id)->current();
            $data = array(
                'staff_id'  => $staff_id,
                'ID_number' => $staff['ID_number'],
                'dob'       => $staff['dob'],
                'firstname' => $staff['firstname'],
                'lastname'  => $staff['lastname'],
                'ID_date'   => $staff['ID_date']
            );
            $this->insert($data);
        }
    }
}