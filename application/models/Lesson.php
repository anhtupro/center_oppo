<?php
class Application_Model_Lesson extends Zend_Db_Table_Abstract
{
	protected $_name = 'lesson';

    function fetchPagination($page, $limit, &$total, $params) {
        $db = Zend_Registry::get('db');
  if (isset($params['group_cat']) and $params['group_cat']){
            $select = $db->select();
            $select_fields = array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'), 'p.*','title_cat'=>'o.title' );

            if (isset($params['get_fields']) and is_array($params['get_fields']))
                foreach ($params['get_fields'] as $get_field)
                    array_push($select_fields, $get_field);
            else
                array_push($select_fields, 'p.*');

            $select
                ->from(array('p' => $this->_name),
                    $select_fields
                )
                ->joinLeft(array('o' => 'lesson_cat'), 'o.id = p.cat_id', array())
                ->group('p.categories_id');

        } 
        else
        {
             $select = $db->select()
            ->from(array('p' => $this->_name),
                array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'), 'p.*', 'title_cat'=>'o.title'))
            ->joinLeft(array('o' => 'lesson_cat'), 'o.id = p.cat_id', array());
             $select->group('p.id');
        }

        if(isset($params['sort']) && $params['sort']) {
            $desc = (isset($params['desc']) and $params['desc'] == 1) ? ' DESC ' : ' ASC ';
            $collate = ' COLLATE utf8_unicode_ci ';
            $order_str = 'p.`'.$params['sort']. '` ' . $collate . $desc;

            $select->order(new Zend_Db_Expr($order_str));
        } else {
        	$select->order('created_at DESC');
        }
        
        if(isset($params['title']) and $params['title'])
            $select->where('p.title LIKE ?','%'.$params['title'].'%');
        
        if ($limit)
            $select->limitPage($page, $limit);
        $select->where('p.id IN (?)',$params['priviledge_arr']);
        //$select->where('p.id_parent = 0 OR p.id_parent IS NULL');
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
       
        return $result;

    }
    
    
    function getRegionalMarket($id){
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('a' => 'asm'),
                    array('b.id','b.name')
                )
                ->where('a.staff_id = ?', $id)
                ->joinLeft(array('b' => 'regional_market'), 'a.area_id = b.area_id', array());
        $result = $db->fetchAll($select);
        return $result;
    }
    
    function getNewLesson(){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.title",
            "title_cat" => "c.title",
            "p.file_photo"
        );

        $select->from(array('p' => 'lesson'), $arrCols);
        $select->joinLeft(array('c' => 'lesson_cat'), 'c.id = p.cat_id', array());
        $select->joinLeft(array('l' => 'lesson_priviledge'), 'l.lesson_id = p.id', array());

        if (!empty($params['type_priviledge'])) {
            $select->where('l.type IN (?)', $params['type_priviledge']);
        }

        $select->group('p.id');
        $select->order('p.created_at DESC');
        $select->limitPage(0,4);

        $result = $db->fetchAll($select);

        return $result;
    }
    
    function getHotLesson($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id",
            "p.title",
            "title_cat" => "c.title",
            "p.file_photo"
        );

        $select->from(array('p' => 'lesson'), $arrCols);
        $select->joinLeft(array('c' => 'lesson_cat'), 'c.id = p.cat_id', array());
        $select->joinLeft(array('l' => 'lesson_priviledge'), 'l.lesson_id = p.id', array());
        
        $select->where('p.hot = 1');
        
        if (!empty($params['type_priviledge'])) {
            $select->where('l.type IN (?)', $params['type_priviledge']);
        }

        $select->group('p.id');
        $select->order('p.created_at DESC');
        $select->limitPage(0,8);

        $result = $db->fetchAll($select);

        return $result;
    }
    
     function getTongHopLession()
    {
       $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('a' => 'lesson'),
                    array('a.id','a.title','a.file_photo')
                )
                ->order('a.created_at DESC')
                ->where('a.total_test = ?', 1);
        $result = $db->fetchAll($select);
        //echo $select; exit;
        return $result; 
    }
    function getInforDisplay($id)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('a' => 'lesson'),
                    array('total'=>'a.total_test')
                )
                ->where('a.id = ?', $id);
        $result = $db->fetchAll($select);
        return $result; 
    }
    function getQuestion($id)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('q' => 'lesson_question'),
                    array('question_id'=>'q.id','answer_id'=>'a.id','content'=>'q.content','title'=>'a.title','result'=>'a.true')
                )
               ->joinLeft(array('a' => 'lesson_answer'), 'a.question_id = q.id', array())
                ->where('q.lesson_id = ?', $id);
                $select->where('q.type = ?',0);
        $result = $db->fetchAll($select);
        //echo $select; exit;
        return $result; 
    }
    function getQuestionOnline($id)
    {

        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('l' => 'lesson'),
                    array('id'=>'q.id','title'=>'l.title','content'=>'q.content','scores'=>'q.scores','lesson_id'=>'q.lesson_id','type'=>'q.type','published'=>'q.published','type_lesson'=>'l.type_test')
                )
               ->joinLeft(array('q' => 'lesson_question'), 'q.lesson_id = l.id', array())
                ->where('l.id_parent = ?', $id);

                $select->where('l.type_test = ?',1);
                $select->where('q.type = ?',0);
        $result = $db->fetchAll($select);
        //echo $select; exit;
        return $result; 
    }
    function getQuestionOffline($id)
    {

        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('l' => 'lesson'),
                    array('id'=>'q.id','title'=>'l.title','content'=>'q.content','scores'=>'q.scores','lesson_id'=>'q.lesson_id','type'=>'q.type','published'=>'q.published','type_lesson'=>'l.type_test')
                )
               ->joinLeft(array('q' => 'lesson_question'), 'q.lesson_id = l.id', array())
                ->where('l.id_parent = ?', $id);

                $select->where('l.type_test = ?',2);
                $select->where('q.type = ?',0);
        $result = $db->fetchAll($select);
        //echo $select; exit;
        return $result; 
    }
    function getQuestionTest($id)
    {

        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('l' => 'lesson'),
                    array('id'=>'q.id','title'=>'l.title','content'=>'q.content','scores'=>'q.scores','lesson_id'=>'q.lesson_id','type'=>'q.type','published'=>'q.published','type_lesson'=>'l.type_test')
                )
               ->joinLeft(array('q' => 'lesson_question'), 'q.lesson_id = l.id', array())
                ->where('l.id_parent = ?', $id);

                $select->where('l.type_test = ?',3);
                $select->where('q.type = ?',0);
        $result = $db->fetchAll($select);
        //echo $select; exit;
        return $result; 
    }
    function getQuestionOnline_tuluan($id)
    {

        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('l' => 'lesson'),
                    array('id'=>'q.id','title'=>'l.title','content'=>'q.content','scores'=>'q.scores','lesson_id'=>'q.lesson_id','type'=>'q.type','published'=>'q.published','type_lesson'=>'l.type_test')
                )
               ->joinLeft(array('q' => 'lesson_question'), 'q.lesson_id = l.id', array())
                ->where('l.id_parent = ?', $id);

                $select->where('l.type_test = ?',1);
                $select->where('q.type = ?',1);
        $result = $db->fetchAll($select);
        //echo $select; exit;
        return $result; 
    }
    function getQuestionOffline_tuluan($id)
    {

        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('l' => 'lesson'),
                    array('id'=>'q.id','title'=>'l.title','content'=>'q.content','scores'=>'q.scores','lesson_id'=>'q.lesson_id','type'=>'q.type','published'=>'q.published','type_lesson'=>'l.type_test')
                )
               ->joinLeft(array('q' => 'lesson_question'), 'q.lesson_id = l.id', array())
                ->where('l.id_parent = ?', $id);

                $select->where('l.type_test = ?',2);
                $select->where('q.type = ?',1);
        $result = $db->fetchAll($select);
        //echo $select; exit;
        return $result; 
    }
    function getQuestionTest_tuluan($id)
    {

        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('l' => 'lesson'),
                    array('id'=>'q.id','title'=>'l.title','content'=>'q.content','scores'=>'q.scores','lesson_id'=>'q.lesson_id','type'=>'q.type','published'=>'q.published','type_lesson'=>'l.type_test')
                )
               ->joinLeft(array('q' => 'lesson_question'), 'q.lesson_id = l.id', array())
                ->where('l.id_parent = ?', $id);

                $select->where('l.type_test = ?',3);
                $select->where('q.type = ?',1);
        $result = $db->fetchAll($select);
        //echo $select; exit;
        return $result; 
    }
    function getLessonPriviledge($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('l' => 'lesson'),
                    array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS l.id'),'title'=>'l.title'))
                ->joinLeft(array('p' => 'lesson_priviledge'), 'p.lesson_id = l.id', array('type' => 'GROUP_CONCAT(p.type)'));
                $select->GROUP('l.id');
        $select->ORDER('l.id DESC');
        $result  = $db->fetchAll($select);
        //echo $select; exit;
        return $result; 
    }
    public  function getListLessonStafNew($params)
    {

         $db     = Zend_Registry::get("db");
        $select = $db->select();
            
        $arrCols = array(
            'stt'       => 'l.stt_staff_new',
            'lesson_id'  => 'l.id',
            'scores' => 's.scores'
        );
        $select->from(array('l'=> 'lesson'), $arrCols);
        $select->joinLeft(array('s' => 'lesson_scores_new'), 's.lesson_id = l.id AND s.scores = 10 AND s.ID_number = '.$params['id_number'].' ', array());
        $select ->where('l.stt_staff_new IS NOT NULL');
        // if($params['type'])
        // {
        //     $select ->where('l.type_staff = ?', $params['type']);
        // }
        // $select->where('l.id IN (?)',$params['priviledge_arr']);
        $select->GROUP('l.stt_staff_new');

        $result  = $db->fetchAll($select);
        //echo $select; exit;
        return $result;
    }
    public function getQuestionStaffNew($params)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('l' => 'lesson'),
                    array('stt'=>'l.stt_staff_new','id_question'=>'q.id','title'=>'l.title','content'=>'q.content','scores'=>'q.scores','lesson_id'=>'q.lesson_id','type'=>'q.type','published'=>'q.published','type_lesson'=>'l.type_test')
                )
               ->joinLeft(array('q' => 'lesson_question'), 'q.lesson_id = l.id', array())
                ->where('l.id = ?', $params['id_lesson']);
               $select->where('q.type = ?',0);
               $select ->where('l.type_staff = ?', $params['type']);
        $result = $db->fetchAll($select);
        
        if($_GET['dev'] == 1){
            echo $select;exit;
        }
        
        return $result;  
    }
    public function getWriteLessonStaffNew($params)
    {
         $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('l' => 'lesson'),
                    array('stt'=>'l.stt_staff_new','id_question'=>'q.id','title'=>'l.title','content'=>'q.content','scores'=>'q.scores','lesson_id'=>'q.lesson_id','type'=>'q.type','published'=>'q.published','type_lesson'=>'l.type_test')
                )
            ->joinLeft(array('q' => 'lesson_question'), 'q.lesson_id = l.id', array())
            ->where('l.id = ?', $params['id_lesson']);
            $select ->where('l.type_staff = ?', $params['type']);
        $select->where('q.type = ?',1);
        $result = $db->fetchAll($select);
        return $result; 
    }

    public  function getIdLesson($params)
    {
        
        $db     = Zend_Registry::get("db");
        $select = $db->select();
            
        $arrCols = array(
            'id'    => 'p.id',
            'stt'   =>"MIN(p.stt_staff_new)"
        );
        $select->from(array('p'=> 'lesson'), $arrCols);
        $select ->where('p.stt_staff_new IS NOT NULL', NULL);
        $select ->where("p.id NOT IN 
                        (
                            SELECT lesson_id
                            FROM lesson_scores_new 
                            WHERE scores = 10 AND ID_number = ?
                        )", $params['id_number']);

        $result = $db->fetchAll($select);
        
        if($_GET['dev'] == 1){
            echo $select;exit;
        }
        
        return $result;
    }
    public function getQuestionStaffNewAuto($params)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('l' => 'lesson'),
                    array('stt'=>'l.stt_staff_new','id_question'=>'q.id','title'=>'l.title','content'=>'q.content','scores'=>'q.scores','lesson_id'=>'q.lesson_id','type'=>'q.type','published'=>'q.published','type_lesson'=>'l.type_test')
                )
               ->joinLeft(array('q' => 'lesson_question'), 'q.lesson_id = l.id', array())
                ->where('l.stt_staff_new = ?', $params['stt']);
                $select->where('q.type = ?',0);
                $select->where('l.type_staff = ?',$params['type']);
        $result = $db->fetchAll($select);
        if($_GET['dev'] == 2){

            echo $select; exit;
        }
        //echo $select; exit;
        return $result;  
    }
     public function getWriteLessonStaffNewAuto($params)
    {
         $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('l' => 'lesson'),
                    array('stt'=>'l.stt_staff_new','id_question'=>'q.id','title'=>'l.title','content'=>'q.content','scores'=>'q.scores','lesson_id'=>'q.lesson_id','type'=>'q.type','published'=>'q.published','type_lesson'=>'l.type_test')
                )
            ->joinLeft(array('q' => 'lesson_question'), 'q.lesson_id = l.id', array())
            ->where('l.stt_staff_new = ?', $params['stt']+1);

        $select->where('q.type = ?',1);
         $select->where('l.type_staff = ?',$params['type']);
        $result = $db->fetchAll($select);
        return $result; 
    }

    public function getCountLesson($params)
    {

         $db     = Zend_Registry::get("db");
        $select = $db->select();
         $select_main = $db->select();   
        $arrCols = array(
            'stt'       => 'l.stt_staff_new',
            'scores'   =>'s.scores'
        );
        $select->from(array('l'=> 'lesson'), $arrCols);
        $select->joinLeft(array('s' => 'lesson_scores_new'), 's.lesson_id = l.id and s.scores = 10 and s.ID_number="'.$params['ID_number'].'"', array());
        $select ->where('l.type_staff = ?',$params['type']);
        $select ->where('l.stt_staff_new IS NOT NULL');
        $select->group('l.stt_staff_new');


         $arrCols_main = array(
                     'count'    =>"COUNT(f.scores)"
                );
        $select_main->from(array('f'=> new Zend_Db_Expr('(' . $select . ')')), $arrCols_main);
        $select_main->where('f.scores = ?',10);
        $result  = $db->fetchAll($select_main);
        return $result;
    }

    public function getLessonArr($id,$priviledge_arr, $list_channel){
        $db     = Zend_Registry::get("db");
        $select = $db->select();
         $select_main = $db->select();   
        $arrCols = array(
            'id'               => 'l.id',
            'title'         =>'l.title',
            'cat_id'         =>'l.cat_id',
            'new'               =>'l.new'
        );
        $select->from(array('l'=> 'lesson'), $arrCols);
        $select ->where('l.cat_id = ?',$id);
        $select ->where('l.del = ?', 0);
        if($userStorage->team != TRAINING_TEAM){
            $select->where('l.id IN (?)',$priviledge_arr);
        }
        
        if(!empty($list_channel)){
            $select->where('l.channel_id IN (?) OR l.channel_id IS NULL',$list_channel);
        }
        //         if($_GET['dev']){
        //     echo $select->__toString();
        //     exit;
        // }
        
        $result  = $db->fetchAll($select);
        return $result;
    }
    public function getLesson_Array($id,$priviledge_arr, $list_channel){
        $db     = Zend_Registry::get("db");
        $select = $db->select();
         $select_main = $db->select();   
        $arrCols = array(
            'id'               => 'l.id',
            'title'         =>'l.title',
            'cat_id'         =>'l.cat_id'
        );
        $select->from(array('l'=> 'lesson'), $arrCols);

        // Toan 16042020
        $select->joinLeft(array('lbc'=>'lesson_by_channel'),'lbc.lesson_id=l.id',array());

        $select ->where('l.cat_id = ?',$id);
        $select ->where('l.del = ?', 0);
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        
        if($userStorage->team != TRAINING_TEAM){
            if($userStorage->title != 537){
                $select->where('l.id IN (?)',$priviledge_arr);
            }
        }
        
        if(!empty($list_channel)){
            $select->where('l.channel_id IN (?) OR l.channel_id IS NULL',$list_channel);
            //$select->where('lbc.channel_id IN (?) ',$list_channel);

        }
        $select->group('l.id');
        

        
        $result  = $db->fetchAll($select);
        return $result;
    }
    
    public function reportLessonSales($params){
        $db     = Zend_Registry::get("db");
        $select = $db->select();
        $arrCols = array(
            'p.id',
            'p.title'
        );
        $select->from(array('p'=> 'lesson'), $arrCols);
        $select->joinLeft(array('l' => 'lesson_priviledge'), 'l.lesson_id = p.id', array());
        $select->where('l.type = 4');
        $select->where('p.id <> 366');
        $select->order("p.created_at DESC");
        $select->limit('5');
        $result  = $db->fetchAll($select);
        return $result;
    }
    
    public function getTotalSaleSaleLeader($params){
        $db     = Zend_Registry::get("db");
        $select = $db->select();
         $select_main = $db->select();   
        $arrCols = array(
            'total_sale' => 'COUNT(DISTINCT p.id)',
            'r.area_id'
        );
        $select->from(array('p'=> 'staff'), $arrCols);
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = p.regional_market', array());
        $select->where('p.off_date IS NULL');
        $select->where('p.status = 1');
        $select->where('p.title IN (?)', [SALES_TITLE, LEADER_TITLE]);
        
        if(isset($params['area_list']) and $params['area_list']){
            $select->where('r.area_id IN (?)', $params['area_list']);
        }

        $result  = $db->fetchRow($select);
        return $result;
    }
    
    public function getPassLesson($params){
        $db     = Zend_Registry::get("db");
        $select = $db->select();
         $select_main = $db->select();   
        $arrCols = array(
            'p.lesson_id',
            'pass' => 'COUNT(DISTINCT p.staff_cmnd)'
        );
        $select->from(array('p'=> 'lesson_scores'), $arrCols);
        $select->joinLeft(array('s' => 'staff'), 's.ID_number = p.staff_cmnd', array());
        $select->joinLeft(array('r' => 'regional_market'), 'r.id = s.regional_market', array());
        $select->joinLeft(array('pri' => 'lesson_priviledge'), 'pri.lesson_id = p.lesson_id', array());
        $select->where('s.off_date IS NULL');
        $select->where('s.status = 1', NULL);
        $select->where('s.title IN (?)', [SALES_TITLE, LEADER_TITLE]);
        $select->where('p.scores = 10 AND pri.type = 4');
        
        if(isset($params['area_list']) and $params['area_list']){
            $select->where('r.area_id IN (?)', $params['area_list']);
        }
        
        $select->group('p.lesson_id');
        
        $result  = $db->fetchAll($select);
        
        $data = [];
        foreach($result as $key=>$value){
            $data[$value['lesson_id']] = $value['pass'];
        }
        
        return $data;
    }
    
    public function getListChannel(){
        $db     = Zend_Registry::get("db");
        $select = $db->select();
         $select_main = $db->select();   
        $arrCols = array(
            'p.id',
            'p.name',
            'p.desc',
            'p.status',
            'p.parent',
        );
        $select->from(array('p'=> 'channel'), $arrCols);
        
        
        $result  = $db->fetchAll($select);
        
        return $result;
    }
    
    public function getLessonLevel1($params){
        $db     = Zend_Registry::get("db");
        $select = $db->select();
         $select_main = $db->select();   
        $arrCols = array(
            'id'            => 'l.id',
            'title'         =>'l.title',
            'cat_id'        =>'l.cat_id',
            'new'           =>'l.new'
        );
        $select->from(array('l'=> 'lesson'), $arrCols);
        $select->joinLeft(array('c' => 'lesson_cat'), 'c.id = l.cat_id', array());
        $select ->where('c.parent_id = ?',0);
        
        if(!empty($params['priviledge_arr'])){
            if($params['team'] != TRAINING_TEAM){
                $select->where('l.id IN (?)', $params['priviledge_arr']);
            }
            else{
                $select->where('l.id IN (?)', $params['priviledge_arr']);
            }
        }
        
        $result  = $db->fetchAll($select);
        
        $data = [];
        foreach($result as $lesson){
            $data[$lesson['cat_id']][] = $lesson;
        }
        
        return $data;
    }
    public function getStaffDoLessonOverTimeActive($lessonId){
        $list_sale_psg = unserialize(LIST_PGS_BI);
         $db     = Zend_Registry::get("db");

        $select = $db->select();
        $nestedSelect = $db->select();


        $arrCols = array(
            'id'            => 'l.id',
        );
        $nestedSelect->from(array('l'=> 'lesson'), $arrCols);
        $nestedSelect->joinLeft(array('lc' => 'lesson_scores'), 'lc.lesson_id = l.id', array());
        $nestedSelect->joinLeft(array('s'=> 'staff'),'s.ID_number=lc.staff_cmnd',array());
        $nestedSelect->where(new Zend_Db_Expr('lc.created_at > (SELECT l.to_test FROM lesson l WHERE l.id='.$lessonId.' )')) ;   
        $nestedSelect->where('l.id =? ',$lessonId );
        $nestedSelect->where('s.title IN (?) ',$list_sale_psg);
        $nestedSelect->where('s.status = 1 ');
        $nestedSelect->where('s.off_date is null');
        $nestedSelect->group('lc.staff_cmnd');

        $select->from(array(),array());
        $select->joinLeft(array('child' => new Zend_Db_Expr('('.$nestedSelect.')')),'',array('total' => new Zend_Db_Expr('COUNT(child.id)')));

        // echo $select->__toString();exit;
        $result  = $db->fetchRow($select);
        
        return $result['total'];
    }
    
}
