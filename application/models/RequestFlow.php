<?php
class Application_Model_RequestFlow extends Zend_Db_Table_Abstract
{
    protected $_name = 'request_flow';
    
    public function fetchPagination($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "department_team_id" => "IFNULL(p.department_id, p.team_id)",
            "p.id", 
            "p.type", 
            "p.department_id", 
            "p.team_id", 
            "p.from_price", 
            "p.to_price", 
            "p.has_leader",
            "department_name" => "t.name",
            "team_name" => "t2.name",
            "request_type_group" => "p.request_type_group",
            "request_type" => "type.title",
            "group_name" => "r.title",
            "request_type_name" => "type.title",
            "company_name" => "c.name",
            "list_staff_confirm" => "GROUP_CONCAT(CONCAT(s.firstname, ' ', s.lastname) ORDER BY d.step ASC)"
        );

        $select->from(array('p' => 'request_flow'), $arrCols);
        $select->joinLeft(array('c' => 'company'), 'c.id = p.company_id', array());
        $select->joinLeft(array('t' => 'team'), 't.id = p.department_id', array());
        $select->joinLeft(array('t2' => 'team'), 't2.id = p.team_id', array());
        $select->joinLeft(array('r' => 'request_type_group'), 'r.id = p.request_type_group', array());
        $select->joinLeft(array('type' => 'request_type'), 'type.id = p.request_type', array());
        $select->joinLeft(array('d' => 'request_flow_details'), 'd.flow_id = p.id AND d.is_del = 0', array());
        $select->joinLeft(array('s' => 'staff'), 's.id = d.staff_id', array());

        $select->where('p.is_del = 0');

        $select->group("p.id");
        $select->order(array("p.department_id", "p.team_id", "p.request_type_group"));
        
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
    
    public function fetchFlow($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "department_team_id" => "IFNULL(p.department_id, p.team_id)",
            "p.id", 
            "p.type", 
            "p.department_id", 
            "p.team_id", 
            "p.from_price", 
            "p.to_price", 
            "list_staff_confirm" => "GROUP_CONCAT(CONCAT(s.firstname, ' ', s.lastname) ORDER BY d.step ASC)",
        );

        $select->from(array('p' => 'request_flow'), $arrCols);
        $select->joinLeft(array('d' => 'request_flow_details'), 'd.flow_id = p.id AND d.is_del = 0', array());
        $select->joinLeft(array('s' => 'staff'), 's.id = d.staff_id', array());

        $select->where('p.is_del = 0');
        $select->group('p.id');
        $result = $db->fetchAll($select);

        return $result;
    }

}