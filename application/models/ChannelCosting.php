<?php
class Application_Model_ChannelCosting extends Zend_Db_Table_Abstract
{
    protected $_name = 'channel_costing';
   // protected $_schema = DATABASE_COST;


     public function getChannel()
	    {
	    	$db     = Zend_Registry::get("db");
	        $select = $db->select();

	        $arrCols = array(
	            'p.id', 
	            'name'        => 'p.name' 
	        );
	        $select->from(array('p'=> 'channel_costing'), $arrCols);
	        $result = $db->fetchAll($select);
	        return $result;
	    }
}
