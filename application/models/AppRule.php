<?php
class Application_Model_AppRule extends Zend_Db_Table_Abstract
{
    protected $_name = 'app_rule';
	protected $_schema = DATABASE_TRADE;
    
    public function getRule($params){

    	$db     = Zend_Registry::get("db");
    	$select = $db->select();
    	    
    	$arrCols = array(
    	    'p.id', 
    	    'p.category_id', 
    	    'p.so_from', 
    	    'p.so_to', 
    	    'p.value'
    	);
    	
    	$select->from(array('p'=> DATABASE_TRADE.'.app_rule'), $arrCols);
    	$select->where('p.from_date <= NOW()', NULL);
    	$select->where('p.to_date >= NOW() OR p.to_date IS NULL', NULL);
    	$select->where('p.so_from <= ?', $params['so']);
    	$select->where('p.so_to >= ?', $params['so']);
    	
    	$result  = $db->fetchAll($select);
    	
    	return $result;
    }
}