<?php

class Application_Model_AdditionalAirCost extends Zend_Db_Table_Abstract
{
    protected $_name = 'additional_air_cost';
    protected $_schema = DATABASE_TRADE;

    public function getCost($params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
                     ->from(['a' => DATABASE_TRADE.'.additional_air_cost'], [
                         'a.*',
                         'area_name' => 'area.name'
                     ])
        ->joinLeft(['area' => 'area'], 'a.area_id = area.id AND area.del = 0', [])
        ->order('a.id');

        if ($params['season']) {
            $select->where('a.season = ?', $params['season']);
        }

        if ($params['year']) {
            $select->where('a.year = ?', $params['year']);
        }

        $result = $db->fetchAll($select);

        foreach ($result as $element) {
            $list [$element['area_id']] = $element;
        }

        return $list;
    }
}    