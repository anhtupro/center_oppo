<?php

class Application_Model_StaffAddress extends Zend_Db_Table_Abstract {

    protected $_name = 'staff_address';

    public function getStaffAddress($staff_id,$flag) {
        $db            = Zend_Registry::get('db');
        $select        = $db->select();
        $select->from(['sa' => $this->_name] , '*');
        $select->join(['w' => 'ward'] , 'w.id = sa.ward_id',['name_ward'=> 'w.name' , 'w.code_insurance', 'is_hidden' => 'w.is_hidden']);
        $select->where('sa.staff_id = ?' , $staff_id);
        $select->where('sa.address_type = ?' , $flag);
        $result = $db->fetchRow($select);
        return $result;
    }

}
