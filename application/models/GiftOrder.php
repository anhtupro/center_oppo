<?php
class Application_Model_GiftOrder extends Zend_Db_Table_Abstract
{
    protected $_name = 'gift_order';

    public function fetchPagination($page, $limit, &$total, $params){
        $db = Zend_Registry::get('db');
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();

        $cols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS p.id'),
            'f.status', 'm.name', 'm.from_date', 'm.to_date', 'm.desc', 'cat_id' => 'm.id',
            'f.step', 'f.has_rejected', 'area_name' => 'a.name', 'p.created_at',
            "created_by" => "CONCAT(st.firstname, ' ',st.lastname)",
        );

        $select = $db->select()
            ->distinct()
            ->from(array('p' => $this->_name), $cols);
        $select->joinLeft(array('m' => 'gift_main'), 'm.id = p.main_id', array());
        $select->joinLeft(array('f' => 'gift_order_flow'), 'p.id = f.order_id', array());
        $select->joinLeft(array('a' => 'area'), 'p.area_id = a.id',array());
        $select->joinLeft(array('st' => 'staff'), 'st.id = p.created_by', array());
        $select->where('m.del = ?', 0);
        $select->where('p.del = ?', 0);

        if($params['view_all']){

            $select->where('f.has_approved = ?', 0);
            $select->where('f.has_rejected = ?', 0);
            $select->where('f.step != ?', 0);

        }elseif(!empty($params['cat'])){
            // params [cat] từ list của super sales admin đẩy qua
            $select->where('p.main_id = ?', $params['cat']);
            $select->where('f.step IN (?)', [1,3]);
            $select->where('f.has_approved = ?', 0);
            $select->where('f.has_rejected = ?', 0);
            // flow type == 1, đơn approve
        }elseif($params['asm']){

            $select->where('m.from_date < ?', date('Y-m-d H:i:s'));
            $select->where('m.to_date > ?', date('Y-m-d H:i:s'));
            $select->where('f.step = ?', 0);
            $select->where('f.has_approved = ?', 0);
            $select->where('f.has_rejected = ?', 0);
            $select->where('p.area_id IN (?)', $params['area_id']);

        }elseif (in_array($userStorage->group, [SALES_EXT_ID]) || in_array($userStorage->title, [SALES_ASSISTANT_TITLE]) || in_array($userStorage->id, array(SUPERADMIN_ID, 3026, 23154))){

            $select->where('f.has_approved = ?', 0);
            $select->where('f.has_rejected = ?', 0);
            $select->where('f.step IN (?)', [1,3]);

        }else{
            if($params['staff_id']){
                $select->where('p.created_by = ?', $params['staff_id']);
            }else{
                $select->where('m.from_date < ?', date('Y-m-d H:i:s'));
                $select->where('m.to_date > ?', date('Y-m-d H:i:s'));
            }
            $select->where('f.has_approved = ?', 0);
            $select->where('p.area_id IN (?)', $params['area_id']);
        }

        if(!empty($_GET['dev'])){
            echo $select->__toString();
            exit;
        }

        if(empty($params['export'])){
            $select->limitPage($page, $limit);
        }

        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");
        return $result;
    }

    public function getInfo($id){
        $db     = Zend_Registry::get('db');
        $cols   = array(
            "p.*",
            "staff_name"       => "CONCAT(st.firstname, ' ',st.lastname)",
        );
        $select = $db->select()->from(array('p' => $this->_name), $cols);
        $select->joinLeft(array('st'=>'staff'),'st.id = p.created_by',array());
        $select->where('p.id = ?', $id);
        $result = $db->fetchRow($select);
        return $result;
    }
}