<?php

class Application_Model_KpiByModelBrand extends Zend_Db_Table_Abstract
{
    protected $_name = 'kpi_by_model_brand';

    public function fetchPagination($page, $limit, &$total, $params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['km' => 'kpi_by_model_brand'], [
                new Zend_Db_Expr('SQL_CALC_FOUND_ROWS km.id'),
                'store_name' => 's.name',
                'store_id' => 's.id',
                'district_name' => 'di.name',
                'province_name' => 'r.name',
                'area_name' => 'a.name',
                'total_quantity' => "SUM(km.qty)",
                'total_value' => "SUM(km.qty * km.value)",
                'total_value_80' => "SUM(km.qty * km.value * 0.8)",
                'model' => 'g.desc',
                'km.timing_date'
            ])
            ->join(['s' => 'store'], 'km.store_id = s.id', [])
            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'di.parent = r.id', [])
            ->joinLeft(['a' => 'area'], 'a.id = r.area_id', [])
            ->joinLeft(['g' => WAREHOUSE_DB . '.good'], 'km.good_id = g.id', [])
            ->where('km.timing_date >= ?', $params['from_date'])
            ->where('km.timing_date <= ?', $params['to_date']);
//            ->group(['km.store_id', 'km.good_id']);

        if (!$params['export']) {
            $select->group(['km.store_id', 'km.good_id']);
        } else {
            $select->group(['km.store_id', 'km.good_id', 'km.timing_date', 'km.color_id']);
        }

        if($params['area_id']) {
            $select->where('a.id = ?', $params['area_id']);
        }

        if ($limit) {
            $select->limitPage($page, $limit);
        }

        $result = $db->fetchAll($select);

        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }


    public function getToalQuantity( $params)
    {
        $db = Zend_Registry::get("db");
        $select = $db->select()
            ->from(['km' => 'kpi_by_model_brand'], [
                'total_quantity' => "SUM(km.qty)"
            ])
            ->joinLeft(['s' => 'store'], 'km.store_id = s.id', [])
            ->joinLeft(['di' => 'regional_market'], 's.district = di.id', [])
            ->joinLeft(['r' => 'regional_market'], 'di.parent = r.id', [])
            ->joinLeft(['a' => 'area'], 'a.id = r.area_id', [])
            ->where('km.timing_date >= ?', $params['from_date'])
            ->where('km.timing_date <= ?', $params['to_date']);

        if($params['area_id']) {
            $select->where('a.id = ?', $params['area_id']);
        }


        $result = $db->fetchOne($select);

        return $result;
    }

    public function export($data)
    {

        require_once 'PHPExcel.php';
        $PHPExcel = new PHPExcel();

        $heads = array(
            'Store',
            'Area',
            'Province',
            'District',
            'Model',
            'Sellout date',
            'Sellout',
            'Revenue',
            'Revenue 80%'
        );


        $PHPExcel->setActiveSheetIndex(0);
        $sheet = $PHPExcel->getActiveSheet();

        $alpha = 'A';
        $index = 1;
        foreach ($heads as $key) {
            $sheet->setCellValue($alpha . $index, $key);
            $alpha++;
        }
        $index = 2;

        $i = 1;

        foreach ($data as  $item) {
            $alpha = 'A';

            $sheet->setCellValue($alpha++ . $index, $item['store_name']);
            $sheet->setCellValue($alpha++ . $index, $item['area_name']);
            $sheet->setCellValue($alpha++ . $index, $item['province_name'] );
            $sheet->setCellValue($alpha++ . $index, $item['district_name'] );
            $sheet->setCellValue($alpha++ . $index, $item['model']);
            $sheet->setCellValue($alpha++ . $index, date('Y-m-d', strtotime($item['timing_date'])));
            $sheet->setCellValue($alpha++ . $index, $item['total_quantity']);
            $sheet->setCellValue($alpha++ . $index, $item['total_value']);
            $sheet->setCellValue($alpha++ . $index, $item['total_value_80']);

            $index++;
        }

        $filename = 'Report Oneplus ' . date('d-m-Y H-i-s');
        $objWriter = new PHPExcel_Writer_Excel2007($PHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        $objWriter->save('php://output');
        exit;

    }

}