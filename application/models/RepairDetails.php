<?php
class Application_Model_RepairDetails extends Zend_Db_Table_Abstract
{
    protected $_name = 'repair_details';
    protected $_schema = DATABASE_TRADE;
    
    public function getRepairDetails($params){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "p.id", 
            "p.repair_id", 
            "p.category_id", 
            "p.quantity", 
            "type_title" => "GROUP_CONCAT(DISTINCT type.title)", 
            "tp_id" => "GROUP_CONCAT(DISTINCT t.repair_tp_id)", 
            "category_name" => "c.name",
            "file_url"  => "GROUP_CONCAT(DISTINCT file.url)",
            "file_accept"  => "GROUP_CONCAT(DISTINCT file_accept.url)",
            "p.note",
            "p.contructors_id",
            "p.repair_date",
            "contructors_name"  => "contructors.name",
            'category_type_name' => 'e.name',
            'category_type_id' => 'e.id',
            'o.height',
            'o.width',
            "c.has_type",
            "c.has_width",
            'p.review_cost',
            'p.review_note',
            'p.contract_number',
            'p.month_debt'
        );

        $select->from(array('p' => DATABASE_TRADE.'.repair_details'), $arrCols);
        $select->joinLeft(array('c' => DATABASE_TRADE.'.category'), 'c.id = p.category_id', array());
        $select->joinLeft(array('t' => DATABASE_TRADE.'.repair_details_type'), 't.repair_details_id = p.id', array());
        $select->joinLeft(array('file' => DATABASE_TRADE.'.repair_details_file'), 'file.repair_details_id = p.id AND file.type = 1', array());
        $select->joinLeft(array('file_accept' => DATABASE_TRADE.'.repair_details_file'), 'file_accept.repair_details_id = p.id AND file_accept.type = 3', array());
        $select->joinLeft(array('type' => DATABASE_TRADE.'.repair_type'), 'type.id = t.repair_type_id', array());
        $select->joinLeft(array('tp' => DATABASE_TRADE.'.repair_details_tp'), 'tp.repair_details_id = p.id', array());
        $select->joinLeft(array('contructors' => DATABASE_TRADE.'.contructors'), 'contructors.id = p.contructors_id', array());

        $select->joinLeft(['o' => DATABASE_TRADE.'.app_checkshop_detail_child'], 'p.app_checkshop_detail_child_id = o.id', []);
        $select->joinLeft(['e' => DATABASE_TRADE.'.category_inventory_type'], 'o.category_type = e.id', []);
        
        $select->where('p.repair_id = ?', $params['id']);
        $select->group('p.id');
        
        $result = $db->fetchAll($select);

        return $result;
    }
    

    public function getContract($params){
         $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "store"    => "s.name",
            "category"      => "c.name",
            "p.id", 
            "p.repair_id", 
            "p.category_id", 
            "final_price"      => "p.price_final",
            "p.quantity",
            "p.contract_id"
        );

        $select->from(array('p' => DATABASE_TRADE.'.repair_details'), $arrCols);
         $select->joinLeft(array('a' => DATABASE_TRADE.'.repair'), 'a.id = p.repair_id', array());
        $select->joinLeft(array('c' => DATABASE_TRADE.'.category'), 'c.id = p.category_id', array());
         $select->joinLeft(array('s' =>'store'), 's.id = a.store_id', array());
        
        $select->where('p.contructors_id = ?', $params['contractor_id']);
        $select->where('a.id IS NOT NULL');
        $result = $db->fetchAll($select);
        return $result;
    }
}