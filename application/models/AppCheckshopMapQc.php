<?php
class Application_Model_AppCheckshopMapQc extends Zend_Db_Table_Abstract
{
    protected $_name = 'app_checkshop_map_qc';

    protected $_schema = DATABASE_TRADE;
    
    //Check xem shop đó trong tháng đc QC chưa
    public function checkQc($store_id, $created_at,$type){
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            'id' => "p.id" 
        );

        $select->from(array('p' => DATABASE_TRADE.'.app_checkshop_map_qc'), $arrCols);
        $select->joinLeft(array('c' => DATABASE_TRADE.'.app_checkshop'), 'c.id = p.checkshop_id', array());
        $select->where('c.store_id = ?', $store_id);
        $select->where('p.type = ?', $type);
        $select->where('MONTH(c.created_at) = ?', date('m', strtotime($created_at)));
        $select->where('YEAR(c.created_at) = ?', date('Y', strtotime($created_at)));

        $result = $db->fetchRow($select);

        return $result;
    }
    
    public function getHistoryQc($params) {
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            "id" => "p.id",
            "created_at" => "c.created_at",
            "p.note",
            "q.title",
            "p.type",
            "month_year" => "DATE_FORMAT(`c`.`created_at`, '%c/%Y')",
            "type_name" => '(CASE WHEN p.type = 1 THEN "TMK Local" WHEN p.type = 2 THEN "TMK Leader" WHEN p.type = 3 THEN "QC" END)'
        );

        $select->from(array('p' => DATABASE_TRADE.'.app_checkshop_map_qc'), $arrCols);
        $select->joinLeft(array('q' => DATABASE_TRADE.'.app_checkshop_qc'), 'q.id = p.qc_id', array());
        $select->joinLeft(array('c' => DATABASE_TRADE.'.app_checkshop'), 'c.id = p.checkshop_id', array());
        $select->where('c.store_id = ?', $params['store_id']);
        
        $result = $db->fetchAll($select);
        
        if($_GET['dev'] == 2){
            echo $select;exit;
        }

        return $result;
    }
    
    public function  getQcByMonth($params){
        
        $sub_select = "
            select
                p.store_id,
                p.updated_at,
                @last_updated_at,
                @last_updated_at := IF(@last_store_id <> p.store_id, DATE_FORMAT(p.updated_at, '%Y-%m-%d'), IF( DATE_FORMAT(p.updated_at, '%Y-%m-%d') > DATE_ADD(@last_updated_at, INTERVAL +2 DAY) , DATE_FORMAT(p.updated_at, '%Y-%m-%d'), @last_updated_at )) AS last_updated_at,
                @last_store_id := p.store_id
            from
                `trade_marketing`.app_checkshop p,
                ( select @last_store_id := 0, @last_updated_at := 0 ) i
            WHERE p.updated_at IS NOT NULL
            order by p.store_id,p.updated_at ASC
            ";
        
        
        $db     = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            's.name', 
            'p.store_id', 
            'p.updated_at', 
            'p.last_updated_at', 
            'month' => 'MONTH(p.updated_at)', 
            'year' => 'YEAR(p.updated_at)',
            'month_year' => 'CONCAT(MONTH(p.updated_at), "/", YEAR(p.updated_at))',
            'eligible' => 'COUNT(DISTINCT p.last_updated_at)' 
        );
        
        $select->from(array('p' => new Zend_Db_Expr('(' . $sub_select . ')')), $arrCols);
        $select->joinLeft(array('s' => 'store'), 's.id = p.store_id', array());
        
        if(!empty($params['store_id'])){
            $select->where('p.store_id = ?', $params['store_id']);
        }
        
        $select->group(array('p.store_id', 'MONTH(p.updated_at)', 'YEAR(p.updated_at)'));
        
        $result = $db->fetchAll($select);
        return $result;
    }

}