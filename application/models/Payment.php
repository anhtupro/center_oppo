<?php
class Application_Model_Payment extends Zend_Db_Table_Abstract
{
    protected $_name = 'payment';
    
    public function fetchPagination($page, $limit, &$total, $params){
        
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            'content' => "p.content",
        );

        $select->from(array('p' => 'payment'), $arrCols);
        $select->joinLeft(array('s' => 'staff'), 's.id = p.created_by', array());

        $select->limitPage($page, $limit);
        $result = $db->fetchAll($select);
        $total = $db->fetchOne("select FOUND_ROWS()");

        return $result;
    }
    
    public function getDetail($id){
        
        $db = Zend_Registry::get("db");
        $select = $db->select();

        $arrCols = array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS DISTINCT p.id'),
            'content' => "p.content",
            'created_by' => "p.created_by",
            'department_id' => "p.department_id",
            'title_id' => "p.title_id",
            'team_id' => "p.team_id",
            'fullname' => "CONCAT(s.firstname, ' ', s.lastname)",
            'department_name' => 't.name',
            'price' => 'p.price',
            'invoice_number' => 'p.invoice_number',
            'status' => 'p.status',
            
        );

        $select->from(array('p' => 'payment'), $arrCols);
        $select->joinLeft(array('s' => 'staff'), 's.id = p.created_by', array());
        $select->joinLeft(array('t' => 'team'), 't.id = p.department_id', array());
        
        $select->where('p.id = ?', $id);
        
        $result = $db->fetchRow($select);

        return $result;
    }
    
    

}