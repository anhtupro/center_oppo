<?php

class NotificationController extends My_Application_Controller_Cli {

    public function init() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        set_time_limit(0);
        ini_set('memory_limit', '-1');
    }

    public function clearExpiredAction() {
        exit;
        echo "\r\n" . date('Y-m-d H:i:s') . " : Start clearing expired notifications\r\n";
        $sql = sprintf("DELETE FROM notification_new  WHERE show_to < NOW() AND type = %d", My_Notification_Type::MassUpload);
        $db  = Zend_Registry::get('db');
        $db->query($sql);
        echo "Done\r\n";

        exit;
    }

    public function logExcutionAction() {
        $db     = Zend_Registry::get('db');
        $sql    = "SELECT  l.* FROM hr.`log_excution_action` l WHERE (l.`finished` =0 OR l.`excution_time` >20)
                    AND UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(l.`created_at`) <= 120 AND l.`action_name` NOT IN ('laser-text-save','laserdeduction-save','add-invoice-number','logout','confirm','view-e-invoice','print-list')";
        $stmt   = $db->query($sql);
        $result = $stmt->fetchAll();
        if (count($result) >= 20) {
            // send notification
            $ding          = new My_DingNotification(805901136, 'fZAZZBpVWG9yr6QulBQOiRyK9qVIXaGVjPquzzSNubn87OPF5CV1YagJ1Z8Ut-Zu', 'dingnev91pol1g0mash4');
            $list_uid_tech = '235568670348483834116,
2148595210-1797256885,
235569066329-752312035,
2727273251-930780342,
235568690422-2052823153,
235569256446-445935308,
21542463431140551780,
3103671549712351255,
235601171459672293739,
235600662420-2042988506,
235600174361-1860461914,
235603463919-1279487344,
235603491654-219182205
';
            $message       = "Hi team, Database hiện tại có hơn 10 request đang treo quá 120s.Team vào check gấp ". date('Y-m-d H:i:s').",session " . uniqid();
            $ding->sendTextNotification($list_uid_tech, $message);
        }
        exit("ok");
    }

}
