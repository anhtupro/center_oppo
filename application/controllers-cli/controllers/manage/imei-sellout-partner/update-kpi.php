<?php

// $imei			= $this->getRequest()->getParam('imei');
// $userStorage	= Zend_Auth::getInstance()->getStorage()->read();
// $user_id             = $userStorage->id;
// $QImeiSelloutPartner = new Application_Model_ImeiSelloutPartner();
// $data = array('pg_id' => $user_id );
// $where = $QImeiSelloutPartner->getAdapter()->quoteInto('imei_sn = ?', $imei);
// $QImeiSelloutPartner->update($data, $where);
// $this->_redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : HOST.'manage/imei-sellout-partner');

$status               = $this->getRequest()->getParam('status');
$product              = $this->getRequest()->getParam('product');
$color                = $this->getRequest()->getParam('model');
$store                = $this->getRequest()->getParam('store_id');
$from                 = $this->getRequest()->getParam('from', date('01/m/Y'));
$to                   = $this->getRequest()->getParam('to', date('d/m/Y'));
$imei                 = $this->getRequest()->getParam('imei', null);
$area_id              = $this->getRequest()->getParam('area_id');
$regional_market      = $this->getRequest()->getParam('regional_market');
$district             = $this->getRequest()->getParam('district');
$sales_team           = $this->getRequest()->getParam('sales_team');
$staff                = $this->getRequest()->getParam('staff_id', null);
$shop_type            = $this->getRequest()->getParam('shop_type');
$kpi                  = $this->getRequest()->getParam('kpi');
$export               = $this->getRequest()->getParam('export');
$status_imei          = $this->getRequest()->getParam('status_imei');
$page                 = $this->getRequest()->getParam('page');
$imei_sn              = $this->getRequest()->getParam('imei_sn');
$pg_id                = $this->getRequest()->getParam('pg_id');
$installment_company  = $this->getRequest()->getParam('installment_company');
$installment_contract = $this->getRequest()->getParam('installment_contract');


 if (in_array($_SERVER['REMOTE_ADDR'], array('103.37.32.126', '112.109.92.6', '14.161.22.196', '115.78.166.171'))) {
        echo "<pre>";
        print_r(222);
        die;
    }


$params = array(
    'product'         => intval($product),
    'color'           => intval($color),
    'store_id'        => trim($store),
    'from'            => trim($from),
    'to'              => trim($to),
    'imei'            => $imei,
    'status'          => intval($status),
    'shop_type'       => intval($shop_type),
    'kpi'             => intval($kpi),
    'area_id'         => intval($area_id),
    'regional_market' => intval($regional_market),
    'district'        => intval($district),
    'status_imei'     => intval($status_imei),
    'page'            => $page
);

try {
    $flashMessenger = $this->_helper->flashMessenger;

    if (!$imei_sn)
        throw new Exception("Invalid imei");
    $QLogSelloutPartner  = new Application_Model_LogSelloutPartner();
    $userStorage         = Zend_Auth::getInstance()->getStorage()->read();
    $user_id             = $userStorage->id;
    $QImeiSelloutPartner = new Application_Model_ImeiSelloutPartner();
    $where_check         = [];
    $where_check[]       = $QImeiSelloutPartner->getAdapter()->quoteInto('imei_sn = ?', $imei_sn);
    $where_check[]       = $QImeiSelloutPartner->getAdapter()->quoteInto('is_chot = 2');
    $data_check          = $QImeiSelloutPartner->fetchAll($where_check);

    if (!empty($data_check->toArray())) {
        $data_log = array(
            'user_id'    => $user_id,
            'imei_sn'    => $imei_sn,
            'pg_id'      => empty($pg_id) ? $user_id : $pg_id,
            'action'     => "update fail",
            'created_at' => date('Y-m-d H:i:s')
        );
        $QLogSelloutPartner->insert($data_log);
        throw new Exception("Imei đã bị khóa update.Hệ thống đã ghi nhận và sẽ chuyển về phòng nhân sự giải quyết");
    }
// echo HOST.'manage/imei-sellout-partner/'.( $params ? '?'.http_build_query($params).'&' : '?');die;
    if (empty($pg_id)) {
        $data = array('pg_id' => $user_id);
    } else
        $data = array('pg_id' => $pg_id);



    $data_log = array(
        'user_id'    => $user_id,
        'imei_sn'    => $imei_sn,
        'pg_id'      => empty($pg_id) ? $user_id : $pg_id,
        'action'     => "update",
        'created_at' => date('Y-m-d H:i:s')
    );

   

    $where = $QImeiSelloutPartner->getAdapter()->quoteInto('imei_sn = ?', $imei_sn);
    $QImeiSelloutPartner->update($data, $where);
    $QLogSelloutPartner->insert($data_log);
    if (in_array($_SERVER['REMOTE_ADDR'], array('103.37.32.126', '112.109.92.6', '14.161.22.196', '115.78.166.171'))) {
        $installment_model = new Application_Model_ImeiInstallment();
        $where_install     = [];
        $where_install[]   = $installment_model->getAdapter()->quoteInto('imei_sn = ?', $imei_sn);
        $where_install[]   = $installment_model->getAdapter()->quoteInto('installment_company = ?', $installment_company);
        $info              = $installment_model->fetchAll($where_install);
        if(empty($info)) {
            $data_installment = array(
                'imei_sn'             => $imei_sn,
                'staff_id'            => $pg_id,
                'contract_number'     => $installment_contract,
                'installment_company' => $installment_company,
                'is_ka_link'          => 1,
                'created_at'          => date('Y-m-d H:i:s')
            );
            $installment_model->insert($data_installment);
        }
    }
    $flashMessenger->setNamespace('success')->addMessage('Success');
    $this->_redirect(HOST . 'manage/imei-sellout-partner/' . ( $params ? '?' . http_build_query($params) . '&' : '?' ));
} catch (Exception $e) {
    $flashMessenger->setNamespace('error')->addMessage($e->getMessage());
    $this->_redirect(HOST . 'manage/imei-sellout-partner/' . ( $params ? '?' . http_build_query($params) . '&' : '?' ));
}