<?php
/**
*
*/
class TimingController extends My_Application_Controller_Cli
{
	public function init()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
        set_time_limit(0);
        ini_set('memory_limit', '-1');

	}

    public function timingExpired()
    {
        $db = Zend_Registry::get('db');

        $QTime = new Application_Model_Time();
        $limited_time_as_today = $QTime->getLimitedTime();
        $QStaff = new Application_Model_Staff();
        $staff_cache = $QStaff->get_cache();
        $QTimeStaffExpired = new Application_Model_TimeStaffExpired();
        $QLog = new Application_Model_Log();
        $category_id = 4;
        // tìm các chấm công chưa chấm
        $sql = sprintf("SELECT
                            p.staff_id,
                            CONCAT(s.`lastname`, s.`firstname` ,  ' '),
                            s.title,
                            select_limited_timing(s.id) AS limited
                        FROM
                            time AS p
                        INNER JOIN staff AS s ON p.staff_id = s.id
                        WHERE
                            (
                                p.created_at BETWEEN DATE_FORMAT(NOW(), '%Y-%m-01')
                                AND NOW()
                            )
                        AND(
                            p.created_at BETWEEN DATE_FORMAT(NOW(), '%Y-%m-%d')
                            AND NOW()
                        )
				");

        $data = $db->fetchAll($sql);

        foreach($data as $k=>$v)
        {
            if($v['limited'] <= 22)
            {

                $where = array();
                $where[] = $QTimeStaffExpired->getAdapter()->quoteInto('staff_id = ? ' , $v['staff_id']);
                $where[] = $QTimeStaffExpired->getAdapter()->quoteInto('approved_at is null' , null);
                $result = $QTimeStaffExpired->fetchRow($where);

                if($result)
                {
                    break;
                }

                $title = sprintf("Tài khoản chấm công của bạn %s đã bị khóa", $staff_cache[ $v['staff_id'] ]);
                $content = sprintf("<p>Chào bạn %s,</p>
                    <p>Theo chính sách của Công ty, Bạn đã không chấm công ba ngày , hệ thống sẽ khóa tài khoản của bạn.</p>
                    <p>Để sử dụng lại tài khoản vui lòng liên hệ bộ phận nhân sự: </p> " , $staff_cache[ $v['staff_id'] ] );

                My_Notification::add(
                    $title,
                    $content,
                    $category_id,
                    array(
                        'staff' => $v['staff_id'],
                        'from'  => date('d/m/Y H:i:s'),
                        'to'    => (new DateTime('now'))
                            ->add(new DateInterval('P10D'))
                            ->format('d/m/Y H:i:s'),
                    ),
                    1,
                    My_Notification_Type::SystemCreate
                );
                //them nhan vien do vo bang LOG
                $data = array(
                    'staff_id' => $v['staff_id'],
                    'locked_at' => date('Y-m-d h:i:s')
                );

                $QTimeStaffExpired->insert($data);
            }
        }
    }

	/**
	 * Xóa các IMEI trong bảng chấm công
	 * 		3 ngày sau khi chấm công chưa active
	 * 		hoặc active trước khi chấm công hơn 3 ngày
	 * @return
	 */
	public function removeExpiredImeiAction()
	{
		$delete_from = "2015-04-07 00:00:00";
		$category_id = 4;
		$timing_day = 7;
		// $dead_day = 15;

		$QStore      = new Application_Model_Store();
		$QStaff      = new Application_Model_Staff();
		$QGood       = new Application_Model_Good();
		$QGoodColor  = new Application_Model_GoodColor();

		$store_cache = $QStore->get_cache();
		$staff_cache = $QStaff->get_cache();
		$good_cache  = $QGood->get_cache();
		$color_cache = $QGoodColor->get_cache();

		$db = Zend_Registry::get('db');

		// tìm các chấm công sẽ xóa
		$sql = sprintf("SELECT
					timing.staff_id,
					timing.`from`,
					timing.store,
					timing_sale.product_id,
					timing_sale.model_id,
					timing_sale.imei,
					warehouse.imei.activated_date
				FROM
					timing,
					timing_sale,
					warehouse.imei
				WHERE
					timing.id = timing_sale.timing_id
				AND imei.imei_sn = timing_sale.imei
				AND (
						(
							imei.activated_date IS NULL
							OR imei.activated_date = 0
							OR imei.activated_date LIKE ''
					) -- chưa active
					AND DATEDIFF(NOW(), timing.`from`) > %d -- từ ngày kiểm tra về ngày chấm công hơn $timing_day ngày
				)
				AND timing.`from` >= '%s' -- chỉ xóa IMEI của các báo cáo sau ngày ra thông báo
				AND (timing_sale.reimport = 0 OR timing_sale.reimport IS NULL); -- không xóa IMEI đã tái nhập
				", $timing_day, $delete_from);

		$timings = $db->query($sql);
		$timing_arr = array();

		// gộp các IMEI theo staff
		foreach ($timings as $_key => $_timing) {
			if (! isset( $staff_cache[ $_timing['staff_id'] ] ) ) continue;

			if (!isset($timing_arr[ $_timing['staff_id'] ]))
				$timing_arr[ $_timing['staff_id'] ] = array();

			$timing_arr[ $_timing['staff_id'] ][] = array(
				'imei'       => $_timing['imei'],
				'good_id'    => $_timing['product_id'],
				'good_color' => $_timing['model_id'],
				'store_id'   => $_timing['store'],
				'activated_date' => isset($_timing['activated_date']) ? date('d/m/Y', strtotime($_timing['activated_date'])) : 0,
				'date'       => date( 'd/m/Y', strtotime( $_timing['from'] ) ),
			);
		} // END foreach: gộp các IMEI theo staff

		unset($sql);
		unset($timings);

		// tạo notification
		$title = sprintf("Danh sách IMEI bị xóa ngày %s", date("d/m/Y"));

		foreach ($timing_arr as $_staff_id => $_timings) {
			if (! count($_timings) ) continue;

			$content = sprintf("<p>Chào bạn %s,</p>
<p>Theo chính sách của Công ty, từ ngày 07/04/2015, những IMEI đã chấm công mà sau 07 ngày chưa active đều không được tính KPI và sẽ được gỡ khỏi danh sách các máy chấm công.</p>
<p>Sau đây là danh sách các IMEI bạn đã chấm công bị gỡ bỏ trong ngày %s:</p>
<ul>",
				$staff_cache[ $_staff_id ],
				date("d/m/Y")
			);

			// danh sách IMEI bị xóa
			foreach ($_timings as $_key => $_timing) {
				$content .= sprintf("<li>IMEI: <strong>%s</strong>;
<br />Model: %s/%s;
<br />Ngày bán: %s;
<br />Ngày active: %s;
<br />Cửa hàng: %s;</li>",
					$_timing['imei'],
					isset( $good_cache[ $_timing['good_id'] ] ) ? $good_cache[ $_timing['good_id'] ] : '#',
					isset( $color_cache[ $_timing['good_color'] ] ) ? $color_cache[ $_timing['good_color'] ] : '#',
					$_timing['date'],
					$_timing['activated_date'] ? $_timing['activated_date'] : 'Chưa active',
					isset( $store_cache[ $_timing['store_id'] ] ) ? $store_cache[ $_timing['store_id'] ] : '#'
				);
			} // END foreach: danh sách IMEI bị xóa

			$content .= "</ul>";

			My_Notification::add(
			    $title,
			    $content,
			    $category_id,
			    array(
					'staff' => $_staff_id,
					'from'  => date('d/m/Y H:i:s'),
					'to'    => (new DateTime('now'))
		                ->add(new DateInterval('P10D'))
		                ->format('d/m/Y H:i:s'),
			    ),
			    1,
			    My_Notification_Type::SystemCreate
			);

			unset($content);
		} // END foreach: tạo notification

		unset($timing_arr);

		$sql = sprintf("REPLACE INTO timing_sale_expired
				SELECT
					timing_sale.*
				FROM
					timing,
					timing_sale,
					warehouse.imei
				WHERE
					timing.id = timing_sale.timing_id
				AND imei.imei_sn = timing_sale.imei
				AND (
						(
							imei.activated_date IS NULL
							OR imei.activated_date = 0
							OR imei.activated_date LIKE ''
					) -- chưa active
					AND DATEDIFF(NOW(), timing.`from`) > %d -- từ ngày kiểm tra về ngày chấm công hơn $timing_day ngày
				)
				AND timing.`from` >= '%s' -- chỉ xóa IMEI của các báo cáo sau ngày ra thông báo
				AND (timing_sale.reimport = 0 OR timing_sale.reimport IS NULL); -- không xóa IMEI đã tái nhập
				", $timing_day, $delete_from);

		try {
			$res = $db->query($sql);
			$n = $res->rowCount();
			echo sprintf("%s\t- Moved %d IMEIs to trash\r\n", date('Y-m-d H:i:s'), $n);

			unset($n);
			unset($res);
			unset($sql);
		} catch (Exception $e) {}

		$sql = sprintf("DELETE e
				FROM
					timing_sale e
				INNER JOIN timing ON e.timing_id = timing.id
				INNER JOIN warehouse.imei ON warehouse.imei.imei_sn = e.imei
				WHERE (
					(
						imei.activated_date IS NULL
						OR imei.activated_date = 0
						OR imei.activated_date LIKE ''
					)
					AND DATEDIFF(NOW(), timing.`from`) > %d
				)
				AND timing.`from` >= '%s'
				AND (e.reimport = 0 OR e.reimport IS NULL); -- không xóa IMEI đã tái nhập
				", $timing_day, $delete_from);

		try {
			$res = $db->query($sql);
			$n = $res->rowCount();
			echo sprintf("\t\t\t- Deleted %d IMEIs from timing list\r\n", $n);

			unset($n);
			unset($res);
			unset($sql);
		} catch (Exception $e) {}

		unset($db);

		exit;
	}

	public function importAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		////////////////////////////////////////////////

		$file_path = $this->getRequest()->getParam('file');

		if (!$file_path) exit("File path is required.\r\n");

		$file_path = trim($file_path);

		if (!file_exists($file_path)) exit("File not exists.\r\n");

		//read file
		include 'PHPExcel/IOFactory.php';

		//  Read your Excel workbook
		try {
		    $inputFileType = PHPExcel_IOFactory::identify($file_path);
		    $path_info = pathinfo($file_path);
		    $extension = $path_info['extension'];
		    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
		    $objPHPExcel = $objReader->load($file_path);
		} catch (Exception $e) {
		    exit($e->getMessage()."\r\n");
		}

		//  Get worksheet dimensions
		$sheet = $objPHPExcel->getSheet(0);
		$highestRow = $sheet->getHighestRow();
		$highestColumn = $sheet->getHighestColumn();

		/**
		 * Danh sách các đơn hàng lỗi
		 * @var array
		 */
		define("START_ROW", 2);
		define("IMEI_COL", 3);
		$error_list = array();
		$success_list = array();
		$order_rows = $highestRow - START_ROW;

		$header = $sheet->rangeToArray('A' . 1 . ':' . $highestColumn . 1,
		            NULL, TRUE, FALSE);
		$header = isset($header[0]) ? $header[0] : array();

		$db = Zend_Registry::get('db');
		$QTimingSale = new Application_Model_TimingSale();
		$QTimingSaleExpired = new Application_Model_TimingSaleExpired();

		for ($row = START_ROW; $row <= $highestRow; $row++) {

		    My_Cli_Util::show_status($row, $order_rows);

		    try {
		        //
		        $range = 'A' . $row . ':' . $highestColumn . $row;
		        $sheet
		            ->getStyle($range)
		            ->getNumberFormat()
		            ->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );

	            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
		            NULL, TRUE, FALSE);

		        // validate data
		        $rowData = isset($rowData[0]) ? $rowData[0] : array();

		        $rowData[IMEI_COL] = preg_replace( '/[^0-9]/', '', trim($rowData[IMEI_COL]) );

		        if (empty($rowData[IMEI_COL])) throw new Exception("Empty IMEI, row: ".$row);

		        // kiểm tra IMEI chưa nhập nảy giờ
		        if (in_array($rowData[IMEI_COL], $success_list))
		        	throw new Exception("Duplicated IMEI, row: ".$row);
		        $where = $QTimingSale->getAdapter()->quoteInto('imei = ?', $rowData[IMEI_COL]);
		        $timing_sale = $QTimingSale->fetchRow($where);
		        if ($timing_sale) throw new Exception("IMEI exists, row: ". $row);

		        $where = $QTimingSaleExpired->getAdapter()->quoteInto('imei = ?', $rowData[IMEI_COL]);
		        $timing_sale_expired = $QTimingSaleExpired->fetchRow($where);
		        if (!$timing_sale_expired) throw new Exception("IMEI ".$rowData[IMEI_COL]." not exists, row: ".$row);

		        try {
		        	$sql = sprintf("REPLACE INTO timing_sale SELECT * FROM timing_sale_expired WHERE imei=%s", $rowData[IMEI_COL]);
		        	$db->query($sql);

		        	$sql = sprintf("DELETE FROM timing_sale_expired WHERE imei=%s", $rowData[IMEI_COL]);
		        	$db->query($sql);

		        	// đánh dấu IMEI đã nhập
		        	$success_list[] = $rowData[IMEI_COL];
		        } catch(Exception $e) {
		        	throw new Exception($e->getMessage().", row: ". $row." -- ".$sql);
		        }
		    } catch (Exception $e) {
		    	$rowData[IMEI_COL] = "'".$rowData[IMEI_COL];
		        $error_list[] = array_merge($rowData, array($e->getMessage()));
		    }
		    // nothing here
		} // END loop through order rows

		// xuất file excel các order lỗi
		if (is_array($error_list) && count($error_list) > 0) {
		    $error_file_name = 'FAILED-'.date('Y-m-d H-i-s') . '.'.$extension;
		    // xuất excel @@
		    //
		    $objPHPExcel_out = new PHPExcel();
		    $objPHPExcel_out->createSheet();
		    $objWorksheet_out = $objPHPExcel_out->getActiveSheet();
		    //
		    // Title
		    $index = '1';
		    $row = $header;
		    $objWorksheet_out->fromArray($row, NULL, 'A'.$index++);

		    // các dòng lỗi
		    foreach ($error_list as $key => $row) {
		        $objWorksheet_out->fromArray($row, NULL, 'A'.$index++);
		    }
		    //

		    try {
		        switch ($extension) {
		            case 'xls':
		                $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel_out);
		                break;
		            case 'xlsx':
		                $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel_out);
		                break;
		            default:
		                throw new Exception("Invalid file extension");
		                break;
		        }

		        $new_file_dir = dirname($file_path) . DIRECTORY_SEPARATOR . $error_file_name;

		        $objWriter->save($new_file_dir);
		    } catch (Exception $e) {
		        echo $e->getMessage()."\r\n";
		    }
		}
		// END IF // xuất file excel các order lỗi

		printf("Success: %d\r\n", count($success_list));
		printf("Failed: %d\r\n", count($error_list));
	}

	public function callImeiKpiAction()
	{
		error_reporting(E_ALL);
		ini_set('display_errors', 1);
		ini_set('memory_limit', -1);
		set_time_limit(0);

		$from = $this->getRequest()->getParam('from');
		$to = $this->getRequest()->getParam('to');

		if (!isset($from) || !$from || !strtotime($from))
			exit("From date invalid");

		if (!isset($to) || !$to || !strtotime($to))
			exit("From date invalid");

		echo "Start call sp_set_imei_kpi, from $from, to $to";
		$sql = "call sp_set_imei_kpi(?, ?)";
		$db = Zend_Registry::get('db');
		$db->query($sql, array($from, $to));
	}
}