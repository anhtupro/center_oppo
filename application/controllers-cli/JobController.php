<?php
class JobController extends My_Application_Controller_Cli {
    
    private function sendmailjob($to, $subject, $maildata, $image = ''){
        include_once APPLICATION_PATH.'/../library/phpmail/class.phpmailer.php';
        $app_config   = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
        $config       = $app_config->mail->smtp;

        $mailfrom     = 'recruitment@oppomobile.vn';
    // $mailfrom     = $config->user2;
        $mailfromname = 'OPPO Tuyển dụng';
        $mail = new PHPMailer();

        $body = $maildata; // nội dung email
//        $body = eregi_replace("[\]",'',$body);

        $mail->IsHTML();

        $mail->IsSMTP();
        $mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
                                                   // 1 = errors and messages
                                                   // 2 = messages only
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
        $mail->SMTPSecure = $config->ssl;                 // sets the prefix to the servier // Config this
        $mail->Host       = $config->host;      // sets GMAIL as the SMTP server // Config this
        $mail->Port       = $config->port;                   // set the SMTP port for the GMAIL server // Config this
       // $mail->Username   = $config->user2;  // GMAIL username // Config this
        $mail->Username   = 'recruitment@oppomobile.vn';  // GMAIL username // Config this
     //   $mail->Password   = $config->pass2;            // GMAIL password // Config this
        $mail->Password   = '2017#oppo';            // GMAIL password // Config this
        
        $mail->From = $mailfrom;
        $mail->FromName = $mailfromname;

        $mail->SetFrom($mailfrom, $mailfromname); //Định danh người gửi

        //$mail->AddReplyTo($mailfrom, $mailfromname); //Định danh người sẽ nhận trả lời

        $mail->Subject    = $subject; //Tiêu đề Mail

        $mail->AltBody    = "Để xem tin này, vui lòng bật chế độ hiển thị mã HTML!"; // optional, comment out and test

        // $mail->AddAttachment($image);
        if($image != '')
            $mail->AddEmbeddedImage($image, 'embed_img');

        $mail->MsgHTML($body);

        if (is_array($to)) {
            foreach ($to as $key => $value) {
                $mail->AddAddress($value, ''); //Gửi tới ai ?
            }
        } else {
            $mail->AddAddress($to, ''); //Gửi tới ai ?
        }
        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $QStaff = new Application_Model_Staff();
        $select_staff = $QStaff->getAdapter()->quoteInto('id = ?', $userStorage->id);
        $infoStaff = $QStaff->fetchRow($select_staff);
        $emailInfo      = $infoStaff->email;
//        $mail->addBcc($emailInfo);
        $mail->addBcc('recruitment@oppomobile.vn');

        if(!$mail->Send() ) {
//             echo date('Y-m-d H:i:s')." - Failed\n";
            return -1;
        } else {
            //echo date('Y-m-d H:i:s')." - Done\n"; //DEBUG
            return 1;
        }

    }
    public function sendmailAction(){
        $QJob = new Application_Model_Job();
        $action_jobs = $QJob->fetchSendMailList();
        if(is_array($action_jobs) && count($action_jobs)!=0){
            foreach ($action_jobs as $cv) {
                // GET CONTENT
                $this_mailto = $cv['infor']['email'];
                $title       = $cv['infor']['title'];
                $this_subject = "Hồ sơ ứng tuyển cho vị trí $title";
                $this_content = $this->getMailContent($cv['infor'],$cv['cvs']);
                // SEND MAIL
                 $this->sendmailjob($this_mailto, $this_subject, $this_content);
            }
        }else{
            echo "no cv is sent yet";
        }
        die();
    }
    

    public function getMailContent($job, $cvs){
        $QLogMail = new Application_Model_LogJobEmail();
        $job_id = $job['id'];
        $job_title = $job['title'];
        $mail_template = "Xin chào [created_by],<br><br>Hệ thống center OPPO vừa nhân được [count_cv] hồ sơ ứng tuyển cho $job_title<br>";
        
        $mail_template = str_replace('[created_by]', $job['fullname'], $mail_template);
        $mail_template = str_replace('[count_cv]', count($cvs), $mail_template);
        
        // echo $mail_template;
        foreach ($cvs as $key => $item) {
            $this_link = HOST."job/view-cv?id=". $item['id'];
            $mail_template.= 'Link truy cập hồ sơ '.($key+1).': <a href="'.$this_link.'" target="_blank">Đường dẫn</a> <br>';
                // SEND MAIL AND INSERT TO LOG
                $QLogMail->insert([
                    'job_id'    =>  $job_id,
                    'cv_id'     =>  $item['id'],
                    'sent_at'   =>  date('Y-m-d H:i:s'),
                ]);
        }

        $mail_template.="<br>Trân trọng.";
        $hr_signature = '
                    <h3 style="color: #00b050;margin-bottom: 0;">Recruitment Team</h3>
                    <p style="margin-bottom: 0;
                        margin-top: 0;">__________________________________</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">T (84-8) 39202555 - ext: 111</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">F (84-8) 39202555 - ext: 120</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">E <a href="mailto:recruitment@oppomobile.vn">recruitment@oppomobile.vn</a></p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">OPPO Science & Technology Co.,Ltd</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 5px;">12th Floor, Lim II Tower, No. 62A CMT8 St., Ward 6, Dist. 3, HCMC, VN</p>
                    <p style="margin-bottom: 0;
                        margin-top: 0;">__________________________________</p>
                    <p style="margin-bottom: 5px;
                        margin-top: 0;"><a href="http://vieclam.oppomobile.vn/">www.vieclam.oppomobile.vn</a></p>';
        return $mail_template.$hr_signature;
    }
}
