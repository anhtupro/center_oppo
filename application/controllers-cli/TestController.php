<?php
class TestController extends My_Application_Controller_Cli
{
    public function init()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

    }

    // public function getEnumAction()
    // {
    //     include_once APPLICATION_PATH.'/../library/My/Class/Enum.php';
        
    // }

    public function indexAction()
    {
        echo "\nStart at: " . date('H:i:s') ."\n";

        $test = new Zend_Session_Namespace('test');
        
        for ($i=0; $i < 100; $i++) { 
            
            if ( ! isset( $test->viewed ) ) {
                $test->viewed = 1;
                echo "New Session: " . date('H:i:s') ."\n";
            } else {
                echo "Session Existed: " . date('H:i:s') ."\n";
            }

            sleep(60);

        }

        echo "End at: " . date('H:i:s') ."\n";
        exit;
    }
}
