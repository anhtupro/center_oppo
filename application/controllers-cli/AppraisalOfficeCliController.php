<?php
/**
 * Created by PhpStorm.
 * User: lphuctai
 * Date: 11/02/2019
 * Time: 13:59
 */

class AppraisalOfficeCliController extends My_Application_Controller_Cli
{
    public function init()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }

    public function initAction()
    {
        try {
            $QPlan = new Application_Model_AppraisalOfficePlan();
            $plans = $QPlan->fetchAll($QPlan->getAdapter()->quoteInto('aop_status = 0 and aop_from <= now()', null))->toArray();
            foreach ($plans as $plan) {
                if(in_array($plan['aop_type'], [1, 2]))  {
                    /** @var Zend_Db_Adapter_Abstract $db */
                    $db = Zend_Registry::get('db');
                    // Get staff from head office (office_id = 49)
                    $query = 'insert into appraisal_office_to_do(fk_plan, aotd_head_id, aotd_staff_id)
select :plan_id, m.id, s.id
from appraisal_office_head h
  join staff m on h.manager_title = m.title and m.off_date is null
  join staff s on h.staff_title = s.title and s.off_date is null
where h.status = 1';
                    $stmt = $db->prepare($query);
                    $stmt->bindParam('plan_id', $plan['aop_id'], PDO::PARAM_INT);
                    $result = $stmt->execute();
                    $stmt->closeCursor();
                    // Update plan running
                    if($result) {
                        $QPlan->update([
                            'aop_status' => 1,
                            'aop_updated_at' => date('Y-m-d H:i:s'),
                            'aop_updated_by' => 0
                        ], $QPlan->getAdapter()->quoteInto('aop_id = ?', $plan['aop_id']));
                    }
                }
            }
            echo 'Done' . "\n";
        } catch (\Exception $ex) {
            var_dump($ex->getMessage()."\n\n".$ex->getTraceAsString());
        }
    }
}