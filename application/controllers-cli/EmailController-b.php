<?php

class EmailController extends My_Application_Controller_Cli {

//    private $hr_signature = '<h3 style="color: #00b050;margin-bottom: 0;">Hr Department</h3>
//                    <p style="margin-bottom: 0;
//                        margin-top: 0;">__________________________________</p>
//                    <p style="margin-bottom: 5px;
//                        margin-top: 5px;">T (84-8) 39202555 -ext:111</p>
//                    <p style="margin-bottom: 5px;
//                        margin-top: 5px;">F (84-8) 39204095</p>
//                    <p style="margin-bottom: 5px;
//                        margin-top: 5px;">E <a href="mailto:hr@oppo-aed.vn">hr@oppo-aed.vn</a></p>
//                    <p style="margin-bottom: 5px;
//                        margin-top: 5px;">OPPO Science & Technology Co.,Ltd</p>
//					<p style="margin-bottom: 5px;
//                        margin-top: 5px;">12th Floor, Lim II Tower.</p>
//                    <p style="margin-bottom: 5px;
//                        margin-top: 5px;">No. 62A CMT8 St., Ward 6, Dist. 3, Ho Chi Minh City.</p>
//                    <p style="margin-bottom: 0;
//                        margin-top: 0;">__________________________________</p>
//                    <p style="margin-bottom: 5px;
//                        margin-top: 0;"><a href="http://www.oppomobile.vn/">www.oppomobile.vn</a></p>';
    private $hr_signature = '<table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" style="font-family: &quot;segoe ui&quot;, &quot;lucida sans&quot;, sans-serif; border-collapse: collapse;">'
            . '<tbody style="">'
            . '<tr style="height: 29.4pt;">'
            . '<td width="374" style="width: 3.9in; border-top: none; border-right: none; border-left: none; border-image: initial; border-bottom: 1pt dashed black; padding: 0in 5.4pt; height: 29.4pt;">'
            . '<p class="MsoNormal" style="margin: 0in 0in 0.0001pt; font-size: 11pt; font-family: calibri, sans-serif;">'
            . '<span style="font-family: arial, sans-serif; color: rgb(0, 146, 93);">HR Department<br style=""></span>'
            . '<span style="font-size: 10pt; font-family: arial, sans-serif;">Giá trị cốt lõi – “Bổn phận cao hơn chữ tín”</span>'
            . '<span style="font-family: arial, sans-serif; color: rgb(0, 146, 93);"></span>'
            . '</p>'
            . '</td>'
            . '</tr>'
            . '<tr style="height: 37.5pt;">'
            . '<td width="374" style="width: 3.9in; border-top: none; border-right: none; border-left: none; border-image: initial; border-bottom: 1pt dashed black; padding: 0in 5.4pt; height: 37.5pt;">'
            . '<p class="MsoNormal" style="margin: 0in 0in 0.0001pt; font-size: 11pt; font-family: calibri, sans-serif;">'
            . '<span style="font-size: 10pt; font-family: arial, sans-serif;">T:&nbsp;'
            . '<span class="Object" id="OBJ_PREFIX_DWT357_com_zimbra_phone" style="color: rgb(0, 90, 149); cursor: pointer;">'
            . '<a href="callto:(84-8) 3920 2555" style="color: rgb(0, 90, 149); text-decoration-line: none; cursor: pointer;" target="_blank">(84-8) 3920 2555</a></span>&nbsp;- Ext: 111</span></p><p class="MsoNormal" style="margin: 0in 0in 0.0001pt; font-size: 11pt; font-family: calibri, sans-serif;"><span style="font-size: 10pt; font-family: arial, sans-serif;">F:&nbsp;<span class="Object" id="OBJ_PREFIX_DWT358_com_zimbra_phone" style="color: rgb(0, 90, 149); cursor: pointer;"><a href="callto:(84-8) 3920 4095" style="color: rgb(0, 90, 149); text-decoration-line: none; cursor: pointer;" target="_blank">(84-8) 3920 4095</a></span><br style="">E:&nbsp;<span class="Object" id="OBJ_PREFIX_DWT359_ZmEmailObjectHandler" style="color: rgb(0, 90, 149); cursor: pointer;"><span class="Object" role="link" id="OBJ_PREFIX_DWT45_ZmEmailObjectHandler">hr@oppo-aed.vn</span></span></span></p></td></tr><tr style="height: 38.9pt;"><td width="374" style="width: 3.9in; border-top: none; border-right: none; border-left: none; border-image: initial; border-bottom: 1pt dashed black; padding: 0in 5.4pt; height: 38.9pt;"><p class="MsoNormal" style="margin: 0in 0in 0.0001pt; font-size: 11pt; font-family: calibri, sans-serif;"><b style=""><span style="font-size: 10pt; font-family: arial, sans-serif; color: rgb(4, 106, 56);">OPPO Authorized Exclusive Distributor <br> CÔNG TY CỔ PHẦN KỸ THUẬT & KHOA HỌC VĨNH KHANG</span></b><span style="font-size: 10pt; font-family: arial, sans-serif;"><br style="">12<sup style="">th</sup><b style="">&nbsp;</b>Floor, Lim II Tower<br style="">No. 62A CMT8 St., Dist. 3, HCMC, VN</span></p></td></tr><tr style="height: 16.5pt;"><td width="374" style="width: 3.9in; padding: 0in 5.4pt; height: 16.5pt;"><p class="MsoNormal" style="margin: 0in 0in 0.0001pt; font-size: 11pt; font-family: calibri, sans-serif;"><span style="color: rgb(31, 73, 125);"><span class="Object" id="OBJ_PREFIX_DWT360_com_zimbra_url" style="color: rgb(0, 90, 149); cursor: pointer;"><span class="Object" role="link" id="OBJ_PREFIX_DWT46_com_zimbra_url"><a href="https://www.oppo.com/vn/" target="_blank" style="color: rgb(0, 90, 149); text-decoration-line: none; cursor: pointer;"><span style="font-size: 10pt; font-family: arial, sans-serif; color: rgb(0, 146, 93);">www.oppo.com/vn</span></a></span></span></span></p></td></tr></tbody></table>';
    private $_wish        = array(
        'Thay mặt BCH Công Đoàn & BGĐ Công Ty, Chúc bạn Sinh nhật đầy áp yêu thuơng và tiếng cười, thêm tuổi, thêm hạnh phúc, thêm nhiều niềm vui nhé. Happy Birthday!',
        'Thay mặt BCH Công Đoàn & BGĐ Công Ty, Chúc bạn có những phút giây thật tuyệt vời bên bạn bè và người thân trong ngày quan trọng này. Hi vọng bạn luôn thành công và hạnh phúc trong cuộc sống. Happy Birthday!',
        'Thay mặt BCH Công Đoàn & BGĐ Công Ty gửi đến bạn: Hôm nay không như ngày hôm qua, hôm nay là một ngày đặc biệt, là ngày mà một thiên thần đáng yêu đã có mặt trên thế giới này. Luôn mỉm cười và may mắn nhé.',
        'Thay mặt BCH Công Đoàn & BGĐ Công Ty, Chúc mọi điều ước trong ngày sinh nhật của bạn đều trở thành hiện thực, hãy thổi nến trên bánh sinh nhật để ước mơ được nhiệm màu.',
        'Nhân dịp sinh nhật của bạn, thay mặt BCH Công Đoàn & BGĐ Công Ty chúc bạn luôn tươi khỏe, trẻ đẹp. Cầu mong những gì may mắn nhất, tốt đẹp nhất và hạnh phúc nhất sẽ đến với bạn trong tuổi mới.',
    );

    const ALL     = 1;
    const ONLY_PG = 2;
    const NO_PG   = 3;

    public function init() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }

    public function birthdayAction() {
        /////////////////////////
        /////// CONFIG
        /////////////////////////
        error_reporting(~E_ALL);
        ini_set("display_error", 0);
        set_time_limit(0);

        $app_config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');

        $config = array(
            'auth'     => $app_config->mail->smtp->auth,
            'username' => $app_config->mail->smtp->user2,
            'password' => $app_config->mail->smtp->pass2,
            'port'     => $app_config->mail->smtp->port,
            'ssl'      => $app_config->mail->smtp->ssl
        );

        // Thay mặt cho BCH Công đoàn và BGĐ Công ty
        $wish = $this->_wish;

        /////////////////////////
        /////// GET STAFFs HAVING BIRTHDAY TODAY
        /////////////////////////
        $QTeam   = new Application_Model_Team();
        $QArea   = new Application_Model_Area();
        $QStaff  = new Application_Model_Staff();
        $QRegion = new Application_Model_RegionalMarket();
        $QStaff  = new Application_Model_Staff();

        $team_cache = $QTeam->get_all_cache();
        $areas      = $QArea->get_cache();

        $id = $this->getRequest()->getParam('id');
		// $id = 23253;

        $staffs = $this->_getStaffBirthday($id, self::NO_PG);
		
        if (!isset($staffs) || !$staffs)
            exit;

        /////////////////////////
        /////// SEND EMAIL
        /////////////////////////
        foreach ($staffs as $k => $staff) {
            $transport = new Zend_Mail_Transport_Smtp($app_config->mail->smtp->host, $config);

            $mail = new My_Mail($app_config->mail->smtp->charset);

            $mail->setFrom($app_config->mail->smtp->from2, $app_config->mail->smtp->from2);

            $fname = trim($this->mb_ucwords($staff['firstname'] . ' ' . $staff['lastname']));
            $mail->setSubject('Happy Birthday ' . $fname);

            $d = explode('/', $staff['dob']);
            $d = $d[0] . '/' . $d[1];



            /*
              TẠO HÌNH ẢNH
             */
            // Set the content-type
            //header('Content-Type: image/png');
            // Create the image
            // $im = imagecreatefromjpeg(APPLICATION_PATH . "/../public/photo/mail/birthday/thiep-2017-0" . rand(1, 4) . ".jpg");
            $im = imagecreatefromjpeg(APPLICATION_PATH . "/../public/photo/mail/birthday/thiep-2019-0" . rand(1, 4) . ".jpg");

            // Create some colors
            $white = imagecolorallocate($im, 255, 255, 255);
            $grey  = imagecolorallocate($im, 128, 128, 128);
            $black = imagecolorallocate($im, 0, 0, 0);

            $green = imagecolorallocate($im, 0, 139, 86);
            $pink  = imagecolorallocate($im, 232, 120, 139);

            // The text to draw
            $name     = $fname;
            $birthday = $d;

            $deparment = "Bộ phận: " . strtoupper($this->mb_ucwords(@$team_cache[$staff['department']]['name']));
            $team      = "Team: " . strtoupper($this->mb_ucwords(@$team_cache[$staff['team']]['name']));
            $title     = 'Chức vụ: ' . strtoupper($this->mb_ucwords(@$team_cache[$staff['title']]['name']));

            // Replace path by your own font path
            $font   = '/var/www/html/center/public/css/font/BAUHAUSM.ttf';
            $font_b = '/var/www/html/center/public/css/font/BAUHAUHB.ttf';

            // Add the text
            imagettftext($im, 20, 0, (450 - (mb_strlen($name, "UTF-8") * 14 / 2)), 225, $green, $font_b, $name);
            imagettftext($im, 24, 0, (445 - (mb_strlen($birthday, "UTF-8") * 14 / 2)), 260, $green, $font_b, $birthday);

            imagettftext($im, 20, 0, (450 - (mb_strlen($deparment, "UTF-8") * 13 / 2)), 299, $pink, $font, $deparment);
            imagettftext($im, 20, 0, (450 - (mb_strlen($team, "UTF-8") * 13 / 2)), 332, $pink, $font, $team);
            imagettftext($im, 20, 0, (450 - (mb_strlen($title, "UTF-8") * 13 / 2)), 365, $pink, $font, $title);


            $rand_ = rand(1, 5);
            if ($rand_ == 1) {
                $descreption1 = '"Gia đình OPPO chúc bạn tuổi mới';
                $descreption2 = 'hạnh phúc, vui vẻ và luôn trẻ khỏe"';

                imagettftext($im, 20, 0, (475 - (mb_strlen($descreption1, "UTF-8") * 14.5 / 2)), 410, $green, $font_b, $descreption1);
                imagettftext($im, 20, 0, (495 - (mb_strlen($descreption2, "UTF-8") * 14.5 / 2)), 437, $green, $font_b, $descreption2);
            } elseif ($rand_ == 2) {
                $descreption1 = '"Gia đình OPPO chúc bạn luôn tươi trẻ';
                $descreption2 = 'và thành công trong cuộc sống"';

                imagettftext($im, 20, 0, (475 - (mb_strlen($descreption1, "UTF-8") * 14.5 / 2)), 410, $green, $font_b, $descreption1);
                imagettftext($im, 20, 0, (475 - (mb_strlen($descreption2, "UTF-8") * 14.5 / 2)), 437, $green, $font_b, $descreption2);
            } elseif ($rand_ == 3) {
                $descreption1 = '"Gia đình OPPO cùng bạn chào đón tuổi mới';
                $descreption2 = 'với nhiều niềm vui, hạnh phúc';
                $descreption3 = 'và thành công"';

                imagettftext($im, 20, 0, (475 - (mb_strlen($descreption1, "UTF-8") * 14.5 / 2)), 405, $green, $font_b, $descreption1);
                imagettftext($im, 20, 0, (505 - (mb_strlen($descreption2, "UTF-8") * 14.5 / 2)), 432, $green, $font_b, $descreption2);
                imagettftext($im, 20, 0, (455 - (mb_strlen($descreption3, "UTF-8") * 14.5 / 2)), 459, $green, $font_b, $descreption3);
            } elseif ($rand_ == 4) {
                $descreption1 = '"Thêm tuổi mới, đại gia đình OPPO chúc bạn';
                $descreption2 = 'thật nhiều sức khỏe';
                $descreption3 = 'và niềm vui trong cuộc sống"';

                imagettftext($im, 20, 0, (480 - (mb_strlen($descreption1, "UTF-8") * 14.5 / 2)), 405, $green, $font_b, $descreption1);
                imagettftext($im, 20, 0, (445 - (mb_strlen($descreption2, "UTF-8") * 14.5 / 2)), 432, $green, $font_b, $descreption2);
                imagettftext($im, 20, 0, (452 - (mb_strlen($descreption3, "UTF-8") * 14.5 / 2)), 459, $green, $font_b, $descreption3);
            } elseif ($rand_ == 5) {
                $descreption1 = '"Nhân ngày sinh nhật, đại gia đình OPPO';
                $descreption2 = 'mến chúc bạn thật nhiều may mắn';
                $descreption3 = 'thành công và hạnh phúc"';

                imagettftext($im, 20, 0, (475 - (mb_strlen($descreption1, "UTF-8") * 14.5 / 2)), 405, $green, $font_b, $descreption1);
                imagettftext($im, 20, 0, (475 - (mb_strlen($descreption2, "UTF-8") * 14.5 / 2)), 432, $green, $font_b, $descreption2);
                imagettftext($im, 20, 0, (475 - (mb_strlen($descreption3, "UTF-8") * 14.5 / 2)), 459, $green, $font_b, $descreption3);
            }



            // Using imagepng() results in clearer text compared with imagejpeg()

            $date_cur     = date('Y-m-d');
            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                    DIRECTORY_SEPARATOR . 'birthday' . DIRECTORY_SEPARATOR . $date_cur . DIRECTORY_SEPARATOR;

            if (!is_dir($uploaded_dir)) {
                $old = umask(0);
                @mkdir($uploaded_dir, 0777, true);
                umask($old);
            }
            imagepng($im, $uploaded_dir . $staff['id'] . '.png');
            imagedestroy($im);
            /*
              END TẠO HÌNH ẢNH
             */




            //end lưu nội dung thành hình ảnh
            $img_birthday = "<div><table><tr><td><img src='" . HOST . "photo/birthday/" . $date_cur . "/" . $staff['id'] . ".png'/></td></tr></table></div>";
            $img_birthday .= $this->hr_signature;

            $mail->setBodyHtml($img_birthday);
////////////khuc nay
//            $mailto = array(); // Config this
//
//            // lấy khu vực để chọn email gửi tới
//            $region_id = (isset($staff['regional_market']) && $staff['regional_market']) ? $staff['regional_market'] : 0;
//            $region = $QRegion->find($region_id);
//            $region = $region->current();
//
//            if ($region) {
//                $area = $QArea->find($region['area_id']);
//                $area = $area->current();
//
//                if ($area) {
//
//                    if ( $area['id'] == HCMC4
//                            && isset($staff['is_officer']) && $staff['is_officer']) {
//                        $mailto[] = EMAIL_HCM_OFFICE; // gửi nv văn phòng HCM
//
//                    } elseif ( !empty($area['email']) &&  WARRANTY_CENTER != $staff['department']) {
//                        $mailto[] = $area['email']; // email khu vực
//                    }
//                }
//            }
//
//            // email cá nhân
//            if (!empty($staff['email'])) {
//                $mailto[] = $staff['email'];
//            }
//
//            // nếu là asm thì gửi vào EMAIL_ASM
//            if ( isset($staff['group_id']) && in_array($staff['group_id'], array(ASM_ID, ASMSTANDBY_ID)) ) {
//                $mailto[] = EMAIL_ASM;
//
//            } elseif ( isset($staff['group_id']) && $staff['group_id'] == SALES_ADMIN_ID ) {
//                $mailto[] = EMAIL_SALESADMIN;
//            }
//
//            if ( !empty( $team_cache[ $staff['team'] ]['email'] ) ) {
//                $mailto[] = $team_cache[ $staff['team'] ]['email'];
//
//            } elseif ( !empty( $team_cache[ $staff['department'] ]['email'] ) ) {
//                $mailto[] = $team_cache[ $staff['department'] ]['email'];
//            }
//
//            $mailto[] = 'thehien.hoang@oppo-aed.vn';
//            $mailto[] = 'yen.dao@oppo-aed.vn';
//            $mailto[] = 'an.nguyen@oppo-aed.vn';
//            
//            if (isset($staff['is_officer']) && $staff['is_officer'])
//                $mailto[] = 'yennhi.pham@oppo-aed.vn';
//
//            // for debug only
//            foreach ($mailto as $key => $value) {
//                echo date('Y-m-d H:i:s') .": ".$value."\n";
//
//            }//

            $list_email = $this->getMail($staff['email'], 1);

            if (isset($list_email) && count($list_email)) {
                $mailTo  = array();
                $mailBcc = array();

                if (!empty($list_email['to'])) {
                    $mailTo[] = $list_email['to'];
                    $mail->addTo($mailTo);
                }

                if (!empty($list_email['cc'])) {
                    if ($staff['code'] == '14020052') {
                        
                    } else {
                        $mail->addCc($list_email['cc']);
                    }
                }

                if (!empty($list_email['bcc'])) {
                    $mailBcc = explode(",", $list_email['bcc']);
                    $mail->addBcc($mailBcc);
                }

                $r = $mail->send($transport);
                sleep(1);
            }
        }

        exit;
    }

    public function welcomestaffAction() {


        /////////////////////////
        /////// CONFIG
        /////////////////////////
        error_reporting(~E_ALL);
        ini_set("display_error", 0);
        set_time_limit(0);
//        $mailfrom = 'hr@oppo-aed.vn'; // Config this
//        $mailfromname = "HR"; // Config this

        $app_config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
        $font       = '/var/www/html/center/public/css/font/BAUHAUSM.ttf';
        $font_b     = '/var/www/html/center/public/css/font/BAUHAUHB.ttf';
        $config     = array(
            'auth'     => $app_config->mail->smtp->auth,
            'username' => $app_config->mail->smtp->user2,
            'password' => $app_config->mail->smtp->pass2,
            'port'     => $app_config->mail->smtp->port,
            'ssl'      => $app_config->mail->smtp->ssl
        );

//        $subject = 'Chào đón nhân viên mới!';
        /////////////////////////
        /////// GET STAFFs HAVING BIRTHDAY TODAY
        /////////////////////////
        $QRegion               = new Application_Model_RegionalMarket();
        $QTeam                 = new Application_Model_Team();
        $team_cache            = $QTeam->get_all_cache();
        $regional_market_cache = $QRegion->get_cache_all();

        $db = Zend_Registry::get('db');

        $sql = "SELECT
                    s.*,t.access_group
                FROM
                    staff s
                join team t on s.title=t.id
                WHERE
                    s.id NOT IN(
                        SELECT
                            staff_id
                        FROM
                            welcome_email
                    )
                AND s.email IS NOT NULL
                AND s.email <> ''
                AND s.title <> 0
                AND s.title <> ''
                AND s.title IS NOT NULL
                AND t.access_group NOT IN (" . implode(',', array(PGPB_ID, SALES_ID, LEADER_ID)) . ")
                AND t.access_group <> 0
                AND t.access_group IS NOT NULL
                AND s.is_officer = 1
                AND s.joined_at <= '" . date('Y-m-d') . "'
                AND s.joined_at >'2017-01-01'
                AND s.off_date IS NULL
                AND s.code NOT IN ('14050110', '14050124', '14070095','18090112','19090303',
                    '14070096', '14050096', '14080335', '14080334',
                    '1308HN142', '14090369', '14090370', '15070090', '00000002', '15080244','14110314','17100091','18020042','18050051','18060276', '18110067')
                ";

//        $staffs = $db->query($sql);
        $stmt   = $db->prepare($sql);
        $stmt->execute();
        $staffs = $stmt->fetchAll();

        // find to-send email per staff        
        foreach ($staffs as $k => $staff) {

            if (!$staff)
                continue;
            $list_email = $this->getMail($staff['email'], 2);
            if (!empty($list_email) && count($list_email)) {
                $transport = new Zend_Mail_Transport_Smtp($app_config->mail->smtp->host, $config);
                $mail      = new My_Mail($app_config->mail->smtp->charset);
                $mail->setFrom($app_config->mail->smtp->from2, $app_config->mail->smtp->from2);
//                $fname = trim($this->mb_ucwords($staff['firstname'] . ' ' . $staff['lastname']));
                $mail->setSubject('GIA ĐÌNH OPPO CHÀO ĐÓN NHÂN VIÊN MỚI!');

                /*
                  TẠO HÌNH ẢNH
                 */
                // Create the image
                $im = imagecreatefromjpeg(APPLICATION_PATH . "/../public/photo/mail/birthday/welcome-new-oppper_email-2019-0" . rand(1, 4) . ".jpg");
             
            
                $green = imagecolorallocate($im, 0, 139, 86);
                $pink  = imagecolorallocate($im, 232, 120, 139);

                // The text to draw
                $name           = $this->mb_ucwords($staff['firstname'] . ' ' . $staff['lastname']);
                $deparment      = 'Bộ phận : ' . @$team_cache[$staff['department']]['name'];
                $team           = 'Team : ' . @$team_cache[$staff['team']]['name'];
                $title          = 'Chức vụ : ' . @$team_cache[$staff['title']]['name'];
                $province_staff = 'Tỉnh : ' . (
                        isset($regional_market_cache[$staff['regional_market']]['name']) ? ( $regional_market_cache[$staff['regional_market']]['name'] ) : ''
                        );
                $email_staff    = 'Email : ' . $staff['email'];

                $text1 = "______";
                $text2 = "____";
                $text3 = "_____";
                $text4 = "___";
                $text5 = "____";

                imagettftext($im, 20, 0, (560 - (mb_strlen($name, "UTF-8") * 11 / 2)), 250, $green, $font_b, $name);
                imagettftext($im, 16, 0, 405, 285, $green, $font, $deparment);
                imagettftext($im, 16, 0, 432, 310, $green, $font, $team);
                imagettftext($im, 16, 0, 415, 335, $green, $font, $title);
                imagettftext($im, 16, 0, 445, 360, $green, $font, $province_staff);
                imagettftext($im, 16, 0, 433, 385, $green, $font, $email_staff);

                imagettftext($im, 20, 0, 405, 284, $green, $font, $text1);
                imagettftext($im, 20, 0, 432, 309, $green, $font, $text2);
                imagettftext($im, 20, 0, 417, 334, $green, $font, $text3);
                imagettftext($im, 20, 0, 445, 359, $green, $font, $text4);
                imagettftext($im, 20, 0, 430, 384, $green, $font, $text5);

                // Using imagepng() results in clearer text compared with imagejpeg()
                $date_cur     = date('Y-m-d');
                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                        DIRECTORY_SEPARATOR . 'birthday' . DIRECTORY_SEPARATOR . $date_cur . DIRECTORY_SEPARATOR;

                if (!is_dir($uploaded_dir)) {
                    $old = umask(0);
                    @mkdir($uploaded_dir, 0777, true);
                    umask($old);
                }
                imagepng($im, $uploaded_dir . $staff['id'] . '_wellcome.png');
                imagedestroy($im);
                /*
                  END TẠO HÌNH ẢNH
                 */

                $img_wellcome = "<div><table><tr><td><img src='" . HOST . "photo/birthday/" . $date_cur . "/" . $staff['id'] . "_wellcome.png'/></td></tr></table></div>";
                $img_wellcome .= $this->hr_signature;

                $mail->setBodyHtml($img_wellcome);

                ///////////////////////////////////////
                $have_staff = 0;
                $sql        = "INSERT INTO welcome_email(staff_id) VALUES  (" . $staff['id'] . ")";
                $have_staff = $db->query($sql);
                if ($have_staff) {
                    $mailTo  = array();
                    $mailBcc = array();

                    if (!empty($list_email['to'])) {
                        $mailTo[] = $list_email['to'];
                        $mail->addTo($mailTo);
                    }

                    if (!empty($list_email['cc'])) {
                        $mail->addCc($list_email['cc']);
                    }

                    if (!empty($list_email['bcc'])) {
                        $mailBcc = explode(",", $list_email['bcc']);
                        $mail->addBcc($mailBcc);
                    }
                    $mail->send($transport);
                    sleep(1);
                    $this->welcomeNew($staff['id']);
                }
            }
        }
        $font   = '/var/www/html/center/public/css/font/BAUHAUSM.ttf';
        $font_b = '/var/www/html/center/public/css/font/BAUHAUHB.ttf';
        exit;
    }

    public function welcomeAction() {
        die;
        /////////////////////////
        /////// CONFIG
        /////////////////////////
//        error_reporting(~E_ALL);
//        ini_set("display_error", 0);
//        set_time_limit(0);
//        $mailfrom = 'hr@oppo-aed.vn'; // Config this
//        $mailfromname = "HR"; // Config this
//
//        $app_config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
//
//        $config = array(
//            'auth' => $app_config->mail->smtp->auth,
//            'username' => $app_config->mail->smtp->user2,
//            'password' => $app_config->mail->smtp->pass2,
//            'port' => $app_config->mail->smtp->port,
//            'ssl' => $app_config->mail->smtp->ssl
//        );
//
//        $subject = 'Chào đón nhân viên mới!';
//        $has_mail = 0;
//        /////////////////////////
//        /////// GET STAFFs HAVING BIRTHDAY TODAY
//        /////////////////////////
//        $QRegion = new Application_Model_RegionalMarket();
//        $QTeam = new Application_Model_Team();
//        $QArea = new Application_Model_Area();
//        $QStaff = new Application_Model_Staff();
//
//        $team_cache = $QTeam->get_all_cache();
//
//        $areas = $QArea->get_cache();
//        $regional_market_cache = $QRegion->get_cache_all();
//
//        $db = Zend_Registry::get('db');
//
//        $sql = "SELECT
//                    *
//                FROM
//                    staff s
//                WHERE
//                    s.id NOT IN(
//                        SELECT
//                            staff_id
//                        FROM
//                            welcome_email
//                    )
//                AND s.email IS NOT NULL
//                AND s.email <> ''
//                AND s.title <> 0
//                AND s.title <> ''
//                AND s.title IS NOT NULL
//                AND s.group_id NOT IN (" . implode(',', array(PGPB_ID, SALES_ID, LEADER_ID)) . ")
//                AND s.group_id <> 0
//                AND s.group_id IS NOT NULL
//                AND s.is_officer = 1
//                AND s.joined_at <= '" . date('Y-m-d') . "'
//                AND s.joined_at >'2017-01-01'
//                AND s.off_date IS NULL
//                AND s.code NOT IN ('14050110', '14050124', '14070095',
//                    '14070096', '14050096', '14080335', '14080334',
//                    '1308HN142', '14090369', '14090370', '15070090', '00000002', '15080244','14110314')
//                ";
//
//        $staffs = $db->query($sql);
//        $to_send_emails = array();
//
//        $sql = "INSERT INTO welcome_email(staff_id) VALUES ";
//
//        // find to-send email per staff
//        foreach ($staffs as $k => $staff) {
//            if (trim($staff['email']) == '')
//                continue;
//
//            $mailto = array(); // Config this
//
//            $region_id = (isset($staff['regional_market']) && $staff['regional_market']) ? $staff['regional_market'] : 0;
//            $region = $QRegion->find($region_id);
//            $region = $region->current();
//
//            if ($region) {
//                $area = $QArea->find($region['area_id']);
//                $area = $area->current();
//
//                if ($area) {
//                    if ($staff['team'] == TRAINING_TEAM) {
//                        if ($area['id'] == HCMC4) {
//                            $mailto[] = EMAIL_HCM_OFFICE; // gửi nv văn phòng HCM
//                        }
//                    } else {
//                        if ($area['id'] == HCMC4 && isset($staff['is_officer']) && $staff['is_officer'] && !in_array($staff['department'], array(WARRANTY_CENTER, SERVICE_CENTER, CALL_CENTER))
//                        ) {
//                            $mailto[] = EMAIL_HCM_OFFICE; // gửi nv văn phòng HCM
//                        } elseif (!empty($area['email']) && WARRANTY_CENTER != $staff['department']) {
//                            $mailto[] = $area['email']; // email khu vực
//                        }
//                    }
//                }
//
//                // email cá nhân
//                if (!empty($staff['email'])) {
//                    $mailto[] = $staff['email'];
//                }
//
//                // nếu là asm thì gửi vào EMAIL_ASM
//                if (isset($staff['group_id']) && $staff['group_id'] == ASM_ID) {
//                    $mailto[] = EMAIL_ASM;
//                } elseif (isset($staff['group_id']) && $staff['group_id'] == SALES_ADMIN_ID) {
//                    $mailto[] = EMAIL_SALESADMIN;
//                }
//
//                if (!empty($team_cache[$staff['team']]['email'])) {
//                    $mailto[] = $team_cache[$staff['team']]['email'];
//                } elseif (!empty($team_cache[$staff['department']]['email'])) {
//                    $mailto[] = $team_cache[$staff['department']]['email'];
//                }
//
//                $mailto[] = 'yen.dao@oppo-aed.vn';
//                $mailto[] = 'hr@oppo-aed.vn';
//                $mailto[] = 'truongtoan.dinh@oppo-aed.vn';
//
//                $mailto = array_unique($mailto);
//
//                $to_send_emails[$staff['id']] = $mailto;
//            }
//
////            $sql .= " (".$staff['id']."),";
//        }
//
//        // list to-send emails
//        $to_send_group = array();
//
//        foreach ($to_send_emails as $staff_email => $mailto_list) {
//            if (!$this->array_in_array($mailto_list, $to_send_group)) {
//                $to_send_group[] = array('group' => $mailto_list);
//            }
//        }
//
//        // group staffs by to-send emails
//        foreach ($to_send_group as $key => $item) {
//            foreach ($to_send_emails as $staff_email => $mailto_list) {
//                if (0 == count(array_diff($item['group'], $mailto_list)) && 0 == count(array_diff($mailto_list, $item['group']))) {
//
//                    if (!isset($to_send_group[$key]['staff'])) {
//                        $to_send_group[$key]['staff'] = array();
//                    }
//
//                    $to_send_group[$key]['staff'][] = $staff_email;
//                }
//            }
//        }
//
////        $sql = trim($sql, ',');
//        /////////////////////////
//        /////// SEND EMAIL
//        /////////////////////////
//        $font = '/var/www/html/center/public/css/font/BAUHAUSM.ttf';
//        $font_b = '/var/www/html/center/public/css/font/BAUHAUHB.ttf';
//
//        foreach ($to_send_group as $group) {
//
//            if (count($group['staff']) == 0) {
//                continue;
//            }
//
//            foreach ($group['staff'] as $staff_id) {
//
//                $transport = new Zend_Mail_Transport_Smtp($app_config->mail->smtp->host, $config);
//
//                $mail = new My_Mail($app_config->mail->smtp->charset);
//
//                $mail->setFrom($app_config->mail->smtp->from2, $app_config->mail->smtp->from2);
//
//                $fname = trim($this->mb_ucwords($staff['firstname'] . ' ' . $staff['lastname']));
//                $mail->setSubject('GIA ĐÌNH OPPO CHÀO ĐÓN NHÂN VIÊN MỚI!');
//                //
//
//                $staff = $QStaff->find($staff_id);
//                $staff = $staff->current();
//
//                if (!$staff)
//                    continue;
//
//
//
//
//                /*
//                  TẠO HÌNH ẢNH
//                 */
//                // Create the image
//                $im = imagecreatefromjpeg(HOST . "photo/mail/birthday/new_oppoer_0" . rand(1, 4) . ".jpg");
//
//                $green = imagecolorallocate($im, 0, 139, 86);
//                $pink = imagecolorallocate($im, 232, 120, 139);
//
//                // The text to draw
//                $name = $this->mb_ucwords($staff['firstname'] . ' ' . $staff['lastname']);
//                $deparment = 'Bộ phận : ' . @$team_cache[$staff['department']]['name'];
//                $team = 'Team : ' . @$team_cache[$staff['team']]['name'];
//                $title = 'Chức vụ : ' . @$team_cache[$staff['title']]['name'];
//                $province_staff = 'Tỉnh : ' . (
//                        isset($regional_market_cache[$staff['regional_market']]['name']) ? ( $regional_market_cache[$staff['regional_market']]['name'] ) : ''
//                        );
//                $email_staff = 'Email : ' . $staff['email'];
//
//                $text1 = "______";
//                $text2 = "____";
//                $text3 = "_____";
//                $text4 = "___";
//                $text5 = "____";
//
//                imagettftext($im, 20, 0, (560 - (mb_strlen($name, "UTF-8") * 11 / 2)), 250, $green, $font_b, $name);
//                imagettftext($im, 16, 0, 405, 285, $green, $font, $deparment);
//                imagettftext($im, 16, 0, 432, 310, $green, $font, $team);
//                imagettftext($im, 16, 0, 415, 335, $green, $font, $title);
//                imagettftext($im, 16, 0, 445, 360, $green, $font, $province_staff);
//                imagettftext($im, 16, 0, 433, 385, $green, $font, $email_staff);
//
//                imagettftext($im, 20, 0, 405, 284, $green, $font, $text1);
//                imagettftext($im, 20, 0, 432, 309, $green, $font, $text2);
//                imagettftext($im, 20, 0, 417, 334, $green, $font, $text3);
//                imagettftext($im, 20, 0, 445, 359, $green, $font, $text4);
//                imagettftext($im, 20, 0, 430, 384, $green, $font, $text5);
//
//                // Using imagepng() results in clearer text compared with imagejpeg()
//                $date_cur = date('Y-m-d');
//                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
//                        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
//                        DIRECTORY_SEPARATOR . 'birthday' . DIRECTORY_SEPARATOR . $date_cur . DIRECTORY_SEPARATOR;
//
//                if (!is_dir($uploaded_dir)) {
//                    $old = umask(0);
//                    @mkdir($uploaded_dir, 0777, true);
//                    umask($old);
//                }
//                imagepng($im, $uploaded_dir . $staff_id . '_wellcome.png');
//                imagedestroy($im);
//                /*
//                  END TẠO HÌNH ẢNH
//                 */
//
//                $img_wellcome = "<div><table><tr><td><img src='" . HOST . "photo/birthday/" . $date_cur . "/" . $staff_id . "_wellcome.png'/></td></tr></table></div>";
//                $img_wellcome .= $this->hr_signature;
//
//                $mail->setBodyHtml($img_wellcome);
//
//                ///////////////////////////////////////
//
//                $list_email = $this->getMail($staff['email'], 2);
//
//                if (isset($list_email) && count($list_email)) {
//                    $sql .= " (" . $staff['id'] . "),";
//                    $has_mail = 1;
//                    $mailTo = array();
//                    $mailBcc = array();
//
//                    if (!empty($list_email['to'])) {
//                        $mailTo[] = $list_email['to'];
//                        $mail->addTo($mailTo);
//                    }
//
//                    if (!empty($list_email['cc'])) {
//                        $mail->addCc($list_email['cc']);
//                    }
//
//                    if (!empty($list_email['bcc'])) {
//                        $mailBcc = explode(",", $list_email['bcc']);
//                        $mail->addBcc($mailBcc);
//                    }
//
//                    $r = $mail->send($transport);
//                    sleep(1);
//
//                    $this->welcomeNew($staff_id);
//                }
//
//                ///////////////////////////////////////
////                $mailto = $group['group']; // Config this
////
////                // for debug only
////                foreach ($mailto as $key => $value) {
////                    echo date('Y-m-d H:i:s') .": ".$value."\n";
////
////                }//
////                
////                if (isset($mailto) && count($mailto)) {
////                    $mail->addTo($mailto);
////
////                    $r = $mail->send($transport);
////                    sleep(1);
////                }
//            }
//        }
//        $sql = trim($sql, ',');
//        if ($has_mail) {
//
//            $db->query($sql);
//        }
//
//
//        exit;
    }

    private function sendmail($to, $subject, $maildata, $image = '') {
        include_once APPLICATION_PATH . '/../library/phpmail/class.phpmailer.php';
        $app_config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
        $config     = $app_config->mail->smtp;

        $mailfrom     = $config->user2;
        $mailfromname = $config->fromname2;
        $mail         = new PHPMailer();

        $body = $maildata; // nội dung email
        $body = eregi_replace("[\]", '', $body);

        $mail->IsHTML();

        $mail->IsSMTP();
        $mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
        // 1 = errors and messages
        // 2 = messages only
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
        $mail->SMTPSecure = $config->ssl;                 // sets the prefix to the servier // Config this
        $mail->Host       = $config->host;      // sets GMAIL as the SMTP server // Config this
        $mail->Port       = $config->port;                   // set the SMTP port for the GMAIL server // Config this
        $mail->Username   = $config->user2;  // GMAIL username // Config this
        $mail->Password   = $config->pass2;            // GMAIL password // Config this

        $mail->From     = $mailfrom;
        $mail->FromName = $mailfromname;

        $mail->SetFrom($mailfrom, $mailfromname); //Định danh người gửi
        //$mail->AddReplyTo($mailfrom, $mailfromname); //Định danh người sẽ nhận trả lời

        $mail->Subject = $subject; //Tiêu đề Mail

        $mail->AltBody = "Để xem tin này, vui lòng bật chế độ hiển thị mã HTML!"; // optional, comment out and test
        // $mail->AddAttachment($image);
        if ($image != '')
            $mail->AddEmbeddedImage($image, 'embed_img');

        $mail->MsgHTML($body);

        if (is_array($to)) {
            foreach ($to as $key => $value) {
                $mail->AddAddress($value, ''); //Gửi tới ai ?
            }
        } else {
            $mail->AddAddress($to, ''); //Gửi tới ai ?
        }


        if (!$mail->Send()) {
            echo date('Y-m-d H:i:s') . " - Failed\n";
        } else {
            echo date('Y-m-d H:i:s') . " - Done\n"; //DEBUG
        }
    }

    private function mb_ucwords($str) {
        $str = mb_convert_case($str, MB_CASE_TITLE, "UTF-8");
        return ($str);
    }

    private function data_uri($file, $mime) {
        return "data:$mime;base64," . base64_encode(file_get_contents($file));
    }

    private function array_in_array($needle, $haystack) {
        foreach ($haystack as $key => $value) {
            if (0 == count(array_diff($needle, $value['group'])) && 0 == count(array_diff($value['group'], $needle))) {
                return true;
            }
        }

        return false;
    }

    public function birthdayNotificationAction() {
        $QModel = new Application_Model_Notification();
        $this->_helper->viewRenderer->setNoRender(true);
        error_reporting(~E_ALL);
        ini_set("display_error", 0);
        ini_set("memory_limit", -1);
        set_time_limit(0);

        $staffs = $this->_getStaffBirthday(0, self::ONLY_PG);

        if (!isset($staffs) || !$staffs)
            exit;

        $userStorage = Zend_Auth::getInstance()->getStorage()->read();
        $db          = Zend_Registry::get('db');
        $db->beginTransaction();

        try {
            foreach ($staffs->toArray() as $k => $staff) {
                $fname   = trim($this->mb_ucwords($staff['firstname'] . ' ' . $staff['lastname']));
                $title   = 'Happy Birthday ' . $fname;
                $content = $this->_generateContent($staff);

                $data               = array(
                    'title'       => htmlspecialchars($title, ENT_QUOTES, "UTF-8"),
                    'content'     => htmlspecialchars($content, ENT_QUOTES, "UTF-8"),
                    'category_id' => 1,
                    'status'      => 1,
                    'type'        => 1,
                );
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['created_by'] = $userStorage ? $userStorage->id : 0;
                $id                 = $QModel->insert($data);

                $notifi_to = $this->_getNotifiTo2($staff);

                $params = array(
                    'staff'           => $notifi_to['staff'],
                    'title_objects'   => $notifi_to['title_objects'],
                    'notification_id' => $id,
                );

                $this->saveNotificationAction($params);
                unset($content);
                unset($title);
                unset($notifi_to);
                unset($staff);
                unset($fname);
            }

            $db->commit();

            return true;
        } catch (Exception $e) {
            $db->rollback();
            throw new Exception($e->getMessage());
        }
    }

    private function _getStaffBirthday($id = 0, $type = null) {
        $QStaff = new Application_Model_Staff();

        if ($id && intval($id) > 0) {
            $where = $QStaff->getAdapter()->quoteInto('id = ?', $id);
        } else {
            $where   = array();
            $date    = date('d/m/') . '%';
            $date2   = date('j/n/') . '%';
            $where[] = $QStaff->getAdapter()->quoteInto(sprintf("dob LIKE '%s' OR dob LIKE '%s'", $date, $date2), 1);

            if ($type) {
                switch ($type) {
                    case self::ALL:
                        break;
                    case self::ONLY_PG:
                        $where[] = $QStaff->getAdapter()->quoteInto("title IN (?)", My_Staff::pgTitleArray());
                        break;
                    case self::NO_PG:
                        $where[] = $QStaff->getAdapter()->quoteInto("title NOT IN (?)", My_Staff::pgTitleArray());
                        break;
                    default:
                        break;
                }
            }

            $where[] = $QStaff->getAdapter()->quoteInto('off_date IS NULL', 1);
        }

        return $QStaff->fetchAll($where);
    }

    private function _generateContent($staff) {
        $QTeam      = new Application_Model_Team();
        $team_cache = $QTeam->get_all_cache();
        $wish       = $this->_wish;

        $fname = trim($this->mb_ucwords($staff['firstname'] . ' ' . $staff['lastname']));
        $d     = explode('/', $staff['dob']);
        $d     = sprintf("%2d/%2d", $d[0], $d[1]);

        $header = '<div>';
        $header .= '<table style="width: 900px;border: 1px solid #aaa;">';
        $header .= "<tr>";
        $header .= '<td style="width: 440px;padding:10px;">';

        $header .= "<img src=\"http://center.opposhop.vn/photo/mail/birthday/" . rand(1, 10) . ".jpg\"
                    width=\"440px\" alt=\"happy birthday\" />";

        $header .= "</td>";
        $header .= "<td style=\"width: 440px;padding:10px;\">";

        $header .= "<h2 style=\"font-size: 26.0pt;
            font-family: 'Times New Roman','serif';
            color: red;
            font-style: italic;
            text-align:center;
            margin-top:10px;\">Happy birthday!!!</h2>";

        $header .= "<p style=\"font-size: 20.0pt;
            font-family: 'Times New Roman','serif';
            font-style: italic;
            color: #00b050;\">Nhân ngày sinh nhật, gửi lời chúc tốt đẹp đến bạn ^^</p>";

        $content = $header;

        $content .= "<p  style=\"font-size: 19.0pt;
            font-family: 'Times New Roman','serif';
            font-weight: bold;
            color: red;
            margin-bottom: 5px;
            margin-top: 10px;\">" . $this->mb_ucwords($fname) . "</p>";

        $content .= "<p style=\"font-size: 17.0pt;
            font-family: 'Times New Roman','serif';
            color: red;
            margin-bottom: 4px;
            margin-top: 5px;\">Bộ phận: " . $this->mb_ucwords(@$team_cache[$staff['department']]['name']) . "</p>";

        $content .= "<p style=\"font-size: 17.0pt;
            font-family: 'Times New Roman','serif';
            color: red;
            margin-bottom: 4px;
            margin-top: 5px;\">Team: " . $this->mb_ucwords(@$team_cache[$staff['team']]['name']) . "</p>";

        $content .= "<p style=\"font-size: 17.0pt;
            font-family: 'Times New Roman','serif';
            color: red;
            margin-bottom: 5px;
            margin-top: 5px;\">Chức vụ: " . $this->mb_ucwords(@$team_cache[$staff['title']]['name']) . "</p>";

        $content .= "<p style=\"font-size: 17.0pt;
            font-family: 'Times New Roman','serif';
            color: red;
            margin-bottom: 10px;
            margin-top: 5px;\">Sinh nhật: " . $d . "</p>";

        $footer = "<p style=\"font-size: 20.0pt;
            font-family: 'Times New Roman','serif';
            font-style: italic;
            color: #0070c0;
            margin-bottom: 5px;
            margin-top: 10px;
            text-align:justify;\">" . $wish[array_rand($wish, 1)] . "</p>";

        $footer .= "</td>";
        $footer .= "</tr>";
        $footer .= "</table>";
        $footer .= '</div>';

        $content .= $footer;
        return $content;
    }

    private function _getNotifiTo($staff) {
        $mailto    = array(); // Config this
        // lấy khu vực để chọn email gửi tới
        $QArea     = new Application_Model_Area();
        $QRegion   = new Application_Model_RegionalMarket();
        $region_id = (isset($staff['regional_market']) && $staff['regional_market']) ? $staff['regional_market'] : 0;
        $region    = $QRegion->find($region_id);
        $region    = $region->current();

        if ($region) {
            $area = $QArea->find($region['area_id']);
            $area = $area->current();

            if ($area && WARRANTY_CENTER != $staff['department']) {
                $mailto['area'] = array($area['id']); // email khu vực
            }
        }

        $mailto['staff']      = $staff['id'];
        $mailto['department'] = array(152); // SALES
        $mailto['team']       = array(75); // SALES
        $mailto['title']      = array(179, 181, 182, 183, 190, 191, 274);

        return $mailto;
    }

    private function _getNotifiTo2($staff) {
        $QArea     = new Application_Model_Area();
        $QRegion   = new Application_Model_RegionalMarket();
        $region_id = (isset($staff['regional_market']) && $staff['regional_market']) ? $staff['regional_market'] : 0;
        $region    = $QRegion->find($region_id);
        $region    = $region->current();


        if ($region) {
            $area = $QArea->find($region['area_id']);
            $area = $area->current();

            if ($area && WARRANTY_CENTER != $staff['department']) {
                $mailto['area_objects'] = array($area['id']); // email khu vực
            }
        }

        $mailto['staff'] = $staff['id'];

        //$mailto['title_objects']      = array(179, 181, 182, 183, 190, 191, 274);

        return $mailto;
    }

    public function saveNotificationAction($params) {

        $db                  = Zend_Registry::get('db');
        //     	List Staff_id with Title
        $user_ids_with_title = array();
        if (!empty($params['title_objects'])) {
            $select_ids_Title    = $db->select()->from(array('p' => 'staff'), array('p.id'));
            $select_ids_Title->where('p.title IN (?)', $params['title_objects'])
                    ->where('p.status = 1');
            $user_ids_with_title = $db->fetchAll($select_ids_Title);
        }

        //     	List Staff_id with area
        $user_ids_with_area = array();
        if (!empty($params['area_objects'])) {
            $select_ids_Area    = $db->select()->from(array('p' => 'staff'), array('p.id'));
            $select_ids_Area->joinInner(array('rm' => 'regional_market'), 'p.regional_market=rm.id', array());
            $select_ids_Area->where('rm.area_id IN (?)', $params['area_objects'])
                    ->where('p.status = 1');
            $user_ids_with_area = $db->fetchAll($select_ids_Area);
        }

        $list_user_ids = (array_merge($user_ids_with_title, $user_ids_with_area));

        $data = array();
        foreach ($list_user_ids as $k => $list_user_id) {
            $data[$k]['user_id']         = $list_user_id['id'];
            $data[$k]['notification_id'] = $params['notification_id'];
            $data[$k]['created_date']    = date('Y-m-d H:i:s');
        }

        if ($list_user_ids) {
            $this->insertToNotificationAccess($data);
        } else {
            throw new Exception("Cannot Save, Please choose Staff ");
        }
    }

    public static function insertToNotificationAccess($params) {
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
        $config = $config->toArray();
        $db_log = new Zend_Db_Adapter_Pdo_Mysql(array(
            'host'     => $config['resources']['db']['params']['host'],
            'username' => $config['resources']['db']['params']['username'],
            'password' => My_HiddenDB::decryptDb($config['resources']['db']['params']['password']),
            'dbname'   => $config['resources']['db']['params']['dbname']
        ));

        $temp = $params[0];

        $arrkey = array_keys($temp);

        $str_insert = '';
        foreach ($params as $k => $param) {
            $str_insert .= "('" . implode("', '", $param) . "')" . ',';
        }

        $str_rows = rtrim($str_insert, ',');

        $sql = "INSERT INTO `notification_access` ";

        $sql .= " (`" . implode("`, `", array_keys($temp)) . "`)";

        $sql .= " VALUES $str_rows ";

        $db_log->query($sql);
    }

    ////# Gửi mail sinh nhật và chào đón nhân viên mới
    //#INPUT: 
    //$staff_email: mail cần gửi đến
    //$type: =1:chúc sinh nhật; =2:welcome NV mới
    //#OUTPUT:Array([from],[to],[cc],[bcc])
    private function getMail($staff_email, $type = 1) {
        $staff_email = trim($staff_email);
      
        if (!empty($staff_email)) {
            $list_email       = [
                'from' => 'hr@oppo-aed.vn',
                'to'   => '',
                'cc'   => array(),
                'bcc'  => ''
            ];
            $send_mail        = 0;
            $staff_model      = new Application_Model_Staff();
            $staff_info_where = $staff_model->getAdapter()->quoteInto('email = ?', $staff_email);
            $staff_info       = $staff_model->fetchRow($staff_info_where);
         
            if (!empty($staff_info)) {
                $staff_info = $staff_info->toArray();
            } else {
                //$this->view->data = "Email chưa chính xác hoặc không tồn tại";
                return null;
            }
            if (!empty($staff_info)) {
                $list_email['to'] = $staff_info['email'];
                $db               = Zend_Registry::get('db');
                if (in_array($staff_info['department'], array(152, 157)) && !in_array($staff_info['team'], array(133, 131,406))) {
                    //  sale department && not trainer & not trade
                    $select_rule = $db->select()
                            ->from(array('a' => 'auto_mail_rule'), array('a.*'))
                            ->where('status = 1')
                            ->where('type = ?', $type)
                            ->where('department_id = ? ', $staff_info['department'])
                            ->where('(team_id is NULL ')
                            ->orwhere('team_id not in  (?)) ', array(133, 131,406));
                    $result_mail = $db->fetchRow($select_rule);

                    if (!empty($result_mail)) {
                        if (in_array($staff_info['title'], array(
                                    190, // SALE LEADER
                                    183, // SALE
                                    179, // ASM
                                    308, // RSM
                                    181, // ASM STANBY
                                    373, // STORE LEADER
                                    162, // SALE LEADER ACCESSORIES
                                    164, // SALE ACCESSORIES
                                    291, // ACCESSORIES SALES MANAGER
                                    191, // SALE ADMIN
                                ))) {
                            if ($type == 1) {
                                if (!empty($result_mail['area_mail_cc'])) {
                                    $select_area = $db->select()
                                            ->from(array('r' => 'regional_market'), array('r.*'))
                                            ->join(array('ar' => 'area'), 'r.area_id =ar.id', array('ar.email'))
                                            ->where('r.id = ?', $staff_info['regional_market']);
                                    $result_area = $db->fetchRow($select_area);
                                    $result_area = $db->fetchRow($select_area);
                                    if (!empty($result_area['email'])) {
                                        $list_email['cc'][] = $result_area['email'];
                                        $send_mail          = 1;
                                    }
                                }
                            } else {
                                if (in_array($staff_info['title'], array(
                                            191, // SALE ADMIN
                                        ))) {
                                    $select_area = $db->select()
                                            ->from(array('r' => 'regional_market'), array('r.*'))
                                            ->join(array('ar' => 'area'), 'r.area_id =ar.id', array('ar.email'))
                                            ->where('r.id = ?', $staff_info['regional_market']);
                                    $result_area = $db->fetchRow($select_area);
                                    $result_area = $db->fetchRow($select_area);
                                    if (!empty($result_area['email'])) {
                                        $list_email['cc'][] = $result_area['email'];
                                        $send_mail          = 1;
                                    }
                                }
                            }
                        } else {

                            if (!empty($result_mail['only_office'])) {
                                if ($staff_info['office_id'] == 49) {
                                    $list_email['cc'][] = "headoffice@oppo-aed.vn";
                                    $send_mail          = 1;
                                }
                            }
                        }
                    }
                } else {
                    // get mail rule by team
                    $select_rule = $db->select()
                            ->from(array('a' => 'auto_mail_rule'), array('a.*'))
                            ->where('status = 1')
                            ->where('type = ?', $type);
                    if (in_array($staff_info['department'], array(150,149,156,557))) {
                        $select_rule->where('department_id = ?', $staff_info['department']);
                    } else {
                        $select_rule->where('team_id = ? ', $staff_info['team']);
                    }

                    $result_mail = $db->fetchRow($select_rule);

                    if (!empty($result_mail)) {
                        $list_email['cc'][] = $result_mail['team_mail_cc'];
                        $send_mail          = 1;
                        if ($result_mail['officer_check'] == 1 && $staff_info['office_id'] == 49) {
                            $list_email['cc'][] = "headoffice@oppo-aed.vn";
                        } else
                        if ($result_mail['area_mail_cc'] == 1) {
                            $select_area        = $db->select()
                                    ->from(array('r' => 'regional_market'), array('r.*'))
                                    ->join(array('ar' => 'area'), 'r.area_id =ar.id', array('ar.email'))
                                    ->where('r.id = ?', $staff_info['regional_market']);
                            $result_area        = $db->fetchRow($select_area);
                            $list_email['cc'][] = $result_area['email'];
                        }
                        if ($result_mail['area_mail_cc'] == 2) {
                            $list_email['cc'][] = "headoffice@oppo-aed.vn";
                        }
                    }
                }
                $list_email['bcc'] = $result_mail['email_bcc'];
                $list_email['bcc'] .= ',tanthinh.phan@oppo-aed.vn,daithanh.nguyen@oppo-aed.vn';
                if ($send_mail == 1) {
                    //$this->view->data = $list_email;
                    return $list_email;
                } else {
                    //$this->view->data = "Không nằm trong danh sách gửi mail";
                    return null;
                }
                //return;
            }
        }
    }

    public function welcomeNew_b($staff_id) {
        $QStaff       = new Application_Model_Staff();
//        $staff_id = 8813;
//         $staff_id = 5;
        /////////////////////////
        /////// CONFIG
        /////////////////////////
//        error_reporting(~E_ALL);
//        ini_set("display_error", 0);
        set_time_limit(0);
        $mailfrom     = 'hr@oppo-aed.vn'; // Config this
        $mailfromname = "HR"; // Config this

        $app_config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');

        $config = array(
            'auth'     => $app_config->mail->smtp->auth,
            'username' => $app_config->mail->smtp->user2,
            'password' => $app_config->mail->smtp->pass2,
            'port'     => $app_config->mail->smtp->port,
            'ssl'      => $app_config->mail->smtp->ssl
        );

        /////////////////////////
        /////// SEND EMAIL
        /////////////////////////
        $font      = '/var/www/html/center/public/css/font/BAUHAUSM.ttf';
        $font_b    = '/var/www/html/center/public/css/font/BAUHAUHB.ttf';
        $transport = new Zend_Mail_Transport_Smtp($app_config->mail->smtp->host, $config);

        $mail = new My_Mail($app_config->mail->smtp->charset);

        $mail->setFrom($app_config->mail->smtp->from2, $app_config->mail->smtp->from2);


        $mail->setSubject('CHÀO MỪNG BẠN ĐẾN VỚI GIA ĐÌNH OPPO!');
        //

        $staff = $QStaff->find($staff_id);
        $staff = $staff->current();

        /*
          TẠO HÌNH ẢNH
         */
        // Create the image
        if ($staff['office_id'] == 49) {
            $ran   = rand(4, 6);
            $hight = 79;
            $x     = 380;
        } else {
            $ran   = rand(1, 3);
            $hight = 113;
            $x     = 385;
        }
        $im    = imagecreatefromjpeg(APPLICATION_PATH . "/../public/photo/mail/birthday/welcome-new-oppper_email-0" . $ran . ".jpg");
//        var_dump($im);die;
        $green = imagecolorallocate($im, 0, 139, 86);
        // The text to draw
        $name  = $this->mb_ucwords($staff['firstname'] . ' ' . $staff['lastname']);

        imagettftext($im, 14, 0, ($x - (mb_strlen($name, "UTF-8") * 11 / 2)), $hight, $green, $font_b, $name);
        // Using imagepng() results in clearer text compared with imagejpeg()
        $date_cur     = date('Y-m-d');
        $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                DIRECTORY_SEPARATOR . 'birthday' . DIRECTORY_SEPARATOR . $date_cur . DIRECTORY_SEPARATOR;

        if (!is_dir($uploaded_dir)) {
            $old = umask(0);
            @mkdir($uploaded_dir, 0777, true);
            umask($old);
        }
        imagepng($im, $uploaded_dir . $staff_id . 'welcome_new_oppper.png');
        imagedestroy($im);
        /*
          END TẠO HÌNH ẢNH
         */

        $img_wellcome = "<div><table><tr><td><img src='" . HOST . "photo/birthday/" . $date_cur . "/" . $staff_id . "welcome_new_oppper.png'/></td></tr><tr></tr>";
        if ($staff['office_id'] == 49) {
            $img_wellcome .= "<tr><td><b>Các bạn tham khảo và thực hiện các hướng dẫn dành cho nhân viên mới theo đường link  :</b>https://cloud.opposhop.vn/index.php/s/xEQdWBz9D1kBenT</td></tr>";
        } else {
            $img_wellcome .= "<tr><td><b>Các bạn tham khảo và thực hiện các hướng dẫn dành cho nhân viên mới theo đường link:</b>https://cloud.opposhop.vn/index.php/s/QZDUVaJKlGamkWM</td></tr>";
        }
        $img_wellcome .= "</table></div>";
        $img_wellcome .= $this->hr_signature;
        $mail->setBodyHtml($img_wellcome);
        $mailTo       = [$staff['email']];
        $mailcc       = [ 'daithanh.nguyen@oppo-aed.vn', 'phuongnga.nguyen@oppo-aed.vn', 'yennhi.pham@oppo-aed.vn', 'vandiem.nguyen@oppo-aed.vn'];
        $mail->addTo($mailTo);
        $mail->addBcc($mailcc);
        $r            = $mail->send($transport);
        return true;
    }
    public function welcomeNew($staff_id) {

        $QStaff       = new Application_Model_Staff();

//         $staff_id = 5;
        /////////////////////////
        /////// CONFIG
        /////////////////////////
//        error_reporting(~E_ALL);
//        ini_set("display_error", 0);
        set_time_limit(0);
        $mailfrom     = 'hr@oppo-aed.vn'; // Config this
        $mailfromname = "HR"; // Config this

        $app_config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');

        $config = array(
            'auth'     => $app_config->mail->smtp->auth,
            'username' => $app_config->mail->smtp->user2,
            'password' => $app_config->mail->smtp->pass2,
            'port'     => $app_config->mail->smtp->port,
            'ssl'      => $app_config->mail->smtp->ssl
        );

        /////////////////////////
        /////// SEND EMAIL
        /////////////////////////
        $font      = '/var/www/html/center/public/css/font/BAUHAUSM.ttf';
        $font_b    = '/var/www/html/center/public/css/font/BAUHAUHB.ttf';
        $transport = new Zend_Mail_Transport_Smtp($app_config->mail->smtp->host, $config);

        $mail = new My_Mail($app_config->mail->smtp->charset);

        $mail->setFrom($app_config->mail->smtp->from2, $app_config->mail->smtp->from2);


        $mail->setSubject('CHÀO MỪNG BẠN ĐẾN VỚI GIA ĐÌNH OPPO!');
        //

        $staff = $QStaff->find($staff_id);
        $staff = $staff->current();
        /*
          TẠO HÌNH ẢNH
         */

        // The text to draw
        $name  = $this->mb_ucwords($staff['firstname'] . ' ' . $staff['lastname']);

        // Using imagepng() results in clearer text compared with imagejpeg()
        $date_cur     = date('Y-m-d');
        $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                DIRECTORY_SEPARATOR . 'birthday' . DIRECTORY_SEPARATOR . $date_cur . DIRECTORY_SEPARATOR;

        if (!is_dir($uploaded_dir)) {
            $old = umask(0);
            @mkdir($uploaded_dir, 0777, true);
            umask($old);
        }
        /*
          END TẠO HÌNH ẢNH
         */

        // $img_wellcome = "<div><table><tr><td><img src='" . HOST . "photo/birthday/" . $date_cur . "/" . $staff_id . "welcome_new_oppper.png'/></td></tr><tr></tr>";
        $img_wellcome = "<div><table>";

        // if ($staff['office_id'] == 49) {
        //     $img_wellcome .= "<tr><td><b>Các bạn tham khảo và thực hiện các hướng dẫn dành cho nhân viên mới theo đường link  :</b>https://cloud.opposhop.vn/index.php/s/xEQdWBz9D1kBenT</td></tr>";
        // } else {
        //     $img_wellcome .= "<tr><td><b>Các bạn tham khảo và thực hiện các hướng dẫn dành cho nhân viên mới theo đường link:</b>https://cloud.opposhop.vn/index.php/s/QZDUVaJKlGamkWM</td></tr>";
        // }
        $img_wellcome .="<tr><td>Xin chào<b> ".$name."</b> !</td></tr>";
        $img_wellcome .="<tr><td>Chào mừng bạn gia nhập và trở thành mảnh ghép tiếp theo của gia đình OPPO!</td></tr>";
        $img_wellcome .="<tr><td>Ngày đầu tiên của một OPPOer chắc hẳn sẽ có nhiều điều bỡ ngỡ.</td></tr>";
        $img_wellcome .="<tr><td>Đừng lo lắng! Hãy dành thời gian dạo quanh một vòng hệ thống Center OPPO để tìm hiểu thông tin về văn hóa, chính sách, quy định dành cho các bạn OPPOer nhé.</td></tr>";
        $img_wellcome .="<tr><td>Đăng nhập vào link:<a style='color:rgb(4, 106, 56)' href='https://center.opposhop.vn'>https://center.opposhop.vn</a></td></tr>";
        $img_wellcome .="<tr><td style='color:rgb(4, 106, 56)'><ul><li>User: email cá nhân</li><li>Password: 123456</li></ul></td></tr>";
        $img_wellcome .="<tr><td>Hướng dẫn, biểu mẫu: <a style='color:rgb(4, 106, 56)' href='https://cloud.opposhop.vn/index.php/s/xEQdWBz9D1kBenT'>https://cloud.opposhop.vn/index.php/s/xEQdWBz9D1kBenT</a></td></tr>";
        $img_wellcome .='<tr><td>Và đừng quên kế bên bạn có những đồng nghiệp dễ thương và "Sếp" tận tâm có thể trả lời những thắc mắc của bạn bất cứ lúc nào.</td></tr>';
        $img_wellcome .="<tr><td><b>Chúc bạn ngày làm việc đầu tiên tuyệt vời tại OPPO!</b></td></tr>";

        $img_wellcome .= "</table><img style='width:1100px;height:700px;'src='" . HOST . "photo/mail/birthday/welcome_staff_2020.jpg'/></div>";
        $img_wellcome .= $this->hr_signature;

        $mail->setBodyHtml($img_wellcome);
        $mailTo       = [$staff['email']];

        $mailcc       = [ 'thuydung.dang@oppo-aed.vn'];
        $mail->addTo($mailTo);
        $mail->addCc($mailcc);
        $r            = $mail->send($transport);
        return true;
    }
    
	
    public function remindOldAction() {
		     /////////////////////////
        /////// CONFIG
        /////////////////////////
        error_reporting(~E_ALL);
        ini_set("display_error", 0);
        set_time_limit(0);
        $db         = Zend_Registry::get('db');
        $app_config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
        $config     = array(
            'auth'     => $app_config->mail->smtp->auth,
            'username' => $app_config->mail->smtp->user2,
            'password' => $app_config->mail->smtp->pass2,
            'port'     => $app_config->mail->smtp->port,
            'ssl'      => $app_config->mail->smtp->ssl
        );

        // Thay mặt cho BCH Công đoàn và BGĐ Công ty
        /////////////////////////
        /////// GET STAFFs HAVING BIRTHDAY TODAY
        /////////////////////////


        $sql = "SELECT s.email, s.code,CONCAT(s.firstname,' ',s.lastname) AS fullname,s.joined_at,s.title,DATEDIFF(CURDATE(),s.joined_at) AS diff ,ROUND(DATEDIFF(CURDATE(),s.joined_at)/365) AS nam  
FROM staff s 
WHERE s.off_date IS NULL and DATE(s.joined_at)<CURDATE()
AND s.email IS NOT NULL
HAVING DAYOFMONTH(s.joined_at)=DAYOFMONTH(CURDATE()) AND MONTH(s.joined_at)=MONTH(CURDATE())  ";
//                $sql    = "SELECT s.email, s.code,CONCAT(s.firstname,' ',s.lastname) AS fullname,s.joined_at,s.title,DATEDIFF(CURDATE(),s.joined_at) AS diff ,ROUND(DATEDIFF(CURDATE(),s.joined_at)/365) AS nam  
//FROM staff s 
//WHERE s.off_date IS NULL
//AND s.email IS NOT NULL
//HAVING DAYOFMONTH(s.joined_at)=DAYOFMONTH('2018-03-28') AND MONTH(s.joined_at)=MONTH('2018-03-28')
//                ";
//        $staffs = $db->query($sql);
        $stmt   = $db->prepare($sql);
        $stmt->execute();
        $staffs = $stmt->fetchAll();
       

        /////////////////////////
        /////// SEND EMAIL
        /////////////////////////

        foreach ($staffs as $k => $staff) {



            $list_email = $this->getMail($staff['email'], 1);
//
            if (!empty($list_email) && count($list_email)) {
                $transport = new Zend_Mail_Transport_Smtp($app_config->mail->smtp->host, $config);

                $mail = new My_Mail($app_config->mail->smtp->charset);

                $mail->setFrom($app_config->mail->smtp->from2, $app_config->mail->smtp->from2);

                $fname = trim($this->mb_ucwords($staff['fullname']));
                $mail->setSubject('Chúc mừng bạn đã đồng hành ' . $staff['nam'] . ' năm cùng gia đình OPPO ');



                /*
                  TẠO HÌNH ẢNH
                 */
                // Set the content-type
                //header('Content-Type: image/png');
                // Create the image
              $im = imagecreatefrompng(APPLICATION_PATH . "/../public/photo/mail/remind/" . $staff['nam']. ".png");
              
                // Create some colors
                $white = imagecolorallocate($im, 255, 255, 255);
                $grey  = imagecolorallocate($im, 128, 128, 128);
                $black = imagecolorallocate($im, 0, 0, 0);

                $green = imagecolorallocate($im, 0, 139, 86);
                $pink  = imagecolorallocate($im, 232, 120, 139);

                // The text to draw
                $name = $fname;

                // Replace path by your own font path

                $font_b = '/var/www/html/center/public/css/font/BAUHAUHB.ttf';

                // Add the text
                imagettftext($im, 20, 0, (460 - (mb_strlen($name, "UTF-8") * 14 / 2)), 200, $green, $font_b, $name);




                $date_cur     = date('Y-m-d');
                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                        DIRECTORY_SEPARATOR . 'remind' . DIRECTORY_SEPARATOR . $date_cur . DIRECTORY_SEPARATOR;

                if (!is_dir($uploaded_dir)) {
                    $old = umask(0);
                    @mkdir($uploaded_dir, 0777, true);
                    umask($old);
                }
                imagepng($im, $uploaded_dir . $staff['code'] . '.png');
                imagedestroy($im);
                /*
                  END TẠO HÌNH ẢNH
                 */




                //end lưu nội dung thành hình ảnh
                $img_birthday = "<div><table><tr><td><img src='" . HOST . "photo/remind/" . $date_cur . "/" . $staff['code'] . ".png'/></td></tr></table></div>";
                $img_birthday .= $this->hr_signature;

                // $mail->setBodyHtml($img_birthday);
				$mail->setBodyHtml('xin chào');

                $mailTo  = array();
                $mailBcc = array();
//                echo $staff['email'];
              //  $mail->addTo($staff['email']);
                $mail->addTo('vandiem.nguyen@oppo-aed.vn');
                
               // $list_email['bcc'] = 'oppofamily@oppo-aed.vn,daithanh.nguyen@oppo-aed.vn,khahoang.le@oppo-aed.vn,vandiem.nguyen@oppo-aed.vn,tanthinh.phan@oppo-aed.vn';
               if (!empty($list_email['bcc'])) {
                    $mailBcc = explode(",", $list_email['bcc']);
                    $mail->addBcc($mailBcc);
                }
				
//                echo "<pre>";
//                print_r($list_email['bcc']);
//                $mail->addBcc('anhthoai.vo@oppo-aed.vn');
                $r = $mail->send($transport);
                sleep(1);
            }
        }
        echo "<pre>";
        print_r('ok');

        exit;

    }
	
	public function remindNewAction() {

        /////////////////////////
        /////// CONFIG
        /////////////////////////
        error_reporting(~E_ALL);
        ini_set("display_error", 0);
        set_time_limit(0);
        $db         = Zend_Registry::get('db');
        $app_config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
        $config     = array(
            'auth'     => $app_config->mail->smtp->auth,
            'username' => $app_config->mail->smtp->user2,
            'password' => $app_config->mail->smtp->pass2,
            'port'     => $app_config->mail->smtp->port,
            'ssl'      => $app_config->mail->smtp->ssl
        );

        // Thay mặt cho BCH Công đoàn và BGĐ Công ty
        /////////////////////////
        /////// GET STAFFs HAVING BIRTHDAY TODAY
        /////////////////////////


        $sql = "SELECT s.email, s.code,CONCAT(s.firstname,' ',s.lastname) AS fullname,s.joined_at,s.title,DATEDIFF(CURDATE(),s.joined_at) AS diff ,ROUND(DATEDIFF(CURDATE(),s.joined_at)/365) AS nam  
FROM staff s 
WHERE s.off_date IS NULL and DATE(s.joined_at)<CURDATE()
AND s.email IS NOT NULL
HAVING DAYOFMONTH(s.joined_at)=DAYOFMONTH(CURDATE()) AND MONTH(s.joined_at)=MONTH(CURDATE())  ";
//                $sql    = "SELECT s.email, s.code,CONCAT(s.firstname,' ',s.lastname) AS fullname,s.joined_at,s.title,DATEDIFF(CURDATE(),s.joined_at) AS diff ,ROUND(DATEDIFF(CURDATE(),s.joined_at)/365) AS nam  
//FROM staff s 
//WHERE s.off_date IS NULL
//AND s.email IS NOT NULL
//HAVING DAYOFMONTH(s.joined_at)=DAYOFMONTH('2018-03-28') AND MONTH(s.joined_at)=MONTH('2018-03-28')
//                ";
//        $staffs = $db->query($sql);
        $stmt   = $db->prepare($sql);
        $stmt->execute();
        $staffs = $stmt->fetchAll();
       

        /////////////////////////
        /////// SEND EMAIL
        /////////////////////////

        foreach ($staffs as $k => $staff) {



            $list_email = $this->getMail($staff['email'], 1);
//
            if (!empty($list_email) && count($list_email)) {
                $transport = new Zend_Mail_Transport_Smtp($app_config->mail->smtp->host, $config);

                $mail = new My_Mail($app_config->mail->smtp->charset);

                $mail->setFrom($app_config->mail->smtp->from2, $app_config->mail->smtp->from2);

                $fname = trim($this->mb_ucwords($staff['fullname']));
                $mail->setSubject('Chúc mừng bạn đã đồng hành ' . $staff['nam'] . ' năm cùng gia đình OPPO ');



                /*
                  TẠO HÌNH ẢNH
                 */
                // Set the content-type
                //header('Content-Type: image/png');
                // Create the image
              $im = imagecreatefrompng(APPLICATION_PATH . "/../public/photo/mail/remind/" . $staff['nam']. ".png");
              
                // Create some colors
                $white = imagecolorallocate($im, 255, 255, 255);
                $grey  = imagecolorallocate($im, 128, 128, 128);
                $black = imagecolorallocate($im, 0, 0, 0);

                $green = imagecolorallocate($im, 0, 139, 86);
                $pink  = imagecolorallocate($im, 232, 120, 139);

                // The text to draw
                $name = $fname;

                // Replace path by your own font path

                $font_b = '/var/www/html/center/public/css/font/BAUHAUHB.ttf';

                // Add the text
                imagettftext($im, 20, 0, (460 - (mb_strlen($name, "UTF-8") * 14 / 2)), 200, $green, $font_b, $name);




                $date_cur     = date('Y-m-d');
                $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                        DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                        DIRECTORY_SEPARATOR . 'remind' . DIRECTORY_SEPARATOR . $date_cur . DIRECTORY_SEPARATOR;

                if (!is_dir($uploaded_dir)) {
                    $old = umask(0);
                    @mkdir($uploaded_dir, 0777, true);
                    umask($old);
                }
                imagepng($im, $uploaded_dir . $staff['code'] . '.png');
                imagedestroy($im);
                /*
                  END TẠO HÌNH ẢNH
                 */




                //end lưu nội dung thành hình ảnh
                $img_birthday = "<div><table><tr><td><img src='" . HOST . "photo/remind/" . $date_cur . "/" . $staff['code'] . ".png'/></td></tr></table></div>";
                $img_birthday .= $this->hr_signature;

                $mail->setBodyHtml($img_birthday);

                $mailTo  = array();
                $mailBcc = array();
//                echo $staff['email'];
               $mail->addTo($staff['email']);
//                $mail->addTo('vandiem.nguyen@oppo-aed.vn');
                
                $list_email['bcc'] = 'oppofamily@oppo-aed.vn,daithanh.nguyen@oppo-aed.vn,khahoang.le@oppo-aed.vn,vandiem.nguyen@oppo-aed.vn, tanthinh.phan@oppo-aed.vn';
                if (!empty($list_email['bcc'])) {
                    $mailBcc = explode(",", $list_email['bcc']);
                    $mail->addBcc($mailBcc);
                }
//                echo "<pre>";
//                print_r($list_email['bcc']);
//                $mail->addBcc('anhthoai.vo@oppo-aed.vn');
                $r = $mail->send($transport);
                sleep(1);
            }
        }
        echo "<pre>";
        print_r('ok');

        exit;
    }
	
	public function remindAction() {
        /////////////////////////
        /////// CONFIG
        /////////////////////////
        error_reporting(~E_ALL);
        ini_set("display_error", 0);
        set_time_limit(0);

        $app_config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');

        $config = array(
            'auth'     => $app_config->mail->smtp->auth,
            'username' => $app_config->mail->smtp->user2,
            'password' => $app_config->mail->smtp->pass2,
            'port'     => $app_config->mail->smtp->port,
            'ssl'      => $app_config->mail->smtp->ssl
        );

        // Thay mặt cho BCH Công đoàn và BGĐ Công ty
        $wish = $this->_wish;

        /////////////////////////
        /////// GET STAFFs HAVING BIRTHDAY TODAY
        /////////////////////////
        $QTeam   = new Application_Model_Team();
        $QArea   = new Application_Model_Area();
        $QStaff  = new Application_Model_Staff();
        $QRegion = new Application_Model_RegionalMarket();
        $QStaff  = new Application_Model_Staff();

        $team_cache = $QTeam->get_all_cache();
        $areas      = $QArea->get_cache();

        $id = $this->getRequest()->getParam('id');
		// $id = 23253;

        $staffs = $this->_getStaffBirthday($id, self::NO_PG);
		
        if (!isset($staffs) || !$staffs)
            exit;

        /////////////////////////
        /////// SEND EMAIL
        /////////////////////////
        foreach ($staffs as $k => $staff) {
            $transport = new Zend_Mail_Transport_Smtp($app_config->mail->smtp->host, $config);

            $mail = new My_Mail($app_config->mail->smtp->charset);

            $mail->setFrom($app_config->mail->smtp->from2, $app_config->mail->smtp->from2);

            $fname = trim($this->mb_ucwords($staff['firstname'] . ' ' . $staff['lastname']));
            $mail->setSubject('Happy Birthday ' . $fname);

            $d = explode('/', $staff['dob']);
            $d = $d[0] . '/' . $d[1];



            /*
              TẠO HÌNH ẢNH
             */
            // Set the content-type
            //header('Content-Type: image/png');
            // Create the image
            // $im = imagecreatefromjpeg(APPLICATION_PATH . "/../public/photo/mail/birthday/thiep-2017-0" . rand(1, 4) . ".jpg");
            $im = imagecreatefromjpeg(APPLICATION_PATH . "/../public/photo/mail/birthday/thiep-2019-0" . rand(1, 4) . ".jpg");

            // Create some colors
            $white = imagecolorallocate($im, 255, 255, 255);
            $grey  = imagecolorallocate($im, 128, 128, 128);
            $black = imagecolorallocate($im, 0, 0, 0);

            $green = imagecolorallocate($im, 0, 139, 86);
            $pink  = imagecolorallocate($im, 232, 120, 139);

            // The text to draw
            $name     = $fname;
            $birthday = $d;

            $deparment = "Bộ phận: " . strtoupper($this->mb_ucwords(@$team_cache[$staff['department']]['name']));
            $team      = "Team: " . strtoupper($this->mb_ucwords(@$team_cache[$staff['team']]['name']));
            $title     = 'Chức vụ: ' . strtoupper($this->mb_ucwords(@$team_cache[$staff['title']]['name']));

            // Replace path by your own font path
            $font   = '/var/www/html/center/public/css/font/BAUHAUSM.ttf';
            $font_b = '/var/www/html/center/public/css/font/BAUHAUHB.ttf';

            // Add the text
            imagettftext($im, 20, 0, (450 - (mb_strlen($name, "UTF-8") * 14 / 2)), 225, $green, $font_b, $name);
            imagettftext($im, 24, 0, (445 - (mb_strlen($birthday, "UTF-8") * 14 / 2)), 260, $green, $font_b, $birthday);

            imagettftext($im, 20, 0, (450 - (mb_strlen($deparment, "UTF-8") * 13 / 2)), 299, $pink, $font, $deparment);
            imagettftext($im, 20, 0, (450 - (mb_strlen($team, "UTF-8") * 13 / 2)), 332, $pink, $font, $team);
            imagettftext($im, 20, 0, (450 - (mb_strlen($title, "UTF-8") * 13 / 2)), 365, $pink, $font, $title);


            $rand_ = rand(1, 5);
            if ($rand_ == 1) {
                $descreption1 = '"Gia đình OPPO chúc bạn tuổi mới';
                $descreption2 = 'hạnh phúc, vui vẻ và luôn trẻ khỏe"';

                imagettftext($im, 20, 0, (475 - (mb_strlen($descreption1, "UTF-8") * 14.5 / 2)), 410, $green, $font_b, $descreption1);
                imagettftext($im, 20, 0, (495 - (mb_strlen($descreption2, "UTF-8") * 14.5 / 2)), 437, $green, $font_b, $descreption2);
            } elseif ($rand_ == 2) {
                $descreption1 = '"Gia đình OPPO chúc bạn luôn tươi trẻ';
                $descreption2 = 'và thành công trong cuộc sống"';

                imagettftext($im, 20, 0, (475 - (mb_strlen($descreption1, "UTF-8") * 14.5 / 2)), 410, $green, $font_b, $descreption1);
                imagettftext($im, 20, 0, (475 - (mb_strlen($descreption2, "UTF-8") * 14.5 / 2)), 437, $green, $font_b, $descreption2);
            } elseif ($rand_ == 3) {
                $descreption1 = '"Gia đình OPPO cùng bạn chào đón tuổi mới';
                $descreption2 = 'với nhiều niềm vui, hạnh phúc';
                $descreption3 = 'và thành công"';

                imagettftext($im, 20, 0, (475 - (mb_strlen($descreption1, "UTF-8") * 14.5 / 2)), 405, $green, $font_b, $descreption1);
                imagettftext($im, 20, 0, (505 - (mb_strlen($descreption2, "UTF-8") * 14.5 / 2)), 432, $green, $font_b, $descreption2);
                imagettftext($im, 20, 0, (455 - (mb_strlen($descreption3, "UTF-8") * 14.5 / 2)), 459, $green, $font_b, $descreption3);
            } elseif ($rand_ == 4) {
                $descreption1 = '"Thêm tuổi mới, đại gia đình OPPO chúc bạn';
                $descreption2 = 'thật nhiều sức khỏe';
                $descreption3 = 'và niềm vui trong cuộc sống"';

                imagettftext($im, 20, 0, (480 - (mb_strlen($descreption1, "UTF-8") * 14.5 / 2)), 405, $green, $font_b, $descreption1);
                imagettftext($im, 20, 0, (445 - (mb_strlen($descreption2, "UTF-8") * 14.5 / 2)), 432, $green, $font_b, $descreption2);
                imagettftext($im, 20, 0, (452 - (mb_strlen($descreption3, "UTF-8") * 14.5 / 2)), 459, $green, $font_b, $descreption3);
            } elseif ($rand_ == 5) {
                $descreption1 = '"Nhân ngày sinh nhật, đại gia đình OPPO';
                $descreption2 = 'mến chúc bạn thật nhiều may mắn';
                $descreption3 = 'thành công và hạnh phúc"';

                imagettftext($im, 20, 0, (475 - (mb_strlen($descreption1, "UTF-8") * 14.5 / 2)), 405, $green, $font_b, $descreption1);
                imagettftext($im, 20, 0, (475 - (mb_strlen($descreption2, "UTF-8") * 14.5 / 2)), 432, $green, $font_b, $descreption2);
                imagettftext($im, 20, 0, (475 - (mb_strlen($descreption3, "UTF-8") * 14.5 / 2)), 459, $green, $font_b, $descreption3);
            }



            // Using imagepng() results in clearer text compared with imagejpeg()

            $date_cur     = date('Y-m-d');
            $uploaded_dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .
                    DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' .
                    DIRECTORY_SEPARATOR . 'birthday' . DIRECTORY_SEPARATOR . $date_cur . DIRECTORY_SEPARATOR;

            if (!is_dir($uploaded_dir)) {
                $old = umask(0);
                @mkdir($uploaded_dir, 0777, true);
                umask($old);
            }
            imagepng($im, $uploaded_dir . $staff['id'] . '.png');
            imagedestroy($im);
            /*
              END TẠO HÌNH ẢNH
             */




            //end lưu nội dung thành hình ảnh
            $img_birthday = "<div><table><tr><td><img src='" . HOST . "photo/birthday/" . $date_cur . "/" . $staff['id'] . ".png'/></td></tr></table></div>";
            $img_birthday .= $this->hr_signature;

            $mail->setBodyHtml($img_birthday);
////////////khuc nay
//            $mailto = array(); // Config this
//
//            // lấy khu vực để chọn email gửi tới
//            $region_id = (isset($staff['regional_market']) && $staff['regional_market']) ? $staff['regional_market'] : 0;
//            $region = $QRegion->find($region_id);
//            $region = $region->current();
//
//            if ($region) {
//                $area = $QArea->find($region['area_id']);
//                $area = $area->current();
//
//                if ($area) {
//
//                    if ( $area['id'] == HCMC4
//                            && isset($staff['is_officer']) && $staff['is_officer']) {
//                        $mailto[] = EMAIL_HCM_OFFICE; // gửi nv văn phòng HCM
//
//                    } elseif ( !empty($area['email']) &&  WARRANTY_CENTER != $staff['department']) {
//                        $mailto[] = $area['email']; // email khu vực
//                    }
//                }
//            }
//
//            // email cá nhân
//            if (!empty($staff['email'])) {
//                $mailto[] = $staff['email'];
//            }
//
//            // nếu là asm thì gửi vào EMAIL_ASM
//            if ( isset($staff['group_id']) && in_array($staff['group_id'], array(ASM_ID, ASMSTANDBY_ID)) ) {
//                $mailto[] = EMAIL_ASM;
//
//            } elseif ( isset($staff['group_id']) && $staff['group_id'] == SALES_ADMIN_ID ) {
//                $mailto[] = EMAIL_SALESADMIN;
//            }
//
//            if ( !empty( $team_cache[ $staff['team'] ]['email'] ) ) {
//                $mailto[] = $team_cache[ $staff['team'] ]['email'];
//
//            } elseif ( !empty( $team_cache[ $staff['department'] ]['email'] ) ) {
//                $mailto[] = $team_cache[ $staff['department'] ]['email'];
//            }
//
//            $mailto[] = 'thehien.hoang@oppo-aed.vn';
//            $mailto[] = 'yen.dao@oppo-aed.vn';
//            $mailto[] = 'an.nguyen@oppo-aed.vn';
//            
//            if (isset($staff['is_officer']) && $staff['is_officer'])
//                $mailto[] = 'yennhi.pham@oppo-aed.vn';
//
//            // for debug only
//            foreach ($mailto as $key => $value) {
//                echo date('Y-m-d H:i:s') .": ".$value."\n";
//
//            }//
			$mail->addTo('vandiem.nguyen@oppo-aed.vn');
            $list_email = $this->getMail($staff['email'], 1);
			
            if (isset($list_email) && count($list_email)) {
                $mailTo  = array();
                $mailBcc = array();
				/*
                if (!empty($list_email['to'])) {
                    $mailTo[] = $list_email['to'];
                    $mail->addTo($mailTo);
                }

                if (!empty($list_email['cc'])) {
                    if ($staff['code'] == '14020052') {
                        
                    } else {
                        $mail->addCc($list_email['cc']);
                    }
                }
				
                if (!empty($list_email['bcc'])) {
                    $mailBcc = explode(",", $list_email['bcc']);
                    $mail->addBcc($mailBcc);
                }
					*/
                $r = $mail->send($transport);
                sleep(1);
            }
			
        }

        exit;
    }

}
