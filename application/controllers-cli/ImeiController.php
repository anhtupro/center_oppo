<?php
class ImeiController extends My_Application_Controller_Cli
{
	public function checkImeiLostAction(){
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
        $config = $config->toArray();
        $cf     = $config['resources']['db']['params'];
        $cf2    = $config['resources']['db2']['params'];        
        $con    = mysqli_connect($cf['host'],$cf['username'],My_HiddenDB::decryptDb($cf['password']),$cf['dbname']);
        
        // Check connection
        if (mysqli_connect_errno())
        {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }

        $sqls = array();

        $sqls['activated'] = '
                INSERT INTO imei_lost(imei_sn,good_id, good_color, type, activated_date,warehouse_id, warehouse_type, 
                    imei_status,created_at, time_check_in)
                SELECT a.imei_sn, a.good_id, a.good_color, 2 as "type", a.activated_date, a.warehouse_id, 
                    1 as warehouse_type, a.`status`, NOW(),
                    NOW() as time_check_in
                FROM '.WAREHOUSE_DB.'.imei a
                LEFT JOIN imei_lost b ON a.imei_sn = b.imei_sn AND b.type = 2
                WHERE a.activated_date IS NOT NULL AND a.activated_date <> 0 
                    AND DATE(a.activated_date) >= "'.IMEI_LOST_START.'"
                    AND a.return_sn IS NULL
                    AND (a.distributor_id IS NULL OR a.distributor_id = 0)
                    AND (a.out_date IS NULL)
                    AND (b.id IS NULL);
        ';

        $sqls['timing'] = '
            INSERT INTO imei_lost(imei_sn, type, good_id, good_color, warehouse_id, warehouse_type, staff_id, created_at, 
                activated_date, store_id, time_check_in)
            SELECT b.imei, 
                1 as type, 
                b.product_id as good_id, 
                b.model_id as good_color, 
                c.warehouse_id as "warehouse_id", 
                1 as "warehouse_type", 
                a.staff_id,
                NOW(), 
                c.activated_date, 
                a.store,
                a.created_at as time_check_in
            FROM timing a
            INNER JOIN timing_sale b ON a.id = b.timing_id AND b.imei IS NOT NULL
            INNER JOIN '.WAREHOUSE_DB.'.imei c ON b.imei = c.imei_sn 
                AND (c.distributor_id IS NULL OR c.distributor_id = 0)
                AND a.approved_at IS NOT NULL
                AND (c.return_sn IS NULL OR c.return_sn = "")
                AND (c.out_date IS NULL)
            LEFT JOIN imei_lost d ON d.imei_sn = b.imei AND d.type = 1
            WHERE d.id IS NULL 
                    AND (c.distributor_id IS NULL OR c.distributor_id = 0)
                    AND DATE(a.created_at) >= "'.IMEI_LOST_START.'"

            ;
        ';

        $sqls['check_in'] = '
            INSERT INTO imei_lost(imei_sn, type, good_id, good_color, warehouse_id, warehouse_type, staff_id, created_at, 
                activated_date, time_check_in)
            SELECT c.imei,
                3,
                b.good_id, 
                b.good_color, 
                b.warehouse_id, 
                1 as warehouse_type, 
                c.user_id,
                c.time,
                b.activated_date,
                c.time as time_check_in
            FROM (
                SELECT imei, MIN(id) id
                FROM check_imei_log
                WHERE result = 2
                GROUP BY imei
            ) a
            INNER JOIN check_imei_log c ON a.id = c.id
            INNER JOIN '.WAREHOUSE_DB.'.imei b ON a.imei = b.imei_sn 
                AND (b.distributor_id IS NULL OR b.distributor_id = 0)
                AND (b.return_sn IS NULL OR b.return_sn = "")
                AND (b.out_date IS NULL)
            LEFT JOIN imei_lost d ON d.imei_sn = a.imei AND d.type = 3
            WHERE d.id IS NULL
                    AND DATE(c.time) >= "'.IMEI_LOST_START.'"
            ;
        ';
        // var_dump($sqls);
        // exit;
        try{

            //chạy query
            foreach($sqls as $key => $sql){
                $result = mysqli_query($con,$sql);
            }

            //check imei from cs
            $result = $this->loadFromCs();
            if($result['code'] < 0){
                echo $result['message'];
                exit;
            }
            echo 'Done';
        }catch(Exception $e){
            echo $e->getMessage();
        }
        exit;
        
    }

    public function loadFromCs(){
        $QImeiLost = new Application_Model_ImeiLost();
        $QImeiLost->checkImeiFromCs();
        $QImeiLost->checkImeiCustomerFromCs();
    }
}