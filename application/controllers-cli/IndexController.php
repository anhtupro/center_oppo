<?php
class IndexController extends My_Application_Controller_Cli
{

    public function indexAction(){
        ignore_user_abort();
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        error_reporting(0);
        ini_set('display_error', 0);

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');

        $config = $config->toArray();

        $con=mysqli_connect($config['resources']['db']['params']['host'],$config['resources']['db']['params']['username'],My_HiddenDB::decryptDb($config['resources']['db']['params']['password']),$config['resources']['db']['params']['dbname']);

        // Check connection
        if (mysqli_connect_errno())
        {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }

        $sql = 'truncate wh_imei;';
        mysqli_query($con,$sql);

        $sql = ' 	replace into '.$config['resources']['db']['params']['dbname'].'.wh_imei
						select * from warehouse.`imei`
		';

        mysqli_query($con,$sql);

        mysqli_close($con);

        exit;
    }

    public function imeiactivationAction(){
        ignore_user_abort();
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        error_reporting(0);
        ini_set('display_error', 0);

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');

        $config = $config->toArray();

        $con=mysqli_connect($config['resources']['db']['params']['host'],$config['resources']['db']['params']['username'],My_HiddenDB::decryptDb($config['resources']['db']['params']['password']),$config['resources']['db']['params']['dbname']);

        // Check connection
        if (mysqli_connect_errno())
        {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }

        $sql = 'truncate wh_imei_activation;';
        mysqli_query($con,$sql);

        $sql = ' 	replace into '.$config['resources']['db']['params']['dbname'].'.wh_imei_activation
						select * from warehouse.`imei_activation`
		';

        mysqli_query($con,$sql);

        mysqli_close($con);

        exit;
    }

    public function marketAction(){
        ignore_user_abort();
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        error_reporting(0);
        ini_set('display_error', 0);

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');

        $config = $config->toArray();

        $con=mysqli_connect($config['resources']['db']['params']['host'],$config['resources']['db']['params']['username'],My_HiddenDB::decryptDb($config['resources']['db']['params']['password']),$config['resources']['db']['params']['dbname']);

        // Check connection
        if (mysqli_connect_errno())
        {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }

        $sql = 'truncate wh_market;';
        mysqli_query($con,$sql);

        $sql = ' 	replace into '.$config['resources']['db']['params']['dbname'].'.wh_market
						select * from warehouse.`market`
		';

        mysqli_query($con,$sql);

        mysqli_close($con);

        exit;
    }

    //import distributor
    public function priceLogAction ()
    {
        ignore_user_abort();
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        //error_reporting(0);
        //ini_set('display_error', 0);

        require_once 'My'.DIRECTORY_SEPARATOR.'nusoap'.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'nusoap.php';


        $client = new soapclient(WSS_MK_URI);

        // Check for an error
        $err = $client->getError();
        if ($err) {
            // Display the error
            echo '<p><b>Constructor error: ' . $err . '</b></p>';
            // At this point, you know the call that follows will fail
        }

        // Call the SOAP method
        $result = $client->call(
            'fetch_price_log'
        );

        // Check for a fault
        if ($client->fault) {
            echo '<p><b>Fault: ';
            print_r($result);
            echo '</b></p>';
        } else {
            // Check for errors
            $err = $client->getError();
            if ($err) {
                // Display the error
                echo '<p><b>Error: ' . $err . '</b></p>';
            } else {

                if ($result){

                    //decode
                    $imeis = json_decode($result, true);


                    if ($imeis) {

                        $count = count($imeis);

                        $QPrice = new Application_Model_GoodPriceLog();

                        for ($i=0; $i<$count; $i++){

                            try {
                                $QPrice->insert($imeis[$i]);
                            } catch (Exception $e){
                                $where = $QPrice->getAdapter()->quoteInto('id = ?', $imeis[$i]['id']);
                                $QPrice->update($imeis[$i], $where);
                            }

                        }


                        $content = date('Y-m-d H:i:s') . ' imported price log';

                        echo 'imported price log: done' . "\n";

                        error_log($content . "\n", 3, APPLICATION_PATH.'/../bin/access.log');
                    }

                }
            }
        }

        //clear cache
        $cache_folder = APPLICATION_PATH.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'data'.DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR;

        try {
            foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($cache_folder, FilesystemIterator::SKIP_DOTS), RecursiveIteratorIterator::CHILD_FIRST) as $path) {
                if ($path->getFilename() == 'zend_cache---server_notifis_cache'
                    || $path->getFilename() == 'zend_cache---internal-metadatas---server_notifis_cache')
                    continue;

                $path->isFile() ? @unlink($path->getPathname()) : @rmdir($path->getPathname());
            }
        } catch (Exception $e){}

        echo 'clear cached: done' . "\n";

        exit;
    }

    public function downloadImeiAction(){


        set_include_path(
            realpath(APPLICATION_PATH . '/../library/phpseclib0.3.6')
        );

        include('Net/SFTP.php');

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');

        $config = $config->toArray();

        $server		=	$config['sftp']['server'];
        $user_name	=	$config['sftp']['user_name'];
        $user_pass	=	$config['sftp']['user_pass'];

        $sftp = new Net_SFTP($server);
        if (!$sftp->login($user_name, $user_pass)) {
            exit('Login Failed');
        }

        // define some variables
        $local_file = '/var/www/html/imei.sql';
        $server_file = '/var/bk/imei.sql';

        // copies filename.remote to filename.local from the SFTP server
        $sftp->get($server_file, $local_file);

        //truncate table

        $con=mysqli_connect($config['resources']['db']['params']['host'],$config['resources']['db']['params']['username'],My_HiddenDB::decryptDb($config['resources']['db']['params']['password']),$config['resources']['db']['params']['dbname']);

        // Check connection
        if (mysqli_connect_errno())
        {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }

        $sql = ' TRUNCATE TABLE `imei` ';

        mysqli_query($con,$sql);

        mysqli_close($con);

        //ENTER THE RELEVANT INFO BELOW
        $mysqlDatabaseName =$config['resources']['db']['params']['dbname'];
        $mysqlUserName 	=	$config['resources']['db']['params']['username'];
        $mysqlPassword 	=	My_HiddenDB::decryptDb($config['resources']['db']['params']['password']);
        $mysqlHostName 	=	$config['resources']['db']['params']['host'];
        $mysqlImportFilename = $local_file;


        //Export the database and output the status to the page
        $command	= 'mysql -h' .$mysqlHostName .' -u' .$mysqlUserName .' -p' .$mysqlPassword .' ' .$mysqlDatabaseName .' < ' .$mysqlImportFilename;
        $output		= array();

        exec($command,$output,$worked);

        switch($worked){
            case 0:
                echo 'Import file ' .$mysqlImportFilename .' successfully imported to database ' .$mysqlDatabaseName . "\n";
                break;
            case 1:
                echo 'There was an error during import.' . "\n";
                break;
        }

        @unlink($local_file);

        exit;

    }

    public function downloadImeiActivationAction(){


        set_include_path(
            realpath(APPLICATION_PATH . '/../library/phpseclib0.3.6')
        );

        include('Net/SFTP.php');

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');

        $config = $config->toArray();

        $server		=	$config['sftp']['server'];
        $user_name	=	$config['sftp']['user_name'];
        $user_pass	=	$config['sftp']['user_pass'];

        $sftp = new Net_SFTP($server);
        if (!$sftp->login($user_name, $user_pass)) {
            exit('Login Failed');
        }

        // define some variables
        $local_file = '/var/www/html/imei_activation.sql';
        $server_file = '/var/bk/imei_activation.sql';

        // copies filename.remote to filename.local from the SFTP server
        $sftp->get($server_file, $local_file);

        //truncate table


        $con=mysqli_connect($config['resources']['db']['params']['host'],$config['resources']['db']['params']['username'],My_HiddenDB::decryptDb($config['resources']['db']['params']['password']),$config['resources']['db']['params']['dbname']);

        // Check connection
        if (mysqli_connect_errno())
        {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }

        $sql = ' TRUNCATE TABLE `imei_activation` ';

        mysqli_query($con,$sql);

        mysqli_close($con);

        //ENTER THE RELEVANT INFO BELOW
        $mysqlDatabaseName =$config['resources']['db']['params']['dbname'];
        $mysqlUserName 	=	$config['resources']['db']['params']['username'];
        $mysqlPassword 	=	My_HiddenDB::decryptDb($config['resources']['db']['params']['password']);
        $mysqlHostName 	=	$config['resources']['db']['params']['host'];
        $mysqlImportFilename = $local_file;


        //Export the database and output the status to the page
        $command	= 'mysql -h' .$mysqlHostName .' -u' .$mysqlUserName .' -p' .$mysqlPassword .' ' .$mysqlDatabaseName .' < ' .$mysqlImportFilename;
        $output		= array();

        exec($command,$output,$worked);

        switch($worked){
            case 0:
                echo 'Import file ' .$mysqlImportFilename .' successfully imported to database ' .$mysqlDatabaseName . "\n";
                break;
            case 1:
                echo 'There was an error during import.' . "\n";
                break;
        }

        @unlink($local_file);

        exit;

    }

    public function downloadMarketAction(){


        set_include_path(
            realpath(APPLICATION_PATH . '/../library/phpseclib0.3.6')
        );

        include('Net/SFTP.php');

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');

        $config = $config->toArray();

        $server		=	$config['sftp']['server'];
        $user_name	=	$config['sftp']['user_name'];
        $user_pass	=	$config['sftp']['user_pass'];

        $sftp = new Net_SFTP($server);
        if (!$sftp->login($user_name, $user_pass)) {
            exit('Login Failed');
        }

        // define some variables
        $local_file = '/var/www/html/market.sql';
        $server_file = '/var/bk/market.sql';

        // copies filename.remote to filename.local from the SFTP server
        $sftp->get($server_file, $local_file);

        //truncate table

        $con=mysqli_connect($config['resources']['db']['params']['host'],$config['resources']['db']['params']['username'],My_HiddenDB::decryptDb($config['resources']['db']['params']['password']),$config['resources']['db']['params']['dbname']);

        // Check connection
        if (mysqli_connect_errno())
        {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }

        $sql = ' TRUNCATE TABLE `market` ';

        mysqli_query($con,$sql);

        mysqli_close($con);

        //ENTER THE RELEVANT INFO BELOW
        $mysqlDatabaseName =$config['resources']['db']['params']['dbname'];
        $mysqlUserName 	=	$config['resources']['db']['params']['username'];
        $mysqlPassword 	=	My_HiddenDB::decryptDb($config['resources']['db']['params']['password']);
        $mysqlHostName 	=	$config['resources']['db']['params']['host'];
        $mysqlImportFilename = $local_file;


        //Export the database and output the status to the page
        $command	= 'mysql -h' .$mysqlHostName .' -u' .$mysqlUserName .' -p' .$mysqlPassword .' ' .$mysqlDatabaseName .' < ' .$mysqlImportFilename;
        $output		= array();

        exec($command,$output,$worked);

        switch($worked){
            case 0:
                echo 'Import file ' .$mysqlImportFilename .' successfully imported to database ' .$mysqlDatabaseName . "\n";
                break;
            case 1:
                echo 'There was an error during import.' . "\n";
                break;
        }

        @unlink($local_file);

        exit;

    }

    public function storeSoldProductAction(){
        ignore_user_abort();
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        error_reporting(0);
        ini_set('display_error', 0);

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');

        $config = $config->toArray();

        $con=mysqli_connect($config['resources']['db']['params']['host'],$config['resources']['db']['params']['username'],My_HiddenDB::decryptDb($config['resources']['db']['params']['password']),$config['resources']['db']['params']['dbname']);

        // Check connection
        if (mysqli_connect_errno())
        {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }

        $sql = ' 	    replace into '.$config['resources']['db']['params']['dbname'].'.store_sold_product

						select sto.id store_id, g.id good_id, count(*) as amount
                        from store sto
                         join timing t on sto.id = t.store
                         join timing_sale ts on t.id = ts.timing_id
                         join wh_good g on ts.product_id = g.id
                        where t.approved_at is not null
                        group by sto.id, g.id
		';

        mysqli_query($con,$sql);

        mysqli_close($con);

        exit;
    }

    public function syncStaffAction()
    {
        error_reporting(0);
        ini_set('display_error', 0);

        My_Request::sync_table(array('table' => 'staff'));

        exit;
    }

    public function syncStaffCsGroupAction()
    {
        error_reporting(0);
        ini_set('display_error', 0);

        My_Request::sync_table(array('table' => 'staff_cs_group'));

        exit;
    }

    public function syncStaffCsPriviledgeAction()
    {
        error_reporting(0);
        ini_set('display_error', 0);

        My_Request::sync_table(array('table' => 'staff_cs_priviledge'));

        exit;
    }
}