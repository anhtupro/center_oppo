<?php 

define('HOST', 'http://center_oppo.test:8080/');

define('DATABASE_TRADE','trade_marketing');
define('DATABASE_CENTER','hr');
define('WAREHOUSE_DB', 'warehouse');
define('CS_DB', 'cs_new');
define('DATABASE_SALARY','salary');
define('REALME_DATABASE_CENTER','realme_hr');
define('HR_DB_RM', 'realme_hr');

define('LIST_PRODUCT_HERO_BI' , serialize(array(703, 772, 708, 696, 691, 778)));
define('PRODUCT_HERO_BI' , 687);
define('PRODUCT_HERO_BI_NAME' , 'F11 pro');
define('LIST_SALE_PGS' , serialize(array(182, 183, 274, 312, 293, 419,417,403)));
define('LIST_PGS_BI' , serialize(array(182, 293, 419,420)));
// define('LIST_PGS_BI' , serialize(array(182, 293, 419,420,174,175,281)));


// Tables Settings
define('MENU_TABLE', 'menu');
define('MENUGROUP_TABLE', 'menu_group');

// Fields Settings
define('MENU_ID', 'id');
define('MENU_PARENT', 'parent_id');
define('MENU_TITLE', 'title');
define('MENU_TITLE_VIE', 'title_vie');
define('MENU_URL', 'url');
define('MENU_CLASS', 'class');
define('MENU_CLASS_NEW', 'class_new');
define('MENU_POSITION', 'position');
define('MENU_GROUP', 'group_id');

define('MENUGROUP_ID', 'id');
define('MENUGROUP_TITLE', 'title');

