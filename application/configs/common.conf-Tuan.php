<?php
include APPLICATION_PATH.'/configs/custom.conf.php';

define('VERSION', "2.2.1");
define('LIMITATION', 10);
define('SUPERADMIN_ID', 5899);
define('ADMINISTRATOR_ID', 1);
define('EMPLOYEE_ID', 6);
define('ASM_ID', 5);
define('PGPB_ID', 4);
define('STORE_LEADER_ID', 29);
define('HR_ID', 7);
define('BOARD_ID', 8);
define('SALES_ID', 9);
define('PB_SALES_ID', 30);
define('SALES_ADMIN_ID', 12);
define('SALES_EXT_ID', 11);
define('HR_EXT_ID', 10);
define('LEADER_ID', 14);
define('ASMSTANDBY_ID', 16);
define('TRAINING_TEAM_ID', 17);
define('TRADE_MARKETING_ID', 20);
define('DIGITAL_ID', 19);
define('TRADE_TEAM', 131);
define('PGPB_TEAM', 16);
define('CALL_CENTER_ID', 23);
define('TRAINING_LEADER_ID', 26);

/* CONFIG by Title */
define('PGPB_TITLE', 182);
define('SALES_TITLE', 183);
define('SALES_TRAINEE_TITLE', 274);
define('LEADER_TITLE', 190);
define('SALES_ACCESSORIES_TITLE', 164);
define('SALES_ACCESSORIES_LEADER_TITLE', 162);
define('SALES_ADMIN_TITLE', 191);
define('SALES_LEADER_TITLE', 190);
define('NHAN_VIEN_KIEM_HANG_TITLE',264);
define('CHUYEN_VIEN_BAN_HANG_TITLE',293);
define('PGPB_PARTTIME_TITLE',375);
define('DELIVERY_MAN_TITLE',295);
define('DELIVERY_ADMIN_TITLE',296);
define('PGPB_2_TITLE', CHUYEN_VIEN_BAN_HANG_TITLE);
define('PB_SALES_TITLE', 312);
define('TRAINER_TITLE_ID', 175);
define('TRUCK_DRIVER_TITLE',316);
define('DRIVER_TITLE',319);

/* add CONFIG by Title  PG Leader*/
define('PG_LEADER_TITLE', 417);

/* End of CONFIG by Title */

define('SALES_TEAM', 75);
define('SALES_ADMIN_TEAM', 119);
define('DELIVERY_TEAM', 294);
define('TRAINING_TEAM', 133);
define('BRANDSHOP_TEAM', 397);

define('TRAINING_TEAM_GROUP_ID', 17);
define('TRADE_MARKETING_GROUP_ID', 20);
defined('MODULE_SALE', 28);

define('HCMC1', 24);
define('HCMC2', 25);
define('HCMC3', 26);
define('HCMC4', 1);
define('HCMC5', 34);

define('HN1', 10);
define('HN2', 31);
define('HN3', 32);
define('HN4', 33);

define('AG', 28);
define('VINH', 14);

define('TIME_LIMIT_TIMING', 24*3600*1 ); // default: 24*3600*1 - Fix 30 ngày tạm thời cho ngày 2/12
define('TIME_LIMIT_TIMING_SALE', 24*3600*6 ); // default: 24*3600*6
define('TIME_LIMIT_DEL_TIMING', 24*3600*1 ); // default: 24*3600*1
define('IMEI_ACTIVATION_EXPIRE', 7); // default: 7 ngày
define('IMEI_APPROVE_TIMING_EXPIRE', 7);

// define('TIME_LIMIT_TIMING', 24*3600*30 );
// define('TIME_LIMIT_TIMING_SALE', 24*3600*30 );
// define('TIME_LIMIT_DEL_TIMING', 24*3600*30 ); 
// define('IMEI_ACTIVATION_EXPIRE', 45);
// define('IMEI_APPROVE_TIMING_EXPIRE', 45);

define('EMAIL_SUFFIX', '@oppomobile.vn' );

define('SALES_KPI', 1 ); // id của dòng tương ứng trong bảng KPI
define('LOG_DASHBOARD', 5 );

define('CSKH_ID', 1 );
define('WARRANTY_CENTER', 161 );
define('CALL_CENTER', 160 );
define('SERVICE_CENTER', 159 );
define('LOGISTICS_ID', 3 );
define('TRAINER_TITLE', 'NHÂN VIÊN TRAINER' );

define('EMAIL_HCM_PGPB', 'hcmpromoter@oppomobile.vn' );
define('EMAIL_CSKH', 'care@oppomobile.vn' );
define('EMAIL_ASM', 'asm@oppomobile.vn' );
define('EMAIL_SALESADMIN', 'saleadmin@oppomobile.vn' );
define('EMAIL_HCM_OFFICE', 'hochiminh@oppomobile.vn' );
define('EMAIL_LOGISTICS', 'vnwarehouse@oppomobile.vn' );
define('EMAIL_TRAINER', 'trainer@oppomobile.vn' );
define('FILENAME_SALT', 'Th0ng!@#' );

define('PHONE_CAT_ID', 11);
define('ACCESS_CAT_ID', 12);

define('DONG_KHUONG', 765);
define('PHONG_LAU', 34);
define('TAG_STAFF', 1);
define('TAG_STAFF_TEMP', 2);

define('HAPPY_TIME_CHECKIN', 1);
define('ACCESSORIES_TEAM', 147);
define('ACCESSORIES_ID', 18);
define('SERVICE_ID', 21);

define('PREVENT_TIMING_AT_TGDD', 1);

define('LOCK_TIMING', 0);
define('PASSWORD_EXPIRE_TIME', 60); // 60 ngày
define('NOT_CHECK_IMEI_ACTIVATED_7_DAYS', 1); // 1 or 0

define('IMEI_CHECKING_FAILED_IN_30_DAYS', 60); // ngày
define('IMEI_CHECKING_FAILED_IN_1_DAYS', 20); // ngày

define('STAFF_PRINT_LOG_UPDATE',1);
define('STAFF_PRINT_LOG_INSERT',2);
define('STAFF_PRINT_LOG_PRINT',3);


define('LIMIT_STAFF_WORKING', 20);
define('LABOUR_CONTRACT' , 2);
define('DAY_OFF_BEGIN' , '2015-03-01');

define('CONTRACT_TERM_12_MONTH', 1);
define('CONTRACT_TERM_LABOUR', 2);
define('CONTRACT_TERM_SEASONAL', 3);
define('CONTRACT_TERM_SEASONAL_CHALLENGE', 4);
define('CONTRACT_TERM_NOT_YET', 5);
define('CONTRACT_TERM_36_MONTH', 6);
define('CONTRACT_TERM_UNLIMITED', 7);
define('CONTRACT_TERM_SEASONAL_PB_SALE', 13);


/* Check SAlE admin duoc edit ho so khu vuc cua minh thoi */
define('CHECK_USER_EDIT_AREA',  1);
/* Check SAlE admin duoc edit ho so khu vuc cua minh thoi */

/* trainer evaluation */
define('TRAINER_EVALUATION_GOOD',1);
define('TRAINER_EVALUATION_AVERAGE',2);
define('TRAINER_EVALUATION_BELOW_AVERAGE',3);

define('TRAINER_EVALUATION_GOOD_NAME','GOOD');
define('TRAINER_EVALUATION_AVERAGE_NAME','AVERAGE');
define('TRAINER_EVALUATION_BELOW_AVERAGE_NAME','BELOW AVERAGE');

define ("TRAINER_EVALUATION", serialize (array (
    TRAINER_EVALUATION_GOOD                      => TRAINER_EVALUATION_GOOD_NAME,
    TRAINER_EVALUATION_AVERAGE                   => TRAINER_EVALUATION_AVERAGE_NAME,
    TRAINER_EVALUATION_BELOW_AVERAGE             => TRAINER_EVALUATION_BELOW_AVERAGE_NAME
)));

/*Type Reward and warning PG*/
define('TYPE_REWARD_PG',1);
define('TYPE_WARNING_PG',2);
define('TYPE_REWARD_PG_NAME','REWARD');
define('TYPE_WARNING_PG_NAME','WARNING');

define ("TYPE_PG", serialize (array (
    TYPE_REWARD_PG         => TYPE_REWARD_PG_NAME,
    TYPE_WARNING_PG        => TYPE_WARNING_PG_NAME
)));
define('EXPORT_ID', 15);

/*Config team Is Head Office*/
define('DEPARTMENT_SALE',               152); // department
define('DEPARTMENT_WARRANTY_CENTER',    161); // department
define('DEPARTMENT_OTHER',    321); // department

define('TEAM_SALE_SALE',                75); // team
define('SALE_SALE_ASM',                 179); // title
define('SALE_SALE_ASM_STANDBY',         181); // title
define('SALE_SALE_PGPB',                182); // title
define('SALE_SALE_SALE',                183); // title
define('SALE_SALE_SALE_LEADER',         190); // title
define('SALE_SALE_SALE_ADMIN',          191); // title
define('SALE_SALE_DELIVERY',            277); // title

define('TEAM_SALE_ACCESSORIES',         147); // team
define('SALE_ACCESSORIES_SALE_LEADER',  162); // title
define('SALE_ACCESSORIES_SALE',         164); // title
define('SALE_ACCESSORIES_SALE_ADMIN',   163); // title

define('TEAM_SALE_DIGITAL',             148); //team
define('SALE_DIGITAL_SALE_LEADER',      165); // title
define('SALE_DIGITAL_SALE',             166); // title
define('SALE_DIGITAL_SALE_ADMIN',       167); // title
define('WESTERN_SALE_ANAGER',           186); // title
define('SALE_DIRECTOR',                 187); // title
define('NATIONAL_SALE_MANAGER',         188); // title
define('STORE_LEADER',                  403); // title
define('SALE_BRANDSHOP',                399); // title

define('TEAM_DELIVERY',         294); // TEAM

define ("CONFIG_NOT_HEAD_OFFICE", serialize (array (
    /*'department' => array(
        array(
            DEPARTMENT_WARRANTY_CENTER
        ),
        array(
            DEPARTMENT_SALE,
            'team' => array(
                array(
                    TEAM_SALE_SALE,
                    'title' => array(
                        SALE_SALE_ASM,
                        SALE_SALE_ASM_STANDBY,
                        SALE_SALE_PGPB,
                        SALE_SALE_SALE,
                        SALE_SALE_SALE_LEADER,
                    )
                ),
                array(
                    TEAM_SALE_ACCESSORIES,
                    'title' => array(
                        SALE_ACCESSORIES_SALE_LEADER,
                        SALE_ACCESSORIES_SALE,
                    ),
                ),
                array(
                    TEAM_SALE_DIGITAL,
                    'title' => array(
                        SALE_DIGITAL_SALE_LEADER,
                        SALE_DIGITAL_SALE,
                    )
                )
            )
        ),
    )*/
    DEPARTMENT_WARRANTY_CENTER => DEPARTMENT_WARRANTY_CENTER,
    DEPARTMENT_SALE => array(
        TEAM_SALE_SALE => array(
            SALE_SALE_ASM => SALE_SALE_ASM,
            SALE_SALE_ASM_STANDBY => SALE_SALE_ASM_STANDBY,
            SALE_SALE_PGPB => SALE_SALE_PGPB,
            SALE_SALE_SALE => SALE_SALE_SALE,
            SALE_SALE_SALE_LEADER => SALE_SALE_SALE_LEADER,
            PB_SALES_TITLE => PB_SALES_TITLE,
            STORE_LEADER => STORE_LEADER,
            SALE_SALE_DELIVERY => SALE_SALE_DELIVERY,
            CHUYEN_VIEN_BAN_HANG_TITLE => CHUYEN_VIEN_BAN_HANG_TITLE,
            PGPB_PARTTIME_TITLE => PGPB_PARTTIME_TITLE,
        ),
        TEAM_SALE_ACCESSORIES => array(
            SALE_ACCESSORIES_SALE_LEADER => SALE_ACCESSORIES_SALE_LEADER,
            SALE_ACCESSORIES_SALE => SALE_ACCESSORIES_SALE,
        ),
        TEAM_SALE_DIGITAL => array(
            SALE_DIGITAL_SALE_LEADER => SALE_DIGITAL_SALE_LEADER,
            SALE_DIGITAL_SALE => SALE_DIGITAL_SALE,
        ),
        TEAM_DELIVERY => array(
            DELIVERY_MAN_TITLE => DELIVERY_MAN_TITLE,
            DELIVERY_ADMIN_TITLE => DELIVERY_ADMIN_TITLE,
            TRUCK_DRIVER_TITLE => TRUCK_DRIVER_TITLE,
            DRIVER_TITLE => DRIVER_TITLE,
        ),
    ),
)));
/*End of Config team Is Head Office*/

define('PGPB_NAME','PGPB');
define('PGPB_2_NAME','Chuyên viên Bán hàng');
define('SALE_SALE_SALE_NAME','SALE');
define('STORE_LEADER_NAME','STORE LEADER');

define ("TRAINER_TITLE_SEARCH", serialize (array (
    PGPB_TITLE     => PGPB_NAME,
    PGPB_2_TITLE   => PGPB_2_NAME,
    SALE_SALE_SALE => SALE_SALE_SALE_NAME
)));

define ("BRANDSHOP_TITLE_SEARCH", serialize (array (
    PGPB_TITLE     => PGPB_NAME,
    PGPB_2_TITLE   => PGPB_2_NAME,
    STORE_LEADER   => STORE_LEADER_NAME
)));

define("TITLE_TRAINER_LEADER",174);
define("TITLE_TRAINING_MANAGER",176);
define("NOTIFICATION_PRIMARY_TYPE" , 1);

define('PERMIT',1);
define('SICK',2);
define('CHILDBEARING', 3);
define('CHILDBEARING_KL',4);
define('OFF_KL',5);
define('OFF_TEMP',6);
define('PERMIT_NAME','NGHỈ PHÉP');
define('SICK_NAME','NGHỈ ỐM');
define('CHILDBEARING_NAME', 'THAI SẢN');
define('CHILDBEARING_KL_NAME','NGHỈ THAI SẢN KHÔNG LƯƠNG');
define('OFF_KL_NAME','NGHỈ KHÔNG LƯƠNG');
define('OFF_TEMP_NAME','TẠM NGHỈ');

define('TEMPORARY_OFF',2);
define('OFF_TYPE',serialize(array(
    PERMIT          => PERMIT_NAME,
    SICK            => SICK_NAME,
    CHILDBEARING    => CHILDBEARING_NAME,
    CHILDBEARING_KL => CHILDBEARING_KL_NAME,
    OFF_KL          => OFF_KL_NAME,
    OFF_TEMP        => OFF_TEMP_NAME,
)));


/*TYPE TRAINING REPORT*/
define('TRAINING_REPORT_INTERNALLY',    1);
define('TRAINING_REPORT_PARTNERS',      2);
define('TRAINING_REPORT_NEW_STAFF',     3);
define('TRAINING_REPORT_INTERNALLY_NAME',    'INTERNAL');
define('TRAINING_REPORT_PARTNERS_NAME',      'PARTNER');
define('TRAINING_REPORT_NEW_STAFF_NAME',     'NEW STAFF');

define ("TYPE_TRAINING_REPORT", serialize (array (
    TRAINING_REPORT_INTERNALLY       => TRAINING_REPORT_INTERNALLY_NAME,
    TRAINING_REPORT_PARTNERS         => TRAINING_REPORT_PARTNERS_NAME,
    //TRAINING_REPORT_NEW_STAFF        => TRAINING_REPORT_NEW_STAFF_NAME
)));

/*TYPE LOYALTY_PLAN*/
define('LOYALTY_PLAN_RULE_TYPE_SELL_IN',            1);
define('LOYALTY_PLAN_RULE_TYPE_SELL_OUT',           2);
define('LOYALTY_PLAN_RULE_TYPE_SELL_IN_NAME',       'SELL IN');
define('LOYALTY_PLAN_RULE_TYPE_SELL_OUT_NAME',      'SELL OUT');

define ("LOYALTY_PLAN_RULE_TYPE", serialize (array (
    LOYALTY_PLAN_RULE_TYPE_SELL_IN                  => LOYALTY_PLAN_RULE_TYPE_SELL_IN_NAME,
    LOYALTY_PLAN_RULE_TYPE_SELL_OUT                 => LOYALTY_PLAN_RULE_TYPE_SELL_OUT_NAME,
)));
/*END TYPE LOYALTY_PLAN*/

/*STATUS ORDER TRAINING*/
define('ORDER_STATUS_PENDING_TRAINING',        1);
define('ORDER_STATUS_COMPLETED_TRAINING',      2);
define('ORDER_STATUS_PENDING_TRAINING_NAME',   'PENDING');
define('ORDER_STATUS_COMPLETED_TRAINING_NAME', 'COMPLETED');

define ("ORDER_STATUS_TRAINING", serialize (array (
    ORDER_STATUS_PENDING_TRAINING       => ORDER_STATUS_PENDING_TRAINING_NAME,
    ORDER_STATUS_COMPLETED_TRAINING     => ORDER_STATUS_COMPLETED_TRAINING_NAME
)));
/*END STATUS ORDER TRAINING*/

define('TITLE_TRAINING_ASSISTANT', 178);

/*RIGHTS TRAINER FULL*/
define ("FULL_RIGHTS_TRAINER", serialize (array (
    TITLE_TRAINING_ASSISTANT,
    TITLE_TRAINING_MANAGER
)));
define('TRAINER_KEY_PROJECT',serialize(array(241,553,644,7278,3026,3028)));
/*END RIGHTS TRAINER FULL*/

define('COMPANY_OPPO' , 1);
define('COMPANY_DIDONGTHONGMINH' , 2);

/* Define Team assign staff for trainer course */
define ("TEAM_FOR_ASSIGN_STAFF_TRAINER_COURSE", serialize (array (
    SALES_TEAM,
    TRAINING_TEAM,
    TRADE_TEAM
)));
/* end define  */

/**define result training for new staff **/
define('RESULT_PASS_TRAINING_NEWSTAFF_COURSE',1);
define('RESULT_FAIL_TRAINING_NEWSTAFF_COURSE',2);
define('RESULT_QUIT_JOB_TRAINING_NEWSTAFF_COURSE',3);

define('RESULT_PASS_TRAINING_NEWSTAFF_COURSE_NAME','PASS');
define('RESULT_FAIL_TRAINING_NEWSTAFF_COURSE_NAME','FAIL');
define('RESULT_QUIT_JOB_TRAINING_NEWSTAFF_COURSE_NAME','QUIT JOB');

define("RESULT_TRANIING_NEWSTAFF_COURSE", serialize (array (
    RESULT_PASS_TRAINING_NEWSTAFF_COURSE => RESULT_PASS_TRAINING_NEWSTAFF_COURSE_NAME,
    RESULT_FAIL_TRAINING_NEWSTAFF_COURSE => RESULT_FAIL_TRAINING_NEWSTAFF_COURSE_NAME,
    RESULT_QUIT_JOB_TRAINING_NEWSTAFF_COURSE => RESULT_QUIT_JOB_TRAINING_NEWSTAFF_COURSE_NAME
)));
/** end define **/

/* define TIME LIMIT LOCK TRAINER COURSE */
define('TIME_LIMIT_LOCK_TRAINER_COURSE',3);
/* end define */

/* define enable tool lock trainer by month */
define('ENABLE_STORE_TRAINER_LOCK_BY_MONTH',1);
/* end define */


define('SIGN_TIME_CHECKIN_SUNDAY_FULLDAY' , 'C');
define('SIGN_TIME_CHECKIN_SUNDAY_HALFDAY' , 'D');
define('SIGN_TIME_CHECKIN_WEEKDAY_HALFDAY' , 'H');
define('SIGN_TIME_CHECKIN_WEEKDAY_FULLDAY' , 'X');
define('SIGN_TIME_CHECKIN_SPECIAL_DAY' ,     'L');
define('SIGN_TIME_CHECKIN_OFF_DAY' ,     'P');
define('SIGN_TIME_CHECKIN_TRAINING',     'T');

define('CATEGORY_ASSET_TRAINER_PHONE',      1);
define('CATEGORY_ASSET_TRAINER_STATIONERY', 2);

define('CATEGORY_ASSET_TRAINER_PHONE_NAME',     'PHONE');
define('CATEGORY_ASSET_TRAINER_STATIONERY_NAME','STATIONERY');

define('CATEGORY_ASSET_TRAINER',serialize(array(
    CATEGORY_ASSET_TRAINER_PHONE        => CATEGORY_ASSET_TRAINER_PHONE_NAME,
    CATEGORY_ASSET_TRAINER_STATIONERY   => CATEGORY_ASSET_TRAINER_STATIONERY_NAME
)));

define('MAX_POINT_TRAINER', 10);

define('USER_TRAINER_FULL_RIGHT', serialize(array(553)));

/*SYSTEM CONFIGURE*/
define('SYSTEM_CENTER',     1);
define('SYSTEM_CS',         2);

define ('SYSTEM_CONFIG', serialize (array (
    SYSTEM_CENTER   => 'Center',
    SYSTEM_CS       => 'CS',
)));

define('SYSTEM_RIGHTS_TYPE_MENU',           1);
define('SYSTEM_RIGHTS_TYPE_ACTION',         2);
define('SYSTEM_RIGHTS_TYPE_AREA',           3);
define('SYSTEM_RIGHTS_TYPE_PROVINCE',       4);
define('SYSTEM_RIGHTS_TYPE_SHOWROOM',       5);
define('SYSTEM_RIGHTS_TYPE_STAFF',          6);
define('SYSTEM_RIGHTS_TYPE_DEFAULT_PAGE',   7);

define ('SYSTEM_RIGHTS_TYPE', serialize (array (
    SYSTEM_RIGHTS_TYPE_MENU                 => 'MENU',
    SYSTEM_RIGHTS_TYPE_ACTION               => 'ACTION',
    SYSTEM_RIGHTS_TYPE_AREA                 => 'AREA',
    SYSTEM_RIGHTS_TYPE_PROVINCE             => 'PROVINCE',
    SYSTEM_RIGHTS_TYPE_SHOWROOM             => 'SHOWROOM',
    SYSTEM_RIGHTS_TYPE_STAFF                => 'STAFF',
    SYSTEM_RIGHTS_TYPE_DEFAULT_PAGE         => 'DEFAULT PAGE',
)));
/*SYSTEM CONFIGURE*/

/*WEBSERVICE*/
define('AUTHENTICATE_KEY', serialize(array(
    'WH' => '6uZ5pMZREEHmRtXrekxT', // warehouse
    'CS' => 'vsv4LRuPHVE7YYFb8A7W', // CS
    'TM' => '3HNbcrC4R6ENPsRzCGvS', // trade marketing
    'FR' => '8GwQwagtpacLmXX9p5ux', // forum
)));
define('PRIMARY_KEY',   '#&$sdfdfs789fs7d');
define('WS_USERNAME',   'tech');
define('WS_PASSWORD',   md5('thongdepchai'));
define('SECRET_KEY' , '6LfSZfsSAAAAALeTUBXEWadNrCYc-vE1ASOOte1W');
/*WEBSERVICE*/

/* TRAINER */
define('TRAINER_ORDER_IN',1);
define('TRAINER_ORDER_OUT',2);
define('TRAINER_ASSET_SOURCE_NEW',      1);
define('TRAINER_ASSET_SOURCE_RETURN', 2);
define('TRAINER_ASSET_SOURCE_NEW_NAME','NEW');
define('TRAINER_ASSET_SOURCE_RETURN_NAME','RETURN');
define('TRAINER_ASSET_SOURCE',serialize(array(
    TRAINER_ASSET_SOURCE_NEW => TRAINER_ASSET_SOURCE_NEW_NAME,
    TRAINER_ASSET_SOURCE_RETURN => TRAINER_ASSET_SOURCE_RETURN_NAME
)));
define('TRAINER_SHIFT_COURSE' , 7);

define('TRAINER_WORK',serialize(array(
    1 => 'TRAINING',
    2 => 'RECRUITMENT',
    3 => 'EVENT',
    4 => 'SALE',
    5 => 'CHECK SHOP',
    6 => 'OTHER',
)));

define('ID_NUMBER_LENGTH',serialize(array(9,12)));

define('STAFF_BUY_40',730);

define('IMEI_LOST_START','2015-12-17');
define('IMEI_LOST_PG',1);
define('IMEI_LOST_PG_NAME','PG BÁO LÊN');
define('IMEI_LOST_ACTIVATED',2);
define('IMEI_LOST_ACTIVATED_NAME','ACTIVATED');
define('IMEI_LOST_IMEI_CHECK',3);
define('IMEI_LOST_IMEI_CHECK_NAME','IMEI CHECK IN');
define('IMEI_LOST_WARRANTY',4);
define('IMEI_LOST_WARRANTY_NAME','SERVICE CENTER');
define('IMEI_LOST_CUSTOMER',5);
define('IMEI_LOST_CUSTOMER_NAME','CUSTOMER');
define('IMEI_LOST_ACTION',serialize(array(
    //IMEI_LOST_PG         => IMEI_LOST_PG_NAME,
    IMEI_LOST_ACTIVATED  => IMEI_LOST_ACTIVATED_NAME,
    IMEI_LOST_IMEI_CHECK => IMEI_LOST_IMEI_CHECK_NAME,
    IMEI_LOST_WARRANTY   => IMEI_LOST_WARRANTY_NAME,
    IMEI_LOST_CUSTOMER   => IMEI_LOST_CUSTOMER_NAME
)));

define('CHECK_IMEI_IN_WAREHOUSE',2);
define('CHECK_IMEI_IN_WAREHOUSE_NAME','IMEI IN WAREHOUSE');

/* Config CS*/
define('STAFF_SHOWROOM_ALL',                            0);
define('STAFF_SHOWROOM_NONE',                           -1);
//1:group;2:privilege menu;3:privilege access;4: privilege default page;5:showroom;6:WH keeper
define('STAFF_CS_PRIVILEGE_TYPE_GROUP',                 1);
define('STAFF_CS_PRIVILEGE_TYPE_MENU',                  2);
define('STAFF_CS_PRIVILEGE_TYPE_ACCESS',                3);
define('STAFF_CS_PRIVILEGE_TYPE_DEFAULT_PAGE',          4);
define('STAFF_CS_PRIVILEGE_TYPE_SHOWROOM',              5);
define('STAFF_CS_PRIVILEGE_TYPE_WH_KEEPER',             6);
define('STAFF_CS_PRIVILEGE_TYPE_CONF',                  7);
/* End of Config CS*/

/* Training Online */
define('TRAINER_LEADER' , 241);
define('TRAINING_SCORES_PASS_FAIL', 5);
define('TRAINING_SCORES_PASS', 6);
define('TRAINING_SCORES_PASS_GOOD', 8);
define('TRAINEE_STAFF_ID', 27);
define('TRAINING_STAFF_GROUP_ID', 27);
/* END Training Online */

/* edit bank and PIT */
define('STAFF_EDIT_BANK_PIT' , 21);
define('LIST_STAFF_EDIT_BANK_PIT' , serialize(array(21,2807)));

define('REGIONAL_HN1',3433);
define('REGIONAL_HN2',4189);
define('AREA_HN1',10);
define('AREA_HN2',51);

define('IMEI_INTERNAL', serialize(array(16232))); // Danh sách nhưng shop internal nhưng vẫn được báo số
define('PGPB_PART_TIME_TITLE', 375);

define('HERO_ID', 519);
define('HERO_NAME', 'F3');

define('AUTHENTICATE_KEY_PARTNER', serialize(array(
                                                 'TGDD_SO' => array(
                                                     'id'        => 1,
                                                     'password'  => 'e3dae86caa2bfb8fbddde16dcf5ff802',
                                                 )
                                             )));

define('JOB_TYPE' , serialize(
    array(
        1 => 'Toàn thời gian',
        2 => 'Part-time',
    )
));
