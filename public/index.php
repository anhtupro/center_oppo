<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once(__DIR__ . '/../library/PhpConsole/__autoload.php');
PhpConsole\Helper::register();

$handler = PhpConsole\Handler::getInstance();
$handler->start();
// Define path to application directory
// APPLICATION_PATH = C:\xampp\htdocs\center_oppo\APLICATION
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
// Nếu có hằng  APPLICATION_ENV thì lấy còn không thì tạo biến = development
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'development'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));


/** Zend_Application */
require_once 'Zend/Application.php';
/** Custom configuration */
include_once APPLICATION_PATH . '/configs/common.conf.php';
date_default_timezone_set('Asia/Saigon');

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

try {
    $application->bootstrap()
                ->run();
} catch (Exception $e){
    var_dump($e->getMessage());
    exit;
}
?>
