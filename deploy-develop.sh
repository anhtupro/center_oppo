ssh bitbucket@171.244.18.74 /bin/bash <<EOT
 cd /var/www/html/center-oppo/
 echo "Backup source..."
 sudo rsync -az --exclude=public --exclude=logs --exclude=bin/log --delete /var/www/html/center-oppo /tmp/backup/
 echo "Backup success!"
 sudo git checkout develop
 echo "Pull..."
 sudo git pull origin develop
 echo "Pull success!"
 sudo chown -R apache.techteam /var/www/html/center-oppo/
 sudo chown -R bitbucket.bitbucket bitbucket-pipelines.yml deploy-develop.sh deploy-master.sh .git .gitignore
 echo "Done!"
EOT
